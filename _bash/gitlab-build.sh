#!/bin/bash

tag=latest;

while getopts "t:" flag
do
  case "${flag}" in
    t) tag="${OPTARG}";;
  esac
done

docker build -t registry.gitlab.com/ii-atl/ts-nextjs-atl-app:$tag .
docker push registry.gitlab.com/ii-atl/ts-nextjs-atl-app:$tag

printf '%s\n%s\n' "[$(date '+%d/%m/%Y %H:%M:%S')] BUILD: $tag" "$(cat _bash/build.log)" > _bash/build.log
