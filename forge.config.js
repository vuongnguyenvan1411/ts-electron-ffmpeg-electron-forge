module.exports = {
  packagerConfig: {
      asar : {
          unpack : 'desktop/.electron/bin/ffmpeg'
      },
      appOutDir : 'out'
  },
  rebuildConfig: {},
  makers: [
    {
      name: '@electron-forge/maker-squirrel',
      config: {
          certificateFile: './cert.pfx',
          certificatePassword: process.env.CERTIFICATE_PASSWORD,
      },
    },
    {
      name: '@electron-forge/maker-zip',
      platforms: ['darwin', 'mas', 'win32'],
    },
    {
      name: '@electron-forge/maker-rpm',
      config: {},
    },
  ],
};
