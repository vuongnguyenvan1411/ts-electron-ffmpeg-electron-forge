const path = require('path');
const withPlugins = require('next-compose-plugins');
const {withSentryConfig} = require('@sentry/nextjs');
const withAntdLess = require('next-plugin-antd-less');
const chalk = require("chalk");

// const {PHASE_DEVELOPMENT_SERVER} = require('next/constants');
// require('dotenv').config({
//     override: true
// })
//
// const nextEnv = {}
//
// Object.entries(process.env).forEach(([key, value]) => {
//     if (key.indexOf('NEXT_PUBLIC') === 0) {
//         nextEnv[key] = value
//     }
// })

console.log(chalk.green('RUN next.config.js'), "\n")

const envNodeEnv = process.env.NODE_ENV
const envBuildType = process.env.BUILD_TYPE

console.log(chalk.green(`- BUILD_TYPE: ${envBuildType}`))
console.log(chalk.green(`- NODE_ENV  : ${envNodeEnv}`))

const isProd = envNodeEnv === 'production'

let distDir = 'build'
let pageExtensions = ['web.tsx', 'next.tsx']

switch (envBuildType) {
    case 'web':
        distDir = isProd ? 'web/build' : 'web/.next'

        break
    case 'docker':
        distDir = 'build'

        break
    case 'desktop':
        distDir = isProd ? 'desktop/next' : 'desktop/.next'

        if (isProd) {
            pageExtensions = ['electron.tsx', 'next.tsx']
        }

        break
}

/**
 * @type {import('next').NextConfig}
 */
const desktopConfigProd = {
    images: {
        unoptimized: true
    },
    exportPathMap: async (defaultMap) => {
        return {
            ...defaultMap,
            ...{
                '/': {page: '/'},
            }
        }
    },
    trailingSlash: true,
    assetPrefix: "."
}

/**
 * @type {import('next').NextConfig}
 */
const desktopConfig = {
    env: {
        NEXT_PUBLIC_WEB_TYPE: 'AppDesktop'
    },
    ...(
        isProd && {...desktopConfigProd}
    )
}

/**
 * @type {import('next').NextConfig}
 */
const nextConfig = {
    // experimental: {
    //     forceSwcTransforms: true
    // },
    productionBrowserSourceMaps: false,
    reactStrictMode: true,
    /** @see https://nextjs.org/docs/advanced-features/output-file-tracing#automatically-copying-traced-files-experimental */
    sassOptions: {
        includePaths: [
            path.join(__dirname, 'src/styles')
        ]
    },
    distDir: distDir,
    // amp: {
    //     canonicalBase: 'src'
    // },
    // webpackDevMiddleware: null,
    eslint: {
        "react/display-name": "off"
    },
    poweredByHeader: false,
    pageExtensions: pageExtensions,
    webpack: (config, {isServer}) => {
        if (!isServer) {
            if (envBuildType === 'desktop') {
                config.target = 'electron-renderer'
            }
        }

        return config
    },
    typescript: {
        // !! WARN !!
        // Dangerously allow production builds to successfully complete even if
        // your project has type errors.
        // !! WARN !!
        ignoreBuildErrors: true
    },
    ...(
        envBuildType === 'desktop'
            ? desktopConfig
            : {
                output: 'standalone'
            }
    )
}

const pluginAntdLess = withAntdLess({
    modifyVars: {
        // '@THEME--DARK': 'theme-dark',
        '@primary-color': '#009B90',
        '@warning-color': '#F5C61D',
        '@error-color': '#EB5757'
    }
})

/**
 * Additional config options for the Sentry Webpack plugin. Keep in mind that
 * the following options are set automatically, and overriding them is not
 * Recommended:
 *  release, url, org, project, authToken, configFile, stripPrefix,
 *  urlPrefix, include, ignore
 *
 * @link https://docs.sentry.io/platforms/javascript/guides/nextjs/
 */
const pluginSentry = withSentryConfig(nextConfig, {
    silent: true, // Suppresses all logs
    /**
     * For all available options
     * @see https://github.com/getsentry/sentry-webpack-plugin#options
     */
    autoInstrumentServerFunctions: false
})

module.exports = async (phase, {defaultConfig}) => {
    return withPlugins(
        [
            [pluginSentry],
            [pluginAntdLess]
        ],
        nextConfig
    )(phase, {...defaultConfig, ...nextConfig})
}
