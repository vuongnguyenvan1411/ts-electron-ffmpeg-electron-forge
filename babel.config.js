module.exports = {
    presets: [
        "next/babel"
    ],
    plugins: [
        [
            "@babel/plugin-proposal-decorators",
            {
                legacy: true
            }
        ],
        [
            "import",
            {
                libraryName: "antd",
                style: true
            }
        ]
    ],
    env: {
        production: {
            plugins: [
                "transform-remove-console"
            ]
        }
    }
}
