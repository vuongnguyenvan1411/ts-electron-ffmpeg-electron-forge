#!/usr/bin/env node

const fs = require("fs");
const path = require("path");
const chalk = require("chalk");

console.log(chalk.green('RUN electron-builder.after.pack.js (Electron builder process after pack app)'))

exports.default = async function (context) {
    const isAsar = context.packager.info._configuration.asar

    if (isAsar) {
        console.log(chalk.yellow('RUN electron-builder.after.pack.js (Electron builder process after pack app)'))

        return
    }

    const dirRootApp = path.join(__dirname, '../')

    let dirAppTo = undefined
    let pathFfmpeg = undefined

    switch (context.packager.platform.name) {
        case 'mac':
            dirAppTo = path.join(dirRootApp, `desktop/release/mac/AutoTimelapsePro.app/Contents/Resources/app`)
            pathFfmpeg = 'macos/ffmpeg'

            break
        case 'windows':
            dirAppTo = path.join(dirRootApp, `desktop/release/win-unpacked/resources/app`)
            pathFfmpeg = 'windows/ffmpeg.exe'

            break
    }

    if (dirAppTo) {
        if (!fs.existsSync(dirAppTo)) {
            console.log(chalk.red(`Error: dir not found ${dirAppTo}`))
        } else {
            const dirNodeModules = path.join(dirAppTo, 'node_modules')

            if (fs.existsSync(dirNodeModules)) {
                fs.rmSync(dirNodeModules, {
                    recursive: true, maxRetries: 2, retryDelay: 1
                })

                console.log(chalk.green('Remove node_modules'))
            }

            const filePackageJsonFrom = path.join(dirRootApp, 'electron/package.json')
            const filePackageJsonTo = path.join(dirAppTo, 'package.json')

            if (!fs.existsSync(filePackageJsonTo)) {
                fs.copyFileSync(filePackageJsonFrom, filePackageJsonTo)

                console.log(chalk.green('Copy override package.json'))
            }

            const dirBinTo = path.join(dirAppTo, 'bin')

            if (!fs.existsSync(filePackageJsonTo)) {
                if (!fs.existsSync(dirBinTo)) {
                    fs.mkdirSync(dirBinTo, {
                        recursive: true
                    })

                    console.log(chalk.green('Make dir bin'))
                }

                if (pathFfmpeg) {
                    const fileBinFfmpegFrom = path.join(dirRootApp, `_bin/ffmpeg/${pathFfmpeg}`)
                    const fileBinFfmpegTo = path.join(dirBinTo, path.basename(pathFfmpeg))

                    if (fs.existsSync(fileBinFfmpegFrom) && !fs.existsSync(fileBinFfmpegTo)) {
                        fs.copyFileSync(fileBinFfmpegFrom, fileBinFfmpegTo)

                        console.log(chalk.green('Copy bin ffmpeg'))
                    } else {
                        console.log(chalk.yellow('Cannot copy bin ffmpeg'))
                    }
                }
            }
        }
    }
}
