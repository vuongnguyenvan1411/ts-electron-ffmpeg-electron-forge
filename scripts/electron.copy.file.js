#!/usr/bin/env node

const fs = require("fs");
const path = require("path");
const chalk = require("chalk");
const moment = require("moment/moment");

console.log(chalk.green('RUN electron.copy.file.js (Electron check & process file|directory before build)'))

const dirRootApp = path.join(__dirname, '../')

const copyRecursiveSync = (src, dest) => {
    const exists = fs.existsSync(src)
    const stats = exists && fs.statSync(src)
    const isDirectory = exists && stats.isDirectory()

    if (isDirectory) {
        fs.mkdirSync(dest)
        fs.readdirSync(src).forEach((childItemName) => {
            copyRecursiveSync(
                path.join(src, childItemName),
                path.join(dest, childItemName)
            )
        })
    } else {
        fs.copyFileSync(src, dest)
    }
}

const args = process.argv.slice(2)

const buildSubPath = args[0] === 'prod' ? 'electron' : '.electron'

/**
 * Copy file events từ /electron sang /src
 * Đồng bộ các sự kiện giao tiếp giữa electron & nextjs
 */
{
    const eventTsFileFrom = path.join(dirRootApp, 'electron/core/events.ts')
    const eventTsFileTo = path.join(dirRootApp, 'src/electron/events.ts')

    let isCopyEventTsFile = false

    if (!fs.existsSync(eventTsFileTo)) {
        isCopyEventTsFile = true
    } else {
        const statsFileFrom = fs.statSync(eventTsFileFrom)
        const statsFileTo = fs.statSync(eventTsFileTo)

        if (statsFileFrom.size !== statsFileTo.size) {
            isCopyEventTsFile = true
        } else if (moment(statsFileFrom.mtime).diff(moment(statsFileTo.mtime), 'seconds') > 0) {
            isCopyEventTsFile = true
        }
    }

    if (isCopyEventTsFile) {
        fs.copyFileSync(eventTsFileFrom, eventTsFileTo)

        console.log(chalk.green('Copy file events.ts to src/electron'))
    }
}
// ------------------------------------
{
    const buildDir = path.join(dirRootApp, `desktop/${buildSubPath}`)

    if (!fs.existsSync(buildDir)) {
        fs.mkdirSync(buildDir, {
            recursive: true
        })

        console.log(chalk.green(`Make directory desktop/${buildSubPath}`))
    }
}
// ------------------------------------
{
    const assetsDir = path.join(dirRootApp, `desktop/${buildSubPath}/assets`)

    if (!fs.existsSync(assetsDir)) {
        copyRecursiveSync(path.join(dirRootApp, 'electron/assets'), assetsDir)

        console.log(chalk.green('Copy directory electron/assets to build'))
    }
}
// ------------------------------------
{
    const localesDir = path.join(dirRootApp, `desktop/${buildSubPath}/locales`)

    if (!fs.existsSync(localesDir)) {
        copyRecursiveSync(path.join(dirRootApp, 'electron/locales'), localesDir)

        if (fs.existsSync(path.join(localesDir, 'i18n.ts'))) {
            fs.unlinkSync(path.join(localesDir, 'i18n.ts'))
        }

        console.log(chalk.green('Copy directory electron/locales to build'))
    }
}
