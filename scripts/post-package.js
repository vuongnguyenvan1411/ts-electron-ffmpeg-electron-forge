const path = require('path');
const fs = require('fs');
const { spawn } = require('child_process');
const electronPackage = require('../forge.config').packagerConfig;

const appOutDir = electronPackage.appOutDir;
const ffmpegPath = path.join(appOutDir, 'ffmpeg');

// Create directory for ffmpeg binary
fs.mkdirSync(path.join(appOutDir, 'bin'), { recursive: true });

// Copy ffmpeg binary to bin directory
fs.copyFileSync(ffmpegPath, path.join(appOutDir, 'bin', 'ffmpeg'));

// Make ffmpeg binary executable
spawn('chmod', ['+x', path.join(appOutDir, 'bin', 'ffmpeg')]);
