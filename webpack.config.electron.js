'use strict';

const path = require('path');
const TerserPlugin = require("terser-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const {DefinePlugin} = require('webpack');
const chalk = require("chalk");

console.log(chalk.green('RUN webpack.config.electron.js'), "\n")

const envNodeEnv = process.env.NODE_ENV
const envIsHostEnv = process.env.IS_HOST_ENV

console.log(chalk.green(`- NODE_ENV : ${envNodeEnv}`))

if (envIsHostEnv) {
    console.log(chalk.green(`- APP_HOST : ${envIsHostEnv}`))
}

const isProd = envNodeEnv === 'production'

module.exports = {
    optimization: {
        minimize: isProd,
        minimizer: [new TerserPlugin({
            terserOptions: {
                compress: {
                    drop_console: true
                },
                output: {
                    comments: false
                },
                sourceMap: false
            }
        })]
    },
    node: {
        __dirname: false
    },
    mode: envNodeEnv,
    entry: {
        index: ['./electron/index.ts'],
        preload: ['./electron/preload.ts'],
    },
    // externals: {
    //     next: 'require("next")'
    // },
    plugins: [
        new CopyPlugin({
            patterns: [
                {
                    from: "./electron/assets",
                    to: "assets"
                },
                {
                    from: "./electron/locales",
                    to: "locales",
                    globOptions: {
                        ignore: [
                            "**/*.ts"
                        ]
                    }
                }
            ]
        }),
        new DefinePlugin({
            'process.env.FLUENTFFMPEG_COV': false,
            'process.env.APP_ROOT_PATH': JSON.stringify(__dirname),
            ...(
                envIsHostEnv && {
                    'process.env.IS_ROOT_HOST': JSON.stringify(envIsHostEnv),
                }
            )
        })
    ],
    devtool: isProd ? false : 'source-map', // inline-source-map
    target: 'electron-main',
    module: {
        /** @link https://github.com/webpack/webpack/issues/196#issuecomment-889603503 */
        exprContextCritical: false,
        rules: [
            {
                test: /\.tsx?$/,
                use: [{loader: 'ts-loader'}]
            }
        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
    output: {
        // filename: '[name]_bundle.js',
        filename: '[name].js',
        path: path.join(__dirname, 'desktop', isProd ? 'electron' : '.electron')
    }
}
