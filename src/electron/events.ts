export class NextTronEventName {
    public static readonly AtlRenderVideoStartDownload = "evt:atl_rvStartDownload"
    public static readonly AtlRenderVideoCompleteDownload = "evt:atl_rvCompleteDownload"
    public static readonly AtlRenderVideoErrorDownload = "evt:atl_rvErrorDownload"
    public static readonly AtlRenderVideoProgressDownload = "evt:atl_rvProgressDownload"
    public static readonly AtlRenderVideoTimeRemainDownload = "evt:atl_rvTimeRemainDownload"


    public static readonly AtlRenderVideoStartRender = "evt:atl_rvStartRender"
    public static readonly AtlRenderVideoCompleteRender = "evt:atl_rvCompleteRender"
    public static readonly AtlRenderVideoErrorRender = "evt:atl_rvErrorRender"
    public static readonly AtlRenderVideoProgressRender = "evt:atl_rvProgressRender"

    public static readonly FormInputGetPathComplete = "evt:form_inputGetPathComplete"

    public static readonly SystemGetPathComplete = "evt:sys_getPathComplete"
    public static readonly SystemGetOsInfoComplete = "evt:sys_getOsInfoComplete"
    public static readonly SystemMonitorOsComplete = "evt:sys_getOsInfoComplete"
}

export class NextTronCmdName {
    public static readonly AtlRenderVideo = "cmd:atl_rv"
    public static readonly FormInputGetPath = "cmd:form_inputGetPath"
    public static readonly SystemGetPath = "cmd:system_getPath"
    public static readonly SystemGetOsInfo = "cmd:system_getOsInfo"
    public static readonly SystemMonitorOs = "cmd:system_monitorOs"
}

export type T_AtlRenderVideoProgressDownload = {
    index: number
    order: number
    percent: number
    percentSuccess: number
    percentError: number
}

export type T_AtlRenderVideoProgressRender = T_FfmpegProgressCallback & {
    remaining: number
}

export type T_FfmpegProgressCallback = {
    frames: number
    currentFps: number
    currentKbps: number
    targetSize: number
    timemark: string
    percent: number
}

export type T_ATL_RenderVideoArgs = {
    name?: string
    output?: string
    size: number
    fps: number
    threads?: number
    items: {
        id: string
        name: string
        order: number
        link: string
    }[]
}

export type T_SystemGetOsInfo = {
    os: {
        platform: string
        arch: string
        version: string
        release: string
    },
    cpu: {
        threads: number
    },
    ram: {
        total: number
    }
}

export type T_SystemMonitorOs = {
    cpu: {
        percent: number
    },
    ram: {
        percent: number
    }
}
