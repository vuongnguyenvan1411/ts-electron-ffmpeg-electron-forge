import {GetStaticProps, NextPage} from "next";
import App from "../client/presentation/App";
import {EDData} from "../client/core/encrypt/EDData";
import moment from "moment";

type _T_Props = {
    data?: any
}

const IndexPage: NextPage<_T_Props> = ({data}) => {
    let state: {} | undefined = undefined;

    if (data) {
        try {
            state = EDData.getData(data)
        } catch (_) {
            //
        }
    }

    return <App state={state}/>
}

export const getStaticProps: GetStaticProps = async (context) => {
    return {
        props: {
            more: EDData.setData({
                now: moment().unix()
            })
        }
    }
}

export default IndexPage
