import '../styles/globals.scss';
import type {AppProps} from 'next/app';
import {Provider as InversifyProvider} from "inversify-react";
import {container} from "../client/config/InversifyConfig";
import {BrowserRouter, HashRouter} from "react-router-dom";
import {ConfigContextProvider} from "../client/presentation/contexts/ConfigContext";
import {SessionContextProvider} from "../client/presentation/contexts/SessionContext";
import Head from "next/head";
import {Fragment, ReactNode, useEffect, useMemo, useState} from "react";
import {RecoilRoot} from "recoil";
import {E_WebType, TNextAppData} from "../client/const/Types";
import {InitTracking} from "../client/presentation/InitTracking";
import {EDData} from "../client/core/encrypt/EDData";
import {RouteConfig} from "../client/config/RouteConfig";

type _T_Props = {
    header: string
    more: string
}

function MyApp({Component, pageProps}: AppProps<_T_Props>) {
    const [render, setRender] = useState<ReactNode>()

    // let state: {} | undefined = undefined
    //
    // if (pageProps && pageProps.hasOwnProperty('data')) {
    //     state = EDFile.decrypt(pageProps.data)
    // }

    useEffect(() => {
        const data: TNextAppData = {}

        if (pageProps) {
            if (pageProps.hasOwnProperty('header')) {
                data.header = EDData.getData(pageProps.header)
            }

            if (pageProps.hasOwnProperty('more')) {
                data.more = EDData.getData(pageProps.more)
            }
        }

        const isInitTracking = process.env.NEXT_PUBLIC_INIT_TRACKING && (process.env.NEXT_PUBLIC_INIT_TRACKING == "true")

        const isDesktop = (
            process.env.NEXT_PUBLIC_WEB_TYPE === E_WebType.AppDesktop
            || global.ipcRenderer !== undefined
            || (global.platform !== undefined && global.platform == E_WebType.AppDesktop)
        )

        let isHashRouter = false

        if (isDesktop) {
            if (global.routerType !== undefined && global.routerType == 'hash') {
                isHashRouter = true
            }
        }

        RouteConfig.isHashRouter = isHashRouter

        const child = (
            <InversifyProvider container={container}>
                <SessionContextProvider data={data}>
                    <ConfigContextProvider data={data}>
                        <RecoilRoot>
                            {/*<App state={state}/>*/}
                            {
                                isInitTracking
                                    ? <InitTracking>
                                        <Component {...pageProps} />
                                    </InitTracking>
                                    : <Component {...pageProps} />
                            }
                        </RecoilRoot>
                    </ConfigContextProvider>
                </SessionContextProvider>
            </InversifyProvider>
        )

        setRender(
            <Fragment>
                {
                    isHashRouter
                        ? <HashRouter>{child}</HashRouter>
                        : <BrowserRouter>{child}</BrowserRouter>
                }
            </Fragment>
        )
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <>
            {
                useMemo(() => (
                    <Head>
                        <title>AutoTimelapse</title>
                        <meta name="description" content="AutoTimelapse"/>
                        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                    </Head>
                ), [])
            }
            {render ?? null}
        </>
    )
}

export default MyApp
