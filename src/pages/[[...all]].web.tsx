import {GetServerSideProps, NextPage} from "next";
import {RouteConfig} from "../client/config/RouteConfig";
import {CHashids} from "../client/core/CHashids";
import {AxiosClient} from "../client/repositories/AxiosClient";
import {ApiService} from "../client/repositories/ApiService";
import Cookies from "universal-cookie";
import {StoreConfig} from "../client/config/StoreConfig";
import {SpaceModel} from "../client/models/service/geodetic/SpaceModel";
import {TimelapseProjectModel} from "../client/models/service/timelapse/TimelapseProjectModel";
import {TParamMachine} from "../client/const/Types";
import {Vr360Model} from "../client/models/Vr360Model";
import App from "../client/presentation/App";
import {E_CookieKey} from "../client/const/Events";
import {EDLocal} from "../client/core/encrypt/EDLocal";
import {EDData} from "../client/core/encrypt/EDData";
import {AccessTokenModel} from "../client/models/UserModel";
import {App as ConstApp} from "../client/const/App";
import moment from "moment";

type TContextParams = {
    all: string[]
}

const AllPage: NextPage<{ data: any }> = props => {
    let state: {} | undefined = undefined;

    if (props.data && props.hasOwnProperty('data')) {
        try {
            state = EDData.getData(props.data)
        } catch (_) {
            //
        }
    }

    return <App state={state}/>
}

export const getServerSideProps: GetServerSideProps = async (context) => {
    context.res.statusCode = 200

    const params = context.params as TContextParams
    const storeConfig = StoreConfig.getInstance()
    const cookies = new Cookies(context.req.headers.cookie)

    const cookieUser = cookies.get(E_CookieKey.User)

    if (cookieUser) {
        const accessToken = EDLocal.getCookie(E_CookieKey.User, cookieUser)

        if (accessToken) {
            storeConfig.accessToken = new AccessTokenModel(accessToken)
        }
    }

    const more = EDData.setData({
        now: moment().unix()
    })

    let props: any = {
        more: more
    }

    if (context.req.headers.authorization) {
        const token = context.req.headers.authorization.replace("Bearer ", "").trim()

        // Store token nodejs call api
        if (token && token.length > 0) {
            storeConfig.accessToken = new AccessTokenModel({
                token: token
            })
        }

        const header: Record<string, any> = {
            token: token,
            appAgent: {}
        }

        if (context.req.headers["o-app-agent"]) {
            const appAgent = (context.req.headers["o-app-agent"] as string).split("|")

            appAgent.map(value => {
                const [k, v] = value.split(":")

                if (k && v) {
                    header["appAgent"][k] = v
                }
            })
        } else {
            if (context.req.headers["o-platform-version"]) {
                header["appAgent"]["platformVersion"] = context.req.headers["o-platform-version"]
            }

            if (context.req.headers["o-device-model"]) {
                header["appAgent"]["deviceModel"] = context.req.headers["o-device-model"]
            }

            // if (context.req.headers["o-os-name"]) {
            //     header["appAgent"]["osName"] = context.req.headers["o-os-name"]
            // }

            if (context.req.headers["o-os-version"]) {
                header["appAgent"]["osVersion"] = context.req.headers["o-os-version"]
            }

            if (context.req.headers["o-platform-name"]) {
                header["appAgent"]["platformName"] = context.req.headers["o-platform-name"]
            }
        }

        props = {
            ...props,
            header: EDData.setData(header)
        }
    }

    if (storeConfig.accessToken) {
        if (params.all && params.all.length >= 2 && 2 in params.all) {
            const path = `/${params.all[0]}/${params.all[1]}`

            const tmp = ConstApp.ApiUrl;

            if (process.env.NEXT_PUBLIC_NODE_API_URL) {
                ConstApp.ApiUrl = `${process.env.NEXT_PUBLIC_NODE_API_URL}/v1/aw`
            }

            switch (path) {
                case `${RouteConfig.TIMELAPSE}/machine`:
                case `${RouteConfig.TIMELAPSE}/view`:
                case `${RouteConfig.TIMELAPSE}/info`:
                case `${RouteConfig.TIMELAPSE}/setting`: {
                    const projectId = CHashids.decodeGetFirst(params.all[2]);

                    if (projectId) {
                        const res = await AxiosClient.get(ApiService.getTimelapseProject(projectId));

                        if (res.success && res.data) {
                            const projectModel = new TimelapseProjectModel(res.data);

                            const encData = EDData.setData({
                                name: projectModel.name,
                                isSensor: projectModel.isSensor,
                                machines: projectModel.machine?.map((value): TParamMachine => {
                                    return {
                                        machineId: value.machineId ?? 0,
                                        name: value.name ?? '',
                                        active: value.active,
                                        total: value.total
                                    }
                                }) ?? []
                            })

                            props = {
                                ...props,
                                data: encData
                            }
                        }
                    }

                    break
                }
                case `${RouteConfig.GEODETIC}/info`:
                case `${RouteConfig.GEODETIC}/vr360`:
                case `${RouteConfig.GEODETIC}/setting`: {
                    const spaceId = CHashids.decodeGetFirst(params.all[2])

                    if (spaceId) {
                        const res = await AxiosClient.get(ApiService.resSpace(spaceId))

                        if (res.success && res.data) {
                            const spaceModel = new SpaceModel(res.data)
                            const encData = EDData.setData({
                                spaceId: spaceModel.spaceId,
                                image: spaceModel.image,
                                name: spaceModel.name
                            })

                            props = {
                                ...props,
                                data: encData
                            }
                        }
                    }

                    break
                }
                case `${RouteConfig.VR360}/view`:
                case `${RouteConfig.VR360}/setting`: {
                    const vr360Id = CHashids.decodeGetFirst(params.all[2]);

                    if (vr360Id) {
                        const res = await AxiosClient.get(ApiService.resVr360(vr360Id))

                        if (res.success) {
                            const vr360Model = new Vr360Model(res.data)
                            const encData = EDData.setData({
                                vr360Id: vr360Model.vr360Id,
                                name: vr360Model.name,
                                link: vr360Model.link,
                                createdAt: vr360Model.createdAtUnix()
                            })

                            props = {
                                ...props,
                                data: encData
                            }
                        }
                    }
                }
            }

            ConstApp.ApiUrl = tmp
        }
    }

    if (Object.keys(props).length > 0) {
        return {
            props: props
        }
    }

    return {
        props: {}
    }
}

export default AllPage
