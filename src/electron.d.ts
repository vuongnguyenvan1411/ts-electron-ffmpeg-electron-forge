import {IpcRenderer} from 'electron'

declare global {
    namespace NodeJS {
        interface Global {
            ipcRenderer: IpcRenderer
            platform: string
            routerType: 'browser' | 'hash'
        }
    }
}
