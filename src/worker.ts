import pi from './workers/pi'

addEventListener('message', (event: MessageEvent<number>) => {
    postMessage(pi(event.data))
})
