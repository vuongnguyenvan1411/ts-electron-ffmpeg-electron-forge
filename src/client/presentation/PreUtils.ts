import {initLng} from "../locales/i18n";

export class PreUtils {
    public static decodeSuffixDay(day: number) {
        const lng: string = initLng();
        let suffixTxt = 'ngày';

        if (lng === "vi") {
            suffixTxt = "ngày"
        } else if (lng === "en") {
            if (day > 1) {
                suffixTxt = "days"
            } else {
                suffixTxt = "day"
            }
        } else if (lng === "zh") {
            suffixTxt = '日'
        }

        return `${day} ${suffixTxt}`;
    }
}
