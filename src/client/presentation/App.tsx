import {Route, Routes} from 'react-router-dom';
import {RouteConfig} from "../config/RouteConfig";
import {PrivateRoute} from "./widgets/PrivateRoute";
import {useLocation} from "react-router";
import {useEffect} from "react";
import {RouterAction} from "../recoil/router/RouterAction";
import {MasterLayoutV2} from "./layouts/MasterLayoutV2";
import {MasterLayoutTest} from "./layouts/MasterLayoutTest";
import {useConfigContext} from "./contexts/ConfigContext";
import {Color} from "../const/Color";

const App = ({...props}) => {
    const [config] = useConfigContext()
    const location = useLocation();

    const {
        detectRouter
    } = RouterAction();

    useEffect(() => {
        // Remove action in app
        if (config.agent?.isWebAppInApp()) {
            console.log(`%cDisable Init & Tracking`, Color.ConsoleWarning)
        } else {
            detectRouter(location.pathname)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [location])

    return (
        <Routes>
            <Route element={<MasterLayoutV2/>}>
                {
                    RouteConfig.routesNotLayout.map((route, index) => (
                        <Route
                            key={index}
                            path={route.path}
                            element={
                                route.protect
                                    ? <PrivateRoute props={{...props, routes: route.routes}} component={route.component}/>
                                    : <route.component {...props} routes={route.routes}/>
                            }
                        />
                    ))
                }
            </Route>
            <Route element={<MasterLayoutTest {...props}/>}>
                {
                    RouteConfig.routesInLayout.map((route, index) => (
                        <Route
                            key={index}
                            path={route.path}
                            element={
                                route.protect
                                    ? <PrivateRoute props={{...props, routes: route.routes}} component={route.component}/>
                                    : <route.component {...props} routes={route.routes}/>
                            }
                        />
                    ))
                }
            </Route>
            {
                RouteConfig.routes.map((route, index) => (
                    <Route
                        key={index}
                        path={route.path}
                        element={
                            route.protect
                                ? <PrivateRoute props={{...props, routes: route.routes}} component={route.component}/>
                                : <route.component {...props} routes={route.routes}/>
                        }
                    />
                ))
            }
        </Routes>
    )
}

export default App;
