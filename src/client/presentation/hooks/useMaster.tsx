import {TOutletCtx} from "../../const/Types";
import {useOutletContext} from "react-router";

type _TContextType = { outletCtx: TOutletCtx }

export const useMaster = () => {
    const {outletCtx} = useOutletContext<_TContextType>();

    return {
        master: outletCtx,
    }
}
