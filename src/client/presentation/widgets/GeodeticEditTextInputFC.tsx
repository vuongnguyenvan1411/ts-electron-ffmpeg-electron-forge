import React, {useState} from "react";
import {Input, Select} from "antd";
import {useTranslation} from "react-i18next";
import {HuePicker} from "react-color";

type FontRotation = '0' | '45' | '90';

interface EditTextValue {
    rotation: FontRotation;
    color: string;
    textInput: string;
}

interface EditTextInputProps {
    value?: EditTextValue;
    onChange?: (value: EditTextValue) => void;
}

export const EditTextInputFC: React.FC<EditTextInputProps> = ({value = {}, onChange}) => {
    const [rotation, setRotation] = useState<FontRotation>('0');
    const [color, setColor] = useState('#000000');
    const [textInput, setTextInput] = useState<string>('');
    const {t} = useTranslation()

    const triggerChange = (changedValue: {
        rotation?: FontRotation;
        color?: string;
        textInput?: string;
    }) => {
        onChange?.({
            rotation,
            color,
            textInput,
            ...value,
            ...changedValue
        })
    }

    const onTextInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newTextInput = e.target.value || ''

        if (!('inputValue' in value)) {
            setTextInput(newTextInput);
        }

        triggerChange({textInput: newTextInput});
    }

    const onRotationChange = (newRotation: FontRotation) => {
        if (!('rotation' in value)) {
            setRotation(newRotation);
        }

        triggerChange({rotation: newRotation});
    }

    const onColorChange = (newColor: any) => {
        setColor(newColor.hex);
        triggerChange({color: newColor.hex})
    }

    return (
        <>
            <div className={'m-1'}>
                {t('text.inputNote')}
            </div>
            <Input
                className={'inputText w-72 mt-1 ml-4'}
                type="text"
                value={value?.textInput || textInput}
                onChange={onTextInputChange}
                // style={{width: 300, margin: '0 8px'}}
            />
            <div className={'m-1'}>
                {t('text.chooseRotation')}
            </div>
            <Select
                value={value?.rotation || rotation}
                className={'select-rotation w-72 mt-1 ml-4'}
                onChange={onRotationChange}
                defaultValue={'0'}
            >
                <Select.Option value="0">
                    0<sup>0</sup>
                </Select.Option>
                <Select.Option value="45">
                    45<sup>0</sup>
                </Select.Option>
                <Select.Option value="90">
                    90<sup>0</sup>
                </Select.Option>
            </Select>
            <div className={'m-1'}>
                {t('text.chooseColor')}
            </div>
            <HuePicker
                color={color}
                onChangeComplete={onColorChange}
            />
        </>
    )
}
