import React, {HTMLProps, MutableRefObject} from "react";
import PlyrJS from 'plyr'
import Plyr, {Options, PlyrEvent as PlyrJSEvent, SourceInfo} from 'plyr'
import PropTypes from 'prop-types'
import useAptor from 'react-aptor'

export type PlyrFCInstance = PlyrJS;
export type PlyrFCEvent = PlyrJSEvent;
export type PlyrFCCallback = (this: PlyrJS, event: PlyrJSEvent) => void;

export type PlyrFCProps = Omit<HTMLProps<HTMLVideoElement>, 'ref'> & {
    source?: SourceInfo,
    options?: Options,
}

export type HTMLPlyrFCVideoElement = HTMLVideoElement & { plyr?: PlyrFCInstance };
export type APITypes = ReturnType<ReturnType<typeof getAPI>>;

const instantiate = (node: NodeList | HTMLElement | HTMLElement[] | string | null, {
    options,
    source
}: { options?: Plyr.Options, source: PlyrJS.SourceInfo }) => {
    // @ts-ignore
    const plyr = new PlyrJS(node, options);

    plyr.source = source;
    return plyr;
}

const destroy = (plyr: PlyrJS | null) => {
    if (plyr) plyr.destroy();
}

const noop = () => {
}

const getAPI = (plyr: PlyrJS | null) => {
    if (!plyr)
        return () => new Proxy(
            {plyr: {source: null}},
            {
                get(target: { plyr: { source: null } }, p: string | symbol, _: any): any {
                    if (p === "plyr") {
                        return target[p];
                    }

                    return noop;
                }
            }
        )

    return () => ({
        plyr
    })
}

/**
 * Luân chuyển ref
 * Hợp nhất api
 */
const PlyrFC = React.forwardRef<APITypes, PlyrFCProps>((props, ref) => {
    const {source, options = null, ...rest} = props;


    // @ts-ignore
    const raptorRef = useAptor(
        ref,
        {
            instantiate,
            getAPI,
            destroy,
            params: {options, source}
        },
        [options, source]
    )

    return (
        <video
            ref={raptorRef as MutableRefObject<HTMLVideoElement>}
            className={'plyr-react plyr'}
            {...rest}
        />
    )
})

PlyrFC.displayName = 'PlyrFC';

PlyrFC.defaultProps = {
    options: {
        captions: {},
        controls: [
            'rewind',
            'play',
            'fast-forward',
            'progress',
            'current-time',
            'duration',
            'mute',
            'volume',
            'settings',
            'fullscreen',
        ],
        i18n: {
            restart: 'Restart',
            rewind: 'Rewind {seektime}s',
            play: 'Play',
            pause: 'Pause',
            fastForward: 'Forward {seektime}s',
            seek: 'Seek',
            seekLabel: '{currentTime} of {duration}',
            played: 'Played',
            buffered: 'Buffered',
            currentTime: 'Current time',
            duration: 'Duration',
            volume: 'Volume',
            mute: 'Mute',
            unmute: 'Unmute',
            enableCaptions: 'Enable captions',
            disableCaptions: 'Disable captions',
            download: 'Download',
            enterFullscreen: 'Enter fullscreen',
            exitFullscreen: 'Exit fullscreen',
            frameTitle: 'Player for {title}',
            captions: 'Captions',
            settings: 'Settings',
            menuBack: 'Go back to previous menu',
            speed: 'Speed',
            normal: 'Normal',
            quality: 'Quality',
            loop: 'Loop',
        }
    },
    source: {
        type: 'video',
        sources: [
            {
                src: 'https://cdn.plyr.io/static/blank.mp4',
                type: 'video/mp4',
                size: 720,
            },
            {
                src: 'https://cdn.plyr.io/static/blank.mp4',
                type: 'video/mp4',
                size: 1080,

            },
        ],

    }
}

PlyrFC.propTypes = {
    options: PropTypes.object,
    source: PropTypes.any,
}

export default PlyrFC;
