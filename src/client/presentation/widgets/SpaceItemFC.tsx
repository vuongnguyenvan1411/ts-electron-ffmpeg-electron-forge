import {SpaceModel} from "../../models/service/geodetic/SpaceModel";
import {useTranslation} from "react-i18next";
import LazyLoad from "react-lazyload";
import {Avatar, Divider, Image as AntImage, Modal} from "antd";
import {RouteConfig} from "../../config/RouteConfig";
import {useState} from "react";
import {CHashids} from "../../core/CHashids";
import {CustomImage} from "../components/CustomImage";
import NextImage from "next/image";
import {useNavigate} from "react-router";
import styles from "../../styles/module/Geodetic.module.scss";
import {CustomTypography} from "../components/CustomTypography";
import {Images} from "../../const/Images";

export const SpaceItemFC = (props: { index?: number, item: SpaceModel }) => {
    const {t} = useTranslation();
    const [isModalVisible, setIsModalVisible] = useState(false);
    const navigate = useNavigate();

    const onClickAvatar = () => {
        setIsModalVisible(true);
    }

    const handleClose = () => {
        setIsModalVisible(false);
    };

    const hash = CHashids.encode(props.item.spaceId);

    const _params = {
        spaceId: props.item.spaceId,
        image: props.item.image,
        name: props.item.name
    }

    return (
        <>
            <LazyLoad
                height={200}
                offset={100}
            >
                <div
                    onClick={(event) => {
                        event.preventDefault();

                        navigate(`${RouteConfig.GEODETIC}/info/${hash}`, {
                            state: _params,
                        })
                    }}
                    className={styles.Item}
                >
                    <div className={"w-full aspect-w-3 aspect-h-2"}>
                        <div className={"absolute inset-0"}>
                            <CustomImage
                                src={props.item.image}
                                alt={props.item.name}
                                objectFit={"fill"}
                            />
                        </div>
                    </div>
                    <Divider className={"divider-main divider-border-4-amber divider-horizontal-m-0"}/>
                    <div className={styles.BoxInfo}>
                        <div className={styles.BoxInfoMeta}>
                            <div className={styles.InfoMeta}>
                                <div
                                    onClick={(event) => {
                                        event.stopPropagation();

                                        onClickAvatar();
                                    }}
                                >
                                    <Avatar
                                        size={32}
                                        src={props.item?.user?.image}
                                    />
                                </div>
                                <CustomTypography
                                    textStyle={"text-16-24"}
                                    isStrong
                                >
                                    {props.item.name?.toUpperCase()}
                                </CustomTypography>
                            </div>
                            <div className={"flex-col flex gap-2 w-full"}>
                                {
                                    props.item.address && <div className={styles.InfoMeta}>
                                        <NextImage
                                            className={"atl-icon atl-icon-color-geodetic"}
                                            width={20}
                                            height={20}
                                            src={Images.iconMapPinLine.data}
                                            alt={Images.iconMapPinLine.atl}
                                        />
                                        <CustomTypography textStyle={"text-14-18"}>
                                            {props.item.address}
                                        </CustomTypography>
                                    </div>
                                }
                                <div className={styles.InfoMeta}>
                                    <NextImage
                                        className={"atl-icon atl-icon-color-geodetic"}
                                        width={20}
                                        height={20}
                                        src={Images.iconCalendar.data}
                                        alt={Images.iconCalendar.atl}
                                    />
                                    <CustomTypography textStyle={"text-14-18"}>
                                        {props.item.createdAtFormatted(t('format.date'))}
                                    </CustomTypography>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        {
                            props.item.total
                                ? <div className={styles.Box_Count_Projects}>
                                    {
                                        props.item.total.m2d && props.item.total.m2d > 0
                                            ? <div className={styles.Box_Count_Item_Project}>
                                                <div>
                                                    <CustomTypography textStyle={"text-14-18"}>
                                                        {props.item.total.m2d < 10 ? "0" : ""}{props.item.total.m2d}
                                                    </CustomTypography>
                                                </div>
                                                <div>
                                                    <CustomTypography
                                                        isStrong
                                                        textStyle={"text-14-18"}
                                                    >
                                                        2D
                                                    </CustomTypography>
                                                </div>
                                            </div>
                                            : null
                                    }
                                    {
                                        props.item.total.m3d && props.item.total.m3d > 0
                                            ? <div className={styles.Box_Count_Item_Project}>
                                                <div>
                                                    <CustomTypography textStyle={"text-14-18"}>
                                                        {props.item.total.m3d < 10 ? "0" : ""}{props.item.total.m3d}
                                                    </CustomTypography>
                                                </div>
                                                <div>
                                                    <CustomTypography
                                                        isStrong
                                                        textStyle={"text-14-18"}
                                                    >
                                                        3D
                                                    </CustomTypography>
                                                </div>
                                            </div>
                                            : null
                                    }
                                    {
                                        props.item.total.vr360 && props.item.total.vr360 > 0
                                            ?
                                            <div className={styles.Box_Count_Item_Project}>
                                                <div>
                                                    <CustomTypography textStyle={"text-14-18"}>
                                                        {props.item.total.vr360 < 10 ? "0" : ""}{props.item.total.vr360}
                                                    </CustomTypography>
                                                </div>
                                                <div>
                                                    <CustomTypography
                                                        isStrong
                                                        textStyle={"text-14-18"}
                                                    >
                                                        VR360
                                                    </CustomTypography>
                                                </div>
                                            </div>
                                            : null
                                    }
                                </div>
                                : null
                        }
                    </div>
                    <div className={styles.BoxActions}>
                        <div
                            onClick={(event) => {
                                event.stopPropagation();

                                navigate(`${RouteConfig.GEODETIC}/info/${hash}`, {
                                    state: _params,
                                })
                            }}
                        >
                            <NextImage
                                className={"atl-icon atl-icon-color-hex-969696"}
                                width={20}
                                height={20}
                                src={Images.iconEye.data}
                                alt={Images.iconEye.atl}
                            />
                            <CustomTypography
                                color={"#969696"}
                                isStrong
                                textStyle={"text-14-18"}
                            >
                                {t("button.view")}
                            </CustomTypography>
                        </div>
                        <div
                            onClick={(event) => {
                                event.stopPropagation();

                                navigate(`${RouteConfig.GEODETIC}/setting/${hash}`, {
                                    state: _params,
                                })
                            }}
                        >
                            <NextImage
                                className={"atl-icon atl-icon-color-hex-969696"}
                                width={20}
                                height={20}
                                src={Images.iconGearOutlined.data}
                                alt={Images.iconGearOutlined.atl}
                            />
                            <CustomTypography
                                color={"#969696"}
                                isStrong
                                textStyle={"text-14-18"}
                            >
                                {t("button.setting")}
                            </CustomTypography>
                        </div>
                    </div>
                </div>
            </LazyLoad>
            {
                props.item?.user && isModalVisible
                    ? <Modal
                        key={props.index}
                        visible={true}
                        onCancel={handleClose}
                        cancelText={t('button.close')}
                        footer={null}
                        bodyStyle={{
                            padding: "0",
                            paddingBottom: "1rem",
                        }}
                    >
                        <AntImage
                            className={"w-full"}
                            src={props.item.user.background}
                            alt={props.item.user.name}
                        />
                        <div
                            className={'text-center'}
                            style={{
                                marginTop: "-3.5rem"
                            }}
                        >
                            <Avatar
                                size={{xs: 24, sm: 32, md: 40, lg: 64, xl: 80, xxl: 100}}
                                src={props.item.user.image}
                                alt={props.item.user.name}
                                style={{
                                    backgroundColor: "#ccc"
                                }}
                            />
                            <div className={"mt-3"}>
                                {props.item.user.name ?? ''}
                            </div>
                        </div>
                    </Modal>
                    : null
            }
        </>
    )
}

export const SpaceSkeleton = () => {
    const {t} = useTranslation();
    return (
        <div className={styles.Item}>
            <div className={"w-full aspect-w-3 aspect-h-2 bg-gray-300"}/>
            <Divider className={"divider-main divider-border-4-amber divider-horizontal-m-0"}/>
            <div className={styles.BoxInfo}>
                <div className={styles.BoxInfoMeta}>
                    <div className={styles.InfoMeta}>
                        <div className={"w-10 h-10 bg-gray-300"}/>
                        <div className={"w-full h-5 bg-gray-300"}/>
                    </div>
                    <div className={"flex-col flex gap-2 w-full"}>
                        <div className={styles.InfoMeta}>
                            <div className={"w-5 h-5 bg-gray-300"}/>
                            <div className={"w-full h-4 bg-gray-300"}/>
                        </div>
                        <div className={styles.InfoMeta}>
                            <div className={"w-5 h-5 bg-gray-300"}/>
                            <div className={"w-10 h-4 bg-gray-300"}/>
                        </div>
                    </div>
                </div>
            </div>
            <div className={styles.Box_Count_Projects}>
                <div className={styles.Box_Count_Item_Project}>
                    <div className={"h-4 w-1 bg-gray-300"}/>
                    <div className={"h-4 w-1 bg-gray-300"}/>
                </div>
                <div className={styles.Box_Count_Item_Project}>
                    <div className={"h-4 w-1 bg-gray-300"}/>
                    <div className={"h-4 w-1 bg-gray-300"}/>
                </div>
                <div className={styles.Box_Count_Item_Project}>
                    <div className={"h-4 w-1 bg-gray-300"}/>
                    <div className={"h-4 w-1 bg-gray-300"}/>
                </div>
            </div>
            <div className={styles.BoxActions}>
                <div>
                    <NextImage
                        className={"atl-icon atl-icon-color-hex-969696"}
                        width={20}
                        height={20}
                        src={Images.iconEye.data}
                        alt={Images.iconEye.atl}
                    />
                    <CustomTypography
                        color={"#969696"}
                        isStrong
                        textStyle={"text-14-18"}
                    >
                        {t("button.view")}
                    </CustomTypography>
                </div>
                <div>
                    <NextImage
                        className={"atl-icon atl-icon-color-hex-969696"}
                        width={20}
                        height={20}
                        src={Images.iconGear.data}
                        alt={Images.iconGear.atl}
                    />
                    <CustomTypography
                        color={"#969696"}
                        isStrong
                        textStyle={"text-14-18"}
                    >
                        {t("button.setting")}
                    </CustomTypography>
                </div>
            </div>
        </div>
    )
}
