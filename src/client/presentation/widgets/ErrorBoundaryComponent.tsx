import React, {ErrorInfo} from "react";

type ErrorState = {
    error?: Error,
    errorInfo?: ErrorInfo,
}

/**
 * @see https://vi.reactjs.org/docs/error-boundaries.html/
 */
class ErrorBoundary extends React.Component {
    state: ErrorState = {
        error: undefined,
        errorInfo: undefined,
    }

    componentDidCatch(error: Error, errorInfo: ErrorInfo) {
        // Catch errors in any components below and re-render with error message
        this.setState({
            error: error,
            errorInfo: errorInfo
        })
        // You can also log error messages to an error reporting service here
    }

    render() {
        if (this.state.errorInfo) {
            // Error path
            return (
                <div>
                    <h2>Something went wrong.</h2>
                    <details style={{whiteSpace: 'pre-wrap'}}>
                        {this.state.error && this.state.error.toString()}
                        <br/>
                        {this.state.errorInfo.componentStack}
                    </details>
                </div>
            )
        }

        return this.props.children;
    }
}

export default ErrorBoundary
