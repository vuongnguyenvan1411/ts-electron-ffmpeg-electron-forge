import React from "react";
import {BaseShareModel} from "../../models/ShareModel";
import {CustomTypography} from "../components/CustomTypography";
import {useTranslation} from "react-i18next";
import NextImage from "next/image";
import {Images} from "../../const/Images";
import {Tooltip} from "antd";
import styles from "../../styles/module/Share.module.scss"

type TShareItemProps = {
    index: number,
    item: BaseShareModel,
    onView: (item: BaseShareModel, index: number) => void,
    onQrCode: (item: BaseShareModel, index: number) => void,
    onCopy: (item: BaseShareModel, index: number) => void,
    onLink: (item: BaseShareModel, index: number) => void,
    onEdit: (item: BaseShareModel, index: number) => void,
    onDelete: (item: BaseShareModel, index: number) => void,
}

export const ShareItemFC: React.FC<TShareItemProps> = (props) => {
    const {t} = useTranslation();

    return (
        <div>
            <div className={`${styles.Item} ${props.item.status ? styles.ItemSuccess : ''}`}>
                <div
                    onClick={() => props.onView(props.item, props.index)}
                    className={styles.ItemInfo}
                >
                    <CustomTypography
                        isStrong
                        textStyle={"text-body-1"}
                    >
                        {props.item.name}
                    </CustomTypography>
                    <div className={"flex flex-col gap-1"}>
                        {
                            props.item.dateEndFormatted() && props.item.dateStartFormatted() ?
                                <CustomTypography textStyle={"text-14-20"}>
                                    {t("text.dateStart")}:&nbsp;&nbsp;{props.item.dateStartFormatted(t('format.dateTimeShort'))}
                                    <br/>
                                    {t("text.dateEnd")}:&nbsp;&nbsp;{props.item.dateEndFormatted(t('format.dateTimeShort'))}
                                </CustomTypography> : props.item.dateStartFormatted() ?
                                    <CustomTypography
                                        textStyle={"text-14-20"}>{t("text.dateStart")}:&nbsp;&nbsp;{props.item.dateStartFormatted(t('format.dateTimeShort'))}</CustomTypography>
                                    : props.item.dateEndFormatted()
                                        ?
                                        <CustomTypography
                                            textStyle={"text-14-20"}>{t("text.dateEnd")}:&nbsp;&nbsp;{props.item.dateEndFormatted(t('format.dateTimeShort'))}</CustomTypography>
                                        : <CustomTypography textStyle={"text-14-20"}>{t("text.activeTime")}:&nbsp;{t("text.unlimited")}</CustomTypography>
                        }
                        <CustomTypography textStyle={"text-14-20"}>
                            {t("text.status")}:&nbsp;<span className={props.item.status ? styles.InfoActive : styles.InfoInActive}>
                        {
                            props.item.status ? t("text.active") : t("text.inActive")
                        }
                    </span>
                        </CustomTypography>
                    </div>
                </div>
                <div className={styles.ItemTool}>
                    <Tooltip
                        title={t('text.codeEmbedQrCode')}
                    >
                    <span className={"anticon"}>
                        <NextImage
                            src={Images.iconQrCode.data}
                            alt={Images.iconQrCode.atl}
                            onClick={() => props.onQrCode(props.item, props.index)}
                        />
                    </span>
                    </Tooltip>
                    <Tooltip
                        title={t('button.copyLink')}
                    >
                    <span className={"anticon"}>
                                    <NextImage
                                        src={Images.iconCopy.data}
                                        alt={Images.iconCopy.atl}
                                        onClick={() => props.onCopy(props.item, props.index)}
                                    />
                    </span>
                    </Tooltip>
                    <Tooltip
                        title={t('text.linkShare')}
                    >
                    <span className={"anticon"}>
                                    <NextImage
                                        src={Images.iconLinkSimple.data}
                                        alt={Images.iconLinkSimple.atl}
                                        onClick={() => props.onLink(props.item, props.index)}
                                    />
                    </span>
                    </Tooltip>
                    <Tooltip
                        title={t("button.edit")}
                    >
                    <span className={"anticon"}>
                                   <NextImage
                                       src={Images.iconPencilSimpleLine.data}
                                       alt={Images.iconPencilSimpleLine.atl}
                                       onClick={() => props.onEdit(props.item, props.index)}
                                   />
                    </span>
                    </Tooltip>
                    <Tooltip
                        title={t("button.delete")}
                    >
                    <span className={"anticon"}>
                         <NextImage
                             src={Images.iconTrash.data}
                             alt={Images.iconTrash.atl}
                             onClick={() => props.onDelete(props.item, props.index)}
                         />
                    </span>
                    </Tooltip>
                </div>
            </div>
        </div>
    )
}

export const ShareItemSkeleton = () => {
    return (
        <div className={"p-4 flex flex-col gap-3 bg-white"}>
            <div className={"flex flex-col gap-2 w-full animate-pulse"}>
                <div className={"bg-gray-300 h-6 w-2/5"}/>
                <div className={"flex flex-col gap-1"}>
                    <div className={"bg-gray-300 h-5 w-3/5"}/>
                    <div className={"bg-gray-300 h-5 w-3/6"}/>
                </div>
            </div>
            <div className={"flex justify-between"}>
                <div className={"bg-gray-300 h-6 w-6"}/>
                <div className={"bg-gray-300 h-6 w-6"}/>
                <div className={"bg-gray-300 h-6 w-6"}/>
                <div className={"bg-gray-300 h-6 w-6"}/>
                <div className={"bg-gray-300 h-6 w-6"}/>
            </div>
        </div>
    )
}