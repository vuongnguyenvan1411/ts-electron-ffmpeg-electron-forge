import {TParamPartGeodetic} from "../../const/Types";
import React, {useEffect, useState} from "react";
import {Button, Menu, MenuProps, Modal, Tabs} from "antd";
import {FolderViewOutlined} from "@ant-design/icons";
import {Link} from "react-router-dom";
import {RouteConfig} from "../../config/RouteConfig";
import {CHashids} from "../../core/CHashids";
import {useTranslation} from "react-i18next";
import Image from "next/image";
import {Images} from "../../const/Images";
import sms from "../../presentation/modules/v2d/styles/MapView.module.scss";
import {CustomTypography} from "../components/CustomTypography";

type MenuItem = Required<MenuProps>['items'][number];

export const GeodeticPartMenuFC = React.memo((props: {
    parts?: TParamPartGeodetic,
    active?: {
        id: number,
        type: string
    }
    isVisible?: boolean,
    onClose?: () => void,
}) => {
    const {t} = useTranslation();

    const [isModalVisible, setIsModalVisible] = useState<boolean>(false);

    useEffect(() => {
        if (props.isVisible) {
            setIsModalVisible(true)
        }
    }, [props.isVisible])

    const onClosePartModal = () => {
        setIsModalVisible(false)
        if (props.onClose) {
            props.onClose()
        }
    }

    const getMenuItem = (
        label: React.ReactNode,
        key?: React.Key | null,
        icon?: React.ReactNode,
        children?: MenuItem[],
    ): MenuItem => {
        return {
            key,
            icon,
            children,
            label,
        } as MenuItem;
    }

    if (
        props.parts
        && ((props.parts.m2d && props.parts.m2d.length > 0)
            || (props.parts.m3d && props.parts.m3d.length > 0)
            || (props.parts.vr360 && props.parts.vr360.length > 0)
        )
    ) {
        return (
            <>
                <Button
                    className={'border-none'}
                    type="primary"
                    icon={<FolderViewOutlined/>}
                    onClick={() => setIsModalVisible(true)}
                />
                <Modal
                    className={'modal-main ant-modal-v2d small'}
                    title={<span className={"text-base"}>{t('text.productsInProject')}</span>}
                    visible={isModalVisible}
                    onCancel={onClosePartModal}
                    bodyStyle={{padding: 0}}
                    footer={null}
                    closeIcon={<Image
                        height={18}
                        width={18}
                        src={Images.iconXCircle.data}
                        alt={Images.iconXCircle.atl}
                    />}
                >
                    <Tabs
                        className={props.parts.m2d ? 'tabs-main' : ''}
                        defaultActiveKey={props.active?.type}
                        tabBarStyle={{
                            paddingLeft: "1rem"
                        }}
                    >
                        {
                            (props.parts.m2d && props.parts.m2d.length > 0) &&
                            <Tabs.TabPane
                                tab={
                                    <>
                                            <span className={sms.TabSwitchPart}>
                                                <CustomTypography
                                                    textStyle={'text-14-18'}
                                                    color={'#FFFFFF'}
                                                >
                                                    {String(props.parts.m2d.length).padStart(2, '0')}
                                                </CustomTypography>
                                            </span>
                                        <CustomTypography
                                            textStyle={'text-14-18'}
                                            color={'#4F4F4F'}
                                            isStrong
                                        >
                                            2D
                                        </CustomTypography>
                                    </>
                                }
                                key="m2d"
                            >
                                <Menu
                                    className={'menu-v2d menu-part'}
                                    defaultSelectedKeys={props.active && props.active.type === 'm2d' ? [props.active.id.toString()] : []}
                                    items={
                                        props.parts.m2d?.map((value, index) => {
                                            const hash = CHashids.connection("main").encode([value.spaceId, value.m2dId])

                                            return getMenuItem(
                                                (
                                                    <Link
                                                        to={`${RouteConfig.GEODETIC}/m2d/${hash}`}
                                                        state={{
                                                            spaceId: value.spaceId,
                                                            m2dId: value.m2dId,
                                                            name: value.name,
                                                            parts: props.parts
                                                        }}
                                                        replace={true}
                                                    >
                                                        <CustomTypography
                                                            color={'#4F4F4F'}
                                                            textStyle={'text-12-18'}
                                                        >
                                                            {value.name}
                                                        </CustomTypography>
                                                    </Link>
                                                ),
                                                index
                                            )
                                        })
                                    }
                                />
                            </Tabs.TabPane>
                        }
                        {
                            (props.parts.m3d && props.parts.m3d.length > 0) &&
                            <Tabs.TabPane
                                tab={
                                    <>
                                            <span className={sms.TabSwitchPart}>
                                                <CustomTypography
                                                    textStyle={'text-14-18'}
                                                    color={'#FFFFFF'}
                                                >
                                                 {String(props.parts.m3d.length).padStart(2, '0')}
                                                </CustomTypography>
                                            </span>
                                        <CustomTypography
                                            textStyle={'text-14-18'}
                                            color={'#4F4F4F'}
                                            isStrong
                                        >
                                            3D
                                        </CustomTypography>
                                    </>
                                }
                                key="m3d"
                            >
                                <Menu
                                    className={'menu-v2d menu-part'}
                                    defaultSelectedKeys={props.active && props.active.type === 'm3d' ? [props.active.id.toString()] : []}
                                    items={
                                        props.parts.m3d?.map((value, index) => {
                                            const hash = CHashids.connection("main").encode([value.spaceId, value.m3dId])

                                            return getMenuItem(
                                                (
                                                    <Link
                                                        to={`${RouteConfig.GEODETIC}/pc3d/${hash}`}
                                                        replace={true}
                                                        state={{
                                                            spaceId: value.spaceId,
                                                            m3dId: value.m3dId,
                                                            name: value.name,
                                                            parts: props.parts
                                                        }}
                                                    >
                                                        <CustomTypography
                                                            color={'#4F4F4F'}
                                                            textStyle={'text-12-18'}
                                                        >
                                                            {value.name}
                                                        </CustomTypography>
                                                    </Link>
                                                ),
                                                index
                                            )
                                        })
                                    }
                                />
                            </Tabs.TabPane>
                        }
                        {
                            (props.parts.vr360 && props.parts.vr360.length > 0) &&
                            <Tabs.TabPane
                                tab={
                                    <>
                                            <span className={sms.TabSwitchPart}>
                                                <CustomTypography
                                                    textStyle={'text-14-18'}
                                                    color={'#FFFFFF'}
                                                >
                                                    {String(props.parts.vr360.length).padStart(2, '0')}
                                                </CustomTypography>
                                            </span>
                                        <CustomTypography
                                            textStyle={'text-14-18'}
                                            color={'#4F4F4F'}
                                            isStrong
                                        >
                                            VR 360
                                        </CustomTypography>
                                    </>
                                }
                                key="vr360"
                            >
                                <Menu
                                    className={'menu-v2d menu-part'}
                                    defaultSelectedKeys={props.active && props.active.type === 'vr360' ? [props.active.id.toString()] : []}
                                    items={
                                        props.parts.vr360?.map((value, index) => {
                                            const hash = CHashids.connection("main").encode([value.spaceId, value.vr360Id])

                                            return getMenuItem(
                                                (
                                                    <Link
                                                        to={`${RouteConfig.GEODETIC}/vr360/${hash}`}
                                                        replace={true}
                                                        state={{
                                                            spaceId: value.spaceId,
                                                            vr360Id: value.vr360Id,
                                                            name: value.name,
                                                            parts: props.parts
                                                        }}
                                                    >
                                                        <CustomTypography
                                                            color={'#4F4F4F'}
                                                            textStyle={'text-12-18'}
                                                        >
                                                            {value.name}
                                                        </CustomTypography>
                                                    </Link>
                                                ),
                                                index
                                            )
                                        })
                                    }
                                />
                            </Tabs.TabPane>
                        }
                    </Tabs>
                </Modal>
            </>
        )
    }

    return null;
})
