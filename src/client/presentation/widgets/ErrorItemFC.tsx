import {useTranslation} from "react-i18next";
import {SendingStatus} from "../../const/Events";
import {Button, Col, Modal, notification, Result, Row, Space, Typography} from "antd";
import {RouteConfig} from "../../config/RouteConfig";
import {CloseCircleOutlined, FrownOutlined} from "@ant-design/icons";
import {includes} from "lodash";
import {useNavigate} from "react-router";

export const ErrorItemFC = (props: {
    children: any;
    status?: SendingStatus,
    typeView?: 'default' | 'modal' | 'notification',
    onRefresh?: Function | null
}) => {
    const {t} = useTranslation()
    const navigate = useNavigate()

    if (!includes([SendingStatus.disConnect, SendingStatus.serverError, SendingStatus.unauthorized, SendingStatus.unauthorized], props.status)) {
        if (props.status === SendingStatus.maintenance) {
            Modal.error({
                title: t('title.maintenance'),
                content: (
                    <div className={"text-justify"}>
                        {t('message.maintenance')}
                    </div>
                ),
                okText: (
                    <>
                        <CloseCircleOutlined className="site-result-demo-error-icon mr-2"/>{t('button.close')}
                    </>
                ),
                okType: "default"
            });

            return (
                <></>
            );
        } else {
            return (
                props.children
            );
        }
    }

    if (props.typeView === 'modal') {
        if (props.status === SendingStatus.disConnect) {
            Modal.error({
                title: t('message.noInternet'),
                content: (
                    <>
                        <p className={"mb-0"}>
                            <CloseCircleOutlined className="site-result-demo-error-icon mr-2"/>{t('text.checkModem')}
                        </p>
                        <p className={"mb-0"}>
                            <CloseCircleOutlined className="site-result-demo-error-icon mr-2"/>{t('text.reconnectWifi')}
                        </p>
                    </>
                ),
                okText: (
                    <>
                        <CloseCircleOutlined className="site-result-demo-error-icon mr-2"/>{t('button.close')}
                    </>
                ),
                okType: "default"
            });
        } else if (props.status === SendingStatus.serverError) {
            Modal.error({
                title: t('text.error'),
                content: (
                    <div className={"text-justify"}>
                        {t('message.confirmError')}
                    </div>
                ),
                okText: (
                    <>
                        <CloseCircleOutlined className="site-result-demo-error-icon mr-2"/>{t('button.close')}
                    </>
                ),
                okType: "default"
            });
        } else if (props.status === SendingStatus.unauthorized) {
            Modal.error({
                title: t('text.titleLoginAgain'),
                content: (
                    <div className={"text-justify"}>
                        {t('text.contentUnAuthorized')}
                    </div>
                ),
                okText: (
                    <>
                        <CloseCircleOutlined className="site-result-demo-error-icon mr-2"/>{t('button.close')}
                    </>
                ),
                okType: "default"
            });
        }

        return (
            props.children
        );
    } else if (props.typeView === 'notification') {
        if (props.status === SendingStatus.disConnect) {
            notification.error({
                message: t('message.noInternet'),
                description: (
                    <>
                        <p className={"mb-0"}>
                            <CloseCircleOutlined className="site-result-demo-error-icon mr-2"/>{t('text.checkModem')}
                        </p>
                        <p className={"mb-0"}>
                            <CloseCircleOutlined className="site-result-demo-error-icon mr-2"/>{t('text.reconnectWifi')}
                        </p>
                    </>
                )
            });
        } else if (props.status === SendingStatus.serverError) {
            notification.error({
                message: t('text.error'),
                description: (
                    <div className={"text-justify"}>{t('message.confirmError')}</div>
                ),
            });
        } else if (props.status === SendingStatus.unauthorized) {
            notification.error({
                message: t('text.titleLoginAgain'),
                description: (
                    <div className={"text-justify"}>{t('text.contentUnAuthorized')}</div>
                ),
            });
        }

        return (
            props.children
        );
    } else {
        return (
            <Row justify={"center"}>
                <Col xs={24} sm={24} md={24} lg={18} xl={12} xxl={12}>
                    {
                        props.status === SendingStatus.disConnect
                            ? <Result
                                status="error"
                                title={t('message.noInternet')}
                            >
                                <div className="desc">
                                    <Typography.Paragraph>
                                        <Typography.Text
                                            strong
                                            style={{
                                                fontSize: 16,
                                            }}
                                        >
                                            {t('text.tryStepToConnect')}
                                        </Typography.Text>
                                    </Typography.Paragraph>
                                    <Typography.Paragraph>
                                        <CloseCircleOutlined className="site-result-demo-error-icon mr-2"/>{t('text.checkModem')}
                                    </Typography.Paragraph>
                                    <Typography.Paragraph>
                                        <CloseCircleOutlined className="site-result-demo-error-icon mr-2"/>{t('text.reconnectWifi')}
                                    </Typography.Paragraph>
                                </div>
                            </Result>
                            :
                            props.status === SendingStatus.serverError
                                ? <div>
                                    <Result
                                        status="500"
                                        title={t('text.error')}
                                        subTitle={t('message.confirmError')}
                                        extra={
                                            <Space>
                                                <Button
                                                    type="primary"
                                                    onClick={() => navigate(RouteConfig.HOME)}
                                                >
                                                    {t('title.home')}
                                                </Button>
                                                <Button
                                                    type="primary"
                                                    onClick={() => props.onRefresh != null ? props.onRefresh() : null}
                                                >
                                                    {t('button.tryAgain')}
                                                </Button>
                                            </Space>
                                        }
                                    />
                                </div>
                                : props.status === SendingStatus.unauthorized
                                    ? <Result
                                        status={"error"}
                                        icon={<FrownOutlined/>}
                                        title={t('text.noData')}
                                    />
                                    : null
                    }
                </Col>
            </Row>
        )
    }
}
