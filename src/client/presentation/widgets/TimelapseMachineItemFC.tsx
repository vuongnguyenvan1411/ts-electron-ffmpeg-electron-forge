import {TimelapseMachineModel} from "../../models/service/timelapse/TimelapseMachineModel";
import {RouteConfig} from "../../config/RouteConfig";
import {CHashids} from "../../core/CHashids";
import {useTranslation} from "react-i18next";
import {Utils} from "../../core/Utils";
import {TParamMachine} from "../../const/Types";
import Image from "next/image";
import NextImage from "next/image";
import NoImage from "../../assets/image/no_image.png";
import LazyLoad from "react-lazyload";
import styles from "../../styles/module/TimelapseMachineItem.module.scss";
import {CustomImage} from "../components/CustomImage";
import {Images} from "../../const/Images";
import {CustomTypography} from "../components/CustomTypography";
import moment from "moment/moment";
import {useNavigate} from "react-router";
import {PreUtils} from "../PreUtils";
import {E_ResUrlType} from "../../const/Events";

export const TimelapseMachineItemFC = (props: {
    item: TimelapseMachineModel,
    machines: TParamMachine[],
    index?: number,
    projectId: number,
    isSensor?: boolean
}) => {
    let navigate = useNavigate();
    const {t} = useTranslation()
    const hash = CHashids.encode([props.projectId, props.item.machineId])

    return (
        <LazyLoad
            height={200}
            offset={100}
        >
            <div
                onClick={() => navigate(`${RouteConfig.TIMELAPSE}/view/${hash}/image`, {
                    state: {
                        name: props.item.name,
                        isSensor: props.isSensor,
                        machines: props.machines,
                        allTime: [props.item.firstDateShotFormatted(), props.item.lastDateShotFormatted()]
                    },
                })}
                className={styles.Item}
            >
                <div className={"relative"}>
                    <div className={"w-full aspect-w-3 aspect-h-2"}>
                        <div className={"absolute inset-0"}>
                            <CustomImage
                                src={props.item.last ? props.item.last.getShotImageUrl({
                                    type: E_ResUrlType.Thumb,
                                    size: 426
                                }) : NoImage.src}
                                alt={props.item.name}
                                objectFit={"fill"}
                            />
                        </div>
                    </div>
                    {
                        (props.item.last?.sensor && props.item.last.sensor.length > 0) && (
                            <div className={`${styles.InfoSensor}`}>
                                {
                                    props.item.last.sensor.map((item, index) => {
                                        let icon;

                                        if (item.sensor === 'air_temp') {
                                            icon = <Image
                                                height={16}
                                                width={16}
                                                src={Images.iconThermometer.data}
                                                alt={Images.iconThermometer.atl}
                                            />;
                                        } else if (item.sensor === 'air_humid') {
                                            icon = <Image
                                                height={16}
                                                width={16}
                                                src={Images.iconSunDim.data}
                                                alt={Images.iconSunDim.atl}
                                            />;
                                        } else {
                                            return null;
                                        }

                                        return (
                                            <div key={index}>
                                                {icon}
                                                <CustomTypography
                                                    textStyle={"text-info-height-0"}
                                                    isStrong={true}
                                                >
                                                    {item.value}&nbsp;{item.unit2}
                                                </CustomTypography>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        )
                    }
                    {
                        (props.item.total && props.item.size) && (
                            <div className={`${styles.InfoTotal}`}>
                                <div>
                                    <Image
                                        height={16}
                                        width={16}
                                        src={Images.iconImage.data}
                                        alt={Images.iconImage.atl}
                                    />
                                    <CustomTypography
                                        textStyle={"text-info-height-0"}
                                        isStrong={true}
                                    >
                                        {props.item.total}
                                    </CustomTypography>
                                </div>
                                <div>
                                    <Image
                                        height={16}
                                        width={16}
                                        src={Images.iconFolder.data}
                                        alt={Images.iconFolder.atl}
                                    />
                                    <CustomTypography
                                        textStyle={"text-info-height-0"}
                                        isStrong={true}
                                    >
                                        {Utils.formatByte(props.item.size, 0)}
                                    </CustomTypography>
                                </div>
                            </div>
                        )
                    }
                </div>
                <div className={styles.Divider}/>
                <div className={styles.BoxInfo}>
                    <div className={styles.BoxInfoMeta}>
                        <CustomTypography
                            isStrong={true}
                            textStyle={"text-body-text-1"}
                            style={{
                                whiteSpace: "pre",
                                overflow: "hidden",
                                textOverflow: "clip",
                            }}
                        >
                            {props.item.name?.toUpperCase()}
                        </CustomTypography>
                        <div className={styles.InfoMeta}>
                            <div className={styles.Meta}>
                                <Image
                                    height={20}
                                    width={20}
                                    src={Images.iconCalendar.data}
                                    alt={Images.iconCalendar.atl}
                                />
                                <CustomTypography
                                    textStyle={"text-body-text-2"}
                                >
                                    {props.item.createdAtFormatted(t('format.date'))}
                                </CustomTypography>
                            </div>
                            {
                                props.item.lastDateShotFormatted(t('format.dateTimeShort')) &&
                                <>
                                    <NextImage
                                        src={Images.iconArrowLine.data}
                                        alt={Images.iconArrowLine.atl}
                                    />
                                    <CustomTypography textStyle={"text-body-text-2"}>
                                        {
                                            props.item.lastDateShotFormatted(t('format.timeShort'))
                                        }
                                        &nbsp;
                                        <span
                                            style={{
                                                color: "transparent",
                                                textShadow: "0 0 0 rgba(0, 155, 144, 1)",
                                            }}
                                        >
                                            &#10072;
                                        </span>
                                        &nbsp;
                                        {
                                            props.item.lastDateShotFormatted(t('format.date'))
                                        }
                                    </CustomTypography>
                                </>
                            }
                        </div>
                        <div className={styles.InfoMeta}>
                            <div className={styles.Meta}>
                                <Image
                                    height={20}
                                    width={20}
                                    src={Images.iconClock.data}
                                    alt={Images.iconClock.atl}
                                />
                                <CustomTypography
                                    textStyle={"text-body-text-2"}
                                >
                                    {
                                        PreUtils.decodeSuffixDay(
                                            moment(props.item.lastDateShotFormatted(t('format.date')), `${t('format.date')}`).diff(moment(props.item.createdAtFormatted(t('format.date')), `${t('format.date')}`), 'days')
                                        )
                                    }
                                </CustomTypography>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </LazyLoad>
    )
}

export const TimelapseMachineSkeleton = () => {
    return (
        <div className={styles.Item}>
            <div className={"relative"}>
                <div className={"w-full aspect-w-3 aspect-h-2 bg-gray-200"}/>
                <div className={`${styles.InfoSensor}`}>
                    <div>
                        <div className={"w-4 h-4 bg-gray-200"}/>
                        <div className={"w-7 h-2 bg-gray-200"}/>
                    </div>
                    <div>
                        <div className={"w-4 h-4 bg-gray-200"}/>
                        <div className={"w-7 h-2 bg-gray-200"}/>
                    </div>
                </div>
                <div className={`${styles.InfoTotal}`}>
                    <div>
                        <div className={"w-4 h-4 bg-gray-200"}/>
                        <div className={"w-7 h-2 bg-gray-200"}/>
                    </div>
                    <div>
                        <div className={"w-4 h-4 bg-gray-200"}/>
                        <div className={"w-7 h-2 bg-gray-200"}/>
                    </div>
                </div>
            </div>
            <div className={styles.Divider}/>
            <div className={styles.BoxInfo}>
                <div className={styles.BoxInfoMeta}>
                    <div className={"w-10 h-3 bg-gray-200"}/>
                    <div className={styles.InfoMeta}>
                        <div className={"w-5 h-5 bg-gray-200"}/>
                        <div className={"w-10 h-3 bg-gray-200"}/>
                        <div className={"w-5 h-5 bg-gray-200"}/>
                        <div className={"w-10 h-3 bg-gray-200"}/>
                    </div>
                    <div className={styles.InfoMeta}>
                        <div className={"w-5 h-5 bg-gray-200"}/>
                        <div className={"w-3 h-3 bg-gray-200"}/>
                    </div>
                </div>
            </div>
        </div>
    )
}
