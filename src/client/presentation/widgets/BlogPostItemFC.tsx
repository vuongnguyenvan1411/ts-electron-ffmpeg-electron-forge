import {Divider} from "antd";
import {BlogPostModel} from "../../models/blog/BlogPostModel";
import {CustomImage} from "../components/CustomImage";
import LazyLoad from "react-lazyload";
import {useTranslation} from "react-i18next";
import {RouteConfig} from "../../config/RouteConfig";
import {CHashids} from "../../core/CHashids";
import styles from "../../styles/module/Blog.module.scss";
import NextImage from "next/image";
import {Images} from "../../const/Images";
import {CustomTypography} from "../components/CustomTypography";
import {useNavigate} from "react-router";

export const BlogPostItemFC = (props: {
    item: BlogPostModel,
    index?: number,
}) => {
    const {t} = useTranslation();
    const navigate = useNavigate();
    const hash = CHashids.encode(props.item.postId);

    return (
        <LazyLoad
            height={200}
            offset={100}
        >
            <div
                onClick={() => navigate(`${RouteConfig.BLOG}/post/${hash}`, {
                    state: {
                        name: props.item.name,
                    }
                })}
                className={styles.Blog_Item}
            >
                <div className={"w-full aspect-w-3 aspect-h-2"}>
                    <div className={"absolute inset-0"}>
                        <CustomImage
                            src={props.item.image}
                            alt={props.item.name}
                            objectFit={"fill"}
                        />
                    </div>
                </div>
                <Divider className={"divider-main divider-border-6-amber divider-horizontal-m-0"}/>
                <div className={styles.Blog_Item_Content}>
                    <div className={styles.Blog_Item_Row_Time}>
                        <NextImage
                            className={"atl-icon atl-icon-color-009B90"}
                            width={24}
                            height={24}
                            src={Images.iconArrowRight.data}
                            alt={Images.iconArrowRight.atl}
                        />
                        <CustomTypography textStyle={"text-14-20"}>
                            {props.item.createdAtFormatted(t('format.date'))}
                        </CustomTypography>
                    </div>
                    <div className={"flex flex-col gap-2"}>
                        {
                            props.item.name && <CustomTypography
                                textStyle={"text-16-24"}
                                className={"text-justify"}
                                isStrong
                            >
                                {props.item.name}
                            </CustomTypography>
                        }
                        {
                            props.item.description && <div
                                className={"text-justify"}
                                dangerouslySetInnerHTML={{__html: `${props.item.description}`}}
                            />
                        }
                    </div>
                    <div className={styles.Blog_Item_Row_Category}>
                        <div className={"w-6"}>
                            <Divider className={"divider-main divider-horizontal-m-0 divider-border-1-4F4F4F"}/>
                        </div>
                        <CustomTypography
                            textStyle={"text-10-16"}
                            isStrong
                        >
                            Nhật ký công trình xây dựng
                        </CustomTypography>
                    </div>
                </div>
            </div>
        </LazyLoad>
    )
}

export const BlogSkeleton = () => {
    return <div className={`${styles.Blog_Item} animate-pulse`}>
        <div className={"w-full aspect-w-3 aspect-h-2 bg-gray-300"}/>
        <Divider className={"divider-main divider-border-6-amber divider-horizontal-m-0"}/>
        <div className={styles.Blog_Item_Content}>
            <div className={styles.Blog_Item_Row_Time}>
                <NextImage
                    className={"atl-icon atl-icon-color-009B90"}
                    width={24}
                    height={24}
                    src={Images.iconArrowRight.data}
                    alt={Images.iconArrowRight.atl}
                />
                <div className={"w-10 h-4 bg-gray-300"}/>
            </div>
            <div className={"flex flex-col gap-2"}>
                <div className={"w-full h-10 bg-gray-300"}/>
                <div className={"w-full h-16 bg-gray-300"}/>
            </div>
            <div className={styles.Blog_Item_Row_Category}>
                <div className={"w-6"}>
                    <Divider className={"divider-main divider-horizontal-m-0 divider-border-1-4F4F4F"}/>
                </div>
                <div className={"w-8 h-1 bg-gray-300"}/>
            </div>
        </div>
    </div>
}
