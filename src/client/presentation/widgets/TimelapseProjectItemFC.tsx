import {Avatar, Carousel, Divider, Image as AntImage, Modal} from "antd";
import {useState} from "react";
import NoImage from "../../assets/image/no_image.png";
import {TimelapseProjectModel} from "../../models/service/timelapse/TimelapseProjectModel";
import {useTranslation} from "react-i18next";
import LazyLoad from "react-lazyload";
import {RouteConfig} from "../../config/RouteConfig";
import {CHashids} from "../../core/CHashids";
import {CustomImage} from "../components/CustomImage";
import {TParamMachine, TTimelapseMachineParamState} from "../../const/Types";
import Image from "next/image";
import {TimelapseMachineModel} from "../../models/service/timelapse/TimelapseMachineModel";
import {useNavigate} from "react-router";
import styles from "../../styles/module/TimelapseProjectItem.module.scss";
import {Images} from "../../const/Images";
import MapPinLineIcon from "../components/icons/MapPinLineIcon";
import CalendarIcon from "../components/icons/CalendarIcon";
import {CustomTypography} from "../components/CustomTypography";
import {E_ResUrlType} from "../../const/Events";

export const TimelapseProjectItemFC = (props: {
    index?: number,
    item: TimelapseProjectModel
}) => {
    const navigate = useNavigate();
    const {t} = useTranslation();
    const hash = CHashids.encode(props.item.projectId);
    const [isModalVisible, setIsModalVisible] = useState(false);

    const onClickAvatar = () => {
        setIsModalVisible(true);

    }
    const handleClose = () => {
        setIsModalVisible(false);
    }

    const paramState: TTimelapseMachineParamState = {
        name: props.item.name ?? '',
        isSensor: props.item.isSensor ?? false,
        machines: props.item.machine?.map((value): TParamMachine => {
            return {
                machineId: value.machineId ?? 0,
                name: value.name ?? '',
                active: value.active,
                total: value.total
            }
        }) ?? []
    }

    let filterMachine: TimelapseMachineModel[] = [];

    if (props.item.machine && props.item.machine.length > 0) {
        props.item.machine.forEach(value => {
            if (value.image) {
                filterMachine.push(value);
            }
        })

        if (filterMachine.length > 4) {
            filterMachine = filterMachine.slice(0, 4);
        }
    }

    const _buildBg = (item: TimelapseMachineModel, index: number) => {
        return (
            <div className={"relative"} key={index}>
                <div className={"w-full aspect-w-3 aspect-h-2"}>
                    <div className={"absolute inset-0"}>
                        <CustomImage
                            key={index}
                            src={
                                item.image?.getShotImageUrl({
                                    type: E_ResUrlType.Thumb,
                                    size: 426
                                }) ?? NoImage.src
                            }
                            alt={item.name}
                            objectFit={"fill"}
                        />
                    </div>
                </div>
                {
                    item.sensor && item.sensor.length > 0
                        ? <div className={styles.InfoSensor}>
                            {
                                item.sensor.map((item, index) => {
                                    let icon;

                                    if (item.sensor === 'air_temp') {
                                        icon = <Image
                                            height={16}
                                            width={16}
                                            src={Images.iconThermometer.data}
                                            alt={Images.iconThermometer.atl}
                                        />;
                                    } else if (item.sensor === 'air_humid') {
                                        icon = <Image
                                            height={16}
                                            width={16}
                                            src={Images.iconSunDim.data}
                                            alt={Images.iconSunDim.atl}
                                        />;
                                    } else {
                                        return null;
                                    }

                                    return (
                                        <div key={index}>
                                            {icon}
                                            <CustomTypography
                                                style={{
                                                    color: "#FFFFFF",
                                                }}
                                                textStyle={"text-info-height-0"}
                                                isStrong
                                            >
                                                {item.value}&nbsp;{item.unit2}
                                            </CustomTypography>
                                        </div>
                                    )
                                })
                            }
                        </div>
                        : null
                }
            </div>
        )
    }

    const _buildCarousel = () => {
        if (filterMachine.length > 1) {
            const _items: TimelapseMachineModel[] = [];

            for (let el of filterMachine) {
                if (el.image) {
                    _items.push(el);
                }
            }

            if (_items.length === 0) {
                return (
                    <div className={"w-full aspect-w-3 aspect-h-2"}>
                        <div className={"absolute inset-0"}>
                            <CustomImage
                                src={NoImage.src}
                                alt={props.item.name}
                                objectFit={"fill"}
                            />
                        </div>
                    </div>
                )
            }

            let _wg: JSX.Element[] = [];

            for (let i = 0; i < (_items.length > 4 ? 4 : _items.length); i++) {
                _wg.push(_buildBg(_items[i], i));
            }

            return _wg.length > 1 ?
                <Carousel
                    className={styles.Carousel}
                    autoplay={true}
                    dots={false}
                >
                    {
                        _wg.map(e => e)
                    }
                </Carousel> : _wg[0]

        } else {
            return (
                <div className={"w-full aspect-w-3 aspect-h-2"}>
                    <div className={"absolute inset-0"}>
                        <CustomImage
                            src={filterMachine[0]?.image?.getShotImageUrl({
                                type: E_ResUrlType.Thumb,
                                size: 426
                            }) ?? NoImage.src}
                            alt={filterMachine[0]?.name ?? props.item.name}
                            objectFit={"fill"}
                        />
                    </div>
                </div>
            )
        }
    }

    return (
        <>
            <LazyLoad
                height={200}
                offset={100}
            >
                <div
                    onClick={() => navigate(`${RouteConfig.TIMELAPSE}/machine/${hash}`, {
                        state: paramState,
                    })}
                    className={styles.Item}
                >
                    {
                        _buildCarousel()
                    }
                    <div className={styles.Divider}/>
                    <div className={styles.BoxInfo}>
                        <div className={styles.BoxInfoMeta}>
                            <div
                                style={{
                                    zIndex: 1,
                                }}
                                onClick={onClickAvatar}
                                className={styles.InfoMeta}
                            >
                                <Avatar
                                    size={40}
                                    src={props.item?.user?.image}
                                />
                                <CustomTypography
                                    textStyle={"text-body-text-3"}
                                    isStrong
                                >
                                    {props.item.name?.toUpperCase()}
                                </CustomTypography>
                            </div>
                            {
                                props.item.address && <div className={styles.InfoMeta}>
                                    <MapPinLineIcon/>
                                    <CustomTypography textStyle={"text-14-20"}>
                                        {props.item.address}
                                    </CustomTypography>
                                </div>
                            }
                            <div className={styles.InfoMeta}>
                                <CalendarIcon/>
                                <CustomTypography textStyle={"text-14-20"}>
                                    {props.item.createdAtFormatted(t('format.date'))}
                                </CustomTypography>
                            </div>
                        </div>
                        <Divider
                            style={{
                                margin: 0,
                            }}
                        />
                        <div className={styles.BoxStatusAndCount}>
                            <div>
                                <div className={`w-2 h-2 rounded-full ${props.item.active ? "bg-state-success" : "bg-dark3"}`}/>
                                <CustomTypography textStyle={"text-body-text-2"}>
                                    {
                                        props.item.active ? t('text.active') : t('text.inActive')
                                    }
                                </CustomTypography>
                            </div>
                            <CustomTypography
                                textStyle={"text-body-text-2"}>{`${props.item.total} ${t('text.machine')}`}</CustomTypography>
                        </div>
                    </div>
                </div>
            </LazyLoad>
            {
                props.item?.user && isModalVisible
                    ? <Modal
                        key={props.index}
                        visible={true}
                        onCancel={handleClose}
                        cancelText={t('button.close')}
                        footer={null}
                        bodyStyle={{
                            padding: "0",
                            paddingBottom: "1rem",
                        }}
                    >
                        <AntImage
                            className={"w-full"}
                            src={props.item.user.background}
                            alt={props.item.user.name}
                        />
                        <div
                            className={'text-center'}
                            style={{
                                marginTop: "-3.5rem"
                            }}
                        >
                            <Avatar
                                size={{xs: 24, sm: 32, md: 40, lg: 64, xl: 80, xxl: 100}}
                                src={props.item.user.image}
                                alt={props.item.user.name}
                                style={{
                                    backgroundColor: "#ccc"
                                }}
                            />
                            <div className={"mt-3"}>
                                {props.item.user.name ?? ''}
                            </div>
                        </div>
                    </Modal>
                    : null
            }
        </>
    )
}

export const TimelapseProjectSkeleton = () => {
    return (
        <div className="w-full flex items-center flex-col bg-white">
            <div className="w-full flex flex-col bg-white shadow-md rounded-md items-center">
                <div className="w-full flex justify-start items-center p-4">
                    <div
                        className="animate-pulse mr-2 h-10 w-10 rounded-full overflow-hidden relative bg-gray-200"/>
                    <div className="animate-pulse mb-2 h-5 w-40 overflow-hidden relative bg-gray-200"/>
                </div>
                <div className="animate-pulse h-52 w-full overflow-hidden relative bg-gray-200"/>
                <div className="w-full flex flex-col p-4">
                    <div className="flex">
                        <div
                            className="animate-pulse flex h-5 w-5 overflow-hidden relative bg-gray-200 mr-1"/>
                        <div className="animate-pulse flex h-5 w-48 overflow-hidden relative bg-gray-200"/>
                    </div>
                    <div className="flex mt-1">
                        <div
                            className="animate-pulse flex h-5 w-5 overflow-hidden relative bg-gray-200 mr-1"/>
                        <div className="animate-pulse flex h-5 w-48 overflow-hidden relative bg-gray-200"/>
                    </div>
                </div>
                <div className="w-full h-px overflow-hidden relative bg-gray-200"/>
                <div className="flex justify-between items-center p-4 w-full">
                    <div className="animate-pulse h-4 w-20 overflow-hidden relative bg-gray-200"/>
                    <div className="animate-pulse h-4 w-16 overflow-hidden relative bg-gray-200"/>
                </div>
            </div>
        </div>
    )
}
