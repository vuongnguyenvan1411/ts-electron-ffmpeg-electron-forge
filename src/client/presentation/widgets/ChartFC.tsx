import React, {useEffect, useRef} from "react";
import {BarController, BarElement, CategoryScale, Chart, ChartDataset, ChartOptions, ChartType, DefaultDataPoint, Legend, LinearScale, LineController, LineElement, PointElement, Tooltip,} from "chart.js";
import styles from "../../styles/module/Chart.module.scss";

Chart.register(
    CategoryScale,
    LinearScale,
    LineController,
    BarController,
    BarElement,
    PointElement,
    LineElement,
    Tooltip,
    Legend,
);

let myLineChart: any;

export const LineGraph = React.memo((props: {
    labels: any,
    datasets: ChartDataset<ChartType, DefaultDataPoint<ChartType>>[],
    options?: ChartOptions,
}) => {
    const chartRef = useRef<HTMLCanvasElement>(null);

    useEffect(() => {
        if (chartRef.current !== null) {
            const myChartRef = chartRef.current!.getContext("2d");

            if (typeof myLineChart !== "undefined") myLineChart.destroy();

            myLineChart = new Chart(myChartRef!, {
                type: 'line',
                data: {
                    // Bring in data
                    labels: props.labels,
                    datasets: props.datasets,
                },
                options: props.options,
            });
        }

        return () => {
            if (typeof myLineChart !== "undefined") myLineChart.destroy();
        }
    })

    return (
        <div
            className={styles.GraphContainer}
            style={{
                position: "relative",
                height: "40vh",
                width: "100%"
            }}
        >
            <canvas
                id={"line-chart"}
                ref={chartRef}
            />
        </div>
    );
})
