import {BlogPostModel} from "../../models/blog/BlogPostModel";
import {Card} from "antd";
import {Link} from "react-router-dom";
import {RouteConfig} from "../../config/RouteConfig";
import {CustomImage} from "../components/CustomImage";
import LazyLoad from "react-lazyload";
import {CHashids} from "../../core/CHashids";

export const BlogPostRelatedFC = (props: { item: BlogPostModel }) => {
    const hash = CHashids.encode(props.item.postId);

    return (
        <LazyLoad>
            <Card
                className={'mt-3'}
                size={"small"}
                hoverable={true}
                bordered={false}
                style={{width: 300, height: 300}}
                cover={
                    <>
                        <Link
                            to={`${RouteConfig.BLOG}/post/${hash}`}
                            state={{
                                name: props.item.name
                            }}
                        >
                            <CustomImage
                                src={props.item.image}
                                alt={props.item.name!}
                                aspectRatio="3/2"
                            />
                        </Link>
                    </>
                }
            >
                <div className={"clear-both"}>
                    {
                        props.item.name
                            ? <Link
                                to={`${RouteConfig.BLOG}/post/${hash}`}
                                state={{
                                    name: props.item.name
                                }}
                            >
                                <div className={'text-green-500 font-medium'}>
                                    {props.item.name}
                                </div>
                            </Link>
                            : null
                    }
                </div>
            </Card>
        </LazyLoad>
    )
}
