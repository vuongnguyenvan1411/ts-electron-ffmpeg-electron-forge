import {useTranslation} from "react-i18next";
import moment from "moment";
import {findKey, range} from "lodash";
import {useEffect, useState} from "react";
import {DatePicker, Select, Typography} from "antd";
import {CustomButton} from "../components/CustomButton";
import styles from "../../styles/module/Filter.module.scss";
import {CustomTypography} from "../components/CustomTypography";
import {CustomTimePicker} from "../components/CustomTimePicker";

interface ILstDate {
    name: string,
    value: string | string[] | null,
}

interface _IRangeDates {
    label: string,
    value?: moment.Moment,
    onChange: (date: moment.Moment | null, dateString: string) => void,
    disableDate?: (date: moment.Moment) => boolean,
    disabledTime?: () => any,
}

type _TFilterState = {
    period: string;
    dateStart: string;
    dateEnd: string;
    timeStart: string;
    timeEnd: string;
}

const _initState: _TFilterState = {
    period: '',
    dateEnd: '',
    dateStart: '',
    timeStart: '',
    timeEnd: '',
}

export const TimelapseViewVideoFilterFilterFC = (props: {
    onClick: Function,
    current?: {
        period: string | undefined,
        dateStart: string | undefined,
        dateEnd: string | undefined,
        timeStart: string | undefined,
        timeEnd: string | undefined,
    }
    allTime?: [string | undefined, string | undefined]
}) => {
    const {t} = useTranslation();
    const formatDate = t('format.date');
    const formatTime = "HH:mm";
    const _today = Date.now();
    const _tomorrow = new Date(_today);
    _tomorrow.setDate(_tomorrow.getDate() - 1);
    const _lastWeek = new Date(_today);
    _lastWeek.setDate(_lastWeek.getDate() - 7);
    const _lastHalfMonth = new Date(_today);
    _lastHalfMonth.setDate(_lastHalfMonth.getDate() - 15);
    const _lastMonth = new Date(_today);
    _lastMonth.setDate(_lastMonth.getDate() - 30);
    const lstSelectDate: ILstDate[] = [
        {
            name: t("text.today"),
            value: [moment(Date.now()).format(formatDate).toString(), moment(Date.now()).format(formatDate).toString()]
        },
        {
            name: t("text.yesterday"),
            value: [moment(_tomorrow).format(formatDate).toString(), moment(_tomorrow).format(formatDate).toString()]
        },
        {
            name: t("text.lastWeek"),
            value: [moment(_lastWeek).format(formatDate).toString(), moment(_today).format(formatDate).toString()]
        },
        {
            name: t("text.lastHalfMount"),
            value: [moment(_lastHalfMonth).format(formatDate).toString(), moment(_today).format(formatDate).toString()]
        },
        {
            name: t("text.mount"),
            value: [moment(_lastMonth).format(formatDate).toString(), moment(_today).format(formatDate).toString()]
        },
        {
            name: t("text.allTime"),
            value: [
                props.allTime && props.allTime[0] ? props.allTime[0]?.split(' ')[0] : moment(Date.now()).format(formatDate).toString(),
                props.allTime && props.allTime[1] ? props.allTime[1]?.split(' ')[0] : moment(Date.now()).format(formatDate).toString()
            ]
        },
        {
            name: t("text.custom"),
            value: null
        },
    ];
    const lstSelectPeriod: ILstDate[] = [
        {
            name: t("text.morning"),
            value: 'm'
        },
        {
            name: t("text.night"),
            value: 'n'
        },
        {
            name: t("text.lightAndDark"),
            value: 'ld'
        },
        {
            name: t("text.custom"),
            value: 'h'
        },
    ];

    const [filter, setFilter] = useState<_TFilterState>(_initState);
    const [selectDateValue, setSelectDateValue] = useState<number | undefined>(undefined);
    const [selectPeriodValue, setSelectPeriodValue] = useState<number | undefined>(undefined);
    const [isEditPeriod, setIsEditPeriod] = useState<boolean>(false);

    const onChangeDatePicker = (date: moment.Moment | null, dateString: string, type: "end" | "start") => {
        if (type === "end") {
            setFilter({
                ...filter,
                dateEnd: date !== null ? date.format(formatDate) : '',
            })
        } else {
            setFilter({
                ...filter,
                dateStart: date !== null ? date.format(formatDate) : '',
            })
        }

        setSelectDateValue(lstSelectDate.length - 1);
    }

    useEffect(() => {
        const currentKeyDate = findKey(lstSelectDate, ['value', [props.current?.dateStart, props.current?.dateEnd]]);
        const currentKeyPeriod = findKey(lstSelectPeriod, ['value', props.current?.period]);

        setSelectDateValue(currentKeyDate ? parseInt(currentKeyDate) : undefined);
        setSelectPeriodValue(currentKeyPeriod ? parseInt(currentKeyPeriod) : undefined)
        setIsEditPeriod(props.current?.period === 'h')
        setFilter({
            period: props.current?.period ?? 'm',
            dateStart: props.current?.dateStart ?? '',
            dateEnd: props.current?.dateEnd ?? '',
            timeStart: props.current?.timeStart ?? '',
            timeEnd: props.current?.timeEnd ?? '',
        })
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.current])

    const onChangeTimePicker = (time: moment.Moment | null, timeString: string, type: "end" | "start") => {
        if (type === "end") {
            setFilter({
                ...filter,
                timeEnd: time !== null ? time.format(formatTime) : '',
            })
        } else {
            setFilter({
                ...filter,
                timeStart: time !== null ? time.format(formatTime) : '',
            })
        }
    }

    const handleChange = (value: number) => {
        setSelectDateValue(value);

        const select = lstSelectDate[value].value;

        if (select !== null) {
            setFilter({
                ...filter,
                dateStart: select[0],
                dateEnd: select[1],
            })
        }
    }

    function handleChangeSelectPeriod(value: number) {
        setSelectPeriodValue(value);

        const select = lstSelectPeriod[value].value;

        if (select !== null && typeof select === "string") {
            // keyPeriod.current = select;
            filter.period = select;
        }

        if (select === 'h') {
            setIsEditPeriod(true);
        } else {
            setIsEditPeriod(false);
            filter.dateStart = '';
            filter.dateEnd = '';
        }

        setFilter({...filter});
    }

    const onClickFilter = () => {
        if (filter.period !== 'h') {
            filter.timeStart = '';
            filter.timeEnd = '';
        }

        props.onClick({
            period: filter.period,
            dateStart: filter.dateStart,
            dateEnd: filter.dateEnd,
            timeStart: filter.timeStart,
            timeEnd: filter.timeEnd,
        });


        setFilter({...filter})
    };

    const onReset = () => {
        setSelectDateValue(undefined);
        setSelectPeriodValue(undefined);
        setIsEditPeriod(false);

        props.onClick({
            period: _initState.period,
            dateStart: _initState.dateStart,
            dateEnd: _initState.dateEnd,
            timeStart: _initState.timeStart,
            timeEnd: _initState.timeEnd,
        });

        setFilter(_initState);
    }

    const pickerDateRanges: any = {};
    pickerDateRanges[t('text.today')] = [moment(), moment()];
    pickerDateRanges[t('text.thisMonth')] = [moment().startOf('month'), moment().endOf('month')];
    pickerDateRanges[t('text.thisYear')] = [moment().startOf('year'), moment()];

    const _buildFastFilter = () => {
        return <div className={styles.Filter_Row}>
            <div className={styles.Filter_Div}>
                <CustomTypography
                    textStyle={"text-14-20"}
                    isStrong
                >
                    {t("text.fastFilterByDay")}
                </CustomTypography>
            </div>
            <div className={styles.Filter_Div}>
                <Select
                    className={"select-form-main select-form-bg-white select-form-rounded select-form-border-969696"}
                    key={'select-time'}
                    style={{width: '100%'}}
                    placeholder={t("text.selectTimePeriod")}
                    value={selectDateValue}
                    onChange={handleChange}
                >
                    {
                        lstSelectDate.map((value, index) =>
                            <Select.Option key={index} value={index}>
                                <Typography.Text strong>{value.name}</Typography.Text>
                            </Select.Option>
                        )
                    }
                </Select>
            </div>
        </div>
        // eslint-disable-next-line react-hooks/exhaustive-deps
    };

    const _buildRangeDate = () => {
        const _items: _IRangeDates[] = [
            {
                label: t("text.dateStart"),
                value: filter.dateStart.length > 0 ? moment(filter.dateStart, formatDate) : undefined,
                onChange: (date, dateString) => {
                    onChangeDatePicker(date, dateString, "start")
                },
                disableDate: (date) => {
                    return date.endOf('day') > moment(filter.dateEnd, formatDate).endOf('day');
                }
            },
            {
                label: t("text.dateEnd"),
                value: filter.dateEnd.length > 0 ? moment(filter.dateEnd, formatDate) : undefined,
                onChange: (date, dateString) => {
                    onChangeDatePicker(date, dateString, "end")
                },
                disableDate: (date) => {
                    return date.endOf('day') < moment(filter.dateStart, formatDate).endOf('day');
                }
            },
        ]

        return <div className={styles.Filter_Row}>
            {
                _items.map((value, index) => {
                    return <div key={index} className={styles.Filter_Div}>
                        <div className={styles.Filter_Div_Col}>
                   <span>
                       <CustomTypography
                           textStyle={"text-14-20"}
                           isStrong
                       >
                           {value.label}
                       </CustomTypography>&nbsp;
                       <CustomTypography
                           textStyle={"text-12-18"}
                           isStrong={false}
                       >
                         ({t("text.option")})
                   </CustomTypography>
                   </span>
                            <DatePicker
                                className={"date-picker-main date-picker-radius date-picker-p-10-16 date-picker-border-969696"}
                                format={t("format.date")}
                                dropdownClassName={"date-picker-dropdown-main date-picker-dropdown-no-rounded"}
                                onChange={value.onChange}
                                value={value.value}
                                defaultValue={value.value}
                                disabledDate={value.disableDate}
                                placement={"bottomRight"}
                                showNow={false}
                                showToday={false}
                            />
                        </div>
                    </div>;
                })
            }
        </div>
    }

    const _buildFastFilterTime = () => {
        return <div className={styles.Filter_Row}>
            <div className={styles.Filter_Div}>
                <CustomTypography
                    textStyle={"text-14-20"}
                    isStrong
                >
                    {t("text.timeFrame")}
                </CustomTypography>
            </div>
            <div className={styles.Filter_Div}>
                <Select
                    className={"select-form-main select-form-bg-white select-form-rounded select-form-border-969696"}
                    key={'select-time'}
                    style={{width: '100%'}}
                    placeholder={t("text.selectTimePeriod")}
                    value={selectPeriodValue}
                    onChange={handleChangeSelectPeriod}
                >
                    {
                        lstSelectPeriod.map((value, index) =>
                            <Select.Option key={index} value={index}>
                                <Typography.Text strong>{value.name}</Typography.Text>
                            </Select.Option>
                        )
                    }
                </Select>
            </div>
        </div>
    }

    const _buildRangeTime = () => {
        const _items: _IRangeDates[] = [
            {
                label: t("text.timeStart"),
                value: filter.timeStart.length > 0 ? moment(filter.timeStart, formatTime) : undefined,
                onChange: (date, dateString) => {
                    onChangeTimePicker(date, dateString, "start")
                },
                disabledTime: moment(filter.dateStart, formatDate).endOf('day').isSame(moment(filter.dateEnd, formatDate).endOf('day')) ?
                    () => ({
                        disabledHours: () => filter.timeEnd.length > 0 ? range(0, 24).splice(moment(filter.timeEnd, formatTime).hours() + 1, (24 - moment(filter.timeEnd, formatTime).hours())) : [],
                        disabledMinutes: () => filter.timeStart.length > 0 && moment(filter.timeStart, formatTime).hours() === moment(filter.timeEnd, formatTime).hours() ? range(moment(filter.timeEnd, formatTime).minutes(), 60) : [],
                    }) : undefined
            },
            {
                label: t("text.timeEnd"),
                value: filter.timeEnd.length > 0 ? moment(filter.timeEnd, formatTime) : undefined,
                onChange: (date, dateString) => {
                    onChangeTimePicker(date, dateString, "end")
                },
                disabledTime: moment(filter.dateStart, formatDate).endOf('day').isSame(moment(filter.dateEnd, formatDate).endOf('day')) ?
                    () => ({
                        disabledHours: () => filter.timeStart.length > 0 ? range(0, moment(filter.timeStart, formatTime).hours()) : [],
                        disabledMinutes: () => filter.timeStart.length > 0 && moment(filter.timeStart, formatTime).hours() === moment(filter.timeEnd, formatTime).hours() ? range(0, moment(filter.timeStart, formatTime).minutes()) : [],
                    }) : undefined
            },
        ]

        return <div className={styles.Filter_Row}>
            {
                _items.map((value, index) => {
                    return <div key={`range-time ${index}`} className={styles.Filter_Div}>
                        <div className={styles.Filter_Div_Col}>
                   <span>
                       <CustomTypography
                           textStyle={"text-14-20"}
                           isStrong
                       >
                           {value.label}
                       </CustomTypography>&nbsp;
                       <CustomTypography
                           textStyle={"text-12-18"}
                           isStrong={false}
                       >
                        ({t("text.option")})
                   </CustomTypography>
                   </span>
                            <CustomTimePicker
                                format={formatTime}
                                onChange={value.onChange}
                                value={value.value}
                                defaultValue={value.value}
                                disabledTime={value.disabledTime}
                            />
                        </div>
                    </div>;
                })
            }
        </div>
    }

    return (
        <div className={styles.Filter_Container}>
            {
                _buildFastFilter()
            }
            {
                _buildRangeDate()
            }
            {
                _buildFastFilterTime()
            }
            {
                isEditPeriod && _buildRangeTime()
            }
            <div className={"flex gap-2 justify-end"}>
                <CustomButton
                    type={"outline"}
                    onClick={() => onReset()}
                >
                    {t("button.setAgain")}
                </CustomButton>
                <CustomButton
                    onClick={() => onClickFilter()}
                >
                    {t("button.filter")}
                </CustomButton>
            </div>
        </div>
    );
}
