import {useTranslation} from "react-i18next";
import {Empty, Spin, Tag} from "antd";
import {LoadingOutlined} from "@ant-design/icons";
import {indexOf} from "lodash";
import {CustomTypography} from "../components/CustomTypography";

export const CommonEmptyFC = () => {
    const {t} = useTranslation();

    return (
        <div className={"my-10"}>
            <Empty description={t('text.noData')}/>
        </div>
    );
}

interface iCTF {
    key: string,
    label: string,
    check?: any[],
}

export const CommonTagFilterFC = (props: {
    is: boolean,
    onClose: Function,
    data: any;
    items: iCTF[],
    className?: string,
}) => {
    return (
        props.is && props.items.length > 0
            ? <div className={`flex flex-wrap gap-2 pb-4 ${props.className}`}>
                {
                    props.items.map((element, index) => {
                        const key = element.key.split('.');

                        let isTag;
                        let value;

                        if (key.length > 1) {
                            isTag = props.data.hasOwnProperty(key[0])
                                && props.data[key[0]].hasOwnProperty(key[1])
                                && props.data[key[0]][key[1]].length > 0;

                            if (isTag) {
                                if (element.check !== undefined && element.check.length > 1) {
                                    if (typeof element.check[0] === "object") {
                                        value = element.check[1][indexOf(element.check[0], props.data[key[0]][key[1]])];
                                    } else {
                                        value = props.data[key[0]][key[1]] === element.check[0] ? element.check[1] : element.check[2];
                                    }
                                } else {
                                    value = props.data[key[0]][key[1]];
                                }
                            }
                        } else {
                            isTag = typeof props.data[element.key] !== "undefined"
                                && props.data[element.key].length > 0;

                            if (isTag) {
                                if (element.check !== undefined && element.check.length > 1) {
                                    if (typeof element.check[0] === "object") {
                                        value = element.check[1][indexOf(element.check[0], props.data[element.key])!];
                                    } else {
                                        value = props.data[element.key] === element.check[0] ? element.check[1] : element.check[2];
                                    }
                                } else {
                                    value = props.data[element.key];
                                }
                            }
                        }

                        return (
                            isTag && value !== undefined
                                ? <Tag
                                    className={"tag-main"}
                                    key={index}
                                    closable
                                    onClose={_ => props.onClose(element.key)}
                                >
                                    <CustomTypography
                                        isStrong
                                        textStyle={"text-body-text-3"}
                                    >
                                        {element.label}
                                    </CustomTypography>
                                    :   &nbsp;
                                    <CustomTypography
                                        textStyle={"text-body-text-3"}
                                    >
                                        {value}
                                    </CustomTypography>
                                </Tag>
                                : null
                        )
                    })
                }
            </div>
            : null
    )
}

export const CommonLoadingSpinFC = () => {
    return (
        <div className={"container mx-auto text-center mt-8"}>
            <Spin
                size="large"
                indicator={<LoadingOutlined style={{fontSize: "2.5rem"}} spin/>}
            />
        </div>
    );
}
