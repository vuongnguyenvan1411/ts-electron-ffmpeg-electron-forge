import {App} from "../../const/App";
import {CustomTypography} from "../components/CustomTypography";

export const ContributorFC = (props: {
    usingFor?: 'v2d' | 'v3d' | 'vr360'
}) => {
    return (
        <div
            style={{
                position: "absolute",
                bottom: 0,
                right: 0,
                zIndex: 999,
                background: "rgba(255, 255, 255, .9)",
                padding: "0 0.6rem"
            }}
        >

            <CustomTypography
                textStyle={"text-v2d-highlight-semibold"}
                isStrong
            >
                <a
                    href={App.Domain}
                    target={"_blank"}
                    rel={"noreferrer"}
                    style={{
                        color: props.usingFor === 'v2d' ? "#DE9F59" : "#0d6efd"
                    }}
                >
                    &copy;&nbsp;{App.Company}&nbsp;Contributors
                </a>
            </CustomTypography>

        </div>
    )
}
