import {UserModel} from "../../models/UserModel";
import {Avatar, Button, Card, Divider, Modal, Typography} from "antd";
import styles from "../../styles/module/MeFC.module.scss"
import {CustomImage} from "../components/CustomImage";
import {CameraOutlined} from "@ant-design/icons";
import {useEffect, useState} from "react";
import {MeImageScreen} from "../screens/account/MeImageScreen";
import {useTranslation} from "react-i18next";
import {SendingStatus} from "../../const/Events";
import {MeImageAction} from "../../recoil/account/me_image/MeImageAction";

export const MeFC = (props: { item: UserModel }) => {
    const {t} = useTranslation()

    const {vm} = MeImageAction()

    const [isModalVisible, setIsModalVisible] = useState(false)
    const [type, setType] = useState('')

    useEffect(() => {
        if (vm.isUpdating === SendingStatus.success) {
            setIsModalVisible(false);
        }
    }, [vm.isUpdating])

    const showModal = (type: string) => {
        setType(type)
        setIsModalVisible(true)
    }

    const handleCancel = () => {
        setIsModalVisible(false)
    }

    return (
        <div className={`me flex  justify-center`}>
            <div className={"flex flex-col w-full pb-5"} style={
                {
                    backgroundColor: "#f0f3f5"
                }
            }>
                <div className={"flex w-full flex-grow justify-center"}>
                    <div className={`flex w-full flex-col`}>
                        <div className={`flex flex-col relative justify-items-center flex-grow w-full`}>
                            <div className={"flex w-full bg-white justify-center"}>
                                <div className={`flex flex-col relative ${styles.WidgetCard} flex-grow`}>
                                    <div className={`${styles.WidgetUserBackground} relative background mb-4`}>
                                        <CustomImage
                                            className={"rounded-b-lg hover:rounded-b-lg "}
                                            src={props.item.background}
                                            alt={""}
                                            aspectRatio="2.7/1"
                                            preview={true}
                                        />
                                        <div className={"absolute bottom-2 right-2  rounded-full"}>
                                            <Button
                                                className={`${styles.WidgetColorSecondary} rounded-md`}
                                                size={"large"}
                                                onClick={() => showModal('background')}
                                                icon={
                                                    <CameraOutlined
                                                        style={{fontSize: '20px',}}
                                                    />
                                                }
                                            >
                                                <Typography.Text strong={true}>
                                                    {t("button.addBackground")}
                                                </Typography.Text>
                                            </Button>
                                        </div>
                                    </div>
                                    <div
                                        className={`${styles.WidgetUserAvatar} rounded-full bg-white absolute inset-x-0 bottom-0 left-1/2 bg-center z-10 w-min`}>
                                        <div className={"relative"}>
                                            <div className={`bg-cover p-0.5`}>
                                                <Avatar
                                                    style={
                                                        {
                                                            backgroundColor: "white",
                                                            display: "inherit"
                                                        }
                                                    }
                                                    size={168}
                                                    src={
                                                        <CustomImage src={props.item.image} alt={""} preview={true}/>
                                                    }
                                                />
                                            </div>
                                            <div className={"absolute bottom-2 right-2  rounded-full"}>
                                                <Button
                                                    className={`${styles.WidgetColorSecondary}`}
                                                    size={"large"}
                                                    shape="circle"
                                                    onClick={() => showModal('avatar')}
                                                    icon={
                                                        <CameraOutlined style={{fontSize: '20px',}}/>
                                                    }
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div
                                className={`flex w-full justify-center bg-white`}
                            >
                                <div className={`${styles.WidgetCard}`}>
                                    <div className={`${styles.WidgetUserInfoName} info-name flex flex-col place-items-center`}>
                                        <Typography.Title level={2}>
                                            {props.item.name}
                                        </Typography.Title>
                                        {/*<Typography.Text strong>{props.item.username}</Typography.Text>*/}
                                    </div>
                                </div>
                            </div>
                            <div className={"flex w-full justify-center bg-white"}>
                                <div className={`flex flex-col  ${styles.WidgetCard} flex-grow`}>
                                    <Divider
                                        style={{
                                            color: "red",
                                            margin: "0px"
                                        }}
                                    />
                                </div>
                            </div>
                            <div className={"w-full flex flex-grow justify-center pt-2"}>
                                <Card className={`${styles.WidgetCard} flex-grow`}>
                                    <BuildRowInfo label={t("text.account")} text={props.item.name}/>
                                    <BuildRowInfo label={t("text.telephone")} text={props.item.telephone}/>
                                    <BuildRowInfo label={t("text.email")} text={props.item.email}/>
                                    <BuildRowInfo label={t("text.address")} text={props.item.address}/>
                                </Card>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <Modal
                closable={false}
                title={type === "avatar" ? t("title.editAvatar") : t("title.editBackground")}
                visible={isModalVisible}
                footer={
                    [
                        <Button key="back" onClick={handleCancel}>
                            {t("button.close")}
                        </Button>
                    ]
                }
            >
                <MeImageScreen item={props.item} type={type}/>
            </Modal>
        </div>
    )
}

const BuildRowInfo = (props: { label: string, text: string }) => {
    return (
        <div className={"flex"}>
            <Typography.Text>{props.label + ":"}</Typography.Text>
            <Typography.Text strong>&nbsp;{props.text}</Typography.Text>
        </div>
    )
}
