import {MenuFoldOutlined} from "@ant-design/icons";
import {CustomTypography} from "../components/CustomTypography";
import React, {useEffect} from "react";
import {THeaderCtx} from "../../const/Types";
import {useMaster} from "../hooks/useMaster";

type _TWrapContent = {
    masterHeader: THeaderCtx;
    bodyHeader: {
        title?: string,
        wrapClassName?: string,
        customChild?: React.ReactNode,
        right?: React.ReactNode,
    } | null;
    children: React.ReactNode;
    className?: string;
}

export const WrapContentWidget: React.FC<_TWrapContent> = React.memo((props) => {
    const {master} = useMaster();

    useEffect(() => {
        if (master.header?.setHeader) {
            master.header.setHeader(props.masterHeader);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.masterHeader.title, props.masterHeader.isLoading, props.masterHeader.onReload])

    return (
        <div className={"flex-grow"}>
            {
                props.bodyHeader
                    ? <div className={`relative bg-white h-auto w-full flex flex-wrap py-2 items-center justify-between px-4 shadow-custom md:px-6 md:h-14 md:flex-nowrap md:flex-row md:py-0  xxl:px-10 ${props.bodyHeader.wrapClassName ? props.bodyHeader.wrapClassName : ""}`}>
                        <div className={"flex items-center gap-4 w-full"}>
                            {
                                !master.isMobile
                                    ? <MenuFoldOutlined
                                        width={32}
                                        height={32}
                                        onClick={() => master.onOpenDrawer?.call(this)}/>
                                    : null
                            }
                            {
                                props.bodyHeader.customChild && props.bodyHeader.customChild
                            }
                            {
                                props.bodyHeader.customChild !== undefined ? null : typeof props.bodyHeader.title === "string"
                                    ? <CustomTypography
                                        textStyle={"text-title-3"}
                                        isStrong
                                    >
                                        {props.bodyHeader.title}
                                    </CustomTypography>
                                    : props.bodyHeader.title
                            }
                        </div>
                        {
                            !props.bodyHeader.customChild && props.bodyHeader.right && props.bodyHeader.right
                        }
                    </div>
                    : null
            }
            <div className={props.className ?? "p-4 md:p-6 xxl:px-10 xxl:py-6 3xl:container 3xl:mx-auto"}>
                {props.children}
            </div>
        </div>
    )
})
