import {useState} from "react";
import {Vr360Model} from "../../models/Vr360Model";
import {Avatar, Divider, Image as AntImage, Modal} from "antd";
import {useTranslation} from "react-i18next";
import LazyLoad from "react-lazyload";
import {RouteConfig} from "../../config/RouteConfig";
import {CHashids} from "../../core/CHashids";
import {CustomImage} from "../components/CustomImage";
import NextImage from "next/image";
import styles from "../../styles/module/Vr360.module.scss";
import {CustomTypography} from "../components/CustomTypography";
import {useNavigate} from "react-router";
import {Images} from "../../const/Images";

export const Vr360ItemFC = (props: {
    index?: number,
    item: Vr360Model
}) => {
    const {t} = useTranslation();
    const navigate = useNavigate();
    const [isModalVisible, setIsModalVisible] = useState(false);

    const onClickAvatar = () => {
        setIsModalVisible(true);

    }
    const handleClose = () => {
        setIsModalVisible(false);
    }

    const hash = CHashids.encode(props.item.vr360Id);

    const _params = {
        name: props.item.name,
        link: props.item.link,
        createdAt: props.item.createdAtUnix(),
    }

    return (
        <>
            <LazyLoad
                height={200}
                offset={100}
            >
                <div
                    onClick={(event) => {
                        event.preventDefault();

                        navigate(`${RouteConfig.VR360}/view/${hash}`, {
                            state: _params,
                        })
                    }}
                    className={styles.Item}
                >
                    <div
                        className={"w-full aspect-w-3 aspect-h-2"}
                    >
                        <div className={"absolute inset-0"}>
                            <CustomImage
                                src={props.item.image}
                                alt={props.item.name}
                                objectFit={"fill"}
                            />
                        </div>
                    </div>
                    <Divider className={"divider-main divider-border-4-amber divider-horizontal-m-0"}/>
                    <div className={styles.BoxInfo}>
                        <div className={styles.BoxInfoMeta}>
                            <div className={styles.InfoMeta}>
                                <div
                                    onClick={(event) => {
                                        event.stopPropagation();

                                        onClickAvatar();
                                    }}
                                >
                                    <Avatar
                                        size={40}
                                        src={props.item?.user?.image}
                                    />
                                </div>
                                <CustomTypography
                                    textStyle={"text-16-24"}
                                    isStrong
                                >
                                    {props.item.name?.toUpperCase()}
                                </CustomTypography>
                            </div>
                            <div className={"flex-col flex gap-2 w-full"}>
                                {
                                    props.item.address && <div className={styles.InfoMeta}>
                                        <NextImage
                                            className={"atl-icon atl-icon-color-geodetic"}
                                            width={20}
                                            height={20}
                                            src={Images.iconMapPinLine.data}
                                            alt={Images.iconMapPinLine.atl}
                                        />
                                        <CustomTypography textStyle={"text-14-18"}>
                                            {props.item.address}
                                        </CustomTypography>
                                    </div>
                                }
                                <div className={styles.InfoMeta}>
                                    <NextImage
                                        className={"atl-icon atl-icon-color-geodetic"}
                                        width={20}
                                        height={20}
                                        src={Images.iconCalendar.data}
                                        alt={Images.iconCalendar.atl}
                                    />
                                    <CustomTypography textStyle={"text-14-18"}>
                                        {props.item.createdAtFormatted(t('format.date'))}
                                    </CustomTypography>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={styles.BoxActions}>
                        <div
                            onClick={(event) => {
                                event.stopPropagation();

                                navigate(`${RouteConfig.VR360}/view/${hash}`, {
                                    state: _params,
                                })
                            }}
                        >
                            <NextImage
                                className={"atl-icon atl-icon-color-hex-969696"}
                                width={20}
                                height={20}
                                src={Images.iconEye.data}
                                alt={Images.iconEye.atl}
                            />
                            <CustomTypography
                                color={"#969696"}
                                isStrong
                                textStyle={"text-14-18"}
                            >
                                {t("button.view")}
                            </CustomTypography>
                        </div>
                        <div
                            onClick={(event) => {
                                event.stopPropagation();

                                navigate(`${RouteConfig.VR360}/setting/${hash}`, {
                                    state: _params,
                                })
                            }}
                        >
                            <NextImage
                                className={"atl-icon atl-icon-color-hex-969696"}
                                width={20}
                                height={20}
                                src={Images.iconGearOutlined.data}
                                alt={Images.iconGearOutlined.atl}
                            />
                            <CustomTypography
                                color={"#969696"}
                                isStrong
                                textStyle={"text-14-18"}
                            >
                                {t("button.setting")}
                            </CustomTypography>
                        </div>
                    </div>
                </div>
            </LazyLoad>
            {
                props.item?.user && isModalVisible
                    ? <Modal
                        key={props.index}
                        visible={true}
                        onCancel={handleClose}
                        cancelText={t('button.close')}
                        footer={null}
                        bodyStyle={{
                            padding: "0",
                            paddingBottom: "1rem",
                        }}
                    >
                        <AntImage
                            className={"w-full"}
                            src={props.item.user.background}
                            alt={props.item.user.name}
                        />
                        <div
                            className={'text-center'}
                            style={{
                                marginTop: "-3.5rem"
                            }}
                        >
                            <Avatar
                                size={{xs: 24, sm: 32, md: 40, lg: 64, xl: 80, xxl: 100}}
                                src={props.item.user.image}
                                alt={props.item.user.name}
                                style={{
                                    backgroundColor: "#ccc"
                                }}
                            />
                            <div className={"mt-3"}>
                                {props.item.user.name ?? ''}
                            </div>
                        </div>
                    </Modal>
                    : null
            }
        </>
    )
}

export const Vr360Skeleton = () => {
    const {t} = useTranslation();
    return (
        <div className={styles.Item}>
            <div className={"w-full aspect-w-3 aspect-h-2 bg-gray-300"}/>
            <Divider className={"divider-main divider-border-4-amber divider-horizontal-m-0"}/>
            <div className={styles.BoxInfo}>
                <div className={styles.BoxInfoMeta}>
                    <div className={styles.InfoMeta}>
                        <div className={"w-10 h-10 bg-gray-300"}/>
                        <div className={"w-full h-5 bg-gray-300"}/>
                    </div>
                    <div className={"flex-col flex gap-2 w-full"}>
                        <div className={styles.InfoMeta}>
                            <div className={"w-5 h-5 bg-gray-300"}/>
                            <div className={"w-full h-4 bg-gray-300"}/>
                        </div>
                        <div className={styles.InfoMeta}>
                            <div className={"w-5 h-5 bg-gray-300"}/>
                            <div className={"w-10 h-4 bg-gray-300"}/>
                        </div>
                    </div>
                </div>
            </div>
            <div className={styles.BoxActions}>
                <div>
                    <NextImage
                        className={"atl-icon atl-icon-color-hex-969696"}
                        width={20}
                        height={20}
                        src={Images.iconEye.data}
                        alt={Images.iconEye.atl}
                    />
                    <CustomTypography
                        color={"#969696"}
                        isStrong
                        textStyle={"text-14-18"}
                    >
                        {t("button.view")}
                    </CustomTypography>
                </div>
                <div>
                    <NextImage
                        className={"atl-icon atl-icon-color-hex-969696"}
                        width={20}
                        height={20}
                        src={Images.iconGear.data}
                        alt={Images.iconGear.atl}
                    />
                    <CustomTypography
                        color={"#969696"}
                        isStrong
                        textStyle={"text-14-18"}
                    >
                        {t("button.setting")}
                    </CustomTypography>
                </div>
            </div>
        </div>
    )
}
