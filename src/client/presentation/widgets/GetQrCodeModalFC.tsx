import {Divider, Modal, Result, Spin} from "antd";
import {useTranslation} from "react-i18next";
import {AxiosClient} from "../../repositories/AxiosClient";
import {ApiService} from "../../repositories/ApiService";
import {useEffect, useState} from "react";
import {Color} from "../../const/Color";
import {SendingStatus} from "../../const/Events";
import {FrownOutlined} from "@ant-design/icons";
import download from "downloadjs";
import moment from "moment";
import {Utils} from "../../core/Utils";
import {AxiosError} from "axios";
import {CustomButton} from "../components/CustomButton";
import NextImage from "next/image";
import {Images} from "../../const/Images";

interface IO {
    base64: string,
    url: string
}

export const GetQrCodeModalFC = (props: {
    sid: string,
    name: string,
    onClose: Function
}) => {
    const {t} = useTranslation();

    const initialState = {
        isLoading: SendingStatus.loading,
        item: {
            base64: '',
            url: '',
        }
    }

    const [isLoading, setIsLoading] = useState(initialState.isLoading);
    const [item, setItem] = useState<IO>(initialState.item);

    useEffect(() => {
        console.log('%cMount FC: GetQrCodeModalFC', Color.ConsoleInfo);

        AxiosClient
            .get(ApiService.getQrCode(props.sid))
            .then(r => {
                if (r.success && r.data) {
                    setIsLoading(SendingStatus.success);
                    setItem(r.data as IO);
                } else {
                    setIsLoading(SendingStatus.error);
                }
            })
            .catch((e: AxiosError) => {
                if (typeof e.response === 'object') {
                    setIsLoading(SendingStatus.serverError);
                } else {
                    setIsLoading(SendingStatus.disConnect);
                }
            });

        return () => {
            setIsLoading(initialState.isLoading);
            setItem(initialState.item);

            console.log('%cUnmount FC: GetQrCodeModalFC', Color.ConsoleInfo);
        };

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const [isModalVisible, setIsModalVisible] = useState(true);

    const handleClose = () => {
        setIsModalVisible(false);
        props.onClose();
    };

    const onClickDownload = () => {
        let name = 'QrCode_';
        if (props.name.length > 0) {
            name += `${Utils.strSlug(props.name)}_`
        }
        name += moment(Date.now()).format('DD-MM-YYYY-HH-mm-ss');
        name += '.png';

        download(`data:image/png;base64, ${item.base64}`, name, 'image/png')
    }

    return (
        <Modal
            title={"QrCode"}
            visible={isModalVisible}
            onCancel={handleClose}
            cancelText={t('button.close')}
            footer={null}
            width={400}
        >
            {
                isLoading === SendingStatus.loading
                    ? <Spin spinning={true} size={"large"}>
                        <div className={"text-center"}>loading...</div>
                    </Spin>
                    : isLoading === SendingStatus.success
                        ? <div className={"text-center"}>
                            {/* eslint-disable-next-line @next/next/no-img-element */}
                            <img className={"w-full"} src={`data:image/png;base64, ${item.base64}`} alt={"QrCode"}/>
                            <Divider/>
                            <CustomButton
                                onClick={onClickDownload}
                                icon={
                                    <NextImage
                                        src={Images.iconUploadSimple.data}
                                        alt={Images.iconUploadSimple.atl}
                                    />
                                }
                            >
                                {t('button.download')}
                            </CustomButton>
                        </div>
                        : isLoading === SendingStatus.error || isLoading === SendingStatus.disConnect || isLoading === SendingStatus.serverError
                            ? <Result
                                status={"error"}
                                icon={<FrownOutlined/>}
                                title={t('error.getData')}
                            />
                            : null
            }
        </Modal>
    );
}
