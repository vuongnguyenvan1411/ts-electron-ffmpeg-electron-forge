import {TimelapseImageModel} from "../../models/service/timelapse/TimelapseImageModel";
import {Divider, List} from "antd";
import {useRef} from "react";
import {Style} from "../../const/Style";
import {CustomImage} from "../components/CustomImage";
import {MediaQuery} from "../../core/MediaQuery";
import {ListGridType} from "antd/lib/list";
import useIntersectionObserver from "../hooks/useIntersectionOnserver";
import styles from "../../styles/module/TimelapseViewImage.module.scss";
import {TimelapseDownloadImageFC} from "./TimelapseDownloadImageFC";
import {E_ResUrlType} from "../../const/Events";

export const TimelapseViewImageFC = (props: {
    item: TimelapseImageModel,
    index?: number,
    children?: JSX.Element,
    placeHolder?: JSX.Element,
    isDivider?: boolean,
    isDownload?: boolean
}) => {
    const ref = useRef<HTMLDivElement | null>(null);
    const entry = useIntersectionObserver(ref, {freezeOnceVisible: true, threshold: 0.1,});
    const isVisible = !!entry?.isIntersecting

    return (
        <div className={"relative rounded"} ref={ref}>
            {
                <div className={styles.Item}>
                    <div className={"w-full aspect-w-3 aspect-h-2"}>
                        <div className={"absolute inset-0 rounded-t"}>
                            <CustomImage
                                lazy={!isVisible}
                                className={"rounded-t"}
                                src={props.item.getShotImageUrl({
                                    type: E_ResUrlType.Thumb,
                                    size: 426
                                })}
                                alt={props.item.name}
                                objectFit={"fill"}
                            />
                        </div>
                    </div>
                    {props.isDivider &&
                        <Divider className={"divider-main divider-horizontal-m-0 divider-border-6-amber"}/>}
                    {props.children}
                </div>
            }
            {
                props.isDownload && <div className={"absolute top-2 left-2"}>
                    <TimelapseDownloadImageFC
                        wrapClass={"btn-main-auto-icon"}
                        buttonStyle={{
                            background: "#172723",
                            opacity: 0.5,
                        }}
                        viewBoxIcon={16}
                        item={props.item}
                        isLabel={false}
                    />
                </div>
            }
        </div>
    )
}

export const TimelapseViewImageSkeleton = () => {
    return (
        <div className={`${styles.Item} bg-white`}>
            <div className={"w-full flex flex-col animate-pulse"}>
                <div className="w-full aspect-w-3 aspect-h-2 bg-gray-300"/>
                <div className={styles.Divider}/>
                <div className={"flex justify-between p-2"}>
                    <div className={"bg-gray-300 h-4 w-1/12"}/>
                    <div className={"bg-gray-300 h-4 w-2/6"}/>
                </div>
                <div className={"flex justify-between px-2 mb-1"}>
                    <div className={"bg-gray-300 h-4 w-2/6"}/>
                    <div className={"bg-gray-300 h-4 w-1/12"}/>
                </div>
                <div className={"flex justify-between px-2 mb-2"}>
                    <div className={"bg-gray-300 h-4 w-2/6"}/>
                    <div className={"bg-gray-300 h-4 w-1/12"}/>
                </div>
            </div>
        </div>
    );
}


export const TimelapseViewImageLoadingFC = (props: {
    limit: number,
    grid?: ListGridType,
    isOne?: boolean,
}) => {
    const items = new Array(props.isOne ? 1 : (new MediaQuery(Style.GridLoadingAnimation)).getPoint(props.limit)).fill('OK');

    return (
        <List
            grid={props.grid ?? Style.GridTypeMain}
            dataSource={items}
            renderItem={i => (
                <List.Item key={i}>
                    <div className="w-full flex items-center flex-col bg-white">
                        <div className="w-full flex flex-col bg-white shadow-md rounded-md items-center">
                            <div className="w-full animate-pulse">
                                <div style={{aspectRatio: '3/2'}} className="bg-gray-300"/>
                                <div className={"flex justify-between p-2"}>
                                    <div className={"bg-gray-300 h-4 w-20"}/>
                                    <div className={"bg-gray-300 h-4 w-32"}/>
                                </div>
                                <div className={"flex justify-between px-2 mb-1"}>
                                    <div className={"bg-gray-300 h-4 w-36"}/>
                                    <div className={"bg-gray-300 h-4 w-12"}/>
                                </div>
                                <Divider className={"my-2"}/>
                                <div className={"flex justify-between px-2 mb-2"}>
                                    <div className={"bg-gray-300 h-4 w-36"}/>
                                    <div className={"bg-gray-300 h-4 w-12"}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </List.Item>
            )}
        />
    );
}
