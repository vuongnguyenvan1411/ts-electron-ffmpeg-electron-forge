import {useTranslation} from "react-i18next";
import React, {CSSProperties, useState} from "react";
import {Dropdown, Menu, notification, Progress, Spin} from "antd";
import axios from "axios";
import download from "downloadjs";
import {CustomButton} from "../components/CustomButton";
import Image from "next/image";
import {Images} from "../../const/Images";
import {TimelapseImageModel} from "../../models/service/timelapse/TimelapseImageModel";
import {LoadingOutlined} from "@ant-design/icons";
import {E_ResUrlType} from "../../const/Events";

type _TTimelapseDownloadBtn = {
    item?: TimelapseImageModel
    children?: JSX.Element
    buttonStyle?: CSSProperties
    viewBoxIcon?: number
    wrapClassProgress?: string
    wrapClass?: string
    isLabel?: boolean
}

export const TimelapseDownloadImageFC: React.FC<_TTimelapseDownloadBtn> = (props) => {
    const {t} = useTranslation()
    const [isDownload, setIsDownload] = useState(false);
    const [percentComplete, setPercentComplete] = useState<number | undefined>(undefined)
    const antIcon = <LoadingOutlined style={{fontSize: 24}} spin/>;

    const menuDownload = () => (
        <Menu
            items={[
                {
                    key: "1",
                    label: t("text.dlOrig"),
                    onClick: () => {
                        setIsDownload(true)
                        downloadImage(props.item?.getShotImageUrl({
                            type: E_ResUrlType.Download
                        }))
                    }
                },
                {
                    key: "2",
                    label: t("text.dlAttach"),
                    onClick: () => {
                        setIsDownload(true)
                        downloadImage(props.item?.getShotImageUrl({
                            type: E_ResUrlType.Download,
                            attach: true
                        }))
                    }
                }

            ]}
        />
    )

    const downloadImage = (url?: string) => {
        if (url) {
            axios
                .get(url, {
                    responseType: "blob",
                    onDownloadProgress: (evt: ProgressEvent) => {
                        if (evt.lengthComputable) {
                            setPercentComplete((evt.loaded / evt.total) * 100);
                        }
                    }
                })
                .then(r => {
                    const data = r.data as Blob
                    const filename = `${props.item?.order}_${props.item?.dateShotFormatted("DD-MM-YYYY-HH-mm-ss")}.jpg`

                    download(data, filename, data.type);

                    setPercentComplete(undefined);
                    setIsDownload(false)

                    notification.success({
                        message: t('message.downloadImageSuccess'),
                    })
                })
                .catch(_ => {
                    setPercentComplete(undefined);
                    setIsDownload(false)

                    notification.error({
                        message: t('message.downloadImageFailure'),
                    })
                })
        }
    }

    return (
        percentComplete !== undefined || isDownload
            ? <div className={props.wrapClassProgress ? props.wrapClassProgress : "bg-gray-400 p-1"}>
                {
                    percentComplete !== undefined
                        ? <Progress
                            type="circle"
                            strokeColor={{
                                '0%': '#108ee9',
                                '100%': '#87d068',
                            }}
                            width={28}
                            percent={percentComplete}
                            strokeWidth={18}
                            showInfo={false}
                        />
                        : <Spin indicator={antIcon}/>
                }
            </div>
            : props.item?.sensor && props.item.sensor.length > 0
                ? <Dropdown
                    overlay={menuDownload()}
                    trigger={['click']}
                    arrow
                    placement="bottomRight"
                >
                    <CustomButton
                        className={props.wrapClass}
                        style={props.buttonStyle}
                        size={"large"}
                        icon={
                            <Image
                                width={props.viewBoxIcon}
                                height={props.viewBoxIcon}
                                src={Images.iconUploadSimple.data}
                                alt={Images.iconUploadSimple.atl}
                            />
                        }
                    >
                        {props.isLabel ? t("button.download") : undefined}
                    </CustomButton>
                </Dropdown>
                : <CustomButton
                    className={props.wrapClass}
                    style={props.buttonStyle}
                    size={"large"}
                    icon={
                        <Image
                            width={props.viewBoxIcon}
                            height={props.viewBoxIcon}
                            src={Images.iconUploadSimple.data}
                            alt={Images.iconUploadSimple.atl}
                        />
                    }
                    onClick={_ => downloadImage(props.item?.getShotImageUrl({
                        type: E_ResUrlType.Download
                    }))}
                >
                    {props.isLabel ? t("button.download") : undefined}
                </CustomButton>
    )
}
