import React, {useRef} from "react";
import PlyrFC, {APITypes, PlyrFCEvent} from "../PlyrFC";
import Plyr from "plyr";

interface HlsEvents {
    onInit?: (ref: APITypes | null) => void;
    onPlay?: (event: PlyrFCEvent) => void;
    onTimeUpdate?: (event: PlyrFCEvent) => void;
}

export interface PlyrNormalProps extends HlsEvents {
    source?: Plyr.SourceInfo;
    elementClassName?: string;
}

const PlyrNormalFC: React.FC<PlyrNormalProps> = ({
                                                     source,
                                                     elementClassName,
                                                     // onPlay,
                                                     // onTimeUpdate,
                                                 }: PlyrNormalProps) => {
    const ref = useRef<APITypes>(null);

    return (
        <>
            <PlyrFC
                key={`plyr-video-auto ${elementClassName ? elementClassName : ''}`}
                preload={"none"}
                playsInline={true}
                id={"plyr"}
                ref={ref}
                options={{volume: 0.1}}
                source={source}
                autoPlay={true}
                width={'full'}
                height={'full'}
            />
        </>
    )
}

export default PlyrNormalFC;
