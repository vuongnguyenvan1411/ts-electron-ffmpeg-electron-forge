import {FC, useCallback, useEffect, useMemo, useRef} from "react";
import PlyrFC, {APITypes, PlyrFCCallback, PlyrFCEvent, PlyrFCInstance, PlyrFCProps} from "../PlyrFC";
import Hls from "hls.js";
import {App} from "../../../const/App";
import {EDFile} from "../../../core/encrypt/EDFile";

interface IHlsEvents {
    onInit?: (ref: APITypes | null) => void;
    onPlay?: (event: PlyrFCEvent) => void;
    onTimeUpdate?: (event: PlyrFCEvent) => void;
}

export interface IHlsProps extends IHlsEvents {
    source: string;
    elementClassName?: string;
    mEncrypt: any;
    nEncrypt: any;
}

const HlsVideoFC: FC<IHlsProps> = ({
                                       source,
                                       elementClassName,
                                       mEncrypt,
                                       nEncrypt,
                                       // onInit,
                                       onPlay,
                                       onTimeUpdate,
                                   }: IHlsProps) => {
    const ref = useRef<APITypes>(null)

    const registerEvents = useCallback(() => {
        const onPlayFor: PlyrFCCallback = event => {
            onPlay!(event)
        }

        const onTimeUpdateIn: PlyrFCCallback = event => {
            onTimeUpdate!(event)
        }

        if (onPlay && ref && ref.current) {
            (ref.current.plyr as PlyrFCInstance).once('play', onPlayFor)
        }

        if (onTimeUpdate && ref && ref.current) {
            (ref.current.plyr as PlyrFCInstance).on('timeupdate', onTimeUpdateIn)
        }

        if (ref && ref.current) {
            (ref.current.plyr as PlyrFCInstance).on('ready', event => console.log('ready', event))
        }

    }, [onPlay, onTimeUpdate])

    useEffect(() => {
        const video = document.getElementById("plyr") as HTMLVideoElement;

        const hls = new Hls({
            fetchSetup: (context, initParams) => {
                return new Request(context.url, initParams);
            },
            xhrSetup: (xhr, url) => {
                const path = url.split('/')
                const ep = EDFile.setLinkUrl({
                    m: mEncrypt,
                    n: nEncrypt,
                    f: path[path.length - 1]
                    // e: moment(new Date()).add(1, 'hours').unix()
                })

                xhr.open('GET', `${App.UrlCdnGs}/sv/${ep}`, true)
            }
        })

        hls.loadSource(source)
        hls.attachMedia(video)

        // @ts-ignore
        ref.current!.plyr.media = video

        hls.on(Hls.Events.MANIFEST_LOADED, _ => {
            // video.play();
            (ref.current!.plyr as PlyrFCInstance).play()
            registerEvents()
        })

        return () => hls.destroy()

    }, [mEncrypt, nEncrypt, registerEvents, source])

    return useMemo(() => {
        return (
            <PlyrFC
                key={`plyr-video-auto ${elementClassName ? elementClassName : ''}`}
                preload={"none"}
                playsInline={true}
                id={"plyr"}
                ref={ref}
                options={{volume: 0.1}}
                source={{} as PlyrFCProps["source"]}
            />
        )

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
}

export default HlsVideoFC;
