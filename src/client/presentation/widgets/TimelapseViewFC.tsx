import {CHashids} from "../../core/CHashids";
import {Menu} from "antd";
import {Link} from "react-router-dom";
import {RouteConfig} from "../../config/RouteConfig";
import {InfoCircleOutlined, SettingOutlined} from "@ant-design/icons";
import {useTranslation} from "react-i18next";
import {TTimelapseMachineParamState} from "../../const/Types";
import {useState} from "react";

export const TimelapseViewSiderFC = (props: {
    params: TTimelapseMachineParamState,
    hash: string
}) => {
    const {t} = useTranslation()

    const state = {
        name: props.params.name,
        isSensor: props.params.isSensor,
        machines: props.params.machines,
    }

    const hashDecode = CHashids.decode(props.hash)
    const [projectId] = hashDecode

    const [menuActive, setMenuActive] = useState<string>(`machine-${props.hash}`);

    const onClickMenu = (e: any) => {
        setMenuActive(e.key);
    }

    return (
        <Menu
            theme="light"
            // defaultSelectedKeys={[menuActive]}
            defaultOpenKeys={['service']}
            selectedKeys={[menuActive]}
            mode="inline"
            onClick={onClickMenu}
        >
            {
                props.params.machines && props.params.machines.length > 1
                    ? props.params.machines.map((item, index) => {
                        const hashPM = CHashids.encode([projectId, item.machineId]);

                        return (
                            <>
                                <Menu.Item
                                    key={`machine-${hashPM}`}
                                    style={{
                                        whiteSpace: 'normal',
                                        height: 'auto',
                                        lineHeight: "1.15rem",
                                        padding: "0.3rem 0.6rem",
                                        textAlign: "justify",
                                        margin: "0",
                                    }}
                                    icon={
                                        <span
                                            className={"m-0 text-white px-1 py-0 rounded"}
                                            style={{
                                                fontSize: "0.7rem",
                                                backgroundColor: item.active ? "#149388" : "#72808e"
                                            }}
                                        >
                                            {index + 1}
                                        </span>
                                    }
                                >
                                    <Link
                                        to={`${RouteConfig.TIMELAPSE}/view/${hashPM}/image`}
                                        state={state}
                                        replace={true}
                                    >
                                        {item.name}
                                    </Link>
                                </Menu.Item>
                                <Menu.Divider key={`div-${index}`}/>
                            </>
                        )
                    })
                    : null
            }
            <Menu.Item key="info" icon={<InfoCircleOutlined/>}>
                <Link
                    to={`${RouteConfig.TIMELAPSE}/info/${CHashids.encode(projectId)}`}
                    state={state}
                    replace={true}
                >
                    {t('text.info')}
                </Link>
            </Menu.Item>
            <Menu.Item key="setting" icon={<SettingOutlined/>}>
                <Link
                    to={`${RouteConfig.TIMELAPSE}/setting/${CHashids.encode(projectId)}`}
                    state={state}
                    replace={true}
                >
                    {t('text.setting')}
                </Link>
            </Menu.Item>
        </Menu>
    );
}
