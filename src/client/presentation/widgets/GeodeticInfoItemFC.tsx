import LazyLoad from "react-lazyload";
import {RouteConfig} from "../../config/RouteConfig";
import {CHashids} from "../../core/CHashids";
import {CustomImage} from "../components/CustomImage";
import {TParamPartGeodetic} from "../../const/Types";
import {useNavigate} from "react-router";
import styles from "../../styles/module/Geodetic.module.scss";
import {CustomTypography} from "../components/CustomTypography";
import {Button, Divider, Space} from "antd";
import {useSessionContext} from "../contexts/SessionContext";
import {TM3dPC, TM3dTM} from "../../models/service/geodetic/SpaceModel";
import {useInjection} from "inversify-react";
import {StoreConfig} from "../../config/StoreConfig";

export const GeodeticInfoItemFC = (props: {
    spaceId: number,
    id: number,
    src: string,
    link: string,
    type: string
    name?: string,
    parts?: TParamPartGeodetic,
}) => {
    const navigate = useNavigate();
    const hash = CHashids.connection("main").encode([props.spaceId, props.id])
    let routeTo: { pathname: any; state: any; } | undefined;

    switch (props.type) {
        case 'm2d':
            routeTo = {
                pathname: `${RouteConfig.GEODETIC}/m2d/${hash}`,
                state: {
                    spaceId: props.spaceId,
                    m2dId: props.id,
                    name: props.name,
                    parts: props.parts
                }
            };

            break;
        case 'm3d':
            routeTo = {
                pathname: `${RouteConfig.GEODETIC}/pc3d/${hash}`,
                state: {
                    spaceId: props.spaceId,
                    m3dId: props.id,
                    name: props.name,
                    parts: props.parts
                }
            };

            break;
        case 'vr360':
            routeTo = {
                pathname: `${RouteConfig.GEODETIC}/vr360/${hash}`,
                state: {
                    spaceId: props.spaceId,
                    vr360Id: props.id,
                    name: props.name,
                    parts: props.parts
                }
            };

            break;
    }

    return (
        routeTo
            ? <LazyLoad
                height={200}
                offset={100}
            >
                <div
                    onClick={(event) => {
                        event.preventDefault();

                        navigate(`${routeTo?.pathname}`, {
                            state: routeTo?.state,
                        })
                    }}
                    className={styles.Item}
                >
                    <div
                        className={"w-full aspect-w-3 aspect-h-2"}
                    >
                        <div className={"absolute inset-0"}>
                            <CustomImage
                                src={props.src}
                                alt={props.name ?? ''}
                                objectFit={"fill"}
                            />
                        </div>
                    </div>
                    <div className={styles.BoxInfo}>
                        <div className={styles.BoxInfoMeta}>
                            <div className={styles.InfoMeta}>
                                <CustomTypography
                                    textStyle={"text-16-24"}
                                    isStrong
                                >
                                    {props.name?.toUpperCase()}
                                </CustomTypography>
                            </div>
                        </div>
                    </div>
                </div>
            </LazyLoad>
            : null
    )
}

export const Geodetic3DItemFC = (props: {
    spaceId: number,
    id: number,
    src: string,
    link: string,
    type: string
    name?: string,
    data?: {
        pcs?: TM3dPC[]
        tms?: TM3dTM[]
    },
    parts?: TParamPartGeodetic,
}) => {
    const navigate = useNavigate()
    const [session] = useSessionContext()
    const storeConfig = useInjection(StoreConfig)

    const hash = CHashids.connection("main").encode([props.spaceId, props.id])

    const routeToPC = {
        pathname: `${RouteConfig.GEODETIC}/pc3d/${hash}`,
        state: {
            spaceId: props.spaceId,
            m3dId: props.id,
            name: props.name,
            parts: props.parts
        }
    }

    const routeToTM = {
        pathname: `${RouteConfig.GEODETIC}/tm3d/${hash}`,
        state: {
            spaceId: props.spaceId,
            m3dId: props.id,
            name: props.name,
            parts: props.parts
        }
    }

    const isTest = session.user
        && (
            storeConfig.checkSlpkTest(session.user.username)
        )
        && (
            (props.data?.pcs && props.data.pcs.length > 0)
            || (props.data?.tms && props.data.tms.length > 0)
        )

    return (
        <LazyLoad
            height={200}
            offset={100}
        >
            <div
                className={isTest ? `${styles.Item} ${styles.NoHover}` : styles.Item}
                onClick={(event) => {
                    event.preventDefault();

                    if (!isTest) {
                        navigate(`${routeToPC.pathname}`, {
                            state: routeToPC.state,
                        })
                    }
                }}
            >
                <div
                    className={"w-full aspect-w-3 aspect-h-2"}
                >
                    <div
                        className={"absolute inset-0"}
                    >
                        <CustomImage
                            src={props.src}
                            alt={props.name ?? ''}
                            objectFit={"fill"}
                        />
                    </div>
                </div>
                <div className={styles.BoxInfo}>
                    <div className={styles.BoxInfoMeta}>
                        <div className={styles.InfoMeta}>
                            <CustomTypography
                                textStyle={"text-16-24"}
                                isStrong
                            >
                                {props.name?.toUpperCase()}
                            </CustomTypography>
                        </div>
                    </div>
                </div>
                {/** TODO: coming soon */}
                {
                    isTest && (
                        <>
                            <Divider>Hình thức</Divider>
                            <div className={"flex justify-center mb-3"}>
                                <Space>
                                    {
                                        (props.data?.pcs && props.data.pcs.length > 0) && (
                                            <Button
                                                type="primary"
                                                onClick={(event) => {
                                                    event.preventDefault();

                                                    navigate(`${routeToPC?.pathname}`, {
                                                        state: routeToPC?.state,
                                                    })
                                                }}
                                            >
                                                Point Cloud
                                            </Button>
                                        )
                                    }
                                    {
                                        (props.data?.tms && props.data.tms.length > 0) && (
                                            <Button
                                                type="primary"
                                                onClick={(event) => {
                                                    event.preventDefault();

                                                    navigate(`${routeToTM?.pathname}`, {
                                                        state: routeToTM?.state,
                                                    })
                                                }}
                                            >
                                                Textured Mesh
                                            </Button>
                                        )
                                    }
                                </Space>
                            </div>
                        </>
                    )
                }
            </div>
        </LazyLoad>
    )
}

export const GeodeticInfoSkeleton = () => {
    return (
        <div className={styles.Item}>
            <div className={"w-full aspect-w-3 aspect-h-2 bg-gray-300"}/>
            <div className={styles.BoxInfo}>
                <div className={styles.BoxInfoMeta}>
                    <div className={styles.InfoMeta}>
                        <div className={"w-full h-5 bg-gray-300"}/>
                    </div>
                </div>
            </div>
        </div>
    )
}
