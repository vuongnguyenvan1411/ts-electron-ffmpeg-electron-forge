import React, {useCallback, useEffect, useMemo, useRef, useState} from "react";
import {TimelapseVideoModel} from "../../models/service/timelapse/TimelapseVideoModel";
import styles from "../../styles/module/TimelapseViewVideoAuto.module.scss"
import {Color} from "../../const/Color";
import {App} from "../../const/App";
import {Table} from "antd";
import {useTranslation} from "react-i18next";
import {APITypes, PlyrFCInstance} from "./PlyrFC";
import HlsVideoFC from "./video/HlsVideoFC";
import Plyr from "plyr";

interface IRect {
    x: number;
    y: number;
    width: number;
    height: number;
}

interface IBox extends IRect {
    rect: IRect[];
    month: string;
}

export const TimelapseViewVideoAutoFC = React.memo((props: {
        item: TimelapseVideoModel,
        tab: string,
    }) => {
        const {t} = useTranslation();
        const dateStart = props.item.info?.dateStartMoment();
        const dateEnd = props.item.info?.dateEndMoment();
        const videoRef = React.useRef(null);
        const [percentTime, setPercentTime] = useState<number>(0);
        const ref = useRef<APITypes | null>(null);
        const [plyrInstance, setPlyrInstance] = useState<APITypes | null>(null);


        useEffect(() => {
            if (props.tab !== "video_auto" && ref && ref.current) {
                (ref.current.plyr as PlyrFCInstance).pause();
            }


        }, [props.tab, ref])

        useEffect(() => {
            console.log('ref-vodoi', plyrInstance)

            if (props.tab !== "video_auto" && plyrInstance) {
                (plyrInstance.plyr as PlyrFCInstance).pause();
            }

        }, [plyrInstance, props.tab])

        const onPlay = (_: Plyr.PlyrEvent) => {
            setPlyrInstance(_.detail);
        }

        const onTimeUpdate = (_: Plyr.PlyrEvent) => {
            let durationTime;
            durationTime = _.detail.plyr.duration;

            if (durationTime > (App.VideoIntroSecond + App.VideoOutroSecond + 10)) {
                durationTime -= (App.VideoIntroSecond + App.VideoOutroSecond);
            }

            let cur = 0;

            if (durationTime !== 0) {
                cur = _.detail.plyr.currentTime;
                console.log()

                if (cur > (App.VideoIntroSecond + App.VideoOutroSecond + 10)) {
                    cur -= App.VideoIntroSecond;
                }

                if (cur > 0 && cur < durationTime) {
                    setPercentTime((cur / durationTime) * 100)
                }
            }

        }

        const InfoHtml = useCallback(() => {
            const items: any[] = [];

            items.push({
                key: 1,
                name: t('text.dateRender'),
                value: props.item.info?.dateRenderFormatted(t('format.date'))
            });

            const totalMonth = props.item.info?.dateEndMoment().diff(props.item.info?.dateStartMoment(), 'months', true);
            const totalDay = props.item.info?.dateEndMoment().diff(props.item.info?.dateStartMoment(), 'days', true);

            items.push({
                key: 2,
                name: t('text.date'),
                value: `${props.item.info?.dateStartFormatted("DD/MM/YYYY")} -> ${props.item.info?.dateEndFormatted("DD/MM/YYYY")} (${Math.round(totalMonth ?? 0)} ${t('text.month').toLocaleLowerCase()}) - (${totalDay} ${t('text.day').toLocaleLowerCase()})`
            });

            items.push({
                key: 3,
                name: t('text.timePeriod'),
                value: `${props.item.info?.timeStartFormatted("HH:mm")} - ${props.item.info?.timeEndFormatted("HH:mm")}`
            });

            items.push({
                key: 4,
                name: t('text.time'),
                value: `${Math.round(props.item.info?.duration ?? 0)}s`
            });

            items.push({
                key: 5,
                name: t('text.resolution'),
                value: props.item.info?.resolution ? props.item.info?.resolution : (`${props.item.info?.width}x${props.item.info?.height}`)
            });

            items.push({
                key: 6,
                name: t('text.bitRate'),
                value: props.item.info?.bitRate?.toLocaleString()
            });

            items.push({
                key: 7,
                name: t('text.fps'),
                value: props.item.info?.fps
            });

            items.push({
                key: 8,
                name: t('text.frameRate'),
                value: props.item.info?.frameRate
            });

            items.push({
                key: 9,
                name: t('text.totalFrames'),
                value: props.item.info?.totalFrames
            });

            const columns = [
                {
                    title: 'Name',
                    dataIndex: 'name',
                    key: 'name',
                },
                {
                    title: 'Value',
                    dataIndex: 'value',
                    key: 'value',
                },
            ];

            return (
                <Table
                    className={"mt-2"}
                    dataSource={items}
                    columns={columns}
                    bordered
                    pagination={false}
                    showHeader={false}
                />
            );

            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [props.item]);

        React.useEffect(() => {
            setPercentTime(0);
        }, [props.item]);

        const listMonth: string[] = [];

        if (dateStart !== undefined && dateEnd !== undefined) {
            while (dateEnd > dateStart || dateStart.format('M') === dateEnd.format('M')) {
                listMonth.push(dateStart.format('M/YY'));

                dateStart.add(1, 'month');
            }
        }

        const DrawTimelineFC = useCallback(() => {
            const totalBox = listMonth.length;
            const heightLine = 56;
            const heightText = 20;
            const colorLine1 = "#494849";
            const colorLine2 = "#656565";
            const colorActive = Color.Main;
            const totalLine = 10;
            const widthLine = 3;
            const spaceLine = 5;
            const spaceBox = 0;

            let widthSvg = 0;
            let widthBox = 0;

            const itemBox: IBox[] = [];

            listMonth.forEach((value, indexB) => {
                const itemRect: IRect[] = [];

                let widthG = 0;

                new Array(totalLine).fill('.').forEach((_, indexL) => {
                    let width = widthLine;
                    let height = heightLine / 2;

                    if (indexL === 0) {
                        height = heightLine;
                        // width += 0.5;
                    } else if (indexL === (totalLine / 2)) {
                        height = (heightLine / 2) + (heightLine / 4);
                    }

                    const x = (width + spaceLine) * indexL;
                    const y = heightLine - height;

                    widthG += (width + spaceLine);

                    itemRect.push({
                        x: x,
                        y: y,
                        width: width,
                        height: height
                    });
                });

                if (widthBox === 0) {
                    widthBox = widthG;
                }

                widthSvg += widthG;

                const x = (widthG + spaceBox) * indexB;
                const y = 0;

                itemBox.push({
                    x: x,
                    y: y,
                    width: widthG,
                    height: heightLine + heightText,
                    rect: itemRect,
                    month: value
                })
            });

            let runWidth;

            if (percentTime > 0) {
                runWidth = ((percentTime / 100) * widthSvg);
            } else {
                runWidth = widthLine;
            }

            const divTimeline = document.getElementById('timeline');

            if (videoRef.current !== null && divTimeline !== null) {
                const videoWidth = (videoRef.current! as Element).clientWidth;
                const maxScrollLeft = divTimeline.scrollWidth - videoWidth;

                if (runWidth > videoWidth - (widthBox * 2)) {
                    divTimeline.scrollTo({
                        left: (percentTime / 100) * maxScrollLeft,
                        behavior: "smooth"
                    })
                }
            }

            const indexCurrent = ((percentTime / 100) * (widthBox * totalBox)) / widthBox

            return (
                <svg width={widthSvg} height={heightLine + heightText} fill="red">
                    {
                        itemBox.map((e1, indexB) => {
                            return (
                                <svg key={indexB} width={e1.width} height={e1.height} x={e1.x} y={e1.y}>
                                    {
                                        e1.rect.map((e2, indexR) => {
                                            return (
                                                <rect
                                                    key={indexR}
                                                    width={e2.width}
                                                    height={e2.height}
                                                    x={e2.x}
                                                    y={e2.y}
                                                    fill={indexR === 0 ? colorLine1 : colorLine2}
                                                />
                                            )
                                        })
                                    }
                                    <text x={1}
                                          y={heightLine + heightText}
                                          fontSize={14}
                                          fontWeight={700}
                                          fill={parseInt(indexCurrent.toString().split('.')[0]) === indexB ? colorActive : colorLine1}
                                    >
                                        {e1.month}
                                    </text>
                                </svg>
                            );
                        })
                    }
                    <rect width={runWidth} height={heightLine} x={0} y={0} fill={colorActive} fillOpacity={0.4}/>
                </svg>
            );

            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [percentTime]);

        const getVideo = useMemo(() => {
            console.log('rerender');

            return (
                <HlsVideoFC
                    source={props.item?.hls?.file ?? ""}
                    mEncrypt={props.item.hls?.hid}
                    nEncrypt={props.item.hls?.name}
                    onPlay={onPlay}
                    onTimeUpdate={onTimeUpdate}
                />
            )
        }, [props.item])

        return (
            <>
                <div>
                    {getVideo}
                </div>
                <div className={`${styles.CardTimeline} bg-gray-300`} id={"timeline"}>
                    <DrawTimelineFC/>
                </div>
                <div>
                    <InfoHtml/>
                </div>
            </>
        );
    }
)
