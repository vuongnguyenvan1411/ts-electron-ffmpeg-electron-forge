import {CustomTypography} from "../components/CustomTypography";
import React from "react";
import {useTranslation} from "react-i18next";


export const InfoBusinessWidget = () => {
    const {t} = useTranslation();

    return (
        <div className={"bg-dark4 p-6"}>
                                    <span>
                                        <CustomTypography
                                            color={"#009B90"}
                                            textStyle={"text-14-20"}
                                            isStrong
                                        >
                                            Autotimelapse - Giải pháp cập nhật tiến độ xây dựng công trình
                                        </CustomTypography>
                                        <br/>
                                        <br/>
                                        <span>
                                            <CustomTypography
                                                textStyle={"text-14-20"}
                                            >
                                                Hotline:
                                            </CustomTypography>
                                            &nbsp;
                                            <CustomTypography
                                                textStyle={"text-14-20"}
                                                isStrong
                                            >
                                                (+84)886885808 – (+84)888985808
                                            </CustomTypography>
                                        </span>
                                        <br/>
                                        <span>
                                            <CustomTypography
                                                textStyle={"text-14-20"}
                                            >
                                                {t("text.address")}:
                                            </CustomTypography>
                                            &nbsp;
                                            <CustomTypography
                                                textStyle={"text-14-20"}
                                                isStrong
                                            >
                                                Tòa nhà D8, Đại học Bách Khoa Hà Nội, Trần Đại Nghĩa, Hà Nội, Việt Nam
                                            </CustomTypography>
                                        </span>
                                        <br/>
                                        <span>
                                            <CustomTypography
                                                textStyle={"text-14-20"}
                                            >
                                                Email:
                                            </CustomTypography>
                                            &nbsp;
                                            <CustomTypography
                                                textStyle={"text-14-20"}
                                                isStrong
                                            >
                                                autotimelapsevn@gmail.com
                                            </CustomTypography>
                                        </span>
                                        <br/>
                                        <span>
                                            <CustomTypography
                                                textStyle={"text-14-20"}
                                            >
                                                Youtube:
                                            </CustomTypography>
                                            &nbsp;
                                            <CustomTypography
                                                textStyle={"text-14-20"}
                                                isStrong
                                                type={"link"}
                                                href={"https://www.youtube.com/channel/UCgRFxKxd2vt5gn_mbS1sJgA"}
                                                target={"_blank"}
                                            >
                                            https://www.youtube.com/channel/UCgRFxKxd2vt5gn_mbS1sJgA
                                            </CustomTypography>
                                        </span>
                                    </span>
        </div>
    )
}
