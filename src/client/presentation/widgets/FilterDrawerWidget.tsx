import {useTranslation} from "react-i18next";
import moment from "moment";
import {findKey, range} from "lodash";
import React, {FC, useCallback, useEffect, useState} from "react";
import {Checkbox, DatePicker, Divider, Drawer, Select, Typography} from "antd";
import {CustomButton} from "../components/CustomButton";
import styles from "../../styles/module/Filter.module.scss";
import {CustomTypography} from "../components/CustomTypography";
import {CheckboxChangeEvent} from "antd/es/checkbox";
import {TActive, TFilterData, TOrder} from "../../const/Types";
import {CustomTimePicker} from "../components/CustomTimePicker";

interface _ILst {
    name: string,
    value: string | string[] | null,
}

interface _ILstTime extends _ILst {
    subName?: string,
    range: string[],
}

interface _IOrder {
    name: string,
    checked: boolean,
    onChange: (e: CheckboxChangeEvent) => void,
}

interface _IRangeDates {
    label: string,
    value?: moment.Moment,
    onChange: (date: moment.Moment | null, dateString: string) => void,
    disableDate?: (date: moment.Moment) => boolean,
    disabledTime?: () => any,
}

type _TFilterProps = {
    onClick: (data: TFilterData) => void,
    current?: Partial<_TFilterState>,
    display: {
        active: boolean,
        order: boolean,
        rangeDate: boolean,
        timeRange: boolean,
        fastFilter: boolean,
    },
    onClose: (e: any) => void,
    visible: boolean,
    allTime?: [string | undefined, string | undefined],
}

type _TFilterState = {
    active: TActive;
    order: TOrder;
    dateStart: string;
    dateEnd: string;
    timeStart: string;
    timeEnd: string;
    period: string;
}

const FilterDrawerWidget: FC<_TFilterProps> = props => {
    const {t} = useTranslation();
    const formatDate = t('format.date');
    const formatTime = "HH:mm";
    const _today = Date.now();
    const _tomorrow = new Date(_today);
    _tomorrow.setDate(_tomorrow.getDate() - 1);
    const _lastWeek = new Date(_today);
    _lastWeek.setDate(_lastWeek.getDate() - 7);
    const _lastHalfMonth = new Date(_today);
    _lastHalfMonth.setDate(_lastHalfMonth.getDate() - 15);
    const _lastMonth = new Date(_today);
    _lastMonth.setDate(_lastMonth.getDate() - 30);
    const lstSelectDate: _ILst[] = [
        {
            name: t("text.today"),
            value: [moment(Date.now()).format(formatDate).toString(), moment(Date.now()).format(formatDate).toString()]
        },
        {
            name: t("text.yesterday"),
            value: [moment(_tomorrow).format(formatDate).toString(), moment(_tomorrow).format(formatDate).toString()]
        },
        {
            name: t("text.lastWeek"),
            value: [moment(_lastWeek).format(formatDate).toString(), moment(_today).format(formatDate).toString()]
        },
        {
            name: t("text.lastHalfMount"),
            value: [moment(_lastHalfMonth).format(formatDate).toString(), moment(_today).format(formatDate).toString()]
        },
        {
            name: t("text.mount"),
            value: [moment(_lastMonth).format(formatDate).toString(), moment(_today).format(formatDate).toString()]
        },
        ...(props.allTime !== undefined ? [{
            name: t("text.allTime"),
            value: [
                props.allTime && props.allTime[0] ? props.allTime[0]?.split(' ')[0] : moment(Date.now()).format(formatDate).toString(),
                props.allTime && props.allTime[1] ? props.allTime[1]?.split(' ')[0] : moment(Date.now()).format(formatDate).toString()
            ]
        } as _ILst] : []),
        {
            name: t("text.custom"),
            value: null
        },
    ]
    const lstSelectPeriod: _ILstTime[] = [
        {
            name: t("text.sunrise"),
            subName: "(5h - 7h)",
            value: 'bm',
            range: ["05:00", "07:00"],
        },
        {
            name: t("text.morning"),
            subName: "(8h - 16h)",
            value: 'm',
            range: ["08:00", "16:00"],
        },
        {
            name: t("text.sunset"),
            subName: "(17h - 19h)",
            value: 'hh',
            range: ["17:00", "19:00"],
        },
        {
            name: `${t("text.night")}`,
            subName: "(19h - 5h)",
            value: 'n',
            range: ["19:00", "5:00"],
        },
        {
            name: t("text.lightAndDark"),
            value: 'ld',
            range: ["00:01", "23:59"],
        },
        {
            name: t("text.custom"),
            value: 'h',
            range: [],
        },
    ];
    const [filter, setFilter] = useState<_TFilterState>({
        active: '',
        order: 'desc',
        dateEnd: '',
        dateStart: '',
        timeStart: '',
        timeEnd: '',
        period: '',
    });
    const [selectDateValue, setSelectDateValue] = useState<number | undefined>(undefined);
    const [selectPeriodValue, setSelectPeriodValue] = useState<number | undefined>(undefined);

    const onChangeDatePicker = (date: moment.Moment | null, dateString: string, type: "end" | "start") => {
        if (type === "end") {
            setFilter({
                ...filter,
                dateEnd: date !== null ? date.format(formatDate) : '',
            })
        } else {
            setFilter({
                ...filter,
                dateStart: date !== null ? date.format(formatDate) : '',
            })
        }

        setSelectDateValue(lstSelectDate.length - 1);
    }

    const onChangeTimePicker = (time: moment.Moment | null, timeString: string, type: "end" | "start") => {
        if (type === "end") {
            setFilter({
                ...filter,
                timeEnd: time !== null ? time.format(formatTime) : '',
            })
        } else {
            setFilter({
                ...filter,
                timeStart: time !== null ? time.format(formatTime) : '',
            })
        }

        setSelectPeriodValue(lstSelectPeriod.length - 1);
    }

    function handleChange(value: number) {
        setSelectDateValue(value);

        const select = lstSelectDate[value].value;

        if (select !== null) {
            setFilter({
                ...filter,
                dateStart: select[0],
                dateEnd: select[1],
            })
        }
    }

    const onClickFilter = () => {
        props.onClick({
            active: filter.active,
            order: filter.order,
            dateStart: filter.dateStart,
            dateEnd: filter.dateEnd,
            timeStart: filter.timeStart,
            timeEnd: filter.timeEnd,
            period: filter.period,
        });
    };

    useEffect(() => {
        const currentKeyDate = findKey(lstSelectDate, ['value', [props.current?.dateStart, props.current?.dateEnd]]);
        let currentKeyTime;

        if (props.current?.period) {
            currentKeyTime = findKey(lstSelectPeriod, ['value', props.current?.period]);
        } else {
            currentKeyTime = findKey(lstSelectPeriod, ['range', [props.current?.timeStart, props.current?.timeEnd]]);
        }

        if (!(props.current?.period) && (props.current?.timeStart || props.current?.timeEnd)) currentKeyTime = (lstSelectPeriod.length - 1).toString();

        const _parseKeyTime = currentKeyTime ? parseInt(currentKeyTime) : undefined;

        setSelectDateValue(currentKeyDate ? parseInt(currentKeyDate) : undefined);
        setSelectPeriodValue(_parseKeyTime);

        let _timeStart;
        let _timeEnd;

        if (props.current?.period && props.current.period !== "h" && _parseKeyTime) {
            _timeStart = lstSelectPeriod[_parseKeyTime].range[0];
            _timeEnd = lstSelectPeriod[_parseKeyTime].range[1];
        } else {
            _timeStart = props.display.timeRange ? props.current?.timeStart ?? '' : '';
            _timeEnd = props.display.timeRange ? props.current?.timeEnd ?? '' : '';
        }

        setFilter({
            active: props.display.active ? props.current?.active ?? '' : '',
            order: props.display.order ? props.current?.order ?? 'desc' : '',
            dateStart: props.display.rangeDate ? props.current?.dateStart ?? '' : '',
            dateEnd: props.display.rangeDate ? props.current?.dateEnd ?? '' : '',
            timeStart: _timeStart,
            timeEnd: _timeEnd,
            period: props.display.timeRange ? props.current?.period ?? '' : '',
        })
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.current, props.display])

    const onReset = () => {
        setSelectDateValue(undefined);

        props.onClick({
            active: '',
            order: '',
            dateStart: '',
            dateEnd: '',
            timeStart: '',
            timeEnd: '',
            period: '',
        })

        setFilter({
            active: '',
            order: '',
            dateStart: '',
            dateEnd: '',
            timeStart: '',
            timeEnd: '',
            period: '',
        })
    }

    const pickerDateRanges: any = {};
    pickerDateRanges[t('text.today')] = [moment(), moment()];
    pickerDateRanges[t('text.thisMonth')] = [moment().startOf('month'), moment().endOf('month')];
    pickerDateRanges[t('text.thisYear')] = [moment().startOf('year'), moment()];

    const pickerTimeRanges: any = {};
    pickerTimeRanges[t('text.morning')] = [moment("08:00", formatTime), moment("16:00", formatTime)];
    pickerTimeRanges[t('text.night')] = [moment("19:00", formatTime), moment("23:59", formatTime)];

    const onChangeActive = (e: CheckboxChangeEvent, type: 'active' | 'inActive') => {
        if (type === 'active' && e.target.checked) {
            setFilter({
                ...filter,
                active: '1',
            })
        } else if (type === 'inActive' && e.target.checked) {
            setFilter({
                ...filter,
                active: '0',
            })
        } else {
            setFilter({
                ...filter,
                active: '',
            })
        }
    };

    const onChangeOrder = (e: CheckboxChangeEvent, type: TOrder) => {
        if (type === 'asc' && e.target.checked) {
            setFilter({
                ...filter,
                order: 'asc',
            })
        } else if (type === 'desc' && e.target.checked) {
            setFilter({
                ...filter,
                order: 'desc',
            })
        } else {
            setFilter({
                ...filter,
                order: '',
            })
        }
    };

    function handleChangeSelectPeriod(value: number) {
        setSelectPeriodValue(value);

        const select = lstSelectPeriod[value].value;
        const _range = lstSelectPeriod[value].range;

        if (select !== null && typeof select === "string") {
            // keyPeriod.current = select;
            filter.period = select;
        }

        // if (select === 'h') {
        //     setIsEditPeriod(true);
        // } else {
        //     setIsEditPeriod(false);
        //     filter.dateStart = '';
        //     filter.dateEnd = '';
        // }

        if (_range && _range.length > 0) {
            setFilter({
                ...filter,
                timeStart: _range[0],
                timeEnd: _range[1],
            })
        }
    }

    const _buildActive = () => {
        const _items: _IOrder[] = [
            {
                name: t("text.inActive"),
                checked: filter.active === '0',
                onChange: (e) => {
                    onChangeActive(e, 'inActive')
                }
            },
            {
                name: t("text.active"),
                checked: filter.active === '1',
                onChange: (e) => {
                    onChangeActive(e, 'active')
                }
            }
        ];

        return <div className={styles.Filter_Row}>
            <div className={styles.Filter_Div_Status_Label}>
                <CustomTypography
                    textStyle={"text-16-24"}
                    isStrong
                >
                    {t("text.status")}
                </CustomTypography>
            </div>
            <div className={styles.Filter_Div_Status_Value}>
                {
                    _items.map((value, index) => {
                        return <div key={index} className={"flex gap-2 items-center"}>
                            <Checkbox
                                className={"checkbox-main checkbox-main-bg-BDBDBD-h-24"}
                                onChange={value.onChange}
                                checked={value.checked}
                            />
                            <CustomTypography
                                textStyle={"text-14-20"}
                            >
                                {value.name}
                            </CustomTypography>
                        </div>
                    })
                }
            </div>
        </div>
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }

    const _buildOrder = () => {
        const _items: _IOrder[] = [
            {
                name: t("text.orderAsc"),
                checked: filter.order === 'asc',
                onChange: (e) => {
                    onChangeOrder(e, 'asc')
                }
            },
            {
                name: t("text.orderDesc"),
                checked: filter.order === 'desc',
                onChange: (e) => {
                    onChangeOrder(e, 'desc')
                }
            }
        ];

        return <div className={styles.Filter_Row}>
            <div className={styles.Filter_Div}>
                <CustomTypography
                    textStyle={"text-16-24"}
                    isStrong
                >
                    {t("text.order")}
                </CustomTypography>
            </div>
            <div className={styles.Filter_Div}>
                {
                    _items.map((value, index) => {
                        return <div key={index} className={"flex gap-2 items-center"}>
                            <Checkbox
                                className={"checkbox-main checkbox-main-bg-BDBDBD-h-24"}
                                onChange={value.onChange}
                                checked={value.checked}
                            />
                            <CustomTypography
                                textStyle={"text-14-20"}
                            >
                                {value.name}
                            </CustomTypography>
                        </div>
                    })
                }
            </div>
        </div>
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }

    const _buildFastFilter = useCallback(() => {
        return <div className={styles.Filter_Row_Time}>
            <div className={styles.Filter_Div}>
                <CustomTypography
                    textStyle={"text-14-20"}
                    isStrong
                >
                    {t("text.fastFilter")}
                </CustomTypography>
            </div>
            <div className={styles.Filter_Div}>
                <Select
                    className={"select-form-main select-form-bg-white select-form-rounded select-form-border-969696"}
                    key={'select-time'}
                    style={{width: '100%'}}
                    placeholder={t("text.selectTimePeriod")}
                    value={selectDateValue}
                    onChange={handleChange}
                >
                    {
                        lstSelectDate.map((value, index) =>
                            <Select.Option key={index} value={index}>
                                <Typography.Text strong>{value.name}</Typography.Text>
                            </Select.Option>
                        )
                    }
                </Select>
            </div>
        </div>
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selectDateValue, t])

    const _buildRangeDate = () => {
        const _items: _IRangeDates[] = [
            {
                label: t("text.dateStart"),
                value: filter.dateStart.length > 0 ? moment(filter.dateStart, formatDate) : undefined,
                onChange: (date, dateString) => {
                    onChangeDatePicker(date, dateString, "start")
                },
                disableDate: (date) => {
                    return date.endOf('day') > moment(filter.dateEnd, formatDate).endOf('day');
                }
            },
            {
                label: t("text.dateEnd"),
                value: filter.dateEnd.length > 0 ? moment(filter.dateEnd, formatDate) : undefined,
                onChange: (date, dateString) => {
                    onChangeDatePicker(date, dateString, "end")
                },
                disableDate: (date) => {
                    return date.endOf('day') < moment(filter.dateStart, formatDate).endOf('day');
                }
            },
        ]

        return <div className={"flex  gap-3 w-full justify-between"}>
            {
                _items.map((value, index) => (
                    <div key={index} className={"flex flex-col w-full"}>
                        <div className={"flex flex-col gap-2"}>
                           <span>
                               <CustomTypography
                                   textStyle={"text-14-20"}
                                   isStrong
                               >
                                   {value.label}
                               </CustomTypography>&nbsp;
                               {/*    <CustomTypography*/}
                               {/*        textStyle={"text-12-18"}*/}
                               {/*        isStrong={false}*/}
                               {/*    >*/}
                               {/*      ({t("text.option")})*/}
                               {/*</CustomTypography>*/}
                           </span>
                            <DatePicker
                                disabled={props.display.fastFilter && selectDateValue !== (lstSelectDate.length - 1)}
                                className={"date-picker-main date-picker-radius date-picker-p-10-16 date-picker-border-969696"}
                                format={t("format.date")}
                                dropdownClassName={"date-picker-dropdown-main date-picker-dropdown-no-rounded"}
                                onChange={value.onChange}
                                value={value.value}
                                defaultValue={value.value}
                                disabledDate={value.disableDate}
                                placement={"bottomRight"}
                                showNow={false}
                                showToday={false}
                            />
                        </div>
                    </div>
                ))
            }
        </div>
    }

    const _buildFastFilterTime = () => {
        return <div className={styles.Filter_Row_Time}>
            <div className={styles.Filter_Div}>
                <CustomTypography
                    textStyle={"text-14-20"}
                    isStrong
                >
                    {t("text.fastFilter")}
                </CustomTypography>
            </div>
            <div className={styles.Filter_Div}>
                <Select
                    className={"select-form-main select-form-bg-white select-form-rounded select-form-border-969696"}
                    key={'select-time'}
                    style={{width: '100%'}}
                    placeholder={t("text.selectTimePeriod")}
                    value={selectPeriodValue}
                    onChange={handleChangeSelectPeriod}
                >
                    {
                        lstSelectPeriod.map((value, index) =>
                            <Select.Option key={index} value={index}>
                                <span>
                                    <CustomTypography
                                        textStyle={"text-14-20"}
                                        isStrong
                                    >
                                        {value.name}
                                    </CustomTypography>
                                    &nbsp;
                                    {
                                        value.subName && <CustomTypography
                                            textStyle={"text-14-20"}
                                            style={{
                                                fontWeight: "300 !important",
                                            }}
                                        >
                                            {value.subName}
                                        </CustomTypography>
                                    }
                                </span>
                            </Select.Option>
                        )
                    }
                </Select>
            </div>
        </div>
    }

    const _buildRangeTime = () => {
        const _items: _IRangeDates[] = [
            {
                label: t("text.timeStart"),
                value: filter.timeStart.length > 0 ? moment(filter.timeStart, formatTime) : undefined,
                onChange: (date, dateString) => {
                    onChangeTimePicker(date, dateString, "start")
                },
                disabledTime: moment(filter.dateStart, formatDate).endOf('day').isSame(moment(filter.dateEnd, formatDate).endOf('day')) ?
                    () => ({
                        disabledHours: () => filter.timeEnd.length > 0 ? range(0, 24).splice(moment(filter.timeEnd, formatTime).hours() + 1, (24 - moment(filter.timeEnd, formatTime).hours())) : [],
                        disabledMinutes: () => filter.timeStart.length > 0 && moment(filter.timeStart, formatTime).hours() === moment(filter.timeEnd, formatTime).hours() ? range(moment(filter.timeEnd, formatTime).minutes(), 60) : [],
                    }) : undefined
            },
            {
                label: t("text.timeEnd"),
                value: filter.timeEnd.length > 0 ? moment(filter.timeEnd, formatTime) : undefined,
                onChange: (date, dateString) => {
                    onChangeTimePicker(date, dateString, "end")
                },
                disabledTime: moment(filter.dateStart, formatDate).endOf('day').isSame(moment(filter.dateEnd, formatDate).endOf('day')) ?
                    () => ({
                        disabledHours: () => filter.timeStart.length > 0 ? range(0, moment(filter.timeStart, formatTime).hours()) : [],
                        disabledMinutes: () => filter.timeStart.length > 0 && moment(filter.timeStart, formatTime).hours() === moment(filter.timeEnd, formatTime).hours() ? range(0, moment(filter.timeStart, formatTime).minutes()) : [],
                    }) : undefined
            },
        ]

        return <div className={"flex  gap-3 w-full justify-between"}>
            {
                _items.map((value, index) => (
                    <div
                        key={`range-time ${index}`}
                        className={"flex flex-col w-full"}
                    >
                        <div className={"flex flex-col gap-2"}>
                           <span>
                               <CustomTypography
                                   textStyle={"text-14-20"}
                                   isStrong
                               >
                                   {value.label}
                               </CustomTypography>&nbsp;
                               {/*<CustomTypography*/}
                               {/*    textStyle={"text-12-18"}*/}
                               {/*    isStrong={false}*/}
                               {/*>*/}
                               {/* ({t("text.option")})*/}
                               {/*</CustomTypography>*/}
                           </span>
                            <CustomTimePicker
                                disabled={selectPeriodValue !== (lstSelectPeriod.length - 1)}
                                format={formatTime}
                                onChange={value.onChange}
                                value={value.value}
                                defaultValue={value.value}
                                disabledTime={value.disabledTime}
                            />
                        </div>
                    </div>
                ))
            }
        </div>
    }

    const _buildDividerWithText = (title: string) => {
        return <div className={"w-full flex gap-4"}>
            <div className={"flex-none"}>
                <CustomTypography
                    textStyle={"text-16-24"}
                    isStrong
                >
                    {title}
                </CustomTypography>
            </div>
            <div className={"grow my-auto"}>
                <Divider className={"divider-main divider-horizontal-m-0 divider-border-min-dark50"}/>
            </div>
        </div>
    }

    return (
        <Drawer
            className={styles.Filter_Drawer}
            title={t("title.filterDetail")}
            placement={"right"}
            width={375}
            onClose={e => props.onClose(e)}
            visible={props.visible}
        >
            <div className={styles.Filter_Container}>
                {
                    props.display.active && _buildActive()
                }
                {
                    props.display.order && _buildOrder()
                }
                {
                    (props.display.fastFilter || props.display.rangeDate) && _buildDividerWithText(t("text.filterByDate"))
                }
                <div className={"flex flex-col gap-2"}>
                    {
                        props.display.fastFilter && _buildFastFilter()
                    }
                    {
                        props.display.rangeDate && _buildRangeDate()
                    }
                </div>
                {
                    props.display.timeRange && _buildDividerWithText(t("text.filterByHour"))
                }
                <div className={"flex flex-col gap-2"}>
                    {
                        props.display.timeRange && _buildFastFilterTime()
                    }
                    {
                        props.display.timeRange && _buildRangeTime()
                    }
                </div>
            </div>
            <div className={"absolute bottom-0 right-0 left-0 flex gap-2 pb-10 pt-4 px-4 bg-white"}>
                <CustomButton
                    fullWidth={true}
                    type={"outline"}
                    onClick={e => {
                        onReset();
                        props.onClose(e);
                    }}
                >
                    {t("button.setAgain")}
                </CustomButton>
                <CustomButton
                    fullWidth={true}
                    onClick={e => {
                        onClickFilter();
                        props.onClose(e);
                    }}
                >
                    {t("button.filter")}
                </CustomButton>
            </div>
        </Drawer>
    );
}

FilterDrawerWidget.defaultProps = {
    display: {
        order: true,
        active: true,
        rangeDate: true,
        timeRange: true,
        fastFilter: true,
    }
}

export default FilterDrawerWidget;
