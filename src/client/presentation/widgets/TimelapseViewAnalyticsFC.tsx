import {TimelapseSensorModel} from "../../models/service/timelapse/TimelapseSensorModel";
import {LineGraph} from "./ChartFC";
import {ChartDataset, ChartType, DefaultDataPoint} from "chart.js";
import moment from "moment";
import {Color} from "../../const/Color";
import {CommonEmptyFC} from "./CommonFC";

export const TimelapseViewAnalyticsFC = (props: {
    item: TimelapseSensorModel,
    selected: number
}) => {
    const range = props.item.range;
    const _id = props.item.sensor ? props.item.sensor[props.selected].sensor : undefined;

    const itemSelected = props.item.data && _id ? props.item.data[_id] : undefined;

    if (itemSelected === undefined) {
        return <CommonEmptyFC/>
    }

    const state = {
        labels: itemSelected.chart?.xaxis?.map((e: any) => {
            if (range === 'hour') {
                return moment(e, "HH:mm").format("HH:mm");
            } else {
                return moment(e, "YYYY-MM-DD").format("DD/MM/YYYY");
            }
        }),
    }

    const datasets: ChartDataset<ChartType, DefaultDataPoint<ChartType>>[] = [
        {
            label: `${itemSelected.label} (${itemSelected.avg} ${itemSelected.unit2})`,
            data: itemSelected.chart?.data ?? [],
            tension: 0.1,
            fill: true,
            backgroundColor: 'rgba(0, 155, 144, 0.6)',
            borderColor: 'rgba(0, 155, 144, 1)',
            borderWidth: 4,
            pointBorderColor: 'rgba(0, 155, 144, 1)',
            pointBackgroundColor: '#FFFFFF',
            pointHoverBackgroundColor: Color.Main,
            borderJoinStyle: "miter",
            pointHoverBorderColor: 'black',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBorderWidth: 1,
            pointRadius: 5,
            pointHitRadius: 10,
        }
    ]

    let max = Math.max.apply(Math, itemSelected.chart?.data ?? []);
    max = max ?? 1;

    return (
        <div>
            <LineGraph
                labels={state.labels}
                datasets={datasets}
                options={{
                    plugins: {
                        legend: {
                            display: true,
                            labels: {
                                color: 'rgba(248, 150, 51, 1)'
                            }
                        },
                        tooltip: {
                            callbacks: {
                                label: function (context) {
                                    let label = itemSelected.label;

                                    if (label) {
                                        label += ': ';
                                    }

                                    if (context.parsed.y !== null) {
                                        label += context.parsed.y.toString() + itemSelected.unit2;
                                    }

                                    return label ?? '';
                                }
                            }
                        }
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                    animation: {
                        duration: 200,
                        easing: "easeOutExpo",
                    },
                    scales: {
                        y: {
                            beginAtZero: false,
                            ticks: {
                                callback: tickValue => {
                                    if (itemSelected.unit2) {
                                        return tickValue + itemSelected.unit2;
                                    } else {
                                        return tickValue;
                                    }
                                },
                                // maxTicksLimit: 5,
                                // stepSize: Math.ceil(max / 5),
                            },
                            max: max + 2
                        },
                    }
                }}
            />
        </div>
    )
}
