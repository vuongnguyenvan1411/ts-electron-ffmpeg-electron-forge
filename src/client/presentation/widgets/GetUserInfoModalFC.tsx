import {useEffect, useState} from "react";
import {Color} from "../../const/Color";
import {Avatar, Image as AntImage, Modal} from "antd";
import {useTranslation} from "react-i18next";
import {UserModel} from "../../models/UserModel";
import NoAvatar from "../../assets/image/no_avatar.jpg";
import NoBg from "../../assets/image/no_bg.jpg";
import Image from "next/image";

export const GetUserInfoModalFC = (props: {
    user: UserModel,
    onClose: Function,
}) => {
    const {t} = useTranslation();

    useEffect(() => {
        console.log('%cMount FC: GetUserInfoModalFC', Color.ConsoleInfo);

        return () => {
            console.log('%cUnmount FC: GetUserInfoModalFC', Color.ConsoleInfo);
        }
    }, [])

    const [isModalVisible, setIsModalVisible] = useState(true);

    const handleClose = () => {
        setIsModalVisible(false);
        props.onClose();
    };

    return (
        <Modal
            key={1}
            visible={isModalVisible}
            onCancel={handleClose}
            cancelText={t('button.close')}
            footer={null}
            bodyStyle={{
                padding: "0",
                paddingBottom: "1rem",
            }}
        >
            <AntImage
                className={"w-full"}
                src={props.user.background}
                alt={props.user.name}
                fallback={NoBg.src}
            />
            <div
                className={'text-center'}
                style={{
                    marginTop: "-3.5rem"
                }}
            >
                <Avatar
                    size={{xs: 24, sm: 32, md: 40, lg: 64, xl: 80, xxl: 100}}
                    src={props.user.image}
                    alt={props.user.name}
                    style={{
                        backgroundColor: Color.BgAvatar
                    }}
                    onError={() => true}
                    icon={<Image src={NoAvatar} alt={props.user.name}/>}
                />
                <div className={"mt-3"}>
                    {props.user.name}
                </div>
            </div>
        </Modal>
    );
}
