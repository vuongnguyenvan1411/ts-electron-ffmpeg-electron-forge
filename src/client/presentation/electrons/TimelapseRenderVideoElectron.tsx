// import "../../../../electron"
import {lazy, Suspense, useEffect} from "react";
import {Color} from "../../const/Color";
import {useParams} from "react-router-dom";
import {useLocation} from "react-router";
import {CommonLoadingSpinFC} from "../widgets/CommonFC";
import ErrorBoundaryComponent from "../widgets/ErrorBoundaryComponent";
import {EDFile} from "../../core/encrypt/EDFile";

const RenderVideoFC = lazy(() => import("../modules/e-rv/RenderVideoFC"));

type TParamState = {
    name: string
    link: string
    createdAt: number
}

export const TimelapseRenderVideoElectron = (props: {
    state?: TParamState
}) => {
    const location = useLocation()
    const {hash} = useParams<{ hash: string }>()
    const params = (location.state ?? props.state) as TParamState

    useEffect(() => {
        console.log('%cMount Screen: TimelapseRenderVideoElectron', Color.ConsoleInfo)

        return () => {
            console.log('%cUnmount Screen: TimelapseRenderVideoElectron', Color.ConsoleInfo)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <ErrorBoundaryComponent>
            <Suspense fallback={<CommonLoadingSpinFC/>}>
                <RenderVideoFC
                    filter={EDFile.getLinkData(hash)}
                />
            </Suspense>
        </ErrorBoundaryComponent>
    )
}
