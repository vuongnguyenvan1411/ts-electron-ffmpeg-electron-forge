import Icon from "@ant-design/icons";
import {CustomIconComponentProps} from "@ant-design/icons/lib/components/Icon";


const TimelapseSvg = () => (
    <svg width="107" height="66" viewBox="0 0 107 66" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path opacity="0.5" d="M66.4929 64.9994H1L14.3135 19.3872H79.7467L66.4929 64.9994Z" fill="white" stroke="white"
              strokeWidth="0.3694" strokeMiterlimit="10"/>
        <path opacity="0.8" d="M79.6277 55.8065H14.1348L27.4483 10.1943H92.9412L79.6277 55.8065Z" fill="white"
              stroke="white" strokeWidth="0.3694" strokeMiterlimit="10"/>
        <path d="M92.7624 46.6122H27.2695L40.583 1H106.076L92.7624 46.6122Z" fill="white" stroke="white"
              strokeWidth="0.3694" strokeMiterlimit="10"/>
    </svg>
)

const TimelapseIcon = (props: Partial<CustomIconComponentProps>) => <Icon component={TimelapseSvg} {...props} />

export default TimelapseIcon;
