import Icon from "@ant-design/icons";


const ArrowRightFillSvg = () => (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fillRule="evenodd" clipRule="evenodd"
              d="M12 1.5C6.20101 1.5 1.5 6.20101 1.5 12C1.5 17.799 6.20101 22.5 12 22.5C17.799 22.5 22.5 17.799 22.5 12C22.5 6.20101 17.799 1.5 12 1.5ZM11.4697 8.78033C11.1768 8.48744 11.1768 8.01256 11.4697 7.71967C11.7626 7.42678 12.2374 7.42678 12.5303 7.71967L16.2803 11.4697C16.5732 11.7626 16.5732 12.2374 16.2803 12.5303L12.5303 16.2803C12.2374 16.5732 11.7626 16.5732 11.4697 16.2803C11.1768 15.9874 11.1768 15.5126 11.4697 15.2197L13.9393 12.75L8.25 12.75C7.83579 12.75 7.5 12.4142 7.5 12C7.5 11.5858 7.83579 11.25 8.25 11.25L13.9393 11.25L11.4697 8.78033Z"
              fill="#009B90"/>
    </svg>
)

const ArrowRightFillIcon = (props: any) => <Icon component={ArrowRightFillSvg} {...props} />

export default ArrowRightFillIcon;