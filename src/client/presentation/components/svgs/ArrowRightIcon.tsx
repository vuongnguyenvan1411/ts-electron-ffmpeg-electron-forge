import Icon from "@ant-design/icons";


const ArrowRightSvg = () => (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M9 4.5L16.5 12L9 19.5" stroke="white" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
    </svg>
)

const ArrowRightIcon = (props: any) => <Icon component={ArrowRightSvg} {...props} />

export default ArrowRightIcon;