import Icon from "@ant-design/icons";


const ArrowDownSvg = () => (
    <svg width="12" height="8" viewBox="0 0 12 8" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
            d="M6 4.73077L1.96154 0.692307C1.5 0.230769 0.807692 0.230769 0.346154 0.692307C-0.115385 1.15385 -0.115385 1.84615 0.346154 2.30769L5.19231 7.15385C5.65385 7.61538 6.34615 7.61538 6.80769 7.15385L11.6538 2.30769C11.8846 2.07692 12 1.84615 12 1.5C12 1.15384 11.8846 0.923076 11.6538 0.692306C11.1923 0.230768 10.5 0.230768 10.0385 0.692307L6 4.73077Z"
            fill="white"/>
    </svg>
)

const ArrowDownIcon = (props: any) => <Icon component={ArrowDownSvg} {...props} />

export default ArrowDownIcon;