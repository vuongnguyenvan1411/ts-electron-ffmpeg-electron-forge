import Icon from "@ant-design/icons";


const MailSvg = () => (
    <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
            d="M0.0728134 2.18006C0.325165 1.2134 1.20427 0.5 2.25 0.5H15.75C16.7957 0.5 17.6748 1.2134 17.9272 2.18006L9.40266 7.60476C9.15698 7.7611 8.84302 7.7611 8.59734 7.60476L0.0728134 2.18006Z"
            fill="#BDBDBD"/>
        <path
            d="M0 3.91169V13.25C0 14.4926 1.00736 15.5 2.25 15.5H15.75C16.9926 15.5 18 14.4926 18 13.25V3.91169L10.208 8.87025C9.47095 9.33927 8.52905 9.33927 7.79203 8.87025L0 3.91169Z"
            fill="#BDBDBD"/>
    </svg>

)

const MailIcon = (props: any) => <Icon component={MailSvg} {...props} />

export default MailIcon;