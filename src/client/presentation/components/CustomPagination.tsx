import {FC} from "react";
import {Pagination, PaginationProps} from "antd";

interface Props extends PaginationProps {
    defaultClass?: boolean,
    containerClassName?: string,
    isShow?: boolean,
}

const CustomPagination: FC<Props> = props => {
    return props.isShow ? <div className={`${props.containerClassName ?? props.defaultClass ? "pt-6" : ''}`}>
        <Pagination
            responsive={true}
            showLessItems={false}
            showSizeChanger={false}
            showQuickJumper
            {...props}
        />
    </div> : null;
}

CustomPagination.defaultProps = {
    defaultClass: true,
    isShow: true,
}

export default CustomPagination;
