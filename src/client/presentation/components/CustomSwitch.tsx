import {Switch, SwitchProps} from "antd";

export const CustomSwitch = (props: Partial<SwitchProps>) => {
    return (
        <Switch
            {...props}
            size={"small"}
            style={{
                minWidth: 31,
            }}
        />
    )
}