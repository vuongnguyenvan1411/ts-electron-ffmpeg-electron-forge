import React, {CSSProperties} from "react";
import {Button} from "antd";
import {ButtonHTMLType} from "antd/es/button/button";

interface Props {
    id?: string,
    size?: "small" | "normal" | "large";
    type?: "normal" | "outline" | "text",
    children?: React.ReactNode;
    onClick?: React.MouseEventHandler<HTMLElement> | undefined;
    disabled?: boolean;
    fullWidth?: boolean;
    htmlType?: ButtonHTMLType;
    loading?: boolean | {
        delay?: number;
    };
    style?: CSSProperties | undefined;
    className?: string;
    icon?: React.ReactNode;
    responsive?: boolean;
    usingFor?: "main" | "geodetic"
}

export const CustomButton: React.FC<Props> = ({
                                                  id,
                                                  size,
                                                  type,
                                                  children,
                                                  onClick,
                                                  disabled,
                                                  fullWidth,
                                                  htmlType,
                                                  loading,
                                                  style,
                                                  className,
                                                  icon,
                                                  responsive,
                                                  usingFor,
                                              }) => {

    const getType = (): "link" | "text" | "ghost" | "default" | "primary" | "dashed" | undefined => {
        switch (type) {
            case "normal":
                return 'primary';
            case "outline":
                return 'default';
            case "text":
                return 'text';
            default:
                return 'primary';
        }
    }

    const getClassName = (): string => {
        let cln: string;

        switch (type) {
            case "normal":
                cln = `btn-main${disabled ? ' btn-main-disable' : ''}`;
                break;

            case 'outline':
                cln = `btn-outline-main${disabled ? ' btn-outline-main-disable' : ''}`;
                break;

            case 'text':
                cln = ``;
                break;

            default:
                cln = `btn-main${disabled ? ' btn-main-disable' : ''}`;
                break;
        }

        switch (size) {
            case 'small':
                cln += `${type === "outline" ? " btn-out-line-small" : " btn-main-small"} ${responsive ? "btn-responsive-p-small" : ""}`;
                break;

            case "normal":
                cln += `${type === "outline" ? " btn-out-line-normal" : " btn-main-normal"} `;
                break;

            case "large":
                cln += `${type === "outline" ? " btn-out-line-large" : " btn-main-large"} `;
                break;

            default:
                cln += `${type === "outline" ? " btn-out-line-normal" : " btn-main-normal"} `;
                break;
        }

        switch (usingFor) {
            case "main":
                break;

            case "geodetic":
                cln += ` btn-bg-geodetic`;
                break;

            default:
                break;
        }

        if (type === "text") cln = '';

        if (icon) {
            cln += ` ${type === "outline" ? " btn-outline-icon" : " btn-main-icon"}`;
        }

        if (responsive) cln += ` ${type === "outline" ? " btn-outline-responsive" : " btn-main-responsive"}`;

        return cln;
    }

    return (
        <Button
            id={id}
            icon={icon && <span className={"anticon"}>
                {icon}
            </span>}
            style={style}
            htmlType={htmlType}
            type={getType()}
            onClick={onClick}
            className={`${getClassName()}  ${children !== undefined ? "" : "btn-icon-only"} ${className ?? ''} `}
            disabled={disabled}
            block={fullWidth}
            loading={loading}
        >
            {children}
        </Button>
    )
}

