import {TimePicker, TimePickerProps} from "antd";
import {FC} from "react";

export const CustomTimePicker: FC<TimePickerProps> = props => {
    return <TimePicker
        {...props}
        className={props.className ?? "time-picker-main time-picker-h-40 time-picker-p-10-16 time-picker-border-969696"}
        popupClassName={`timepicker-popup-main${props.popupClassName ? ` ${props.popupClassName}` : ''}`}
        onSelect={value => {
            props.onSelect && props.onSelect(value)
            props.onChange &&
            props.onChange(value, value.format(props.format?.toString()))
        }}
    />
}