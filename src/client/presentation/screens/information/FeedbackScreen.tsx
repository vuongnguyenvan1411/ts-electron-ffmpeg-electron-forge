import {useTranslation} from "react-i18next";
import {useEffect, useState} from "react";
import {Color} from "../../../const/Color";
import {Alert, Button, Card, Form, Input, Result} from "antd";
import {ErrorItemFC} from "../../widgets/ErrorItemFC";
import {RouteConfig} from "../../../config/RouteConfig";
import {SendingStatus} from "../../../const/Events";
import {useNavigate} from "react-router";
import {FeedbackAction} from "../../../recoil/information/feedback/FeedbackAction";
import {FieldData, ValidateErrorEntity} from "rc-field-form/lib/interface";
import {CustomButton} from "../../components/CustomButton";
import {WrapContentWidget} from "../../widgets/WrapContentWidget";

type TFormFinish = {
    message: string
}

export function FeedbackScreen() {
    const {t} = useTranslation()
    const navigate = useNavigate()
    const _title = t("title.feedback");

    const {
        vm,
        onPost,
        onClearState
    } = FeedbackAction()

    useEffect(() => {
        console.log('%cMount Screen: FeedbackScreen', Color.ConsoleInfo)

        return () => {
            onClearState()

            console.log('%cUnmount Screen: FeedbackScreen', Color.ConsoleInfo)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const onFinish = (values: TFormFinish) => {
        onPost({
            message: values.message,
        })
    }

    const [clientMessageError, setClientMessageError] = useState('')

    const onFinishFailed = (errorInfo: ValidateErrorEntity) => {
        errorInfo.errorFields.forEach((e: any) => {
            if (e.name[0] === "message") {
                setClientMessageError(e.errors[0])
            }
        })
    }

    const onFieldsChange = (data: FieldData[]) => {
        data.forEach((e: any) => {
            if (e.name[0] === "message") {
                setClientMessageError(e.errors.length > 0 ? e.errors[0] : '')
            }
        })
    }

    useEffect(() => {
        if (vm.status === SendingStatus.error) {
            setClientMessageError('')
        }
    }, [vm.status])

    return (
        <WrapContentWidget
            masterHeader={{
                title: _title,
            }}
            bodyHeader={{
                title: _title,
            }}
        >
            <ErrorItemFC status={vm.status} typeView={'modal'}>
                {
                    vm.status === SendingStatus.error && vm.error && vm.error.hasOwnProperty('warning')
                        ? <Alert className={"mb-5"} message={vm.error['warning']} type="error" showIcon/>
                        : null
                }
                <Card>
                    {
                        vm.status !== SendingStatus.success
                            ? <Form
                                name={"feedback"}
                                initialValues={{
                                    remember: true
                                }}
                                onFinish={onFinish}
                                onFinishFailed={onFinishFailed}
                                onFieldsChange={onFieldsChange}
                                size={'large'}
                            >
                                <Form.Item
                                    key={1}
                                    name={"message"}
                                    rules={[
                                        {
                                            required: true,
                                            message: t('error.feedbackMessageRequired')
                                        },
                                        {
                                            min: 10,
                                            message: t('error.feedbackMessageMin')
                                        },
                                        {
                                            max: 1000,
                                            message: t("error.feedbackMessageMax")
                                        }
                                    ]}
                                    validateStatus={(vm.status === SendingStatus.error && vm.error && vm.error.hasOwnProperty('message')) || clientMessageError.length > 0 ? 'error' : ''}
                                    help={clientMessageError.length > 0 ? clientMessageError : (vm.status === SendingStatus.error && vm.error && vm.error.hasOwnProperty('message') ? vm.error.message : null)}
                                >
                                    <Input.TextArea
                                        showCount
                                        maxLength={1000}
                                        placeholder={t('text.contentComment')}
                                    />
                                </Form.Item>

                                <Form.Item>
                                    <CustomButton
                                        loading={vm.status === SendingStatus.loading}
                                        htmlType="submit"
                                    >
                                        {t('button.sendFeedBack')}
                                    </CustomButton>
                                </Form.Item>
                            </Form>
                            : <Result
                                key={"feedback"}
                                status="success"
                                title={t("message.feedBackSuccess")}
                                extra={[
                                    <Button key={"home"} type="primary"
                                            onClick={() => navigate(RouteConfig.HOME, {replace: true})}>
                                        {t("title.home")}
                                    </Button>,
                                ]}
                            />
                    }
                </Card>
            </ErrorItemFC>
        </WrapContentWidget>
    )
}
