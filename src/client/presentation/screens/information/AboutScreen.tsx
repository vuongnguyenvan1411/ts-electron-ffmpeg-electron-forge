import {useTranslation} from "react-i18next";
import React, {useEffect, useState} from "react";
import {Color} from "../../../const/Color";
import {ErrorItemFC} from "../../widgets/ErrorItemFC";
import {SendingStatus} from "../../../const/Events";
import {CommonEmptyFC, CommonLoadingSpinFC} from "../../widgets/CommonFC";
import {AboutModel} from "../../../models/information/AboutModel";
import {Divider, notification, Progress, Spin} from "antd";
import {AboutAction} from "../../../recoil/information/about/AboutAction";
import {WrapContentWidget} from "../../widgets/WrapContentWidget";
import {CustomTypography} from "../../components/CustomTypography";
import bgTimelapse from "../../../assets/image/bg_timelapse.jpeg";
import NextImage from "next/image";
import styles from "../../../styles/module/About.module.scss";
import {Images} from "../../../const/Images";
import axios from "axios";
import download from "downloadjs";
import {LoadingOutlined} from "@ant-design/icons";
import {InfoBusinessWidget} from "../../widgets/InfoBusinessWidget";

export const AboutScreen = () => {
    const {t} = useTranslation()
    const [isDownload, setIsDownload] = useState(false);
    const [percentComplete, setPercentComplete] = useState<number | undefined>(undefined)
    const antIcon = <LoadingOutlined style={{fontSize: 24}} spin/>;

    const {
        vm,
        onLoadItem
    } = AboutAction()

    const title = t('title.about')

    useEffect(() => {
        console.log('%cMount Screen: AboutScreen', Color.ConsoleInfo)

        if (!vm.item) {
            onLoadItem()
        }

        return () => {
            console.log('%cUnmount Screen: AboutScreen', Color.ConsoleInfo);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const _onClickLink = (link: string) => {
        let el = document.createElement("a");
        el.href = link;
        el.target = "_blank";
        el.rel = "noopener noreferrer";
        el.click();
    }

    const downloadImage = (url: string, fileName: string) => {
        setIsDownload(true);
        axios
            .get(url, {
                responseType: "blob",
                onDownloadProgress: (evt: ProgressEvent) => {
                    if (evt.lengthComputable) {
                        setPercentComplete((evt.loaded / evt.total) * 100);
                    }
                }
            })
            .then(r => {
                const data = r.data as Blob;
                const filename = `${fileName}.pdf`;

                download(data, filename, data.type);

                setPercentComplete(undefined);
                setIsDownload(false)


                notification.success({
                    message: t('message.downloadImageSuccess'),
                });
            })
            .catch(_ => {
                setPercentComplete(undefined);
                setIsDownload(false)
                notification.error({
                    message: t('message.downloadImageFailure'),
                });
            });
    }

    return (
        <WrapContentWidget
            masterHeader={{
                title: t(`ATL-${title}`),
                isLoading: vm.isLoading,
                onReload: onLoadItem,
            }}
            bodyHeader={null}
        >
            <ErrorItemFC
                status={vm.isLoading}
            >
                {
                    vm.isLoading === SendingStatus.loading
                        ? <CommonLoadingSpinFC/>
                        : vm.item instanceof AboutModel
                            ? <div className={"max-w-screen-xl mx-auto flex flex-col gap-8"}>
                                <div dangerouslySetInnerHTML={{__html: vm.item.description ?? ''}}/>
                                <InfoBusinessWidget/>
                                <CustomTypography
                                    isStrong
                                    textStyle={"text-24-30"}
                                >
                                    {t("text.seeDocument")}
                                </CustomTypography>
                                <div className={"grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 gap-8"}>
                                    {
                                        vm.item.brochure?.map((value, index) => (
                                            <div key={index} className={styles.Item}>
                                                <NextImage
                                                    src={value.image ?? bgTimelapse}
                                                />
                                                <Divider
                                                    className={"divider-main divider-horizontal-m-0 divider-border-6-amber"}/>
                                                <div className={"flex flex-col p-4 gap-2"}>
                                                    <CustomTypography
                                                        textStyle={"text-16-24"}
                                                        isStrong
                                                    >
                                                        {value.name}
                                                    </CustomTypography>
                                                    {
                                                        value.description && <CustomTypography textStyle={"text-14-20"}>
                                                            {value.description}
                                                        </CustomTypography>
                                                    }
                                                </div>
                                                <div className={"relative"}>
                                                    <div className={styles.BoxActions}>
                                                        <div onClick={() => _onClickLink(value.file)}>
                                                            <NextImage
                                                                className={"atl-icon atl-icon-color-hex-969696"}
                                                                width={20}
                                                                height={20}
                                                                src={Images.iconEye.data}
                                                                alt={Images.iconEye.atl}
                                                            />
                                                            <CustomTypography
                                                                color={"#969696"}
                                                                isStrong
                                                                textStyle={"text-14-18"}
                                                            >
                                                                {t("button.view")}
                                                            </CustomTypography>
                                                        </div>
                                                        <div onClick={() => downloadImage(value.file, value.name)}>
                                                            <NextImage
                                                                className={"atl-icon atl-icon-color-hex-969696"}
                                                                width={20}
                                                                height={20}
                                                                src={Images.iconUploadSimple.data}
                                                                alt={Images.iconUploadSimple.atl}
                                                            />
                                                            <CustomTypography
                                                                color={"#969696"}
                                                                isStrong
                                                                textStyle={"text-14-18"}
                                                            >
                                                                {t("button.download")}
                                                            </CustomTypography>
                                                        </div>
                                                    </div>
                                                    {
                                                        percentComplete !== undefined || isDownload
                                                            ? <div
                                                                className={"bg-gray-200 absolute inset-0 flex justify-center items-center"}>
                                                                {
                                                                    percentComplete !== undefined ?
                                                                        <Progress
                                                                            type="circle"
                                                                            strokeColor={{
                                                                                '0%': '#108ee9',
                                                                                '100%': '#87d068',
                                                                            }}
                                                                            width={28}
                                                                            percent={percentComplete}
                                                                            strokeWidth={18}
                                                                            showInfo={false}
                                                                        />
                                                                        :
                                                                        <Spin indicator={antIcon}/>
                                                                }
                                                            </div> : null
                                                    }
                                                </div>
                                            </div>
                                        ))
                                    }
                                </div>
                            </div>
                            : <CommonEmptyFC/>
                }
            </ErrorItemFC>
        </WrapContentWidget>
    )
}
