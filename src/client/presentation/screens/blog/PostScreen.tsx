import {useTranslation} from "react-i18next";
import {CHashids} from "../../../core/CHashids";
import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import {useLocation} from "react-router";
import {Utils} from "../../../core/Utils";
import {Color} from "../../../const/Color";
import {Button, Divider, List, notification, Typography} from "antd";
import {CheckOutlined, ShareAltOutlined} from "@ant-design/icons";
import {App} from "../../../const/App";
import {Style} from "../../../const/Style";
import styles from "../../../styles/module/BlogPost.module.scss";
import {ErrorItemFC} from "../../widgets/ErrorItemFC";
import {SendingStatus} from "../../../const/Events";
import {CommonEmptyFC, CommonLoadingSpinFC} from "../../widgets/CommonFC";
import {PostAction} from "../../../recoil/blog/post/PostAction";
import {BlogPostInfoModel} from "../../../models/blog/BlogPostModel";
import {WrapContentWidget} from "../../widgets/WrapContentWidget";
import {CustomTypography} from "../../components/CustomTypography";
import {InfoBusinessWidget} from "../../widgets/InfoBusinessWidget";
import {BlogPostItemFC} from "../../widgets/BlogPostItemFC";

type TParamState = {
    name: string
}

export const PostScreen = () => {
    const {t} = useTranslation()
    const location = useLocation()
    const {hash} = useParams<{ hash: string }>()
    const params = location.state as TParamState
    const postId = CHashids.decodeGetFirst(hash!)
    const title = params?.name ?? `Auto TimeLapse ${t('title.news')}`

    const {
        vm,
        onLoadPost,
        resetStateWithEffect,
    } = PostAction()

    useEffect(() => {
        console.log('%cMount Screen: PostScreen', Color.ConsoleInfo)

        if (
            postId && (vm.item === null
                || vm.key !== postId!.toString()
                || (vm.timestamp !== undefined && Utils.checkHourState(vm.timestamp)))
        ) {
            if (vm.item) resetStateWithEffect()
            onLoadPost(postId)
        }

        return () => {
            console.log('%cUnmount Screen: PostScreen', Color.ConsoleInfo)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [postId])

    const [isCopyOk, setIsCopyOk] = useState(false)

    const onClickCopy = () => {
        Utils.copyClipboard(vm.item?.share!).then(() => {
            setIsCopyOk(true)

            notification.success({
                message: t('text.successCopy'),
                description: vm.item?.share!,
            })

            setTimeout(_ => setIsCopyOk(false), App.TimeoutHideCopy)
        })
    }


    return (
        <WrapContentWidget
            masterHeader={{
                title: title,
                isLoading: vm.isLoading,
                onReload: () => onLoadPost(postId!)
            }}
            bodyHeader={null}

        >
            <div className={"max-w-screen-xl mx-auto flex flex-col gap-8"}>
                <ErrorItemFC
                    status={vm.isLoading}
                >
                    {
                        vm.isLoading === SendingStatus.loading
                            ? <CommonLoadingSpinFC/>
                            : vm.item instanceof BlogPostInfoModel
                                ? <>
                                    <div className={"flex flex-col gap-3"}>
                                        <CustomTypography
                                            isStrong
                                            textStyle={"text-24-30"}
                                        >
                                            {title}
                                        </CustomTypography>
                                        <div className={"flex flex-row gap-2 items-center"}>
                                            <div className={"w-6"}>
                                                <Divider
                                                    className={"divider-main divider-horizontal-m-0 divider-border-1-009B90"}/>
                                            </div>
                                            <CustomTypography
                                                color={"#009B90"}
                                                isStrong
                                                textStyle={"text-14-20"}
                                            >
                                                {vm.item?.createdAtFormatted(t('format.date'))}
                                            </CustomTypography>
                                            <div className={"w-6"}>
                                                <Divider
                                                    className={"divider-main divider-horizontal-m-0 divider-border-1-009B90"}/>
                                            </div>
                                            <CustomTypography
                                                color={"#009B90"}
                                                isStrong
                                                textStyle={"text-14-20"}
                                            >
                                                Tin tức Auto Timelapse
                                            </CustomTypography>
                                            {
                                                vm.item?.share
                                                    ? <Button
                                                        className={'border-none bg-transparent'}
                                                        onClick={onClickCopy}
                                                        type={"default"}
                                                    >
                                                        {
                                                            isCopyOk
                                                                ? <CheckOutlined/>
                                                                : <ShareAltOutlined/>
                                                        }
                                                    </Button>
                                                    : null
                                            }
                                        </div>
                                    </div>
                                    <div>
                                        {
                                            vm.item?.description
                                                ? <div
                                                    className={styles.BlogPostDescription}
                                                    dangerouslySetInnerHTML={{__html: vm.item.description}}
                                                />
                                                : null
                                        }
                                    </div>
                                    <InfoBusinessWidget/>
                                    <div>
                                        {
                                            vm.item?.related !== undefined && vm.item?.related.length > 0
                                                ? <>
                                                    <Typography.Title level={4}>{t('text.relatedPosts')}</Typography.Title>
                                                    <List
                                                        // itemLayout="horizontal"
                                                        grid={Style.GridTypeBlogPost}
                                                        dataSource={vm.item.related}
                                                        split={true}
                                                        renderItem={(item, index) => (
                                                            <List.Item key={index}>
                                                                <BlogPostItemFC key={index} item={item}/>
                                                            </List.Item>
                                                        )}
                                                    />
                                                </>
                                                : null
                                        }
                                    </div>
                                </>
                                : <CommonEmptyFC/>
                    }
                </ErrorItemFC>
            </div>
        </WrapContentWidget>
    )
}
