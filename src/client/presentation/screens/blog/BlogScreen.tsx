import {useEffect, useRef, useState} from "react";
import {SendingStatus} from "../../../const/Events";
import {Color} from "../../../const/Color";
import {Utils} from "../../../core/Utils";
import {MediaQuery} from "../../../core/MediaQuery";
import {Style} from "../../../const/Style";
import {useTranslation} from "react-i18next";
import {useLocation, useNavigate} from "react-router";
import {Drawer, Form, Input} from "antd";
import {OrderedListOutlined} from "@ant-design/icons";
import {BlogPostItemFC, BlogSkeleton} from "../../widgets/BlogPostItemFC";
import {TPostsFilterVO} from "../../../models/blog/BlogPostModel";
import {ErrorItemFC} from "../../widgets/ErrorItemFC";
import {CommonEmptyFC, CommonTagFilterFC} from "../../widgets/CommonFC";
import {UrlQuery} from "../../../core/UrlQuery";
import {debounce} from "lodash";
import {App} from "../../../const/App";
import {PostsAction} from "../../../recoil/blog/posts/PostsAction";
import BlogCategoriesSiderLayout from "../../layouts/BlogCategoriesSiderLayout";
import {CustomButton} from "../../components/CustomButton";
import {WrapContentWidget} from "../../widgets/WrapContentWidget";
import styles from "../../../styles/module/Blog.module.scss";
import SearchIcon from "../../components/icons/SearchIcon";
import CustomPagination from "../../components/CustomPagination";

const cacheSearch = new Map()

export function BlogScreen() {
    const {t} = useTranslation()
    const navigate = useNavigate()
    const location = useLocation()
    const title = `${t('title.news')}`
    const [form] = Form.useForm();
    const {
        vm,
        onLoadItems,
        onCacheItems,
    } = PostsAction()

    const URL = new UrlQuery(location.search)
    const filter = URL.get('filter', {})
    const sort = URL.get('sort')
    const order = URL.get('order')
    const page = URL.getInt('page', vm.query.page)
    const limit = URL.getInt('limit', vm.query.limit)
    const categoryId = URL.get('category_id')

    const [queryParams, setQueryParams] = useState<TPostsFilterVO>({
        filter: filter,
        sort: sort,
        order: order,
        page: page,
        limit: limit,
        category_id: categoryId,
    })
    const [visible, setVisible] = useState(false);

    const showDrawer = () => {
        setVisible(true);
    };

    const onClose = () => {
        setVisible(false);
    };

    const onInit = () => {
        const urlQueryParams = new UrlQuery()
        urlQueryParams.set('page', 1)
        urlQueryParams.set('limit', (new MediaQuery(Style.GridLimitBlog)).getPoint(limit))

        onLoadItems(urlQueryParams.toObject())

        navigate({
            search: urlQueryParams.toString()
        })

        setQueryParams(urlQueryParams.toObject());
    }

    useEffect(() => {
        console.log('%cMount Screen: BlogScreen', Color.ConsoleInfo)
        if (
            vm.items.length === 0
            || (vm.timestamp !== undefined && Utils.checkHourState(vm.timestamp))
        ) {
            onInit();
        }

        return () => {
            console.log('%cUnmount Screen: BlogScreen', Color.ConsoleInfo)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        const urlQueryParams = new UrlQuery(queryParams)
        urlQueryParams.set('limit', vm.query.limit)
        setQueryParams(urlQueryParams.toObject())
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.query])

    const onChangePage = (page: number) => {
        const urlQueryParams = new UrlQuery(queryParams)
        urlQueryParams.set('page', page)
        urlQueryParams.set('category_id', categoryId)

        navigate({
            search: urlQueryParams.toString()
        })

        setQueryParams(urlQueryParams.toObject())
        onLoadItems(urlQueryParams.toObject())
    }

    const [keySearch, setKeySearch] = useState<string>('')

    useEffect(() => {
        if (vm.isLoading === SendingStatus.success && keySearch.length > 0 && !cacheSearch.has(keySearch)) {
            cacheSearch.set(keySearch, {
                items: vm.items,
                oMeta: vm.oMeta,
                query: vm.query,
            })
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.items])

    const onChangeSearch = (e: any) => setDebounceSearch.current(e.target.value)

    const setDebounceSearch = useRef(debounce((value: string) => {
        setKeySearch(value)
        const urlQueryParams = new UrlQuery(queryParams)

        if (value.length > 0) {
            urlQueryParams.set('filter.q', value)
        } else {
            urlQueryParams.delete('filter.q')
        }
        urlQueryParams.set('page', 1)
        urlQueryParams.delete('category_id')

        if (value.length > 0 && cacheSearch.has(value)) {
            onCacheItems(cacheSearch.get(value))
        } else {
            onLoadItems(urlQueryParams.toObject())
        }

        navigate({
            search: urlQueryParams.toString()
        }, {
            replace: true
        })

        setQueryParams(urlQueryParams.toObject())
    }, App.DelaySearch))

    const isTags = (typeof queryParams.filter?.q !== "undefined" && queryParams.filter?.q?.length > 0)

    const handleCloseTag = (name: string) => {
        const urlQueryParams = new UrlQuery(queryParams)
        urlQueryParams.delete(name)
        urlQueryParams.set('page', 1)

        navigate({
            search: urlQueryParams.toString()
        }, {
            replace: true
        })

        if (name === "filter.q") {
            form.resetFields();
        }

        setQueryParams(urlQueryParams.toObject())
        onLoadItems(urlQueryParams.toObject())
    }

    const _buildForm = () => {
        return <div className={"flex flex-none items-center gap-4"}>
            <Form form={form}>
                <Form.Item
                    className={"w-52 md:w-[360px]"}
                    style={{
                        marginBottom: "0px",
                    }}
                    name={"search"}
                >
                    <Input
                        className={"input-under-line-main"}
                        size="small"
                        placeholder={t('text.search')}
                        allowClear
                        onChange={onChangeSearch}
                        prefix={<SearchIcon/>}
                    />
                </Form.Item>
            </Form>
            <CustomButton
                size={"small"}
                onClick={() => showDrawer()}
                icon={<OrderedListOutlined/>}
            >
                {t('text.category')}
            </CustomButton>
        </div>;
    }

    const onClickReload = () => {
        const urlQueryParams = new UrlQuery(queryParams)
        onLoadItems(urlQueryParams.toObject())
    }

    const _buildLoading = () => {
        const _number = (new MediaQuery({
            xs: 1,
            sm: 2,
            md: 3,
            lg: 3,
            xl: 3,
            xxl: 4,
        })).getPoint(4);

        return (
            <>
                {
                    new Array(_number).fill('OK').map((_, index: number) => {
                        return <BlogSkeleton key={index}/>
                    })
                }
            </>
        );
    }

    return (
        <WrapContentWidget
            masterHeader={{
                title: title,
                isLoading: vm.isLoading,
                onReload: onClickReload,
            }}
            bodyHeader={{
                title: title,
                right: _buildForm(),
            }}
            className={styles.Blog_Container}
        >
            <CommonTagFilterFC
                is={isTags}
                onClose={handleCloseTag}
                data={queryParams}
                items={[
                    {
                        key: 'filter.q',
                        label: t('text.name')
                    }
                ]}
            />
            <ErrorItemFC
                status={vm.isLoading}
            >
                {

                    (vm.items.length > 0 && vm.isLoading === SendingStatus.success) || (vm.isLoading === SendingStatus.loading)
                        ? <div className={styles.Blog_Container_Items}>
                            {
                                vm.isLoading !== SendingStatus.loading
                                    ? vm.items.slice((page - 1) * vm.query.limit, page * vm.query.limit).map((item, index) => {
                                        return <BlogPostItemFC key={index.toString()} item={item}/>
                                    })
                                    : _buildLoading()
                            }
                        </div>
                        : <CommonEmptyFC/>
                }
            </ErrorItemFC>
            <CustomPagination
                isShow={vm.isLoading !== SendingStatus.loading && vm.items.length > 0 && vm.oMeta && vm.oMeta.pageCount > 1}
                defaultCurrent={page}
                current={page}
                total={vm.oMeta?.totalCount}
                defaultPageSize={vm.query.limit}
                onChange={onChangePage}
            />
            <Drawer
                placement="right"
                onClose={onClose}
                visible={visible}
                bodyStyle={{
                    padding: "1.5rem 0"
                }}
                extra={
                    <CustomButton
                        type={"outline"}
                        onClick={() => {
                            onInit();
                            onClose();
                        }}
                    >
                        {t("button.setAgain")}
                    </CustomButton>
                }
            >
                <BlogCategoriesSiderLayout/>
            </Drawer>
        </WrapContentWidget>

    )
}
