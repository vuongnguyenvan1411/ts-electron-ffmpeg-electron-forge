import {Avatar, Button, notification, Space, Typography, Upload} from "antd";
import {UserModel} from "../../../models/UserModel";
import {SendingStatus} from "../../../const/Events";
import {useEffect} from "react";
import {useTranslation} from "react-i18next";
import {Color} from "../../../const/Color";
import ImgCrop from "antd-img-crop";
import {CustomImage} from "../../components/CustomImage";
import {UploadOutlined} from "@ant-design/icons";
import {MeImageAction} from "../../../recoil/account/me_image/MeImageAction";

export const MeImageScreen = (props: { item: UserModel, type: string }) => {
    const {t} = useTranslation()

    const {
        vm,
        onUpdateImage,
        onClearState
    } = MeImageAction()

    useEffect(() => {
        console.log('%cMount Screen: MeImageScreen', Color.ConsoleInfo);

        return () => {
            onClearState()

            console.log('%cUnmount Screen: MeImageScreen', Color.ConsoleInfo);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (vm.isUpdating === SendingStatus.success) {
            notification.success({
                message: t('success.changeImage'),
            })
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.isUpdating])

    const onUpload = (file: File, type: string) => onUpdateImage({
        image: file,
        type: type
    })

    return (
        <div className={"image w-full flex justify-center"}>
            <Space direction="vertical" align="center">
                {
                    props.type === "avatar" ?
                        <Avatar
                            style={
                                {
                                    backgroundColor: "white",
                                    display: "block"
                                }
                            }
                            size={168}
                            src={
                                <CustomImage src={props.item.image} alt={""} preview={true}/>
                            }
                        />
                        :
                        <div className={`mb-4`}>
                            <CustomImage
                                src={props.item.background}
                                alt={""}
                                preview={true}
                                aspectRatio="2.7/1"
                            />
                        </div>
                }

                <ImgCrop
                    aspect={props.type === 'avatar' ? 1 : 2.7}
                    rotate
                >
                    <Upload
                        fileList={[]}
                        maxCount={1}
                        beforeUpload={
                            file => {
                                onUpload(file, props.type);
                                return false;
                            }
                        }
                    >
                        <Button
                            type={"primary"}
                            loading={vm.isUpdating === SendingStatus.loading}
                        >
                            <UploadOutlined/>
                            <Typography.Text className={"text-white"}>{t("button.upLoadFile")}</Typography.Text>
                        </Button>
                    </Upload>
                </ImgCrop>
            </Space>
        </div>
    )
}
