import {UrlQuery} from "../../../core/UrlQuery";
import {useLocation,} from "react-router";
import React, {useCallback} from "react";
import {MePasswordScreen} from "./MePasswordScreen";
import {MeEditScreen} from "./MeEditScreen";
import {CommonEmptyFC} from "../../widgets/CommonFC";
import {useTranslation} from "react-i18next";
import {WrapContentWidget} from "../../widgets/WrapContentWidget";

type TMeTypeBuild = {
    account: JSX.Element
    security: JSX.Element
}

export const MeSettingSiderScreen = () => {
    const {t} = useTranslation();
    const location = useLocation()
    const URL = new UrlQuery(location.search)
    const tab = URL.get('tab')
    const _title = t("title.setting");

    const typeBuildJsxs: TMeTypeBuild = {
        account: <MeEditScreen/>,
        security: <MePasswordScreen/>
    }

    const buildInfoTab = useCallback(() => {
        switch (tab) {
            case 'security':
                return typeBuildJsxs.security
            case 'account':
                return typeBuildJsxs.account
            default:
                return <CommonEmptyFC/>
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [URL])

    return (
        <WrapContentWidget
            masterHeader={{
                title: _title,
            }}
            bodyHeader={{
                title: _title,
            }}
        >
            {buildInfoTab()}
        </WrapContentWidget>
    )
}
