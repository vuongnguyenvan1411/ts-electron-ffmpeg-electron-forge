import {useEffect} from "react";
import {Color} from "../../../const/Color";
import {ErrorItemFC} from "../../widgets/ErrorItemFC";
import {SendingStatus} from "../../../const/Events";
import {MeFC} from "../../widgets/MeFC";
import {CommonEmptyFC, CommonLoadingSpinFC} from "../../widgets/CommonFC";
import {MeAction} from "../../../recoil/account/me/MeAction";
import {UserModel} from "../../../models/UserModel";
import {WrapContentWidget} from "../../widgets/WrapContentWidget";

export const MeScreen = () => {
    const {
        vm,
        onLoadMe
    } = MeAction()

    useEffect(() => {
        console.log('%cMount Screen: MeScreen', Color.ConsoleInfo)

        if (!vm.user) {
            onLoadMe()
        }

        return () => {
            console.log('%cUnmount Screen: MeScreen', Color.ConsoleInfo)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <WrapContentWidget
            masterHeader={{
                title: "ATL Account",
                isLoading: vm.isLoading,
                onReload: () => onLoadMe(),
            }}
            bodyHeader={{
                title: "ATL Account",
            }}
        >
            <ErrorItemFC
                status={vm.isLoading}
            >
                {
                    vm.isLoading === SendingStatus.loading
                        ? <CommonLoadingSpinFC/>
                        : vm.user instanceof UserModel
                            ? <div className={`me-screen flex flex-col justify-items-center w-full`}>
                                <MeFC
                                    item={vm.user}
                                />
                            </div>
                            : <CommonEmptyFC/>
                }
            </ErrorItemFC>
        </WrapContentWidget>
    )
}
