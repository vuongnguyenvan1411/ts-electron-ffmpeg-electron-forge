import {Card, Divider, Form, Input, notification} from "antd";
import {SaveOutlined} from "@ant-design/icons";
import {SendingStatus} from "../../../const/Events";
import {useCallback, useEffect} from "react";
import {useTranslation} from "react-i18next";
import {Color} from "../../../const/Color";
import {CommonLoadingSpinFC} from "../../widgets/CommonFC";
import {MeAction} from "../../../recoil/account/me/MeAction";
import {MeEditAction} from "../../../recoil/account/me_edit/MeEditAction";
import {CustomButton} from "../../components/CustomButton";

type TFormFinish = {
    fullName: string
    email: string
    phone: string
    address: string
}

export const MeEditScreen = () => {
    const {t} = useTranslation()

    const {
        vm: vmEdit,
        onUpdateInfo,
        onClearState
    } = MeEditAction()

    const {
        vm: vmMe,
        onLoadMe,
    } = MeAction()

    useEffect(() => {
        console.log('%cMount Screen: MeEditScreen', Color.ConsoleInfo);

        if (!vmMe.user) {
            onLoadMe()
        }

        return () => {
            onClearState()

            console.log('%cUnmount Screen: MeEditScreen', Color.ConsoleInfo);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        if (vmEdit.isUpdating === SendingStatus.success) {
            onNotification();
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vmEdit.isUpdating])

    const onNotification = useCallback(() => {
        notification.success({
            message: t('text.successSetting'),
        })
    }, [t])

    const formItemLayout = {
        labelCol: {span: 5},
        wrapperCol: {span: 12},
    }

    const onFinish = (values: TFormFinish) => {
        onUpdateInfo({
            name: values.fullName,
            email: values.email,
            telephone: values.phone,
            address: values.address,
        })
    }

    return (
        <>
            <div className={"flex justify-center"}>
                <Card
                    className={"card-like-fb flex-grow"}
                >
                    {
                        vmMe.isLoading !== SendingStatus.loading
                            ? <Form
                                onFinish={onFinish}
                                initialValues={
                                    {
                                        "fullName": vmMe.user?.name,
                                        "email": vmMe.user?.email,
                                        "phone": vmMe.user?.telephone,
                                        "address": vmMe.user?.address,
                                    }
                                }
                            >
                                <Form.Item
                                    {...formItemLayout}
                                    name={"fullName"}
                                    label={t("label.fullName")}
                                    labelAlign={"left"}
                                    rules={[
                                        {
                                            required: true,
                                            message: t("error.nameRequired")
                                        },
                                        {
                                            min: 1,
                                            max: 33,
                                            message: t("error.nameMinAndMax")
                                        }
                                    ]}
                                >
                                    <Input placeholder={t("label.fullName")}/>
                                </Form.Item>
                                <Form.Item
                                    {...formItemLayout}
                                    name={"email"}
                                    label={t("label.email")}
                                    labelAlign={"left"}
                                    rules={
                                        [
                                            {
                                                required: true,
                                                type: "email",
                                                message: t("error.emailRequired")
                                            }
                                        ]
                                    }
                                >
                                    <Input placeholder={t("label.email")}/>
                                </Form.Item>
                                <Form.Item
                                    {...formItemLayout}
                                    name={"phone"}
                                    label={t("label.telephone")}
                                    labelAlign={"left"}
                                    rules={
                                        [
                                            {
                                                required: true,
                                                min: 3,
                                                max: 33,
                                                message: t("error.telephoneRequired")
                                            }
                                        ]
                                    }
                                >
                                    <Input placeholder={t("label.telephone")}/>
                                </Form.Item>
                                <Form.Item
                                    {...formItemLayout}
                                    name={"address"}
                                    label={t("label.address")}
                                    labelAlign={"left"}
                                >
                                    <Input placeholder={t("label.address")}/>
                                </Form.Item>
                                <Divider/>
                                <div className={"flex justify-between"}>
                                    <Form.Item
                                        {...formItemLayout}
                                    >
                                        <CustomButton
                                            loading={vmEdit.isUpdating === SendingStatus.loading}
                                            htmlType="submit"
                                        >
                                            <SaveOutlined/>{t('text.updateInfo')}
                                        </CustomButton>
                                    </Form.Item>
                                </div>
                            </Form>
                            : <CommonLoadingSpinFC/>
                    }
                </Card>
            </div>
        </>
    )
}
