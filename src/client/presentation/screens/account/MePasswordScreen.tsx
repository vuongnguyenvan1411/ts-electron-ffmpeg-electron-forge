import {Card, Divider, Form, Input, notification} from "antd";
import {SaveOutlined} from "@ant-design/icons";
import {SendingStatus} from "../../../const/Events";
import {useEffect, useState} from "react";
import {useTranslation} from "react-i18next";
import {Color} from "../../../const/Color";
import {MePasswordAction} from "../../../recoil/account/me_password/MePasswordAction";
import {CustomButton} from "../../components/CustomButton";
import {FieldData, ValidateErrorEntity} from "rc-field-form/lib/interface";

type _TFormFinish = {
    password: string
    current: string
}

export const MePasswordScreen = () => {
    const {t} = useTranslation()
    const [form] = Form.useForm()
    const {
        vm,
        onUpdatePassword,
        onClearState,
        onInitialStatus,
    } = MePasswordAction()

    const isLoading = vm.isUpdating === SendingStatus.loading

    const [clientPasswordError, setClientPasswordError] = useState('')

    useEffect(() => {
        console.log('%cMount Screen: MePasswordScreen', Color.ConsoleInfo)

        return () => {
            onClearState()

            console.log('%cUnmount Screen: MePasswordScreen', Color.ConsoleInfo)
        };

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (vm.isUpdating === SendingStatus.success) {
            notification.success({
                message: t('success.passwordChange'),
            })

            form.resetFields()
        } else if (vm.isUpdating === SendingStatus.error) {
            if (vm.error && vm.error.hasOwnProperty('current')) {
                setClientPasswordError(vm.error.current)
            }

            onInitialStatus()
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.isUpdating])


    const onFinishFailed = (errorInfo: ValidateErrorEntity) => {
        errorInfo.errorFields.forEach((e) => {
            if (e.name[0] === "current") {
                setClientPasswordError(e.errors[0])
            }
        })
    }

    const onFieldsChange = (data: FieldData[]) => {
        data.forEach((e: any) => {
            if (e.name[0] === "current") {
                setClientPasswordError(e.errors.length > 0 ? e.errors[0] : '')
            }
        })
    }

    const formItemLayout = {
        labelCol: {span: 5},
        wrapperCol: {span: 12},
    }

    const onFinish = (values: _TFormFinish) => {
        onUpdatePassword({
            password: values.password,
            current: values.current
        })
    }

    return (
        <div className={"flex justify-center"}>
            <Card className={"card-like-fb flex-grow"}>
                <Form
                    form={form}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    onFieldsChange={onFieldsChange}
                >
                    <Form.Item
                        {...formItemLayout}
                        labelAlign={"left"}
                        name="current"
                        label={t("label.passwordCurrent")}
                        rules={[
                            {
                                required: true,
                                message: t("validation.passwordRequired"),
                            },
                            {
                                min: 6,
                                message: t("validation.password"),
                            },
                            {
                                max: 30,
                                message: t('error.passwordMax')
                            }
                        ]}
                        hasFeedback
                        validateStatus={clientPasswordError.length > 0 ? 'error' : ''}
                        help={clientPasswordError.length > 0 ? clientPasswordError : null}
                    >
                        <Input.Password/>
                    </Form.Item>
                    <Divider/>
                    <Form.Item
                        {...formItemLayout}
                        labelAlign={"left"}
                        name="password"
                        label={t("label.password")}
                        rules={[
                            {
                                required: true,
                                message: t("validation.passwordRequired"),
                            },
                            {
                                min: 6,
                                message: t("validation.password"),
                            },
                            {
                                max: 30,
                                message: t('error.passwordMax')
                            },
                            ({getFieldValue}) => ({
                                validator(_, value) {
                                    if (!value || getFieldValue('current') !== value) {
                                        return Promise.resolve()
                                    }

                                    return Promise.reject(new Error(t("error.samePassword")))
                                },
                            })
                        ]}
                        hasFeedback
                    >
                        <Input.Password/>
                    </Form.Item>
                    <Form.Item
                        {...formItemLayout}
                        labelAlign={"left"}
                        name="confirm"
                        label={t("label.confirmPassword")}
                        dependencies={['password']}
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: t("validation.confirmPasswordRequired"),
                            },
                            ({getFieldValue}) => ({
                                validator(_, value) {
                                    if (!value || getFieldValue('password') === value) {
                                        return Promise.resolve()
                                    }

                                    return Promise.reject(new Error(t("validation.repeatPassword")))
                                },
                            })
                        ]}
                    >
                        <Input.Password/>
                    </Form.Item>
                    <Divider/>
                    <div className={"flex justify-between"}>
                        <Form.Item
                            {...formItemLayout}
                        >
                            <CustomButton
                                loading={isLoading}
                                htmlType="submit"
                            >
                                <SaveOutlined/>{t('button.change')}
                            </CustomButton>
                        </Form.Item>
                    </div>
                </Form>
            </Card>
        </div>
    )
}
