import {useTranslation} from "react-i18next";
import {useCallback, useEffect, useState} from "react";
import {Color} from "../../../../const/Color";
import {WrapContentWidget} from "../../../widgets/WrapContentWidget";
import {useLocation} from "react-router";
import {UrlQuery} from "../../../../core/UrlQuery";
import {IWeighingFilterVO, WeighingsAction} from "../../../../recoil/service/weighing/weighings/WeighingsAction";
import {Utils} from "../../../../core/Utils";
import {MediaQuery} from "../../../../core/MediaQuery";
import {Style} from "../../../../const/Style";
import styles from "../../../../styles/module/BlogPost.module.scss";

export function WeighingScreen() {
    const {t} = useTranslation()
    let location = useLocation()
    const URL = new UrlQuery(location.search)
    const {vm, onLoadItems} = WeighingsAction()

    const filter = URL.get('filter', {})
    const sort = URL.get('sort')
    const order = URL.get('order')
    const page = URL.getInt('page', vm.query.page)
    const limit = URL.getInt('limit', vm.query.limit)
    const _title = t('title.weighing');

    const [queryParams, setQueryParams] = useState<IWeighingFilterVO>({
        filter: filter,
        sort: sort,
        order: order,
        page: page,
        limit: limit,
    });

    useEffect(() => {
        console.log('%cMount Screen: WeighingScreen', Color.ConsoleInfo);

        if (
            vm.items.length === 0
            || (vm.timestamp !== undefined && Utils.checkHourState(vm.timestamp))
        ) {
            const urlQueryParams = new UrlQuery(queryParams)
            urlQueryParams.set('limit', (new MediaQuery(Style.GridLimit)).getPoint(limit))

            setQueryParams(urlQueryParams.toObject())
            onLoadItems(urlQueryParams.toObject())
        }

        return () => {
            console.log('%cUnmount Screen: WeighingScreen', Color.ConsoleInfo)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const _build = useCallback(() => {
        return vm.infoEmptyModel && vm.infoEmptyModel.description
            ? <div className={"container mx-auto"}>
                <div
                    className={styles.BlogPostDescription}
                    dangerouslySetInnerHTML={{__html: vm.infoEmptyModel.description}}
                />
            </div>
            : <div>Weighing</div>

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.items, vm.infoEmptyModel])

    return (
        <WrapContentWidget
            masterHeader={{
                title: _title,
            }}
            bodyHeader={{
                title: _title,
            }}
        >
            {_build()}
        </WrapContentWidget>
    )
}
