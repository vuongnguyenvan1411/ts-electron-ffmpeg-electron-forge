import {useTranslation} from "react-i18next";
import {useEffect, useRef, useState} from "react";
import {Form, FormInstance, Input} from "antd";
import {Color} from "../../../../const/Color";
import {Vr360ItemFC, Vr360Skeleton} from "../../../widgets/Vr360ItemFC";
import {SendingStatus} from "../../../../const/Events";
import {Style} from "../../../../const/Style";
import {useLocation, useNavigate} from "react-router";
import {UrlQuery} from "../../../../core/UrlQuery";
import {debounce} from "lodash";
import {ErrorItemFC} from "../../../widgets/ErrorItemFC";
import {App} from "../../../../const/App";
import {Vr360FilterVO} from "../../../../models/Vr360Model";
import {MediaQuery} from "../../../../core/MediaQuery";
import {CommonEmptyFC, CommonTagFilterFC} from "../../../widgets/CommonFC";
import {Utils} from "../../../../core/Utils";
import {Vr360sAction} from "../../../../recoil/service/vr360/vr360s/Vr360sAction";
import SearchIcon from "../../../components/icons/SearchIcon";
import {WrapContentWidget} from "../../../widgets/WrapContentWidget";
import CustomPagination from "../../../components/CustomPagination";
import {GridLoadingWidget} from "../../../widgets/GridLoadingWidget";

export const Vr360Screen = () => {
    const {t} = useTranslation()
    const navigate = useNavigate()
    const location = useLocation()
    const [form] = Form.useForm();
    const URL = new UrlQuery(location.search)

    const {
        vm,
        onLoadItems,
        onCacheItems,
    } = Vr360sAction()

    const filter = URL.get('filter', {})
    const sort = URL.get('sort')
    const order = URL.get('order')
    const page = URL.getInt('page', vm.query.page)
    const limit = URL.getInt('limit', vm.query.limit)

    const [queryParams, setQueryParams] = useState<Vr360FilterVO>({
        filter: filter,
        sort: sort,
        order: order,
        page: page,
        limit: limit,
    });

    const _buildForm = (form: FormInstance<any>) => {
        return <Form form={form}>
            <Form.Item
                className={"w-52 md:w-[360px]"}
                style={{
                    marginBottom: "0px",
                }}
                name={"search"}
            >
                <Input
                    className={"input-under-line-main"}
                    size="small"
                    placeholder={t('text.search')}
                    allowClear
                    onChange={onChangeSearch}
                    prefix={<SearchIcon/>}
                />
            </Form.Item>
        </Form>;
    }

    const _title = t('title.vr360');

    useEffect(() => {
        console.log('%cMount Screen: Vr360Screen', Color.ConsoleInfo);

        if (
            vm.items.length === 0
            || (vm.timestamp !== undefined && Utils.checkHourState(vm.timestamp))
        ) {
            const urlQueryParams = new UrlQuery(queryParams);
            urlQueryParams.set('limit', (new MediaQuery(Style.GridLimit)).getPoint(limit));

            onLoadItems(urlQueryParams.toObject());
        }

        return () => {
            console.log('%cUnmount Screen: Vr360Screen', Color.ConsoleInfo);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        const urlQueryParams = new UrlQuery(queryParams);
        urlQueryParams.set('limit', vm.query.limit);

        setQueryParams(urlQueryParams.toObject());
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.query])

    const onChangePage = (page: number) => {
        const urlQueryParams = new UrlQuery(queryParams);
        urlQueryParams.set('page', page);

        setQueryParams(urlQueryParams.toObject());
        onLoadItems(urlQueryParams.toObject());

        navigate({
            search: urlQueryParams.toString()
        }, {
            replace: true
        })
    }

    const onChangeSearch = (e: any) => setDebounceSearch.current(e.target.value);

    const setDebounceSearch = useRef(debounce((value: string) => {
        const urlQueryParams = new UrlQuery(queryParams);

        if (value.length > 0) {
            urlQueryParams.set('filter.q', value);
        } else {
            urlQueryParams.delete('filter.q');
        }
        urlQueryParams.set('page', 1);

        onCacheItems(urlQueryParams.toObject());

        navigate({
            search: urlQueryParams.toString()
        }, {
            replace: true
        })

        setQueryParams(urlQueryParams.toObject());
    }, App.DelaySearch))

    const isTags = (typeof queryParams.filter?.q !== "undefined" && queryParams.filter?.q?.length > 0);

    const handleCloseTag = (name: string) => {
        const urlQueryParams = new UrlQuery(queryParams);
        urlQueryParams.delete(name);
        urlQueryParams.set('page', 1);

        navigate({
            search: urlQueryParams.toString()
        }, {
            replace: true
        })

        setQueryParams(urlQueryParams.toObject());

        if (name === "filter.q") {
            form.resetFields();
        }

        onLoadItems(urlQueryParams.toObject());
    }

    const onClickReload = () => {
        const urlQueryParams = new UrlQuery(queryParams);

        onLoadItems(urlQueryParams.toObject());
    }

    return (
        <WrapContentWidget
            masterHeader={
                {
                    title: _title,
                    isLoading: vm.isLoading,
                    onReload: onClickReload,
                }
            }
            bodyHeader={
                {
                    title: t('title.vr360'),
                    right: _buildForm(form),
                }
            }
        >
            <CommonTagFilterFC
                is={isTags}
                onClose={handleCloseTag}
                data={queryParams}
                items={[
                    {
                        key: 'filter.q',
                        label: t('text.name')
                    }
                ]}
            />
            <ErrorItemFC
                status={vm.isLoading}
            >
                {

                    (vm.items.length > 0 && vm.isLoading === SendingStatus.success) || (vm.isLoading === SendingStatus.loading)
                        ? <div
                            className={"grid grid-cols-1 gap-4 sm:grid-cols-2 md:grid-cols-3 md:gap-6 xxl:grid-cols-4 3xl:gap-8 "}>
                            {
                                vm.isLoading !== SendingStatus.loading ?
                                    vm.items.slice((page - 1) * vm.query.limit, page * vm.query.limit).map((item, index) => {
                                        return <Vr360ItemFC key={index.toString()} item={item}/>
                                    })
                                    :
                                    <GridLoadingWidget>
                                        <Vr360Skeleton/>
                                    </GridLoadingWidget>
                            }
                        </div>
                        : <CommonEmptyFC/>
                }
            </ErrorItemFC>
            <CustomPagination
                isShow={vm.isLoading !== SendingStatus.loading && vm.items.length > 0 && vm.oMeta && vm.oMeta.pageCount > 1}
                defaultCurrent={page}
                current={page}
                total={vm.oMeta?.totalCount}
                defaultPageSize={vm.query.limit}
                onChange={onChangePage}
            />
        </WrapContentWidget>
    )
}
