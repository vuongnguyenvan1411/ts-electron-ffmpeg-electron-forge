import {lazy, Suspense, useEffect} from "react";
import {Color} from "../../../../const/Color";
import {Navigate, useParams} from "react-router-dom";
import {CHashids} from "../../../../core/CHashids";
import {useLocation, useNavigate} from "react-router";
import {ArrowLeftOutlined} from "@ant-design/icons";
import {RouteConfig} from "../../../../config/RouteConfig";
import {CommonLoadingSpinFC} from "../../../widgets/CommonFC";
import ErrorBoundaryComponent from "../../../widgets/ErrorBoundaryComponent";
import {ContributorFC} from "../../../widgets/ContributorFC";
import {RouteAction} from "../../../../const/RouteAction";
import {CustomTypography} from "../../../components/CustomTypography";
import {CustomButton} from "../../../components/CustomButton";
import NextImage from "next/image";
import {Images} from "../../../../const/Images";
import {Tooltip} from "antd";
import {useTranslation} from "react-i18next";
import {useConfigContext} from "../../../contexts/ConfigContext";
import {FeatureSingleton} from "../../../../models/FeatureSingleton";

const EmbedPano = lazy(() => import("../../../modules/vr360/EmbedPano"));

type TParamState = {
    name: string;
    link: string;
    createdAt: number;
}

export const Vr360ViewScreen = (props: {
    state?: TParamState
}) => {
    const {t} = useTranslation()
    const location = useLocation()
    const navigate = useNavigate()
    const [config] = useConfigContext()
    const {hash} = useParams<{ hash: string }>()
    const params = (location.state ?? props.state) as TParamState
    const vr360Id = CHashids.decodeGetFirst(hash!)


    useEffect(() => {
        console.log('%cMount Screen: Vr360ViewScreen', Color.ConsoleInfo);

        if (params !== undefined) {
            document.title = params.name;
        }

        return () => {
            console.log('%cUnmount Screen: Vr360ViewScreen', Color.ConsoleInfo);
        };

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        params && vr360Id !== null
            ? <>
                <ErrorBoundaryComponent>
                    <Suspense fallback={<CommonLoadingSpinFC/>}>
                        <EmbedPano
                            vr360Id={vr360Id}
                            name={params.name}
                            createdAt={params.createdAt}
                        />
                    </Suspense>
                </ErrorBoundaryComponent>
                <div
                    className={"absolute z-50 top-2 left-2"}
                >
                    {
                        config.agent?.isWebApp()
                            ? <div className={"flex flex-col gap-2 items-start"}>
                                <div className={"flex gap-3"}>
                                    <CustomButton
                                        icon={<ArrowLeftOutlined/>}
                                        onClick={() => navigate(RouteAction.GoBack())}
                                    />
                                    <CustomTypography textStyle={"text-16-24"}>
                                        {params.name}
                                    </CustomTypography>
                                </div>
                                <Tooltip
                                    title={t("button.share")}
                                >
                                    <div>
                                        <CustomButton
                                            icon={
                                                <NextImage
                                                    src={Images.iconShareNetwork.data}
                                                    alt={Images.iconShareNetwork.atl}
                                                />

                                            }
                                            onClick={() => navigate(`${RouteConfig.VR360}/setting/${hash}`, {
                                                state: params,
                                            })}
                                        />
                                    </div>
                                </Tooltip>
                            </div>
                            : null
                    }
                </div>
                {
                    FeatureSingleton.getInstance().isContributor ? <ContributorFC/> : null
                }
            </>
            : <Navigate to={RouteConfig.VR360}/>
    );
}
