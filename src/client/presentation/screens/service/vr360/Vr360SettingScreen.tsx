import {useParams} from "react-router-dom";
import {useTranslation} from "react-i18next";
import React, {useEffect, useState} from "react";
import {Color} from "../../../../const/Color";
import {CHashids} from "../../../../core/CHashids";
import {Alert, DatePicker, Divider, Form, Input, Modal, notification, Space, Switch, Typography} from "antd";
import {SendingStatus} from "../../../../const/Events";
import {CloseOutlined, EditOutlined, SaveOutlined, SettingOutlined} from "@ant-design/icons";
import {Utils} from "../../../../core/Utils";
import {useLocation, useNavigate} from "react-router";
import {App} from "../../../../const/App";
import {RouteAction} from "../../../../const/RouteAction";
import {BaseShareModel, TBaseShareVO, TimelapseShareModel} from "../../../../models/ShareModel";
import moment from "moment";
import {Vr360SettingAction} from "../../../../recoil/service/vr360/vr360_setting/Vr360SettingAction";
import {ShareScreen} from "../../common/ShareScreen";
import {UrlQuery} from "../../../../core/UrlQuery";
import {CustomButton} from "../../../components/CustomButton";
import {MediaQuery} from "../../../../core/MediaQuery";
import {Style} from "../../../../const/Style";
import {WrapContentWidget} from "../../../widgets/WrapContentWidget";

type TModalFormMainProps = {
    vr360Id: number | null
    sid?: string
    visible: boolean
    preview: boolean
    onCancel: () => void
    initFormValues?: BaseShareModel
}

type TParamState = {
    name: string;
}

export const Vr360SettingScreen = (props: {
    state?: TParamState
}) => {
    const {t} = useTranslation()
    const navigate = useNavigate()
    const location = useLocation()
    const {hash} = useParams<{ hash: string }>()
    const params = (location.state ?? props.state) as TParamState
    const vr360Id = CHashids.decodeGetFirst(hash!);
    const URL = new UrlQuery(location.search)

    if (vr360Id === null) {
        navigate(RouteAction.GoBack())
    }

    const {
        vm,
        onLoadItems,
        onDeleteItem,
        resetStateWithEffect,
    } = Vr360SettingAction();

    const page = URL.getInt('page', vm.query.page);
    const limit = URL.getInt('limit', vm.query.limit);
    const [queryParams, setQueryParams] = useState<{ page: number, limit: number }>({
        page: page,
        limit: limit,
    });

    const title = t('text.setting');

    const [visible, setVisible] = useState<{
        sid?: string,
        visible: boolean,
        preview: boolean,
        initValues?: BaseShareModel,
    }>({
        visible: false,
        preview: false,
    })

    const onEdit = (item: TimelapseShareModel, _: number) => {
        setVisible({
            ...visible,
            sid: item.sid,
            visible: true,
            preview: false,
            initValues: item,
        })
    }

    const onPreview = (item: TimelapseShareModel, _: number) => {
        setVisible({
            ...visible,
            sid: item.sid,
            visible: true,
            preview: true,
            initValues: item,
        })
    }

    const showFormModal = (sid?: string) => {
        setVisible({
            ...visible,
            sid: sid,
            visible: true,
            preview: false,
            initValues: undefined,
        })
    };

    const hideUserModal = () => {
        setVisible({
            ...visible,
            visible: false,
            preview: false,
        })
    };

    const onInit = () => {
        if (vr360Id) {
            const urlQueryParams = new UrlQuery(queryParams);
            const mediaQuery = new MediaQuery(Style.GridLimit);
            const mediaLimit = mediaQuery.getPoint(limit);
            urlQueryParams.set('limit', mediaLimit);
            setQueryParams(urlQueryParams.toObject());
            onLoadItems(vr360Id!, urlQueryParams.toObject());
        }
    }

    useEffect(() => {
        console.log('%cMount Screen: Vr360SettingScreen', Color.ConsoleInfo);

        if (
            vm.items.length === 0
            || vm.key !== vr360Id!.toString()
            || (vm.timestamp !== undefined && Utils.checkHourState(vm.timestamp))
        ) {
            if (vm.items.length > 0) resetStateWithEffect()
            onInit()
        }

        return () => {
            console.log('%cUnmount Screen: Vr360SettingScreen', Color.ConsoleInfo);
        };

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (vm.isUpdating === SendingStatus.success) {
            notification.success({
                message: t('text.successSetting'),
            });
            hideUserModal()
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.isUpdating]);


    const onDelete = (sid: string) => {
        if (vr360Id) {
            onDeleteItem(vr360Id, sid)
        }
    }

    const onLoadMore = () => {
        if (vr360Id) {
            const urlQueryParams = new UrlQuery(queryParams);
            urlQueryParams.set('page', (vm.items.length / vm.query.limit) + 1);
            setQueryParams(urlQueryParams.toObject());
            onLoadItems(vr360Id!, urlQueryParams.toObject());
        }
    }

    const isUpdatingError = vm.isUpdating === SendingStatus.error;

    return (
        <WrapContentWidget
            masterHeader={{
                title: title,
                isLoading: vm.isLoading,
                onReload: onInit,
            }}
            bodyHeader={{
                title: params.name ?? title,
                right: <CustomButton
                    onClick={() => {
                        showFormModal();
                    }}
                >
                    {t("text.addSharingLink")}
                </CustomButton>,
            }}
        >
            {
                isUpdatingError && vm.error && vm.error.hasOwnProperty('warning')
                    ? <Alert className={"mb-5"} message={vm.error['warning']} type="error" showIcon/>
                    : null
            }
            <ShareScreen
                title={params.name ?? ''}
                status={vm.isLoading}
                items={vm.items ?? []}
                isNextAvailable={vm.oMeta?.nextPage !== undefined}
                onDelete={onDelete}
                deleteStatus={vm.isDeleting}
                onEdit={onEdit}
                onView={onPreview}
                onLoadMore={onLoadMore}
            />
            <ModalFormMain
                key={"form"}
                vr360Id={vr360Id}
                sid={visible.sid}
                visible={visible.visible}
                onCancel={hideUserModal}
                initFormValues={visible.initValues}
                preview={visible.preview}
            />
        </WrapContentWidget>
    );
}

const ModalFormMain: React.FC<TModalFormMainProps> = ({
                                                          preview,
                                                          vr360Id,
                                                          sid,
                                                          visible,
                                                          onCancel,
                                                          initFormValues
                                                      }) => {
    const {t} = useTranslation();
    const [formMain] = Form.useForm();
    const [isPreview, setIsPreview] = useState(false);

    useEffect(() => {
        setIsPreview(preview);
    }, [preview])

    const {
        vm,
        onAddItem,
        onEditItem,
    } = Vr360SettingAction();

    const onAfterClose = () => {
        formMain.resetFields();
    }

    const onOk = () => {
        formMain.submit();
    };

    const onChangeToEdit = () => {
        setIsPreview(false);
    }

    const onFinish = (values: any) => {
        const data: TBaseShareVO = {
            name: values.name,
            password: values.password,
            dateStart: values['range-time-picker'] && values['range-time-picker'][0] ? moment(values['range-time-picker'][0]).format('YYYY-MM-DD HH:mm:ss').toString() : '',
            dateEnd: values['range-time-picker'] && values['range-time-picker'][1] ? moment(values['range-time-picker'][1]).format('YYYY-MM-DD HH:mm:ss').toString() : '',
            status: values.isShare,
            sInfo: values.shareInfo,
        }

        if (vr360Id) {
            if (sid) {
                onEditItem(vr360Id, sid, data);
            } else {
                onAddItem(vr360Id, data);
            }
        }
    };

    return (
        <Modal
            title={isPreview ? t("text.detailInfo") : !sid ? t("text.createLink") : t("text.editLink")}
            visible={visible}
            onOk={onOk}
            onCancel={onCancel}
            afterClose={onAfterClose}
            destroyOnClose={true}
            confirmLoading={vm.isLoading === SendingStatus.loading}
            width={1000}
            footer={
                isPreview
                    ? [
                        <CustomButton
                            key={"edit"}
                            onClick={onChangeToEdit}
                            icon={<EditOutlined/>}
                        >
                            {t("button.edit")}
                        </CustomButton>,
                        <CustomButton
                            key={"close"}
                            onClick={onCancel}
                            icon={<CloseOutlined/>}
                        >
                            {t("button.close")}
                        </CustomButton>
                    ]
                    : [
                        <CustomButton
                            key={"close"}
                            onClick={onCancel}
                            icon={<CloseOutlined/>}
                        >
                            {t("button.close")}
                        </CustomButton>,
                        <CustomButton
                            key={"save"}
                            onClick={() => onOk()}
                            icon={<SaveOutlined/>}
                            loading={vm.isUpdating === SendingStatus.loading}
                        >
                            {sid ? t("button.save") : t("button.saveNew")}
                        </CustomButton>
                    ]
            }
        >
            <Form
                form={formMain}
                labelCol={{
                    xs: {span: 24},
                    sm: {span: 8},
                    xxl: {span: 10},
                    xl: {span: 10},
                }}
                wrapperCol={{
                    xs: {span: 24},
                    sm: {span: 16},
                    xxl: {span: 14},
                    xl: {span: 14},
                }}
                onFinish={onFinish}
            >
                <Form.Item
                    labelAlign={"left"}
                    key={"k_name"}
                    label={t("text.name")}
                    name={"name"}
                    rules={[
                        {
                            required: true,
                            message: t("validation.emptyData"),
                        },
                        {
                            max: 100,
                            message: t('validation.minAndMaxCharacter', {
                                label: t("text.name"),
                                min: '1',
                                max: '100'
                            }),
                        }
                    ]}
                    initialValue={initFormValues?.name}
                >
                    <Input disabled={isPreview}/>
                </Form.Item>
                <Form.Item
                    labelAlign={"left"}
                    key={"k_password"}
                    label={t("label.password")}
                    name={"password"}
                    initialValue={initFormValues?.password}
                >
                    <Input disabled={isPreview}/>
                </Form.Item>
                <Form.Item
                    labelAlign={"left"}
                    key={"k_range-time-picker"}
                    name="range-time-picker"
                    label={t("text.activeTime")}
                    {...(initFormValues?.expiryDate?.start || initFormValues?.expiryDate?.end
                            ? {
                                initialValue: [
                                    initFormValues?.expiryDate.start ? moment(initFormValues.expiryDate.start, App.FormatISOFromMoment) : null,
                                    initFormValues?.expiryDate.end ? moment(initFormValues.expiryDate.end, App.FormatISOFromMoment) : undefined
                                ]
                            }
                            : null
                    )}
                >
                    <DatePicker.RangePicker
                        allowEmpty={[true, true]}
                        disabled={isPreview}
                        showTime
                        format={t("format.dateTime")}
                    />
                </Form.Item>
                <Divider/>
                <Form.Item>
                    <Space>
                        <SettingOutlined/>
                        <Typography.Text>
                            {t("title.setUpBasicPermissions")}
                        </Typography.Text>
                    </Space>
                </Form.Item>
                <Form.Item
                    key={"isShare"}
                    label={t('text.sharingAllow')}
                    labelAlign={'left'}
                    name={"isShare"}
                    valuePropName={"checked"}
                    initialValue={initFormValues?.status ?? false}
                >
                    <Switch
                        disabled={isPreview}
                        defaultChecked={false}
                    />
                </Form.Item>
                <Form.Item
                    key={"shareInfo"}
                    label={t('text.displayInformation')}
                    labelAlign={'left'}
                    name={"shareInfo"}
                    valuePropName={"checked"}
                    initialValue={initFormValues?.sInfo ?? false}
                >
                    <Switch
                        disabled={isPreview}
                        defaultChecked={false}
                    />
                </Form.Item>
            </Form>
        </Modal>
    )
}
