import {Alert, Button, Card, Col, DatePicker, Divider, Form, Input, Modal, notification, Radio, Row, Space, Switch, Table, Tabs, Tag, Typography} from "antd";
import {CloseOutlined, EditOutlined, PlusCircleOutlined, SaveOutlined, SettingOutlined} from "@ant-design/icons";
import React, {useEffect, useState} from "react";
import {useTranslation} from "react-i18next";
import {useParams} from "react-router-dom";
import {CHashids} from "../../../../core/CHashids";
import {useLocation, useNavigate} from "react-router";
import {Color} from "../../../../const/Color";
import {SendingStatus} from "../../../../const/Events";
import {Utils} from "../../../../core/Utils";
import {App} from "../../../../const/App";
import {RouteAction} from "../../../../const/RouteAction";
import {Map2DShareModel, Map3DShareModel, SpaceShareModel, TConfigMapShare, TMapPreviewShare, TSpaceShareV0} from "../../../../models/ShareModel";
import {ColumnsType} from "antd/es/table";
import moment from "moment";
import {TM2d, TM3d, TVr360} from "../../../../models/service/geodetic/SpaceModel";
import {GeodeticInfoAction} from "../../../../recoil/service/geodetic/geodetic_info/GeodeticInfoAction";
import {GeodeticSettingAction} from "../../../../recoil/service/geodetic/geodetic_setting/GeodeticSettingAction";
import {Map2DShareAction} from "../../../../recoil/service/geodetic/map_2d_share/Map2DShareAction";
import {Map3DShareAction} from "../../../../recoil/service/geodetic/map_3d_share/Map3DShareAction";
import {Map360ShareAction} from "../../../../recoil/service/geodetic/map_360_share/Map360ShareAction";
import {CustomButton} from "../../../components/CustomButton";
import {ShareScreen} from "../../common/ShareScreen";
import {UrlQuery} from "../../../../core/UrlQuery";
import {MediaQuery} from "../../../../core/MediaQuery";
import {Style} from "../../../../const/Style";
import {WrapContentWidget} from "../../../widgets/WrapContentWidget";

interface ModalFormMainProps {
    spaceId: number | null,
    shareId?: string,
    visible: boolean,
    preview: boolean,
    onCancel: () => void,
    initFormValues?: SpaceShareModel,
    m2d?: TM2d[],
    m3d?: TM3d[],
    vr360?: TVr360[],
}

interface ModalFormProps {
    id: number,
    name: string,
    visible: boolean,
    onCancel: () => void,
    initFromValues?: TConfigMapShare[],
}

export type TParamState = {
    name: string;
}

export const GeodeticSettingScreen = (props: {
    state?: TParamState
}) => {
    const {t} = useTranslation()
    const navigate = useNavigate()
    const location = useLocation()
    const {hash} = useParams<{ hash: string }>()
    const params = (location.state ?? props.state) as TParamState
    const spaceId = CHashids.decodeGetFirst(hash!)
    const URL = new UrlQuery(location.search)

    const {
        vm,
        onDeleteItem,
        onLoadItems,
        resetStateWithEffect,
    } = GeodeticSettingAction()

    if (spaceId === null) {
        navigate(RouteAction.GoBack())
    }

    const page = URL.getInt('page', vm.query.page);
    const limit = URL.getInt('limit', vm.query.limit);
    const [queryParams, setQueryParams] = useState<{ page: number, limit: number }>({
        page: page,
        limit: limit,
    });

    const {
        vm: vmInfo,
        onLoadItem: onLoadInfo,
    } = GeodeticInfoAction()

    const title = params !== undefined ? params.name : t('text.setting');

    const [isModalShareVisible, setIsModalShareVisible] = useState<{
        shareId?: string,
        visible: boolean,
        preview: boolean,
        initValues?: SpaceShareModel,
    }>({
        visible: false,
        preview: false,
    });

    const [visibleDelete, setVisibleDelete] = useState<{
        shareId?: string,
        visible: boolean,
    }>({
        visible: false,
    })

    const onEdit = (item: SpaceShareModel, _: number) => {
        setIsModalShareVisible({
            ...isModalShareVisible,
            shareId: item.sid,
            visible: true,
            preview: false,
            initValues: item,
        })
    }

    const onPreview = (item: SpaceShareModel, _: number) => {
        setIsModalShareVisible({
            ...isModalShareVisible,
            shareId: item.sid,
            visible: true,
            preview: true,
            initValues: item,
        })
    }

    const onInit = () => {
        if (spaceId) {
            const urlQueryParams = new UrlQuery(queryParams);
            const mediaQuery = new MediaQuery(Style.GridLimit);
            const mediaLimit = mediaQuery.getPoint(limit);
            urlQueryParams.set('limit', mediaLimit);
            setQueryParams(urlQueryParams.toObject());
            onLoadItems(spaceId, queryParams);
            onLoadInfo(spaceId)
        }
    }

    useEffect(() => {
        console.log('%cMount Screen: GeodeticSettingScreen', Color.ConsoleInfo);

        if (
            vm.items.length === 0
            || vm.key !== spaceId!.toString()
            || (vm.timestamp !== undefined && Utils.checkHourState(vm.timestamp))
        ) {
            if (vm.items.length > 0) resetStateWithEffect()
            onInit();
        }

        return () => {
            console.log('%cUnmount Screen: GeodeticSettingScreen', Color.ConsoleInfo)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const showUserModal = (shareId?: string) => {
        setIsModalShareVisible({
            ...isModalShareVisible,
            shareId: shareId,
            visible: true,
            preview: false,
            initValues: undefined,
        })
    };

    const hideUserModal = () => {
        setIsModalShareVisible({
            ...isModalShareVisible,
            visible: false,
            preview: false,
        })
    };

    useEffect(() => {
        if (vm.isUpdating === SendingStatus.success) {
            setIsModalShareVisible(
                {
                    ...isModalShareVisible,
                    visible: false,
                }
            )
            notification.success({
                message: t('text.successSetting')
            })
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.isUpdating]);

    useEffect(() => {
        if (vm.isDeleting === SendingStatus.success) {
            setVisibleDelete(
                {
                    ...visibleDelete,
                    visible: false,
                }
            )

            notification.success({
                message: t('success.delete'),
            });
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.isDeleting]);

    useEffect(() => {
        if (vmInfo.item) {
            navigate({}, {
                replace: true,
                state: {
                    name: params.name,
                    m2d: vmInfo.item?.m2d,
                    m3d: vmInfo.item?.m3d,
                    vr360: vmInfo.item?.vr360,
                }
            });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vmInfo.item])

    const isUpdatingError = vm.isUpdating === SendingStatus.error;

    const onDelete = (sid: string) => {
        if (spaceId) {
            onDeleteItem(spaceId, sid)
        }
    }

    const onLoadMore = () => {
        if (spaceId) {
            const urlQueryParams = new UrlQuery(queryParams);
            urlQueryParams.set('page', (vm.items.length / vm.query.limit) + 1);
            setQueryParams(urlQueryParams.toObject());
            onLoadItems(spaceId, urlQueryParams.toObject());
        }
    }

    const onCLickReload = () => {
        if (spaceId) {
            const urlQueryParams = new UrlQuery(queryParams);
            onLoadItems(spaceId, urlQueryParams.toObject());
        }
    }

    return (
        <WrapContentWidget
            bodyHeader={{
                title: title,
                right: <CustomButton
                    key={"btn_show_modal"}
                    size={"small"}
                    onClick={() => {
                        showUserModal();
                    }}
                    icon={<PlusCircleOutlined/>}
                >
                    {t("text.addSharingLink")}
                </CustomButton>,
            }}
            masterHeader={{
                title: title,
                isLoading: vm.isLoading,
                onReload: onCLickReload,
            }}
        >
            {
                isUpdatingError && vm.error && vm.error.hasOwnProperty('warning')
                    ? <Alert className={"mb-5"} message={vm.error['warning']} type="error" showIcon/>
                    : null
            }
            <ShareScreen
                title={params.name ?? ''}
                status={vm.isLoading}
                items={vm.items ?? []}
                isNextAvailable={vm.oMeta?.nextPage !== undefined}
                onDelete={onDelete}
                deleteStatus={vm.isDeleting}
                onEdit={onEdit}
                onView={onPreview}
                onLoadMore={onLoadMore}
            />
            <ModalShareSpace
                key={"modal_type_add_edit_view_space_share"}
                spaceId={spaceId}
                shareId={isModalShareVisible.shareId}
                visible={isModalShareVisible.visible}
                onCancel={hideUserModal}
                initFormValues={isModalShareVisible.initValues}
                preview={isModalShareVisible.preview}
                m2d={vmInfo.item?.m2d}
                m3d={vmInfo.item?.m3d}
                vr360={vmInfo.item?.vr360}
            />
        </WrapContentWidget>
    )
}

export const ModalShareSpace: React.FC<ModalFormMainProps> = ({
                                                                  spaceId,
                                                                  preview,
                                                                  shareId,
                                                                  visible,
                                                                  onCancel,
                                                                  initFormValues,
                                                                  m2d,
                                                                  m3d,
                                                                  vr360,
                                                              }) => {
    const {t} = useTranslation();
    const [formMain] = Form.useForm();
    const [isPreview, setIsPreview] = useState(false);

    const formItemLayout = {
        labelCol: {
            xs: {span: 24},
            sm: {span: 8},
            xxl: {span: 10},
            xl: {span: 10},
        },
        wrapperCol: {
            xs: {span: 24},
            sm: {span: 16},
            xxl: {span: 14},
            xl: {span: 14},
        },
    };

    const {
        vm,
        onAddItem,
        onEditItem,
    } = GeodeticSettingAction();

    const onAfterClose = () => {
        formMain.resetFields();
    }

    const onOk = () => {
        formMain.submit();
    };

    useEffect(() => {
        setIsPreview(preview);
    }, [preview]);

    const [visibleModal2D, setVisibleModal2D] = useState<{
        id: number,
        visible: boolean,
        name: string,
    }>({
        id: 0,
        visible: false,
        name: '',
    });
    const [visibleModal3D, setVisibleModal3D] = useState<{
        id: number,
        visible: boolean,
        name: string,
    }>({
        id: 0,
        visible: false,
        name: '',
    });
    const [visibleModalVr360, setVisibleModalVr360] = useState<{
        id: number,
        visible: boolean,
        name: string,
    }>({
        id: 0,
        visible: false,
        name: '',
    });

    const showUserModal2D = (id: number, name: string) => {
        setVisibleModal2D({
            ...visibleModal2D,
            id: id,
            visible: true,
            name: name,
        })
    };

    const hideUserModal2D = () => {
        setVisibleModal2D({
            ...visibleModal2D,
            visible: false,
        })
    };
    const showUserModal3D = (id: number, name: string) => {
        setVisibleModal3D({
            ...visibleModal3D,
            id: id,
            visible: true,
            name: name,
        })
    };

    const hideUserModal3D = () => {
        setVisibleModal3D({
            ...visibleModal3D,
            visible: false,
        })
    };
    const showUserModalVr360 = (id: number, name: string) => {
        setVisibleModalVr360({
            ...visibleModalVr360,
            id: id,
            visible: true,
            name: name,
        })
    };

    const hideUserModalVr360 = () => {
        setVisibleModalVr360({
            ...visibleModalVr360,
            visible: false,
        })
    };

    const onChangeToEdit = () => {
        setIsPreview(false);
    };
    const [dynamic2DState, setDynamic2DState] = useState<TConfigMapShare[]>(initFormValues?.config?.m2ds ?? []);
    const [dynamic3DState, setDynamic3DState] = useState<TConfigMapShare[]>(initFormValues?.config?.m3ds ?? []);
    const [dynamicVr360State, setDynamicVr360State] = useState<TConfigMapShare[]>(initFormValues?.config?.vr360s ?? []);

    useEffect(() => {
        setDynamic2DState(initFormValues?.config?.m2ds ?? []);
        setDynamic3DState(initFormValues?.config?.m3ds ?? []);
        setDynamicVr360State(initFormValues?.config?.vr360s ?? []);

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [initFormValues])

    const timeFormatted = (values: string = App.FormatToMoment): string | undefined => {
        return moment(values).format('YYYY-MM-DD HH:mm:ss').toString();
    }

    const columns: ColumnsType<TMapPreviewShare> = [
        {
            title: t('text.name'),
            key: 'name',
            dataIndex: 'name',
        },
        {
            title: t('text.createAt'),
            key: 'createdAt',
            render: (_, item) => {
                return (
                    <Typography.Text key={`createdAt_${item.createdAt}`}>
                        {timeFormatted(item.createdAt)}
                    </Typography.Text>
                )
            }
        },
        {
            title: t('text.status'),
            key: 'status',
            render: (_, item, __) => <Tag key={`tag_status_${item.status}`} color={item.status ? 'success' : 'red'}>
                {item.status ? t("text.active") : t("text.inActive")}
            </Tag>
        },
        {
            title: t('text.activeTime'),
            key: 'expiryDate',
            render: (_, item) => {
                return (
                    item.expiryDate && item.expiryDate.start && item.expiryDate.end
                        ? <Space direction={"vertical"}>
                            <Typography.Text>{t("text.dateStart")}:&nbsp;&nbsp;{timeFormatted(item.expiryDate.start)}</Typography.Text>
                            <Typography.Text>{t("text.dateEnd")}:&nbsp;&nbsp;{timeFormatted(item.expiryDate.end)}</Typography.Text>
                        </Space>
                        : item.expiryDate && item.expiryDate.start
                            ?
                            <Typography.Text>{t("text.dateStart")}:&nbsp;&nbsp;{timeFormatted(item.expiryDate.start)}</Typography.Text>
                            : item.expiryDate && item.expiryDate.end
                                ?
                                <Typography.Text>{t("text.dateEnd")}:&nbsp;&nbsp;{timeFormatted(item.expiryDate.end)}</Typography.Text>
                                : <Typography.Text>{t("text.unlimited")}</Typography.Text>
                )
            }
        }
    ];

    const onFinish = (values: any) => {
        let shareM2D: TConfigMapShare[] = [];
        let shareM3D: TConfigMapShare[] = [];
        let shareVr360: TConfigMapShare[] = [];

        m2d?.forEach(value => {
            if (value.m2dId && values[`status2D_${value.m2dId}`]) {
                const fMap2D = dynamic2DState.find(_ => {
                    if (_ && _.id !== undefined) {

                        return _.id === value.m2dId
                    } else return false
                });

                shareM2D = [...shareM2D, {
                    id: value.m2dId,
                    sid: fMap2D ? fMap2D.sid : '',
                    status: values[`status2D_${value.m2dId}`] || false,
                }]
            }
            if (value.m2dId && !values[`status2D_${value.m2dId}`]) {
                const fMap2D = dynamic2DState.find(_ => {
                    if (_ && _.id !== undefined) {

                        return _.id === value.m2dId
                    } else return false
                });

                if (fMap2D && fMap2D.sid !== undefined) {
                    shareM2D = [...shareM2D, {
                        id: value.m2dId,
                        sid: fMap2D ? fMap2D.sid : '',
                        status: values[`status2D_${value.m2dId}`],
                    }]
                }
            }
        });

        m3d?.forEach(value => {
            if (value.m3dId && values[`status3D_${value.m3dId}`]) {
                const fMap3D = dynamic3DState.find(_ => {
                    if (_ && _.id !== undefined) {
                        return _.id === value.m3dId
                    } else return false
                });

                shareM3D = [...shareM3D, {
                    id: value.m3dId,
                    sid: fMap3D ? fMap3D.sid : '',
                    status: values[`status3D_${value.m3dId}`],
                }]
            }
        });
        vr360?.forEach((value => {
            if (value.vr360Id && values[`statusVr360_${value.vr360Id}`]) {
                const fMapVr360 = dynamicVr360State.find(_ => {
                    if (_ && _.id !== undefined) {
                        return _.id === value.vr360Id;
                    } else return false
                });
                shareVr360 = [...shareVr360, {
                    id: value.vr360Id,
                    sid: fMapVr360?.sid ?? '',
                    status: values[`statusVr360_${value.vr360Id}`],
                }]
            }
        }))

        const data: TSpaceShareV0 = {
            name: values.name,
            password: values.password,
            dateStart: values['range-time-picker'] && values['range-time-picker'][0] ? moment(values['range-time-picker'][0]).format('YYYY-MM-DD HH:mm:ss').toString() : '',
            dateEnd: values['range-time-picker'] && values['range-time-picker'][1] ? moment(values['range-time-picker'][1]).format('YYYY-MM-DD HH:mm:ss').toString() : '',
            status: values.isShare,
            sInfo: values.shareInfo,
            config: {
                m2ds: shareM2D,
                m3ds: shareM3D,
                vr360s: shareVr360,
            }
        }

        if (spaceId) {
            if (shareId) {
                onEditItem(spaceId, shareId, data)
            } else {
                onAddItem(spaceId, data)
            }
        }
    }

    return (
        <Modal
            visible={visible}
            onCancel={() => {
                onCancel()
                setIsPreview(false)
            }}
            key={"modal-form-main-setting-space"}
            title={isPreview ? t("text.detailInfo") : !shareId ? t("text.createLink") : t("text.editLink")}
            onOk={onOk}
            afterClose={onAfterClose}
            destroyOnClose={true}
            confirmLoading={vm.isLoading === SendingStatus.loading}
            width={1000}
            footer={
                isPreview ?
                    [
                        <Button
                            key={"edit"}
                            type={"primary"}
                            onClick={onChangeToEdit}
                            icon={<EditOutlined/>}
                        >
                            {t("button.edit")}
                        </Button>,
                        <Button
                            key={"close"}
                            onClick={() => {
                                onCancel()
                                setIsPreview(false)
                            }}
                            icon={<CloseOutlined/>}
                        >
                            {t("button.close")}
                        </Button>
                    ] : [
                        <Button
                            key={"close"}
                            onClick={() => {
                                onCancel()
                                setIsPreview(false)
                            }}
                            icon={<CloseOutlined/>}
                        >
                            {t("button.close")}
                        </Button>,
                        <Button
                            key={"save"}
                            type={"primary"}
                            onClick={() => onOk()}
                            icon={<SaveOutlined/>}
                            loading={vm.isUpdating === SendingStatus.loading}
                        >
                            {shareId ? t("button.save") : t("button.saveNew")}
                        </Button>
                    ]
            }
        >
            <Form.Provider
                onFormFinish={(name, {values}) => {
                    m2d?.forEach((value, index) => {
                        if (name === `userForm_${value.m2dId}` && value.m2dId) {
                            let newDynamicState = dynamic2DState;
                            newDynamicState[index] = {
                                ...newDynamicState[index],
                                id: value.m2dId,
                                sid: values.share2D.sid,
                                status: values.status,
                            };
                            if (newDynamicState[index] !== null) {
                                setDynamic2DState(newDynamicState);
                            }

                            setVisibleModal2D({
                                ...visibleModal2D,
                                visible: false,
                            });
                        }
                    })
                    m3d?.forEach((value, index) => {
                        if (name === `userForm_${value.m3dId}` && value.m3dId) {
                            let newDynamicState = dynamic3DState;
                            newDynamicState[index] = {
                                ...newDynamicState[index],
                                id: value.m3dId,
                                sid: values.share3D.sid,
                                status: values.status,
                            };
                            if (newDynamicState[index] !== null) {
                                setDynamic3DState(newDynamicState);
                            }

                            setVisibleModal3D({
                                ...visibleModal3D,
                                visible: false,
                            });
                        }
                    })
                    vr360?.forEach((value, index) => {
                        if (name === `userForm_${value.vr360Id}` && value.vr360Id) {
                            let newDynamicState = dynamicVr360State;
                            newDynamicState[index] = {
                                ...newDynamicState[index],
                                id: value.vr360Id,
                                sid: values.shareVr360.sid,
                                status: values.status,
                            };
                            if (newDynamicState[index] !== null) {
                                setDynamicVr360State(newDynamicState);
                            }

                            setVisibleModalVr360({
                                ...visibleModalVr360,
                                visible: false,
                            });
                        }
                    })
                }}
            >
                <Form
                    key={'form_space'}
                    form={formMain}
                    {...formItemLayout}
                    onFinish={onFinish}
                >
                    <Form.Item
                        labelAlign={"left"}
                        key={"k_name"}
                        label={t("text.name")}
                        name={"name"}
                        rules={[
                            {
                                required: true,
                                message: t("validation.emptyData"),
                            },
                            {
                                max: 100,
                                message: t('validation.minAndMaxCharacter', {
                                    label: t("text.name"),
                                    min: '1',
                                    max: '100'
                                }),
                            }
                        ]}
                        initialValue={initFormValues?.name}
                    >
                        <Input disabled={isPreview} allowClear={true}/>
                    </Form.Item>
                    <Form.Item
                        labelAlign={"left"}
                        key={"k_password"}
                        label={t("label.password")}
                        name={"password"}
                        initialValue={initFormValues?.password}
                        rules={[
                            {
                                min: 6,
                                message: t('validation.password', {
                                    label: t("label.password"),
                                    min: '6',
                                    max: '100'
                                }),
                            }
                        ]}
                    >
                        <Input disabled={isPreview}/>
                    </Form.Item>
                    <Form.Item
                        labelAlign={"left"}
                        key={"k_range-time-picker"}
                        name="range-time-picker"
                        label={t("text.activeTime")}
                        {
                            ...(initFormValues?.expiryDate?.start || initFormValues?.expiryDate?.end
                                    ? {
                                        initialValue: [
                                            initFormValues?.expiryDate.start ? moment(initFormValues.expiryDate.start, App.FormatISOFromMoment) : null,
                                            initFormValues?.expiryDate.end ? moment(initFormValues.expiryDate.end, App.FormatISOFromMoment) : undefined
                                        ]
                                    }
                                    : null
                            )}
                    >
                        <DatePicker.RangePicker
                            disabled={[isPreview, isPreview]}
                            allowEmpty={[true, true]}
                            showTime
                            format={t("format.dateTime")}
                        />
                    </Form.Item>
                    <Form.Item
                        key={"isShare"}
                        label={t('text.sharingAllow')}
                        labelAlign={'left'}
                        name={"isShare"}
                        valuePropName={"checked"}
                        initialValue={initFormValues?.status ?? false}
                    >
                        <Switch
                            disabled={isPreview}
                            defaultChecked={false}
                        />
                    </Form.Item>
                    <Form.Item
                        key={"sInfo"}
                        label={t('text.displayInformation')}
                        labelAlign={'left'}
                        name={"shareInfo"}
                        valuePropName={"checked"}
                        initialValue={initFormValues?.sInfo ?? false}
                    >
                        <Switch
                            disabled={isPreview}
                            defaultChecked={false}
                        />
                    </Form.Item>
                    <Divider/>
                    <Form.Item>
                        <Space>
                            <SettingOutlined/>
                            <Typography.Text>
                                {t("title.setUpAdvancedPermissions")}
                            </Typography.Text>
                        </Space>
                    </Form.Item>
                    <Tabs tabPosition={'left'}>
                        {
                            m2d
                                ? <Tabs.TabPane tab="2D" key="2D">
                                    {
                                        m2d.map((item, index) => {
                                            const sM2d = dynamic2DState.find(_ => {
                                                if (_ && _.id !== undefined) {
                                                    return _.id === item.m2dId
                                                } else {
                                                    return false
                                                }
                                            });

                                            return (
                                                <Card
                                                    key={`${item.m2dId}_${index}`}
                                                    title={item.name}
                                                    style={{maxWidth: 800}}
                                                    bordered={false}
                                                    size={"small"}
                                                    className={'ml-8'}
                                                >
                                                    <Form.Item
                                                        labelAlign={'left'}
                                                        key={`status2D_${item.m2dId}`}
                                                        label={t('text.sharingAllow')}
                                                        name={`status2D_${item.m2dId}`}
                                                        valuePropName={"checked"}
                                                        style={{maxWidth: '80%'}}
                                                        initialValue={sM2d?.status || false}
                                                    >
                                                        <Switch
                                                            disabled={isPreview}
                                                        />
                                                    </Form.Item>
                                                    <Row gutter={32} key={'row2D'}>
                                                        <Col>
                                                            <Form.Item
                                                                key={`status2D_${item.m2dId}`}
                                                                shouldUpdate={(prevValues, curValues) => prevValues[`status2D_${item.m2dId}`] !== curValues[`status2D_${item.m2dId}`]}
                                                            >
                                                                {({getFieldValue}) => {
                                                                    const isActive: boolean = getFieldValue(`status2D_${item.m2dId}`) || false;
                                                                    return (
                                                                        <Button
                                                                            key={`edit_info_advance_${item.m2dId}`}
                                                                            htmlType="button"
                                                                            disabled={isPreview || !isActive}
                                                                            onClick={() => showUserModal2D(item.m2dId!, item.name!)}
                                                                            icon={<EditOutlined/>}
                                                                        >
                                                                            {t("button.edit")}
                                                                        </Button>
                                                                    )
                                                                }}
                                                            </Form.Item>
                                                        </Col>
                                                        <Col>
                                                            {
                                                                dynamic2DState
                                                                    ? dynamic2DState.map((data) => {
                                                                        if (data.id === item.m2dId && data.share !== undefined) {
                                                                            return (
                                                                                <Table
                                                                                    size={'small'}
                                                                                    columns={columns}
                                                                                    pagination={false}
                                                                                    key={`table2D_${data.id}`}
                                                                                    dataSource={[data.share]}
                                                                                    rowKey={record => record.createdAt}
                                                                                />
                                                                            )
                                                                        } else {
                                                                            return null
                                                                        }
                                                                    })
                                                                    : null
                                                            }
                                                        </Col>
                                                    </Row>
                                                </Card>
                                            );
                                        })
                                    }
                                </Tabs.TabPane>
                                : null
                        }
                        {
                            m3d
                                ? <Tabs.TabPane tab="3D" key="3D">
                                    {
                                        m3d.map((item, index) => {
                                            const sM3d = dynamic3DState.find(_ => {
                                                if (_ && _.id !== undefined) {
                                                    return _.id === item.m3dId
                                                } else {
                                                    return false
                                                }
                                            });

                                            return (
                                                <Card
                                                    key={`${item.m3dId}_${index}`}
                                                    title={item.name}
                                                    style={{maxWidth: 800}}
                                                    bordered={false}
                                                    size={"small"}
                                                    className={'ml-8'}
                                                >
                                                    <Form.Item
                                                        labelAlign={'left'}
                                                        key={`status3D_${item.m3dId}`}
                                                        label={t('text.sharingAllow')}
                                                        name={`status3D_${item.m3dId}`}
                                                        valuePropName={"checked"}
                                                        style={{maxWidth: '80%'}}
                                                        initialValue={sM3d?.status}
                                                    >
                                                        <Switch
                                                            disabled={isPreview}
                                                            defaultChecked={false}
                                                        />
                                                    </Form.Item>
                                                    <Row gutter={32} key={'row3D'}>
                                                        <Col>
                                                            <Form.Item
                                                                key={`status3D_${item.m3dId}`}
                                                                shouldUpdate={(prevValues, curValues) => prevValues[`status3D_${item.m3dId}`] !== curValues[`status3D_${item.m3dId}`]}
                                                            >
                                                                {({getFieldValue}) => {
                                                                    const isActive: boolean = getFieldValue(`status3D_${item.m3dId}`) || false;
                                                                    return (
                                                                        <Button
                                                                            key={`edit_info_advance_${item.m3dId}`}
                                                                            htmlType="button"
                                                                            disabled={isPreview || !isActive}
                                                                            onClick={() => showUserModal3D(item.m3dId!, item.name!)}
                                                                            icon={<EditOutlined/>}
                                                                        >
                                                                            {t("button.edit")}
                                                                        </Button>
                                                                    )
                                                                }}
                                                            </Form.Item>
                                                        </Col>
                                                        <Col>
                                                            {
                                                                dynamic3DState
                                                                    ? dynamic3DState.map((data) => {
                                                                        if (data.id === item.m3dId && data.share) {
                                                                            return (
                                                                                <Table
                                                                                    size={'small'}
                                                                                    columns={columns}
                                                                                    pagination={false}
                                                                                    key={`table3D_${data.id}`}
                                                                                    dataSource={[data.share]}
                                                                                    rowKey={record => record.createdAt}
                                                                                />
                                                                            )
                                                                        } else {
                                                                            return null
                                                                        }
                                                                    })
                                                                    : null
                                                            }
                                                        </Col>
                                                    </Row>
                                                </Card>
                                            );
                                        })
                                    }
                                </Tabs.TabPane>
                                : null
                        }
                        {
                            vr360
                                ? <Tabs.TabPane tab="Vr360" key="vr360s">
                                    {
                                        vr360.map((item, index) => {
                                            const sVr360s = dynamicVr360State.find(_ => {
                                                if (_ && _.id !== undefined) {
                                                    return _.id === item.vr360Id
                                                } else {
                                                    return false
                                                }
                                            });

                                            return (
                                                <Card
                                                    key={`${item.vr360Id}_${index}`}
                                                    title={item.name}
                                                    style={{maxWidth: 800}}
                                                    bordered={false}
                                                    size={"small"}
                                                    className={'ml-8'}
                                                >
                                                    <Form.Item
                                                        labelAlign={'left'}
                                                        key={`statusVr360_${item.vr360Id}`}
                                                        label={t('text.sharingAllow')}
                                                        name={`statusVr360_${item.vr360Id}`}
                                                        valuePropName={"checked"}
                                                        style={{maxWidth: '80%'}}
                                                        initialValue={sVr360s?.status}
                                                    >
                                                        <Switch
                                                            disabled={isPreview}
                                                            defaultChecked={false}
                                                        />
                                                    </Form.Item>
                                                    <Row gutter={32} key={'row3D'}>
                                                        <Col>
                                                            <Form.Item
                                                                key={`statusVr360_${item.vr360Id}`}
                                                                shouldUpdate={(prevValues, curValues) => prevValues[`statusVr360_${item.vr360Id}`] !== curValues[`statusVr360_${item.vr360Id}`]}
                                                            >
                                                                {({getFieldValue}) => {
                                                                    const isActive: boolean = getFieldValue(`statusVr360_${item.vr360Id}`) || false;
                                                                    return (
                                                                        <Button
                                                                            key={`edit_info_advance_${item.vr360Id}`}
                                                                            htmlType="button"
                                                                            disabled={isPreview || !isActive}
                                                                            onClick={() => showUserModalVr360(item.vr360Id!, item.name!)}
                                                                            icon={<EditOutlined/>}
                                                                        >
                                                                            {t("button.edit")}
                                                                        </Button>
                                                                    )
                                                                }}
                                                            </Form.Item>
                                                        </Col>
                                                        <Col>
                                                            {
                                                                dynamicVr360State
                                                                    ? dynamicVr360State.map((data) => {
                                                                        if (data.id === item.vr360Id && data.share) {
                                                                            return (
                                                                                <Table
                                                                                    size={'small'}
                                                                                    columns={columns}
                                                                                    pagination={false}
                                                                                    key={`tableVr360_${data.id}`}
                                                                                    dataSource={[data.share]}
                                                                                    rowKey={record => record.createdAt}
                                                                                />
                                                                            )
                                                                        } else {
                                                                            return null
                                                                        }
                                                                    })
                                                                    : null
                                                            }
                                                        </Col>
                                                    </Row>
                                                </Card>
                                            );
                                        })
                                    }
                                </Tabs.TabPane>
                                : null
                        }
                    </Tabs>
                    <ModalFormShare2D
                        id={visibleModal2D.id}
                        name={visibleModal2D.name}
                        visible={visibleModal2D.visible}
                        onCancel={hideUserModal2D}
                        initFromValues={initFormValues?.config?.m2ds}
                    />
                    <ModalFormShare3D
                        id={visibleModal3D.id}
                        name={visibleModal3D.name}
                        visible={visibleModal3D.visible}
                        onCancel={hideUserModal3D}
                        initFromValues={initFormValues?.config?.m3ds}
                    />
                    <ModalFormShareVr360
                        id={visibleModalVr360.id}
                        name={visibleModalVr360.name}
                        visible={visibleModalVr360.visible}
                        onCancel={hideUserModalVr360}
                        initFromValues={initFormValues?.config?.vr360s}
                    />
                </Form>
            </Form.Provider>
        </Modal>
    )
}

const ModalFormShare2D: React.FC<ModalFormProps> = ({
                                                        id,
                                                        name,
                                                        visible,
                                                        onCancel,
                                                        initFromValues
                                                    }) => {
    const {t} = useTranslation();
    const [form] = Form.useForm();

    const {
        vm,
        onLoadItems,
        onClearState,
    } = Map2DShareAction();

    useEffect(() => {
        if ((vm.items === null || vm.key !== id!.toString()
            || (vm.timestamp !== undefined && Utils.checkHourState(vm.timestamp))) && id !== 0
        ) {
            onLoadItems(id, queryParams);
        }

        onClearState();

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [visible]);

    const [queryParams, setQueryParams] = useState<{ page: number, limit: number }>({
        page: vm.query.page,
        limit: vm.query.limit,
    });
    const onChangePage = (page: number) => {
        setQueryParams({
            ...queryParams,
            page: page,
            limit: vm.query.limit,
        })

        if (id && vm.items && vm.items?.slice((page - 1) * vm.query.limit, page * vm.query.limit).length === 0) {
            onLoadItems(id, {
                page: page,
                limit: queryParams.limit,
            })
        }
    };

    const onOk = () => {
        form.submit();
        onCancel();
    };

    const formItemLayout = {
        labelCol: {
            xs: {span: 2},
            sm: {span: 2},
            xxl: {span: 4},
            xl: {span: 4},
        },
    };

    const initShare2D: string[] = [];

    if (initFromValues) {
        initFromValues.forEach((item) => initShare2D.push(item.sid))
    }

    const columns: ColumnsType<Map2DShareModel> = [
        {
            key: 'select',
            dataIndex: 'sid',
            render: (_, item) => {
                const sMap2d = vm.items?.find(_ => initShare2D.includes(_.sid));
                let initShare;

                if (sMap2d !== undefined) {
                    initShare = sMap2d
                } else if (vm.items) {
                    initShare = vm.items[0]
                }
                return (
                    <Form.Item
                        name={`share2D`}
                        key={`share2D_${item.sid}`}
                        labelAlign={"left"}
                        className={'mb-0'}
                        initialValue={initShare}
                    >
                        <Radio.Group>
                            <Radio value={item}>
                            </Radio>
                        </Radio.Group>
                    </Form.Item>
                )
            }
        },
        {
            title: t('text.name'),
            key: 'name',
            dataIndex: 'name',
        },
        {
            title: t('text.createAt'),
            key: 'createdAt',
            render: (_, item) => {
                return (
                    <Typography.Text>
                        {item.dateCreatedAtFormatted()}
                    </Typography.Text>
                )
            }
        },
        {
            title: t('text.status'),
            key: 'status',
            render: (_, item, __) => (
                <Tag key={`tag_status_${item.sid}`} color={item.status ? 'success' : 'red'}>
                    {item.status ? t("text.active") : t("text.inActive")}
                </Tag>
            )
        },
        {
            title: t('text.activeTime'),
            key: 'date',
            render: (_, item) => {
                return (
                    item.dateEndFormatted() && item.dateStartFormatted()
                        ? <Space direction={"vertical"}>
                            <Typography.Text>{t("text.dateStart")}:&nbsp;&nbsp;{item.dateStartFormatted()}</Typography.Text>
                            <Typography.Text>{t("text.dateEnd")}:&nbsp;&nbsp;{item.dateEndFormatted()}</Typography.Text>
                        </Space>
                        : item.dateStartFormatted()
                            ?
                            <Typography.Text>{t("text.dateStart")}:&nbsp;&nbsp;{item.dateStartFormatted()}</Typography.Text>
                            : item.dateEndFormatted()
                                ?
                                <Typography.Text>{t("text.dateEnd")}:&nbsp;&nbsp;{item.dateEndFormatted()}</Typography.Text>
                                : <Typography.Text>{t("text.unlimited")}</Typography.Text>
                )
            }
        }
    ];

    return (
        <Modal
            visible={visible}
            onCancel={onCancel}
            onOk={onOk}
            title={name}
            afterClose={() => form.resetFields()}
            destroyOnClose={true}
            width={800}
        >
            <Form
                form={form}
                name={`userForm_${id}`}
                {...formItemLayout}
            >
                <Table
                    loading={vm.isLoading === SendingStatus.loading}
                    columns={columns}
                    dataSource={vm.items}
                    rowKey={record => record.sid}
                    pagination={
                        {
                            total: vm.oMeta?.totalCount ?? 0,
                            defaultPageSize: 5,
                            defaultCurrent: 1,
                            onChange: (page) => onChangePage(page),
                        }
                    }
                />
            </Form>
        </Modal>
    )
}

const ModalFormShare3D: React.FC<ModalFormProps> = ({
                                                        id,
                                                        name,
                                                        visible,
                                                        onCancel,
                                                        initFromValues
                                                    }) => {
    const {t} = useTranslation();
    const [form] = Form.useForm();
    const {
        vm,
        onLoadItems,
        onClearState,
    } = Map3DShareAction();

    useEffect(() => {
        if (
            (
                vm.items === null
                || vm.key !== id!.toString()
                || (vm.timestamp !== undefined && Utils.checkHourState(vm.timestamp))
            )
            && id !== 0
        ) {
            onLoadItems(id, queryParams);
        }

        onClearState();

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [visible]);

    const [queryParams, setQueryParams] = useState<{ page: number, limit: number }>({
        page: vm.query.page,
        limit: vm.query.limit,
    });

    const onChangePage = (page: number) => {
        setQueryParams({
            ...queryParams,
            page: page,
            limit: vm.query.limit,
        })

        if (id && vm.items && vm.items?.slice((page - 1) * vm.query.limit, page * vm.query.limit).length === 0) {
            onLoadItems(id, {
                page: page,
                limit: queryParams.limit,
            })
        }
    };

    const onOk = () => {
        form.submit();
        onCancel();
    };

    const formItemLayout = {
        labelCol: {
            xs: {span: 2},
            sm: {span: 2},
            xxl: {span: 4},
            xl: {span: 4},
        },
    };

    const initShare3D: string[] = [];

    if (initFromValues) {
        initFromValues.forEach((item) => initShare3D.push(item.sid))
    }

    const columns: ColumnsType<Map3DShareModel> = [
        {
            key: 'select',
            dataIndex: 'sid',
            render: (_, item) => {
                const sMap3d = vm.items?.find(_ => initShare3D.includes(_.sid));

                let initShare;
                if (sMap3d !== undefined) {
                    initShare = sMap3d
                } else if (vm.items) {
                    initShare = vm.items[0]
                }

                return (
                    <Form.Item
                        name={`share3D`}
                        key={`share3D_${item.sid}`}
                        labelAlign={"left"}
                        className={'mb-0'}
                        initialValue={initShare}
                    >
                        <Radio.Group>
                            <Radio value={item}>
                            </Radio>
                        </Radio.Group>
                    </Form.Item>
                )
            }
        },
        {
            title: t('text.name'),
            key: 'name',
            dataIndex: 'name',
        },
        {
            title: t('text.createAt'),
            key: 'createdAt',
            render: (_, item) => {
                return (
                    <Typography.Text>
                        {item.dateCreatedAtFormatted()}
                    </Typography.Text>
                )
            }
        },
        {
            title: t('text.status'),
            key: 'status',
            render: (_, item, __) => (
                <Tag key={`tag_status_${item.sid}`} color={item.status ? 'success' : 'red'}>
                    {item.status ? t("text.active") : t("text.inActive")}
                </Tag>
            )
        },
        {
            title: t('text.activeTime'),
            key: 'date',
            render: (_, item) => {
                return (
                    item.dateEndFormatted() && item.dateStartFormatted()
                        ? <Space direction={"vertical"}>
                            <Typography.Text>{t("text.dateStart")}:&nbsp;&nbsp;{item.dateStartFormatted()}</Typography.Text>
                            <Typography.Text>{t("text.dateEnd")}:&nbsp;&nbsp;{item.dateEndFormatted()}</Typography.Text>
                        </Space>
                        : item.dateStartFormatted()
                            ?
                            <Typography.Text>{t("text.dateStart")}:&nbsp;&nbsp;{item.dateStartFormatted()}</Typography.Text>
                            : item.dateEndFormatted()
                                ?
                                <Typography.Text>{t("text.dateEnd")}:&nbsp;&nbsp;{item.dateEndFormatted()}</Typography.Text>
                                : <Typography.Text>{t("text.unlimited")}</Typography.Text>
                )
            }
        }
    ];

    return (
        <Modal
            visible={visible}
            onCancel={onCancel}
            onOk={onOk}
            title={name}
            afterClose={() => form.resetFields()}
            destroyOnClose={true}
            width={800}
        >
            <Form
                form={form}
                name={`userForm_${id}`}
                {...formItemLayout}
            >
                <Table
                    loading={vm.isLoading === SendingStatus.loading}
                    columns={columns}
                    dataSource={vm.items}
                    rowKey={record => record.sid}
                    pagination={
                        {
                            total: vm.oMeta?.totalCount ?? 0,
                            defaultPageSize: 5,
                            defaultCurrent: 1,
                            onChange: (page) => onChangePage(page),
                        }
                    }
                />
            </Form>
        </Modal>
    )
}

const ModalFormShareVr360: React.FC<ModalFormProps> = ({
                                                           id,
                                                           name,
                                                           visible,
                                                           onCancel,
                                                           initFromValues
                                                       }) => {
    const {t} = useTranslation();
    const [form] = Form.useForm();

    const {
        vm,
        onLoadItems,
        onClearState,
    } = Map360ShareAction();

    useEffect(() => {
        if ((vm.items === null || vm.key !== id!.toString()
            || (vm.timestamp !== undefined && Utils.checkHourState(vm.timestamp))) && id !== 0
        ) {
            onLoadItems(id, queryParams);
        }

        onClearState();

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [visible]);

    const [queryParams, setQueryParams] = useState<{ page: number, limit: number }>({
        page: vm.query.page,
        limit: vm.query.limit,
    });

    const onChangePage = (page: number) => {
        setQueryParams({
            ...queryParams,
            page: page,
            limit: vm.query.limit,
        })

        if (id && vm.items && vm.items?.slice((page - 1) * vm.query.limit, page * vm.query.limit).length === 0) {
            onLoadItems(id, {
                page: page,
                limit: queryParams.limit,
            })
        }
    };

    const onOk = () => {
        form.submit();
        onCancel();
    };

    const initShareVr360s: string[] = [];

    if (initFromValues) {
        initFromValues.forEach((item) => initShareVr360s.push(item.sid))
    }

    const columns: ColumnsType<Map3DShareModel> = [
        {
            key: 'select',
            dataIndex: 'sid',
            render: (_, item) => {
                const sVr360s = vm.items?.find(_ => initShareVr360s.includes(_.sid));
                let initShare;
                if (sVr360s !== undefined) {
                    initShare = sVr360s
                } else if (vm.items) {
                    initShare = vm.items[0]
                }
                return (
                    <Form.Item
                        name={`shareVr360`}
                        key={`shareVr360_${item.sid}`}
                        labelAlign={"left"}
                        className={'mb-0'}
                        initialValue={initShare}
                    >
                        <Radio.Group>
                            <Radio value={item}>
                            </Radio>
                        </Radio.Group>
                    </Form.Item>
                )
            }
        },
        {
            title: t('text.name'),
            key: 'name',
            dataIndex: 'name',
        },
        {
            title: t('text.createAt'),
            key: 'createdAt',
            render: (_, item) => {
                return (
                    <Typography.Text>
                        {item.dateCreatedAtFormatted()}
                    </Typography.Text>
                )
            }
        },
        {
            title: t('text.status'),
            key: 'status',
            render: (_, item, __) => (
                <Tag key={`tag_status_${item.sid}`} color={item.status ? 'success' : 'red'}>
                    {item.status ? t("text.active") : t("text.inActive")}
                </Tag>
            )
        },
        {
            title: t('text.activeTime'),
            key: 'date',
            render: (_, item) => {
                return (
                    item.dateEndFormatted() && item.dateStartFormatted()
                        ? <Space direction={"vertical"}>
                            <Typography.Text>{t("text.dateStart")}:&nbsp;&nbsp;{item.dateStartFormatted()}</Typography.Text>
                            <Typography.Text>{t("text.dateEnd")}:&nbsp;&nbsp;{item.dateEndFormatted()}</Typography.Text>
                        </Space>
                        : item.dateStartFormatted()
                            ?
                            <Typography.Text>{t("text.dateStart")}:&nbsp;&nbsp;{item.dateStartFormatted()}</Typography.Text>
                            : item.dateEndFormatted()
                                ?
                                <Typography.Text>{t("text.dateEnd")}:&nbsp;&nbsp;{item.dateEndFormatted()}</Typography.Text>
                                : <Typography.Text>{t("text.unlimited")}</Typography.Text>
                )
            }
        }
    ];

    return (
        <Modal
            visible={visible}
            onCancel={onCancel}
            onOk={onOk}
            title={name}
            afterClose={() => form.resetFields()}
            destroyOnClose={true}
            width={800}
        >
            <Form
                form={form}
                name={`userForm_${id}`}
                labelCol={{
                    xs: {span: 2},
                    sm: {span: 2},
                    xxl: {span: 4},
                    xl: {span: 4},
                }}
            >
                <Table
                    loading={vm.isLoading === SendingStatus.loading}
                    columns={columns}
                    dataSource={vm.items as any}
                    rowKey={record => record.sid}
                    pagination={
                        {
                            total: vm.oMeta?.totalCount ?? 0,
                            defaultPageSize: 5,
                            defaultCurrent: 1,
                            onChange: (page) => onChangePage(page),
                        }
                    }
                />
            </Form>
        </Modal>
    )
}

