import {useTranslation} from "react-i18next";
import {useEffect, useRef, useState} from "react";
import {SendingStatus} from "../../../../const/Events";
import {Button, Form, Input} from "antd";
import {Style} from "../../../../const/Style";
import {SpaceItemFC, SpaceSkeleton} from "../../../widgets/SpaceItemFC";
import {Color} from "../../../../const/Color";
import {useLocation, useNavigate} from "react-router";
import {TSpaceFilterVO} from "../../../../models/service/geodetic/SpaceModel";
import {debounce} from "lodash";
import {App} from "../../../../const/App";
import {ErrorItemFC} from "../../../widgets/ErrorItemFC";
import {CommonEmptyFC, CommonTagFilterFC} from "../../../widgets/CommonFC";
import {UrlQuery} from "../../../../core/UrlQuery";
import {MediaQuery} from "../../../../core/MediaQuery";
import {Utils} from "../../../../core/Utils";
import {GeodeticsAction} from "../../../../recoil/service/geodetic/geodetics/GeodeticsAction";
import SearchIcon from "../../../components/icons/SearchIcon";
import {WrapContentWidget} from "../../../widgets/WrapContentWidget";
import {GridLoadingWidget} from "../../../widgets/GridLoadingWidget";
import CustomPagination from "../../../components/CustomPagination";
import {RouteConfig} from "../../../../config/RouteConfig";
import {useSessionContext} from "../../../contexts/SessionContext";
import {useInjection} from "inversify-react";
import {StoreConfig} from "../../../../config/StoreConfig";

export function GeodeticScreen() {
    const {t} = useTranslation()
    const navigate = useNavigate()
    const location = useLocation()
    const [session] = useSessionContext()
    const storeConfig = useInjection(StoreConfig)

    const URL = new UrlQuery(location.search)
    const [form] = Form.useForm()
    const {
        vm,
        onLoadItems,
        onCacheItems,
        resetStateWithEffect,
    } = GeodeticsAction()

    const title = t('title.geodetic')
    const filter = URL.get('filter', {});
    const sort = URL.get('sort');
    const order = URL.get('order');
    const page = URL.getInt('page', vm.query.page);
    const limit = URL.getInt('limit', vm.query.limit);
    const [queryParams, setQueryParams] = useState<TSpaceFilterVO>({
        page: page,
        order: order,
        sort: sort,
        filter: filter,
        limit: limit
    });

    const _buildForm = () => {
        return (
            <>
                {
                    (session.user && storeConfig.checkPlatformTest(session.user.username)) && (
                        <div className={"mr-5"}>
                            <Button onClick={() => navigate(`${RouteConfig.GEODETIC_MANAGER}`)} type={"primary"}>Platform Test</Button>
                        </div>
                    )
                }
                <Form form={form}>
                    <Form.Item
                        className={"w-52 md:w-[360px]"}
                        style={{
                            marginBottom: "0px",
                        }}
                        name={"search"}
                    >
                        <Input
                            className={"input-under-line-main"}
                            size="small"
                            placeholder={t('text.search')}
                            allowClear
                            onChange={onChangeSearch}
                            prefix={<SearchIcon/>}
                        />
                    </Form.Item>
                </Form>
            </>
        )
    }

    useEffect(() => {
        console.log('%cMount Screen: GeodeticScreen', Color.ConsoleInfo);

        if (
            vm.items.length === 0
            || (vm.timestamp !== undefined && Utils.checkHourState(vm.timestamp))
        ) {
            if (vm.items.length > 0) resetStateWithEffect()
            const urlQueryParams = new UrlQuery(queryParams);
            urlQueryParams.set('limit', (new MediaQuery(Style.GridLimit)).getPoint(limit));

            onLoadItems(urlQueryParams.toObject());
            setQueryParams(urlQueryParams.toObject());
        }

        return () => {
            console.log('%cUnmount Screen: GeodeticScreen', Color.ConsoleInfo);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        const urlQueryParams = new UrlQuery(queryParams);

        urlQueryParams.set('limit', vm.query.limit);

        setQueryParams(urlQueryParams.toObject());

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.query]);

    const onChangePage = (page: number) => {
        const urlQueryParams = new UrlQuery(queryParams);

        urlQueryParams.set('page', page);

        navigate({
            search: urlQueryParams.toString()
        }, {
            replace: true
        })

        setQueryParams(urlQueryParams.toObject());

        onLoadItems(urlQueryParams.toObject());
    };

    const onChangeSearch = (e: any) => setDebounceSearch.current(e.target.value);

    const setDebounceSearch = useRef(debounce((value: string) => {

        const urlQueryParams = new UrlQuery(queryParams);

        if (value.length > 0) {
            urlQueryParams.set('filter.q', value);
        } else {
            urlQueryParams.delete('filter.q');
        }
        urlQueryParams.set('page', 1);

        onCacheItems(urlQueryParams.toObject());

        navigate({
            search: urlQueryParams.toString()
        }, {
            replace: true
        })

        setQueryParams(urlQueryParams.toObject())

    }, App.DelaySearch));

    const isTags = (typeof queryParams.filter?.q !== "undefined" && queryParams.filter?.q?.length > 0);

    const handleCloseTag = (name: string) => {
        const urlQueryParams = new UrlQuery(queryParams);
        urlQueryParams.delete(name);
        urlQueryParams.set('page', 1);

        navigate({
            search: urlQueryParams.toString()
        }, {
            replace: true
        })

        if (name === "filter.q") {
            form.resetFields();
        }

        onLoadItems(urlQueryParams.toObject());
        setQueryParams(urlQueryParams.toObject());
    }

    const onClickReload = () => {
        const urlQueryParam = new UrlQuery(queryParams);
        onLoadItems(urlQueryParam.toObject())
    }

    return (
        <WrapContentWidget
            masterHeader={{
                title: title,
                isLoading: vm.isLoading,
                onReload: onClickReload,
            }}
            bodyHeader={{
                title: title,
                right: _buildForm(),
            }}
        >
            <CommonTagFilterFC
                is={isTags}
                onClose={handleCloseTag}
                data={queryParams}
                items={[
                    {
                        key: 'filter.q',
                        label: t('text.name')
                    }
                ]}
            />
            <ErrorItemFC
                status={vm.isLoading}
            >
                {

                    (vm.items.length > 0 && vm.isLoading === SendingStatus.success) || (vm.isLoading === SendingStatus.loading)
                        ? <div
                            className={"grid grid-cols-1 gap-4 sm:grid-cols-2 md:grid-cols-3 md:gap-6 xxl:grid-cols-4 3xl:gap-8"}>
                            {
                                vm.isLoading !== SendingStatus.loading ?
                                    vm.items.slice((page - 1) * vm.query.limit, page * vm.query.limit).map((item, index) => {
                                        return <SpaceItemFC key={index.toString()} item={item}/>
                                    })
                                    : <GridLoadingWidget>
                                        <SpaceSkeleton/>
                                    </GridLoadingWidget>
                            }
                        </div>
                        : <CommonEmptyFC/>
                }
            </ErrorItemFC>
            <CustomPagination
                isShow={vm.isLoading !== SendingStatus.loading && vm.items.length > 0 && vm.oMeta && vm.oMeta.pageCount > 1}
                defaultCurrent={page}
                current={page}
                total={vm.oMeta?.totalCount}
                defaultPageSize={vm.query.limit}
                onChange={onChangePage}
            />
        </WrapContentWidget>
    )
}
