import {WrapContentWidget} from "../../../widgets/WrapContentWidget";
import {useTranslation} from "react-i18next";
import {GeodeticsAction} from "../../../../recoil/service/geodetic/geodetics/GeodeticsAction";
import React from "react";

export const GeodeticManagerScreenDeleted = () => {
    const {t} = useTranslation()

    const {
        vm,
    } = GeodeticsAction()
    const title = t('title.geodetic')


    const onClickReload = () => {
        console.log("reload!")
    }


    return (
        <WrapContentWidget
            masterHeader={{
                title: title,
                isLoading: vm.isLoading,
                onReload: onClickReload,
            }}
            bodyHeader={{
                title: "Thùng rác",
            }}
        >
            Thùng rác!
        </WrapContentWidget>
    )
}