import {WrapContentWidget} from "../../../widgets/WrapContentWidget";
import {useTranslation} from "react-i18next";
import {GeodeticsAction} from "../../../../recoil/service/geodetic/geodetics/GeodeticsAction";
import {Form, Input, Popconfirm, Select, Table, Typography} from "antd";
import SearchIcon from "../../../components/icons/SearchIcon";
import React, {useEffect, useRef, useState} from "react";
import {AxiosClient} from "../../../../repositories/AxiosClient";
import moment from "moment";
import {App} from "../../../../const/App";
import {ApiResModel} from "../../../../models/ApiResModel";
import {Utils} from "../../../../core/Utils";

type _TItem = {
    id: string,
    name: string,
    size: number,
    extension: string,
    created_at: string,
}

type _TData = {
    key: string,
    id: string,
    name: string,
    type: string,
    create_at: string,
    files: _TItem[],
    size: string
}

export const GeodeticManagerScreen = () => {
    const {t} = useTranslation()
    const [form] = Form.useForm()
    const [select, setSelect] = useState<string>('All')
    const [data, setData] = useState<_TData[]>([])
    const dataAll = useRef<_TData[]>([])
    const {
        vm,
    } = GeodeticsAction()
    const title = t('title.geodetic')

    useEffect(() => {
        AxiosClient.get('platform/files')
            .then((items: ApiResModel) => {

                console.log('items: ', items)

                const data = items.items?.map((item: _TData) => {
                    return {
                        ...item,
                        key: item.id,
                    }
                })
                if (data) {
                    dataAll.current = data
                    setData(data)
                }
            })
            .catch(err => console.log(err))
    }, [])

    const handleSearchFile = (e: React.ChangeEvent<HTMLInputElement>) => {
        let d: _TData[]
        if (select === 'All') {
            d = dataAll.current.filter((item: _TData) => item.name.toLowerCase().includes(e.target.value.trim().toLowerCase()))
        } else {
            d = dataAll.current.filter((item: _TData) => item.name.toLowerCase().includes(e.target.value.trim().toLowerCase()) && item.type === select)
        }

        console.log('type:', select)
        if (d) {
            setData(d)
        } else {
            setData([])
        }
    }

    const _buildForm = () => {
        return (
            <Form form={form}>
                <Form.Item
                    className={"w-52 md:w-[360px]"}
                    style={{
                        marginBottom: "0px",
                    }}
                    name={"search"}
                >
                    <Input
                        className={"input-under-line-main"}
                        size="small"
                        placeholder={t('text.search')}
                        allowClear
                        onChange={handleSearchFile}
                        prefix={<SearchIcon/>}
                    />
                </Form.Item>
            </Form>
        )

    }

    const onClickReload = () => {
        console.log("reload!")
    }

    const handleChange = (value: string) => {

        setSelect(value)
        if (value === 'All') {
            setData(dataAll.current)
        } else if (value === '2D') {
            console.log('2d')
            const d: _TData[] = dataAll.current.filter((item: _TData) => item.type === '2d')
            setData(d)
        } else if (value === '3D') {
            console.log('3d')
            const d: _TData[] = dataAll.current.filter((item: _TData) => item.type === '3d')
            console.log(d)
            setData(d)
        } else if (value === 'VR360') {
            console.log('360')
            const d: _TData[] = dataAll.current.filter((item: _TData) => item.type === 'vr')
            setData(d)
        } else {
            setData(dataAll.current)
        }
    };

    const handleDeleteFile = (id: string) => {
        console.log(id)
        const d: _TData[] = data.filter((item: _TData) => item.id !== id)
        setData(d)
    }

    const columns = [
        {
            title: 'Tên',
            dataIndex: 'name',
        },
        {
            title: 'Loại',
            dataIndex: 'type',
        },
        {
            title: 'Dung lượng',
            dataIndex: 'size',
            render: (item) => (
                <>{Utils.formatByte(item)}</>
            )
        },
        {
            title: 'Files',
            dataIndex: 'files',
            render: (item) => {
                console.log('iii: ', item)
                return item.map(i => (
                    <p style={{margin: 0}} key={i.id}>{i.name} - {Utils.formatByte(i.size)}</p>
                ))
            }
        },
        {
            title: 'Ngày tải lên',
            dataIndex: 'created_at',
            render: (item) => (
                <>
                    {moment(item).format(App.FormatToDate) + " - " + moment(item).format(App.FormatToTime)}
                </>
            )
        },
        {
            title: 'Hành động',
            dataIndex: 'operation',
            render: (_, record: { id: string, name: string }) =>
                data.length >= 1 ? (
                    <Popconfirm title="Bạn có chắc chắn xoá không?" onConfirm={() => handleDeleteFile(record.id)}>
                        <a>Xoá</a>
                    </Popconfirm>
                ) : null,
        },
    ]

    return (
        <WrapContentWidget
            masterHeader={{
                title: title,
                isLoading: vm.isLoading,
                onReload: onClickReload,
            }}
            bodyHeader={{
                title: "Quản lý",
                right: _buildForm(),
            }}
        >
            <div className="flex items-center justify-between pb-6 border-custom mb-6">
                <Typography.Title className={"typoZ"} level={5}>File của tôi</Typography.Title>
                <Select
                    defaultValue={select}
                    style={{width: 180}}
                    size={"large"}
                    onChange={handleChange}
                    options={[
                        {
                            value: 'All',
                            label: 'Tất cả',
                        },
                        {
                            value: '2D',
                            label: '2D',
                        },
                        {
                            value: '3D',
                            label: '3D',
                        },
                        {
                            value: 'VR360',
                            label: 'VR360',
                        },
                    ]}
                />
            </div>
            <Table
                bordered
                dataSource={data}
                columns={columns}
            />
        </WrapContentWidget>
    )
}
