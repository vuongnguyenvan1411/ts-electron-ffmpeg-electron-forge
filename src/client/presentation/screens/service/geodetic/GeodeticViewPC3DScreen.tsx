import {lazy, Suspense, useEffect, useMemo} from "react";
import {useTranslation} from "react-i18next";
import {useLocation, useNavigate} from "react-router";
import {Color} from "../../../../const/Color";
import {CHashids} from "../../../../core/CHashids";
import {useParams} from "react-router-dom";
import {RouteAction} from "../../../../const/RouteAction";
import {MasterLayout} from "../../../layouts/MasterLayout";
import {ErrorItemFC} from "../../../widgets/ErrorItemFC";
import {SendingStatus} from "../../../../const/Events";
import {CommonEmptyFC, CommonLoadingSpinFC} from "../../../widgets/CommonFC";
import {Map3DModel} from "../../../../models/service/geodetic/Map3DModel";
import ErrorBoundaryComponent from "../../../widgets/ErrorBoundaryComponent";
import {TParamPartGeodetic} from "../../../../const/Types";
import {ContributorFC} from "../../../widgets/ContributorFC";
import {GeodeticView3DAction} from "../../../../recoil/service/geodetic/geodetic_view_3d/GeodeticView3DAction";
import {FeatureSingleton} from "../../../../models/FeatureSingleton";

const ThreeView = lazy(() => import("../../../modules/pc3d/ThreeView"));

type TParamState = {
    name: string
    parts?: TParamPartGeodetic
}

export const GeodeticViewPC3DScreen = (props: {
    state?: TParamState
}) => {
    const {t} = useTranslation()
    const navigate = useNavigate()
    const location = useLocation()
    const params = (location.state ?? props.state) as TParamState
    const {hash} = useParams<{ hash: string }>()

    const decode = CHashids.decode(hash!);
    const [spaceId, m3dId] = decode;

    const {
        vm,
        onLoadItem,
        onClearState,
        resetStateWithEffect,
    } = GeodeticView3DAction();

    useEffect(() => {
        console.log('%cMount Screen: GeodeticViewPC3DScreen', Color.ConsoleInfo);

        if (params) {
            document.title = params.name;
        } else {
            document.title = t('title.geodetic');
        }

        if (vm.item) resetStateWithEffect()
        onLoadItem(spaceId, m3dId)

        return () => {
            onClearState();

            console.log('%cUnmount Screen: GeodeticViewPC3DScreen', Color.ConsoleInfo)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [hash]);

    const getThreeView = useMemo(() => {
        if (vm.item instanceof Map3DModel) {
            return (
                <>
                    <ErrorBoundaryComponent>
                        <Suspense fallback={<CommonLoadingSpinFC/>}>
                            <ThreeView
                                m3dId={m3dId}
                                item={vm.item}
                                parts={params ? params.parts : undefined}
                            />
                        </Suspense>
                    </ErrorBoundaryComponent>
                    {
                        FeatureSingleton.getInstance().isContributor ? <ContributorFC/> : null
                    }
                </>
            )
        } else {
            return <CommonEmptyFC/>;
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.item])

    return (
        <MasterLayout
            siderType={"out"}
            onBack={() => navigate(RouteAction.GoBack())}
            sider={false}
            contentClassName={"m-0"}
            header={FeatureSingleton.getInstance().isHeaderEmbedWebView ? undefined : null}
            footer={null}
        >
            <ErrorItemFC
                status={vm.isLoading}
            >
                {
                    vm.isLoading === SendingStatus.loading
                        ? <CommonLoadingSpinFC/>
                        : getThreeView
                }
            </ErrorItemFC>
        </MasterLayout>
    );
}
