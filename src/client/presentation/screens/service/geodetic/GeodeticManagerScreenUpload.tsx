import {WrapContentWidget} from "../../../widgets/WrapContentWidget";
import {useTranslation} from "react-i18next";
import {GeodeticsAction} from "../../../../recoil/service/geodetic/geodetics/GeodeticsAction";
import {Button, Form, Input, message, Progress, Select, Spin, Typography, Upload} from "antd";
import React, {useEffect, useRef, useState} from "react";
import {CheckCircleOutlined, DeleteOutlined, UploadOutlined} from "@ant-design/icons";
import {useNavigate} from "react-router";
import {BigUp, T_BigUpOptions} from "../../../../core/upload/BigUp";
import {RouteConfig} from "../../../../config/RouteConfig";
import {EDData} from "../../../../core/encrypt/EDData";
import moment from "moment";
import {BigUpFile} from "../../../../core/upload/BigUpFile";
import {AxiosClient} from "../../../../repositories/AxiosClient";
import {App} from "../../../../const/App";
import {ApiResModel} from "../../../../models/ApiResModel";
import useDebounce from "../../../hooks/useDebounce";
import {Utils} from "../../../../core/Utils";

type _TFIle = {
    uid: string,
    percent: number,
    name: string,
    status: string,
    thumbUrl: string,
    type: string,
    size: number,
    originFileObj: any,
    isValid: boolean
}

export const GeodeticManagerScreenUpload = () => {
    const {t} = useTranslation()
    const [per, setPer] = useState({})
    const [select, setSelect] = useState<string>('required')
    const [isLoading, setIsLoading] = useState<boolean>(false)
    const [isDisable, setIsDisable] = useState<boolean>(true)
    const [listFile, setListFile] = useState<_TFIle[]>([])
    const [isHandle, setIsHandle] = useState<boolean>(false)
    const [isDone, setIsDone] = useState<boolean>(true)
    const currentFiles = useRef<any[]>([])
    const currentIndex = useRef<number>(0)
    const nameCurrent = useRef<string>('')
    const lengthFiles = useRef<number>(0)

    const {
        vm,
    } = GeodeticsAction()

    const title = t('title.geodetic')
    const navigate = useNavigate()
    const bigUpRef = useRef<BigUp>()

    const isDebounce = useDebounce(listFile, 500)

    const getType = (name: string) => {
        const type = name.split('.')
        return type[type.length - 1]
    }

    useEffect(() => {
        lengthFiles.current = isDebounce.length
    }, [isDebounce])

    useEffect(() => {

        const opts: T_BigUpOptions = {
            targetUpload: 'https://api-gfile.autotimelapse.com/v1/upload/chunk',
            permanentErrors: [404, 500, 501],
            maxChunkRetries: 1,
            chunkRetryInterval: 5000,
            simultaneousUploads: 4,
            chunkSize: 1024 * 1024,
            headers: {
                Platform: 'web'
            },
        }

        if (process.env.NEXT_PUBLIC_API_GFILE_TOKEN) {
            const et = EDData.setData({
                t: process.env.NEXT_PUBLIC_API_GFILE_TOKEN,
                // e: moment().add(30, 'seconds').unix()
                e: moment().add(1, 'days').unix()
            })

            opts.headers = {
                ...opts.headers,
                Authorization: `Bearer ${et}`
            }
        }

        const bigUp = new BigUp(opts)

        bigUp.on("fileProgress", (a, b) => {
            setIsLoading(true)
            // console.log("BBBBB", b)
            const process = Math.round(bigUp.progress() * 100)
            // console.log('progress: ', process + '%')
            if (process === 100) {
                setIsHandle(true)
            }

            // setPercent(Math.round(bigUp.progress() * 100))

            if (b?.fileObj?.name === BigUpFile.slugName(currentFiles.current[currentIndex.current]?.name ?? '')) {
                nameCurrent.current = b.fileObj.name
                const progress = Math.round(b.fileObj.progress() * 100)
                const time = Utils.formatTime(b.fileObj.timeRemaining())

                setPer(prev => (
                    {
                        ...prev,
                        [`${nameCurrent.current.split('.')[0]}`]: {
                            percent: progress,
                            time: time
                        }
                    }
                ))
                console.log(b.fileObj.name + ": " + progress)
                console.log('time: ', b.fileObj.timeRemaining())
                if (progress === 100) {
                    currentIndex.current += 1
                }
            }
        })

        bigUp.on("fileSuccess", (file, mes) => {
            console.log('fileSuccess', file, mes)
            console.log('per: ', Math.round(bigUp.progress() * 100))
            if (Math.round(bigUp.progress() * 100) === 100) {
                // setProcessUpload(false)
                window.onbeforeunload = null;
                message.success("Successfully!").then()
                navigate(RouteConfig.GEODETIC_MANAGER)
            }
        })

        bigUpRef.current = bigUp

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const createError = (item) => {
        const ele = document.getElementById(item.uid)

        if (ele) {
            const p = ele.querySelector('#error')

            if (p) ele.removeChild(p)

            const err = document.createElement('p')
            err.id = 'error'
            Object.assign(err.style, {
                color: 'red',
                margin: '0',
                marginTop: '8px'
            })
            err.innerText = 'Ảnh không đúng định dạng 360!'
            ele.appendChild(err)
        }
    }

    useEffect(() => {
        console.log(select)

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [select])

    useEffect(() => {
        if (select === 'vr') {

            console.log('change!')
            if (isDone) {
                listFile.filter(item => item.type !== 'image/png' && item.type !== 'image/tiff' && getType(item.name) !== 'slpk' && getType(item.name) !== 'las').forEach(item => {
                    let img = new Image()
                    img.src = window.URL.createObjectURL(item.originFileObj)
                    img.onload = () => {
                        if (img.width / img.height !== 2) {

                            item.isValid = false
                            createError(item)
                        } else {
                            item.isValid = true
                        }
                        return true
                    }

                    if (getType(item.name) === 'las') {
                        item.isValid = false
                    } else if (getType(item.name) === 'slpk') {
                        item.isValid = false
                    }

                })
            }
        } else if (select === '2d') {
            listFile.filter(item => item.type !== 'image/tiff').forEach(i => {
                i.isValid = false
                const ele = document.getElementById(i.uid)

                if (ele) {
                    const p = ele.querySelector('#error')

                    if (p) ele.removeChild(p)

                    const err = document.createElement('p')
                    err.id = 'error'
                    Object.assign(err.style, {
                        color: 'red',
                        margin: '0',
                        marginTop: '8px'
                    })
                    err.innerText = 'File không đúng định dạng 2D!'
                    ele.appendChild(err)
                }
            })
        } else if (select === '3d') {
            listFile.filter(item => getType(item.name) !== 'slpk' && getType(item.name) !== 'las').forEach(iz => {
                iz.isValid = false
                const ele = document.getElementById(iz.uid)

                if (ele) {
                    const p = ele.querySelector('#error')

                    if (p) ele.removeChild(p)

                    const err = document.createElement('p')
                    err.id = 'error'
                    Object.assign(err.style, {
                        color: 'red',
                        margin: '0',
                        marginTop: '8px'
                    })
                    err.innerText = 'File không đúng định dạng 3D!'
                    ele.appendChild(err)
                }
            })
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isDebounce])

    useEffect(() => {
        return () => {
            window.onbeforeunload = null;
        };
    }, []);

    const handleChange = (value: string) => {
        if (value !== 'required') {
            setIsDisable(false)
        } else {
            setIsDisable(true)
        }
        setSelect(value)
    }

    const onClickReload = () => {
        console.log("reload!")
    }

    const normFile = (e: any) => {
        console.log('e files: ', e)

        if (lengthFiles.current < e.fileList.length) {
            setIsDone(true)
        }

        if (e.fileList.length === 0) {
            setIsDone(true)
        }
        setListFile(e.fileList)

        return e?.fileList
    };

    const onFinish = (values: any) => {

        const isCheck = values.upload.every(item => item.type !== 'image/png' && item.isValid !== false)

        AxiosClient.post('platform/file', {
            type: select,
            name: values.name
        })
            .then((data: ApiResModel) => {
                if (isCheck && data.success) {
                    const upload = values.upload.map(item => item.originFileObj)

                    window.onbeforeunload = function () {
                        return true
                    };
                    currentFiles.current = upload

                    if (bigUpRef.current) {
                        bigUpRef.current.opts.query = {
                            ...bigUpRef.current.opts.query,
                            id: data.data?.id,
                            created_at: moment(data.data?.created_at, App.FormatISOFromMoment).unix(),
                        }
                        bigUpRef.current.addFiles(upload, new InputEvent('input'))
                        bigUpRef.current.upload()
                    }
                } else {
                    message.error("Có lỗi! Vui lòng kiểm tra lại!").then()
                }
            })
            .catch(err => console.log(err))
    }

    return (
        <>
            <WrapContentWidget
                masterHeader={{
                    title: title,
                    isLoading: vm.isLoading,
                    onReload: onClickReload,
                }}
                bodyHeader={{
                    title: "Tải File",
                    right: <Select
                        defaultValue={select}
                        style={{width: 180, fontSize: 15}}
                        size={"large"}
                        onChange={handleChange}
                        options={[
                            {
                                value: 'required',
                                label: 'Chọn loại file',
                            },
                            {
                                value: '2d',
                                label: '2D',
                            },
                            {
                                value: '3d',
                                label: '3D',
                            },
                            {
                                value: 'vr',
                                label: 'VR360',
                            },
                        ]}
                    />
                }}
            >
                <div className="flex h-full">
                    <div className="m-auto w-2/3 py-6">
                        <Form
                            labelCol={{span: 6}}
                            wrapperCol={{span: 14}}
                            onFinish={onFinish}
                        >
                            <Form.Item
                                label={"Tên File"}
                                name="name"
                                rules={[{required: true, message: "Vui lòng nhập trường này!"}]}
                            >
                                <Input size={"middle"}/>
                            </Form.Item>
                            <Form.Item
                                name="upload"
                                label="Upload"
                                valuePropName="fileList"
                                getValueFromEvent={normFile}
                                extra={isDisable
                                    ? "Vui lòng chọn loại file!"
                                    : select === '2d' || select === '3d'
                                        ? "Upload tối đa 2 files"
                                        : select === 'vr'
                                            ? "Upload tối đa 10 files"
                                            : ""}
                                rules={[{required: true, message: "Trường này là bắt buộc!"}]}
                            >
                                <Upload
                                    maxCount={select === 'vr' ? 10 : select === '2d' || select === '3d' ? 2 : 999}
                                    customRequest={() => {
                                    }}
                                    previewFile={(() => {
                                        return Promise.resolve('')
                                    })}
                                    disabled={isDisable}
                                    iconRender={() => <CheckCircleOutlined/>}
                                    itemRender={(_, item: any, ___, actions) => {
                                        return (
                                            <>
                                                <div className={`flex items-center justify-between mt-4 px-4 py-4 border ${item.type === 'image/png' ? 'border-red-500' : 'border-dark3'} border-solid`}>
                                                    <div id={`${item.uid}`}>
                                                        <div className={"flex items-center"}>
                                                            {/* eslint-disable-next-line @next/next/no-img-element */}
                                                            <img className={"mr-4 object-cover"} src={"https://www.namepros.com/attachments/empty-png.89209/"} width={64} height={32} alt={""}/>
                                                            <Typography.Title level={5}>{item.name} </Typography.Title>
                                                        </div>
                                                        {
                                                            item.type === 'image/png' && <Typography style={{color: 'red'}}>Định dạng file này không được hỗ trợ!</Typography>
                                                        }
                                                        {
                                                            select === 'vr' && item.type === 'image/tiff' && <Typography style={{color: 'red'}}>Định dạng file này không được hỗ trợ!</Typography>
                                                        }
                                                        {
                                                            select === 'vr' && getType(item.name) === 'slpk' && <Typography style={{color: 'red'}}>Định dạng file này không được hỗ trợ!</Typography>
                                                        }
                                                        {
                                                            select === 'vr' && getType(item.name) === 'las' && <Typography style={{color: 'red'}}>Định dạng file này không được hỗ trợ!</Typography>
                                                        }
                                                    </div>
                                                    <div onClick={() => {
                                                        setIsDone(false)
                                                        actions.remove()
                                                    }}><DeleteOutlined style={{fontSize: 24, cursor: 'pointer'}}/></div>
                                                </div>
                                                {
                                                    isLoading && (
                                                        <div>
                                                            <Progress percent={per[BigUpFile.slugName(item.name).split('.')[0]]?.percent ?? 0}/>
                                                            {
                                                                per[BigUpFile.slugName(item.name).split('.')[0]]?.percent < 100
                                                                    ? <p>{per[BigUpFile.slugName(item.name).split('.')[0]]?.time ?? ''}</p>
                                                                    : <></>
                                                            }
                                                        </div>
                                                    )
                                                }
                                            </>
                                        )
                                    }} name="logo" action="" listType="picture" multiple accept=".tif, .tiff, .btf, .tf8, .bigtiff, .jpg, .jpeg, .psd, .psb, .kro, .las, .slpk, .png">
                                    <Button icon={<UploadOutlined/>}>Click to upload</Button>
                                </Upload>
                            </Form.Item>
                            <Form.Item className={"btn-submit-manager"} wrapperCol={{offset: 6, span: 14}}>
                                <Button disabled={isLoading} type="primary" htmlType="submit">
                                    {
                                        isLoading ? 'Xin chờ' : 'Upload'
                                    }
                                </Button>
                            </Form.Item>
                        </Form>
                    </div>
                </div>
            </WrapContentWidget>
            {
                isHandle && (
                    <div className="fixed inset-0 flex" style={{
                        backgroundColor: 'rgba(0,0,0,0.5)'
                    }}>
                        <div className="m-auto w-32 h-32 text-white flex justify-center items-center">
                            <Spin style={{color: '#fff'}} tip={'Đang xử lý...'} size="large">
                            </Spin>
                        </div>
                    </div>
                )
            }

        </>
    )
}
