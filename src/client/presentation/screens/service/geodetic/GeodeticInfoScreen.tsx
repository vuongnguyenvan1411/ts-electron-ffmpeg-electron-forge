import {useTranslation} from "react-i18next";
import {useCallback, useEffect, useState} from "react";
import {Divider, Pagination} from "antd";
import {SendingStatus} from "../../../../const/Events";
import {Color} from "../../../../const/Color";
import {useParams} from "react-router-dom";
import {CHashids} from "../../../../core/CHashids";
import {RouteAction} from "../../../../const/RouteAction";
import {useLocation, useNavigate} from "react-router";
import {ErrorItemFC} from "../../../widgets/ErrorItemFC";
import {Geodetic3DItemFC, GeodeticInfoItemFC, GeodeticInfoSkeleton} from "../../../widgets/GeodeticInfoItemFC";
import {CommonEmptyFC} from "../../../widgets/CommonFC";
import {Utils} from "../../../../core/Utils";
import {SpaceModel, TM2d, TM3d, TVr360} from "../../../../models/service/geodetic/SpaceModel";
import {TParamPartGeodetic} from "../../../../const/Types";
import {GeodeticInfoAction} from "../../../../recoil/service/geodetic/geodetic_info/GeodeticInfoAction";
import styles from "../../../../styles/module/Geodetic.module.scss"
import {CustomTypography} from "../../../components/CustomTypography";
import {MediaQuery} from "../../../../core/MediaQuery";
import {WrapContentWidget} from "../../../widgets/WrapContentWidget";
import {GridLoadingWidget} from "../../../widgets/GridLoadingWidget";
import NextImage from "next/image";
import {Images} from "../../../../const/Images";

type TParamState = {
    spaceId: number
    name: string
    image: string
    item: any
}

export const GeodeticInfoScreen = (props: {
    state?: TParamState
}) => {
    const {t} = useTranslation()
    const navigate = useNavigate()
    const location = useLocation()
    const {hash} = useParams<{ hash: string }>()
    const params = (location.state ?? props.state) as TParamState
    const spaceId = CHashids.decodeGetFirst(hash!)
    const [page2d, setPage2d] = useState(1);
    const [page3d, setPage3d] = useState(1);
    const [pageVr360, setPageVr360] = useState(1);

    const {
        vm,
        onLoadItem,
        resetStateWithEffect,
    } = GeodeticInfoAction()

    const title = params !== undefined ? params.name : t('text.info')

    if (spaceId === null) {
        navigate(RouteAction.GoBack())
    }

    useEffect(() => {
        console.log('%cMount Screen: GeodeticInfoScreen', Color.ConsoleInfo);

        if (
            vm.item === null
            || vm.key !== spaceId!.toString()
            || (vm.timestamp !== undefined && Utils.checkHourState(vm.timestamp))
        ) {
            if (vm.item) resetStateWithEffect()
            onLoadItem(spaceId!);
        }

        return () => {
            console.log('%cUnmount Screen: GeodeticInfoScreen', Color.ConsoleInfo);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const onCLickReload = () => {
        if (spaceId) {
            onLoadItem(spaceId);
        }
    }

    const onChangePage2d = (page: number) => {
        setPage2d(page);
    }

    const onChangePage3d = (page: number) => {
        setPage3d(page);
    }

    const onChangeVr360 = (page: number) => {
        setPageVr360(page);
    }

    const _getM2d = (page: number, items: TM2d[]) => {
        const _number = (new MediaQuery({
            xs: 1,
            sm: 2,
            md: 3,
            lg: 3,
            xl: 3,
            xxl: 4,
        })).getPointOnUp(4);

        return items.slice((page - 1) * _number, page * _number);
    }

    const _getM3d = (page: number, items: TM3d[]) => {
        const _number = (new MediaQuery({
            xs: 1,
            sm: 2,
            md: 3,
            lg: 3,
            xl: 3,
            xxl: 4,
        })).getPointOnUp(4);

        return items.slice((page - 1) * _number, page * _number);
    }

    const _getVr360 = (page: number, items: TVr360[]) => {
        const _number = (new MediaQuery({
            xs: 1,
            sm: 2,
            md: 3,
            lg: 3,
            xl: 3,
            xxl: 4,
        })).getPointOnUp(4);

        return items.slice((page - 1) * _number, page * _number);
    }

    const RenderInfoItem = useCallback((props: {
        item: SpaceModel
    }) => {
        const parts: TParamPartGeodetic = {};

        if (props.item.m2d && props.item.m2d.length > 0) {
            parts.m2d = props.item.m2d.map((value) => ({
                spaceId: spaceId!,
                m2dId: value.m2dId!,
                name: value.name!
            }));
        }

        if (props.item.m3d && props.item.m3d.length > 0) {
            parts.m3d = props.item.m3d.map((value) => ({
                spaceId: spaceId!,
                m3dId: value.m3dId!,
                name: value.name!
            }));
        }

        if (props.item.vr360 && props.item.vr360.length > 0) {
            parts.vr360 = props.item.vr360.map((value) => ({
                spaceId: spaceId!,
                vr360Id: value.vr360Id!,
                name: value.name!
            }));
        }

        return (
            <div className={styles.Geodetic_Info}>
                {
                    props.item.m2d && props.item.m2d.length > 0
                        ? <div className={styles.Geodetic_Info_Card}>
                            <div className={styles.Geodetic_Info_Card_Header}>
                                <div className={styles.Geodetic_Info_Card_Header_Left}>
                                    <NextImage
                                        width={32}
                                        height={32}
                                        src={Images.iconGeodeticDark.data}
                                        alt={Images.iconGeodeticDark.atl}
                                    />
                                    <CustomTypography
                                        isStrong
                                        textStyle={"text-16-24"}
                                    >
                                        2D
                                    </CustomTypography>
                                </div>
                                {
                                    props.item.m2d.length > 4 && (
                                        <Pagination
                                            className={"pagination-main"}
                                            defaultCurrent={page2d}
                                            current={page2d}
                                            total={props.item.m2d.length}
                                            defaultPageSize={4}
                                            responsive={false}
                                            showLessItems={true}
                                            showSizeChanger={false}
                                            onChange={onChangePage2d}
                                        />
                                    )
                                }
                            </div>
                            <Divider className={"divider-main divider-horizontal-m-0 divider-border-half-666666"}/>
                            <div className={styles.Geodetic_Info_Card_Grid}>
                                {
                                    _getM2d(page2d, props.item.m2d).map((item, _) => {
                                        if (!item.m2dId) return null;

                                        return (
                                            <GeodeticInfoItemFC
                                                key={item.m2dId}
                                                spaceId={spaceId!}
                                                id={item.m2dId}
                                                src={item.image ?? ''}
                                                link={item.link ?? ''}
                                                type={'m2d'}
                                                name={item.name}
                                                parts={parts}
                                            />
                                        )
                                    })
                                }
                            </div>
                        </div>
                        : null
                }
                {
                    props.item.m3d && props.item.m3d.length > 0
                        ? <div className={styles.Geodetic_Info_Card}>
                            <div className={styles.Geodetic_Info_Card_Header}>
                                <div className={styles.Geodetic_Info_Card_Header_Left}>
                                    <NextImage
                                        width={32}
                                        height={32}
                                        src={Images.iconGeodeticDark.data}
                                        alt={Images.iconGeodeticDark.atl}
                                    />
                                    <CustomTypography
                                        isStrong
                                        textStyle={"text-16-24"}
                                    >
                                        3D
                                    </CustomTypography>
                                </div>
                                {
                                    props.item.m3d.length > 4 && (
                                        <Pagination
                                            className={"pagination-main"}
                                            defaultCurrent={page3d}
                                            current={page3d}
                                            total={props.item.m3d.length}
                                            defaultPageSize={4}
                                            responsive={false}
                                            showLessItems={true}
                                            showSizeChanger={false}
                                            onChange={onChangePage3d}
                                        />
                                    )
                                }
                            </div>
                            <Divider className={"divider-main divider-horizontal-m-0 divider-border-half-666666"}/>
                            <div className={styles.Geodetic_Info_Card_Grid}>
                                {
                                    _getM3d(page3d, props.item.m3d).map((item, _) => {
                                        if (!item.m3dId) return null;

                                        return (
                                            <Geodetic3DItemFC
                                                key={item.m3dId}
                                                spaceId={spaceId!}
                                                id={item.m3dId}
                                                src={item.image ?? ''}
                                                link={item.link ?? ''}
                                                type={'m3d'}
                                                name={item.name}
                                                data={item.data}
                                                parts={parts}
                                            />
                                        )
                                    })
                                }
                            </div>
                        </div>
                        : null
                }
                {
                    props.item.vr360 && props.item.vr360.length > 0
                        ? <div className={styles.Geodetic_Info_Card}>
                            <div className={styles.Geodetic_Info_Card_Header}>
                                <div className={styles.Geodetic_Info_Card_Header_Left}>
                                    <NextImage
                                        width={32}
                                        height={32}
                                        src={Images.iconVr360Dark.data}
                                        alt={Images.iconVr360Dark.atl}
                                    />
                                    <CustomTypography
                                        isStrong
                                        textStyle={"text-16-24"}
                                    >
                                        VR 360
                                    </CustomTypography>
                                </div>
                                {
                                    props.item.vr360.length > 4 && (
                                        <Pagination
                                            className={"pagination-main"}
                                            defaultCurrent={pageVr360}
                                            current={pageVr360}
                                            total={props.item.vr360.length}
                                            defaultPageSize={4}
                                            responsive={false}
                                            showLessItems={true}
                                            showSizeChanger={false}
                                            onChange={onChangeVr360}
                                        />
                                    )
                                }
                            </div>
                            <Divider className={"divider-main divider-horizontal-m-0 divider-border-half-666666"}/>
                            <div className={styles.Geodetic_Info_Card_Grid}>
                                {
                                    _getVr360(pageVr360, props.item.vr360).map((item, _) => {
                                        if (!item.vr360Id) return null;

                                        return (
                                            <GeodeticInfoItemFC
                                                key={item.vr360Id}
                                                spaceId={spaceId!}
                                                id={item.vr360Id}
                                                src={item.image ?? ''}
                                                link={item.link ?? ''}
                                                type={'vr360'}
                                                name={item.name}
                                                parts={parts}
                                            />
                                        )
                                    })
                                }
                            </div>
                        </div>
                        : null
                }
            </div>
        )
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [page2d, page3d, pageVr360])

    useEffect(() => {
        navigate({}, {
            replace: true,
            state: {
                name: vm.item?.name,
                m2d: vm.item?.m2d,
                m3d: vm.item?.m3d,
                vr360: vm.item?.vr360,
            }
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.item])

    const _buildLoading = () => {
        return <div className={styles.Geodetic_Info}>
            <div className={styles.Geodetic_Info_Card}>
                <div className={styles.Geodetic_Info_Card_Header}>
                    <div className={styles.Geodetic_Info_Card_Header_Left}>
                        <div className={"w-8 h-8 bg-gray-300 animate-pulse"}/>
                        <div className={"w-8 h-4 bg-gray-300 animate-pulse"}/>
                    </div>
                    <div className={"w-32 h-8 bg-gray-300 animate-pulse"}/>
                </div>
                <Divider className={"divider-main divider-horizontal-m-0 divider-border-half-666666"}/>
                <div className={styles.Geodetic_Info_Card_Grid}>
                    <GridLoadingWidget>
                        <GeodeticInfoSkeleton/>
                    </GridLoadingWidget>
                </div>
            </div>
        </div>
    }


    return (
        <WrapContentWidget
            masterHeader={{
                title: title,
                isLoading: vm.isLoading,
                onReload: onCLickReload,
            }}
            bodyHeader={{
                title: title,
            }}
        >
            <ErrorItemFC
                status={vm.isLoading}
            >
                {
                    vm.isLoading === SendingStatus.success
                        ? vm.item instanceof SpaceModel
                            ? <RenderInfoItem item={vm.item}/>
                            : <CommonEmptyFC/>
                        : _buildLoading()
                }
            </ErrorItemFC>
        </WrapContentWidget>
    )
}
