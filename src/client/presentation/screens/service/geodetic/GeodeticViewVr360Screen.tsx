import {Navigate, useLocation, useNavigate, useParams} from "react-router-dom";
import React, {lazy, Suspense, useCallback, useEffect, useState} from "react";
import {Color} from "../../../../const/Color";
import ErrorBoundaryComponent from "../../../widgets/ErrorBoundaryComponent";
import {CommonLoadingSpinFC} from "../../../widgets/CommonFC";
import {Alert, Button, Card, DatePicker, Form, Input, Modal, notification, Space, Switch, Table, Tag, Tooltip, Typography} from "antd";
import {ArrowLeftOutlined, CheckOutlined, CloseOutlined, CopyOutlined, DeleteTwoTone, EditOutlined, EyeOutlined, LinkOutlined, PlusCircleOutlined, QrcodeOutlined, SaveOutlined, ShareAltOutlined} from "@ant-design/icons";
import {RouteConfig} from "../../../../config/RouteConfig";
import {TParamPartGeodetic} from "../../../../const/Types";
import {GeodeticPartMenuFC} from "../../../widgets/GeodeticPartMenuFC";
import {useTranslation} from "react-i18next";
import {CHashids} from "../../../../core/CHashids";
import {SendingStatus} from "../../../../const/Events";
import moment from "moment";
import {ColumnsType} from "antd/es/table";
import {GetQrCodeModalFC} from "../../../widgets/GetQrCodeModalFC";
import {Utils} from "../../../../core/Utils";
import {App} from "../../../../const/App";
import {BaseShareModel, TMap360ShareV0} from "../../../../models/ShareModel";
import {RouteAction} from "../../../../const/RouteAction";
import {ContributorFC} from "../../../widgets/ContributorFC";
import {useNavigationType} from "react-router";
import {Map360ShareAction} from "../../../../recoil/service/geodetic/map_360_share/Map360ShareAction";
import {useConfigContext} from "../../../contexts/ConfigContext";
import {FeatureSingleton} from "../../../../models/FeatureSingleton";

const EmbedPano = lazy(() => import("../../../modules/vr360/EmbedPano"));

interface ModalFormMainProps {
    vr360Id: number | null,
    sid?: string,
    visible: boolean,
    preview: boolean,
    onCancel: () => void,
    initFormValues?: BaseShareModel,
}

type TParamState = {
    name: string;
    link: string;
    createdAt: number;
    parts?: TParamPartGeodetic
}

export const GeodeticViewVr360Screen = (props: {
    state?: TParamState
}) => {
    const navigate = useNavigate()
    const location = useLocation()
    const {hash} = useParams<{ hash: string }>()
    const params = (location.state ?? props.state) as TParamState
    const [config] = useConfigContext()

    const decode = CHashids.decode(hash!)
    const vr360Id = decode[1]

    const [isShare2dModalVisible, setIsShare2dModalVisible] = useState(false)

    useEffect(() => {
        console.log('%cMount Screen: GeodeticViewVr360Screen', Color.ConsoleInfo);

        if (params !== undefined) {
            document.title = params.name;
        }

        return () => {
            console.log('%cUnmount Screen: GeodeticViewVr360Screen', Color.ConsoleInfo);
        };

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onClickShareModal = () => {
        setIsShare2dModalVisible(true);
    }

    const onCloseShareModal = () => {
        setIsShare2dModalVisible(false);
    }

    return (
        params && vr360Id !== null
            ? <>
                <ErrorBoundaryComponent>
                    <Suspense fallback={<CommonLoadingSpinFC/>}>
                        <EmbedPano
                            vr360Id={vr360Id}
                            name={params.name}
                            createdAt={params.createdAt}
                        />
                    </Suspense>
                </ErrorBoundaryComponent>
                <div
                    className={"absolute z-50 top-2 left-2"}
                >
                    <Space
                        direction={"vertical"}
                    >
                        {
                            !(config.agent?.isWebAppInApp()) && (
                                <div>
                                    <Button
                                        type="primary"
                                        shape="circle"
                                        icon={<ArrowLeftOutlined/>}
                                        onClick={() => navigate(RouteAction.GoBack())}
                                    />
                                    <span className={"ml-3"}>{params.name}</span>
                                </div>
                            )
                        }
                        <GeodeticPartMenuFC
                            parts={params ? params.parts : undefined}
                            active={{
                                type: 'vr360',
                                id: vr360Id
                            }}
                        />
                        {
                            FeatureSingleton.getInstance().v360Share
                                ? <>
                                    <Button
                                        type={"primary"}
                                        icon={<ShareAltOutlined/>}
                                        onClick={onClickShareModal}
                                    />
                                    {
                                        isShare2dModalVisible
                                            ? <Vr360ShareModalFC
                                                onClose={onCloseShareModal}
                                                isShare2dModalVisible={isShare2dModalVisible}
                                            />
                                            : null
                                    }
                                </>
                                : null
                        }
                    </Space>
                </div>
                {
                    FeatureSingleton.getInstance().isContributor ? <ContributorFC/> : null
                }
            </>
            : <Navigate to={RouteConfig.GEODETIC}/>
    );
}

const Vr360ShareModalFC = React.memo((props: {
    onClose: () => void,
    isShare2dModalVisible: boolean,
}) => {
    const {t} = useTranslation()
    const navigateType = useNavigationType()
    const location = useLocation()
    const {hash} = useParams<{ hash: string }>()

    const decode = CHashids.decode(hash!)
    const vr360Id = decode[1] as number

    const {
        vm,
        onLoadItems,
        onDeleteItem,
        onClearState,
    } = Map360ShareAction();

    const [formVisible, setFormVisible] = useState<{
        sid?: string,
        visible: boolean,
        preview: boolean,
        initValues?: BaseShareModel,
    }>({
        visible: false,
        preview: false,
    });
    const [visibleDelete, setVisibleDelete] = useState<{
        sid?: string,
        visible: boolean,
    }>({
        visible: false,
    });
    const [isCopyOk, setIsCopyOk] = useState(false);

    useEffect(() => {
        console.log('%cMount Screen: Vr360ShareModalFC', Color.ConsoleInfo);

        if (
            vm.items === null || vm.key !== vr360Id!.toString()
            || (vm.timestamp !== undefined && Utils.checkHourState(vm.timestamp))
        ) {
            onLoadItems(vr360Id, queryParams);
        }

        return () => {
            console.log('%cUnmount Screen: Vr360ShareModalFC', Color.ConsoleInfo);
        };

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (vm.isUpdating === SendingStatus.success) {
            setFormVisible(
                {
                    ...formVisible,
                    visible: false,
                }
            )

            notification.success({
                message: t('text.successSetting'),
            });
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.isUpdating]);

    useEffect(() => {
        return () => {
            if (
                navigateType === RouteAction.PUSH
                || navigateType === RouteAction.REPLACE
                || navigateType === RouteAction.POP
            ) {
                if (location.pathname.indexOf(RouteConfig.GEODETIC_VIEW_2D) !== 0) {
                    onClearState();
                }
            }
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [navigateType]);

    useEffect(() => {
        if (vm.isDeleting === SendingStatus.success) {
            setVisibleDelete(
                {
                    ...visibleDelete,
                    visible: false,
                }
            )

            notification.success({
                message: t('success.delete'),
            });
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.isDeleting]);

    const [, forceUpdate] = useState({});

    useEffect(() => {
        forceUpdate({});
    }, []);

    const onHandleCancelDelete = () => {
        setVisibleDelete({
            ...visibleDelete,
            visible: false,
        })
    };
    const onHandleOkDelete = useCallback(() => {
        if (vr360Id && visibleDelete.sid) {
            onDeleteItem(vr360Id, visibleDelete.sid)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [visibleDelete, visibleDelete.sid]);

    const onShowDelete = (sid: string) => {
        setVisibleDelete({
            ...visibleDelete,
            sid: sid,
            visible: true,
        })
    };

    const showUserModal = (sid?: string) => {
        setFormVisible({
            ...formVisible,
            sid: sid,
            visible: true,
            preview: false,
            initValues: undefined,
        })
    };

    const hideUserModal = () => {
        setFormVisible({
            ...formVisible,
            visible: false,
            preview: false,
        })
    };

    const onPreviewRowTable = (item: BaseShareModel) => {
        setFormVisible({
            ...formVisible,
            sid: item.sid,
            visible: true,
            preview: true,
            initValues: item,
        })
    }

    const onClickCopy = (sid: string) => {
        vm.items?.forEach(item => {
            if (item.sid === sid) {
                Utils.copyClipboard(item.link).then(() => {
                    setIsCopyOk(true);

                    notification.success({
                        message: t('text.successCopy'),
                        description: item.link
                    });

                    setTimeout(_ => setIsCopyOk(false), App.TimeoutHideCopy)
                });
            }
        })
    }

    const onEditTable = (item: BaseShareModel) => {
        setFormVisible({
            ...formVisible,
            sid: item.sid,
            visible: true,
            preview: false,
            initValues: item,
        });
    }

    const columns: ColumnsType<BaseShareModel> = [
        {
            key: "name",
            title: t("text.name"),
            dataIndex: "name"
        },
        {
            key: "status",
            title: t("text.status"),
            render: (_, item, __) => (
                <Tag key={`tag_status_${item.sid}`} color={item.status ? 'success' : 'red'}>
                    {item.status ? t("text.active") : t("text.inActive")}
                </Tag>
            )
        },
        {
            key: "alive",
            title: t("text.activeTime"),
            render: (_, item) => {
                return (
                    item.expiryDate && item.expiryDate.start && item.expiryDate.end
                        ? <Space direction={"vertical"} style={{gap: 0}}>
                            <Typography.Text>{t("text.dateStart")}:&nbsp;&nbsp;{item.dateStartFormatted(t('format.dateTimeShort'))}</Typography.Text>
                            <Typography.Text>{t("text.dateEnd")}:&nbsp;&nbsp;{item.dateEndFormatted(t('format.dateTimeShort'))}</Typography.Text>
                        </Space>
                        : item.expiryDate?.start
                            ?
                            <Typography.Text>{t("text.dateStart")}:&nbsp;&nbsp;{item.dateStartFormatted(t('format.dateTimeShort'))}</Typography.Text>
                            : item.expiryDate?.end
                                ?
                                <Typography.Text>{t("text.dateEnd")}:&nbsp;&nbsp;{item.dateEndFormatted(t('format.dateTimeShort'))}</Typography.Text>
                                : <Typography.Text>{t("text.unlimited")}</Typography.Text>
                )
            }
        },
        {
            key: "qrCode",
            title: t('text.linkShare'),
            render: (_, item, __) => {
                return (
                    <Space>
                        <div
                            className={"ant-btn-group"}
                        >
                            <Tooltip
                                title={t('text.linkShare')}
                            >
                                <Button
                                    onClick={() => window.open(item.link, '_blank')}
                                    icon={<LinkOutlined/>}
                                />
                            </Tooltip>
                            <Button
                                type={'primary'}
                                onClick={() => onClickCopy(item.sid)}
                                icon={isCopyOk ? <CheckOutlined/> : <CopyOutlined/>}
                            />
                        </div>
                        <Tooltip
                            title={t('text.codeEmbedQrCode')}
                        >
                            <Button
                                onClick={onClickGetQrCode}
                                icon={<QrcodeOutlined/>}
                            />
                        </Tooltip>
                        {
                            isModalVisible
                                ? <GetQrCodeModalFC
                                    sid={item.sid}
                                    name={item.name}
                                    onClose={onCloseGetQrCode}
                                />
                                : null
                        }
                    </Space>
                )
            }
        },
        {
            key: "action",
            title: t("text.actions"),
            align: "right",
            render: (_, item, __) => {
                return (
                    <Space>
                        <Tooltip
                            title={t("button.view")}
                        >
                            <Button
                                type={"text"}
                                icon={<EyeOutlined/>}
                                onClick={() => onPreviewRowTable(item)}
                            />
                        </Tooltip>
                        <Tooltip
                            title={t("button.edit")}
                        >
                            <Button
                                type={"text"}
                                icon={<EditOutlined/>}
                                onClick={() => onEditTable(item)}
                            />
                        </Tooltip>

                        <Tooltip
                            title={t("button.delete")}
                        >
                            <Button
                                type={"text"}
                                icon={<DeleteTwoTone twoToneColor={Color.danger}/>}
                                onClick={() => onShowDelete(item.sid)}
                            />
                        </Tooltip>
                    </Space>
                )
            }
        }
    ]
    const [isModalVisible, setIsModalVisible] = useState(false);

    const onClickGetQrCode = () => {
        setIsModalVisible(true);
    }

    const onCloseGetQrCode = () => {
        setIsModalVisible(false);
    }

    if (vr360Id === null) {
        props.onClose()
    }

    const [queryParams, setQueryParams] = useState<{ page: number, limit: number }>({
        page: vm.query.page,
        limit: vm.query.limit,
    });
    const onChangePage = (page: number) => {
        setQueryParams({
            ...queryParams,
            page: page,
            limit: vm.query.limit,
        })

        if (vr360Id && vm.items && vm.items?.slice((page - 1) * vm.query.limit, page * vm.query.limit).length === 0)
            onLoadItems(vr360Id, {
                page: page,
                limit: queryParams.limit,
            })
    };

    const isUpdatingError = vm.isUpdating === SendingStatus.error;

    return (
        <>
            <Modal
                key={"list"}
                visible={props.isShare2dModalVisible}
                onCancel={props.onClose}
                title={t('text.createLink')}
                width={1000}
                onOk={props.onClose}
            >
                <Button
                    className={"mb-4"}
                    type={"primary"}
                    icon={<PlusCircleOutlined/>}
                    onClick={() => {
                        showUserModal();
                    }}
                >
                    {t("text.addSharingLink")}
                </Button>
                <Card
                    bodyStyle={{
                        padding: 0
                    }}
                >
                    {
                        isUpdatingError && vm.error && vm.error.hasOwnProperty('warning')
                            ? <Alert className={"mb-5"} message={vm.error['warning']} type="error" showIcon/>
                            : null
                    }
                    <Table<BaseShareModel>
                        loading={vm.isLoading === SendingStatus.loading}
                        columns={columns}
                        dataSource={vm.items}
                        pagination={
                            {
                                total: vm.oMeta?.totalCount ?? 0,
                                defaultPageSize: 5,
                                defaultCurrent: 1,
                                onChange: (page) => onChangePage(page),
                            }
                        }
                        rowKey={_ => _.sid}
                    />
                </Card>
            </Modal>
            <Vr360FormModalFC
                key={"edit"}
                visible={formVisible.visible}
                sid={formVisible.sid}
                vr360Id={vr360Id}
                onCancel={hideUserModal}
                initFormValues={formVisible.initValues}
                preview={formVisible.preview}
            />
            <Modal
                key={"delete"}
                title={t("title.confirmBeforeDeleting")}
                visible={visibleDelete.visible}
                onCancel={onHandleCancelDelete}
                confirmLoading={vm.isDeleting === SendingStatus.loading}
                onOk={onHandleOkDelete}
            >
                {t("message.confirmBeforeDeleting")}
            </Modal>
        </>
    )
})

const Vr360FormModalFC: React.FC<ModalFormMainProps> = ({
                                                            preview,
                                                            vr360Id,
                                                            sid,
                                                            visible,
                                                            onCancel,
                                                            initFormValues
                                                        }) => {
    const {t} = useTranslation();
    const [formMain] = Form.useForm();
    const [isPreview, setIsPreview] = useState(false);

    useEffect(() => {
        setIsPreview(preview);

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [preview])

    const {
        vm,
        onAddItem,
        onEditItem,
    } = Map360ShareAction();

    const onAfterClose = () => {
        formMain.resetFields();
    }

    const onOk = () => {
        formMain.submit();
        setIsPreview(false);
    }

    const onChangeToEdit = () => {
        setIsPreview(false);
    }

    const onFinish = (values: any) => {
        const serializable: TMap360ShareV0 = {
            name: values.name,
            password: values.password,
            dateStart: values['range-time-picker'] && values['range-time-picker'][0] ? moment(values['range-time-picker'][0]).format('YYYY-MM-DD HH:mm:ss').toString() : '',
            dateEnd: values['range-time-picker'] && values['range-time-picker'][1] ? moment(values['range-time-picker'][1]).format('YYYY-MM-DD HH:mm:ss').toString() : '',
            status: values.isShare,
            sInfo: values.shareInfo,
        }

        if (vr360Id) {
            if (sid) {
                onEditItem(vr360Id, sid, serializable)
            } else {
                onAddItem(vr360Id, serializable)
            }
        }
    };

    return (
        <Modal
            visible={visible}
            onCancel={() => {
                onCancel()
                setIsPreview(false)
            }}
            title={isPreview ? t("text.detailInfo") : !sid ? t("text.createLink") : t("text.editLink")}
            onOk={onOk}
            afterClose={onAfterClose}
            destroyOnClose={true}
            confirmLoading={vm.isLoading === SendingStatus.loading}
            width={700}
            footer={
                isPreview ?
                    [
                        <Button
                            key={"edit"}
                            type={"primary"}
                            onClick={onChangeToEdit}
                            icon={<EditOutlined/>}
                        >
                            {t("button.edit")}
                        </Button>,
                        <Button
                            key={"close"}
                            onClick={() => {
                                onCancel()
                                setIsPreview(false)
                            }}
                            icon={<CloseOutlined/>}
                        >
                            {t("button.close")}
                        </Button>
                    ] : [
                        <Button
                            key={"close"}
                            onClick={() => {
                                onCancel()
                                setIsPreview(false)
                            }}
                            icon={<CloseOutlined/>}
                        >
                            {t("button.close")}
                        </Button>,
                        <Button
                            key={"save"}
                            type={"primary"}
                            onClick={() => onOk()}
                            icon={<SaveOutlined/>}
                            loading={vm.isUpdating === SendingStatus.loading}
                        >
                            {sid ? t("button.save") : t("button.saveNew")}
                        </Button>
                    ]

            }
        >
            <Form
                key={'form_m2d'}
                form={formMain}
                labelCol={{
                    xs: {span: 24},
                    sm: {span: 8},
                    xxl: {span: 10},
                    xl: {span: 10},
                }}
                wrapperCol={{
                    xs: {span: 24},
                    sm: {span: 16},
                    xxl: {span: 14},
                    xl: {span: 14},
                }}
                onFinish={onFinish}
            >
                <Form.Item
                    labelAlign={"left"}
                    key={"k_name"}
                    label={t("text.name")}
                    name={"name"}
                    rules={[
                        {
                            required: true,
                            message: t("validation.emptyData"),
                        },
                        {
                            max: 100,
                            message: t('validation.minAndMaxCharacter', {
                                label: t("text.name"),
                                min: '1',
                                max: '100'
                            }),
                        }
                    ]}
                    initialValue={initFormValues?.name}
                >
                    <Input disabled={isPreview} allowClear={true}/>
                </Form.Item>
                <Form.Item
                    labelAlign={"left"}
                    key={"k_password"}
                    label={t("label.password")}
                    name={"password"}
                    initialValue={initFormValues?.password}
                >
                    <Input disabled={isPreview} allowClear={true}/>
                </Form.Item>
                <Form.Item
                    labelAlign={"left"}
                    key={"k_range-time-picker"}
                    name="range-time-picker"
                    label={t("text.activeTime")}
                    initialValue={[
                        initFormValues?.expiryDate?.start ? moment(initFormValues.expiryDate.start, App.FormatISOFromMoment) : null,
                        initFormValues?.expiryDate?.end ? moment(initFormValues.expiryDate.end, App.FormatISOFromMoment) : null
                    ]}
                >
                    <DatePicker.RangePicker
                        allowEmpty={[true, true]}
                        disabled={isPreview}
                        showTime
                        format={t("format.dateTime")}
                    />
                </Form.Item>
                <Form.Item
                    key={"isShare"}
                    label={t('text.sharingAllow')}
                    labelAlign={'left'}
                    name={"isShare"}
                    valuePropName={"checked"}
                    initialValue={initFormValues?.status ?? false}
                >
                    <Switch
                        disabled={isPreview}
                        defaultChecked={false}
                    />
                </Form.Item>
                <Form.Item
                    key={"sInfo"}
                    label={t('text.displayInformation')}
                    labelAlign={'left'}
                    name={"shareInfo"}
                    valuePropName={"checked"}
                    initialValue={initFormValues?.sInfo ?? false}
                >
                    <Switch
                        disabled={isPreview}
                        defaultChecked={false}
                    />
                </Form.Item>
            </Form>
        </Modal>
    )
}
