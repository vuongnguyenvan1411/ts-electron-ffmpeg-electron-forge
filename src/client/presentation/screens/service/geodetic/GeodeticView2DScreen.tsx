import {lazy, Suspense, useEffect, useMemo} from "react";
import {useTranslation} from "react-i18next";
import {useLocation} from "react-router";
import {Color} from "../../../../const/Color";
import {CHashids} from "../../../../core/CHashids";
import {useParams} from "react-router-dom";
import {ErrorItemFC} from "../../../widgets/ErrorItemFC";
import {SendingStatus} from "../../../../const/Events";
import {CommonEmptyFC, CommonLoadingSpinFC} from "../../../widgets/CommonFC";
import {Map2DModel} from "../../../../models/service/geodetic/Map2DModel";
import ErrorBoundaryComponent from "../../../widgets/ErrorBoundaryComponent";
import {TParamPartGeodetic} from "../../../../const/Types";
import {ContributorFC} from "../../../widgets/ContributorFC";
import {GeodeticView2DAction} from "../../../../recoil/service/geodetic/geodetic_view_2d/GeodeticView2DAction";
import {FeatureSingleton} from "../../../../models/FeatureSingleton";

const OLView = lazy(() => import("../../../modules/v2d/OLView"));

type TParamState = {
    name: string
    parts?: TParamPartGeodetic
}

export const GeodeticView2DScreen = (props: {
    state?: TParamState
}) => {
    const {t} = useTranslation()
    const location = useLocation()
    const params = (location.state ?? props.state) as TParamState
    const {hash} = useParams<{ hash: string }>()

    const decode = CHashids.decode(hash!)
    const [spaceId, m2dId] = decode

    const {
        vm,
        onLoadItem,
        onClearState,
        resetStateWithEffect,
    } = GeodeticView2DAction()

    useEffect(() => {
        console.log('%cMount Screen: GeodeticView2DScreen', Color.ConsoleInfo);

        if (params) {
            document.title = params.name;
        } else {
            document.title = t('title.geodetic');
        }

        if (vm.item) resetStateWithEffect()
        onLoadItem(spaceId, m2dId)

        return () => {
            onClearState()

            console.log('%cUnmount Screen: GeodeticView2DScreen', Color.ConsoleInfo)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [hash])

    const getOLView = useMemo(() => {
        if (vm.item instanceof Map2DModel) {
            return (
                <>
                    <ErrorBoundaryComponent>
                        <Suspense fallback={<CommonLoadingSpinFC/>}>
                            <OLView
                                m2dId={m2dId}
                                item={vm.item}
                                parts={params ? params.parts : undefined}
                            />
                        </Suspense>
                    </ErrorBoundaryComponent>
                    {
                        FeatureSingleton.getInstance().isContributor ? <ContributorFC usingFor={'v2d'}/> : null
                    }
                </>
            )
        } else {
            return <CommonEmptyFC/>
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.item])

    return (
        <ErrorItemFC
            status={vm.isLoading}
        >
            {
                vm.isLoading === SendingStatus.loading
                    ? <CommonLoadingSpinFC/>
                    : getOLView
            }
        </ErrorItemFC>
    )
}
