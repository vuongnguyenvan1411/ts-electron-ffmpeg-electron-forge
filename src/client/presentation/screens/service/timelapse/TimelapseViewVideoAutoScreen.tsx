import {useTranslation} from "react-i18next";
import {useLocation} from "react-router";
import {useCallback, useEffect, useState} from "react";
import {UrlQuery} from "../../../../core/UrlQuery";
import {Color} from "../../../../const/Color";
import {Button, Col, Row, Select} from "antd";
import {ErrorItemFC} from "../../../widgets/ErrorItemFC";
import {SendingStatus} from "../../../../const/Events";
import {TimelapseVideoFilterVO} from "../../../../models/service/timelapse/TimelapseVideoModel";
import {TimelapseViewVideoAutoFC} from "../../../widgets/TimelapseViewVideoAutoFC";
import {CommonEmptyFC, CommonLoadingSpinFC} from "../../../widgets/CommonFC";
import {Utils} from "../../../../core/Utils";
import {TParamMachine} from "../../../../const/Types";
import {TimelapseViewVideoAutoAction} from "../../../../recoil/service/timelapse/timelapse_view_video_auto/TimelapseViewVideoAutoAction";
import {CustomTypography} from "../../../components/CustomTypography";
import {useMaster} from "../../../hooks/useMaster";

export const TimelapseViewVideoAutoScreen = (props: {
    machineId: any,
    title: string,
    tab: string,
    machines: TParamMachine[]
}) => {
    const {t} = useTranslation()
    const location = useLocation()
    const URL = new UrlQuery(location.search)
    const {master} = useMaster()

    const {
        vm,
        onLoadItems,
        resetStateWithEffect,
    } = TimelapseViewVideoAutoAction()

    const filter = URL.get('filter', {})
    const sort = URL.get('sort')
    const order = URL.get('order')
    const page = URL.getInt('page', vm.query.page)
    const limit = URL.getInt('limit', vm.query.limit)
    const [selected, setSelected] = useState(0)

    const [queryParams, setQueryParams] = useState<TimelapseVideoFilterVO>({
        filter: filter,
        sort: sort,
        order: order,
        page: page,
        limit: limit
    })

    useEffect(() => {
        console.log('%cMount Screen: TimelapseViewVideoAutoScreen', Color.ConsoleInfo);

        if (
            (vm.items.length === 0
                || vm.key !== props.machineId.toString()
                || (vm.timestamp !== undefined && Utils.checkHourState(vm.timestamp))) && props.tab === "video_auto"
        ) {
            const urlQueryParams = new UrlQuery(queryParams)

            if (vm.items.length > 0) resetStateWithEffect()

            onLoadItems(props.machineId, urlQueryParams.toObject())
        }

        return () => {
            console.log('%cUnmount Screen: TimelapseViewVideoAutoScreen', Color.ConsoleInfo);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.machineId, props.tab])

    const getItems = useCallback(() => {
        return vm.items.map((value, index) => {
            let timeStart = value.info?.timeStartFormatted("HH:mm");
            let timeEnd = value.info?.timeEndFormatted("HH:mm");

            return (
                <Select.Option key={index.toString()} value={index}>
                    <CustomTypography
                        textStyle={"text-body-text-3"}
                    >
                        <CustomTypography isStrong>{t("text.time")}</CustomTypography>:&nbsp;
                        {value.info?.dateStartFormatted('DD/MM/YYYY')}&nbsp;-&nbsp;{value.info?.dateEndFormatted('DD/MM/YYYY')}
                        &nbsp;|&nbsp;
                        {
                            timeStart === timeEnd
                                ? t('text.timeFull').toLocaleLowerCase()
                                : `${timeStart}-${timeEnd}`
                        }
                    </CustomTypography>
                </Select.Option>
            )
        })

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.items])

    const handleChange = (value: number) => {
        setSelected(value)
    }

    const onScroll = (event: any) => {
        const target = event.target

        if (vm.isNextAvailable && target.scrollTop + target.offsetHeight === target.scrollHeight) {
            onChangePage()
        }
    }

    const onChangePage = () => {
        const urlQueryParams = new UrlQuery(queryParams)
        urlQueryParams.set('page', (vm.items.length / vm.query.limit) + 1)

        setQueryParams(urlQueryParams.toObject())
        onLoadItems(props.machineId, urlQueryParams.toObject())
    }

    useEffect(() => {
        if (master.header) {
            master.header.setHeader({
                ...master.header,
                isLoading: vm.isLoading,
                onReload: onClickReload
            })
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.isLoading])

    const onClickReload = () => {
        const urlQueryParams = new UrlQuery(queryParams)
        onLoadItems(props.machineId, urlQueryParams.toObject())
    }

    return (
        <div className="page_timelapse_info_image relative">
            <ErrorItemFC
                status={vm.isLoading}
            >
                {
                    vm.isLoading === SendingStatus.loading && !vm.isNextAvailable
                        ? <CommonLoadingSpinFC/>
                        : vm.items.length > 0
                            ? <Row>
                                <Col
                                    xs={{span: 24, offset: 0}}
                                    sm={{span: 24, offset: 0}}
                                    md={{span: 22, offset: 1}}
                                    lg={{span: 20, offset: 2}}
                                    xl={{span: 18, offset: 3}}
                                    xxl={{span: 18, offset: 3}}
                                >
                                    <div className={"sm:container mx-auto flex flex-col gap-5"}>
                                        <div className={"flex justify-end"}>
                                            <Select
                                                style={{
                                                    minWidth: "300px",
                                                }}
                                                className={"select-main"}
                                                bordered={false}
                                                value={selected}
                                                defaultValue={selected}
                                                onChange={handleChange}
                                                onPopupScroll={onScroll}
                                            >
                                                {vm.isLoading === SendingStatus.loading ?
                                                    [...getItems(),
                                                        <Select.Option
                                                            key="loading"
                                                            disabled={true}
                                                            value={"loading"}
                                                        >
                                                            Loading...
                                                        </Select.Option>
                                                    ]
                                                    : getItems()}
                                            </Select>
                                        </div>
                                        <div>
                                            <TimelapseViewVideoAutoFC
                                                item={vm.items[selected]}
                                                tab={props.tab}
                                            />
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                            : <CommonEmptyFC/>
                }
            </ErrorItemFC>
            {
                vm.isNextAvailable && (
                    <div className={"flex w-full justify-center p-2"}>
                        <Button
                            type="primary"
                            loading={vm.isLoading === SendingStatus.loading}
                            onClick={onChangePage}
                        >
                            {t("button.loadMore")}
                        </Button>
                    </div>
                )
            }
        </div>
    )
}
