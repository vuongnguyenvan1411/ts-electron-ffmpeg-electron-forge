import {useTranslation} from "react-i18next";
import {useLocation, useNavigate} from "react-router";
import {UrlQuery} from "../../../../core/UrlQuery";
import {useCallback, useEffect, useRef, useState} from "react";
import {Color} from "../../../../const/Color";
import {E_ResUrlType, SendingStatus} from "../../../../const/Events";
import {Anchor, Button, Dropdown, Menu, Modal, notification, Progress, Space, Tooltip, Typography} from "antd";
import {ErrorItemFC} from "../../../widgets/ErrorItemFC";
import {LightGallery} from "lightgallery/lightgallery";
import {TimelapseViewImageFC, TimelapseViewImageSkeleton,} from "../../../widgets/TimelapseViewImageFC";
import {groupBy} from "lodash";
import {DownloadOutlined, HistoryOutlined, PlusCircleOutlined} from "@ant-design/icons";
import {TimelapseImageFilterVO} from "../../../../models/service/timelapse/TimelapseImageModel";
import lgThumbnail from 'lightgallery/plugins/thumbnail';
import lgZoom from 'lightgallery/plugins/zoom';
import lgFullScreen from 'lightgallery/plugins/fullscreen';
import lgAutoplay from 'lightgallery/plugins/autoplay';
import styles from "../../../../styles/module/TimelapseViewImage.module.scss";
import {AfterSlideDetail, InitDetail} from "lightgallery/lg-events";
import axios from "axios";
import download from "downloadjs";
import {MediaQuery} from "../../../../core/MediaQuery";
import NoImage from "../../../../assets/image/no_image.png";
import {Style} from "../../../../const/Style";
import {CommonEmptyFC, CommonTagFilterFC} from "../../../widgets/CommonFC";
import {Utils} from "../../../../core/Utils";
import {TimelapseImageCompareToolScreen} from "./TimelapseImageCompareToolScreen";
import {TimelapseImageAnalyticsToolScreen} from "./TimelapseImageAnalyticsToolScreen";
import {useParams} from "react-router-dom";
import {TFilterData, TOrder, TParamMachine} from "../../../../const/Types";
import {TimelapseViewImageAction, TimelapseViewImageLightGalleryAction} from "../../../../recoil/service/timelapse/timelapse_view_image/TimelapseViewImageAction";
import {useRecoilBridgeAcrossReactRoots_UNSTABLE} from "recoil";
import {TimelapseImageCompareToolAction} from "../../../../recoil/service/timelapse/timelapse_image_compare_tool/TimelapseImageCompareToolAction";
import {TimelapseImageAnalyticsToolAction} from "../../../../recoil/service/timelapse/timelapse_image_analytics_tool/TimelapseImageAnalyticsToolAction";
import dynamic from "next/dynamic";
import ReactDOM from "react-dom";
import {CustomTypography} from "../../../components/CustomTypography";
import {CustomButton} from "../../../components/CustomButton";
import NextImage from "next/image";
import {Images} from "../../../../const/Images";
import {TimelapseDownloadImageFC} from "../../../widgets/TimelapseDownloadImageFC";
import {useMaster} from "../../../hooks/useMaster";
import FilterDrawerWidget from "../../../widgets/FilterDrawerWidget";

const LG = dynamic(() => import('lightgallery/react'), {
    ssr: false
})

export const TimelapseViewImageScreen = (props: {
    machineId: number,
    title: string,
    hash: string,
    machines: TParamMachine[],
}) => {
    const {t} = useTranslation()
    const navigate = useNavigate()
    const location = useLocation()
    const RecoilBridge = useRecoilBridgeAcrossReactRoots_UNSTABLE()
    const lightGallery = useRef<LightGallery>()
    const URL = new UrlQuery(location.search)
    const [visible, setVisible] = useState(false)
    const [visibleAnalytics, setVisibleAnalytics] = useState(false)
    const {tab} = useParams<{ tab?: string }>()
    const {master} = useMaster();

    const {
        onClearState: onClearCompareTool
    } = TimelapseImageCompareToolAction()

    const {
        onClearState: onClearAnalyticsTool
    } = TimelapseImageAnalyticsToolAction()

    const {
        vm,
        onLoadItems,
        resetStateWithEffect,
    } = TimelapseViewImageAction()

    const {
        onClearState: onClearLG,
        onChangeCurrentIndex
    } = TimelapseViewImageLightGalleryAction()

    const currentState = useRef(vm);
    const filter = URL.get('filter', {});
    const sort = URL.get('sort');
    const order = URL.get('order');
    const page = URL.getInt('page', vm.query.page);
    const limit = URL.getInt('limit', vm.query.limit);
    const [queryParams, setQueryParams] = useState<TimelapseImageFilterVO>({
        filter: filter,
        sort: sort,
        order: order,
        page: page,
        limit: limit,
    });
    const [visibleDrawer, setVisibleDrawer] = useState(false);

    const showDrawer = () => {
        setVisibleDrawer(true);
    };

    const onClose = () => {
        setVisibleDrawer(false);
    };

    useEffect(() => {
        console.log('%cMount Screen: TimelapseViewImageScreen', Color.ConsoleInfo);

        document.title = `ATL-${props.title}`

        currentState.current = vm;

        if (
            (
                vm.items.length === 0
                || vm.key !== props.machineId.toString()
                || (vm.timestamp !== undefined && Utils.checkHourState(vm.timestamp))
            )
            && tab === "image"
        ) {
            const urlQueryParams = new UrlQuery(queryParams);
            const mediaQuery = new MediaQuery(Style.GridLimit);
            const mediaLimit = mediaQuery.getPoint(limit);
            urlQueryParams.set('limit', mediaLimit);
            urlQueryParams.set('page', 1);

            if (vm.items.length > 0) {
                resetStateWithEffect(undefined, val => {
                    onLoadItems(props.machineId, urlQueryParams.toObject(), val);
                })
            } else {
                onLoadItems(props.machineId, urlQueryParams.toObject());
            }
        }

        return () => {
            onClearLG()
            console.log('%cUnmount Screen: TimelapseViewImageScreen', Color.ConsoleInfo);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.machineId])

    useEffect(() => {
        const urlQueryParams = new UrlQuery(queryParams);

        urlQueryParams.set('limit', vm.query.limit);

        setQueryParams(urlQueryParams.toObject());

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.query])

    const isTags = (typeof queryParams.filter?.date_start !== "undefined" && queryParams.filter?.date_start?.length > 0)
        || (typeof queryParams.filter?.date_end !== "undefined" && queryParams.filter?.date_end?.length > 0)
        || (typeof queryParams.filter?.time_start !== "undefined" && queryParams.filter?.time_start?.length > 0)
        || (typeof queryParams.filter?.time_end !== "undefined" && queryParams.filter?.time_end?.length > 0)
        || (typeof queryParams.sort !== "undefined" && queryParams.sort.length > 0)
        || (typeof queryParams.order !== "undefined" && queryParams.order.length > 0);

    const handleCloseTag = (name: string) => {
        const urlQueryParams = new UrlQuery(queryParams);

        urlQueryParams.delete(name);

        setQueryParams(urlQueryParams.toObject());

        navigate({
            search: urlQueryParams.toString()
        }, {
            replace: true,
            state: {
                name: props.title,
                machines: props.machines,
            }
        })

        onLoadItems(props.machineId, urlQueryParams.toObject());
    }

    const onLoadMore = () => {
        const urlQueryParams = new UrlQuery(queryParams);
        urlQueryParams.set('page', (vm.items.length / vm.query.limit) + 1);

        setQueryParams(urlQueryParams.toObject());
        onLoadItems(props.machineId, urlQueryParams.toObject());
    }

    const onInitGallery = useCallback((detail: InitDetail) => {
        if (detail) {
            lightGallery.current = detail.instance;

            const divQuality = document.createElement("div");
            divQuality.id = "div-quality";
            divQuality.classList.add(styles.DivQuality);

            const divCustomNextButton = document.createElement("div");
            divCustomNextButton.id = "div-custom-next-button";
            divCustomNextButton.classList.add(styles.BtnCustom);

            const divCustomDownloadButton = document.createElement("div");
            divCustomDownloadButton.id = "div-custom-download-button";
            divCustomDownloadButton.classList.add("lg-icon");
            divCustomDownloadButton.classList.add(styles.DivFixLgToolbar);

            const lightGalleryElement = document.getElementsByClassName('lg-custom-thumbnails').item(0);

            if (lightGalleryElement) {
                const lgToolbar = lightGalleryElement.getElementsByClassName('lg-toolbar').item(0);

                if (lgToolbar) {
                    lgToolbar.insertAdjacentElement('afterend', divQuality);
                    lgToolbar.insertAdjacentElement('afterend', divCustomNextButton);
                    lgToolbar.append(divCustomDownloadButton);
                }
            }

            ReactDOM.render(
                <RecoilBridge>
                    <DivQuality/>
                </RecoilBridge>,
                divQuality
            )

            ReactDOM.render(
                <RecoilBridge>
                    <DivCustomDownloadButton/>
                </RecoilBridge>,
                divCustomDownloadButton
            )
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        const btnNextCustom = document.getElementById('light-gallery-custom-next');

        if (btnNextCustom) {
            if (vm.isLoading === SendingStatus.loading) {
                btnNextCustom.classList.add(styles.ButtonLoading);
                btnNextCustom.classList.add(styles.BtnNoEvent);
            }
        }
    }, [vm.isLoading])

    useEffect(() => {
        if (lightGallery.current) {
            const divCustomNextButton = document.getElementById('div-custom-next-button');
            if (divCustomNextButton) {
                ReactDOM.render(
                    <RecoilBridge>
                        <DivCustomNextButton
                            machineId={props.machineId}
                            onLoadMore={onLoadMore}
                        />
                    </RecoilBridge>,
                    divCustomNextButton
                );
            }

            lightGallery.current.refresh();
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.items, lightGallery.current])

    const getLoading = useCallback(() => {
        return (
            <div
                className={`${styles.CardHistoryImage} pt-6`}
            >
                <div className={styles.History}>
                    <HistoryOutlined className={"mr-2"}/>
                    <CustomTypography
                        textStyle={"text-body-text-2"}
                        isStrong
                    >
                        {/*{key}*/}
                    </CustomTypography>
                </div>
                <div className={styles.GridImage}>
                    {
                        Array(
                            (new MediaQuery({
                                xs: 1,
                                sm: 2,
                                md: 2,
                                lg: 3,
                                xl: 3,
                                xxl: 4,
                                xxxl: 4,
                            })).getPoint(4)
                        )
                            .fill(0)
                            .map((_, i) => <TimelapseViewImageSkeleton key={i}/>)
                    }
                </div>
            </div>
        )
    }, []);

    useEffect(() => {
        if (master.header) {
            master.header.setHeader({
                ...master.header,
                isLoading: vm.isLoading,
                onReload: onClickReload,
            });
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.isLoading])

    const getItems = useCallback(() => {
        return Object.entries(groupBy(vm.items, 'dateGroup')).map(([key, value], index) => {
            return (
                <div
                    key={index}
                    id={`ac-${key}`}
                    className={styles.CardHistoryImage}
                >
                    <div className={styles.History}>
                        <HistoryOutlined className={"mr-2"}/>
                        <CustomTypography
                            textStyle={"text-body-text-2"}
                            isStrong
                        >
                            {key}
                        </CustomTypography>
                    </div>
                    <div className={styles.GridImage}>
                        {
                            value.map((item, i) => {
                                const subHtml =
                                    <div
                                        id={`caption-${i}`}
                                        className={styles.BoxContent}
                                    >
                                        <div className={"custom-caption flex w-full justify-between"}>
                                            <CustomTypography
                                                className={"caption-info-left"}
                                                isStrong
                                                textStyle={"text-body-text-3"}
                                                style={{
                                                    color: "#1a8983",
                                                }}
                                            >
                                                #{item.order}
                                            </CustomTypography>
                                            <CustomTypography
                                                className={"caption-info-right"}
                                                textStyle={"text-body-text-3"}
                                                isStrong
                                            >
                                                {
                                                    item.dateShotFormatted(t('format.timeShort'))
                                                }
                                                &nbsp;
                                                <span
                                                    style={{
                                                        color: "transparent",
                                                        textShadow: "0 0 0 rgba(0, 155, 144, 1)"
                                                    }}
                                                >
                                                    &#10072;
                                                </span>
                                                &nbsp;
                                                {
                                                    item.dateShotFormatted(t('format.date'))
                                                }
                                            </CustomTypography>
                                        </div>
                                        {
                                            item.sensor !== undefined && (
                                                <div className={styles.BoxInfoSensor}>
                                                    {
                                                        item.sensor.map((value, index) => (
                                                            <div key={index}
                                                                 className={"custom-caption flex w-full justify-between"}
                                                            >
                                                                <CustomTypography
                                                                    textStyle={"text-body-text-3"}
                                                                >
                                                                    {value.name}
                                                                </CustomTypography>
                                                                <CustomTypography
                                                                    textStyle={"text-body-text-3"}
                                                                >
                                                                    {value.value && value.value}
                                                                    {value.unit2 && value.unit2}
                                                                </CustomTypography>
                                                            </div>
                                                        ))
                                                    }
                                                </div>
                                            )
                                        }
                                    </div>

                                return (
                                    <div key={i} className={"relative"}>
                                        <div
                                            key={i}
                                            className="cursor-pointer gallery-item"
                                            data-src={item.getShotImageUrl({
                                                type: E_ResUrlType.Thumb,
                                                size: 1280
                                            })}
                                            data-thumb={item.getShotImageUrl({
                                                type: E_ResUrlType.Thumb,
                                                size: 426
                                            })}
                                            data-sub-html={`#caption-${i}`}
                                        >
                                            <TimelapseViewImageFC
                                                item={item}
                                                index={i}
                                                isDivider={true}
                                            >
                                                {subHtml}
                                            </TimelapseViewImageFC>
                                        </div>
                                        <div className={"absolute top-2 left-2"}>
                                            <TimelapseDownloadImageFC
                                                wrapClass={"btn-main-auto-icon"}
                                                buttonStyle={{
                                                    background: "#172723",
                                                    opacity: 0.5,
                                                }}
                                                viewBoxIcon={25}
                                                item={item}
                                                isLabel={false}
                                            />
                                        </div>
                                    </div>
                                );
                            })
                        }
                    </div>
                </div>
            )
        });

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.items])

    const onFilter = (data: TFilterData) => {
        const urlQueryParams = new UrlQuery(queryParams);
        urlQueryParams.set('order', data.order as string);
        urlQueryParams.set('filter.date_start', data.dateStart);
        urlQueryParams.set('filter.date_end', data.dateEnd);
        urlQueryParams.set('filter.time_start', data.timeStart);
        urlQueryParams.set('filter.time_end', data.timeEnd);
        urlQueryParams.set('page', 1);

        setQueryParams(urlQueryParams.toObject());

        onLoadItems(props.machineId, urlQueryParams.toObject());

        navigate({
            search: urlQueryParams.toString()
        }, {
            replace: true,
            state: {
                name: props.title,
                machines: props.machines,
            }
        })
    }

    const onClickReload = () => {
        const urlQueryParams = new UrlQuery(queryParams);
        urlQueryParams.set('page', 1);
        resetStateWithEffect(undefined, val => {
            onLoadItems(props.machineId, urlQueryParams.toObject(), val);
        })
    }

    useEffect(() => {
        const divButtonCustom = document.getElementById('light-gallery-custom-next');

        if (divButtonCustom) {
            divButtonCustom.onclick = () => onLoadMore();
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.items])

    const onAfterSlide = (detail: AfterSlideDetail) => {
        onChangeCurrentIndex(detail.index)
    }

    return (
        <div className={"flex"}>
            <div className={"grow"}>
                <div className="TimelapseViewImageScreen relative">
                    <div className={"w-full flex items-center justify-between"}>
                        {
                            vm.isLoading === SendingStatus.loading
                                ? <div className={"animate-pulse"}>
                                    <div className={"bg-gray-300 h-5 w-20"}/>
                                </div>
                                : <div>
                                    <CustomTypography
                                        textStyle={"text-button"}
                                        isStrong
                                    >
                                        {t("text.total")}
                                    </CustomTypography>
                                    <CustomTypography
                                        textStyle={"text-button"}
                                        style={{
                                            color: "#1a8983",
                                        }}
                                        isStrong
                                    >
                                        &nbsp;{vm.items.length > 0 ? vm.oMeta?.totalCount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : 0}&nbsp;
                                    </CustomTypography>
                                    <CustomTypography
                                        textStyle={"text-button"}
                                        isStrong
                                    >
                                        {t("text.image").toLocaleLowerCase()}
                                    </CustomTypography>
                                </div>
                        }
                        {
                            vm.isLoading === SendingStatus.loading
                                ? <div className={"animate-pulse"}>
                                    <div className={"bg-gray-300 h-8 w-20"}/>
                                </div>
                                : <Space>
                                    <CustomButton
                                        responsive={true}
                                        onClick={() => setVisible(true)}
                                        icon={
                                            <NextImage
                                                src={Images.iconCompare.data}
                                                alt={Images.iconCompare.atl}
                                            />
                                        }
                                    >
                                        {t("button.compare")}
                                    </CustomButton>
                                    <CustomButton
                                        responsive={true}
                                        onClick={() => setVisibleAnalytics(true)}
                                        icon={<NextImage
                                            src={Images.iconAnalytics.data}
                                            alt={Images.iconAnalytics.atl}
                                        />
                                        }
                                    >
                                        {t("button.analytic")}
                                    </CustomButton>
                                    <CustomButton
                                        onClick={showDrawer}
                                        responsive={true}
                                        icon={
                                            <NextImage
                                                src={Images.iconFilter.data}
                                                alt={Images.iconFilter.atl}
                                            />
                                        }
                                    >
                                        {t("button.filter")}
                                    </CustomButton>
                                    {/*<Dropdown*/}
                                    {/*    overlay={*/}
                                    {/*        <FilterWidget*/}
                                    {/*            onClick={onFilter}*/}
                                    {/*            current={{*/}
                                    {/*                order: queryParams.order as TOrder,*/}
                                    {/*                dateStart: queryParams.filter?.date_start,*/}
                                    {/*                dateEnd: queryParams.filter?.date_end,*/}
                                    {/*                timeStart: queryParams.filter?.time_start,*/}
                                    {/*                timeEnd: queryParams.filter?.time_end,*/}
                                    {/*            }}*/}
                                    {/*            display={{*/}
                                    {/*                active: false,*/}
                                    {/*                order: true,*/}
                                    {/*                timeRange: true,*/}
                                    {/*                rangeDate: true,*/}
                                    {/*                fastFilter: false,*/}
                                    {/*            }}*/}
                                    {/*        />*/}
                                    {/*    }*/}
                                    {/*    trigger={['click']}*/}
                                    {/*    placement="bottomRight"*/}
                                    {/*    arrow*/}
                                    {/*>*/}
                                    {/*    <CustomButton*/}
                                    {/*        responsive={true}*/}
                                    {/*        icon={*/}
                                    {/*            <NextImage*/}
                                    {/*                src={Images.iconFilter.data}*/}
                                    {/*                alt={Images.iconFilter.atl}*/}
                                    {/*            />*/}
                                    {/*        }*/}
                                    {/*    >*/}
                                    {/*        {t("button.filter")}*/}
                                    {/*    </CustomButton>*/}
                                    {/*</Dropdown>*/}
                                </Space>
                        }
                    </div>
                    <CommonTagFilterFC
                        className={"pt-6"}
                        is={isTags}
                        onClose={handleCloseTag}
                        data={queryParams}
                        items={[
                            {
                                key: 'filter.date_start',
                                label: t('text.dateStart')
                            },
                            {
                                key: 'filter.date_end',
                                label: t('text.dateEnd')
                            },
                            {
                                key: 'filter.time_start',
                                label: t('text.timeStart')
                            },
                            {
                                key: 'filter.time_end',
                                label: t('text.timeEnd')
                            },
                            {
                                key: 'sort',
                                label: t('text.sort')
                            },
                            {
                                key: 'order',
                                label: t('text.order'),
                                check: ['asc', t('text.orderAsc'), t('text.orderDesc')]
                            }
                        ]}
                    />
                    <ErrorItemFC
                        status={vm.isLoading}
                    >
                        {
                            vm.isLoading === SendingStatus.loading && !vm.isNextAvailable
                                ? getLoading() : vm.items.length > 0 ? <div className={"flex"}>
                                    <LG
                                        elementClassNames={"grow"}
                                        galleryId={"1"}
                                        onAfterSlide={onAfterSlide}
                                        preload={2}
                                        selector={'.gallery-item'}
                                        appendSubHtmlTo={".lg-item"}
                                        speed={500}
                                        plugins={[lgZoom, lgThumbnail, lgFullScreen, lgAutoplay]}
                                        download={false}
                                        onInit={onInitGallery}
                                        mode="lg-slide"
                                        addClass={"lg-custom-thumbnails"}
                                        allowMediaOverlap={true}
                                        toggleThumb={true}
                                        appendThumbnailsTo={".lg-components"}
                                        loop={false}
                                        slideEndAnimation={false}
                                        pager={true}
                                        thumbnail={true}
                                        animateThumb={true}
                                        exThumbImage={"data-thumb"}
                                        alignThumbnails={"middle"}
                                        fullScreen={true}
                                        autoplay={true}
                                        slideShowInterval={2500}
                                        scale={10}
                                    >
                                        {
                                            getItems()
                                        }
                                    </LG>
                                </div> : <CommonEmptyFC/>

                        }
                    </ErrorItemFC>
                    {
                        vm.isNextAvailable && vm.items.length > 0 ?
                            <div className={"flex w-full justify-center p-6"}>
                                <CustomButton
                                    loading={vm.isLoading === SendingStatus.loading}
                                    onClick={onLoadMore}
                                    icon={<PlusCircleOutlined/>}
                                >
                                    {t("button.loadMore")}
                                </CustomButton>
                            </div>
                            : null
                    }
                    <ModalTool
                        title={t("title.compareTool")}
                        visible={visible}
                        onCancel={() => setVisible(false)}
                        onAfterClose={() => onClearCompareTool()}
                        child={<TimelapseImageCompareToolScreen/>}
                    />
                    <ModalTool
                        title={t("title.analyticsTool")}
                        visible={visibleAnalytics}
                        onCancel={() => setVisibleAnalytics(false)}
                        onAfterClose={() => onClearAnalyticsTool()}
                        child={<TimelapseImageAnalyticsToolScreen/>}
                    />
                </div>
            </div>
            <div className={"grow-0 pt-2 w-40 hidden md:block"}>
                {
                    vm.isLoading === SendingStatus.loading
                        ? <div
                            style={{
                                paddingLeft: "2.625rem",
                                paddingRight: "0.5rem",
                            }}
                            className={"flex flex-row gap-2 animate-pulse"}
                        >
                            <div className={"w-1 h-28 bg-gray-300"}/>
                            <div className={"flex flex-col gap-2"}>
                                <div className={"w-24 h-4 bg-gray-300"}/>
                                <div className={"w-24 h-4 bg-gray-300"}/>
                                <div className={"w-24 h-4 bg-gray-300"}/>
                                <div className={"w-24 h-4 bg-gray-300"}/>
                                <div className={"w-24 h-4 bg-gray-300"}/>
                            </div>
                        </div>
                        :
                        vm.items.length > 0 && (
                            <div
                                style={{
                                    paddingLeft: "2.625rem",
                                    paddingRight: "0.5rem",
                                }}
                            >
                                <Anchor
                                    className={"anchor-main"}
                                    offsetTop={64}
                                    onClick={(e) => e.preventDefault()}
                                >
                                    {
                                        Object.entries(groupBy(vm.items, 'dateGroup')).map(([key]) => {
                                            return (
                                                <Anchor.Link
                                                    key={key.toString()}
                                                    href={`#ac-${key}`}
                                                    title={key}
                                                />
                                            );
                                        })
                                    }
                                </Anchor>
                            </div>
                        )
                }
            </div>
            <FilterDrawerWidget
                visible={visibleDrawer}
                onClose={onClose}
                onClick={onFilter}
                current={{
                    order: queryParams.order as TOrder,
                    dateStart: queryParams.filter?.date_start,
                    dateEnd: queryParams.filter?.date_end,
                    timeStart: queryParams.filter?.time_start,
                    timeEnd: queryParams.filter?.time_end
                }}
                display={{
                    active: false,
                    order: true,
                    timeRange: true,
                    rangeDate: true,
                    fastFilter: true
                }}
            />
        </div>
    )
}

export const ModalTool = (props: {
    title: string,
    visible: boolean,
    onCancel: () => void,
    onAfterClose: () => void,
    child: JSX.Element
}) => {
    return (
        <Modal
            wrapClassName={`modal-main modal-body-p-0 modal-body-bg modal-header-bg-white modal-full-height modal-header-shadow ${new MediaQuery().isMinBreakpoint("xl") ? "modal-header-center" : ""}`}
            destroyOnClose={true}
            title={props.title}
            centered
            visible={props.visible}
            onCancel={props.onCancel}
            width={"100%"}
            afterClose={props.onAfterClose}
            footer={null}
        >
            {props.child}
        </Modal>
    )
}

const DivQuality = () => {
    const {t} = useTranslation();
    const {vm} = TimelapseViewImageAction();
    const {vm: vmLG} = TimelapseViewImageLightGalleryAction()
    const [isHD, setIsHD] = useState(true);
    const [isLoadingOrig, setIsLoadingOrig] = useState(false);
    const lightGallery = document.getElementsByClassName('lg-custom-thumbnails').item(0);

    const onClickHD = useCallback(() => {
        if (lightGallery && vmLG.index) {
            const imgQuery = lightGallery.querySelector("img[data-index='" + vmLG.index + "']")

            if (imgQuery) {
                imgQuery.removeAttribute("src");
                imgQuery.setAttribute("src", vm.items[vmLG.index].getShotImageUrl({
                    type: E_ResUrlType.Thumb,
                    size: 1280
                }) ?? '');
            }
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vmLG.index, vm.items])

    const onClickOrig = useCallback(() => {
        if (isHD) {
            if (lightGallery && vmLG.index !== undefined) {
                const imgQuery = lightGallery.querySelector("img[data-index='" + vmLG.index + "']");

                if (imgQuery) {
                    setIsLoadingOrig(true);
                    imgQuery.removeAttribute("src");

                    const image = new Image();
                    image.src = vm.items[vmLG.index].getShotImageUrl({
                        type: E_ResUrlType.Orig
                    }) ?? '';
                    image.onload = () => {
                        imgQuery.setAttribute("src", image.src);
                        setIsLoadingOrig(false);
                    }
                    image.onerror = () => {
                        imgQuery.setAttribute("src", NoImage.src);
                        setIsLoadingOrig(false);
                    }
                }
            }
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.items, vmLG.index, isHD])

    useEffect(() => {
        if (!isHD) {
            setIsHD(true);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vmLG.index])

    return (
        <div
            style={{
                textAlign: "center"
            }}
        >
            <Space>
                <Button
                    onClick={() => {
                        onClickHD();
                        setIsHD(true);
                    }}
                    className={isHD ? styles.BtnActive : styles.BtnInActive}
                >
                    <Typography.Text
                        className={styles.AntTypography}
                        strong
                    >
                        {t("text.imageHd")}
                    </Typography.Text>
                </Button>
                <Button
                    loading={isLoadingOrig}
                    onClick={() => {
                        onClickOrig();
                        setIsHD(false);
                    }}
                    className={!isHD ? styles.BtnActive : styles.BtnInActive}
                >
                    <Typography.Text
                        className={styles.AntTypography}
                        strong
                    >
                        {t("text.imageOrig")}
                    </Typography.Text>
                </Button>
            </Space>
        </div>
    )
}

const DivCustomNextButton = (props: {
    machineId: number,
    onLoadMore: Function
}) => {
    const {t} = useTranslation()
    const {vm} = TimelapseViewImageAction()
    const {vm: vmLG} = TimelapseViewImageLightGalleryAction()

    return (
        <Tooltip
            placement="bottom"
            title={t("button.loadMore")}
        >
            <CustomButton
                id={'light-gallery-custom-next'}
                type="normal"
                className={`${styles.BtnCustomVisible} ${vmLG.index !== (vm.items.length - 1) ? styles.BtnCustomHidden : ''}`}
                loading={vm.isLoading === SendingStatus.loading}
                icon={<PlusCircleOutlined/>}
                style={{
                    backgroundColor: "#009579",
                    borderColor: "#009579",
                    height: "40px",
                }}
                onClick={() => {
                    props.onLoadMore()
                }}
            >
                {t("button.loadMore")}
            </CustomButton>
        </Tooltip>
    )
}

const DivCustomDownloadButton = () => {
    const {t} = useTranslation()
    const {vm} = TimelapseViewImageAction()
    const {vm: vmLG} = TimelapseViewImageLightGalleryAction()
    const item = vmLG.index ? vm.items[vmLG.index] : undefined;
    const [percentComplete, setPercentComplete] = useState<number | undefined>(undefined);

    const menuDownload = useCallback(() => {
        return (
            <Menu>
                <Menu.Item
                    key="1"
                    onClick={_ => downloadImage(item?.getShotImageUrl({
                        type: E_ResUrlType.Download
                    }))}
                >
                    {t("text.dlOrig")}
                </Menu.Item>
                <Menu.Item
                    key="2"
                    onClick={_ => downloadImage(item?.getShotImageUrl({
                        type: E_ResUrlType.Download,
                        attach: true
                    }))}
                >
                    {t("text.dlAttach")}
                </Menu.Item>
            </Menu>
        );

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vmLG.index]);

    const downloadImage = (url?: string) => {
        if (url) {
            axios
                .get(url, {
                    responseType: "blob",
                    onDownloadProgress: (evt: ProgressEvent) => {
                        if (evt.lengthComputable) {
                            setPercentComplete((evt.loaded / evt.total) * 100);
                        }
                    }
                })
                .then(r => {
                    const data = r.data as Blob;
                    const filename = `${item?.order}_${item?.dateShotFormatted("DD-MM-YYYY-HH-mm-ss")}.jpg`;

                    download(data, filename, data.type);

                    setPercentComplete(undefined);

                    notification.success({
                        message: t('message.downloadImageSuccess'),
                    });
                })
                .catch(_ => {
                    setPercentComplete(undefined);

                    notification.error({
                        message: t('message.downloadImageFailure'),
                    });
                });
        }
    }

    return (
        vmLG.index
            ? percentComplete !== undefined
                ? <div
                    style={{
                        paddingTop: "4px"
                    }}
                >
                    <Progress
                        type="circle"
                        strokeColor={{
                            '0%': '#108ee9',
                            '100%': '#87d068'
                        }}
                        width={20}
                        percent={percentComplete}
                        strokeWidth={15}
                        showInfo={false}
                    />
                </div>
                : item?.sensor && item.sensor.length > 0
                    ? <Dropdown
                        trigger={['click']}
                        overlay={menuDownload}
                        placement={"bottomRight"}
                    >
                        <Button
                            className={`${styles.ButtonFixIconSize} lg-icon`}
                            type={"text"}
                            style={{
                                marginLeft: 8
                            }}
                            icon={
                                <DownloadOutlined/>
                            }
                        >
                        </Button>
                    </Dropdown>
                    : <span
                        className={"lg-icon"}
                        onClick={_ => downloadImage(item?.getShotImageUrl({
                            type: E_ResUrlType.Download
                        }))}
                    >
                        <DownloadOutlined/>
                    </span>
            : null
    )
}
