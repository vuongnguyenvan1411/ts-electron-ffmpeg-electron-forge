import {Form, Input} from "antd";
import {CustomButton} from "../../../components/CustomButton";
import {useTranslation} from "react-i18next";
import {useLocation, useNavigate} from "react-router";
import {UrlQuery} from "../../../../core/UrlQuery";
import {TimelapseProjectAction} from "../../../../recoil/service/timelapse/timelapse_project/TimelapseProjectAction";
import {useCallback, useEffect, useRef, useState} from "react";
import {TTimelapseProjectFilterVO} from "../../../../models/service/timelapse/TimelapseProjectModel";
import {Utils} from "../../../../core/Utils";
import {MediaQuery} from "../../../../core/MediaQuery";
import {Style} from "../../../../const/Style";
import {Color} from "../../../../const/Color";
import {debounce} from "lodash";
import {App} from "../../../../const/App";
import {CommonEmptyFC, CommonTagFilterFC} from "../../../widgets/CommonFC";
import {ErrorItemFC} from "../../../widgets/ErrorItemFC";
import {SendingStatus} from "../../../../const/Events";
import {TimelapseProjectItemFC, TimelapseProjectSkeleton} from "../../../widgets/TimelapseProjectItemFC";
import Image from "next/image";
import {Images} from "../../../../const/Images";
import {CustomTypography} from "../../../components/CustomTypography";
import {WrapContentWidget} from "../../../widgets/WrapContentWidget";
import {GridLoadingWidget} from "../../../widgets/GridLoadingWidget";
import {TActive, TFilterData} from "../../../../const/Types";
import CustomPagination from "../../../components/CustomPagination";
import FilterDrawerWidget from "../../../widgets/FilterDrawerWidget";

export const TimelapseProjectScreen = () => {
    const {t} = useTranslation()
    let navigate = useNavigate()
    let location = useLocation()
    const [form] = Form.useForm();
    const URL = new UrlQuery(location.search);
    const [showSearchMb, setShowSearchMb] = useState(false);

    const {
        vm,
        onLoadItems,
        onCacheItems,
    } = TimelapseProjectAction();

    const filter = URL.get('filter', {});
    const sort = URL.get('sort');
    const order = URL.get('order');
    const page = URL.getInt('page', vm.query.page);
    const limit = URL.getInt('limit', vm.query.limit);
    const _title = t('title.timelapse');

    const [queryParams, setQueryParams] = useState<TTimelapseProjectFilterVO>({
        filter: filter,
        sort: sort,
        order: order,
        page: page,
        limit: limit,
    });

    const [visible, setVisible] = useState(false);

    const showDrawer = () => {
        setVisible(true);
    };

    const onClose = () => {
        setVisible(false);
    };

    useEffect(() => {
        console.log('%cMount Screen: TimelapseProjectScreen', "color: DodgerBlue");

        if (
            vm.items.length === 0
            || (vm.timestamp !== undefined && Utils.checkHourState(vm.timestamp))
        ) {
            const urlQueryParams = new UrlQuery(queryParams);
            urlQueryParams.set('limit', (new MediaQuery(Style.GridLimit)).getPoint(limit));

            setQueryParams(urlQueryParams.toObject());
            onLoadItems(urlQueryParams.toObject());
        }

        return () => {
            console.log('%cUnmount Screen: TimelapseProjectScreen', Color.ConsoleInfo);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onChangeSearch = (e: any) => setDebounceSearch.current(e.target.value);

    const setDebounceSearch = useRef(debounce((value: string) => {
        const urlQueryParams = new UrlQuery(queryParams);

        if (value.length > 0) {
            urlQueryParams.set('filter.q', value);
        } else {
            urlQueryParams.delete('filter.q');
        }
        urlQueryParams.set('page', 1);

        onCacheItems(urlQueryParams.toObject());

        navigate({
            search: urlQueryParams.toString()
        }, {
            replace: true
        })
        setQueryParams(urlQueryParams.toObject());
    }, App.DelaySearch));

    const onChangePage = (page: number) => {
        const urlQueryParams = new UrlQuery(queryParams);
        urlQueryParams.set('page', page);

        navigate({
            search: urlQueryParams.toString()
        }, {
            replace: true
        })

        setQueryParams(urlQueryParams.toObject());
        onLoadItems(urlQueryParams.toObject());
    };

    const isTags = (typeof queryParams.filter?.q !== "undefined" && queryParams.filter?.q?.length > 0)
        || (typeof queryParams.filter?.active !== "undefined" && queryParams.filter?.active?.length > 0)
        || (typeof queryParams.filter?.date_start !== "undefined" && queryParams.filter?.date_start?.length > 0)
        || (typeof queryParams.filter?.date_end !== "undefined" && queryParams.filter?.date_end?.length > 0);

    const handleCloseTag = (name: string) => {
        const urlQueryParams = new UrlQuery(queryParams);
        urlQueryParams.delete(name);
        urlQueryParams.set('page', 1);

        navigate({
            search: urlQueryParams.toString()
        }, {
            replace: true
        })

        if (name === "filter.q") {
            form.resetFields();
        }

        onLoadItems(urlQueryParams.toObject());
        setQueryParams(urlQueryParams.toObject());
    }

    const onFilter = (data: TFilterData) => {
        const urlQueryParams = new UrlQuery(queryParams);
        urlQueryParams.set('filter.active', data.active as string);
        urlQueryParams.set('filter.date_start', data.dateStart);
        urlQueryParams.set('filter.date_end', data.dateEnd);
        urlQueryParams.set('page', 1);

        setQueryParams(urlQueryParams.toObject());
        onLoadItems(urlQueryParams.toObject());

        navigate({
            search: urlQueryParams.toString()
        }, {
            replace: true
        })
    }

    const _buildForm = () => {
        return <Form form={form}>
            <Form.Item
                style={{
                    marginBottom: "0px",
                }}
                name={"search"}
            >
                <Input
                    className={"input-under-line-main"}
                    size="small"
                    placeholder={t('text.search')}
                    allowClear
                    onChange={onChangeSearch}
                    prefix={
                        showSearchMb ? null : <span className={"anticon"}>
                              <Image
                                  layout={"fixed"}
                                  height={20}
                                  width={20}
                                  src={Images.iconMagnifyingGlass.data}
                                  alt={Images.iconMagnifyingGlass.atl}
                              />
                        </span>
                    }
                />
            </Form.Item>
        </Form>;
    }

    const onClickReload = useCallback(() => {
        const urlQueryParams = new UrlQuery(queryParams);

        onLoadItems(urlQueryParams.toObject());

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [queryParams])

    return (
        <WrapContentWidget
            masterHeader={
                {
                    title: _title,
                    isLoading: vm.isLoading,
                    onReload: onClickReload,
                }
            }
            bodyHeader={{
                customChild: <div className={"flex justify-between items-center w-full"}>
                    <CustomTypography
                        isStrong
                        textStyle={"text-title-3"}
                    >
                        {_title}
                    </CustomTypography>
                    <div className={"flex flex-wrap items-start gap-3"}>
                        <div className={"hidden w-60 xl:w-[22.5rem] md:block"}>
                            {_buildForm()}
                        </div>
                        <div className={"flex gap-2"}>
                            <div className={`${showSearchMb ? "hidden" : "block"} md:hidden`}>
                                <CustomButton
                                    className={`${showSearchMb ? "hidden" : "block"}`}
                                    onClick={() => setShowSearchMb(true)}
                                    size={"small"}
                                    responsive={true}
                                    icon={
                                        <Image
                                            layout={"fixed"}
                                            height={16}
                                            width={16}
                                            src={Images.iconMagnifyingGlass.data}
                                            alt={Images.iconMagnifyingGlass.atl}
                                        />
                                    }
                                >
                                    {t("text.search")}
                                </CustomButton>
                            </div>
                            <CustomButton
                                size={"small"}
                                onClick={showDrawer}
                                responsive={true}
                                icon={
                                    <Image
                                        layout={"fixed"}
                                        height={16}
                                        width={16}
                                        src={Images.iconFilter.data}
                                        alt={Images.iconFilter.atl}
                                    />
                                }
                            >
                                {t("button.filter")}
                            </CustomButton>
                            {/*<Dropdown*/}
                            {/*    overlay={*/}
                            {/*        <FilterWidget*/}
                            {/*            onClick={onFilter}*/}
                            {/*            current={{*/}
                            {/*                active: queryParams.filter?.active as TActive,*/}
                            {/*                dateStart: queryParams.filter?.date_start,*/}
                            {/*                dateEnd: queryParams.filter?.date_end,*/}
                            {/*            }}*/}
                            {/*            display={{*/}
                            {/*                active: true,*/}
                            {/*                rangeDate: true,*/}
                            {/*                timeRange: false,*/}
                            {/*                fastFilter: false,*/}
                            {/*                order: false,*/}
                            {/*            }}*/}
                            {/*        />*/}
                            {/*    }*/}
                            {/*    trigger={['click']}*/}
                            {/*    placement="bottomRight"*/}
                            {/*    arrow*/}
                            {/*>*/}
                            {/*    <CustomButton*/}
                            {/*        size={"small"}*/}
                            {/*        responsive={true}*/}
                            {/*        icon={*/}
                            {/*            <Image*/}
                            {/*                layout={"fixed"}*/}
                            {/*                height={16}*/}
                            {/*                width={16}*/}
                            {/*                src={Images.iconFilter.data}*/}
                            {/*                alt={Images.iconFilter.atl}*/}
                            {/*            />*/}
                            {/*        }*/}
                            {/*    >*/}
                            {/*        {t("button.filter")}*/}
                            {/*    </CustomButton>*/}
                            {/*</Dropdown>*/}
                        </div>
                    </div>
                    <div
                        className={`absolute inset-0 flex justify-end transition ${showSearchMb ? "block" : "hidden"}`}>
                        <div className={"bg-white flex items-center w-full mr-20 pl-2"}>
                            <a
                                onClick={() => setShowSearchMb(false)}
                                className={"flex items-center p-2 hover:bg-gray-500"}
                            >
                                <Image
                                    width={16}
                                    height={16}
                                    src={Images.iconCloseCircle.data}
                                    alt={Images.iconCloseCircle.atl}
                                />
                            </a>
                            <div className={"w-full"}>
                                {_buildForm()}
                            </div>
                        </div>
                    </div>
                </div>,
            }}
        >
            <CommonTagFilterFC
                is={isTags}
                onClose={handleCloseTag}
                data={queryParams}
                items={[
                    {
                        key: 'filter.q',
                        label: t('text.name')
                    },
                    {
                        key: 'filter.date_start',
                        label: t('text.dateStart'),
                    },
                    {
                        key: 'filter.date_end',
                        label: t('text.dateEnd'),
                    },
                    {
                        key: 'filter.active',
                        label: t('text.status'),
                        check: ['1', t('text.active'), t('text.inActive')],
                    },
                ]}
            />
            <ErrorItemFC
                status={vm.isLoading}
            >
                {

                    (vm.items.length > 0 && vm.isLoading === SendingStatus.success) || (vm.isLoading === SendingStatus.loading)
                        ? <div className={"grid-container"}>
                            {
                                vm.isLoading !== SendingStatus.loading ?
                                    vm.items.slice((page - 1) * vm.query.limit, page * vm.query.limit).map((item, index) => {
                                        return <TimelapseProjectItemFC key={index.toString()} item={item}/>
                                    })
                                    : <GridLoadingWidget>
                                        <TimelapseProjectSkeleton/>
                                    </GridLoadingWidget>
                            }
                        </div>
                        : <CommonEmptyFC/>
                }
            </ErrorItemFC>
            <CustomPagination
                isShow={vm.isLoading !== SendingStatus.loading && vm.items.length > 0 && vm.oMeta && vm.oMeta.pageCount > 1}
                defaultCurrent={page}
                current={page}
                total={vm.oMeta?.totalCount}
                defaultPageSize={vm.query.limit}
                onChange={onChangePage}
            />
            <FilterDrawerWidget
                onClick={onFilter}
                display={{
                    active: true,
                    order: false,
                    rangeDate: true,
                    timeRange: false,
                    fastFilter: false,
                }}
                onClose={onClose}
                visible={visible}
                current={{
                    active: queryParams.filter?.active as TActive,
                    dateStart: queryParams.filter?.date_start,
                    dateEnd: queryParams.filter?.date_end,
                }}
            />
        </WrapContentWidget>
    )
}
