import {Button, Cascader, Dropdown, Form, Menu, notification, Progress, Select, Space, TimePicker, Tooltip, Typography} from "antd";
import {useTranslation} from "react-i18next";
import moment from "moment";
import {TimelapseViewImageFC, TimelapseViewImageLoadingFC} from "../../../widgets/TimelapseViewImageFC";
import {useParams} from "react-router-dom";
import {CHashids} from "../../../../core/CHashids";
import {ErrorItemFC} from "../../../widgets/ErrorItemFC";
import LG from "lightgallery/react";
import lgZoom from "lightgallery/plugins/zoom";
import lgThumbnail from "lightgallery/plugins/thumbnail";
import lgFullScreen from "lightgallery/plugins/fullscreen";
import lgAutoplay from "lightgallery/plugins/autoplay";
import {E_ResUrlType, SendingStatus} from "../../../../const/Events";
import {CommonEmptyFC} from "../../../widgets/CommonFC";
import {useCallback, useEffect, useRef, useState} from "react";
import {AfterSlideDetail, InitDetail} from "lightgallery/lg-events";
import styles from "../../../../styles/module/TimelapseViewImage.module.scss";
import ReactDOM from "react-dom";
import {ArrowRightOutlined, DownloadOutlined, PlusCircleOutlined} from "@ant-design/icons";
import NoImage from "../../../../assets/image/no_image.png";
import axios from "axios";
import download from "downloadjs";
import {LightGallery} from "lightgallery/lightgallery";
import {TimelapseImageAnalyticsFilterVO} from "../../../../models/service/timelapse/TimelapseImageModel";
import {MediaQuery} from "../../../../core/MediaQuery";
import {Style} from "../../../../const/Style";
import {TimelapseImageAnalyticsToolAction} from "../../../../recoil/service/timelapse/timelapse_image_analytics_tool/TimelapseImageAnalyticsToolAction";
import {useRecoilBridgeAcrossReactRoots_UNSTABLE} from "recoil";
import {TimelapseViewImageLightGalleryAction} from "../../../../recoil/service/timelapse/timelapse_view_image/TimelapseViewImageAction";
import {Color} from "../../../../const/Color";
import {CustomTypography} from "../../../components/CustomTypography";
import {CustomButton} from "../../../components/CustomButton";
import {TimelapseDownloadImageFC} from "../../../widgets/TimelapseDownloadImageFC";

interface ILst {
    name: string,
    value: string,
}

export const TimelapseImageAnalyticsToolScreen = () => {
    const {t} = useTranslation();
    const [form] = Form.useForm();
    const {hash} = useParams<{ hash: string }>();
    const hashDecode = CHashids.decode(hash!);
    const lightGallery = useRef<any>(null);
    const RecoilBridge = useRecoilBridgeAcrossReactRoots_UNSTABLE()

    const {
        vm,
        onLoadItems,
    } = TimelapseImageAnalyticsToolAction();

    const {
        onChangeCurrentIndex
    } = TimelapseViewImageLightGalleryAction()

    const [queryParams, setQueryParams] = useState<TimelapseImageAnalyticsFilterVO>({
        page: vm.query.page,
        limit: new MediaQuery(Style.GridLimit).getPoint(vm.query.limit),
    });

    useEffect(() => {
        console.log('%cMount Screen: TimelapseImageAnalyticsToolScreen', Color.ConsoleInfo);


        return () => {
            console.log('%cUnmount Screen: TimelapseImageAnalyticsToolScreen', Color.ConsoleInfo);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        setQueryParams({
            ...queryParams,
            limit: vm.query.limit,
        })

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.query.limit])

    const lstSelectOrder: ILst[] = [
        {
            name: t("text.orderAsc"),
            value: 'asc',
        },
        {
            name: t("text.orderDesc"),
            value: 'desc',
        }
    ];

    const dayOfWeek = [
        t("dayOfWeek.monday"),
        t("dayOfWeek.tuesday"),
        t("dayOfWeek.wednesday"),
        t("dayOfWeek.thursday"),
        t("dayOfWeek.friday"),
        t("dayOfWeek.saturday"),
        t("dayOfWeek.sunday"),
    ]
    const lstMonth = Array.from({length: 31}, (_, k) => `${t("text.day")} ${k + 1}`);
    const formatTime = "HH:mm";
    const pickerTimeRanges: any = {};
    pickerTimeRanges[t('text.morning')] = [moment("08:00", formatTime), moment("16:00", formatTime)];
    pickerTimeRanges[t('text.night')] = [moment("19:00", formatTime), moment("23:59", formatTime)];

    const Options = [
        {
            value: "day",
            label: t("text.day"),
        },
        {
            value: "week",
            label: t("text.week"),
            children: dayOfWeek.map((value, index) => ({
                value: index,
                label: value,
            }))
        },
        {
            value: "month",
            label: t("text.month"),
            children: lstMonth.map((value, index) => ({
                value: index + 1,
                label: value,
            }))
        },
    ];

    const onFinish = (values: any) => {
        setQueryParams({
            ...queryParams,
            page: 1,
            limit: vm.query.limit,
            order: values.order,
            filter: {
                ...queryParams.filter,
                range: values.option[0],
                point: values.option[1],
                time_start: values.time ? moment(values.time[0]).format(formatTime) : undefined,
                time_end: values.time ? moment(values.time[1]).format(formatTime) : undefined,
            }
        })

        onLoadItems(hashDecode[1] as number, {
            page: 1,
            limit: vm.query.limit,
            order: values.order,
            filter: {
                range: values.option[0],
                point: values.option[1],
                time_start: values.time ? moment(values.time[0]).format(formatTime) : undefined,
                time_end: values.time ? moment(values.time[1]).format(formatTime) : undefined,
            }
        });
    }

    useEffect(() => {
        const divButtonCustom = document.getElementById('light-gallery-custom-next-2');

        if (divButtonCustom) {
            divButtonCustom.onclick = () => onLoadMore();
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.items])

    const onLoadMore = () => {
        onLoadItems(hashDecode[1] as number, {
            ...queryParams,
            page: vm.oMeta?.nextPage,
            limit: vm.query.limit,
        });
    };

    const onInitGallery = useCallback((detail: InitDetail) => {
        if (detail) {
            lightGallery.current = detail.instance;

            const divQuality = document.createElement("div");
            divQuality.id = "div-quality-2";
            divQuality.classList.add(styles.DivQuality);

            const divCustomNextButton = document.createElement("div");
            divCustomNextButton.id = "div-custom-next-button-2";
            divCustomNextButton.classList.add(styles.BtnCustom);

            const divCustomDownloadButton = document.createElement("div");
            divCustomDownloadButton.id = "div-custom-download-button-2";
            divCustomDownloadButton.classList.add("lg-icon");
            divCustomDownloadButton.classList.add(styles.DivFixLgToolbar);

            const lgToolbar = detail.instance.$toolbar.get();

            if (lgToolbar) {
                lgToolbar.insertAdjacentElement('afterend', divQuality);
                lgToolbar.insertAdjacentElement('afterend', divCustomNextButton);
                lgToolbar.append(divCustomDownloadButton);
            }

            ReactDOM.render(
                <RecoilBridge>
                    <DivQuality/>
                </RecoilBridge>,
                divQuality
            );

            ReactDOM.render(
                <RecoilBridge>
                    <DivCustomDownloadButton/>
                </RecoilBridge>,
                divCustomDownloadButton
            );
        }


        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (lightGallery !== null) {
            ReactDOM.render(
                <RecoilBridge>
                    <DivCustomNextButton machineId={hashDecode[1] as number} onLoadMore={onLoadMore}/>
                </RecoilBridge>,
                document.getElementById('div-custom-next-button-2')
            );

            (lightGallery.current as LightGallery).refresh();
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.items]);

    const onAfterSlide = (detail: AfterSlideDetail) => onChangeCurrentIndex(detail.index);

    const getItems = useCallback(() => {
        return (
            <div className={"w-full grid grid-cols-1 sm:grid-cols-2 xl:grid-cols-3 xxl:grid-cols-4  gap-4"}>
                {
                    vm.items.map((item, i) => {
                        const subHtml =
                            <div id={`caption-${i}`} className={styles.BoxContent}>
                                <div className={"custom-caption flex w-full justify-between"}>
                                    <CustomTypography
                                        className={"caption-info-left"}
                                        isStrong
                                        textStyle={"text-body-text-3"}
                                        style={{
                                            color: "#1a8983",
                                        }}
                                    >
                                        #{item.order}
                                    </CustomTypography>
                                    <CustomTypography
                                        className={"caption-info-right"}
                                        textStyle={"text-body-text-3"}
                                        isStrong
                                    >
                                        {item.dateShotFormatted(t('format.dateTimeShort'))}
                                    </CustomTypography>
                                </div>

                                {
                                    item.sensor !== undefined
                                        ? <div className={styles.BoxInfoSensor}>
                                            {
                                                item.sensor.map((value, i) => {
                                                        return (
                                                            <div key={i}
                                                                 className={"custom-caption flex w-full justify-between"}
                                                            >
                                                                <CustomTypography
                                                                    textStyle={"text-body-text-3"}
                                                                >
                                                                    {value.name}
                                                                </CustomTypography>
                                                                <CustomTypography
                                                                    textStyle={"text-body-text-3"}
                                                                >
                                                                    {value.value && value.value}
                                                                    {value.unit2 && value.unit2}
                                                                </CustomTypography>
                                                            </div>
                                                        );
                                                    }
                                                )
                                            }
                                        </div>
                                        : null
                                }
                            </div>;

                        return (
                            <div key={i} className={"relative"}>
                                <div
                                    className="cursor-pointer gallery-item"
                                    data-src={item.getShotImageUrl({
                                        type: E_ResUrlType.Thumb,
                                        size: 1280
                                    })}
                                    data-thumb={item.getShotImageUrl({
                                        type: E_ResUrlType.Thumb,
                                        size: 426
                                    })}
                                    data-sub-html={`#caption-${i}`}
                                >
                                    <TimelapseViewImageFC
                                        item={item}
                                        index={i}
                                        isDivider={true}
                                    >
                                        {subHtml}
                                    </TimelapseViewImageFC>
                                </div>
                                <div className={"absolute top-2 left-2"}>
                                    <TimelapseDownloadImageFC
                                        wrapClass={"btn-main-auto-icon"}
                                        buttonStyle={{
                                            background: "#172723",
                                            opacity: 0.5,
                                        }}
                                        viewBoxIcon={25}
                                        item={item}
                                        isLabel={false}
                                    />
                                </div>
                            </div>
                        );
                    })
                }
            </div>
        );

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.items]);

    const onReset = () => {
        form.resetFields();
    }

    return (
        <div
            style={{
                overflowY: "hidden",
            }}
            className={"h-full w-full flex flex-col md:flex-row"}
        >
            <div
                style={{
                    boxShadow: "1px 1px 1px rgba(79, 79, 79, 0.15)",
                }}
                className={"w-full md:flex-none md:w-60  p-4 3xl:w-80 3xl:py-10 3xl:px-6 bg-white"}
            >
                <Form
                    onFinish={onFinish}
                    form={form}
                    labelCol={{span: 8, md: 24,}}
                    wrapperCol={{span: 16, md: 24}}
                >
                    <Form.Item
                        labelAlign={"left"}
                        colon={false}
                        className={"form-item label-typography-body-text-2 label-strong"}
                        name={"option"}
                        label={t("text.target")}
                        rules={
                            [
                                {
                                    required: true,
                                    message: t("validation.emptyData")
                                }
                            ]
                        }
                    >
                        <Cascader
                            className={"cascader-main"}
                            placeholder={t("message.select")}
                            options={Options}
                        />
                    </Form.Item>
                    <Form.Item
                        labelAlign={"left"}
                        colon={false}
                        className={"form-item label-typography-body-text-2 label-strong"}
                        name={"time"}
                        label={t("text.timePeriod")}
                    >
                        <TimePicker.RangePicker
                            className={"w-full ranger-time-picker-main ranger-time-picker-border-0 ranger-time-picker-bg-dark4 ranger-time-picker-radius-8"}
                            placeholder={
                                [t("text.timeStart"), t("text.timeEnd")]
                            }
                            allowClear={true}
                            allowEmpty={[true, true]}
                            ranges={pickerTimeRanges}
                            format={formatTime}
                        />
                    </Form.Item>
                    <Form.Item
                        labelAlign={"left"}
                        colon={false}
                        className={"form-item label-typography-body-text-2 label-strong"}
                        name={"order"}
                        label={t("text.order")}
                    >
                        <Select
                            className={"select-form-main"}
                            key={'select-order'}
                            style={{width: '100%'}}
                            placeholder={t("text.selectOrder")}
                        >
                            {
                                lstSelectOrder.map((value, index) =>
                                    <Select.Option key={`order ${index}`} value={value.value!}>
                                        <Typography.Text strong>{value.name}</Typography.Text>
                                    </Select.Option>
                                )
                            }
                        </Select>
                    </Form.Item>
                    <div className={"flex justify-between"}>
                        <CustomButton
                            size={"small"}
                            type={"outline"}
                            htmlType="reset"
                            onClick={onReset}
                        >
                            {t("button.setAgain")}
                        </CustomButton>
                        <CustomButton
                            size={"small"}
                            loading={vm.isLoading === SendingStatus.loading}
                            htmlType="submit"
                        >
                            {t("button.filter")}
                        </CustomButton>
                    </div>
                </Form>
            </div>
            <ErrorItemFC
                status={vm.isLoading}
            >
                <div
                    style={{
                        overflowY: "scroll",
                    }}
                    className={"flex-col flex-grow h-full p-4 3xl:py-10 3xl:px-8"}
                >
                    <LG
                        elementClassNames={"grow"}
                        galleryId={"2"}
                        onAfterSlide={detail => onAfterSlide(detail)}
                        preload={2}
                        selector={'.gallery-item'}
                        appendSubHtmlTo={".lg-item"}
                        speed={500}
                        plugins={[lgZoom, lgThumbnail, lgFullScreen, lgAutoplay]}
                        download={false}
                        onInit={onInitGallery}
                        mode="lg-slide"
                        addClass={"lg-custom-thumbnails"}
                        allowMediaOverlap={true}
                        toggleThumb={true}
                        appendThumbnailsTo={".lg-components"}
                        loop={false}
                        slideEndAnimation={false}
                        pager={true}
                        thumbnail={true}
                        animateThumb={true}
                        exThumbImage={"data-thumb"}
                        alignThumbnails={"middle"}
                        fullScreen={true}
                        autoplay={true}
                        zoomFromOrigin={false}
                        slideShowInterval={2000}
                        scale={10}
                    >
                        {
                            vm.isLoading === SendingStatus.loading && !vm.isNextAvailable
                                ? <TimelapseViewImageLoadingFC limit={vm.query.limit}/>
                                : vm.items.length > 0
                                    ? getItems()
                                    : <CommonEmptyFC/>
                        }
                    </LG>

                    {
                        vm.isNextAvailable ?
                            <div className={"flex w-full justify-center p-4"}>
                                <CustomButton
                                    loading={vm.isLoading === SendingStatus.loading}
                                    onClick={onLoadMore}
                                    icon={<PlusCircleOutlined/>}
                                >
                                    {t("button.loadMore")}
                                </CustomButton>
                            </div>
                            : null
                    }
                </div>
            </ErrorItemFC>
        </div>
    );

}

const DivQuality = () => {
    const {t} = useTranslation();
    const {vm: vmLG} = TimelapseViewImageLightGalleryAction()
    const {vm} = TimelapseImageAnalyticsToolAction();
    const [isHD, setIsHD] = useState(true);
    const [isLoadingOrig, setIsLoadingOrig] = useState(false);
    const lightGallery = document.getElementsByClassName('lg-custom-thumbnails').item(1);

    const onClickHD = useCallback(() => {
        if (lightGallery && vmLG.index) {
            const imgQuery = lightGallery.querySelector("img[data-index='" + vmLG.index + "']")

            if (imgQuery) {
                imgQuery.removeAttribute("src");
                imgQuery.setAttribute("src", vm.items[vmLG.index].getShotImageUrl({
                    type: E_ResUrlType.Thumb,
                    size: 1280
                }) ?? '');
            }
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vmLG.index, vm.items])

    const onClickOrig = useCallback(() => {
        if (isHD) {
            if (lightGallery && vmLG.index !== undefined) {
                const imgQuery = lightGallery.querySelector("img[data-index='" + vmLG.index + "']");

                if (imgQuery) {
                    setIsLoadingOrig(true);
                    imgQuery.removeAttribute("src");

                    const image = new Image();
                    image.src = vm.items[vmLG.index].getShotImageUrl({
                        type: E_ResUrlType.Orig
                    }) ?? '';
                    image.onload = () => {
                        imgQuery.setAttribute("src", image.src);
                        setIsLoadingOrig(false);
                    }
                    image.onerror = () => {
                        imgQuery.setAttribute("src", NoImage.src);
                        setIsLoadingOrig(false);
                    }
                }
            }
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.items, vmLG.index, isHD])

    useEffect(() => {
        if (!isHD) {
            setIsHD(true);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vmLG.index])

    return (
        <div
            style={{
                textAlign: "center"
            }}
        >
            <Space>
                <Button
                    onClick={() => {
                        onClickHD();
                        setIsHD(true);
                    }}
                    className={isHD ? styles.BtnActive : styles.BtnInActive}
                >
                    <Typography.Text
                        className={styles.AntTypography}
                        strong
                    >
                        {t("text.imageHd")}
                    </Typography.Text>
                </Button>
                <Button
                    loading={isLoadingOrig}
                    onClick={() => {
                        onClickOrig();
                        setIsHD(false);
                    }}
                    className={!isHD ? styles.BtnActive : styles.BtnInActive}
                >
                    <Typography.Text
                        className={styles.AntTypography}
                        strong
                    >
                        {t("text.imageOrig")}
                    </Typography.Text>
                </Button>
            </Space>
        </div>
    )
}

const DivCustomNextButton = (props: {
    machineId: number,
    onLoadMore: Function
}) => {
    const {t} = useTranslation();
    const {vm} = TimelapseImageAnalyticsToolAction();
    const {vm: vmLG} = TimelapseViewImageLightGalleryAction()

    return (
        <Tooltip
            placement="bottom"
            title={t("button.loadMore")}
        >
            <Button
                id={'light-gallery-custom-next-2'}
                type="primary"
                className={`${styles.BtnCustomVisible} ${vmLG.index !== (vm.items.length - 1) ? styles.BtnCustomHidden : ''}`}
                loading={vm.isLoading === SendingStatus.loading}
                icon={<PlusCircleOutlined/>}
                style={{
                    backgroundColor: "#009579",
                    borderColor: "#009579",
                    height: "40px",
                }}
                onClick={() => props.onLoadMore}
            >
                <Typography.Text
                    className={`text-white`}
                    strong
                >
                    <ArrowRightOutlined/>
                </Typography.Text>
            </Button>
        </Tooltip>
    )
}

const DivCustomDownloadButton = () => {
    const {t} = useTranslation();
    const {vm: vmLG} = TimelapseViewImageLightGalleryAction()
    const {vm} = TimelapseImageAnalyticsToolAction();
    const item = vmLG.index ? vm.items[vmLG.index] : undefined;

    const menuDownload = useCallback(() => {
        return (
            <Menu>
                <Menu.Item
                    key="1"
                    onClick={_ => downloadImage(item?.getShotImageUrl({
                        type: E_ResUrlType.Download
                    }))}
                >
                    {t("text.dlOrig")}
                </Menu.Item>
                <Menu.Item
                    key="2"
                    onClick={_ => downloadImage(item?.getShotImageUrl({
                        type: E_ResUrlType.Download,
                        attach: true
                    }))}
                >
                    {t("text.dlAttach")}
                </Menu.Item>
            </Menu>
        );

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vmLG.index]);

    const [percentComplete, setPercentComplete] = useState<number | undefined>(undefined);

    const downloadImage = (url?: string) => {
        if (url) {
            axios
                .get(url, {
                    responseType: "blob",
                    onDownloadProgress: (evt: ProgressEvent) => {
                        if (evt.lengthComputable) {
                            setPercentComplete((evt.loaded / evt.total) * 100);
                        }
                    }
                })
                .then(r => {
                    const data = r.data as Blob;
                    const filename = `${item?.order}_${item?.dateShotFormatted("DD-MM-YYYY-HH-mm-ss")}.jpg`;

                    download(data, filename, data.type);

                    setPercentComplete(undefined);

                    notification.success({
                        message: t('message.downloadImageSuccess'),
                    });
                })
                .catch(_ => {
                    setPercentComplete(undefined);

                    notification.error({
                        message: t('message.downloadImageFailure'),
                    });
                });
        }
    }

    return (
        vmLG.index
            ? percentComplete !== undefined
                ? <div
                    style={{
                        paddingTop: "4px"
                    }}
                >
                    <Progress
                        type="circle"
                        strokeColor={{
                            '0%': '#108ee9',
                            '100%': '#87d068'
                        }}
                        width={20}
                        percent={percentComplete}
                        strokeWidth={15}
                        showInfo={false}
                    />
                </div>
                : item?.sensor && item.sensor.length > 0
                    ? <Dropdown
                        trigger={['click']}
                        overlay={menuDownload}
                        placement={"bottomRight"}
                    >
                        <Button
                            className={`${styles.ButtonFixIconSize} lg-icon`}
                            type={"text"}
                            style={{
                                marginLeft: 8
                            }}
                            icon={
                                <DownloadOutlined/>
                            }
                        >
                        </Button>
                    </Dropdown>
                    : <span
                        className={"lg-icon"}
                        onClick={_ => downloadImage(item?.getShotImageUrl({
                            type: E_ResUrlType.Download
                        }))}
                    >
                        <DownloadOutlined/>
                    </span>
            : null
    )
}
