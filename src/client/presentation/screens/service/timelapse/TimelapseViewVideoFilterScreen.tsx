import {useTranslation} from "react-i18next";
import {useLocation, useNavigate} from "react-router";
import {lazy, Suspense, useCallback, useEffect, useState} from "react";
import {UrlQuery} from "../../../../core/UrlQuery";
import {Color} from "../../../../const/Color";
import {Tag} from "antd";
import {ErrorItemFC} from "../../../widgets/ErrorItemFC";
import {SendingStatus} from "../../../../const/Events";
import {CommonEmptyFC, CommonLoadingSpinFC, CommonTagFilterFC} from "../../../widgets/CommonFC";
import {Utils} from "../../../../core/Utils";
import {TimelapseVideoFilterModel, TimelapseVideoFilterVO} from "../../../../models/service/timelapse/TimelapseVideoFilterModel";
import ErrorBoundaryComponent from "../../../widgets/ErrorBoundaryComponent";
import {TParamMachine} from "../../../../const/Types";
import {TimelapseViewVideoFilterAction} from "../../../../recoil/service/timelapse/timelapse_view_video_filter/TimelapseViewVideoFilterAction";
import {CustomTypography} from "../../../components/CustomTypography";
import {CustomButton} from "../../../components/CustomButton";
import Image from "next/image";
import {Images} from "../../../../const/Images";
import {useMaster} from "../../../hooks/useMaster";
import FilterDrawerWidget from "../../../widgets/FilterDrawerWidget";
import {useConfigContext} from "../../../contexts/ConfigContext";
import {RouteConfig} from "../../../../config/RouteConfig";

const PIXIPlayer = lazy(() => import("../../../modules/vf/PIXIPlayer"));

export const TimelapseViewVideoFilterScreen = (props: {
    machineId: any,
    title: string,
    machines: TParamMachine[],
    tab: string,
    allTime?: [string | undefined, string | undefined]
}) => {
    const {t} = useTranslation()
    const navigate = useNavigate()
    const location = useLocation()
    const URLQuery = new UrlQuery(location.search)
    const [config] = useConfigContext()
    const {master} = useMaster()

    const [visible, setVisible] = useState(false)

    const showDrawer = () => {
        setVisible(true)
    }

    const onClose = () => {
        setVisible(false)
    }

    const {
        vm,
        onLoadItem,
        resetStateWithEffect,
    } = TimelapseViewVideoFilterAction()

    const filter = URLQuery.get('filter', {})

    const [queryParams, setQueryParams] = useState<TimelapseVideoFilterVO>({
        filter: filter,
        fields: 'total,render'
    })

    useEffect(() => {
        console.log('%cMount Screen: TimelapseViewVideoFilterScreen', Color.ConsoleInfo)

        if (
            (vm.item === undefined
                || vm.key !== props.machineId.toString()
                || (vm.timestamp !== undefined && Utils.checkHourState(vm.timestamp))
            )
            && props.tab === "video_filter"
        ) {
            if (!vm.item) resetStateWithEffect();
            onLoadItem(props.machineId, (new UrlQuery(queryParams)).toObject());
        }

        return () => {
            console.log('%cUnmount Screen: TimelapseViewVideoFilterScreen', Color.ConsoleInfo)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.machineId, props.tab])

    const handleCloseTag = (name: string) => {
        const urlQueryParams = new UrlQuery(queryParams)
        urlQueryParams.delete(name)
        navigate({
            search: urlQueryParams.toString(),
        }, {
            replace: true,
            state: {
                name: props.title,
                machines: props.machines
            }
        })

        onLoadItem(props.machineId, urlQueryParams.toObject())
        setQueryParams(urlQueryParams.toObject())
    }

    const onFilter = (data: {
        period: string,
        dateStart: string,
        dateEnd: string
        timeStart: string,
        timeEnd: string
    }) => {
        const urlQueryParams = new UrlQuery(queryParams)
        urlQueryParams.set('filter.period', data.period)
        urlQueryParams.set('filter.date_start', data.dateStart)
        urlQueryParams.set('filter.date_end', data.dateEnd)
        urlQueryParams.set('filter.time_start', data.timeStart)
        urlQueryParams.set('filter.time_end', data.timeEnd)

        setQueryParams(urlQueryParams.toObject())
        onLoadItem(props.machineId, urlQueryParams.toObject())

        navigate({
            search: urlQueryParams.toString()
        }, {
            replace: true,
            state: {
                name: props.title,
                machines: props.machines,
            }
        })
    }

    useEffect(() => {
        if (master.header) {
            master.header.setHeader({
                ...master.header,
                isLoading: vm.isLoading,
                onReload: onClickReload,
            });
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.isLoading])

    const onClickReload = () => onLoadItem(props.machineId, (new UrlQuery(queryParams)).toObject())

    const isTags = (typeof queryParams.filter?.period !== "undefined" && queryParams.filter?.period?.length > 0)
        || (typeof queryParams.filter?.date_start !== "undefined" && queryParams.filter?.date_start?.length > 0)
        || (typeof queryParams.filter?.date_end !== "undefined" && queryParams.filter?.date_end?.length > 0)
        || (typeof queryParams.filter?.time_start !== "undefined" && queryParams.filter?.time_start?.length > 0)
        || (typeof queryParams.filter?.time_end !== "undefined" && queryParams.filter?.time_end?.length > 0)

    const _buildPixiPlayer = useCallback(() => {
        return props.tab === "video_filter" ? <PIXIPlayer item={vm.item} tab={props.tab}/> : null;
    }, [vm.item, props.tab])

    return (
        <div className="TimelapseViewVideoFilterScreen relative container mx-auto">
            <div className={"w-full flex flex-col gap-2 md:flex-row"}>
                {
                    vm.isLoading === SendingStatus.success && vm.item !== undefined && vm.item.first !== undefined && vm.item.last !== undefined
                        ? <Tag className={"tag-main tag-no-icon w-full "}>
                            <CustomTypography>
                                <CustomTypography isStrong>{t('text.start')}&nbsp;</CustomTypography>
                                {vm.item.firstDateShotFormatted(t('format.date'))}
                                &nbsp;|&nbsp;<CustomTypography isStrong>{t('text.end')}&nbsp;</CustomTypography>
                                {vm.item.lastDateShotFormatted(t('format.date'))}
                            </CustomTypography>
                        </Tag>
                        : <div className={"flex-grow w-full animate-pulse"}>
                            <div className={"bg-gray-300 h-6 w-full"}/>
                        </div>
                }
                <div className={"flex-none flex gap-2"}>
                    <CustomButton
                        onClick={() => {
                            if (!vm.item) {
                                return
                            }

                            if (config.agent && config.agent.isAppDesktop()) {
                                const url = new URL(vm.item.render!)
                                let pathname = url.pathname

                                if (pathname.indexOf('/') === 0) {
                                    pathname = pathname.substring(1, pathname.length)
                                }

                                navigate(`${RouteConfig.TIMELAPSE}/e-rv/${pathname}`)
                            } else {
                                window.open(vm.item.render, '_blank')
                            }
                        }}
                        icon={(
                            <Image
                                src={Images.iconVideoCamera.data}
                                alt={Images.iconVideoCamera.atl}
                            />
                        )}
                    >
                        Render video
                    </CustomButton>
                    <CustomButton
                        onClick={showDrawer}
                        icon={
                            <Image
                                src={Images.iconFilter.data}
                                alt={Images.iconFilter.atl}
                            />
                        }
                    >
                        {t("button.filter")}
                    </CustomButton>
                    {/*<Dropdown*/}
                    {/*    overlay={*/}
                    {/*        <TimelapseViewVideoFilterFilterFC*/}
                    {/*            onClick={onFilter}*/}
                    {/*            current={{*/}
                    {/*                period: queryParams.filter?.period ?? 'm',*/}
                    {/*                dateStart: queryParams.filter?.date_start,*/}
                    {/*                dateEnd: queryParams.filter?.date_end,*/}
                    {/*                timeStart: queryParams.filter?.time_start,*/}
                    {/*                timeEnd: queryParams.filter?.time_end,*/}
                    {/*            }}*/}
                    {/*            allTime={props.allTime}*/}
                    {/*        />*/}
                    {/*    }*/}
                    {/*    trigger={['click']}*/}
                    {/*    placement="bottomRight"*/}
                    {/*    arrow*/}
                    {/*>*/}
                    {/*    <CustomButton*/}
                    {/*        icon={<Image*/}
                    {/*            src={Images.iconFilter.data}*/}
                    {/*            alt={Images.iconFilter.atl}*/}
                    {/*        />*/}
                    {/*        }*/}
                    {/*    >*/}
                    {/*        {t("button.filter")}*/}
                    {/*    </CustomButton>*/}
                    {/*</Dropdown>*/}
                </div>
            </div>
            <CommonTagFilterFC
                className={"pt-6 flex flex-wrap gap-2"}
                is={isTags}
                onClose={handleCloseTag}
                data={queryParams}
                items={[
                    {
                        key: 'filter.period',
                        label: t('text.timeFrame'),
                        check: [
                            ['m', 'n', 'mn'],
                            [t("text.morning"), t("text.night"), t("text.lightAndDark")]
                        ],
                    },
                    {
                        key: 'filter.date_start',
                        label: t('text.dateStart'),
                    },
                    {
                        key: 'filter.date_end',
                        label: t('text.dateEnd'),
                    },
                    {
                        key: 'filter.time_start',
                        label: t('text.timeStart'),
                    },
                    {
                        key: 'filter.time_end',
                        label: t('text.timeEnd'),
                    },
                ]}
            />
            <ErrorItemFC
                status={vm.isLoading}
            >
                {
                    vm.isLoading === SendingStatus.loading
                        ? <CommonLoadingSpinFC/>
                        : vm.item instanceof TimelapseVideoFilterModel && vm.item.first !== undefined && vm.item.last !== undefined
                            ? <div className={"my-5 flex justify-center"}>
                                <ErrorBoundaryComponent>
                                    <Suspense fallback={<CommonLoadingSpinFC/>}>
                                        {
                                            _buildPixiPlayer()
                                        }
                                        {/*<PIXIPlayer item={vm.item} tab={props.tab}/>*/}
                                    </Suspense>
                                </ErrorBoundaryComponent>
                            </div>
                            : <CommonEmptyFC/>
                }
            </ErrorItemFC>
            <FilterDrawerWidget
                onClick={onFilter}
                display={{
                    active: false,
                    order: false,
                    rangeDate: true,
                    timeRange: true,
                    fastFilter: true,
                }}
                onClose={onClose}
                visible={visible}
                current={{
                    period: queryParams.filter?.period ?? 'm',
                    dateStart: queryParams.filter?.date_start,
                    dateEnd: queryParams.filter?.date_end,
                    timeStart: queryParams.filter?.time_start,
                    timeEnd: queryParams.filter?.time_end,
                }}
                allTime={props.allTime}
            />
        </div>
    )
}
