import {ErrorItemFC} from "../../../widgets/ErrorItemFC";
import {SendingStatus} from "../../../../const/Events";
import {Style} from "../../../../const/Style";
import {TimelapseMachineItemFC, TimelapseMachineSkeleton} from "../../../widgets/TimelapseMachineItemFC";
import {CommonEmptyFC} from "../../../widgets/CommonFC";
import {useTranslation} from "react-i18next";
import {useLocation, useNavigate} from "react-router";
import {useParams} from "react-router-dom";
import {TParamMachine, TTimelapseMachineParamState} from "../../../../const/Types";
import {CHashids} from "../../../../core/CHashids";
import {UrlQuery} from "../../../../core/UrlQuery";
import {TimelapseMachineAction} from "../../../../recoil/service/timelapse/timelapse_machine/TimelapseMachineAction";
import {useCallback, useEffect, useState} from "react";
import {Vr360FilterVO} from "../../../../models/Vr360Model";
import {Color} from "../../../../const/Color";
import {Utils} from "../../../../core/Utils";
import {MediaQuery} from "../../../../core/MediaQuery";
import {WrapContentWidget} from "../../../widgets/WrapContentWidget";
import CustomPagination from "../../../components/CustomPagination";

export const TimelapseMachineScreen = (props: {
    state?: TTimelapseMachineParamState
}) => {
    const {t} = useTranslation()
    const navigate = useNavigate()
    const location = useLocation()
    const {hash} = useParams<{ hash: string }>()
    const params: TTimelapseMachineParamState | undefined = (location.state as (TTimelapseMachineParamState | undefined)) ?? props.state;
    const projectId = CHashids.decodeGetFirst(hash!)
    const URL = new UrlQuery(location.search)
    const _title = params?.name.toUpperCase() ?? t('title.timelapse');

    const {
        vm,
        onLoadItems,
        resetStateWithEffect,
    } = TimelapseMachineAction()

    const filter = URL.get('filter', {})
    const sort = URL.get('sort')
    const order = URL.get('order')
    const page = URL.getInt('page', vm.query.page)
    const limit = URL.getInt('limit', vm.query.limit)

    const [queryParams, setQueryParams] = useState<Vr360FilterVO>({
        filter: filter,
        sort: sort,
        order: order,
        page: page,
        limit: limit,
    })

    useEffect(() => {
        console.log('%cMount Screen: TimelapseMachineScreen', Color.ConsoleInfo);

        if (
            vm.items.length === 0
            || vm.key !== projectId!.toString()
            || (vm.timestamp && Utils.checkHourState(vm.timestamp))
        ) {
            const urlQueryParams = new UrlQuery(queryParams);
            urlQueryParams.set('limit', (new MediaQuery(Style.GridLimit)).getPoint(limit));
            setQueryParams(urlQueryParams.toObject());
            if (vm.items.length > 0) {
                resetStateWithEffect(undefined, val => {
                    onLoadItems(projectId!, urlQueryParams.toObject(), val);
                });
            } else {
                onLoadItems(projectId!, urlQueryParams.toObject());
            }
        }

        return () => {
            console.log('%cUnmount Screen: TimelapseMachineScreen', Color.ConsoleInfo);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const onChangePage = (page: number) => {
        const urlQueryParams = new UrlQuery(queryParams);
        urlQueryParams.set('page', page);

        navigate({
            search: urlQueryParams.toString()
        }, {
            replace: true,
            state: params,
        })

        setQueryParams(urlQueryParams.toObject());
        onLoadItems(projectId!, urlQueryParams.toObject());
    }

    const onClickReload = useCallback(() => {
        const urlQueryParams = new UrlQuery(queryParams);

        onLoadItems(projectId!, urlQueryParams.toObject());

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [queryParams])

    const getMachine = useCallback(() => {
        const data: TParamMachine[] = [];

        if (vm.items.length > 0) {
            vm.items.forEach((value) => {
                if (value.machineId && value.name) {
                    data.push({
                        machineId: value.machineId,
                        active: value.active,
                        total: value.total,
                        name: value.name,
                    })
                }
            })
        }

        return data;
    }, [vm.items])

    const machines = getMachine()

    const _buildLoading = (count: number) => {
        const _number = (new MediaQuery({
            xs: 1,
            sm: 2,
            md: 3,
            lg: 3,
            xl: 3,
            xxl: 4,
            xxxl: 4,
        })).getPoint(4);

        return (
            <>
                {
                    new Array(count > _number ? _number : count).fill('OK').map((_, index: number) => {
                        return <TimelapseMachineSkeleton key={index}/>
                    })
                }
            </>
        );
    }

    return (
        <WrapContentWidget
            masterHeader={{
                title: _title,
                isLoading: vm.isLoading,
                onReload: onClickReload,
            }}
            bodyHeader={{
                title: _title,
            }}
        >
            <ErrorItemFC
                status={vm.isLoading}
            >
                {
                    (vm.items.length > 0 && vm.isLoading === SendingStatus.success) || (vm.isLoading === SendingStatus.loading)
                        ? <div
                            className={"grid grid-cols-1 gap-4 sm:grid-cols-2 md:grid-cols-3 md:gap-6 xxl:grid-cols-4 3xl:gap-8"}>
                            {
                                vm.isLoading !== SendingStatus.loading
                                    ? vm.items.slice((page - 1) * vm.query.limit, page * vm.query.limit).map((item, _) => (
                                        <TimelapseMachineItemFC
                                            key={item.machineId}
                                            item={item}
                                            machines={machines!}
                                            projectId={projectId as number}
                                            isSensor={params ? params.isSensor : undefined}
                                        />
                                    ))
                                    : _buildLoading(params ? params.machines.length : 4)
                            }
                        </div>
                        : <CommonEmptyFC/>
                }
            </ErrorItemFC>
            <CustomPagination
                isShow={vm.isLoading !== SendingStatus.loading && vm.items.length > 0 && vm.oMeta && vm.oMeta.pageCount > 1}
                defaultCurrent={page}
                current={page}
                total={vm.oMeta?.totalCount}
                defaultPageSize={vm.query.limit}
                onChange={onChangePage}
            />
        </WrapContentWidget>
    )
}
