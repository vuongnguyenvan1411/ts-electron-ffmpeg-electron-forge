import {useLocation, useNavigate} from "react-router";
import {UrlQuery} from "../../../../core/UrlQuery";
import {useCallback, useEffect, useState} from "react";
import {Color} from "../../../../const/Color";
import {Card, Select, Space} from "antd";
import {ErrorItemFC} from "../../../widgets/ErrorItemFC";
import {SendingStatus} from "../../../../const/Events";
import {TimelapseAnalyticsFilterVO, TimelapseSensorModel} from "../../../../models/service/timelapse/TimelapseSensorModel";
import {TimelapseViewAnalyticsFC} from "../../../widgets/TimelapseViewAnalyticsFC";
import {CommonEmptyFC, CommonLoadingSpinFC, CommonTagFilterFC} from "../../../widgets/CommonFC";
import {Utils} from "../../../../core/Utils";
import {useTranslation} from "react-i18next";
import {FilterOutlined} from "@ant-design/icons";
import {TFilterData, TParamMachine} from "../../../../const/Types";
import {TimelapseViewAnalyticsAction} from "../../../../recoil/service/timelapse/timelapse_view_analytics/TimelapseViewAnalyticsAction";
import {CustomButton} from "../../../components/CustomButton";
import {useMaster} from "../../../hooks/useMaster";
import FilterDrawerWidget from "../../../widgets/FilterDrawerWidget";

export const TimelapseViewAnalyticScreen = (props: {
    machineId: number,
    title: string,
    tab: string,
    machines: TParamMachine[],
}) => {
    const {t} = useTranslation()
    const navigate = useNavigate()
    const location = useLocation()
    const {master} = useMaster()
    const URL = new UrlQuery(location.search)
    const filter = URL.get('filter', {});
    const [selected, setSelected] = useState(0);
    const [queryParams, setQueryParams] = useState<TimelapseAnalyticsFilterVO>({
        filter: filter,
    });
    const [visible, setVisible] = useState(false);

    const showDrawer = () => {
        setVisible(true);
    };

    const onClose = () => {
        setVisible(false);
    };

    const {
        vm,
        onLoadItem,
        resetStateWithEffect,
    } = TimelapseViewAnalyticsAction()

    useEffect(() => {
        console.log('%cMount Screen: TimelapseViewAnalyticScreen', Color.ConsoleInfo);

        if (
            (vm.item === undefined
                || vm.key !== props.machineId.toString()
                || (vm.timestamp !== undefined && Utils.checkHourState(vm.timestamp))
            )
            && props.tab === "analytics"
        ) {
            const urlQueryParams = new UrlQuery(queryParams);

            if (typeof vm.item !== "undefined") resetStateWithEffect();

            onLoadItem(props.machineId, urlQueryParams.toObject());
        }

        return () => {
            console.log('%cUnmount Screen: TimelapseViewAnalyticScreen', Color.ConsoleInfo);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.machineId, props.tab])

    const getItems = useCallback(() => {
        return vm.item?.sensor?.map((value, index) => {
            return (
                <Select.Option key={value.id} value={index}>
                    {value.name}
                </Select.Option>
            )
        })
    }, [vm.item])

    const handleChange = (value: number) => setSelected(value)

    const isTags = (typeof queryParams.filter?.date_start !== "undefined" && queryParams.filter?.date_start?.length > 0)
        || (typeof queryParams.filter?.date_end !== "undefined" && queryParams.filter?.date_end?.length > 0)
        || (typeof queryParams.filter?.time_start !== "undefined" && queryParams.filter?.time_start?.length > 0)
        || (typeof queryParams.filter?.time_end !== "undefined" && queryParams.filter?.time_end?.length > 0)

    const handleCloseTag = (name: string) => {
        const urlQueryParams = new UrlQuery(queryParams);
        urlQueryParams.delete(name);

        setQueryParams(urlQueryParams.toObject());

        navigate({
            search: urlQueryParams.toString()
        }, {
            replace: true,
            state: {
                name: props.title,
                machines: props.machines,
            }
        })

        onLoadItem(props.machineId, urlQueryParams.toObject());
    }

    const onFilter = (data: TFilterData) => {
        const urlQueryParams = new UrlQuery(queryParams);
        urlQueryParams.set('filter.date_start', data.dateStart);
        urlQueryParams.set('filter.date_end', data.dateEnd);
        urlQueryParams.set('filter.time_start', data.timeStart);
        urlQueryParams.set('filter.time_end', data.timeEnd);

        setQueryParams(urlQueryParams.toObject());

        onLoadItem(props.machineId, urlQueryParams.toObject());

        navigate({
            search: urlQueryParams.toString()
        }, {
            replace: true,
            state: {
                name: props.title,
                machines: props.machines,
            }
        })
    }

    const onClickReload = () => onLoadItem(props.machineId, (new UrlQuery(queryParams)).toObject());

    useEffect(() => {
        if (master.header) {
            master.header.setHeader({
                ...master.header,
                isLoading: vm.isLoading,
                onReload: onClickReload,
            });
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.isLoading])

    return (
        <div className="page_timelapse_info_image relative">
            <Card
                className={"mb-3"}
                bodyStyle={{
                    padding: "0.6rem 1rem",
                }}
            >
                <div className={"w-full flex items-center justify-between"}>
                    {
                        vm.item !== undefined && vm.item.sensor && vm.item.sensor.length > 0 ? vm.isLoading === SendingStatus.success
                            ? <Select
                                value={selected}
                                defaultValue={selected}
                                onChange={handleChange}
                            >
                                {getItems()}
                            </Select>
                            : <div className={"animate-pulse"}>
                                <div className={"bg-gray-300 h-6 w-28"}/>
                            </div> : <div/>
                    }
                    <Space>
                        <CustomButton
                            onClick={showDrawer}
                            responsive={true}
                            icon={<FilterOutlined/>}
                        >
                            {t("button.filter")}
                        </CustomButton>
                        {/*<Dropdown*/}
                        {/*    overlay={*/}
                        {/*        <FilterWidget*/}
                        {/*            onClick={onFilter}*/}
                        {/*            current={{*/}
                        {/*                dateStart: queryParams.filter?.date_start,*/}
                        {/*                dateEnd: queryParams.filter?.date_end,*/}
                        {/*                timeStart: queryParams.filter?.time_start,*/}
                        {/*                timeEnd: queryParams.filter?.time_end,*/}
                        {/*            }}*/}
                        {/*            display={{*/}
                        {/*                timeRange: true,*/}
                        {/*                rangeDate: true,*/}
                        {/*                fastFilter: true,*/}
                        {/*                order: false,*/}
                        {/*                active: false,*/}
                        {/*            }}*/}
                        {/*        />*/}
                        {/*    }*/}
                        {/*    trigger={['click']}*/}
                        {/*    placement="bottomRight"*/}
                        {/*    arrow*/}
                        {/*>*/}
                        {/*   */}
                        {/*</Dropdown>*/}
                    </Space>
                </div>
            </Card>
            <CommonTagFilterFC
                className={"pt-4"}
                is={isTags}
                onClose={handleCloseTag}
                data={queryParams}
                items={[
                    {
                        key: 'filter.date_start',
                        label: t('text.dateStart'),
                    },
                    {
                        key: 'filter.date_end',
                        label: t('text.dateEnd'),
                    },
                    {
                        key: 'filter.time_start',
                        label: t('text.timeStart'),
                    },
                    {
                        key: 'filter.time_end',
                        label: t('text.timeEnd'),
                    },
                ]}
            />
            <ErrorItemFC
                status={vm.isLoading}
            >
                {
                    vm.isLoading === SendingStatus.loading
                        ? <CommonLoadingSpinFC/>
                        : vm.item instanceof TimelapseSensorModel
                            ? <TimelapseViewAnalyticsFC item={vm.item} selected={selected}/>
                            : <CommonEmptyFC/>
                }
            </ErrorItemFC>
            <FilterDrawerWidget
                visible={visible}
                onClose={onClose}
                onClick={onFilter}
                current={{
                    dateStart: queryParams.filter?.date_start,
                    dateEnd: queryParams.filter?.date_end,
                    timeStart: queryParams.filter?.time_start,
                    timeEnd: queryParams.filter?.time_end,
                }}
                display={{
                    timeRange: true,
                    rangeDate: true,
                    fastFilter: true,
                    order: false,
                    active: false,
                }}
            />
        </div>
    )
}
