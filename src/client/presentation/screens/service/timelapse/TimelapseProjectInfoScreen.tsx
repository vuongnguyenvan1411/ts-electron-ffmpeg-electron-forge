import {useTranslation} from "react-i18next";
import {useNavigate} from "react-router";
import {useEffect} from "react";
import {Color} from "../../../../const/Color";
import {Divider, Space, Typography} from "antd";
import {ErrorItemFC} from "../../../widgets/ErrorItemFC";
import {SendingStatus} from "../../../../const/Events";
import {useParams} from "react-router-dom";
import {CHashids} from "../../../../core/CHashids";
import {RouteAction} from "../../../../const/RouteAction";
import {TimelapseProjectInfoAction} from "../../../../recoil/service/timelapse/timelapse_project_info/TimelapseProjectInfoAction";
import {WrapContentWidget} from "../../../widgets/WrapContentWidget";

type TParamState = {
    name: string;
    pathname: string;
    createdAt: number;
}

export const TimelapseProjectInfoScreen = (_: {
    state?: TParamState
}) => {
    const {t} = useTranslation()
    const navigate = useNavigate()
    const {hash} = useParams<{ hash: string }>()
    const projectId = CHashids.decodeGetFirst(hash!)

    const {
        vm,
        onLoadItem,
        resetStateWithEffect,
    } = TimelapseProjectInfoAction()

    const title = t('text.info');

    useEffect(() => {
        console.log('%cMount Screen: TimelapseProjectInfoScreen', Color.ConsoleInfo);

        if (projectId && vm.key !== projectId.toString()) {
            if (vm.item) resetStateWithEffect();
            onLoadItem(projectId);
        }
        // if (!vm.item) {
        //     if (typeof projectId === "number") {
        //
        //     } else {
        //         navigate(RouteAction.GoBack())
        //     }
        // }

        return () => {
            console.log('%cUnmount Screen: TimelapseProjectInfoScreen', Color.ConsoleInfo);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const _onClickReload = () => {
        if (typeof projectId === "number") {
            onLoadItem(projectId);
        } else {
            navigate(RouteAction.GoBack())
        }
    }

    return (
        <WrapContentWidget
            masterHeader={{
                title: title,
                isLoading: vm.isLoading,
                onReload: _onClickReload,
            }}
            bodyHeader={{
                title: title,
            }}
        >
            <ErrorItemFC
                status={vm.isLoading}
                onRefresh={() => typeof projectId === "number" ? onLoadItem(projectId) : null}
            >
                {
                    vm.isLoading === SendingStatus.loading
                        ? <div>
                            <div className="h-7 w-48 overflow-hidden relative bg-gray-200 skeleton-box"/>
                            <Divider/>
                            <div className="h-8 w-32 overflow-hidden relative bg-gray-200 skeleton-box"/>
                        </div>
                        :
                        vm.isLoading === SendingStatus.success
                            ? <div className={"flex flex-col w-full"}>
                                <div className={"flex w-full"}>
                                    <Space>
                                        <Typography.Text strong>
                                            {t("text.projectName") + ": "}
                                        </Typography.Text>
                                        <Typography.Text>
                                            {vm.item?.name}
                                        </Typography.Text>
                                    </Space>
                                </div>
                                {
                                    vm.item && vm.item.attribute !== undefined
                                        ? vm.item.attribute.map((value, index) => (
                                            <div
                                                key={index}
                                                className={"flex w-full"}
                                            >
                                                <Space>
                                                    <Typography.Text strong>
                                                        {value.name + ": "}
                                                    </Typography.Text>
                                                    <Typography.Text>
                                                        {value.description}
                                                    </Typography.Text>
                                                </Space>
                                            </div>
                                        ))
                                        : null
                                }
                            </div>
                            : null
                }
            </ErrorItemFC>
        </WrapContentWidget>
    )
}
