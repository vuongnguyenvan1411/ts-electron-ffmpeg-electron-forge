import {useParams} from "react-router-dom";
import {CHashids} from "../../../../core/CHashids";
import {DatePicker, Modal, Radio, TimePicker,} from "antd";
import moment, {Moment} from "moment";
import React, {useRef, useState} from "react";
import {TimelapseImageFilterVO, TimelapseImageModel,} from "../../../../models/service/timelapse/TimelapseImageModel";
import {TimelapseViewImageFC, TimelapseViewImageLoadingFC} from "../../../widgets/TimelapseViewImageFC";
import {useTranslation} from "react-i18next";
import {Style} from "../../../../const/Style";
import {E_ResUrlType, SendingStatus} from "../../../../const/Events";
import useIntersectionObserver, {obFLoadMore} from "../../../hooks/useIntersectionOnserver";
import {CustomImage} from "../../../components/CustomImage";
import {ReactCompareSlider, ReactCompareSliderHandle} from "react-compare-slider";
import {ReactZoomPanPinchRef, TransformComponent, TransformWrapper} from "react-zoom-pan-pinch";
import {handleWheelZoom2} from "../../../../libs/zoom-pan-pich/core/wheel/wheel.logic";
import {handleAlignToScaleBoundsCustom} from "../../../../libs/zoom-pan-pich/core/zoom/zoom.logic";
import {handlePanningEnd, handlePanningStart} from "../../../../libs/zoom-pan-pich/core/pan/panning.logic";
import {handlePinchStart, handlePinchStop, handlePinchZoomCustom} from "../../../../libs/zoom-pan-pich/core/pinch/pinch.logic";
import {TKeyPushItem} from "../../../../recoil/service/timelapse/timelapse_image_compare_tool/TimelapseImageCompareToolState";
import {TimelapseImageCompareToolAction} from "../../../../recoil/service/timelapse/timelapse_image_compare_tool/TimelapseImageCompareToolAction";
import {CustomButton} from "../../../components/CustomButton";
import {CustomTypography} from "../../../components/CustomTypography";
import styles from "../../../../styles/module/TimelapseImageCompareTool.module.scss";
import NextImage from "next/image";
import {Images} from "../../../../const/Images";
import {BaseFooter} from "../../../layouts/BaseFooter";
import {MediaQuery} from "../../../../core/MediaQuery";
import {TimelapseDownloadImageFC} from "../../../widgets/TimelapseDownloadImageFC";

type TQueryFilter = Record<TKeyPushItem, TimelapseImageFilterVO>;
type TQueryDrag = {
    key?: TKeyPushItem,
    index: number,
    ref: Record<TKeyPushItem, EventTarget & HTMLDivElement | undefined>,
}
type TImgPreview = Record<TKeyPushItem, TimelapseImageModel | undefined>;
type TonDragStart = (event: React.DragEvent<HTMLDivElement>) => void;
type TonDragEnd = (event: React.DragEvent<HTMLDivElement>) => void;
type TQualityImage = 'hd' | 'orig';
type TModal = {
    visible?: boolean,
    type?: "left" | "right",
}


export const TimelapseImageCompareToolScreen = () => {
    const {t} = useTranslation()
    const {hash} = useParams<{ hash: string }>()
    const hashDecode = CHashids.decode(hash!)
    const [, machineId] = hashDecode
    const [previewUrl, setPreviewUrl] = useState<TImgPreview>({
        left: undefined,
        right: undefined,
    })
    const [typeCompare, setTypeCompare] = useState<number>(1)
    const [typeImage, setTypeImage] = useState<TQualityImage>("hd")
    const [leftRef, setLeftRef] = useState<ReactZoomPanPinchRef | null>(null)
    const [rightRef, setRightRef] = useState<ReactZoomPanPinchRef | null>(null)
    const [qDrag, setQDrag] = useState<TQueryDrag>(
        {
            index: 0,
            ref: {
                left: undefined,
                right: undefined,
            }
        }
    )
    const refDiv = useRef<Record<TKeyPushItem, EventTarget & HTMLDivElement | undefined>>(
        {
            left: undefined,
            right: undefined,
        }
    )
    const [visible, setVisible] = useState<TModal>({});

    const onChangeModal = (visible: boolean, type?: "left" | "right") => {
        setVisible({
            visible: visible,
            type: type,
        })
    }

    const {
        vm,
        onLoadImages
    } = TimelapseImageCompareToolAction()

    const [queryFilter, setQueryFiler] = useState<TQueryFilter>(
        {
            left: {
                limit: vm.left.query.limit,
                page: vm.left.query.page,
            },
            right: {
                limit: vm.right.query.limit,
                page: vm.right.query.page,
            },
        }
    )

    const onChangeTypeCompare = (e: any) => {
        setTypeCompare(e.target.value)
    }

    const onChangeTypeImage = (e: any) => {
        setTypeImage(e.target.value)
    }

    const onChange = (date: Moment | null, _: string, keyPush: TKeyPushItem) => {
        const qFilter: TQueryFilter = queryFilter;
        qFilter[keyPush].filter = {
            ...qFilter[keyPush].filter,
            date_start: date?.format('DD-MM-YYYY').toString(),
            date_end: date?.format('DD-MM-YYYY').toString(),
        }

        setQueryFiler(
            qFilter
        )
    }

    const onFilter = (keyPush: TKeyPushItem) => {
        queryFilter[keyPush] = {
            ...queryFilter[keyPush],
            page: 1,
        }

        onLoadImages(machineId, keyPush, queryFilter[keyPush]);
    }

    const formatTime = "HH:mm";
    const pickerTimeRanges: any = {};
    pickerTimeRanges[t('text.morning')] = [moment("08:00", formatTime), moment("16:00", formatTime)];
    pickerTimeRanges[t('text.night')] = [moment("19:00", formatTime), moment("23:59", formatTime)];

    const onLoadMore = (entry: IntersectionObserverEntry, keyPush: TKeyPushItem) => {
        if (entry.isIntersecting && vm[keyPush].isNextAvailable) {
            queryFilter[keyPush] = {
                ...queryFilter[keyPush],
                page: (queryFilter[keyPush].page ?? 0) + 1,
            }

            onLoadImages(machineId, keyPush, queryFilter[keyPush]);
        }
    }

    const onChangeTimePicker = (times: any, _: string[], keyPush: TKeyPushItem) => {
        const cpFilter: TQueryFilter = queryFilter;

        if (times !== null && times[0] !== null && times[1] !== null) {
            cpFilter[keyPush].filter = {
                ...cpFilter[keyPush].filter,
                time_start: (times[0] as Moment).format(formatTime),
                time_end: (times[1] as Moment).format(formatTime),
            }
        } else {
            cpFilter[keyPush].filter = {
                ...cpFilter[keyPush].filter,
                time_start: undefined,
                time_end: undefined,
            }
        }

        setQueryFiler(cpFilter)
    }

    const BuildInputFilter = (key: TKeyPushItem) => {
        return (
            <div className={styles.InputFilter}>
                <div className={styles.InputsZone}>
                    <DatePicker
                        dropdownClassName={"date-picker-dropdown-main"}
                        suffixIcon={
                            <NextImage
                                width={16}
                                height={16}
                                src={Images.iconCalendarRegular.data}
                                alt={Images.iconCalendarRegular.atl}
                            />
                        }
                        className={"w-full date-picker-main date-picker-radius-8 date-picker-h-40 date-picker-bg-dark4 date-picker-border-0 date-picker-p-12-16"}
                        placeholder={t("text.selectDate")}
                        onChange={
                            (date: Moment | null, dateString: string,) => onChange(date, dateString, key)
                        }
                        format={t("format.date")}
                        showToday={false}
                        defaultValue={queryFilter[key].filter?.date_start ? moment(queryFilter[key].filter?.date_start, "DD-MM-YYYY") : undefined}
                    />
                    <TimePicker.RangePicker
                        className={"ranger-time-picker-main ranger-time-picker-border-0 ranger-time-picker-radius-8 ranger-time-picker-p-10-16 ranger-time-picker-bg-dark4"}
                        suffixIcon={
                            <NextImage
                                width={16}
                                height={16}
                                src={Images.iconClockSolid.data}
                                alt={Images.iconClockSolid.atl}
                            />
                        }
                        placeholder={
                            [t("text.timeStart"), t("text.timeEnd")]
                        }
                        allowClear={true}
                        allowEmpty={[true, true]}
                        ranges={pickerTimeRanges}
                        format={formatTime}
                        onChange={(values, formatString) => onChangeTimePicker(values, formatString, key)}
                        onCalendarChange={(values, formatString, info) => console.log(values, formatString, info)}
                        defaultValue={[
                            queryFilter[key].filter?.time_start ? moment(queryFilter[key].filter?.time_start, formatTime) : null,
                            queryFilter[key].filter?.time_end ? moment(queryFilter[key].filter?.time_end, formatTime) : null,
                        ]}
                    />
                </div>
                <CustomButton
                    size={"small"}
                    onClick={() => onFilter(key)}
                    loading={vm[key].isLoading === SendingStatus.loading}
                >
                    {t("button.filter")}
                </CustomButton>
            </div>
        )
    }

    const onDrop = (keyPush: TKeyPushItem) => {
        if (qDrag.key && qDrag.key === keyPush) {
            setPreviewUrl({
                ...previewUrl,
                [keyPush]: vm[keyPush].items[qDrag.index]
            });
        }

        if (qDrag.ref) {
            // refDiv.current?.[keyPush]?.classList.remove("border-solid", "border", keyPush === "left" ? "border-green-500" : "border-red-500");
            refDiv.current = {
                ...refDiv.current,
                [keyPush]: qDrag.ref[keyPush],
            }
            // refDiv.current?.[keyPush]?.classList?.add("border-solid", "border", keyPush === "left" ? "border-green-500" : "border-red-500");
        }
    }

    const onClick = (keyPush: TKeyPushItem, item: TimelapseImageModel, __: number) => {
        setPreviewUrl({
            ...previewUrl,
            [keyPush]: item,
        });

        setVisible({
            ...visible,
            visible: false,
        })
    }

    const _buildList = (type: "left" | "right") => {
        return (
            <div className={type === "left" ? styles.ListLeft : styles.ListRight}>
                {
                    BuildInputFilter(type)
                }
                <div className={"h-full w-full"}>
                    <div
                        className={"pl-4 xl:pr-4"}
                        style={{
                            overflowX: "hidden",
                            overflowY: "auto",
                            maxHeight: "100%",
                            height: "100%",
                            minHeight: "100%",
                        }}
                    >
                        <BuildList
                            onClick={(item, index) => onClick(type, item, index)}
                            keyPush={type}
                            items={vm[type].items}
                            isNextAvailable={vm[type].isNextAvailable}
                            onLoadMore={onLoadMore}
                            onDragStart={event => {
                                setQDrag(
                                    {
                                        key: event.currentTarget.dataset.drag_type as TKeyPushItem,
                                        index: parseInt(event.currentTarget.dataset.drag_index ?? "0"),
                                        ref: {
                                            ...qDrag.ref,
                                            [type]: event.currentTarget,
                                        },
                                    }
                                );
                            }}
                            onDragEnd={event => {
                                event.preventDefault();
                            }}
                            item={previewUrl[type]}
                        />
                    </div>
                </div>
            </div>
        )
    }

    return (
        <>
            <div className={styles.Main}>
                <div className={styles.FixList}>
                    {_buildList("left")}
                </div>

                <div className={styles.CompareZone}>
                    <div className={styles.ContentZone}>
                        <div className={styles.ToolsZone}>
                            <div className={styles.ToolContainer}>
                                <CustomTypography
                                    className={styles.ToolTitle}
                                    textStyle={"text-body-text-1"}
                                    isStrong
                                >
                                    {t('text.modeComparison')}
                                </CustomTypography>
                                <div className={styles.ToolChildren}>
                                    <Radio.Group onChange={onChangeTypeCompare} value={typeCompare}>
                                        <div className={styles.FixChildren}>
                                            <Radio className={"radio-main"} value={1}>{t('text.basic')}</Radio>
                                            <Radio className={"radio-main"} value={2}>Slider</Radio>
                                        </div>
                                    </Radio.Group>
                                </div>
                            </div>
                            <div className={styles.ToolContainer}>
                                <CustomTypography
                                    className={styles.ToolTitle}
                                    textStyle={"text-body-text-1"}
                                    isStrong
                                >
                                    {t('text.imageQuality')}
                                </CustomTypography>
                                <div className={styles.ToolChildren}>
                                    <Radio.Group onChange={onChangeTypeImage} value={typeImage}>
                                        <div className={styles.FixChildren}>
                                            <Radio className={"radio-main"} value={"hd"}>HD</Radio>
                                            <Radio className={"radio-main"} value={"orig"}>{t('text.imageOrig')}</Radio>
                                        </div>
                                    </Radio.Group>
                                </div>
                            </div>
                            <div className={styles.ToolContainer}>
                                <CustomTypography
                                    className={styles.ToolTitle}
                                    textStyle={"text-body-text-1"}
                                    isStrong
                                >
                                    {t('text.zoomImage')}
                                </CustomTypography>
                                <div className={styles.ToolChildren}>
                                    <div className={styles.FixChildren}>
                                        <div
                                            onClick={() => {
                                                if (rightRef) {
                                                    rightRef.zoomIn()
                                                }

                                                if (leftRef) {
                                                    leftRef.zoomIn()
                                                }
                                            }}
                                            className={"flex flex-row items-center gap-2 cursor-pointer"}
                                        >
                                            <NextImage
                                                className={"atl-icon-color-default"}
                                                width={20}
                                                height={20}
                                                src={Images.iconZoomInW.data}
                                                alt={Images.iconZoomInW.atl}
                                            />
                                            <CustomTypography textStyle={"text-14-20"}>
                                                {t('text.zoomIn')}
                                            </CustomTypography>
                                        </div>
                                        <div
                                            onClick={() => {
                                                if (rightRef) {
                                                    rightRef.zoomOut()
                                                }

                                                if (leftRef) {
                                                    leftRef.zoomOut()
                                                }
                                            }}
                                            className={"flex flex-row items-center gap-2 cursor-pointer"}
                                        >
                                            <NextImage
                                                className={"atl-icon-color-default"}
                                                width={20}
                                                height={20}
                                                src={Images.iconZoomOutW.data}
                                                alt={Images.iconZoomOutW.atl}
                                            />
                                            <CustomTypography textStyle={"text-14-20"}>
                                                {t('text.zoomOut')}
                                            </CustomTypography>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {
                            typeCompare === 1 ?
                                <div className={styles.CompareContainer}>
                                    <div className={styles.CompareFixContainer}>
                                        <div className={"flex-none flex justify-between xl:hidden"}>
                                            <_BuildButtons
                                                onFilter={() => onChangeModal(true, "left")}
                                                item={previewUrl.left}
                                                labelFilter={"Chon anh 1"}
                                                labelDownload="Download"
                                                align="left"
                                                isRow={true}
                                            />
                                            <_BuildButtons
                                                onFilter={() => onChangeModal(true, "right")}
                                                item={previewUrl.right}
                                                labelFilter={"Chon anh 1"}
                                                labelDownload="Download"
                                                align="right"
                                                isRow={true}
                                            />
                                        </div>
                                        <div className={"flex flex-col flex-auto xxl:flex-wrap"}>
                                            <BuildDragZone
                                                refZoom={rightRef}
                                                refMain={leftRef}
                                                onInit={ref => {
                                                    setLeftRef(ref);
                                                }}
                                                imageQuality={typeImage}
                                                item={previewUrl.left}
                                                className={"drag-zone "}
                                                onDrop={() => onDrop('left')}
                                                keyBuild={"left"}
                                                onFilter={() => onChangeModal(true, "left")}
                                            />
                                            <BuildDragZone
                                                refZoom={leftRef}
                                                refMain={rightRef}
                                                onInit={ref => {
                                                    setRightRef(ref);
                                                }}
                                                imageQuality={typeImage}
                                                item={previewUrl.right}
                                                className={"drag-zone"}
                                                onDrop={() => onDrop('right')}
                                                keyBuild={"right"}
                                                onFilter={() => onChangeModal(true, "right")}
                                            />
                                        </div>
                                    </div>
                                </div> : null
                        }
                        {
                            typeCompare === 2 && <div className={styles.SliderContainer}>
                                <div className={styles.SliderBoxAspect}>
                                    <div className={"absolute inset-0"}>
                                        <ReactCompareSlider
                                            style={{
                                                width: "100%",
                                                height: "100%",
                                                transform: 'scale(1)',
                                            }}
                                            handle={<ReactCompareSliderHandle style={{color: '#009B90', width: "2px"}}/>}
                                            boundsPadding={0}
                                            itemOne={
                                                <BuildDragZoneSlider
                                                    keyBuild={"left"}
                                                    refZoom={rightRef}
                                                    refMain={leftRef}
                                                    onInit={ref => {
                                                        setLeftRef(ref);
                                                    }}
                                                    imageQuality={typeImage}
                                                    item={previewUrl.left}
                                                    className={"border-dark50"}
                                                    onDrop={() => onDrop('left')}
                                                />
                                            }
                                            itemTwo={
                                                <BuildDragZoneSlider
                                                    keyBuild={"right"}
                                                    refZoom={leftRef}
                                                    refMain={rightRef}
                                                    onInit={ref => {
                                                        setRightRef(ref);
                                                    }}
                                                    imageQuality={typeImage}
                                                    item={previewUrl.right}
                                                    className={"border-dark50"}
                                                    onDrop={() => onDrop('right')}
                                                />
                                            }
                                            position={50}
                                            onlyHandleDraggable={true}
                                        />
                                    </div>
                                </div>
                                <div className={"flex flex-row justify-between xl:hidden"}>
                                    <_BuildButtons
                                        onFilter={() => onChangeModal(true, "left")}
                                        item={previewUrl.left}
                                        labelFilter={"Chon anh 1"}
                                        labelDownload="Download"
                                        align="left"
                                    />
                                    <_BuildButtons
                                        onFilter={() => onChangeModal(true, "right")}
                                        item={previewUrl.right}
                                        labelFilter={"Chon anh 1"}
                                        labelDownload="Download"
                                        align="right"
                                    />
                                </div>
                            </div>
                        }
                    </div>
                    <div className={"p-4 text-center"}>
                        <BaseFooter
                            style={{
                                color: "#969696",
                            }}
                        />
                    </div>
                </div>
                <div className={styles.FixList}>
                    {_buildList("right")}
                </div>
            </div>
            {
                visible.visible && <Modal
                    title={t("title.filter")}
                    wrapClassName={"modal-main modal-header-center modal-header-bg-white modal-full-height modal-body-p-0 modal-body-bg modal-header-shadow"}
                    destroyOnClose={true}
                    visible={visible.visible ?? false}
                    onCancel={() => onChangeModal(false)}
                >
                    {
                        _buildList(visible.type ?? "left")
                    }
                </Modal>
            }
        </>
    );
}

const BuildList = React.memo((
    props: {
        items: TimelapseImageModel[],
        isNextAvailable: boolean,
        onLoadMore: obFLoadMore,
        onDragStart: TonDragStart,
        onDragEnd: TonDragEnd,
        keyPush: TKeyPushItem,
        item?: TimelapseImageModel,
        onClick: (item: TimelapseImageModel, index: number) => void,
    }
) => {
    const {t} = useTranslation()

    return (
        <div
            className={"tl-sc-ct"}
            style={{
                overflowX: "hidden",
                overflowY: "auto",
                maxHeight: "100%",
                height: "100%",
                minHeight: "100%",
            }}
        >
            {
                props.items.length > 0 ?
                    <div className={"grid grid-cols-2 lg:grid-cols-3 xl:grid-cols-1 gap-y-4"}>
                        {
                            props.items.map((item, index) => {
                                if (props.isNextAvailable && props.items.length === (index + 1)) {
                                    return (
                                        <BuildLoadMore key={index.toString()} keyPush={props.keyPush} onLoadMore={props.onLoadMore}/>
                                    )
                                }
                                return (
                                    <div onClick={() => props.onClick(item, index)} key={index.toString()}>
                                        <div className={"flex flex-row"}>
                                            <div
                                                className={"grow bg-white rounded-b"}
                                                data-drag_type={props.keyPush}
                                                data-drag_index={index}
                                                onDragStart={props.onDragStart}
                                                onDragEnd={props.onDragEnd}
                                            >
                                                <TimelapseViewImageFC
                                                    key={index}
                                                    item={item}
                                                    isDivider={props.item?.order === item.order}
                                                    isDownload={true}
                                                >
                                                    <div
                                                        className={"w-full"}
                                                        style={{
                                                            padding: "7px 21px",
                                                        }}
                                                    >
                                                        <div className={"custom-caption flex flex-col items-start"}>
                                                            <CustomTypography
                                                                className={"caption-info-left"}
                                                                isStrong={true}
                                                                style={{
                                                                    color: "#1a8983",
                                                                }}
                                                            >
                                                                #{item.order}
                                                            </CustomTypography>
                                                            <div className={"flex w-full justify-between"}>
                                                                <CustomTypography
                                                                    className={"caption-info-right"}
                                                                    textStyle={"text-body-text-3"}
                                                                >
                                                                    {item.dateShotFormatted(t('format.timeShort'))}
                                                                </CustomTypography>
                                                                <CustomTypography
                                                                    className={"caption-info-right"}
                                                                    textStyle={"text-body-text-3"}
                                                                >
                                                                    {item.dateShotFormatted(t('format.date'))}
                                                                </CustomTypography>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </TimelapseViewImageFC>
                                            </div>
                                            <div className={"w-4"}/>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                    : <div className={"flex flex-col gap-3 items-center pt-10"}>
                        <div
                            style={{
                                width: 100,
                                height: 100,
                                padding: "1rem",
                                borderRadius: "0.5rem",
                                background: "#F4F4F4",
                            }}
                        >
                            <NextImage src={Images.iconRing.data} alt={Images.iconRing.atl}/>
                        </div>
                        <CustomTypography textStyle={"text-body-text-2"}>
                            {t('text.noData')}
                        </CustomTypography>
                    </div>
            }
        </div>
    )
})

const BuildDragZone = (props: {
    item?: TimelapseImageModel
    className: string,
    imageQuality: TQualityImage,
    onDrop: Function,
    onInit: (ref: ReactZoomPanPinchRef) => void,
    refMain: ReactZoomPanPinchRef | null,
    refZoom: ReactZoomPanPinchRef | null,
    keyBuild: TKeyPushItem,
    onFilter: VoidFunction,
}) => {
    const {t} = useTranslation()
    const refWrap = useRef<any>(null)

    return (
        <div className={`${styles.DragZone}`}>
            <div className={`absolute inset-0 ${props.className}`}>
                <div
                    className={`flex-grow w-full h-full`}
                    onDragEnter={event => {
                        event.preventDefault();
                        event.stopPropagation();
                    }}
                    onDragLeave={event => {
                        event.preventDefault();
                        event.stopPropagation();
                    }}
                    onDragOver={event => event.preventDefault()}
                    onDrop={event => {
                        event.preventDefault();
                        event.stopPropagation();
                        props.onDrop();
                    }}
                >
                    {
                        props.item ? <TransformWrapper
                                onInit={props.onInit}
                                ref={refWrap}
                                initialScale={1}
                                maxScale={20}
                                doubleClick={{
                                    disabled: true,
                                }}
                                onPinchingStart={(ref, event) => {
                                    // console.log('onPinchingStart: ', event);
                                    if (props.refZoom) {
                                        handlePinchStart(props.refZoom.instance, event);
                                    }
                                }}
                                onPinching={(_, event) => {
                                    // console.log('onPinching: ', event);
                                    if (props.refZoom) {
                                        handlePinchZoomCustom(_.instance, props.refZoom.instance, event);
                                    }
                                }}
                                onPinchingStop={() => {
                                    // console.log('onPinchingStop: ',);
                                    if (props.refZoom) {
                                        handlePinchStop(props.refZoom.instance);
                                    }
                                }}
                                onWheel={(_, event) => {
                                    if (props.refZoom) {
                                        handleWheelZoom2(_.instance, props.refZoom.instance, event);
                                    }
                                }}
                                onWheelStop={(_, event) => {
                                    if (props.refZoom) {
                                        handleAlignToScaleBoundsCustom(
                                            _.instance,
                                            props.refZoom.instance,
                                            event.x,
                                            event.y,
                                        );
                                    }
                                }}
                                onPanningStart={(ref, event) => {
                                    if (props.refZoom) {
                                        handlePanningStart(props.refZoom.instance, event);
                                    }
                                }}
                                // onPanning={(_, event) => {
                                //     if (props.refZoom && props.refMain) {
                                //         // console.log('onPanning: ', event);
                                //         // handlePanningCustom(props.refMain.instance, props.refZoom.instance, (event as WheelEvent).clientX, (event as WheelEvent).clientY,);
                                //     }
                                //
                                // }}
                                onPanningStop={(_, __) => {
                                    if (props.refZoom) {
                                        handlePanningEnd(props.refZoom.instance);
                                    }
                                }}
                            >
                                <TransformComponent
                                    contentStyle={{
                                        height: "100%",
                                        width: "100%",
                                    }}
                                    wrapperStyle={{
                                        height: "100%",
                                        width: "100%",
                                    }}
                                >
                                    <CustomImage
                                        src={props.item.getShotImageUrl({
                                            type: props.imageQuality === 'hd' ? E_ResUrlType.Thumb : E_ResUrlType.Orig,
                                            ...(
                                                props.imageQuality === 'hd' && {
                                                    size: 1280
                                                }
                                            )
                                        })}
                                        alt={props.item?.name ?? ''}
                                        objectFit={"contain"}
                                        placeholderByTailWind={"w-full aspect-w-3 aspect-h-2 overflow-hidden bg-gray-400 animate-pulse"}
                                    />
                                </TransformComponent>
                            </TransformWrapper>
                            :
                            <div
                                className={"flex w-full h-full flex-col justify-center items-center gap-2 xl:gap-7 px-5"}>
                                <NextImage
                                    src={Images.iconImageRegular.data}
                                    alt={Images.iconImageRegular.atl}
                                />
                                <CustomTypography style={{textAlign: "center"}}>
                                    {
                                        new MediaQuery().isMinBreakpoint("xl") ?
                                            t("message.dragImage")
                                            : "Nhấn nút để chọn ảnh so sánh"
                                    }
                                </CustomTypography>
                            </div>
                    }
                </div>
                {
                    props.item
                    && <div
                        className={`flex xl:flex-col xl:p-4 items-start bg-dark1 opacity-60 p-1 gap-1  absolute  ${props.keyBuild === "left" ? 'top-0 left-0' : 'top-0 right-0'}`}>
                        <CustomTypography
                            style={{
                                color: "#FFF",
                            }}
                            textStyle={"text-body-text-2"}
                            isStrong
                        >
                            #{props.item.order}
                        </CustomTypography>
                        <CustomTypography
                            textStyle={"text-body-text-2"}
                            style={{
                                color: "#FFF",
                            }}
                        >
                            {props.item.dateShotFormatted(t('format.timeShort'))} {props.item.dateShotFormatted(t('format.date'))}
                        </CustomTypography>
                    </div>
                }
            </div>
        </div>
    )
}

const BuildDragZoneSlider = (props: {
    item?: TimelapseImageModel
    className: string,
    imageQuality: TQualityImage,
    onDrop: Function,
    onInit: (ref: ReactZoomPanPinchRef) => void,
    refMain: ReactZoomPanPinchRef | null,
    refZoom: ReactZoomPanPinchRef | null,
    keyBuild: TKeyPushItem
}) => {
    const {t} = useTranslation();
    const refWrap = useRef<any>(null)

    return (
        <div className={`w-full h-full border-solid border ${props.className} relative`}>
            <div className={`absolute inset-0 ${props.className} bg-gray-300`}>
                <div
                    className={`flex-grow w-full h-full`}
                    onDragEnter={event => {
                        event.preventDefault();
                        event.stopPropagation();
                    }}
                    onDragLeave={event => {
                        event.preventDefault();
                        event.stopPropagation();
                    }}
                    onDragOver={event => event.preventDefault()}
                    onDrop={event => {
                        event.preventDefault();
                        event.stopPropagation();
                        props.onDrop();
                    }}
                >
                    {
                        props.item ? <TransformWrapper
                                onInit={props.onInit}
                                ref={refWrap}
                                initialScale={1}
                                maxScale={20}
                                doubleClick={{
                                    disabled: true,
                                }}
                                onPinchingStart={(ref, event) => {
                                    // console.log('onPinchingStart: ', event);
                                    if (props.refZoom) {
                                        handlePinchStart(props.refZoom.instance, event);
                                    }
                                }}
                                onPinching={(_, event) => {
                                    // console.log('onPinching: ', event);
                                    if (props.refZoom) {
                                        handlePinchZoomCustom(_.instance, props.refZoom.instance, event);
                                    }
                                }}
                                onPinchingStop={() => {
                                    // console.log('onPinchingStop: ',);
                                    if (props.refZoom) {
                                        handlePinchStop(props.refZoom.instance);
                                    }
                                }}
                                onWheel={(_, event) => {
                                    if (props.refZoom) {
                                        handleWheelZoom2(_.instance, props.refZoom.instance, event);
                                    }
                                }}
                                onWheelStop={(_, event) => {
                                    if (props.refZoom) {
                                        handleAlignToScaleBoundsCustom(
                                            _.instance,
                                            props.refZoom.instance,
                                            event.x,
                                            event.y,
                                        );
                                    }
                                }}
                                onPanningStart={(ref, event) => {
                                    if (props.refZoom) {
                                        // console.log('onPanningStart: ', event);
                                        handlePanningStart(props.refZoom.instance, event);
                                    }
                                }}
                                // onPanning={(_, event) => {
                                //     if (props.refZoom && props.refMain) {
                                //         // console.log('onPanning: ', event);
                                //         // handlePanningCustom(props.refMain.instance, props.refZoom.instance, (event as WheelEvent).clientX, (event as WheelEvent).clientY,);
                                //     }
                                //
                                // }}
                                onPanningStop={(_, __) => {
                                    if (props.refZoom) {
                                        handlePanningEnd(props.refZoom.instance);
                                    }
                                }}
                            >
                                <TransformComponent
                                    contentStyle={{
                                        height: "100%",
                                        width: "100%",
                                    }}
                                    wrapperStyle={{
                                        height: "100%",
                                        width: "100%",
                                    }}
                                >
                                    <CustomImage
                                        src={props.item.getShotImageUrl({
                                            type: props.imageQuality === 'hd' ? E_ResUrlType.Thumb : E_ResUrlType.Orig,
                                            ...(
                                                props.imageQuality === 'hd' && {
                                                    size: 1280
                                                }
                                            )
                                        })}
                                        alt={props.item?.name ?? ''}
                                        placeholderByTailWind={"flex-grow h-full w-full overflow-hidden relative bg-gray-400 animate-pulse"}
                                        objectFit={"contain"}
                                    />
                                </TransformComponent>
                            </TransformWrapper>
                            :
                            <div
                                className={"flex w-full h-full flex-col justify-center items-center gap-2 xl:gap-7 px-5"}>
                                <NextImage
                                    src={Images.iconImageRegular.data}
                                    alt={Images.iconImageRegular.atl}
                                />
                                <CustomTypography style={{textAlign: "center"}}>
                                    {
                                        new MediaQuery().isMinBreakpoint("xl") ?
                                            t("message.dragImage")
                                            : "Nhấn nút để chọn ảnh so sánh"
                                    }
                                </CustomTypography>
                            </div>
                    }
                </div>
                {
                    props.item
                    && <div
                        className={`flex xl:flex-col xl:p-4 items-start bg-dark1 opacity-60 p-1 gap-1  absolute  ${props.keyBuild === "left" ? 'top-0 left-0' : 'top-0 right-0'}`}>
                        <CustomTypography
                            style={{
                                color: "#FFF",
                            }}
                            textStyle={"text-body-text-2"}
                            isStrong
                        >
                            #{props.item.order}
                        </CustomTypography>
                        <CustomTypography
                            textStyle={"text-body-text-2"}
                            style={{
                                color: "#FFF",
                            }}
                        >
                            {props.item.dateShotFormatted(t('format.timeShort'))} {props.item.dateShotFormatted(t('format.date'))}
                        </CustomTypography>
                    </div>
                }
            </div>
        </div>
    )
}

const _BuildButtons = (props: {
    onFilter: VoidFunction;
    item?: TimelapseImageModel;
    labelFilter: string;
    labelDownload: string;
    align: "left" | "right";
    isRow?: boolean,
}) => {
    const {t} = useTranslation();

    return (
        <div
            className={`flex ${props.isRow ? "flex-row" : "flex-col"} gap-2 ${props.align === "right" ? "items-end" : "items-start"}`}>
            <CustomButton
                size={"small"}
                onClick={props.onFilter}
                responsive={false}
                icon={
                    <NextImage
                        src={Images.iconFilter.data}
                        alt={Images.iconFilter.atl}
                    />
                }
            >
                {t("button.filter")}
            </CustomButton>
            <TimelapseDownloadImageFC
                buttonStyle={{
                    width: 24,
                    height: 24,
                    padding: "2.4px",
                    background: "#009B90",
                    opacity: 1,
                }}
                viewBoxIcon={16}
                item={props.item}
            />
        </div>
    );
}

const BuildLoadMore = (props: { onLoadMore: obFLoadMore, keyPush: TKeyPushItem }) => {
    const ref = useRef<HTMLDivElement | null>(null);

    useIntersectionObserver(ref, {}, props.onLoadMore, props.keyPush);

    return (
        <div ref={ref}>
            <TimelapseViewImageLoadingFC
                limit={1}
                isOne={true}
                grid={Style.GridOnlyOne}
            />
        </div>
    )
}
