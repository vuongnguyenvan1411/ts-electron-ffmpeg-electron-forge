import {Tabs} from "antd";
import {useLocation, useNavigate} from "react-router";
import {useParams} from "react-router-dom";
import {RouteConfig} from "../../../../config/RouteConfig";
import {TimelapseViewImageScreen} from "./TimelapseViewImageScreen";
import {useTranslation} from "react-i18next";
import {TimelapseViewVideoAutoScreen} from "./TimelapseViewVideoAutoScreen";
import {TimelapseViewAnalyticScreen} from "./TimelapseViewAnalyticScreen";
import {CHashids} from "../../../../core/CHashids";
import {useCallback, useMemo} from "react";
import {TimelapseViewVideoFilterScreen} from "./TimelapseViewVideoFilterScreen";
import {TTimelapseMachineParamState} from "../../../../const/Types";
import {WrapContentWidget} from "../../../widgets/WrapContentWidget";

export const TimelapseViewTabScreen = (props: {
    state?: TTimelapseMachineParamState
}) => {
    const {t} = useTranslation()
    const navigate = useNavigate()
    const location = useLocation()
    const {tab} = useParams<{ tab?: string }>()
    const {hash} = useParams<{ hash: string }>()
    const params: TTimelapseMachineParamState | undefined = location.state as (TTimelapseMachineParamState | undefined) ?? props.state
    const hashDecode = CHashids.decode(hash!)
    const [, machineId] = hashDecode

    const onChangeTab = useCallback((key) => {
        if (!params) {
            return
        }

        navigate(`${RouteConfig.TIMELAPSE}/view/${hash}/${key}`, {
            state: {
                name: params.name,
                isSensor: params.isSensor,
                machines: params.machines,
                allTime: params.allTime
            }
        })

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [hash])

    const crMachine = useMemo(() => {
        if (!params) {
            return null
        }

        return params.machines.find(value => value.machineId === (machineId))

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [params?.machines, machineId])

    return useMemo(() => {
        return (
            <WrapContentWidget
                masterHeader={{
                    title: crMachine?.name,
                }}
                bodyHeader={{
                    title: crMachine?.name,
                }}
            >
                <Tabs
                    className={"tabs-main tabs-nav-border-none"}
                    size={"small"}
                    activeKey={tab}
                    onChange={key => onChangeTab(key)}
                    tabBarStyle={{
                        marginBottom: "1rem"
                    }}
                >
                    {
                        params && (
                            <>
                                <Tabs.TabPane tab={t('tab.timelapseImage')} key="image">
                                    <TimelapseViewImageScreen
                                        machineId={machineId}
                                        title={params.name}
                                        machines={params.machines}
                                        hash={hash!}
                                    />
                                </Tabs.TabPane>
                                <Tabs.TabPane tab={t('tab.timelapseVideoAuto')} key="video_auto">
                                    <TimelapseViewVideoAutoScreen
                                        machineId={machineId}
                                        title={params.name}
                                        tab={tab ?? ''}
                                        machines={params.machines}
                                    />
                                </Tabs.TabPane>
                                <Tabs.TabPane tab={t('tab.timelapseVideoFilter')} key="video_filter">
                                    <TimelapseViewVideoFilterScreen
                                        machineId={machineId}
                                        title={params.name}
                                        machines={params.machines}
                                        tab={tab ?? ''}
                                        allTime={params.allTime}
                                    />
                                </Tabs.TabPane>
                                <Tabs.TabPane tab={t('tab.timelapseAnalytics')} key="analytics">
                                    <TimelapseViewAnalyticScreen
                                        machineId={machineId}
                                        title={params.name}
                                        machines={params.machines}
                                        tab={tab ?? ''}
                                    />
                                </Tabs.TabPane>
                            </>
                        )
                    }
                </Tabs>
            </WrapContentWidget>
        )
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [hash, tab, t])
}
