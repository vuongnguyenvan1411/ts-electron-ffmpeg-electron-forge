import {useParams} from "react-router-dom";
import {useTranslation} from "react-i18next";
import React, {useCallback, useEffect, useState} from "react";
import {Color} from "../../../../const/Color";
import {CHashids} from "../../../../core/CHashids";
import {Alert, Checkbox, Col, DatePicker, Divider, Form, Input, Modal, notification, Popover, Row, Space, Typography} from "antd";
import {SendingStatus} from "../../../../const/Events";
import {CheckCircleTwoTone, CloseCircleTwoTone, MinusCircleOutlined, PlusOutlined, SettingOutlined} from "@ant-design/icons";
import {Utils} from "../../../../core/Utils";
import {useLocation, useNavigate} from "react-router";
import {App} from "../../../../const/App";
import {RouteAction} from "../../../../const/RouteAction";
import {TSensorModel} from "../../../../models/service/timelapse/TimelapseImageModel";
import moment from "moment";
import {TParamMachine, TTimelapseMachineParamState} from "../../../../const/Types";
import {TimelapseShareModel, TMachineShare, TTimelapseShareV0} from "../../../../models/ShareModel";
import {TimelapseSettingAction} from "../../../../recoil/service/timelapse/timelapse_setting/TimelapseSettingAction";
import {CustomButton} from "../../../components/CustomButton";
import {CustomTypography} from "../../../components/CustomTypography";
import {CustomSwitch} from "../../../components/CustomSwitch";
import NextImage from "next/image";
import {Images} from "../../../../const/Images";
import {ShareScreen} from "../../common/ShareScreen";
import {UrlQuery} from "../../../../core/UrlQuery";
import {MediaQuery} from "../../../../core/MediaQuery";
import {Style} from "../../../../const/Style";
import {WrapContentWidget} from "../../../widgets/WrapContentWidget";

type TModalFormProps = {
    id?: number
    name: string
    visible: boolean
    onCancel: () => void
    initValues?: TMachineShare
    sensor: TSensorModel[]
}

type TModalFormMainProps = {
    projectId: number | null
    sid?: string
    visible: boolean
    preview: boolean
    onCancel: () => void
    sensor: TSensorModel[]
    initFormValues?: TimelapseShareModel
    machines: TParamMachine[]
}

export const TimelapseSettingScreen = (props: {
    state?: TTimelapseMachineParamState
}) => {
    const {t} = useTranslation()
    const navigate = useNavigate()
    const {hash} = useParams<{ hash: string }>()
    const location = useLocation()
    const params = location.state as TTimelapseMachineParamState ?? props.state;
    const URL = new UrlQuery(location.search)

    const {
        vm,
        onLoadItems,
        onDeleteItem,
        resetStateWithEffect,
    } = TimelapseSettingAction();

    const projectId = CHashids.decodeGetFirst(hash!);
    const [visibleForm, setVisibleForm] = useState<{
        sid?: string,
        visible: boolean,
        preview: boolean,
        initValues?: TimelapseShareModel,
    }>({
        visible: false,
        preview: false,
    })

    const onEdit = (item: TimelapseShareModel, _: number) => {
        setVisibleForm({
            ...visibleForm,
            sid: item.sid,
            visible: true,
            preview: false,
            initValues: item,
        })
    }

    const onPreview = (item: TimelapseShareModel, _: number) => {
        setVisibleForm({
            ...visibleForm,
            sid: item.sid,
            visible: true,
            preview: true,
            initValues: item,
        })
    }

    const onDelete = (sid: string) => {
        if (projectId) {
            onDeleteItem(projectId, sid)
        }
    }

    if (projectId === null) {
        navigate(RouteAction.GoBack())
    }

    const page = URL.getInt('page', vm.query.page);
    const limit = URL.getInt('limit', vm.query.limit);

    const [queryParams, setQueryParams] = useState<{ page: number, limit: number }>({
        page: page,
        limit: limit,
    });

    const title = t('text.setting');


    const showFormModal = (sid?: string) => {
        setVisibleForm({
            ...visibleForm,
            sid: sid,
            visible: true,
            preview: false,
            initValues: undefined,
        })
    };

    const hideFormModal = () => {
        setVisibleForm({
            ...visibleForm,
            visible: false,
            preview: false,
        })
    };

    const onInit = () => {
        const urlQueryParams = new UrlQuery(queryParams);
        const mediaQuery = new MediaQuery(Style.GridLimit);
        const mediaLimit = mediaQuery.getPoint(limit);
        urlQueryParams.set('limit', mediaLimit);
        setQueryParams(urlQueryParams.toObject());
        onLoadItems(projectId!, urlQueryParams.toObject()).then();
    }

    useEffect(() => {
        console.log('%cMount Screen: TimelapseSettingScreen', Color.ConsoleInfo);

        if (
            vm.items.length === 0
            || vm.key !== projectId!.toString()
            || (vm.timestamp !== undefined && Utils.checkHourState(vm.timestamp))
        ) {
            if (vm.items.length > 0) resetStateWithEffect();
            onInit();
        }

        return () => {
            console.log('%cUnmount Screen: TimelapseSettingScreen', Color.ConsoleInfo);
        };

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onLoadMore = () => {
        const urlQueryParams = new UrlQuery(queryParams);
        urlQueryParams.set('page', (vm.items.length / vm.query.limit) + 1);
        setQueryParams(urlQueryParams.toObject());
        onLoadItems(projectId!, urlQueryParams.toObject()).then()
    }

    useEffect(() => {
        if (vm.isUpdating === SendingStatus.success) {
            setVisibleForm(
                {
                    ...visibleForm,
                    visible: false,
                }
            )

            notification.success({
                message: t('text.successSetting'),
            });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.isUpdating])

    const isUpdatingError = vm.isUpdating === SendingStatus.error;

    const onClickReload = () => {
        if (projectId) {
            const urlQueryParams = new UrlQuery(queryParams);
            onLoadItems(projectId!, urlQueryParams.toObject()).then()
        }
    }

    return (
        <WrapContentWidget
            masterHeader={{
                title: title,
                isLoading: vm.isLoading,
                onReload: onClickReload,
            }}
            bodyHeader={{
                title: params.name ?? title,
                right: <CustomButton
                    onClick={() => {
                        showFormModal();
                    }}
                >
                    {t("text.addSharingLink")}
                </CustomButton>,
            }}
        >
            {
                isUpdatingError && vm.error && vm.error.hasOwnProperty('warning')
                    ? <Alert className={"mb-5"} message={vm.error['warning']} type="error" showIcon/>
                    : null
            }
            <ShareScreen
                title={params.name ?? ''}
                status={vm.isLoading}
                items={vm.items ?? []}
                isNextAvailable={vm.oMeta?.nextPage !== undefined}
                onDelete={onDelete}
                deleteStatus={vm.isDeleting}
                onEdit={onEdit}
                onView={onPreview}
                onLoadMore={onLoadMore}
            />
            <ModalFormMain
                key={"modal_type_add_edit_view_tl_share"}
                projectId={projectId}
                sid={visibleForm.sid}
                visible={visibleForm.visible}
                onCancel={hideFormModal}
                sensor={vm.sensors ?? []}
                initFormValues={visibleForm.initValues}
                machines={params.machines}
                preview={visibleForm.preview}
            />
        </WrapContentWidget>
    );
}

const ModalFormMain: React.FC<TModalFormMainProps> = ({
                                                          preview,
                                                          projectId,
                                                          sid,
                                                          visible,
                                                          machines,
                                                          sensor,
                                                          onCancel,
                                                          initFormValues
                                                      }) => {
    const {t} = useTranslation();
    const [formMain] = Form.useForm();
    const [dynamicState, setDynamicState] = useState<TMachineShare[]>(initFormValues?.config?.machines ?? []);
    const [isPreview, setIsPreview] = useState(false);
    const [clicked, setClicked] = useState<Record<number, boolean>>({});
    const [hovered, setHovered] = useState<Record<number, boolean>>({});

    useEffect(() => {
        setIsPreview(preview);
    }, [preview])

    const [visibleModal, setVisibleModal] = useState<{
        id: number,
        visible: boolean,
        name: string,
    }>({
        id: 0,
        visible: false,
        name: '',
    })

    const {
        vm,
        onAddItem,
        onEditItem,
    } = TimelapseSettingAction();


    const handleHoverChange = (visible: boolean, machineId: number) => {
        setHovered({
            ...hovered,
            [machineId]: visible,
        })
        setClicked({
            ...clicked,
            [machineId]: false,
        })
    };

    const handleClickChange = (visible: boolean, machineId: number) => {
        setClicked({
            ...clicked,
            [machineId]: visible,
        })
        setHovered({
            ...hovered,
            [machineId]: false,
        })
    };

    const onAfterClose = () => {
        formMain.resetFields();
    }

    const onOk = () => {
        formMain.submit();
    };

    useEffect(() => {
        setDynamicState(initFormValues?.config?.machines ?? []);

    }, [initFormValues?.config?.machines])

    const showUserModal = (id: number, name: string) => {
        setVisibleModal({
            ...visibleModal,
            id: id,
            visible: true,
            name: name,
        })
    };

    const hideUserModal = () => {
        setVisibleModal({
            ...visibleModal,
            visible: false,
        })
    };

    const onChangeToEdit = () => {
        setIsPreview(false);
    }

    const onFinish = (values: any) => {
        let shareMachines: TMachineShare[] = [];

        machines.forEach(value => {
            if (values[`project_${value.machineId}`]) {
                const fMachine = dynamicState.find(_ => _.id === value.machineId);

                shareMachines = [...shareMachines, {
                    id: value.machineId,
                    status: values[`project_${value.machineId}`] ?? false,
                    image: fMachine?.image ?? false,
                    dates: fMachine?.dates ?? [],
                    video: fMachine?.video ?? false,
                    chart: fMachine?.chart ?? false,
                    sensor: fMachine?.sensor ?? [],
                }]
            }
        })

        const data: TTimelapseShareV0 = {
            name: values.name,
            password: values.password,
            dateStart: values['range-time-picker'] && values['range-time-picker'][0] ? moment(values['range-time-picker'][0]).format('YYYY-MM-DD HH:mm:ss').toString() : '',
            dateEnd: values['range-time-picker'] && values['range-time-picker'][1] ? moment(values['range-time-picker'][1]).format('YYYY-MM-DD HH:mm:ss').toString() : '',
            status: values.isShare,
            sInfo: values.shareInfo,
            config: {
                machines: shareMachines,
            },
        }

        if (projectId) {
            if (sid) {
                onEditItem(projectId, sid, data);
            } else {
                onAddItem(projectId, data);
            }
        }
    }

    return (
        <Modal
            className={"modal-main modal-form-share"}
            key={"modal-form-main-setting-timelapse"}
            title={isPreview ? t("text.detailInfo") : !sid ? t("text.createLink") : t("text.editLink")}
            visible={visible}
            onOk={onOk}
            onCancel={onCancel}
            afterClose={onAfterClose}
            destroyOnClose={true}
            confirmLoading={vm.isLoading === SendingStatus.loading}
            width={696}
            footer={null}
        >
            <Form.Provider
                onFormFinish={(name, {values}) => {
                    machines.forEach((value, index) => {
                        if (name === `userForm_${value.machineId}`) {
                            let newDynamicState = [...dynamicState];
                            let imgConfig: {
                                from: string,
                                to: string,
                            }[] = [];

                            if (values.dates && values.dates.length) {
                                values.dates.forEach((_: any) => {
                                    imgConfig.push({
                                        from: _ && _[0] ? moment(_[0]).format('YYYY-MM-DD HH:mm:ss').toString() : '',
                                        to: _ && _[1] ? moment(_[1]).format('YYYY-MM-DD HH:mm:ss').toString() : '',
                                    });
                                })
                            }

                            newDynamicState[index] = {
                                ...newDynamicState[index],
                                id: value.machineId,
                                dates: imgConfig,
                                image: values.shareImage,
                                video: values.shareVideo,
                                chart: values.shareChart,
                                sensor: values.shareSensor,
                            }

                            setDynamicState(
                                newDynamicState
                            )

                            setVisibleModal({
                                ...visibleModal,
                                visible: false,
                            })
                        }
                    })
                }}
            >
                <Form
                    form={formMain}
                    labelCol={{
                        xs: {span: 24},
                        sm: {span: 8},
                        xxl: {span: 6},
                        xl: {span: 6},
                    }}
                    wrapperCol={{
                        xs: {span: 24},
                        sm: {span: 16},
                        xxl: {span: 18},
                        xl: {span: 18},
                    }}
                    onFinish={onFinish}
                >
                    <div className={"flex flex-col gap-2 pb-3 sm:pb-4 md:pb-6 md:gap-3"}>
                        <Form.Item
                            className={"form-item label-typography-body-1 form-item-mb-0"}
                            colon={false}
                            labelAlign={"left"}
                            key={"k_name"}
                            label={t("text.name")}
                            name={"name"}
                            rules={[
                                {
                                    required: true,
                                    message: t("validation.emptyData"),
                                },
                                {
                                    max: 100,
                                    message: t('validation.minAndMaxCharacter', {
                                        label: t("text.name"),
                                        min: '1',
                                        max: '100'
                                    }),
                                }
                            ]}
                            initialValue={initFormValues?.name}
                        >
                            <Input
                                className={"input-main input-h-40 input-radius-8 input-border-0"}
                                disabled={isPreview}
                                placeholder={"Nhập tên"}
                            />
                        </Form.Item>
                        <Form.Item
                            className={"form-item label-typography-body-1 form-item-mb-0"}
                            colon={false}
                            labelAlign={"left"}
                            key={"k_password"}
                            label={t("label.password")}
                            name={"password"}
                            initialValue={initFormValues?.password}
                        >
                            <Input
                                className={"input-main input-h-40 input-radius-8 input-border-0"}
                                disabled={isPreview}
                                placeholder={"Nhập mật khẩu"}
                            />
                        </Form.Item>
                        <Form.Item
                            className={"form-item label-typography-body-1 form-item-mb-0"}
                            colon={false}
                            labelAlign={"left"}
                            key={"k_range-time-picker"}
                            name="range-time-picker"
                            label={t("text.activeTime")}
                            {
                                ...(
                                    initFormValues?.expiryDate?.start || initFormValues?.expiryDate?.end
                                        ? {
                                            initialValue: [
                                                initFormValues?.expiryDate.start ? moment(initFormValues.expiryDate.start, App.FormatISOFromMoment) : null,
                                                initFormValues?.expiryDate.end ? moment(initFormValues.expiryDate.end, App.FormatISOFromMoment) : undefined
                                            ]
                                        }
                                        : null
                                )
                            }
                        >
                            <DatePicker.RangePicker
                                className={"ranger-picker-main ranger-picker-border ranger-picker-radius w-full ranger-picker-border-0"}
                                allowEmpty={[true, true]}
                                disabled={isPreview}
                                dropdownClassName={"ranger-picker-wrapper-main"}
                                showTime={{
                                    format: "HH:mm"
                                }}
                                format={t("format.dateTime")}
                            />
                        </Form.Item>
                    </div>
                    <Divider className={"divider-main divider-horizontal-m-0 divider-border-half-dark50"}/>
                    <div className={"flex items-center gap-3 pt-3 sm:pt-4 md:pt-6"}>
                        <SettingOutlined/>
                        <CustomTypography
                            isStrong
                            textStyle={"text-14-20"}
                        >
                            {t("title.setUpBasicPermissions")}
                        </CustomTypography>
                    </div>
                    <div className={"flex flex-col w-full px-7 pb-3 pt-2 gap-2 sm:gap-3 sm:py-4 md:pb-6"}>
                        <div className={"flex-grow flex leading-5 items-center"}>
                            <div className={"flex-none w-48"}>
                                <CustomTypography
                                    textStyle={"text-14-20"}
                                >
                                    {t('text.sharingAllow')}
                                </CustomTypography>
                            </div>
                            <Form.Item
                                className={"form-item form-item-h-4 form-item-mb-0 label-typography-body-text-3"}
                                colon={false}
                                key={"isShare"}
                                name={"isShare"}
                                valuePropName={"checked"}
                                initialValue={initFormValues?.status ?? false}
                            >
                                <CustomSwitch
                                    disabled={isPreview}
                                    defaultChecked={false}
                                />
                            </Form.Item>
                        </div>
                        <div className={"flex-grow flex leading-5 items-center"}>
                            <div className={"flex-none w-48"}>
                                <CustomTypography textStyle={"text-14-20"}>
                                    {t('text.displayInformation')}
                                </CustomTypography>
                            </div>
                            <Form.Item
                                className={"form-item form-item-h-4 form-item-mb-0 label-typography-body-text-3"}
                                colon={false}
                                key={"shareInfo"}
                                name={"shareInfo"}
                                valuePropName={"checked"}
                                initialValue={initFormValues?.sInfo ?? false}
                            >
                                <CustomSwitch
                                    disabled={isPreview}
                                    defaultChecked={false}
                                />
                            </Form.Item>
                        </div>
                    </div>
                    <Divider className={"divider-main divider-horizontal-m-0 divider-border-half-dark50"}/>
                    <div className={"flex items-center gap-3 pt-3 sm:pt-4 md:pt-6"}>
                        <SettingOutlined/>
                        <CustomTypography
                            isStrong
                            textStyle={"text-14-20"}
                        >
                            {t("title.setUpAdvancedPermissions")}
                        </CustomTypography>
                    </div>
                    <div className={"flex flex-col gap-3 max-h-52 overflow-auto pt-2 pb-3 sm:pb-4 md:pt-5 md:pb-6"}>
                        {
                            machines.map(value => {
                                const sMachine = dynamicState.find(v => v.id === value.machineId);

                                return (
                                    <div key={value.machineId}
                                         className={"flex flex-wrap sm:flex-row sm:items-center sm:justify-between"}>
                                        <div className={"flex-auto w-full sm:w-2/5 sm:flex-none md:w-3/12"}>
                                            <Form.Item
                                                wrapperCol={{
                                                    span: 24,
                                                }}
                                                style={{
                                                    height: "auto"
                                                }}
                                                className={"form-item form-item-mb-0"}
                                                key={`tab_form_machine_${value.machineId}`}
                                                shouldUpdate={(prevValues, curValues) => prevValues[`project_${value.machineId}`] !== curValues[`project_${value.machineId}`]}
                                            >
                                                {
                                                    ({getFieldValue}) => {
                                                        const isActive: boolean = getFieldValue(`project_${value.machineId}`) ?? sMachine?.status;

                                                        return (
                                                            <div
                                                                className={"flex flex-row items-center gap-3"}>
                                                                {
                                                                    isActive
                                                                        ? <CheckCircleTwoTone twoToneColor="#52c41a"/>
                                                                        : <CloseCircleTwoTone twoToneColor={"#ff4d4f"}/>
                                                                }
                                                                <CustomTypography textStyle={"text-14-20"}>
                                                                    {value.name}
                                                                </CustomTypography>
                                                            </div>
                                                        )
                                                    }
                                                }
                                            </Form.Item>
                                        </div>
                                        <div className={"flex items-center gap-2 pl-6 sm:pl-0"}>
                                            <div>
                                                <CustomTypography textStyle={"text-14-20"}>
                                                    {t('text.sharingAllow')}
                                                </CustomTypography>
                                            </div>
                                            <Form.Item
                                                className={"form-item form-item-h-4 form-item-mb-0 label-typography-body-text-3"}
                                                wrapperCol={{
                                                    span: 24
                                                }}
                                                colon={false}
                                                key={`project_${value.machineId}`}
                                                name={`project_${value.machineId}`}
                                                valuePropName={"checked"}
                                                initialValue={sMachine?.status}
                                            >
                                                <CustomSwitch
                                                    disabled={isPreview}
                                                    defaultChecked={false}
                                                />
                                            </Form.Item>
                                        </div>
                                        <div className={"pl-6 sm:pl-0"}>
                                            <Form.Item
                                                className={"form-item form-item-mb-0"}
                                                key={`project_setting_info_${value.machineId}`}
                                                wrapperCol={{
                                                    span: 24
                                                }}
                                                shouldUpdate={(prevValues, curValues) => prevValues[`project_${value.machineId}`] !== curValues[`project_${value.machineId}`]}
                                            >
                                                {({getFieldValue}) => {
                                                    const isActive: boolean = getFieldValue(`project_${value.machineId}`) || false;

                                                    const contentPopOver = <div className={"flex flex-col gap-3"}>
                                                        <CustomTypography
                                                            textStyle={"text-14-20"}
                                                            isStrong
                                                        >
                                                            Dự án:&nbsp;{value.name}
                                                        </CustomTypography>
                                                        <Space
                                                            key={`space_project_setting_${value.machineId}`}
                                                            direction={"vertical"}
                                                        >
                                                            {
                                                                <ul
                                                                    style={{
                                                                        paddingLeft: "16px",
                                                                        listStyle: "none",
                                                                    }}
                                                                >
                                                                    <li
                                                                        key={`li_share_image_${value.machineId}`}
                                                                    >
                                                                        <div className={"flex items-center gap-3"}>
                                                                            {
                                                                                sMachine && sMachine.image
                                                                                    ? <CheckCircleTwoTone
                                                                                        twoToneColor="#52c41a"/>
                                                                                    : <CloseCircleTwoTone
                                                                                        twoToneColor={"#ff4d4f"}/>
                                                                            }
                                                                            <CustomTypography textStyle={"text-14-20"}>
                                                                                {t('text.displayImage')}
                                                                            </CustomTypography>
                                                                        </div>
                                                                    </li>
                                                                    <li
                                                                        key={`li_share_video_${value.machineId}`}
                                                                    >
                                                                        <div className={"flex items-center gap-3"}>
                                                                            {
                                                                                sMachine && sMachine.video
                                                                                    ? <CheckCircleTwoTone twoToneColor="#52c41a"/>
                                                                                    : <CloseCircleTwoTone twoToneColor={"#ff4d4f"}/>
                                                                            }
                                                                            <CustomTypography>
                                                                                {t('text.displayVideo')}
                                                                            </CustomTypography>
                                                                        </div>
                                                                    </li>
                                                                    <li
                                                                        key={`li_share_chart_${value.machineId}`}
                                                                    >
                                                                        <div className={"flex items-center gap-3"}>
                                                                            {
                                                                                sMachine && sMachine.chart
                                                                                    ? <CheckCircleTwoTone twoToneColor="#52c41a"/>
                                                                                    : <CloseCircleTwoTone twoToneColor={"#ff4d4f"}/>
                                                                            }
                                                                            <CustomTypography>
                                                                                {t('text.displayChart')}
                                                                            </CustomTypography>
                                                                        </div>
                                                                    </li>
                                                                    <li
                                                                        key={`li_share_sensor_${value.machineId}`}
                                                                    >
                                                                        <div className={"flex items-center gap-3"}>
                                                                            <CheckCircleTwoTone twoToneColor="#09B377"/>
                                                                            <CustomTypography>
                                                                                {t('text.displaySensor')}
                                                                            </CustomTypography>
                                                                        </div>
                                                                        <ul
                                                                            style={{
                                                                                listStyle: "none",
                                                                            }}
                                                                        >
                                                                            {
                                                                                sensor.map((item, index) =>
                                                                                    <li key={`li_child_sensor_${value.machineId}_${item.id}_${index}`}>
                                                                                        <div className={"flex items-center gap-3"}>
                                                                                            {
                                                                                                sMachine && sMachine.sensor.find(v => v == item.id)
                                                                                                    ? <CheckCircleTwoTone twoToneColor="#09B377"/>
                                                                                                    : <CloseCircleTwoTone twoToneColor={"#ff4d4f"}/>
                                                                                            }
                                                                                            <CustomTypography>
                                                                                                {item.name}
                                                                                            </CustomTypography>
                                                                                        </div>
                                                                                    </li>
                                                                                )
                                                                            }
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            }
                                                        </Space>
                                                    </div>

                                                    return (
                                                        <div className={"flex gap-3 mr-2"}>
                                                            <Popover
                                                                content={contentPopOver}
                                                                trigger="hover"
                                                                visible={hovered[value.machineId]}
                                                                onVisibleChange={(_) => handleHoverChange(_, value.machineId)}
                                                            >
                                                                <Popover
                                                                    content={contentPopOver}
                                                                    trigger="click"
                                                                    visible={clicked[value.machineId]}
                                                                    onVisibleChange={(_) => handleClickChange(_, value.machineId)}
                                                                >
                                                                    <div className={"flex items-center gap-2"}>
                                                                        <CustomButton
                                                                            key={`view_info_advance_${value.machineId}`}
                                                                            icon={
                                                                                <NextImage
                                                                                    src={Images.iconEye.data}
                                                                                    alt={Images.iconEye.atl}
                                                                                />
                                                                            }
                                                                        />
                                                                        <CustomTypography className={"hidden xl:block"} textStyle={"text-14-20"}>
                                                                            {t("button.view")}
                                                                        </CustomTypography>
                                                                    </div>
                                                                </Popover>
                                                            </Popover>
                                                            <div className={"flex items-center gap-2"}>
                                                                <CustomButton
                                                                    size={"normal"}
                                                                    key={`edit_info_advance_${value.machineId}`}
                                                                    htmlType="button"
                                                                    disabled={isPreview || !isActive}
                                                                    onClick={() => showUserModal(value.machineId, value.name)}
                                                                    icon={
                                                                        <NextImage
                                                                            src={Images.iconWrench.data}
                                                                            alt={Images.iconWrench.atl}
                                                                        />
                                                                    }
                                                                />
                                                                <CustomTypography className={"hidden xl:block"} textStyle={"text-14-20"}>
                                                                    {t("button.edit")}
                                                                </CustomTypography>
                                                            </div>

                                                        </div>
                                                    )
                                                }}
                                            </Form.Item>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                    <Divider className={"divider-main divider-horizontal-m-0 divider-border-half-dark50"}/>
                    <div className={"flex gap-4 justify-end pt-3 sm:pt-4"}>
                        {
                            isPreview
                                ? <>
                                    <CustomButton
                                        key={"edit"}
                                        onClick={onChangeToEdit}
                                    >
                                        {t("button.edit")}
                                    </CustomButton>
                                    <CustomButton
                                        type={"outline"}
                                        key={"close"}
                                        onClick={onCancel}
                                    >
                                        {t("button.close")}
                                    </CustomButton>
                                </>
                                : <>
                                    <CustomButton
                                        key={"close"}
                                        type={"outline"}
                                        onClick={onCancel}
                                    >
                                        {t("button.close")}
                                    </CustomButton>
                                    <CustomButton
                                        key={"save"}
                                        onClick={() => onOk()}
                                        loading={vm.isUpdating === SendingStatus.loading}
                                    >
                                        {sid ? t("button.save") : t("button.saveNew")}
                                    </CustomButton>
                                </>
                        }
                    </div>
                </Form>
                <ModalForm
                    visible={visibleModal.visible}
                    onCancel={hideUserModal}
                    id={visibleModal.id}
                    name={visibleModal.name}
                    initValues={dynamicState.find(value => value.id === visibleModal.id)}
                    sensor={sensor}
                />
            </Form.Provider>
        </Modal>
    )
}

const ModalForm: React.FC<TModalFormProps> = ({id, name, visible, sensor, onCancel, initValues}) => {
    const {t} = useTranslation()
    const [form] = Form.useForm()

    const onOk = () => {
        form.submit()
    }

    const options = useCallback(() => {
        return sensor?.map((item, index) => {
                return (
                    <Col span={16} key={index}>
                        <Checkbox
                            key={index}
                            value={item.id}
                            style={{
                                lineHeight: '32px',
                            }}
                        >
                            {item.name}
                        </Checkbox>
                    </Col>
                )
            }
        );
    }, [sensor]);

    const formItemLayout1 = {
        labelCol: {
            xs: {span: 6},
            sm: {span: 6},
            xxl: {span: 10},
            xl: {span: 10}
        },
        wrapperCol: {
            xs: {span: 18},
            sm: {span: 18},
            xxl: {span: 14},
            xl: {span: 14}
        }
    }

    const formItemLayout = {
        labelCol: {
            xs: {span: 6},
            sm: {span: 6},
            xxl: {span: 8},
            xl: {span: 8}
        },
        wrapperCol: {
            xs: {span: 18},
            sm: {span: 18},
            xxl: {span: 16},
            xl: {span: 16}
        }
    }

    return (
        <Modal
            title={name}
            visible={visible}
            onCancel={onCancel}
            className={"modal-main modal-atl-machine-form"}
            afterClose={() => form.resetFields()}
            destroyOnClose={true}
            footer={null}
        >
            <Form
                form={form}
                name={`userForm_${id}`}
                initialValues={{
                    dates: initValues?.dates.map(value => [
                        moment(value.from, App.FormatISOFromMoment),
                        moment(value.to, App.FormatISOFromMoment)
                    ]) ?? [],
                }}
                {...formItemLayout1}
            >
                <div className={"flex flex-col px-2 sm:px-4"}>
                    <div className={"flex justify-between items-center pb-3"}>
                        <CustomTypography textStyle={"text-14-20"}>
                            {t('text.displayImage')}
                        </CustomTypography>
                        <div className={"flex-none w-3/6"}>
                            <Form.Item
                                className={"form-item form-item-mb-0 form-item-h-5"}
                                key={"shareImage"}
                                name={"shareImage"}
                                colon={false}
                                valuePropName={"checked"}
                                initialValue={initValues?.image ?? false}
                                wrapperCol={{
                                    span: 24
                                }}
                            >
                                <CustomSwitch defaultChecked={false}/>
                            </Form.Item>
                        </div>
                    </div>
                    <div className={"pb-3"}>
                        <Typography.Text>{`${t("text.advanced")} (**): ${t("text.timePeriodImage")}`}</Typography.Text>
                        <br/>
                        <Typography.Text type={"secondary"}>{`${t("message.advancedShareImage")}`}</Typography.Text>
                    </div>
                    <Form.List
                        name="dates"
                    >
                        {(fields, {add, remove}, {errors}) => (
                            <>
                                {fields.map((field, _) => (
                                    <Form.Item
                                        labelAlign={'left'}
                                        {...(formItemLayout)}
                                        required={false}
                                        className={"form-item form-item-mb-3"}
                                        key={field.key}
                                    >
                                        <Space>
                                            <Form.Item
                                                {...field}
                                                validateTrigger={['onChange', 'onBlur']}
                                                rules={[
                                                    {
                                                        required: true,
                                                        message: t("validation.emptyData"),
                                                    }
                                                ]}
                                                noStyle
                                            >
                                                <DatePicker.RangePicker
                                                    dropdownClassName={"ranger-picker-wrapper-main"}
                                                    showTime={{
                                                        format: "HH:mm"
                                                    }}
                                                    format={t("format.dateTime")}
                                                />
                                            </Form.Item>
                                            <MinusCircleOutlined
                                                className="dynamic-delete-button"
                                                onClick={() => remove(field.name)}
                                            />
                                        </Space>
                                    </Form.Item>
                                ))}
                                <Form.ErrorList errors={errors}/>
                                <Form.Item
                                    className={"form-item form-item-mb-0"}
                                >
                                    <CustomButton
                                        size={"small"}
                                        onClick={() => add()}
                                        icon={<PlusOutlined/>}
                                    >
                                        {`${t("button.add")} ${t("text.timePeriod")}`}
                                    </CustomButton>
                                </Form.Item>
                            </>
                        )}
                    </Form.List>
                    <div className={"flex justify-between py-2 sm:py-4"}>
                        <CustomTypography textStyle={"text-14-20"}>
                            {t('text.displayVideo')}
                        </CustomTypography>
                        <div className={"flex-none w-3/6"}>
                            <Form.Item
                                key={"shareVideo"}
                                className={"form-item form-item-mb-0 form-item-h-5"}
                                name={"shareVideo"}
                                colon={false}
                                valuePropName={"checked"}
                                initialValue={initValues?.video ?? false}
                            >
                                <CustomSwitch defaultChecked={false}/>
                            </Form.Item>
                        </div>
                    </div>
                    <div className={"flex justify-between pb-3 sm:pb-6"}>
                        <CustomTypography textStyle={"text-14-20"}>
                            {t('text.displayChart')}
                        </CustomTypography>
                        <div className={"flex-none w-3/6"}>
                            <Form.Item
                                key={"shareChart"}
                                className={"form-item form-item-mb-0 form-item-h-5"}
                                name={"shareChart"}
                                colon={false}
                                valuePropName={"checked"}
                                initialValue={initValues?.chart ?? false}
                            >
                                <CustomSwitch defaultChecked={false}/>
                            </Form.Item>
                        </div>
                    </div>
                </div>
                {
                    (sensor && sensor.length > 0) && (
                        <div className={"bg-dark4 p-2 rounded flex flex-col gap-2 sm:p-4 sm:gap-3"}>
                            <CustomTypography
                                isStrong
                                textStyle={"text-14-20"}
                            >
                                {
                                    t('text.displaySensor') + ":"
                                }
                            </CustomTypography>
                            <Form.Item
                                className={"form-item form-item-mb-0 label-typography-body-1"}
                                key={"shareSensor"}
                                labelAlign={'left'}
                                name={"shareSensor"}
                                initialValue={initValues?.sensor ?? []}
                            >
                                <Checkbox.Group>
                                    <Row>
                                        {
                                            options() && options()
                                        }
                                    </Row>
                                </Checkbox.Group>
                            </Form.Item>
                        </div>
                    )
                }
                <div className={"flex gap-4 justify-end pt-3 sm:pt-4 lg:pt-6"}>
                    <CustomButton
                        onClick={onCancel}
                        type={"outline"}
                        key={"close"}
                    >
                        {t("button.close")}
                    </CustomButton>
                    <CustomButton
                        onClick={onOk}
                        key={"oke"}
                    >
                        OK
                    </CustomButton>
                </div>
            </Form>
        </Modal>
    )
}
