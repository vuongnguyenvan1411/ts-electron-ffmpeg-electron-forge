import {useLocation} from "react-router";
import {Link} from "react-router-dom";
import {Button, Result} from "antd";
import {RouteConfig} from "../../../config/RouteConfig";

export function NotFoundScreen() {
    const location = useLocation()

    return (
        <Result
            status="404"
            title="404"
            subTitle={`Sorry, the page "${location.pathname}" you visited does not exist`}
            extra={
                <Button type="primary">
                    <Link to={RouteConfig.HOME}>Back Home</Link>
                </Button>
            }
        />
    )
}
