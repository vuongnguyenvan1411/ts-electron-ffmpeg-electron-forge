import Krpano from "../../modules/vr/components/Krpano";
import Scene from "../../modules/vr/components/Scene";
import View from "../../modules/vr/components/View";
import {useEffect, useRef, useState} from "react";
import {Utils} from "../../../core/Utils";
import {ImageHotspotConfig} from "../../modules/vr/components/Hotspot";
import X2JS from "x2js";
import axios from "axios";
import SkinSetting from "../../modules/vr/components/SkinSetting";

type _TView = {
    _fov?: string,
    _fovmax?: string,
    _fovmin?: string,
    _fovtype?: string,
    _hlookat?: string,
    _vlookat?: string,
    _limitview?: string,
    _maxpixelzoom?: string,
}

type _TCube = {
    cube?: {
        _url?: string
    }
    _tiledimageheight?: string,
    _tiledimagewidth?: string
}

type _TImage = {
    cube?: {
        _url?: string,
        _multires?: string
    },
    _multires?: string,
    _tilesize?: string,
    _type?: string,
    level?: _TCube | _TCube[]
}

type _TScene = {
    _heading?: string,
    _lat?: string,
    _lng?: string,
    _name?: string,
    _onstart?: string,
    _thumburl?: string,
    _title?: string,
    preview?: {
        _url?: string,
    }
    view?: _TView,
    image?: _TImage
}


type _TData = {
    krpano: {
        scene?: _TScene | _TScene[],
        _title: string,
        _version: string
    }
}

type _TDataScene = {
    type?: string,
    url?: string,
    tileSize?: string,
    tiledImageHeight?: string,
    tileSizeWidth?: string,
}

type _Scenes = {
    heading?: string,
    lat?: string,
    lng?: string,
    name?: string,
    thumburl?: string,
    previewUrl?: string,
    images?: _TDataScene[]
}

// const url = 'https://cdn-gdt.autotimelapse.com/vr/382'
// const url = 'https://cdn-gdt.autotimelapse.com/raw/test/vtour-20-1'

export const TestVrScreen = ({url}) => {
    const [data, setData] = useState<_TData>()
    const scenesData = useRef<_Scenes[]>([])
    const [init, setInit] = useState<boolean>(false)
    const timer = useRef<any>()
    const countScene = useRef<number>()
    const nameSceneCurrent = useRef<string>()
    const krpanoI = useRef<any>()
    const test = useRef<ImageHotspotConfig[]>([])

    const [hotspots, setHotspots] = useState<ImageHotspotConfig[]>([
        {
            name: 'hot1',
            ath: 100,
            atv: 80,
            url: 'https://0xllllh.github.io/krpano-examples/images/hotspot.png',
            type: 'image',
            keep: true,
            width: '200',
            height: '200',
            onUp: () => {
                console.log("ath: ", krpanoI.current.get('hotspot[hot1].ath'))
                console.log("atv: ", krpanoI.current.get('hotspot[hot1].atv'))
            },
        },
        {
            name: 'hot2',
            ath: 160,
            atv: 20,
            url: 'https://0xllllh.github.io/krpano-examples/images/hotspot.png',
            type: 'image',
            keep: true,
            width: '64',
            height: '64',
            visible: true,
            onUp: () => {
                console.log("ath: ", krpanoI.current.get('hotspot[hot2].ath'))
                console.log("atv: ", krpanoI.current.get('hotspot[hot2].atv'))
            },
        },
    ])

    const handler = (name: string) => {
        const index = test.current.findIndex(i => i.name === name)
        const item = test.current.find(i => i.name === name)
        const ath = krpanoI.current.get(`hotspot[${name}].ath`)
        const atv = krpanoI.current.get(`hotspot[${name}].atv`)
        const newArr = [...test.current]
        newArr[index] = {
            ...item,
            name,
            ath,
            atv
        }
        console.log('new arr: ', newArr)
        setHotspots(newArr)
        console.log('ath: ', krpanoI.current.get(`hotspot[${name}].ath`))
        console.log('atv: ', krpanoI.current.get(`hotspot[${name}].atv`))
    }

    useEffect(() => {
        test.current = [...hotspots]
    }, [hotspots])

    useEffect(() => {
        const krpano: any = document.getElementById('krpanoSWFObject')
        if (krpano) {

            hotspots.forEach(item => {
                krpano.call("set(hotspot[" + item.name + "].ondown, draghotspot;");
            })

            timer.current = setInterval(() => {
                console.log('time count: ', krpano.get('scene').count)
                countScene.current = krpano.get('scene').count

                if (countScene.current && countScene.current > 0) {
                    console.log('count: ', countScene.current)
                    krpano.call(`loadscene(${nameSceneCurrent.current},null,MERGE|KEEPVIEW|KEEPMOVING|KEEPPLUGINS|KEEPHOTSPOTS|NOPREVIEW)`)
                    krpano.call('skin_startup()');
                    clearInterval(timer.current)
                }
            }, 1000)

            krpanoI.current = krpano

            console.log("timer: ", timer.current)
        }
    }, [init, hotspots])

    useEffect(() => {
        const vS = 1;
        const scriptFile = document.createElement('script');
        scriptFile.src = `${Utils.getLocalAsset('vr/vtour_test/tour.js')}?v=${vS}`;
        document.body.appendChild(scriptFile);

        scriptFile.onload = () => {
            axios.get(`${url}/tour.xml`)
                .then(res => {
                    const x2Js = new X2JS()
                    const dataRes: _TData = x2Js.xml2js(res.data)
                    console.log(dataRes)

                    setData(dataRes)
                })
                .catch(err => console.log(err))
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const getKrpanoView = () => {
        if (data && data.krpano.scene) {
            if (Array.isArray(data.krpano.scene)) {
                if (data.krpano.scene && data.krpano.scene.length > 0) {

                    scenesData.current = data.krpano.scene?.map(item => {
                        const arr: _TDataScene[] | undefined = []

                        if (data.krpano._version.includes('1.20')) {
                            const d: _TDataScene[] | undefined = item.image?.cube?._multires?.split(',').splice(1).map(
                                i => ({
                                    type: 'cube',
                                    url: `${url}/${item.image?.cube?._url}`,
                                    tileSize: item.image?.cube?._multires?.split(',')[0],
                                    tiledImageHeight: i,
                                    tiledImageWidth: i
                                })
                            )

                            if (d) arr.push(...d)
                        } else {
                            if (Array.isArray(item.image?.level)) {
                                const d: _TDataScene[] | undefined = item.image?.level?.map(
                                    i => ({
                                        type: item.image?._type,
                                        url: `${url}/${i.cube?._url}`,
                                        tileSize: item?.image?._tilesize,
                                        tiledImageHeight: i._tiledimageheight,
                                        tiledImageWidth: i._tiledimagewidth
                                    })
                                )

                                if (d) arr.push(...d)
                            } else {
                                const d = {
                                    type: item.image?._type,
                                    url: `${url}/${item.image?.level?.cube?._url}`,
                                    tileSize: item?.image?._tilesize,
                                    tiledImageHeight: item.image?.level?._tiledimageheight,
                                    tiledImageWidth: item.image?.level?._tiledimagewidth
                                }

                                if (d) arr.push(d)
                            }
                        }

                        return {
                            name: item._name,
                            heading: item._heading,
                            thumburl: item._thumburl,
                            lat: item._lat,
                            lng: item._lng,
                            previewUrl: item.preview?._url,
                            images: arr
                        }
                    })
                    nameSceneCurrent.current = scenesData.current[0].name
                }
            } else {
                const scene: _TScene = {
                    ...data.krpano.scene
                }
                const arr: _TDataScene[] | undefined = []
                if (data.krpano._version.includes('1.20')) {

                    const d: _TDataScene[] | undefined = scene.image?.cube?._multires?.split(',').splice(1).map(
                        i => ({
                            type: 'cube',
                            url: `${url}/${scene.image?.cube?._url}`,
                            tileSize: scene.image?.cube?._multires?.split(',')[0],
                            tiledImageHeight: i,
                            tiledImageWidth: i
                        })
                    )

                    if (d) arr.push(...d)
                } else {
                    if (Array.isArray(scene.image?.level)) {
                        const d: _TDataScene[] | undefined = scene.image?.level?.map(
                            i => ({
                                type: scene.image?._type,
                                url: `${url}/${i.cube?._url}`,
                                tileSize: scene?.image?._tilesize,
                                tiledImageHeight: i._tiledimageheight,
                                tiledImageWidth: i._tiledimagewidth
                            })
                        )

                        if (d) arr.push(...d)
                    } else {
                        const d = {
                            type: scene.image?._type,
                            url: `${url}/${scene.image?.level?.cube?._url}`,
                            tileSize: scene?.image?._tilesize,
                            tiledImageHeight: scene.image?.level?._tiledimageheight,
                            tiledImageWidth: scene.image?.level?._tiledimagewidth
                        }

                        if (d) arr.push(d)
                    }
                }
                const z = {
                    name: scene._name,
                    heading: scene._heading,
                    thumburl: scene._thumburl,
                    lat: scene._lat,
                    lng: scene._lng,
                    previewUrl: scene.preview?._url,
                    images: arr
                }
                scenesData.current = [z]
                nameSceneCurrent.current = scenesData.current[0].name
            }

            console.log('scenes: ', scenesData.current)
            return (
                <>
                    {/*<div>*/}
                    {/*    <button onClick={() => {*/}
                    {/*        const spotname = 's' + new Date().getTime().toString() + Math.floor(Math.random() * 1000000);*/}
                    {/*        const data = {*/}
                    {/*            name: spotname,*/}
                    {/*            ath: 0,*/}
                    {/*            atv: 0,*/}
                    {/*            url: 'https://0xllllh.github.io/krpano-examples/images/hotspot.png',*/}
                    {/*            type: 'image',*/}
                    {/*            keep: true,*/}
                    {/*            width: '64',*/}
                    {/*            height: '64',*/}
                    {/*            onUp: () => handler(spotname),*/}
                    {/*        }*/}
                    {/*        setHotspots(prev => [*/}
                    {/*            ...prev,*/}
                    {/*            data*/}
                    {/*        ])*/}
                    {/*    }} style={{position: 'fixed', top: 10, left: 10, zIndex: 9}}>*/}
                    {/*        Add Hotspot*/}
                    {/*    </button>*/}
                    {/*</div>*/}
                    <Krpano
                        target={'krpano'}
                        onReady={() => {
                            setInit(true)
                        }}
                        xml="/vr/vtour_test/skin/vtourskin.xml"
                    >
                        <SkinSetting littleplanetintro={"true"}/>
                        {
                            scenesData.current.map((scene, index) => (
                                <Scene
                                    key={index}
                                    name={scene.name ?? `scene_${index}`}
                                    lat={scene.lat}
                                    lng={scene.lng}
                                    heading={scene.heading}
                                    previewUrl={`${url}/${scene.previewUrl}`}
                                    images={scene.images}
                                    thumburl={`${url}/${scene.thumburl}`}
                                />
                            ))
                        }
                        <View
                            fov={120}
                            fovMin={70}
                            fovMax={140}
                            limitView={"auto"}
                            maxPixelZoom={2}
                            hlookat={0}
                            vlookat={0}
                        />
                    </Krpano>
                </>
            )
        }

        return null
    }

    return (
        <div>
            {getKrpanoView()}
        </div>
    )
}