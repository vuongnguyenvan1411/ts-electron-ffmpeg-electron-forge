import {browserVersion, engineName, engineVersion, fullBrowserVersion, getUA, mobileModel, mobileVendor, osName, osVersion} from "react-device-detect";
import {Divider, Typography} from "antd";
import {useConfigContext} from "../../contexts/ConfigContext";
import {StoreConfig} from "../../../config/StoreConfig";

export const TestAgentScreen = () => {
    const [config] = useConfigContext();
    const storeSingleton = StoreConfig.getInstance();

    console.log('useConfigContext', config)
    console.log('storeSingleton.Agent', storeSingleton.Agent)
    console.log('isWebApp', config.agent?.isWebApp())
    console.log('isWebAppInBrowser', config.agent?.isWebAppInBrowser())
    console.log('isWebAppInMobile', config.agent?.isWebAppInMobile())

    return (
        <div className={"container mx-auto px-4"}>
            <Divider orientation={"left"}>Agent</Divider>
            <div><Typography.Text strong>getUA:</Typography.Text> {getUA}</div>

            <Divider orientation={"left"}>OS</Divider>
            <div><Typography.Text strong>osName:</Typography.Text> {osName}</div>
            <div><Typography.Text strong>osVersion:</Typography.Text> {osVersion}</div>

            <Divider orientation={"left"}>Browser</Divider>
            <div><Typography.Text strong>engineName:</Typography.Text> {engineName}</div>
            <div><Typography.Text strong>engineVersion:</Typography.Text> {engineVersion}</div>
            <div><Typography.Text strong>browserVersion:</Typography.Text> {browserVersion}</div>
            <div><Typography.Text strong>fullBrowserVersion:</Typography.Text> {fullBrowserVersion}</div>

            <Divider orientation={"left"}>Mobile</Divider>
            <div><Typography.Text strong>mobileModel:</Typography.Text> {mobileModel}</div>
            <div><Typography.Text strong>mobileVendor:</Typography.Text> {mobileVendor}</div>
        </div>
    )
}
