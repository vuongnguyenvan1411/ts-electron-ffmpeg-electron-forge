export const TestEnvScreen = () => {
    return (
        <div className={"container mx-auto px-4"}>
            {
                Object.entries(process.env).map(([key, value], index) => (
                    <div key={index}>{key}: {value}</div>
                ))
            }
            <div>NEXT_PUBLIC_PLATFORM_TEST: {process.env.NEXT_PUBLIC_SLPK_TEST}</div>
            <div>NEXT_PUBLIC_PLATFORM_TEST: {process.env.NEXT_PUBLIC_PLATFORM_TEST}</div>
        </div>
    )
}
