import React, {ReactElement, useCallback, useEffect, useRef, useState} from "react";
import {BigUp, T_BigUpOptions} from "../../../core/upload/BigUp";
import {Button, Divider, message, Progress, Select, Spin, Tooltip, Typography, Upload, UploadFile} from "antd";
import {EDData} from "../../../core/encrypt/EDData";
import moment from "moment/moment";
import {Exif} from "../../../core/Exif";
import {BigUpFile} from "../../../core/upload/BigUpFile";
import {EDFile} from "../../../core/encrypt/EDFile";
import {App} from "../../../const/App";
import {findIndex, forEach, upperFirst} from "lodash";
import {DeleteOutlined, FileTextOutlined, InboxOutlined, LoadingOutlined, PictureOutlined} from "@ant-design/icons";
import {Utils} from "../../../core/Utils";
import {BigUpChecksum} from "../../../core/upload/BigUpChecksum";
import {useInjection} from "inversify-react";
import {StoreConfig} from "../../../config/StoreConfig";
import {DndProvider, useDrag, useDrop} from 'react-dnd';
import {HTML5Backend} from 'react-dnd-html5-backend';
import update from 'immutability-helper';
import {useTranslation} from "react-i18next";
import {ApiService} from "../../../repositories/ApiService";
import {PlatformFileModel} from "../../../models/platform/PlatformFileModel";
import {WrapContentWidget} from "../../widgets/WrapContentWidget";
import {GeodeticsAction} from "../../../recoil/service/geodetic/geodetics/GeodeticsAction";
import useDebounce from "../../hooks/useDebounce";

enum _E_SendingStatus {
    idle,
    checking,
    loading,
    complete,
    success,
    error,
    warning
}

// type _T_Progress = {
//     file: BigUpFile,
//     remaining?: number,
//     speed?: number
//     state: _E_SendingStatus
// }

type _T_ResultProcess = {
    percent: number
    remaining: number,
    sizeUploaded: number
    totalSize: number
}

// const useRefEventListener = (fn) => {
//     const fnRef = useRef(fn)
//     fnRef.current = fn
//
//     return fnRef
// }

interface _I_BigUploadFile<T = any> extends UploadFile<T> {
    isValid?: boolean
    file?: BigUpFile
    remaining?: number
    speed?: number
    state?: _E_SendingStatus
}

interface DragableUploadListItemProps {
    originNode: ReactElement;
    file: _I_BigUploadFile
    fileList: _I_BigUploadFile[]
    moveRow: (dragIndex: any, hoverIndex: any) => void
    search?: _I_BigUploadFile
}

export const TestBigUpScreen = () => {
    const {t} = useTranslation()
    const storeConfig = useInjection(StoreConfig)
    const apiService = useInjection(ApiService)

    const [listChecksum, setListChecksum] = useState<any[]>([])
    const [select, setSelect] = useState<any>('required')
    const [isDelete, setIsDelete] = useState<boolean>(false)
    const [isDisable, setIsDisable] = useState<boolean>(true)
    const [isDone, setIsDone] = useState<boolean>(true)
    const title = t('title.geodetic')
    const lengthFiles = useRef<number>(0)
    const {
        vm,
    } = GeodeticsAction()
    const handleChange = (value: string) => {
        if (value !== 'required') {
            setIsDisable(false)
        } else {
            setIsDisable(true)
        }
        setSelect(value)
    }

    const bigUpRef = useRef<BigUp>()
    // const [upFiles, setUpFiles] = useState<Record<string, _T_Progress>>({})
    // const upFilesRef = useRef<Record<string, _T_Progress>>(upFiles)
    const [resultProcess, setResultProcess] = useState<_T_ResultProcess>()
    const [loadingProcess, setLoadingProcess] = useState<boolean>(false)

    const [fileList, setFileList] = useState<_I_BigUploadFile[]>([])
    const fileListRef = useRef<_I_BigUploadFile[]>(fileList)

    const [resultChecksums, setResultChecksums] = useState<Record<string, any>>()
    const [resultExif, setResultExif] = useState<Record<string, any> | string>()


    const resultApi = useRef<PlatformFileModel>()

    const langTime = {
        ending: '',
        year: t("text.year").toLowerCase(),
        day: t("text.day").toLowerCase(),
        hour: t("text.hour").toLowerCase(),
        minute: t("text.minute").toLowerCase(),
        second: t("text.second").toLowerCase()
    }

    const isDebounce = useDebounce(listChecksum, 500)
    const createError = (file, errMes, type = 'default') => {
        if (type === 'vr') {
            const ele = document.getElementById(file.uid)

            if (ele) {
                const p = ele.querySelector('#error')

                if (p) ele.removeChild(p)

                const err = document.createElement('p')
                err.id = 'error'
                Object.assign(err.style, {
                    color: 'red',
                    position: 'absolute',
                    top: '60px',
                    left: '8px'
                })
                err.innerText = 'Ảnh không đúng định dạng 360!'
                ele.appendChild(err)
            }
        } else {
            const ele = document.getElementById(file.uid)

            if (ele) {
                const p = ele.querySelector('#error')

                if (p) ele.removeChild(p)

                const err = document.createElement('p')
                err.id = 'error'
                Object.assign(err.style, {
                    color: 'red',
                    position: 'absolute',
                    top: '60px',
                    left: '8px'
                })
                err.innerText = `File không đúng định dạng ${errMes}!`
                ele.appendChild(err)
            }
        }
    }

    useEffect(() => {

        console.log('effect run!')
        if (select === 'vr') {
            if (isDone) {
                console.log('change!')
                fileList.filter(item => item.type !== 'image/png' && item.type !== 'image/tiff' && getType(item.name) !== 'slpk' && getType(item.name) !== 'las').forEach(item => {
                    let img = new Image()
                    // @ts-ignore
                    img.src = window.URL.createObjectURL(item.originFileObj)
                    img.onload = () => {
                        if (img.width / img.height !== 2) {

                            item.isValid = false
                            createError(item, '', 'vr')

                        } else {
                            item.isValid = true
                        }
                        return true
                    }
                })
                fileList.filter(item => item.type === 'image/png' || item.type === 'image/tiff' || getType(item.name) === 'slpk' || getType(item.name) === 'las').forEach(i => {
                    i.isValid = false
                    createError(i, '')
                })
                listChecksum.filter(item => item.file.type === 'image/tiff' || getType(item.file.name) === 'slpk' || getType(item.file.name) === 'las').forEach(i => {
                    i.isValid = false
                })
            }
        } else if (select === '2d') {
            fileList.filter(item => item.type !== 'image/tiff').forEach(i => {
                i.isValid = false
                createError(i, '2D')
            })
            listChecksum.filter(item => item.file.type !== 'image/tiff').forEach(i => {
                i.isValid = false
            })
        } else if (select === '3d') {
            fileList.filter(item => !(item.name.includes('slpk')) && !(item.name.includes('las'))).forEach(iz => {
                iz.isValid = false
                createError(iz, '3D')
            })
            listChecksum.filter(item => !(item.file.name.includes('slpk')) && !(item.file.name.includes('las'))).forEach(iz => {
                iz.isValid = false
            })
        }
        console.log('checksum: ', listChecksum)

        const file = listChecksum[0]
        if (file) {

            setIsDone(false)
            if (file.isValid !== false) {
                const index = findIndex(fileListRef.current, (o) => o.uid == (file.file as any).uid)

                if (index != -1) {
                    setFileList(items => {
                        items[index] = {
                            ...items[index],
                            name: 'Đang kiểm tra tập tin...',
                            state: _E_SendingStatus.checking
                        }

                        fileListRef.current = [...items]

                        return [...items]
                    })
                }

                file.setChecksum()
                    .then(() => {
                        const index = findIndex(fileListRef.current, (o) => o.uid == (file.file as any).uid)

                        if (index != -1) {
                            setFileList(items => {
                                items[index] = {
                                    ...items[index],
                                    file: file,
                                    name: `${file.name} (${Utils.formatByte(file.size)})`,
                                    state: _E_SendingStatus.idle
                                }

                                fileListRef.current = [...items]

                                return [...items]
                            })
                        }

                        setListChecksum(prev => prev.filter(item => item.name !== file.name))
                        console.log('queue!')
                    })
            } else {
                setListChecksum(prev => prev.filter(item => item.name !== file.name))
                console.log('skip!')
            }
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isDebounce, select])


    useEffect(() => {
        lengthFiles.current = fileList.length
    }, [fileList])

    useEffect(() => {
        let oAgent

        if (storeConfig.Agent && storeConfig.Agent.geoLocation && Object.keys(storeConfig.Agent.geoLocation).length > 0) {
            oAgent = EDData.setData({
                geoLocation: storeConfig.Agent.geoLocation
            })
        }

        const opts: T_BigUpOptions = {
            targetUpload: 'upload/chunk',
            targetQueue: 'upload/queue',
            permanentErrors: [401, 404, 500, 501, 502],
            maxChunkRetries: 1,
            chunkRetryInterval: 1000 * 5,
            simultaneousUploads: 4,
            chunkSize: (1024 * 1024) * 5,
            checksum: true,
            headers: {
                Platform: 'web'
            },
            // query: {
            //     id: v4(),
            //     created_at: moment().unix()
            // },
            changeRawDataBeforeSend: (chunk, data) => {
                const post = {}
                const file = {}

                if (data instanceof FormData) {
                    let isFromRaw: boolean = false

                    if (data.has('raw')) {
                        const raw = data.get('raw')

                        if (raw && typeof raw === "string") {
                            const jRaw = JSON.parse(raw)

                            if (Object.keys(jRaw).length > 0) {
                                forEach(jRaw, (value, key) => {
                                    post[key] = value
                                })

                                isFromRaw = true
                            }
                        }
                    }

                    data.forEach((value, key) => {
                        if (value instanceof File) {
                            file[key] = value
                        } else if (!isFromRaw) {
                            post[key] = value
                        }
                    })
                }

                const form = new FormData()

                if (Object.keys(post).length > 0) {
                    form.append('data', EDData.setData(post))
                }

                forEach(file, (value, key) => {
                    form.append(key, value)
                })

                return form
            },
            changeRawHeaderBeforeSend: (chunk, header) => {
                if (process.env.NEXT_PUBLIC_API_GFILE_TOKEN) {
                    const et = EDData.setData({
                        t: process.env.NEXT_PUBLIC_API_GFILE_TOKEN,
                        // e: moment().add(30, 'seconds').unix()
                        e: moment().add(1, 'days').unix()
                    })

                    header = {
                        ...header,
                        Authorization: `Bearer ${et}`,
                        ...(
                            oAgent && {
                                "o-agent": oAgent
                            }
                        )
                    }
                }

                return header
            },
            changeRawTargetBeforeSend: ({path, query}) => {
                const ep = EDFile.setLinkUrl({
                    p: path,
                    ...(
                        query && {
                            q: query
                        }
                    ),
                    // e: moment().add(30, 'seconds').unix()
                    e: moment().add(1, 'days').unix()
                })

                return `${App.ApiGFile}/${ep}`
            }
        }

        const bigUp = new BigUp(opts)

        const dropzone = document.getElementById('dropzone');

        if (dropzone) {
            bigUp.assignBrowse(dropzone)
            bigUp.assignDrop(dropzone)
        }

        bigUp.on("filesAdded", (files, event) => {
            console.log('filesAdded', files, event)

            // const _tmp: Record<string, _T_Progress> = {}
            //
            // files.map(file => {
            //     _tmp[file.uniqueIdentifier] = {
            //         file: file,
            //         state: SendingStatus.idle
            //     }
            // })
            //
            // setUpFiles(v => {
            //     const nv = {
            //         ...v,
            //         ..._tmp
            //     }
            //
            //     upFilesRef.current = nv
            //     return nv
            // })
        })
        bigUp.on('fileChecksum', (file, hash) => {
            console.log('fileChecksum', file.name, hash)
        })
        bigUp.on('fileRemoved', file => {
            console.log('removed: ', file)
        })
        bigUp.on("fileAdded", (file) => {

            console.log('filesss: ', file)

            setListChecksum(prev => {
                return [
                    ...prev,
                    file
                ]
            })

            // file.setChecksum()
            //     .then(() => {
            //         const index = findIndex(fileListRef.current, (o) => o.uid == (file.file as any).uid)
            //
            //         if (index != -1) {
            //             setFileList(items => {
            //                 items[index] = {
            //                     ...items[index],
            //                     file: file,
            //                     name: `${file.name} (${Utils.formatByte(file.size)})`,
            //                     state: _E_SendingStatus.idle
            //                 }
            //
            //                 fileListRef.current = [...items]
            //
            //                 return [...items]
            //             })
            //         }
            //     })
        })
        bigUp.on("fileSuccess", (file) => {
            console.log('END', moment().format('DD-MM-YYYY HH:mm:ss'))

            const index = findIndex(fileListRef.current, (o) => o.uid == (file.file as any).uid)

            if (index != -1) {
                setFileList(items => {
                    items[index] = {
                        ...items[index],
                        file: file,
                        state: _E_SendingStatus.complete,
                        status: "success"
                    }

                    fileListRef.current = [...items]

                    return [...items]
                })
            }

            // if (upFilesRef.current && upFilesRef.current.hasOwnProperty(file.uniqueIdentifier)) {
            //     const item = upFilesRef.current[file.uniqueIdentifier]
            //
            //     setUpFiles(_ => {
            //         const nv: Record<string, _T_Progress> = {
            //             ...upFilesRef.current,
            //             [file.uniqueIdentifier]: {
            //                 ...item,
            //                 state: SendingStatus.success
            //             }
            //         }
            //
            //         upFilesRef.current = nv
            //         return nv
            //     })
            // }
        })
        bigUp.on("fileError", (file, message) => {
            console.log('fileError', file, message)

            const index = findIndex(fileListRef.current, (o) => o.uid == (file.file as any).uid)

            if (index != -1) {
                setFileList(items => {
                    items[index] = {
                        ...items[index],
                        file: file,
                        state: _E_SendingStatus.error,
                        status: "error"
                    }

                    fileListRef.current = [...items]

                    return [...items]
                })
            }

            // if (upFilesRef.current && upFilesRef.current.hasOwnProperty(file.uniqueIdentifier)) {
            //     const item = upFilesRef.current[file.uniqueIdentifier]
            //
            //     setUpFiles(_ => {
            //         const nv: Record<string, _T_Progress> = {
            //             ...upFilesRef.current,
            //             [file.uniqueIdentifier]: {
            //                 ...item,
            //                 state: SendingStatus.error
            //             }
            //         }
            //
            //         upFilesRef.current = nv
            //         return nv
            //     })
            // }
        })
        bigUp.on('fileProgress', (file) => {
            const index = findIndex(fileListRef.current, (o) => o.uid == (file.file as any).uid)

            if (index != -1) {
                setFileList(items => {
                    items[index] = {
                        ...items[index],
                        percent: Math.floor(file.progress() * 100),
                        status: "uploading",
                        remaining: file.timeRemaining(),
                        speed: file.averageSpeed
                    }

                    fileListRef.current = [...items]

                    return [...items]
                })
            }

            // if (upFilesRef.current && upFilesRef.current.hasOwnProperty(file.uniqueIdentifier)) {
            //     const item = upFilesRef.current[file.uniqueIdentifier]
            //     setUpFiles(() => {
            //         const nv: Record<string, _T_Progress> = {
            //             ...upFilesRef.current,
            //             [file.uniqueIdentifier]: {
            //                 ...item,
            //                 remaining: file.timeRemaining(),
            //                 speed: file.averageSpeed
            //             }
            //         }
            //
            //         upFilesRef.current = nv
            //         return nv
            //     })
            // }
        })
        bigUp.on('progress', () => {
            setResultProcess({
                percent: Math.floor(bigUp.progress() * 100),
                remaining: bigUp.timeRemaining(),
                sizeUploaded: bigUp.sizeUploaded(),
                totalSize: bigUp.getSize()
            })
        })
        bigUp.on('uploadStart', () => {
            setLoadingProcess(true)
        })
        bigUp.on('complete', () => {
            setLoadingProcess(false)
        })

        bigUpRef.current = bigUp

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const onClickUpload = () => {
        const isUpload = fileList.every(item => item.isValid !== false)

        if (isUpload) {
            setIsDelete(true)
            const upload = () => {
                if (bigUpRef.current && resultApi.current) {
                    if (!bigUpRef.current.hasQuery('id')) {
                        bigUpRef.current.addQuery('id', resultApi.current.id)
                    }
                    if (!bigUpRef.current.hasQuery('created_at')) {
                        bigUpRef.current.addQuery('created_at', resultApi.current.dateCreatedAtUnix() ?? moment().unix())
                    }

                    bigUpRef.current.upload()
                }
            }

            if (!resultApi.current) {
                apiService
                    .addPlatformFile({
                        name: `Haidt-Test-${moment().unix()}`,
                        type: select
                    })
                    .then(r => {
                        if (r.success) {
                            if (r.data) {
                                resultApi.current = new PlatformFileModel(r.data)

                                upload()
                            }
                        } else {
                            console.log('Error:addPlatformFile', r.error)
                        }
                    })
                    .catch(e => console.log('Exception:addPlatformFile', e))
            } else {
                upload()
            }
        } else {
            message.error('Vui lòng kiểm tra lại!').then()
        }
    }

    const moveRow = useCallback((dragIndex: number, hoverIndex: number) => {
        const dragRow = fileList[dragIndex]

        setFileList(
            update(fileList, {
                $splice: [
                    [dragIndex, 1],
                    [hoverIndex, 0, dragRow]
                ]
            })
        )
    }, [fileList])

    const statusBtnUpload = useCallback(() => {
        if (fileList.length === 0) {
            return false
        }

        let checking = 0

        fileList.forEach(item => {
            if (item.state === _E_SendingStatus.checking) {
                checking++
            }
        })

        return checking == 0
    }, [fileList])

    const getType = (name: string) => {
        const type = name.split('.')
        return type[type.length - 1]
    }

    const renderIcon = (file: _I_BigUploadFile) => {
        if (file.state === _E_SendingStatus.checking) {
            return <div className={"w-16 h-8 flex justify-center items-center -ml-4"}><Spin
                indicator={<LoadingOutlined style={{fontSize: '24px'}} spin/>}/></div>
        }

        if (file.type && file.type.length > 0) {
            if (file.type.indexOf('image') === 0) {
                return <div className={"w-16 h-8 flex justify-center items-center -ml-4"}><PictureOutlined
                    style={{fontSize: '24px'}}/></div>
            }
        }

        return <div className={"w-16 h-8 flex justify-center items-center -ml-4"}><FileTextOutlined
            style={{fontSize: '24px'}}/></div>
    }

    return (

        <WrapContentWidget
            masterHeader={{
                title: title,
                isLoading: vm.isLoading,
                onReload: () => {
                },
            }}
            bodyHeader={{
                title: "Tải File",
                right: <Select
                    defaultValue={select}
                    style={{width: 180, fontSize: 15}}
                    size={"large"}
                    onChange={handleChange}
                    options={[
                        {
                            value: 'required',
                            label: 'Chọn loại file',
                        },
                        {
                            value: '2d',
                            label: '2D',
                        },
                        {
                            value: '3d',
                            label: '3D',
                        },
                        {
                            value: 'vr',
                            label: 'VR360',
                        },
                    ]}
                />
            }}
        >
            <DndProvider backend={HTML5Backend}>
                <div className={"container mx-auto px-4"}>
                    <div id="app">
                        <Divider>ANT Upload</Divider>
                        <Upload.Dragger
                            multiple={true}
                            listType="picture"
                            fileList={fileList}
                            disabled={isDisable}
                            maxCount={select === 'vr' ? 10 : select === '2d' || select === '3d' ? 2 : 999}
                            accept=".tif, .tiff, .btf, .tf8, .bigtiff, .jpg, .jpeg, .psd, .psb, .kro, .las, .slpk, .png, .zip"
                            progress={{
                                strokeColor: {
                                    '0%': '#108ee9',
                                    '100%': '#87d068',
                                },
                                strokeWidth: 3,
                            }}
                            customRequest={() => {
                                //
                            }}
                            onRemove={(file: any) => {
                                console.log('onRemove', file)
                                setListChecksum(prev => prev.filter(item => item.file.uid !== file.uid))
                                bigUpRef.current?.removeFile(file.file)
                            }}
                            onChange={(info) => {
                                if (lengthFiles.current < info.fileList.length) {
                                    setIsDone(true)
                                    setFileList(items => {
                                        const _tmp = [
                                            ...items,
                                            {
                                                ...info.file,
                                                // name: 'Đang kiểm tra tệp tin...',
                                                // state: _E_SendingStatus.checking
                                            }
                                        ]

                                        fileListRef.current = _tmp

                                        return _tmp
                                    })
                                    if (bigUpRef.current && info.file.originFileObj) {
                                        bigUpRef.current.addFile(info.file.originFileObj)
                                    }
                                } else {
                                    // setIsDone(false)
                                    console.log('prev list: ', fileList)
                                    setFileList(info.fileList)
                                }
                            }}
                            previewFile={() => Promise.resolve('')}
                            // iconRender={(file: _I_BigUploadFile, listType) => {
                            //     if (file.state === _E_SendingStatus.checking) {
                            //         return <Spin indicator={<LoadingOutlined spin/>}/>
                            //     }
                            //
                            //     if (file.type && file.type.length > 0) {
                            //         if (file.type.indexOf('image') === 0) {
                            //             return <PictureOutlined/>
                            //         }
                            //     }
                            //
                            //     return <FileTextOutlined/>
                            // }}
                            itemRender={(originNode, file: _I_BigUploadFile, currFileList, actions) => {
                                return (
                                    <DragableUploadListItem
                                        // originNode={originNode}
                                        originNode={<>
                                            <div
                                                className={`relative flex items-center justify-between mt-4 px-4 pt-4 pb-8 border border-dark3 border-dashed hover:border-primary`}>
                                                <div id={`${file.uid}`}>
                                                    <div className={"flex items-center"}>
                                                        {
                                                            renderIcon(file)
                                                        }
                                                        <Typography.Title style={{margin: 0}}
                                                                          level={5}>{file.name} </Typography.Title>
                                                    </div>
                                                </div>
                                                {
                                                    !isDelete && (
                                                        <div onClick={() => {
                                                            setIsDone(false)
                                                            actions.remove()
                                                        }}><DeleteOutlined style={{fontSize: 20, cursor: 'pointer'}}/></div>
                                                    )
                                                }
                                            </div>
                                            <div style={{
                                                position: 'absolute',
                                                top: '42px',
                                                right: 0,
                                                width: "100%",
                                                padding: '4px 8px'
                                            }}>
                                                <Progress strokeColor={{'0%': '#108ee9', '100%': '#87d068'}}
                                                          percent={file.percent}/>
                                            </div>
                                        </>}
                                        file={file}
                                        fileList={currFileList}
                                        moveRow={moveRow}
                                    />
                                )
                            }}
                        >
                            <p className="ant-upload-drag-icon">
                                <InboxOutlined/>
                            </p>
                            <p className="ant-upload-text">Click or drag file to this area to upload</p>
                            <p className="ant-upload-hint">
                                {
                                    isDisable
                                        ? "Vui lòng chọn loại file!"
                                        : select === '2d' || select === '3d'
                                            ? "Upload tối đa 2 files"
                                            : select === 'vr'
                                                ? "Upload tối đa 10 files"
                                                : "Support for a single or bulk upload. Strictly prohibit from uploading company data or other band files"
                                }
                            </p>
                        </Upload.Dragger>
                        {
                            !!resultProcess && (
                                <div className={"mt-2"}>
                                    <Progress percent={resultProcess.percent}/>
                                    <div>
                                        {
                                            resultProcess.remaining > 0 && (
                                                <span
                                                    className={"mr-2"}>{Utils.formatTime(resultProcess.remaining, langTime)}</span>
                                            )
                                        }
                                        <span>{Utils.formatByte(resultProcess.sizeUploaded)}/{Utils.formatByte(resultProcess.totalSize)}</span>
                                    </div>
                                </div>
                            )
                        }
                        <Divider/>
                        <Button
                            onClick={onClickUpload}
                            type={"primary"}
                            disabled={!statusBtnUpload()}
                            loading={loadingProcess}
                        >
                            Upload
                        </Button>
                        <Button
                            className={"ml-5"}
                            onClick={() => {
                                if (bigUpRef.current) {
                                    console.log(bigUpRef.current.files)
                                }
                            }}
                        >
                            Console
                        </Button>
                        {/*<Divider>BigUp</Divider>
                <div id="dropzone">Click or drop to select a file</div>
                {
                    Object.keys(upFiles).length > 0 && (
                        <List
                            itemLayout="horizontal"
                        >
                            {
                                Object.entries(upFiles).map(([key, value], index) => (
                                    <List.Item key={index} style={{display: "block"}}>
                                        <List.Item.Meta
                                            avatar={index + 1}
                                            title={value.file.name}
                                            description={Utils.formatByte(value.file.size)}
                                        />
                                        <div>
                                            {
                                                value.state != SendingStatus.complete && (
                                                    <>
                                                        {
                                                            value.speed && (
                                                                <div>{Utils.formatByte(value.speed)}/s</div>
                                                            )
                                                        }
                                                        {
                                                            value.remaining && (
                                                                <div>{Utils.formatTime(value.remaining, langTime)}</div>
                                                            )
                                                        }
                                                    </>
                                                )
                                            }
                                            {
                                                value.file.progress() > 0 && (
                                                    <Progress percent={Math.round(value.file.progress() * 100)}/>
                                                )
                                            }
                                        </div>
                                    </List.Item>
                                ))
                            }
                        </List>
                    )
                }
                {
                    !!totalProgress && (
                        <Progress percent={totalProgress}/>
                    )
                }
                <Divider/>
                <Button
                    onClick={onClickUpload}
                >
                    Upload
                </Button>*/}
                        <Divider>Exif</Divider>
                        <input
                            type={"file"}
                            onChange={(evt) => {
                                if (!evt) {
                                    return
                                }

                                setResultExif(undefined)

                                // @ts-ignore
                                const file = evt.target.files[0]

                                if (file && file.name) {
                                    const exif = new Exif({
                                        debug: true
                                    })

                                    exif.getData(file, function () {
                                        const exifData = exif.pretty(file);

                                        console.log(exif.getAllTags(file))

                                        if (exifData) {
                                            setResultExif(exifData)
                                        } else {
                                            setResultExif("No EXIF data found in image '" + file.name + "'.")
                                        }
                                    })
                                }
                            }}
                        />
                        {
                            resultExif && (
                                <div>
                                    {
                                        typeof resultExif === "string"
                                            ? resultExif.length > 0 && (
                                            <div className={"whitespace-pre-line"}>{resultExif}</div>)
                                            :
                                            Object.keys(resultExif).length > 0 && Object.entries(resultExif).map(([key, value], index) => (
                                                <div key={index}>{upperFirst(key)}: {value}</div>
                                            ))
                                    }
                                </div>
                            )
                        }
                        <Divider>Checksums</Divider>
                        <input
                            type={"file"}
                            onChange={async (evt) => {
                                if (!evt) {
                                    return
                                }

                                setResultChecksums(undefined)

                                // @ts-ignore
                                const file = evt.target.files[0]

                                if (file && file.name) {
                                    const checksums = new BigUpChecksum(file)

                                    const start = Date.now()
                                    const hash = await checksums.readFile(file)
                                    const end = Date.now()
                                    const duration = end - start
                                    const fileSizeMB = file.size / 1024 / 1024
                                    const throughput = fileSizeMB / (duration / 1000)

                                    setResultChecksums(v => {
                                        return {
                                            ...v,
                                            hash: hash,
                                            duration: duration + ' ms',
                                            throughput: throughput.toFixed(2) + ' MB/s'
                                        }
                                    })
                                }
                            }}
                        />
                        {
                            resultChecksums && Object.keys(resultChecksums).length > 0 && (
                                <div>
                                    {
                                        Object.entries(resultChecksums).map(([key, value], index) => (
                                            <div key={index}>{upperFirst(key)}: {value}</div>
                                        ))
                                    }
                                </div>
                            )
                        }
                    </div>
                </div>
            </DndProvider>
        </WrapContentWidget>
    )
}

const type = 'DragableUploadList'

const DragableUploadListItem = ({originNode, moveRow, file, fileList}: DragableUploadListItemProps) => {
    const {t} = useTranslation()

    const ref = useRef<HTMLDivElement>(null);
    const index = fileList.indexOf(file);
    const [{isOver, dropClassName}, drop] = useDrop({
        accept: type,
        collect: (monitor) => {
            const {index: dragIndex} = monitor.getItem() || {};
            if (dragIndex === index) {
                return {};
            }
            return {
                isOver: monitor.isOver(),
                dropClassName: dragIndex < index ? ' drop-over-downward' : ' drop-over-upward',
            };
        },
        drop: (item: any) => {
            moveRow(item.index, index);
        }
    })
    const [, drag] = useDrag({
        type,
        item: {index},
        collect: (monitor) => ({
            isDragging: monitor.isDragging()
        })
    })

    drop(drag(ref))

    const errorNode = <Tooltip title="Upload Error">{originNode.props.children}</Tooltip>

    const langTime = {
        ending: '',
        year: t("text.year").toLowerCase(),
        day: t("text.day").toLowerCase(),
        hour: t("text.hour").toLowerCase(),
        minute: t("text.minute").toLowerCase(),
        second: t("text.second").toLowerCase()
    }

    return (
        <div
            ref={ref}
            className={`ant-upload-draggable-list-item ${isOver ? dropClassName : ''}`}
            style={{
                cursor: 'move',
                position: 'relative'
            }}
        >
            {file.status === 'error' ? errorNode : originNode}

            <div
                style={{
                    position: "absolute",
                    top: "64px",
                    left: "8px",
                    fontSize: "12px"
                }}
            >
                {
                    file.speed && (
                        file.state === 3
                            ? <></>
                            : <span className={"mr-2"}>{Utils.formatByte(file.speed)}/s</span>
                    )
                }
                {
                    (file.remaining && file.remaining > 0)
                        ? <span>{Utils.formatTime(file.remaining, langTime)}</span>
                        : <></>
                }
            </div>
        </div>
    )
}
