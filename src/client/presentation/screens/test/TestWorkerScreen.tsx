import {useCallback, useEffect, useRef, useState} from "react";
import {Button, Divider} from "antd";

export const TestWorkerScreen = () => {
    const webWorkerRef = useRef<Worker>()
    const [webWorkerData, setWebWorkerData] = useState<string>()

    useEffect(() => {
        // Worker Web
        webWorkerRef.current = new Worker(new URL('../../../../worker.ts', import.meta.url))
        webWorkerRef.current.onmessage = (event: MessageEvent<number>) => setWebWorkerData(`WebWorker Response => ${event.data}`)

        // Service Worker
        navigator.serviceWorker
            .register('/workers/sw.js')
            .then((registration) =>
                console.log(
                    'Service Worker registration successful with scope: ',
                    registration.scope
                )
            )
            .catch((err) => console.log('Service Worker registration failed: ', err))

        return () => {
            if (webWorkerRef.current) webWorkerRef.current.terminate()
        }
    }, [])

    const handleWork = useCallback(async () => {
        if (webWorkerRef.current) webWorkerRef.current.postMessage(100000)
    }, [])

    return (
        <div className={"container mx-auto px-4"}>
            <Divider>Do work in a Web Worker!</Divider>
            <Button
                onClick={handleWork}
            >
                Calculate PI
            </Button>
            {
                webWorkerData && (
                    <div className={"mt-2"}>{webWorkerData}</div>
                )
            }
            <Divider>Do work in a Service Worker!</Divider>
        </div>
    )
}
