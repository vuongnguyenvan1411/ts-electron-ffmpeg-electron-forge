import {Button, Divider, Upload} from "antd";
import {UploadOutlined} from "@ant-design/icons";
import {useNavigate} from "react-router";

export const TestTempScreen = () => {
    const navigate = useNavigate()

    const onChange = (e) => {
        console.log(e)
    }

    return (
        <div className={"container mx-auto px-4"}>
            <Button onClick={() => navigate('/')}>Close</Button>
            <Divider>ANT</Divider>
            <Upload
                directory
                fileList={[]}
                showUploadList={false}
                onChange={(e) => {
                    console.log(e)
                }}
                customRequest={() => {
                    //
                }}
                previewFile={() => Promise.resolve('')}
            >
                <Button icon={<UploadOutlined/>}>Upload Directory</Button>
            </Upload>
        </div>
    )
}
