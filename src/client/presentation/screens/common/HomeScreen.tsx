import {Typography} from "antd";
import styles from "../../../styles/module/Home.module.scss"
import {ReactNode, useEffect} from "react";
import {useNavigate} from "react-router";
import {RouteConfig} from "../../../config/RouteConfig";
import {useTranslation} from "react-i18next";
import {Color} from "../../../const/Color";
import Image from "next/image";
import {Images} from "../../../const/Images";
import {WrapContentWidget} from "../../widgets/WrapContentWidget";

type _TIconItem = {
    icon: ReactNode,
    title: string,
    onClick: () => void,
}

export const HomeScreen = () => {
    const {t} = useTranslation()
    const navigate = useNavigate()

    useEffect(() => {
        console.log('%cMount Screen: HomeScreen', Color.ConsoleInfo);

        return () => {
            console.log('%cUnmount Screen: HomeScreen', Color.ConsoleInfo);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const items: _TIconItem[] = [
        {
            icon: <Image
                src={Images.iconTimelapse.data}
                alt={Images.iconTimelapse.atl}
            />,
            title: t("title.timelapse"),
            onClick: () => navigate(RouteConfig.TIMELAPSE),
        },
        {
            icon: <Image
                width={160}
                height={160}
                src={Images.iconGeodetic.data}
                alt={Images.iconGeodetic.atl}
            />,
            title: t("title.geodetic"),
            onClick: () => navigate(RouteConfig.GEODETIC),
        },
        {
            icon: <Image
                width={160}
                height={160}
                src={Images.iconVr360.data}
                alt={Images.iconVr360.atl}
            />,
            title: t("title.vr360"),
            onClick: () => navigate(RouteConfig.VR360),
        },
        {
            icon: <Image
                width={160}
                height={160}
                src={Images.iconCamera.data}
                alt={Images.iconCamera.atl}
            />,
            title: t("title.camera"),
            onClick: () => navigate(RouteConfig.CAMERA),
        },
        {
            icon: <Image
                width={160}
                height={160}
                src={Images.iconWeighing.data}
                alt={Images.iconWeighing.atl}
            />,
            title: t("title.weighing"),
            onClick: () => navigate(RouteConfig.WEIGHING),
        },
        {
            icon: <Image
                width={160}
                height={160}
                src={Images.iconNews.data}
                alt={Images.iconNews.atl}
            />,
            title: t("title.news"),
            onClick: () => navigate(RouteConfig.BLOG),
        },
        {
            icon: <Image
                width={160}
                height={160}
                src={Images.iconAbout.data}
                alt={Images.iconAbout.atl}
            />,
            title: t("title.about"),
            onClick: () => navigate(RouteConfig.ABOUT),
        },
        {
            icon: <Image
                width={160}
                height={160}
                src={Images.iconFeedback.data}
                alt={Images.iconFeedback.atl}
            />,
            title: t("title.feedback"),
            onClick: () => navigate(RouteConfig.FEEDBACK),
        },
    ];

    return (
        <WrapContentWidget
            masterHeader={{
                title: t('title.home'),
            }}
            bodyHeader={null}
            className={styles.FixHeightContent}
        >
            <div className={styles.Content}>
                <Typography.Text
                    className={styles.TextTitle}
                >
                    {t("text.homeTitle")}
                </Typography.Text>
                <Typography.Text
                    className={styles.TextDes}
                >
                    {t("text.homeDescriptionOne")}
                    <br/>
                    {t("text.homeDescriptionTwo")}
                </Typography.Text>
                <div className={styles.Items}>
                    {
                        items.map(value =>
                            <div key={value.title} className={`${styles.Item} animate__animated animate__zoomInUp`}>
                                <div
                                    onClick={value.onClick}
                                    className={styles.Box}
                                >
                                    {value.icon}
                                </div>
                                <Typography.Text
                                >
                                    {value.title}
                                </Typography.Text>
                            </div>
                        )
                    }
                </div>
            </div>
        </WrapContentWidget>
    )
}
