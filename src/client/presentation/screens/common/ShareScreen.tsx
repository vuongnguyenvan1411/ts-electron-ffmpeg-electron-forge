import React, {useCallback, useEffect, useState} from "react";
import {BaseShareModel} from "../../../models/ShareModel";
import {MediaQuery} from "../../../core/MediaQuery";
import {Style} from "../../../const/Style";
import {ShareItemFC, ShareItemSkeleton} from "../../widgets/ShareItemFC";
import {CommonEmptyFC} from "../../widgets/CommonFC";
import {ErrorItemFC} from "../../widgets/ErrorItemFC";
import {SendingStatus} from "../../../const/Events";
import {CustomButton} from "../../components/CustomButton";
import {PlusCircleOutlined} from "@ant-design/icons";
import {useTranslation} from "react-i18next";
import {GetQrCodeModalFC} from "../../widgets/GetQrCodeModalFC";
import {Utils} from "../../../core/Utils";
import {Modal, notification} from "antd";
import styles from "../../../styles/module/Share.module.scss";

type  TShareProps = {
    title: string,
    status: SendingStatus,
    deleteStatus: SendingStatus,
    items: BaseShareModel[],
    isNextAvailable: boolean,
    onLoadMore?: React.MouseEventHandler<HTMLElement> | undefined,
    onDelete?: (sid: string) => void,
    onEdit: (item: BaseShareModel, index: number) => void,
    onView: (item: BaseShareModel, index: number) => void,
}

type  TVisibleModal = {
    sid?: string,
    visible: boolean,
}

export const ShareScreen: React.FC<TShareProps> = (props) => {
    const {t} = useTranslation();
    const [isModalVisible, setIsModalVisible] = useState<{
        sid: string,
        visible: boolean,
    }>({
        sid: '',
        visible: false,
    });
    const [isShowDelete, setIsShowDelete] = useState<TVisibleModal>({
        visible: false,
    })

    const onClickGetQrCode = (item: BaseShareModel, _: number) => {
        setIsModalVisible({
            sid: item.sid,
            visible: true,
        });
    }

    const onHandleCancelDelete = () => {
        setIsShowDelete({
            ...isShowDelete,
            visible: false,
        })
    }

    const onShowDelete = (sid: string) => {
        setIsShowDelete({
            ...isShowDelete,
            sid: sid,
            visible: true,
        })
    }

    const onCopy = (item: BaseShareModel, _: number) => {
        Utils.copyClipboard(item.link).then(() => {
            // setIsCopyOk(true);

            notification.success({
                message: t('text.successCopy'),
                description: item.link,
            });

            // setTimeout(_ => setIsCopyOk(false), App.TimeoutHideCopy)
        });
    }

    const onClickLink = (item: BaseShareModel, _: number) => {
        window.open(item.link, '_blank');
    }

    const onDelete = (item: BaseShareModel, _: number) => {
        onShowDelete(item.sid);
    }

    const onEdit = (item: BaseShareModel, _: number) => {
        props.onEdit(item, _);
    }

    const onView = (item: BaseShareModel, _: number) => {
        props.onView(item, _);
    }

    const onCloseGetQrCode = () => {
        setIsModalVisible({
            ...isModalVisible,
            visible: false,
        });
    }

    const onHandleOkDelete = useCallback(() => {

        if (isShowDelete.sid && props.onDelete) {
            props.onDelete(isShowDelete.sid);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isShowDelete.sid])

    useEffect(() => {
        if (props.deleteStatus === SendingStatus.success) {
            setIsShowDelete({
                ...isShowDelete,
                visible: false,
            })

            notification.success({
                message: t('success.delete'),
            });
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.deleteStatus])

    const _getLoading = () => {
        return <>
            {
                Array((new MediaQuery(Style.GridLoadingAnimation)).getPoint(4) * 2).fill(0).map((_, i) => {
                    return <ShareItemSkeleton key={i}/>;
                })
            }
        </>
    }

    return (
        <>
            <div className={styles.ShareContainer}>
                <ErrorItemFC
                    status={props.status}
                >
                    {
                        props.status === SendingStatus.loading && !props.isNextAvailable ?
                            <div
                                className={styles.GridItems}>{_getLoading()}</div> : props.items.length > 0 ?
                                <div className={styles.GridItems}>
                                    {
                                        props.items.map((e, i) => {
                                            return <ShareItemFC
                                                key={i}
                                                index={i}
                                                item={e}
                                                onQrCode={onClickGetQrCode}
                                                onCopy={onCopy}
                                                onLink={onClickLink}
                                                onDelete={onDelete}
                                                onEdit={onEdit}
                                                onView={onView}
                                            />

                                        })
                                    }
                                </div>
                                : <CommonEmptyFC/>
                    }
                </ErrorItemFC>
                {
                    props.isNextAvailable ?
                        <div className={"flex w-full justify-center p-6"}>
                            <CustomButton
                                loading={props.status === SendingStatus.loading}
                                onClick={props.onLoadMore}
                                icon={<PlusCircleOutlined/>}
                            >
                                {t("button.loadMore")}
                            </CustomButton>
                        </div>
                        : null
                }
            </div>
            {
                isModalVisible.visible
                    ? <GetQrCodeModalFC
                        sid={isModalVisible.sid}
                        name={props.title}
                        onClose={onCloseGetQrCode}
                    />
                    : null
            }
            <Modal
                key={"modal_type_delete_tl_share"}
                title={t("title.confirmBeforeDeleting")}
                visible={isShowDelete.visible}
                onCancel={onHandleCancelDelete}
                confirmLoading={props.deleteStatus === SendingStatus.loading}
                onOk={onHandleOkDelete}
            >
                {t("message.confirmBeforeDeleting")}
            </Modal>
        </>
    )
}
