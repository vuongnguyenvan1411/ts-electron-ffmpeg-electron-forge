import styles from "../../../styles/module/Download.module.scss"
import React, {useEffect} from "react";
import {CustomButton} from "../../components/CustomButton";
import AndroidBoxIcon from "../../components/svgs/AndroidBoxFill";
import WindowBoxIcon from "../../components/svgs/WindowBoxIcon";
import MacOsBoxIcon from "../../components/svgs/MacOsBoxIcon";
import IosIcon from "../../components/icons/IosIcon";
import ArrowRightFillIcon from "../../components/icons/ArrowRightFillIcon";
import {CustomTypography} from "../../components/CustomTypography";
import {Color} from "../../../const/Color";
import {useTranslation} from "react-i18next";
import {WrapContentWidget} from "../../widgets/WrapContentWidget";

interface IDownload {
    icon: React.ReactNode,
    name: string,
    link: string,
}

export const DownloadScreen = () => {
    const {t} = useTranslation();

    const downloads: IDownload[] = [
        {
            icon: <IosIcon/>,
            name: "iOS",
            link: 'https://apps.apple.com/us/app/autotimelapse-pro/id1525118283'
        },
        {
            icon: <AndroidBoxIcon/>,
            name: "Android",
            link: 'https://play.google.com/store/apps/details?id=com.autotimelapse.main'
        },
        {
            icon: <WindowBoxIcon/>,
            name: "Window",
            link: ''
        },
        {
            icon: <MacOsBoxIcon/>,
            name: "MacOS",
            link: ''
        }
    ];

    const onDownload = (linkDl: string) => {
        let el = document.createElement("a");
        el.href = linkDl;
        el.target = "_blank";
        el.download = '';
        el.click();
    }

    useEffect(() => {
        console.log('%cMount Screen: DownloadScreen', Color.ConsoleInfo);

        return () => {
            console.log('%cUnmount Screen: DownloadScreen', Color.ConsoleInfo);
        };

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <WrapContentWidget
            masterHeader={{
                title: `Atl - ${t("button.download")}`,
            }}
            bodyHeader={null}
            className={styles.Download}
        >
            <div className={"flex flex-col"}>
                <CustomTypography
                    textStyle={"text-title-1"}
                    isStrong
                >
                    Auto Timelapse
                </CustomTypography>
                <CustomTypography
                    textStyle={"text-title-1"}
                    isStrong
                >
                    Mobile and Desktop App
                </CustomTypography>
            </div>

            <div className={styles.DZone}>
                {
                    downloads.map(value => <div key={value.name} className={styles.DItem}>
                        <div
                            className={styles.Box}
                        >
                            {value.icon}
                        </div>
                        <CustomTypography
                            textStyle={"text-title-3"}
                            isStrong
                        >
                            {value.name}
                        </CustomTypography>
                        <CustomButton
                            disabled={value.link === ""}
                            onClick={() => onDownload(value.link)}
                        >
                            Download
                        </CustomButton>
                        {
                            value.link !== "" ? value.name.toLocaleLowerCase() === "window" || value.name.toLocaleLowerCase() === "macos" ?
                                    <div className={styles.Guide}>
                                        <CustomTypography
                                            textStyle={"text-body-text-2"}
                                        >
                                            Install Guide
                                        </CustomTypography>
                                        <ArrowRightFillIcon/>
                                    </div>
                                    : undefined
                                : <CustomTypography
                                    textStyle={"text-body-text-2"}
                                    isStrong
                                >
                                    Sắp ra mắt!
                                </CustomTypography>
                        }
                    </div>)
                }
            </div>
        </WrapContentWidget>
    )
}
