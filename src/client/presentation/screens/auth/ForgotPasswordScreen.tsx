import styles from "../../../styles/module/LoginTest.module.scss";
import {Input, Space, Typography} from "antd";
import MailIcon from "../../components/svgs/MailSvg";
import {CustomButton} from "../../components/CustomButton";
import {RouteConfig} from "../../../config/RouteConfig";
import {useNavigate} from "react-router";

export const ForgotPasswordScreen = () => {
    let navigate = useNavigate()

    return (
        <div className={"flex justify-end"}>
            <div
                className={styles.Login}
            >
                <div className={"flex flex-col"}>
                    <Typography.Title
                        level={2}
                        className={"text-title-2 text-weight-strong"}
                    >
                        Quên mật khẩu
                    </Typography.Title>
                    <Typography.Text className={"text-body-text-2"}>
                        Auto Timelapse sẽ gửi mail tới hộp thư của bạn.
                        Bạn hãy kiểm tra mail và làm theo hương dẫn nhé.
                    </Typography.Text>
                </div>
                <Space
                    style={{display: 'flex'}}
                    direction={"vertical"}
                    size={"middle"}
                >
                    <Input
                        className={"input-main"}
                        placeholder="Email"
                        prefix={<MailIcon/>}
                    />
                </Space>
                <CustomButton fullWidth>
                    Gửi
                </CustomButton>
                <div className={"flex justify-center"}>
                    <Typography.Text
                        onClick={() => navigate(RouteConfig.LOGIN)}
                        className={"text-body-text-2 pr-3 hover-pointer"}
                    >
                        Tôi muốn quay lại trang
                    </Typography.Text>
                    <Typography.Text
                        onClick={() => navigate(RouteConfig.LOGIN)}
                        style={{
                            color: "#009B90"
                        }}
                        className={"text-body-text-2 text-weight-strong hover-pointer"}
                    >
                        Đăng nhập
                    </Typography.Text>
                </div>
            </div>
        </div>
    )
}
