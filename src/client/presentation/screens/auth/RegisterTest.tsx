import styles from "../../../styles/module/LoginTest.module.scss";
import {Checkbox, Input, Space, Typography} from "antd";
import MailIcon from "../../components/svgs/MailSvg";
import LockIcon from "../../components/icons/LockIcon";
import {CustomButton} from "../../components/CustomButton";
import {useNavigate} from "react-router";
import {RouteConfig} from "../../../config/RouteConfig";


export const RegisterTest = () => {
    let navigate = useNavigate()

    return (
        <div className={"flex justify-end"}>
            <div
                className={styles.Login}
            >
                <div className={"flex flex-col"}>
                    <Typography.Title
                        level={2}
                        className={"text-title-2 text-weight-strong"}
                    >
                        Đăng ký
                    </Typography.Title>
                    <Typography.Text className={"text-body-text-2"}>
                        Đăng ký một lần sử dụng mãi mãi !
                    </Typography.Text>
                </div>
                <Space
                    style={{display: 'flex'}}
                    direction={"vertical"}
                    size={"middle"}
                >
                    <Input className={"input-main"} placeholder="Email" prefix={<MailIcon/>}/>
                    <Input className={"input-main"} placeholder="Số điện thoại" prefix={<MailIcon/>}/>
                    <Input className={"input-main"} placeholder="Mật khẩu" prefix={<LockIcon/>}/>
                </Space>
                <div className={"flex justify-between"}>
                    <Checkbox className={'checkbox-main text-body-text-3'}>Nhớ tài khoản</Checkbox>
                    <Typography.Text
                        onClick={() => navigate(RouteConfig.FORGOT_PASSWORD)}
                        className={"text-body-text-3 hover-pointer"}
                    >
                        Quên mật khẩu?
                    </Typography.Text>
                </div>
                <CustomButton fullWidth>
                    Tiếp tục
                </CustomButton>
                <div className={"flex justify-center"}>
                    <Typography.Text
                        onClick={() => navigate(RouteConfig.LOGIN)}
                        className={"text-body-text-2 pr-3 hover-pointer"}>
                        Bạn đã có tài khoản?
                    </Typography.Text>
                    <Typography.Text
                        onClick={() => navigate(RouteConfig.LOGIN)}
                        style={{
                            color: "#009B90"
                        }}
                        className={"text-body-text-2 text-weight-strong hover-pointer"}
                    >
                        Đăng nhập
                    </Typography.Text>
                </div>
            </div>
        </div>
    )
}
