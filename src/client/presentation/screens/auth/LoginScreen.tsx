import {Checkbox, Form, Input, Typography} from "antd";
import styles from "../../../styles/module/LoginTest.module.scss";
import LockIcon from "../../components/icons/LockIcon";
import {CustomButton} from "../../components/CustomButton";
import {useNavigate} from "react-router";
import {RouteConfig} from "../../../config/RouteConfig";
import {useTranslation} from "react-i18next";
import {useSessionContext} from "../../contexts/SessionContext";
import {LoginAction} from "../../../recoil/auth/login/LoginAction";
import React, {useEffect, useLayoutEffect, useState} from "react";
import {Color} from "../../../const/Color";
import {SendingStatus} from "../../../const/Events";
import {FieldData, ValidateErrorEntity} from "rc-field-form/lib/interface";
import {Navigate} from "react-router-dom";
import {ErrorItemFC} from "../../widgets/ErrorItemFC";
import {EyeInvisibleOutlined, EyeTwoTone} from "@ant-design/icons";
import NextImage from "next/image";
import {Images} from "../../../const/Images";

type TFormFinish = {
    username: string
    password: string
}

export const LoginScreen = () => {
    const navigate = useNavigate()
    const {t} = useTranslation()
    const [session] = useSessionContext()

    const {
        vm,
        onLogIn,
        onClearState
    } = LoginAction()

    useEffect(() => {
        console.log('%cMount Screen: LoginScreen', Color.ConsoleInfo)

        return () => {
            onClearState()

            console.log('%cUnmount Screen: LoginScreen', Color.ConsoleInfo)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useLayoutEffect(() => {
        if (vm.status === SendingStatus.success) {
            navigate(session.redirectPath)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.status])

    const onFinish = (values: TFormFinish) => {
        onLogIn({
            username: values.username,
            password: values.password,
        })
    }

    const isLoading = vm.status === SendingStatus.loading
    const isError = vm.status === SendingStatus.error

    const [clientUsernameError, setClientUsernameError] = useState('')
    const [clientPasswordError, setClientPasswordError] = useState('')

    const onFinishFailed = (errorInfo: ValidateErrorEntity) => {
        errorInfo.errorFields.forEach((e) => {
            if (e.name[0] === "username") {
                setClientUsernameError(e.errors[0])
            }

            if (e.name[0] === "password") {
                setClientPasswordError(e.errors[0])
            }
        })
    }

    const onFieldsChange = (data: FieldData[]) => {
        data.forEach((e: any) => {
            if (e.name[0] === "username") {
                setClientUsernameError(e.errors.length > 0 ? e.errors[0] : '')
            }

            if (e.name[0] === "password") {
                setClientPasswordError(e.errors.length > 0 ? e.errors[0] : '')
            }
        })
    }

    useEffect(() => {
        if (vm.status === SendingStatus.error) {
            setClientUsernameError('')
            setClientPasswordError('')
        }
    }, [vm.status])

    // redirect if logged
    if (session.isAuthenticated) {
        return <Navigate to={RouteConfig.HOME}/>
    }

    return (
        <ErrorItemFC
            status={vm.status}
            typeView={'modal'}
        >
            <Form
                className={"w-full"}
                name={"login"}
                initialValues={{
                    remember: true
                }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                onFieldsChange={onFieldsChange}
            >
                <div className={"flex justify-end"}>
                    <div
                        className={styles.Login}
                    >
                        <div className={"flex flex-col w-full"}>
                            <Typography.Title
                                level={2}
                                className={"text-title-2 text-weight-strong"}
                            >
                                {t("text.login")}
                            </Typography.Title>
                            <Typography.Text className={"text-body-text-2"}>
                                {t("text.login")}&nbsp;
                                <span className={"text-weight-strong"}>Auto Timelapse</span>
                                &nbsp;{t("text.toExperience")}!
                            </Typography.Text>
                        </div>
                        <div>
                            {/*<div className={"grid grid-cols-2 gap-3.5"}>*/}
                            {/*    <div className={styles.SocialBox}>*/}
                            {/*        <Space align="center">*/}
                            {/*            <div style={{*/}
                            {/*                width: "17px",*/}
                            {/*                height: "16px",*/}
                            {/*            }}>*/}
                            {/*                <SvgFaceBook/>*/}
                            {/*            </div>*/}
                            {/*            <span>Facebook</span>*/}
                            {/*        </Space>*/}
                            {/*    </div>*/}
                            {/*    <div className={styles.SocialBox}>*/}
                            {/*        <Space*/}
                            {/*            align="center"*/}
                            {/*        >*/}
                            {/*            <div*/}
                            {/*                style={{*/}
                            {/*                    width: "16px",*/}
                            {/*                    height: "16px",*/}
                            {/*                }}*/}
                            {/*            >*/}
                            {/*                <SvgGoogle/>*/}
                            {/*            </div>*/}
                            {/*            <span>Google</span>*/}
                            {/*        </Space>*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                            {/*<Divider*/}
                            {/*    className={styles.FixDivider}*/}
                            {/*    style={{*/}
                            {/*        margin: " 0px"*/}
                            {/*    }}*/}
                            {/*    plain*/}
                            {/*>*/}
                            {/*    {t("text.or")}*/}
                            {/*</Divider>*/}
                            <div className={"flex flex-col gap-4"}>
                                <Form.Item
                                    style={{
                                        marginBottom: "0px"
                                    }}
                                    key={'username'}
                                    name={"username"}
                                    className={"form-item-main"}
                                    rules={[
                                        {
                                            required: true,
                                            message: t('error.usernameRequired')
                                        },
                                        {
                                            min: 3,
                                            message: t('error.usernameMin')
                                        },
                                        {
                                            max: 30,
                                            message: t('error.usernameMax')
                                        }
                                    ]}
                                    validateStatus={(isError && vm.error && vm.error.hasOwnProperty('username')) || clientUsernameError.length > 0 ? 'error' : ''}
                                    help={clientUsernameError.length > 0 ? clientUsernameError : (isError && vm.error && vm.error.hasOwnProperty('username') ? vm.error.username : null)}
                                >
                                    <Input
                                        className={"input-main"}
                                        placeholder={t("text.username")}
                                        prefix={
                                            <NextImage
                                                className={"atl-icon atl-icon-color-BDBDBD"}
                                                src={Images.iconUser}
                                                width={24}
                                                height={24}
                                            />
                                        }
                                        autoCorrect={"off"}
                                        autoCapitalize={"none"}
                                    />
                                </Form.Item>
                                <Form.Item
                                    style={{
                                        marginBottom: "0px"
                                    }}
                                    className={"form-item-main"}
                                    key={'password'}
                                    name={"password"}
                                    rules={[
                                        {
                                            required: true,
                                            message: t('error.passwordRequired')
                                        },
                                        {
                                            min: 6,
                                            message: t('error.passwordMin')
                                        },
                                        {
                                            max: 30,
                                            message: t('error.passwordMax')
                                        },
                                    ]}
                                    validateStatus={(isError && vm.error && vm.error.hasOwnProperty('password')) || clientPasswordError.length > 0 ? 'error' : ''}
                                    help={clientPasswordError.length > 0 ? clientPasswordError : (isError && vm.error && vm.error.hasOwnProperty('password') ? vm.error.password : null)}
                                >
                                    <Input.Password
                                        style={{
                                            textAlign: "start",
                                        }}
                                        className={"input-main"}
                                        placeholder={t('label.password')}
                                        iconRender={visible => (visible ? <EyeTwoTone/> : <EyeInvisibleOutlined/>)}
                                        prefix={<LockIcon/>}
                                        autoCorrect={"off"}
                                        autoCapitalize={"none"}
                                    />
                                </Form.Item>
                            </div>
                        </div>
                        <div className={"flex justify-between"}>
                            <Form.Item name="remember" valuePropName="checked" noStyle>
                                <Checkbox className={'checkbox-main text-body-text-3'}>
                                    {t("text.rememberMe")}
                                </Checkbox>
                            </Form.Item>
                            {/*<Typography.Text*/}
                            {/*    onClick={() => navigate(RouteConfig.FORGOT_PASSWORD)}*/}
                            {/*    className={"text-body-text-3 hover-pointer"}*/}
                            {/*>*/}
                            {/*    {t("text.forgotPassword")}?*/}
                            {/*</Typography.Text>*/}
                        </div>

                        <Form.Item
                            className={"fix-ant-form-item"}
                            key={"action"}
                        >
                            <CustomButton
                                htmlType="submit"
                                fullWidth
                                loading={isLoading}
                            >
                                {t("button.login")}
                            </CustomButton>
                        </Form.Item>
                        {/*<div className={"flex justify-center"}>*/}
                        {/*    <Typography.Text*/}
                        {/*        onClick={() => navigate(RouteConfig.REGISTER)}*/}
                        {/*        className={"text-body-text-2 pr-3 hover-pointer"}*/}
                        {/*    >*/}
                        {/*        Bạn chưa có tài khoản?*/}
                        {/*    </Typography.Text>*/}
                        {/*    <Typography.Text*/}
                        {/*        onClick={() => navigate(RouteConfig.REGISTER)}*/}
                        {/*        style={{*/}
                        {/*            color: "#009B90"*/}
                        {/*        }}*/}
                        {/*        className={"text-body-text-2 text-weight-strong hover-pointer"}*/}
                        {/*    >*/}
                        {/*        {t("button.register")}*/}
                        {/*    </Typography.Text>*/}
                        {/*</div>*/}
                    </div>
                </div>
            </Form>
        </ErrorItemFC>
    )
}
