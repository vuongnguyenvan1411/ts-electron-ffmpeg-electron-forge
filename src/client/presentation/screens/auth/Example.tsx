import {CustomButton} from "../../components/CustomButton";
import {Checkbox, DatePicker, Divider, Input, Modal, Select, Space, Tag, Typography} from "antd";
import MailIcon from "../../components/svgs/MailSvg";
import LockIcon from "../../components/icons/LockIcon";
import {CustomTypography} from "../../components/CustomTypography";
import React, {useState} from "react";
import {Images} from "../../../const/Images";
import Image from "next/image";

export const Example = () => {
    const [showModal, setShowModal] = useState(false);

    const onShowModal = (value: boolean) => {
        setShowModal(value);
    }


    return (
        <div className={"flex flex-col justify-center p-12"}>
            {/* Input */}
            <div className={"flex flex-col"}>
                <CustomTypography
                    isStrong
                    textStyle={"text-title-2"}
                >
                    02. <span style={{
                    color: "#009B90",
                }}>Typography | Open Sans</span>
                </CustomTypography>
                <div className={"grid grid-cols-6 gap-6 pt-8"}>
                    <div className={"col-span-2"}>
                        <CustomTypography
                            textStyle={"text-title-3"}
                            isStrong
                            style={{
                                color: "#828282"
                            }}
                        >
                            Name
                        </CustomTypography>
                    </div>
                    <div className={"col-span-4"}/>
                    <div className={"col-span-2"}>
                        <CustomTypography textStyle={"text-title-1"} isStrong style={{
                            color: "#4F4F4F"
                        }}>
                            Title 1
                        </CustomTypography>
                    </div>
                    <div className={"col-span-2"}>
                        <CustomTypography textStyle={"text-title-1"} isStrong style={{
                            color: "#4F4F4F"
                        }}>
                            56 px
                        </CustomTypography>
                    </div>
                    <div className={"col-span-2"}/>
                    <div className={"col-span-2"}>
                        <CustomTypography textStyle={"text-title-2"} isStrong style={{
                            color: "#4F4F4F"
                        }}>
                            Title 2
                        </CustomTypography>
                    </div>
                    <div className={"col-span-2"}>
                        <CustomTypography textStyle={"text-title-2"} isStrong style={{
                            color: "#4F4F4F"
                        }}>
                            40 px
                        </CustomTypography>
                    </div>
                    <div className={"col-span-2"}>
                        <CustomTypography textStyle={"text-title-2"} style={{
                            color: "#4F4F4F"
                        }}>
                            40 px
                        </CustomTypography>
                    </div>
                    <div className={"col-span-2"}>
                        <CustomTypography textStyle={"text-title-3"} isStrong style={{
                            color: "#4F4F4F"
                        }}>
                            Title 3
                        </CustomTypography>
                    </div>
                    <div className={"col-span-2"}>
                        <CustomTypography textStyle={"text-title-3"} isStrong style={{
                            color: "#4F4F4F"
                        }}>
                            24 px
                        </CustomTypography>
                    </div>
                    <div className={"col-span-2"}>
                        <Typography.Title level={3} className={"text-title-3"} style={{
                            color: "#4F4F4F"
                        }}>
                            24 px
                        </Typography.Title>
                    </div>

                    <div className={"col-span-2"}>
                        <Typography.Text className={"text-button text-weight-strong"} style={{
                            color: "#4F4F4F"
                        }}>
                            Button
                        </Typography.Text>
                    </div>
                    <div className={"col-span-2"}>
                        <Typography.Text className={"text-button text-weight-strong"} style={{
                            color: "#4F4F4F"
                        }}>
                            18 px
                        </Typography.Text>
                    </div>
                    <div className={"col-span-2"}/>

                    <div className={"col-span-2"}>
                        <Typography.Text className={"text-body-text-1"} style={{
                            color: "#4F4F4F"
                        }}>
                            Body Text 1
                        </Typography.Text>
                    </div>
                    <div className={"col-span-2"}/>
                    <div className={"col-span-2"}>
                        <Typography.Title level={2} className={"text-body-text-1"} style={{
                            color: "#4F4F4F"
                        }}>
                            20 px
                        </Typography.Title>
                    </div>

                    <div className={"col-span-2"}>
                        <Typography.Text className={"text-body-text-2 text-weight-strong"} style={{
                            color: "#4F4F4F"
                        }}>
                            Body Text 2
                        </Typography.Text>
                    </div>
                    <div className={"col-span-2"}>
                        <Typography.Text className={"text-body-text-2 text-weight-strong"} style={{
                            color: "#4F4F4F"
                        }}>
                            16 px
                        </Typography.Text>
                    </div>
                    <div className={"col-span-2"}>
                        <Typography.Text className={"text-body-text-2"} style={{
                            color: "#4F4F4F"
                        }}>
                            16 px
                        </Typography.Text>
                    </div>

                    <div className={"col-span-2"}>
                        <Typography.Text className={"text-body-text-3"} style={{
                            color: "#4F4F4F"
                        }}>
                            Body Text 3
                        </Typography.Text>
                    </div>
                    <div className={"col-span-2"}/>
                    <div className={"col-span-2"}>
                        <Typography.Text className={"text-body-text-3"} style={{
                            color: "#4F4F4F"
                        }}>
                            14 px
                        </Typography.Text>
                    </div>

                    <div className={"col-span-2"}>
                        <Typography.Title level={2} className={"text-info text-weight-strong"} style={{
                            color: "#4F4F4F"
                        }}>
                            Info Text
                        </Typography.Title>
                    </div>
                    <div className={"col-span-2"}>
                        <Typography.Text className={"text-info text-weight-strong"} style={{
                            color: "#4F4F4F"
                        }}>
                            12 px
                        </Typography.Text>
                    </div>
                    <div className={"col-span-2"}/>
                </div>
            </div>
            {/* Button */}
            <div className={"flex flex-col pt-8"}>
                <Typography.Title level={2} className={"text-title-2 text-weight-strong"} style={{
                    color: "#4F4F4F"
                }}>
                    03. <span style={{
                    color: "#009B90",
                }}>
                    Button
                </span>
                </Typography.Title>

                <div className={"grid grid-cols-6 gap-4 pt-8"}>
                    {/* SMALL BUTTON */}
                    <div className={"flex flex-col"}>
                        <Space direction={"vertical"}>
                            <span className={"text-title-2-semi-bold"}>Small</span>
                            <span>Vertical padding 10px</span>
                            <span>Horizontal padding 24px</span>
                            <span>Font size 14px</span>
                        </Space>
                    </div>
                    <div className={"col-span-5"}>
                        <Space>
                            <div className={"flex flex-col"}>
                                <span>Small</span>
                                <CustomButton size={"small"}>
                                    Đăng nhập
                                </CustomButton>
                            </div>
                            <div className={"flex flex-col"}>
                                <span>Small</span>
                                <CustomButton size={"small"} type={"outline"}>
                                    Đăng nhập
                                </CustomButton>
                            </div>
                            <div className={"flex flex-col"}>
                                <span>Small</span>
                                <CustomButton size={"small"} disabled>
                                    Đăng nhập
                                </CustomButton>
                            </div>
                            <div className={"flex flex-col"}>
                                <span>Small</span>
                                <CustomButton size={"small"} type={"outline"} disabled>
                                    Đăng nhập
                                </CustomButton>
                            </div>
                        </Space>
                    </div>

                    {/* NORMAL BUTTON */}
                    <div className={"flex flex-col"}>
                        <Space direction={"vertical"}>
                            <span className={"text-title-2-semi-bold"}>Normal</span>
                            <span>Vertical padding 12px</span>
                            <span>Horizontal padding 24px</span>
                            <span>Font size 18px</span>
                        </Space>
                    </div>
                    <div className={"col-span-5"}>
                        <Space>
                            <div className={"flex flex-col"}>
                                <span>Normal</span>
                                <CustomButton size={"normal"}>
                                    Đăng nhập
                                </CustomButton>
                            </div>
                            <div className={"flex flex-col"}>
                                <span>Normal</span>
                                <CustomButton size={"normal"} type={"outline"}>
                                    Đăng nhập
                                </CustomButton>
                            </div>
                            <div className={"flex flex-col"}>
                                <span>Normal</span>
                                <CustomButton size={"normal"} disabled>
                                    Đăng nhập
                                </CustomButton>
                            </div>
                            <div className={"flex flex-col"}>
                                <span>Normal</span>
                                <CustomButton size={"normal"} type={"outline"} disabled>
                                    Đăng nhập
                                </CustomButton>
                            </div>
                        </Space>
                    </div>

                    {/* LARGE BUTTON */}
                    <div className={"flex flex-col"}>
                        <Space direction={"vertical"}>
                            <span className={"text-title-2-semi-bold"}>Large</span>
                            <span>Vertical padding 16px</span>
                            <span>Horizontal padding 38px</span>
                            <span>Font size 18px</span>
                        </Space>
                    </div>
                    <div className={"col-span-5"}>
                        <Space>
                            <div className={"flex flex-col"}>
                                <span>Large</span>
                                <CustomButton size={"large"}>
                                    Đăng nhập
                                </CustomButton>
                            </div>
                            <div className={"flex flex-col"}>
                                <span>Large</span>
                                <CustomButton size={"large"} type={"outline"}>
                                    Đăng nhập
                                </CustomButton>
                            </div>
                            <div className={"flex flex-col"}>
                                <span>Large</span>
                                <CustomButton size={"large"} disabled>
                                    Đăng nhập
                                </CustomButton>
                            </div>
                            <div className={"flex flex-col"}>
                                <span>Large</span>
                                <CustomButton size={"large"} type={"outline"} disabled>
                                    Đăng nhập
                                </CustomButton>
                            </div>
                        </Space>
                    </div>

                    {/* FULLWIDTH BUTTON */}
                    <div className={"flex flex-col"}>
                        <Space direction={"vertical"}>
                            <span className={"text-title-2-semi-bold"}>FullWidth</span>
                            <span>Vertical padding 10px</span>
                            <span>Horizontal padding 24px</span>
                            <span>Font size 14px</span>
                        </Space>
                    </div>
                    <div className={"col-span-5"}>
                        <Space direction={"vertical"} style={{
                            display: "flex"
                        }}>
                            <div className={"flex flex-col"}>
                                <span>FullWidth: Small type</span>
                                <CustomButton size={"small"} fullWidth>
                                    Đăng nhập
                                </CustomButton>
                            </div>
                            <div className={"flex flex-col"}>
                                <span>FullWidth: Small type</span>
                                <CustomButton size={"small"} type={"outline"}>
                                    Đăng nhập
                                </CustomButton>
                            </div>
                            <div className={"flex flex-col"}>
                                <span>FullWidth: Small type</span>
                                <CustomButton size={"small"} disabled>
                                    Đăng nhập
                                </CustomButton>
                            </div>
                        </Space>
                    </div>
                </div>
            </div>
            {/* Input*/}
            <div className={"flex flex-col pt-8"}>
                <Typography.Title level={2} className={"text-title-2 text-weight-strong"} style={{
                    color: "#4F4F4F"
                }}>
                    04. <span style={{
                    color: "#009B90",
                }}>
                    Input
                </span>
                </Typography.Title>
                <Space
                    style={{display: 'flex'}}
                    direction={"vertical"}
                    size={"middle"}
                >
                    <Input className={"input-main"} placeholder="Email" prefix={<MailIcon/>}/>
                    <Input className={"input-main"} placeholder="Mật khẩu" prefix={<LockIcon/>}/>
                    <Input className={"input-main input-bg-white input-h-40 input-radius-8 input-border-half"} placeholder="Fix height and custom Input" prefix={<LockIcon/>}/>
                </Space>
            </div>
            {/* Check Box*/}
            <div className={"flex flex-col pt-8"}>
                <Typography.Title level={2} className={"text-title-2 text-weight-strong"} style={{
                    color: "#4F4F4F"
                }}>
                    05. <span style={{
                    color: "#009B90",
                }}>
                    Checkbox
                </span>
                </Typography.Title>
                <Checkbox className={'checkbox-main'}>Nhớ tài khoản</Checkbox>
            </div>
            {/* Check Box*/}
            <div className={"flex flex-col pt-8 gap-4"}>
                <CustomTypography textStyle={"text-title-2"} isStrong style={{
                    color: "#4F4F4F"
                }}>
                    06. <span style={{
                    color: "#009B90",
                }}>
                    Tags
                </span>
                </CustomTypography>
                <div className={"flex"}>
                    <Tag
                        closable
                        className={"tag-main"}
                    >
                        <CustomTypography
                            isStrong
                            textStyle={"text-body-text-3"}
                        >
                            ddd
                        </CustomTypography>
                        :   &nbsp;
                        <CustomTypography
                            textStyle={"text-body-text-3"}
                        >
                            ddd
                        </CustomTypography>
                    </Tag>
                    <Tag
                        className={"tag-main tag-no-icon"}
                    >
                        <CustomTypography
                            isStrong
                            textStyle={"text-body-text-3"}
                        >
                            ddd
                        </CustomTypography>
                        :   &nbsp;
                        <CustomTypography
                            textStyle={"text-body-text-3"}
                        >
                            ddd
                        </CustomTypography>
                    </Tag>
                </div>
            </div>
            {/* Select */}
            <div className={"flex flex-col pt-8 gap-4"}>
                <CustomTypography textStyle={"text-title-2"} isStrong style={{
                    color: "#4F4F4F"
                }}>
                    07. <span style={{
                    color: "#009B90",
                }}>
                    Select
                </span>
                </CustomTypography>
                <Select
                    className={"select-main"}
                    bordered={false}
                    value={"loading"}
                >
                    <Select.Option
                        key="loading"
                        value={"loading"}
                    >
                        Loading...
                    </Select.Option>
                    <Select.Option
                        key="loaded"
                        value={"loaded"}
                    >
                        Loaded...
                    </Select.Option>
                </Select>
            </div>
            {/* Divider */}
            <div className={"flex flex-col pt-8 gap-4"}>
                <CustomTypography textStyle={"text-title-2"} isStrong style={{
                    color: "#4F4F4F"
                }}>
                    08. <span style={{
                    color: "#009B90",
                }}>
                    Divider
                </span>
                </CustomTypography>
                <Divider className={"divider-main divider-horizontal-m-0"}/>
            </div>
            {/* Ranger picker */}
            <div className={"flex flex-col pt-8 gap-4"}>
                <CustomTypography textStyle={"text-title-2"} isStrong style={{
                    color: "#4F4F4F"
                }}>
                    09. <span style={{
                    color: "#009B90",
                }}>
                    Ranger picker
                </span>
                </CustomTypography>
                <DatePicker.RangePicker
                    className={"ranger-picker-main ranger-picker-border ranger-picker-radius w-full"}
                    allowEmpty={[true, true]}
                    showTime
                />
            </div>
            {/* Using svg */}
            <div className={"flex flex-col pt-8 gap-4"}>
                <CustomTypography textStyle={"text-title-2"} isStrong style={{
                    color: "#4F4F4F"
                }}>
                    10. <span style={{
                    color: "#009B90",
                }}>
                    Using svgs
                </span>
                </CustomTypography>
                <div className={"flex flex-row gap-3"}>
                    <div className={"flex flex-col gap-2"}>
                        <CustomTypography>
                            Default size:
                        </CustomTypography>
                        <div className={"bg-black h-24 w-24 flex items-center justify-center"}>
                            <Image
                                alt={Images.iconHome.atl}
                                src={Images.iconHome.data}
                            />
                        </div>
                    </div>
                    <div className={"flex flex-col gap-2"}>
                        <CustomTypography>
                            Custom size
                        </CustomTypography>
                        <div className={"bg-black p-2 h-24 w-24 flex items-center justify-center"}>
                            <Image
                                width={100}
                                height={100}
                                alt={Images.iconHome.atl}
                                src={Images.iconHome.data}
                            />
                        </div>
                    </div>
                </div>
            </div>
            {/* Using Model */}
            <div className={"flex flex-col pt-8 gap-4"}>
                <CustomTypography textStyle={"text-title-2"} isStrong style={{
                    color: "#4F4F4F"
                }}>
                    11. <span style={{
                    color: "#009B90",
                }}>
                    Using Modal
                </span>
                </CustomTypography>
                <CustomButton onClick={() => onShowModal(true)}> show modal</CustomButton>
                <Modal
                    className={"modal-main"}
                    title={"test"}
                    visible={showModal}
                    onCancel={() => onShowModal(false)}
                >
                    ddd
                </Modal>
            </div>
        </div>
    )
}
