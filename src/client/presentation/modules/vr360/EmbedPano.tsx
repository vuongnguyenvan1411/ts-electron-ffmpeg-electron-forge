import {memo, useEffect} from "react";
import {Color} from "../../../const/Color";
import {Utils} from "../../../core/Utils";

const EmbedPano = (props: {
    vr360Id: number,
    name: string;
    scene?: number;
    createdAt?: number;
    bodyRef?: HTMLElement;
}) => {
    useEffect(() => {
        console.log('%cMount Screen: EmbedPano', Color.ConsoleInfo);

        const divPano = document.createElement('div');
        divPano.id = "pano";
        divPano.style.width = "100%";
        divPano.style.height = "100%";

        if (props.bodyRef) {
            props.bodyRef.appendChild(divPano);
        } else {
            document.body.appendChild(divPano);
        }

        const vS = 1;
        const scriptFile = document.createElement('script');
        scriptFile.src = `${Utils.getLocalAsset('vr360/tour.js')}?v=${vS}`;

        if (props.scene) {
            console.log(`View scene: ${props.scene}`)
        }

        const scriptRun = document.createElement('script');
        scriptRun.innerHTML = `
                embedpano({
                    swf                : "${Utils.getLocalAsset('vr360/tour.swf')}?v=${vS}",
                    xml                : "${Utils.cdnGdtAssetRaw(`vr/${props.vr360Id}/tour.xml?v=${props.createdAt ?? 0}`)}",
                    vars               : {startscene:"${props.scene ?? 0}"},
                    target             : "pano",
                    html5              : "auto",
                    mobilescale        : 1.0,
                    passQueryParameters: true,
                    littleplanetintro  : true,
                });
            `;

        document.body.style.height = "100%";
        document.body.appendChild(scriptFile);

        scriptFile.onload = () => document.body.appendChild(scriptRun);

        return () => {
            if (props.bodyRef) {
                props.bodyRef.removeChild(divPano);
            } else {
                document.body.removeChild(divPano);
            }
            document.body.removeChild(scriptFile);
            document.body.removeChild(scriptRun);
            document.body.style.removeProperty('height');

            const tag = document.querySelector('script[src*="maps.google.com/maps/api"]');

            if (tag !== null) {
                document.body.removeChild(tag);
            }

            console.log('%cUnmount Screen: EmbedPano', Color.ConsoleInfo);
        };

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.vr360Id]);

    return null;
}

export default memo(EmbedPano);
