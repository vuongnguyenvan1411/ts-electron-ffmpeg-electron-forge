import {Stats} from '@probe.gl/stats';
import {Tileset3D} from "@loaders.gl/tiles";

export function initStats(url: string) {
    return new Stats({
        id: url
    })
}

export function sumTilesetsStats(tilesets: Tileset3D[], stats: Stats) {
    stats.reset();

    for (const tileset of tilesets) {
        tileset.stats.forEach((stat) => {
            stats.get(stat.name).addCount(stat.count);
        });
    }
}
