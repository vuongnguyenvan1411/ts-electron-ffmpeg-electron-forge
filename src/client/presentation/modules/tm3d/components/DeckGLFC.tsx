import {Deck, MapView, PostProcessEffect, RGBAColor, WebMercatorViewport} from "@deck.gl/core";
import {vibrance} from "@luma.gl/shadertools";
import {LayerProps} from "@deck.gl/core/lib/layer";
import GL from "@luma.gl/constants";
import StaticMap, {MapContext, TransitionInterpolator} from "react-map-gl";
import DeckGL from "@deck.gl/react";
import React, {useCallback, useEffect, useRef, useState} from "react";
import {Viewer} from "../viewer/Viewer";
import {ViewStateProps} from "@deck.gl/core/lib/deck";
import {App, FEATURE_COLORS} from "../const/App";
import {IBaseLayer} from "../const/Types";
import {EditableGeoJsonLayer, ElevatedEditHandleLayer} from "@nebula.gl/layers";
import {GeoJsonEditMode, RotateMode, TranslateMode, ViewMode} from "@nebula.gl/edit-modes";
import {TranslateScene} from "./TranslateScene";
import {RotateScene} from "./RotateScene";
import {TFeature} from "../viewer/clim/CSSceneLayers";
import {DeckEventName} from "../const/Events";
import EventEmitter from "eventemitter3";
import {KeyEventName} from "../../pc3d/event/EventNames";
import {circle} from "@turf/turf";
import {Map3DModel} from "../../../../models/service/geodetic/Map3DModel";
import {EMenuControl} from "../const/Defines";
import {Layer} from "deck.gl";
import {isMobile} from "react-device-detect";

export const DeckGLFC = (props: {
    viewer: Viewer
    mapLayers: Layer<any, LayerProps<any>>[],
    event: EventEmitter
    viewState: ViewStateProps
    onViewStateChange: (args: any) => void,
    item: Map3DModel,
}) => {
    const deckRef = useRef<Deck>()
    const currentViewportRef = useRef<WebMercatorViewport>()
    const webglContextRef = useRef<WebGLRenderingContext>()

    const [baseLayer, setBaseLayer] = useState<IBaseLayer>({
        key: props.viewer.baseLayers.keyLayer,
        visible: props.viewer.baseLayers.visible,
    });
    const [geoJsonVisible, setGeoJsonVisible] = useState(false)
    const [features, setFeatures] = React.useState<TFeature[]>([])
    const [mode, setMode] = useState<GeoJsonEditMode>(new ViewMode())
    const [modeConfig, setModeConfig] = useState({})
    const [selectedFeatureIndexes, setSelectedFeatureIndexes] = useState<number[]>([])
    const [controller, setController] = useState(App.INITIAL_CONTROLLER)

    const geoJson = {
        type: "FeatureCollection",
        features: features
    }

    useEffect(() => {
        document.addEventListener(KeyEventName.KeyDown, eventESCRemove)

        props.event.on(DeckEventName.BaseChanged, _onChangeBaseLayer)
        props.event.on(DeckEventName.FeatureChanged, _onChangeGeoJsonLayer)
        props.event.on(DeckEventName.FeatureSelected, _onSelectedFeature)
        props.event.on(DeckEventName.FeatureMode, _onChangeModeEdit)
        props.event.on(DeckEventName.ControllerChange, _onChangeController)

        if (props.item.info?.tm?.config?.obj) {
            setFeatures(props.item.info && props.item.info.tm.config.obj.map((item, index) => {
                const center = [item.lng, item.lat, item.altitude]

                const drawCircle = circle(center, 30, {
                    steps: 10,
                    units: 'meters',
                })

                const coords = drawCircle.geometry.coordinates[0]

                for (let i = 0; i < coords.length - 1; i++) {
                    coords[i].push(item.altitude)
                }

                return {
                    geometry: drawCircle.geometry,
                    type: "Feature",
                    properties: {
                        id: `scene-${index}`,
                        altitude: item.altitude,
                    }
                } as TFeature
            }))
        }

        return () => {
            props.event.off(DeckEventName.BaseChanged, _onChangeBaseLayer)
            props.event.off(DeckEventName.FeatureChanged, _onChangeGeoJsonLayer)
            props.event.off(DeckEventName.FeatureSelected, _onSelectedFeature)
            props.event.off(DeckEventName.FeatureMode, _onChangeModeEdit)
            props.event.off(DeckEventName.ControllerChange, _onChangeController)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const _onChangeModeEdit = (agrs: {
        mode: GeoJsonEditMode
        modeConfig: {}
    }) => {
        setMode(agrs.mode)
        setModeConfig(modeConfig)
    }

    const _onChangeController = (type: EMenuControl) => {
        switch (type) {
            case EMenuControl.ByPoint:
                setController({
                    ...controller,
                    dragMode: "pan",

                })

                break;
            case EMenuControl.Normal:
                setController({
                    ...controller,
                    dragMode: "rotate",
                })

                break;
        }
    }

    const _onSelectedFeature = (selected: number[]) => setSelectedFeatureIndexes(selected)

    const _onChangeGeoJsonLayer = (visible: boolean) => setGeoJsonVisible(visible)

    const _onChangeBaseLayer = (evt: IBaseLayer) => {
        if (evt.visible) {
            setBaseLayer({
                key: evt.key,
                visible: true
            })
        } else {
            setBaseLayer({visible: false})
        }
    }

    const eventESCRemove = (evt: KeyboardEvent) => {
        if (evt.code.toLowerCase() === "escape") {
            evt.preventDefault()

            _onChangeModeEdit({
                mode: new ViewMode(),
                modeConfig: {}
            })
            setGeoJsonVisible(false)
            setSelectedFeatureIndexes([])
        }
    }

    //trigger viewport transitions after this event
    const onLoadDeck = useCallback(() => {
        props.onViewStateChange({
            ...props.viewState,
            bearing: props.viewState.bearing ? props.viewState.bearing + 45 : 45,
            transitionDuration: 1000,
            transitionInterpolator: new TransitionInterpolator,
            onTransitionEnd: onLoadDeck
        })
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const _initMapView = () => {
        const mapCanvas: HTMLCanvasElement = document.getElementById('mapCanvas') as HTMLCanvasElement;
        mapCanvas.style.width = "100%";
        mapCanvas.style.height = "90vh";

        const contextAttributes = {
            stencil: false,
            depth: true,
            antialias: true,
            failIfMajorPerformanceCaveat: false,
            premultipliedAlpha: false,
            desynchronized: true,
            preserveDrawingBuffer: false,
            alpha: false,
            powerPreference: isMobile ? 'default' : 'high-performance',
            xrCompatible: true,
            webgl1: false,
            throwOnError: true,
            willReadFrequently: true,
        };
        const webglContext = mapCanvas.getContext('webgl', contextAttributes);

        if (webglContext) {
            webglContextRef.current = webglContext as WebGL2RenderingContext;
        }
    }

    const _onWebGLInitialized = () => {
        console.log("onWebGLInitialized");

        props.event.emit(DeckEventName.WebGLInitialized)

        //For testing of passing deck's context to harp
        _initMapView();
    }

    const _getLineColor = (feature, isSelected) => {
        const index = features.indexOf(feature);

        const getDeckColorForFeature = (index: number, bright: number, alpha: number): RGBAColor => {
            const length = FEATURE_COLORS.length;
            const color = FEATURE_COLORS[index % length].map((c) => c * bright * 255);

            // @ts-ignore
            return [...color, alpha * 255];
        }
        return isSelected
            ? getDeckColorForFeature(index, 1.0, 1.0)
            : getDeckColorForFeature(index, 0.5, 1.0);
    };

    const editableLayer = new EditableGeoJsonLayer({
        id: "geoJson",
        data: geoJson,
        mode: mode,
        positionFormat: 'XYZ',
        selectedFeatureIndexes: selectedFeatureIndexes,
        modeConfig: modeConfig,
        autoHighlight: false,
        pointRadiusMinPixels: 1,
        pickable: true,
        parameters: {
            depthTest: true,
            depthMask: false,
            blend: true,
            blendEquation: GL.FUNC_ADD,
            blendFunc: [GL.SRC_ALPHA, GL.ONE_MINUS_SRC_ALPHA],
        },
        editHandlePointRadiusScale: 1,
        _subLayerProps: {
            tooltips: {
                getColor: [255, 255, 255, 255],
            },
            points: {
                type: ElevatedEditHandleLayer,
                getFillColor: [0, 255, 0],
            },
        },
        updateTriggers: {
            data: features
        },
        visible: geoJsonVisible,
        getLineColor: _getLineColor,
        onEdit: ({updatedData}) => {
            if (mode instanceof TranslateMode) {
                TranslateScene({
                    ...props,
                    mode: mode
                })

            } else if (mode instanceof RotateMode) {
                RotateScene({
                    ...props,
                    mode: mode
                })
            }
            setFeatures(updatedData.features)
        },
        onClick: ({index}) => {
            if (selectedFeatureIndexes.includes(index)) {
                setSelectedFeatureIndexes(selectedFeatureIndexes.filter((e) => e !== index))
            } else {
                setSelectedFeatureIndexes([...selectedFeatureIndexes, index])
            }
        },
    })

    return (
        <DeckGL
            ref={ref => {
                if (ref) {
                    deckRef.current = ref.deck
                    props.viewer.setDeck(ref.deck)
                }
            }}
            // onAfterRender={_onAfterRender}
            _typedArrayManagerProps={
                isMobile ? {overAlloc: 1, poolSize: 0} : undefined
            }
            views={[new MapView({
                id: 'main',
                nearZMultiplier: 0.00001,
                farZMultiplier: 4.04,
                controller: true,
            })]}
            effects={[new PostProcessEffect(vibrance, {amount: 0.6})]}
            id={"tm"}
            viewState={props.viewState}
            layers={[...props.mapLayers, editableLayer]}
            controller={controller}
            gl={webglContextRef.current}
            onViewStateChange={props.onViewStateChange}
            onWebGLInitialized={_onWebGLInitialized}
            parameters={{
                depthTest: true,
                blend: false
            }}
            onLoad={onLoadDeck}
            useDevicePixels={1}
            ContextProvider={MapContext.Provider}
            getCursor={editableLayer.getCursor.bind(editableLayer)}
        >
            {({viewport}) => {
                currentViewportRef.current = viewport;
            }}
            {/*Only use @react-map-gl^5.3.11 won't require Mapbox token*/}
            <StaticMap
                reuseMaps
                mapboxApiAccessToken={App.MAPBOX_ACCESS_TOKEN}
                mapStyle={baseLayer.key}
                width="100%"
                height="100%"
                mapOptions={{antialias: false}}
                visible={baseLayer.visible}
                bearing={0}
                attributionControl={false}
                disableTokenWarning
                asyncRender
                preventStyleDiffing
                maxZoom={22}
                minZoom={0}
                {...currentViewportRef.current}
            />
        </DeckGL>
    )
}
