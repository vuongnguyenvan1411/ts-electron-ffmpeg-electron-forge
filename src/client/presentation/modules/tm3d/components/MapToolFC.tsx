import {Menu, message, Space} from "antd";
import sms from "../styles/MapView.module.scss";
import {OptionsMenu} from "../const/OptionsMenu";
import React, {useRef, useState} from "react";
import {EMenuControl, EMenuTool, EMenuZoom, TFormScene, TMbs} from "../const/Defines";
import {LinearInterpolator} from "@deck.gl/core";
import {useTranslation} from "react-i18next";
import {Viewer} from "../viewer/Viewer";
import {MenuInfo} from "rc-menu/lib/interface";
import {UpLoadSceneModalFC} from "./UpLoadSceneModalFC";
import {Utils} from "../../../../core/Utils";
import {TFeature} from "../viewer/clim/CSSceneLayers";
import {ApiResModel} from "../../../../models/ApiResModel";
import {AxiosClient} from "../../../../repositories/AxiosClient";
import {ApiService} from "../../../../repositories/ApiService";
import {Map3DModel} from "../../../../models/service/geodetic/Map3DModel";
import EventEmitter from "eventemitter3";
import {DeckEventName} from "../const/Events";
import {changeViewState} from "./ChangeViewState";
import {Vector3} from "@math.gl/core";

export const MapToolFC = (props: {
    viewer: Viewer,
    mbs: TMbs,
    item: Map3DModel
    m3dId: number,
    event: EventEmitter,
    onModifyScene: (item: { key: EMenuTool, mbs: TMbs }) => void,
    setIsModifyDrawer: any,
}) => {
    const {t} = useTranslation();

    const tmConfigRef = useRef(props.item.info?.tm)
    const tmFormObjRef = useRef(
        props.item.info?.tm?.config?.obj && props.item.info.tm.config.obj.length > 0
            ? props.item.info?.tm.config.obj
            : []
    )

    const [isAddScene, setIsAddScene] = useState<{
        visible: boolean,
        initValue?: TFormScene
    }>();

    const onFinishScene = (value: TFormScene) => {
        const sceneId = `scene-${props.viewer.sceneLayers.children.length + 1}`
        const urlScene = `${Utils.cdnGdtAssetRaw('raw/obj')}/${value.url}`

        const geom: TFeature = {
            geometry: {
                coordinates: [value.lng, value.lat, value.altitude],
                type: 'Point',
            },
            type: "Feature",
            properties: {
                id: sceneId.toString(),
                url: value.url,
                scale: value.scale,
                orientation: [0, 0, 90],
                altitude: value.altitude,
            }
        };

        // setFeatures([...features, geom])
        props.viewer.sceneLayers.setLayer({
            data: geom,
            id: sceneId.toString(),
            url: urlScene,
            name: value.name,
            scale: value.scale,
            orientation: [0, 0, 90]
        })

        tmFormObjRef.current = [
            ...tmFormObjRef.current,
            {
                url: value.url ?? '',
                name: value.name,
                lat: value.lat,
                lng: value.lng,
                altitude: value.altitude,
                scale: value.scale,
                orientation: [0, 0, 90],
            }
        ]

        setIsAddScene({visible: false})
    }

    const onSaveScene = () => {
        const data = {
            config: {
                obj: props.viewer.sceneLayers.children.map(child => {
                    const [lng, lat, altitude] = child.feature.geometry.coordinates;

                    return {
                        altitude: altitude,
                        lat: lat,
                        lng: lng,
                        name: child.title,
                        url: child.feature.properties.url,
                        scale: child.feature.properties.scale,
                        orientation: child.feature.properties.orientation,
                    } as TFormScene
                })
            }
        }

        const onSuccess = (r: ApiResModel) => {
            if (r.success) {
                if (!tmConfigRef.current && r.data) {
                    tmConfigRef.current = {
                        id: r.data.id,
                        config: r.data.config,
                        name: r.data.name
                    }
                }

                message.info('Lưu thành công').then()
            } else {
                message.error('Lưu thất bại').then()
            }
        }

        if (tmConfigRef.current) {
            AxiosClient
                .put(ApiService.resProfile("tm3d", props.m3dId, tmConfigRef.current.id), data)
                .then(onSuccess)
                .catch(_ => message.error('Lưu thất bại').then())
        } else {
            AxiosClient
                .post(ApiService.resProfile("tm3d", props.m3dId), data)
                .then(onSuccess)
                .catch(_ => message.error('Lưu thất bại').then())
        }
    }

    const onClickMenuTool = (evt: MenuInfo) => {
        if (props.viewer.sceneLayers.sceneGroupLayers.length < 1 && evt.key !== EMenuTool.AddScene) {
            message.error(t('message.pleaseCreateScene')).then()

            return
        }
        if (evt.key === EMenuTool.AddScene || evt.key === EMenuTool.SaveScene) {
            switch (evt.key as EMenuTool) {
                case EMenuTool.AddScene:
                    setIsAddScene({
                        visible: true,
                        initValue: {
                            name: `Scene ${props.viewer.sceneLayers.sceneGroupLayers.length}`,
                            scale: 1,
                            lng: props.mbs.lng,
                            lat: props.mbs.lat,
                            altitude: props.mbs.min,
                        }
                    })

                    break;
                case EMenuTool.SaveScene:
                    onSaveScene()

                    break;
            }
        } else {
            if (props.viewer.sceneLayers.sceneGroupLayers.length === 1) {
                props.event.emit(DeckEventName.FeatureSelected, [0])

                props.onModifyScene({
                    key: evt.key as EMenuTool,
                    mbs: props.mbs
                })
            } else {
                props.setIsModifyDrawer({
                    visible: true,
                    type: evt.key as EMenuTool
                })
            }
        }
    }

    const onClickZoomDeck = (evt: MenuInfo) => {
        const deck = props.viewer.deck;

        if (deck && deck.props.viewState.zoom) {
            const _viewState = {
                ...deck.props.viewState,
                transitionDuration: 2000,
                transitionInterpolator: new LinearInterpolator()
            }

            switch (evt.key as EMenuZoom) {
                case EMenuZoom.ZoomIn:
                    deck.setProps({
                        viewState: {
                            ..._viewState,
                            zoom: deck.props.viewState.zoom + 0.5,
                        }
                    })

                    break;
                case EMenuZoom.ZoomOut:
                    deck.setProps({
                        viewState: {
                            ..._viewState,
                            zoom: deck.props.viewState.zoom - 0.5,
                        }
                    })

                    break;
            }
        }
    }

    const onClickChangeOptsControlView = (evt: MenuInfo) => {
        switch (evt.key as EMenuControl) {
            default:
                props.event.emit(DeckEventName.ControllerChange, evt.key)

                break;
            case EMenuControl.Center:
                changeViewState({
                    viewer: props.viewer,
                    cartographicCenter: new Vector3(props.mbs.lng, props.mbs.lat, props.mbs.min),
                    zoom: 14,
                })

                break;
        }
    }

    return (
        <>
            <Space
                className={sms.MenuOptsTool}
                direction={"vertical"}
                size={[10, 10]}
            >
                <Menu
                    items={OptionsMenu.menuOptsZoom(t)}
                    onClick={onClickZoomDeck}
                />
                <Menu
                    items={OptionsMenu.menuOptsControlView(t)}
                    onClick={onClickChangeOptsControlView}
                />
                <Menu
                    items={OptionsMenu.menuOptTool({
                        isDisable: props.viewer.sceneLayers.sceneGroupLayers.length < 1,
                        t
                    })}
                    onClick={onClickMenuTool}
                />
            </Space>
            {
                (isAddScene && isAddScene.initValue) && <UpLoadSceneModalFC
                    onClose={() => setIsAddScene({visible: false})}
                    initValue={isAddScene.initValue}
                    onFinish={onFinishScene}
                    mbs={props.mbs}
                />
            }
        </>
    )
}
