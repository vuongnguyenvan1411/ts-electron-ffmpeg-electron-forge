import {FeatureCollection, Position} from "@nebula.gl/edit-modes/src/geojson-types";
import turfBearing from "@turf/bearing";
import {ModeProps} from "@nebula.gl/edit-modes/src/types";
import {GeoJsonEditAction} from "@nebula.gl/edit-modes/src/lib/geojson-edit-mode";
import turfCentroid from "@turf/centroid";
import turfTransformRotate from "@turf/transform-rotate";
import {ImmutableFeatureCollection, RotateMode} from "@nebula.gl/edit-modes";
import EventEmitter from "eventemitter3";
import {Viewer} from "../viewer/Viewer";
import {find} from "lodash";
import {ModifyScene} from "./ModifyScene";

export const RotateScene = (props: {
    mode: RotateMode,
    event: EventEmitter,
    viewer: Viewer
}) => {
    function getRotationAngle(centroid: Position, startDragPoint: Position, currentPoint: Position) {
        const bearing1 = turfBearing(centroid, startDragPoint);
        const bearing2 = turfBearing(centroid, currentPoint);
        return bearing2 - bearing1;
    }

    props.mode.getRotateAction = (
        startDragPoint: Position,
        currentPoint: Position,
        editType: string,
        modeProps: ModeProps<FeatureCollection>
    ): GeoJsonEditAction | null | undefined => {
        if (!props.mode._geometryBeingRotated) return null;

        const centroid = turfCentroid(props.mode._geometryBeingRotated as any);
        const angle = getRotationAngle(centroid as any, startDragPoint, currentPoint);
        const featureProps = modeProps.data.features[0];
        const propsSelected = featureProps.properties;

        if (featureProps && propsSelected) {
            const newChild = props.viewer.sceneLayers.children;

            const childSelected = find(newChild, function (_) {
                return _.id === propsSelected.id
            })

            if (!childSelected) return

            const orientation = [...childSelected.feature.properties.orientation]
            orientation[1] = angle;

            ModifyScene({
                ...props,
                scene: childSelected,
                value: {
                    feature: childSelected.feature,
                    scale: childSelected.feature.properties.scale,
                    orientation: orientation,
                }
            })
        }

        // @ts-ignore
        const rotatedFeatures: FeatureCollection = turfTransformRotate(
            // @ts-ignore
            props.mode._geometryBeingRotated,
            angle,
            {
                pivot: centroid,
            }
        );

        let updatedData = new ImmutableFeatureCollection(modeProps.data);

        const selectedIndexes = modeProps.selectedIndexes;
        for (let i = 0; i < selectedIndexes.length; i++) {
            const selectedIndex = selectedIndexes[i];
            const movedFeature = rotatedFeatures.features[i];
            updatedData = updatedData.replaceGeometry(selectedIndex, movedFeature.geometry);
        }

        return {
            updatedData: updatedData.getObject(),
            editType,
            editContext: {
                featureIndexes: selectedIndexes,
            },
        };
    }
}
