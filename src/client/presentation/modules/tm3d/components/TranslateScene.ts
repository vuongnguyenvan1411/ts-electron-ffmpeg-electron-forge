import {center} from "@turf/turf";
import {find} from "lodash";
import {TFeature} from "../viewer/clim/CSSceneLayers";
import {TranslateMode} from "@nebula.gl/edit-modes";
import {Viewer} from "../viewer/Viewer";
import EventEmitter from "eventemitter3";
import {ModifyScene} from "./ModifyScene";

export const TranslateScene = (props: {
    mode: TranslateMode,
    viewer: Viewer,
    event: EventEmitter
}) => {
    props.mode.handleStopDragging = (evt, modeProps) => {
        if (modeProps.selectedIndexes.length > 0) {
            const featureProps = modeProps.data.features[0];
            const propsSelected = featureProps.properties;

            if (featureProps && propsSelected) {
                const geom = center(JSON.parse(JSON.stringify(featureProps))).geometry;
                geom.coordinates.push(propsSelected.altitude);

                const childSelected = find(props.viewer.sceneLayers.children, function (_) {
                    return _.id === propsSelected.id
                })

                if (!childSelected) return

                const feature: TFeature = {
                    type: "Feature",
                    geometry: geom,
                    properties: childSelected.feature.properties
                }

                ModifyScene({
                    ...props,
                    scene: childSelected,
                    value: {
                        feature: feature,
                        scale: childSelected.feature.properties.scale,
                        orientation: childSelected.feature.properties.orientation
                    }
                })
            }
        }
    }
}
