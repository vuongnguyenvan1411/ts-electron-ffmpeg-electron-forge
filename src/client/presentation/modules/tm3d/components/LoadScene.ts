import {TFeature} from "../viewer/clim/CSSceneLayers";
import {Utils} from "../../../../core/Utils";
import {Map3DModel} from "../../../../models/service/geodetic/Map3DModel";
import {Viewer} from "../viewer/Viewer";

export const LoadScene = (props: {
    item: Map3DModel,
    viewer: Viewer,
}) => {
    if (props.item.info?.tm?.config?.obj) {
        props.item.info.tm.config.obj.forEach((item, index) => {
            const geom: TFeature = {
                geometry: {
                    coordinates: [item.lng, item.lat, item.altitude],
                    type: 'Point',
                },
                type: "Feature",
                properties: {
                    id: `scene-${index}`,
                    url: item.url,
                    altitude: item.altitude,
                    orientation: item.orientation,
                    scale: item.scale,
                }
            };
            props.viewer.sceneLayers.setLayer({
                data: geom,
                id: `scene-${index}`,
                url: `${Utils.cdnGdtAssetRaw('raw/obj')}/${item.url}`,
                name: item.name,
                scale: item.scale,
                orientation: item.orientation
            })
        })
    }
}
