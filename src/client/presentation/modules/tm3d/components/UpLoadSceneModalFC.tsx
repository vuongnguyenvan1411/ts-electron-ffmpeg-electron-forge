import {Form, Input, message, Modal, Upload, UploadProps} from "antd";
import React, {useState} from "react";
import {useTranslation} from "react-i18next";
import {RcFile} from "antd/es/upload";
import {Utils} from "../../../../core/Utils";
import axios from "axios";
import {CustomButton} from "../../../components/CustomButton";
import {TFormScene, TMbs} from "../const/Defines";

export const UpLoadSceneModalFC = (props: {
    onClose: () => void,
    initValue: TFormScene
    onFinish: (value: TFormScene) => void,
    mbs: TMbs
}) => {
    const [formScene] = Form.useForm();
    const {t} = useTranslation();

    const accept = '.obj, .glb'
    const [percent, setPercent] = useState<number>(0);

    const customRequest = async options => {
        const {onSuccess, onError, file, onProgress} = options;

        const fmData = new FormData();
        const config = {
            headers: {"content-type": "multipart/form-data"},
            onUploadProgress: event => {
                const percent = Math.floor((event.loaded / event.total) * 100);
                setPercent(percent);
                if (percent === 100) {
                    setTimeout(() => setPercent(0), 1000);
                }
                onProgress({percent: (event.loaded / event.total) * 100});
            }
        };
        fmData.append("image", file);
        try {
            const res = await axios.post(Utils.cdnGdtAssetRaw('obj'),
                fmData,
                config
            );

            onSuccess("Ok");
            console.log("server res: ", res);
        } catch (err) {
            console.log("Eroor: ", err);
            const error = new Error("Some error");
            onError({err});
        }
    };

    const uploadProps: UploadProps = {
        name: 'file',
        multiple: false,
        maxCount: 1,
        accept: accept,
        beforeUpload: (file: RcFile) => {
            const fileType = file.name.split('.').pop();

            const isLt10M = file.size / 1024 / 1024 < 32;

            if (!isLt10M) {
                message.error('File must smaller than 20MB!').then();

                return Upload.LIST_IGNORE
            } else {
                if (fileType) {
                    const is3dObject = accept.search(fileType) !== -1

                    if (!is3dObject) message.error(`${file.name} is not a obj file`).then();

                    return is3dObject || Upload.LIST_IGNORE;
                } else {
                    message.error(`${file.name} is not a obj file`).then();

                    return Upload.LIST_IGNORE
                }
            }
        },
        // action: Utils.cdnGdtAssetRaw('obj'),
        progress: {
            strokeColor: {
                '0%': '#108ee9',
                '100%': '#87d068',
            },
            strokeWidth: 3,
            format: () => percent && `${parseFloat(percent.toFixed(2))}%`,
        },
        onChange(info) {
            const {status} = info.file;
            if (status !== 'uploading') {
                console.log(info.file);
            }
            if (status === 'done') {
                message.success(`${info.file.name} file uploaded successfully.`).then();
            } else if (status === 'error') {
                message.error(`${info.file.name} file upload failed.`).then();
            }
        },
        customRequest: customRequest,
        // customRequest: ({onError, onSuccess, file}) => {
        //     const metadata = {
        //         contentType: ''
        //     }
        //     const storageRef = ref(storage, `//test/${(file as any).name}`);
        //
        //     const uploadTask = uploadBytesResumable(storageRef, file as any, metadata);
        //
        //     uploadTask.on(
        //         "state_changed",
        //         (snapshot) => {
        //             const percent = Math.round(
        //                 (snapshot.bytesTransferred / snapshot.totalBytes) * 100
        //             );
        //
        //             // update progress
        //             setPercent(percent);
        //         },
        //         (err) => console.log(err),
        //         () => {
        //             // download url
        //             getDownloadURL(uploadTask.snapshot.ref).then((url) => {
        //                 formScene.setFieldsValue({
        //                     url: url,
        //                 })
        //             });
        //         }
        //     );
        // }
    }

    const rangeAltitude = {
        max: parseFloat((props.mbs.max - 100).toFixed(0)),
        min: parseFloat((props.mbs.min - 100).toFixed(0))
    }


    return (
        <Modal
            className={'modal-main ant-modal-v2d'}
            visible
            closable
            onCancel={props.onClose}
            title={t('button.upLoadFile')}
            footer={
                [
                    <CustomButton
                        size={'small'}
                        usingFor={'geodetic'}
                        key={"ok-delete"}
                        onClick={formScene.submit}
                    >
                        {t("button.ok")}
                    </CustomButton>
                ]
            }
        >
            <Form
                form={formScene}
                labelCol={{
                    xs: {span: 24},
                    sm: {span: 8},
                    xxl: {span: 10},
                    xl: {span: 10},
                }}
                wrapperCol={{
                    xs: {span: 24},
                    sm: {span: 16},
                    xxl: {span: 14},
                    xl: {span: 14},
                }}
                onFinish={props.onFinish}
            >
                <Form.Item
                    labelAlign={"left"}
                    key={"k_url"}
                    label={t("button.upLoadFile")}
                    name={"url"}
                    rules={[{required: true, message: t('validation.emptyData')}]}
                >
                    <Input/>
                    {/*<Upload*/}
                    {/*    {...uploadProps}*/}
                    {/*>*/}
                    {/*    <Button icon={<UploadOutlined/>}>{t("button.upLoadFile")}</Button>*/}
                    {/*</Upload>*/}
                </Form.Item>
                <Form.Item
                    labelAlign={"left"}
                    key={"k_name"}
                    label={t("text.name")}
                    name={"name"}
                    rules={[
                        {
                            required: true,
                            message: t("validation.emptyData"),
                        },
                        {
                            max: 100,
                            message: t('validation.minAndMaxCharacter', {
                                label: t("text.name"),
                                min: '1',
                                max: '100'
                            }),
                        }
                    ]}
                    initialValue={props.initValue?.name}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    className={'mt-3 mr-4 ml-1 '}
                    label={t('text.longitude')}
                    name={'lng'}
                    labelAlign={"left"}
                    initialValue={props.initValue.lng}
                    rules={[
                        {required: true, message: t('validation.emptyData')},
                        () => ({
                            validator(_, value) {
                                if ((value <= 180) && (value >= -180)) {
                                    return Promise.resolve();
                                } else {
                                    return Promise.reject(new Error(t('validation.latitudeRange')));
                                }
                            },
                        }),
                    ]}
                >
                    <Input
                        className={'input-main input-v2d input-h-40 input-bg-white input-radius-4'}
                        placeholder={'000.00000'}
                        type={'number'}
                    />
                </Form.Item>
                <Form.Item
                    labelAlign={"left"}
                    className={'mt-3 mr-4 ml-1'}
                    label={t('text.latitude')}
                    name={'lat'}
                    initialValue={props.initValue.lat}
                    rules={[
                        {required: true, message: t('validation.emptyData')},
                        () => ({
                            validator(_, value) {
                                if ((value <= 90) && (value >= -90)) {
                                    return Promise.resolve();
                                } else {
                                    return Promise.reject(new Error(t('validation.latitudeRange')));
                                }
                            },
                        }),
                    ]}
                >
                    <Input
                        className={'input-main input-v2d input-h-40 input-bg-white input-radius-4'}
                        placeholder={'00.00000'}
                        type={'number'}
                    />
                </Form.Item>
                <Form.Item
                    labelAlign={"left"}
                    className={'mt-3 mr-4 ml-1'}
                    label={t('v3d.text.weightElevation')}
                    name={'altitude'}
                    initialValue={props.initValue.altitude}
                    rules={[
                        {required: true, message: t('validation.emptyData')},
                        () => ({
                            validator(_, value) {
                                if ((value > rangeAltitude.min) && (value < rangeAltitude.max)) {
                                    return Promise.resolve();
                                } else {
                                    return Promise.reject(new Error(t('validation.minAndMax', {
                                        label: t('text.height'),
                                        min: rangeAltitude.min,
                                        max: rangeAltitude.max
                                    })));
                                }
                            },
                        }),
                    ]}
                >
                    <Input
                        className={'input-main input-v2d input-h-40 input-bg-white input-radius-4'}
                        type={'number'}
                    />
                </Form.Item>
                <Form.Item
                    labelAlign={"left"}
                    className={'mt-3 mr-4 ml-1'}
                    label={t('text.scale')}
                    name={'scale'}
                    initialValue={props.initValue.scale}
                    rules={[
                        {required: true, message: t('validation.emptyData')},
                        () => ({
                            validator(_, value) {
                                if ((value > 0) && (value < 10)) {
                                    return Promise.resolve();
                                } else {
                                    return Promise.reject(new Error(t('validation.scaleRange', {
                                        label: t("text.scale"),
                                        min: '0',
                                        max: '10'
                                    })));
                                }
                            },
                        }),
                    ]}
                >
                    <Input
                        className={'input-main input-v2d input-h-40 input-bg-white input-radius-4'}
                        type={'number'}
                    />
                </Form.Item>
            </Form>
        </Modal>
    )
}
