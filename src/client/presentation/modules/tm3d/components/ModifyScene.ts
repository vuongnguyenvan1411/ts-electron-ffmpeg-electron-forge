import {TModifyScene} from "../const/Defines";
import EventEmitter from "eventemitter3";
import {Viewer} from "../viewer/Viewer";
import {PitchYawRoll} from "@deck.gl/core/utils/positions";
import {DeckEventName} from "../const/Events";
import {CLScene} from "../viewer/clim/CSSceneLayers";

export const ModifyScene = (props: {
    value: TModifyScene,
    event: EventEmitter,
    viewer: Viewer,
    scene: CLScene,
}) => {
    const newSceneChild = props.viewer.sceneLayers.children;

    props.scene.feature = props.value.feature;

    props.scene.sceneLayer = props.scene.sceneLayer.clone({
        getOrientation: props.value.orientation as PitchYawRoll,
        data: [props.value.feature],
        sizeScale: props.value.scale * 10
    } as any)

    props.scene.feature.properties = {
        ...props.scene.feature.properties,
        scale: props.value.scale,
        orientation: props.value.orientation,
        altitude: props.value.feature.geometry.coordinates[2],
    }

    props.viewer.sceneLayers.sceneGroupLayers = newSceneChild.map(object => {
        return object.sceneLayer
    })

    props.viewer.sceneLayers.children = newSceneChild

    props.event.emit(DeckEventName.SceneChanged);
}
