import {Viewer} from "../viewer/Viewer";
import {CheckboxOptionType, Radio, RadioChangeEvent, Space} from "antd";
import {App, BASE_STYLES} from "../const/App";
import {CustomTypography} from "../../../components/CustomTypography";
import {DetectMedia} from "../../v2d/const/App";
import {useTranslation} from "react-i18next";
import Image from "next/image";

export const BaseTileSwitcherFC = (props: {
    viewer: Viewer
}) => {
    const swOpts: CheckboxOptionType[] = [];
    const {t} = useTranslation();

    App.CARTO_BASE_MAP(t).forEach(layer => {
        swOpts.push({
            value: layer.key,
            label: <Space>
                <Image
                    src={layer.image.src}
                    alt={layer.key}
                    width={64}
                    height={64}
                />
                <CustomTypography textStyle={!DetectMedia() ? 'text-16-24' : 'text-12-22'}>
                    {layer.title}
                </CustomTypography>
            </Space>,

        })
    })

    const onChangeBaseLayer = (e: RadioChangeEvent) => {
        switch (e.target.value) {
            default:
                props.viewer.baseLayers.setLayer(e.target.value)

                break;
            case BASE_STYLES.NONE:
                props.viewer.baseLayers.setVisible(false)

                break;
        }
    }

    return <Radio.Group
        className={'radio-main radio-v2d-main radio-switch-base radio-mb-3'}
        onChange={onChangeBaseLayer}
        options={swOpts}
        optionType={"button"}
        defaultValue={props.viewer.baseLayers.keyLayer}
    />
}
