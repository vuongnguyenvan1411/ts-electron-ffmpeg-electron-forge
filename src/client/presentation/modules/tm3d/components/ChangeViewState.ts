import {Vector3} from '@math.gl/core';
import {Viewer} from "../viewer/Viewer";
import {FlyToInterpolator, LinearInterpolator} from "@deck.gl/core";
import {App} from "../const/App";
import {ViewStateProps} from "@deck.gl/core/lib/deck";

export const changeViewState = (props: {
    viewer: Viewer,
    zoom?: number,
    cartographicCenter?: Vector3,
    pitch?: number,
}) => {
    const deck = props.viewer.deck
    if (!deck) return

    const _viewState: ViewStateProps = deck.props.viewState ?? {};

    if (props.cartographicCenter) {

        [
            _viewState.longitude,
            _viewState.latitude,
        ] = props.cartographicCenter;

        _viewState.altitude = 1
    }

    if (props.zoom) {
        _viewState.zoom = props.zoom
    } else {
        _viewState.zoom = _viewState.zoom ? _viewState.zoom : App.INITIAL_VIEW_STATE.zoom
    }

    _viewState.pitch = props.pitch ? props.pitch : App.INITIAL_VIEW_STATE.pitch

    const viewState = {
        ..._viewState,
        transitionDuration: 3000,
        transitionInterpolator: new FlyToInterpolator(),
    }

    deck.setProps({viewState: viewState})
}
