import {Divider, Drawer, Form, Input, InputNumber, Select, Slider} from "antd";
import {useTranslation} from "react-i18next";
import {Viewer} from "../viewer/Viewer";
import React, {useEffect} from "react";
import {CustomTypography} from "../../../components/CustomTypography";
import {DefaultOptionType} from "rc-select/es/Select";
import {EMenuTool, TEditScene, TMbs, TModifyScene} from "../const/Defines";
import {Color} from "../../../../const/Color";
import {Utils} from "../../../../core/Utils";
import {useForm} from "antd/es/form/Form";
import {CustomButton} from "../../../components/CustomButton";
import {SaveOutlined} from "@ant-design/icons";
import {TFeature} from "../viewer/clim/CSSceneLayers";
import EventEmitter from "eventemitter3";
import {find} from "lodash";
import {ModifyScene} from "./ModifyScene";
import sms from "../styles/MapView.module.scss";
import Image from "next/image";
import {Images} from "../../../../const/Images";

export const FormEditSceneLayer = (props: {
    viewer: Viewer
    mbs: TMbs,
    initValue?: TModifyScene,
    type?: EMenuTool,
    headerHeightRef: number,
    visible: boolean,
    onClose: () => void,
    onSelectGeoJsonLayer: (agrs: any) => void,
    event: EventEmitter
}) => {
    useEffect(() => {
        console.log('%cMount Screen: FormEditScene', Color.ConsoleInfo);

        if (props.initValue && props.initValue.sceneId) {
            const defaultChild = props.viewer.sceneLayers.getChildById(props.initValue.sceneId)

            if (defaultChild) {
                props.viewer.sceneLayers.setLayer({
                    data: defaultChild.feature,
                    id: 'axis',
                    url: `${Utils.cdnGdtAssetRaw('raw/obj/axis/axis.glb')}`,
                    name: 'axis',
                    scale: 0.1,
                    orientation: props.initValue.orientation
                })
            }
        }
        return () => {
            console.log('%cUnmount Screen: FormEditScene', Color.ConsoleInfo)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.visible])

    const {t} = useTranslation();
    const [form] = useForm();

    const rangeAltitude = {
        max: parseFloat((props.mbs.max - 100).toFixed(0)),
        min: parseFloat((props.mbs.min - 100).toFixed(0))
    }

    const sceneOpts = props.viewer.sceneLayers.children.filter(function (_) {
        return _.id !== 'axis'
    }).map(object => {
        return ({
            value: object.id,
            label: object.title
        } as DefaultOptionType)
    })

    const onFinish = (value: TEditScene) => {
        if (props.type !== EMenuTool.Modify) {
            props.onSelectGeoJsonLayer({
                value: value,
                type: props.type,
            })
        }

        const newChild = props.viewer.sceneLayers.children;
        const coords = [value.translate.lng, value.translate.lat, value.altitude];
        const orientation = Object.values(value.orientation);

        const childSelected = find(newChild, function (_) {
            return _.id === value.scene
        })

        if (!childSelected) return

        const feature: TFeature = {
            type: "Feature",
            geometry: {
                coordinates: coords,
                type: 'Point',
            },
            properties: {
                ...childSelected.feature.properties,
                altitude: value.altitude,
                scale: value.scale,
                orientation: orientation
            }
        }

        ModifyScene({
            ...props,
            scene: childSelected,
            value: {
                feature: feature,
                scale: value.scale,
                orientation: orientation
            },
        })
    }
    return (
        <Drawer
            visible={props.visible}
            className={sms.DrawerRight}
            style={{
                height: `calc(100vh - ${props.headerHeightRef}px)`,
                top: `${props.headerHeightRef}px`
            }}
            width={'15rem'}
            closable
            destroyOnClose
            bodyStyle={{padding: 0}}
            footer={null}
            mask={false}
            onClose={props.onClose}
            placement={'right'}
            closeIcon={<Image
                height={16}
                width={16}
                src={Images.iconXCircle.data}
                alt={Images.iconXCircle.atl}
            />}
            title={
                <CustomTypography textStyle={'text-16-24'} isStrong>
                    {t('text.sceneLayer')}
                </CustomTypography>
            }
        >
            <Form
                form={form}
                labelAlign={"left"}
                onFinish={onFinish}
                layout="vertical"
                className={'m-4'}
            >
                <Form.Item
                    key={"scene"}
                    label={
                        <CustomTypography
                            textStyle={"text-14-18"}
                            isStrong
                            className={'w-full'}
                        >
                            {t('text.sceneLayer')}
                        </CustomTypography>
                    }
                    name={"scene"}
                    className={'form-item label-typography-font-14-line-18 form-item-mb-3'}
                    initialValue={sceneOpts.length ? sceneOpts[0].value : null}
                >
                    <Select
                        className={'select-main select-h-40 select-radius-4 w-full'}
                        options={sceneOpts}
                    />
                </Form.Item>
                {
                    props.type === EMenuTool.Modify && <>
                        <Form.Item
                            key={"translate"}
                            label={
                                <CustomTypography
                                    textStyle={"text-14-18"}
                                    isStrong
                                    className={'w-full'}
                                >
                                    {t('text.moveFeature')}
                                </CustomTypography>
                            }
                            name={"translate"}
                            className={'form-item label-typography-font-14-line-18 form-item-mb-3'}
                        >
                            <Input.Group compact>
                                <Form.Item
                                    name={['translate', 'lng']}
                                    label={
                                        <CustomTypography
                                            textStyle={"text-14-18"}
                                            className={'w-full'}
                                        >
                                            {t('text.longitude')}
                                        </CustomTypography>
                                    }
                                    className={'form-item label-typography-font-14-line-18 form-item-mb-0'}
                                    rules={[
                                        {required: true, message: t('validation.emptyData')},
                                        () => ({
                                            validator(_, value) {
                                                if ((value > -180) && (value < 180)) {
                                                    return Promise.resolve();
                                                } else {
                                                    return Promise.reject(new Error(t('validation.longitudeRange', {
                                                        min: -180,
                                                        max: 180
                                                    })));
                                                }
                                            },
                                        }),
                                    ]}
                                    initialValue={props.initValue?.feature.geometry.coordinates[0]}
                                >
                                    <InputNumber
                                        type={'number'}
                                        className={'input-main input-v2d input-h-40 input-bg-white input-radius-4 input-number'}
                                    />
                                </Form.Item>
                                <Form.Item
                                    name={['translate', 'lat']}
                                    className={'form-item label-typography-font-14-line-18 form-item-mb-0'}
                                    label={
                                        <CustomTypography
                                            textStyle={"text-14-18"}
                                            className={'w-full'}
                                        >
                                            {t('text.latitude')}
                                        </CustomTypography>
                                    }
                                    rules={[
                                        {required: true, message: t('validation.emptyData')},
                                        () => ({
                                            validator(_, value) {
                                                if ((value > -90) && (value < 90)) {
                                                    return Promise.resolve();
                                                } else {
                                                    return Promise.reject(new Error(t('validation.latitudeRange', {
                                                        min: -90,
                                                        max: 90
                                                    })));
                                                }
                                            },
                                        }),
                                    ]}
                                    initialValue={props.initValue?.feature.geometry.coordinates[1]}
                                >
                                    <InputNumber
                                        type={'number'}
                                        className={'input-main input-v2d input-h-40 input-bg-white input-radius-4 input-number'}
                                    />
                                </Form.Item>
                            </Input.Group>
                        </Form.Item>
                        <Form.Item
                            key={"altitude"}
                            name={"altitude"}
                            label={
                                <CustomTypography
                                    textStyle={"text-14-18"}
                                    isStrong
                                    className={'w-full'}
                                >
                                    {t('text.height')}
                                </CustomTypography>
                            }
                            className={'form-item label-typography-font-14-line-18 form-item-mb-3'}
                            initialValue={props.initValue?.feature.properties.altitude}
                            rules={[
                                {
                                    required: true,
                                    message: t('validation.emptyData')
                                },
                                () => ({
                                    validator(_, value) {
                                        if ((value > rangeAltitude.min) && (value < rangeAltitude.max)) {
                                            return Promise.resolve();
                                        } else {
                                            return Promise.reject(new Error(t('validation.minAndMax', {
                                                label: t('text.height'),
                                                min: rangeAltitude.min,
                                                max: rangeAltitude.max
                                            })));
                                        }
                                    },
                                }),
                            ]}
                        >
                            <InputNumber
                                type={'number'}
                                className={'input-main input-v2d input-h-40 input-bg-white input-radius-4 input-number'}
                            />
                        </Form.Item>
                        <Form.Item
                            key={"scale"}
                            name={"scale"}
                            className={'form-item label-typography-font-14-line-18 form-item-mb-3'}
                            label={
                                <CustomTypography
                                    textStyle={"text-14-18"}
                                    isStrong
                                    className={'w-full'}
                                >
                                    {t('text.scale')}
                                </CustomTypography>
                            }
                            initialValue={props.initValue ? props.initValue.scale : 1}
                        >
                            <Slider
                                min={0}
                                max={10}
                                included={false}
                                marks={{
                                    0: 0,
                                    10: {
                                        style: {
                                            color: '#f50',
                                        },
                                        label: <strong>10</strong>,
                                    },
                                }}
                                step={0.01}
                                tooltipPlacement={'bottom'}
                                className={'w-11/12'}
                            />
                        </Form.Item>
                        <Form.Item
                            key={"orientation"}
                            name={"orientation"}
                            className={'form-item label-typography-font-14-line-18 form-item-mb-3'}
                            label={
                                <CustomTypography
                                    textStyle={"text-14-18"}
                                    isStrong
                                    className={'w-full'}
                                >
                                    {t('text.rotateAndAdjust')}
                                </CustomTypography>
                            }
                        >
                            <Input.Group compact>
                                <Form.Item
                                    name={['orientation', 'yz']}
                                    label={
                                        <CustomTypography
                                            textStyle={"text-14-18"}
                                            className={'w-full'}
                                        >
                                            Trục Y-Z
                                        </CustomTypography>
                                    }
                                    className={'form-item label-typography-font-14-line-18 form-item-mb-0'}
                                    initialValue={props.initValue?.orientation[0]}
                                    rules={[
                                        {
                                            required: true,
                                            message: t('validation.emptyData')
                                        },
                                        () => ({
                                            validator(_, value) {
                                                if ((value > -180) && (value < 180)) {
                                                    return Promise.resolve();
                                                } else {
                                                    return Promise.reject(new Error(t('validation.minAndMax', {
                                                        label: 'Trục YZ',
                                                        min: -180,
                                                        max: 180
                                                    })));
                                                }
                                            },
                                        }),
                                    ]}
                                >
                                    <InputNumber
                                        type={'number'}
                                        className={'input-main input-v2d input-h-40 input-bg-white input-radius-4 input-number'}
                                    />
                                </Form.Item>
                                <Form.Item
                                    name={['orientation', 'xy']}
                                    label={
                                        <CustomTypography
                                            textStyle={"text-14-18"}
                                            className={'w-full'}
                                        >
                                            Trục X-Y
                                        </CustomTypography>
                                    }
                                    className={'form-item label-typography-font-14-line-18 form-item-mb-0'}
                                    initialValue={props.initValue?.orientation[1]}
                                    rules={[
                                        {
                                            required: true,
                                            message: t('validation.emptyData')
                                        },
                                        () => ({
                                            validator(_, value) {
                                                if ((value > -180) && (value < 180)) {
                                                    return Promise.resolve();
                                                } else {
                                                    return Promise.reject(new Error(t('validation.minAndMax', {
                                                        label: 'Trục XY',
                                                        min: -180,
                                                        max: 180
                                                    })));
                                                }
                                            },
                                        }),
                                    ]}
                                >
                                    <InputNumber
                                        type={'number'}
                                        className={'input-main input-v2d input-h-40 input-bg-white input-radius-4 input-number'}
                                    />
                                </Form.Item>
                                <Form.Item
                                    name={['orientation', 'xz']}
                                    label={
                                        <CustomTypography
                                            textStyle={"text-14-18"}
                                            className={'w-full'}
                                        >
                                            Trục X-Z
                                        </CustomTypography>
                                    }
                                    className={'form-item label-typography-font-14-line-18 form-item-mb-0'}
                                    initialValue={props.initValue?.orientation[2]}
                                    rules={[
                                        {
                                            required: true,
                                            message: t('validation.emptyData')
                                        },
                                        () => ({
                                            validator(_, value) {
                                                if ((value > -180) && (value < 180)) {
                                                    return Promise.resolve();
                                                } else {
                                                    return Promise.reject(new Error(t('validation.minAndMax', {
                                                        label: 'Trục XZ',
                                                        min: -180,
                                                        max: 180
                                                    })));
                                                }
                                            },
                                        }),
                                    ]}
                                >
                                    <InputNumber
                                        type={'number'}
                                        className={'input-main input-v2d input-h-40 input-bg-white input-radius-4 input-number'}
                                    />
                                </Form.Item>
                            </Input.Group>
                        </Form.Item>
                    </>
                }
                <Divider/>
                <div className={"flex justify-end"}>
                    <Form.Item key={"action"}>
                        <CustomButton
                            htmlType="submit"
                            size={'small'}
                            usingFor={'geodetic'}
                            key={"save"}
                        >
                            <SaveOutlined/>{t('button.save')}
                        </CustomButton>
                    </Form.Item>
                </div>
            </Form>
        </Drawer>
    )
}
