//Loadtiles

import {Tileset3D} from "@loaders.gl/tiles";
import {DeckEventName} from "../const/Events";
import TileHeader from "@loaders.gl/tiles/src/tileset/tile-3d";
import {CSTileLayers} from "../viewer/clim/CSTileLayers";
import {Viewer} from "../viewer/Viewer";
import {Map3DModel} from "../../../../models/service/geodetic/Map3DModel";
import EventEmitter from "eventemitter3";
import sms from "../styles/MapView.module.scss"
import {changeViewState} from "./ChangeViewState";
import {Vector3} from '@math.gl/core';

export const LoadTile = (props: {
    viewer: Viewer,
    item: Map3DModel,
    event: EventEmitter,
}) => {
    const deckElement = document.getElementById('tm-wrapper');

    const _onTileSetLoad = (tileset: Tileset3D) => {
        const {zoom, cartographicCenter} = tileset;

        if (cartographicCenter instanceof Vector3 && props.viewer.deck) {

            changeViewState({
                zoom: zoom + 2.5,
                cartographicCenter: cartographicCenter,
                viewer: props.viewer,
            })

            props.event.emit(DeckEventName.TilesetLoad, tileset)
            props.viewer.tileLayers.addTileSets(tileset)
        }

        if (deckElement !== null) {
            deckElement.classList.add(sms.SpinnerLoading)
        }
    }

    const _onTileLoad = (tile: TileHeader) => {
        props.event.emit(DeckEventName.TileLoad, tile)

        if (deckElement !== null) {
            deckElement.classList.remove(sms.SpinnerLoading)
        }
    }

    const _onTileUnLoad = (tile: Tileset3D) => {
        props.event.emit(DeckEventName.TileUnload, tile)

        if ((tile as any).content) {
            delete (tile as any).content.featureIds;
        }
    }

    const _onTileError = (tileHeader: Object, url: string, message: string) => {
        console.log('tileHeader', tileHeader, 'message', message)

        return null
    }

    const tileLayers = new CSTileLayers({
        item: props.item,
        event: props.event,
        onTilesetLoad: _onTileSetLoad,
        onTileLoad: _onTileLoad,
        onTileUnload: _onTileUnLoad,
        onTileError: _onTileError,
    })

    props.viewer.setTileLayers(tileLayers)

    props.viewer.addLayer(tileLayers)
}
