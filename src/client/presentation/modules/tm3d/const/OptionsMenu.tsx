import {EMenuControl, EMenuTool, EMenuZoom} from "./Defines";
import {Tooltip} from "antd";
import {TFunction} from "react-i18next";
import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {falArrowsAlt, falMinus, falPlus, falSave, farCommentAltEdit, farEdit, farPlanetRinged, farPlusCircle, farScrubber, fasCompressArrowsAlt} from "../../../../const/FontAwesome";
import sms from "../styles/MapView.module.scss"
import {TParamPartGeodetic} from "../../../../const/Types";
import {EMenuMapItem} from "../../v2d/const/Defines";
import Image from "next/image";
import {Images} from "../../../../const/Images";
import {DetectMedia} from "../../v2d/const/App";

export class OptionsMenu {
    static readonly mainMenuTm3d = (props: {
        t: TFunction<"translation">,
        parts?: TParamPartGeodetic,
    }) => [
        {
            key: EMenuMapItem.Part,
            label: props.t('text.switchPart'),
            icon: <Tooltip
                placement="right"
                title={props.t('text.switchPart')}
                arrowPointAtCenter={true}
                overlayClassName={sms.MenuToolTip}
            >
                {
                    !props.parts
                        ? <Image
                            height={24}
                            width={24}
                            src={Images.iconSwitchPartsDisabled.data}
                            alt={Images.iconSwitchPartsDisabled.atl}
                        />
                        : <Image
                            height={24}
                            width={24}
                            src={Images.iconSwitchParts.data}
                            alt={Images.iconSwitchParts.atl}
                        />
                }
            </Tooltip>,
            disabled: !props.parts,
            style: {display: DetectMedia() ? 'none' : "flex"}
        },
        {
            key: EMenuMapItem.Map3DLayer,
            label: props.t('text.3DVisualization'),
            icon: <Tooltip
                overlayClassName={sms.MenuToolTip}
                placement="right"
                title={props.t('text.3DVisualization')}
                arrowPointAtCenter={true}
            >
                <Image
                    height={24}
                    width={24}
                    src={Images.icon3dBox.data}
                    alt={Images.icon3dBox.atl}
                />
            </Tooltip>,
        },
        {
            key: EMenuMapItem.BaseLayer,
            label: props.t('text.baseMap'),
            icon: <Tooltip
                overlayClassName={sms.MenuToolTip}
                placement="right"
                title={props.t('text.baseMap')}
                arrowPointAtCenter={true}
            >
                <Image
                    height={24}
                    width={24}
                    src={Images.iconLayerGroup.data}
                    alt={Images.iconLayerGroup.atl}
                />
            </Tooltip>,
        },
        {
            key: EMenuMapItem.SceneLayer,
            label: props.t('text.sceneLayer'),
            icon: <Tooltip
                overlayClassName={sms.MenuToolTip}
                placement="right"
                title={props.t('text.sceneLayer')}
                arrowPointAtCenter={true}
            >
                <Image
                    height={24}
                    width={24}
                    src={Images.iconSceneLayer.data}
                    alt={Images.iconSceneLayer.atl}
                />
            </Tooltip>,
        },
    ];

    static readonly mainMenuTm3dMobile = ( t: TFunction<"translation">) => [
        {
            key: EMenuMapItem.Map3DLayer,
            label: t('text.3DVisualization'),
            icon: <Image
                height={24}
                width={24}
                src={Images.icon3dBox.data}
                alt={Images.icon3dBox.atl}
            />,
        },
        {
            key: EMenuMapItem.BaseLayer,
            label: t('text.baseMap'),
            icon:<Image
                height={24}
                width={24}
                src={Images.iconLayerGroup.data}
                alt={Images.iconLayerGroup.atl}
            />,
        },
        {
            key: EMenuMapItem.SceneLayer,
            label: t('text.sceneLayer'),
            icon: <Image
                height={24}
                width={24}
                src={Images.iconSceneLayer.data}
                alt={Images.iconSceneLayer.atl}
            />,
        },
    ];

    static readonly menuOptTool = (props: {
        t: TFunction<"translation">,
        isDisable: boolean
    }) => [
        {
            key: EMenuTool.AddScene,
            icon: <Tooltip
                placement="left"
                title={props.t('text.addSceneLayer')}
                arrowPointAtCenter={true}
            >
                <FontAwesomeIcon
                    icon={farPlusCircle}
                    className={sms.IconFas}
                />
            </Tooltip>,
            style: {
                borderTopLeftRadius: "0.25rem",
                borderBottomLeftRadius: props.isDisable ? "0.25rem" : "0",
            }
        },
        {
            key: EMenuTool.Modify,
            icon: <Tooltip
                placement="left"
                title={props.t('text.editTools')}
                arrowPointAtCenter={true}
            >
                <FontAwesomeIcon
                    icon={farCommentAltEdit}
                    className={sms.IconFas}
                />
            </Tooltip>,
            style: {
                display: props.isDisable ? 'none' : "block"
            }
        },
        {
            key: EMenuTool.Translate,
            icon: <Tooltip
                placement="left"
                title={props.t('text.moveFeature')}
                arrowPointAtCenter={true}
            >
                <FontAwesomeIcon
                    icon={farEdit}
                    className={sms.IconFas}
                />
            </Tooltip>,
            style: {
                display: props.isDisable ? 'none' : "block"
            }
        },
        {
            key: EMenuTool.Rotation,
            icon: <Tooltip
                placement="left"
                title={props.t('text.rotateAndAdjust')}
                arrowPointAtCenter={true}
            >
                <FontAwesomeIcon
                    icon={farScrubber}
                    className={sms.IconFas}
                />
            </Tooltip>,
            style: {
                display: props.isDisable ? 'none' : "block"
            }
        },
        {
            key: EMenuTool.SaveScene,
            icon: <Tooltip
                placement="left"
                title={props.t('button.save')}
                arrowPointAtCenter={true}
            >
                <FontAwesomeIcon
                    icon={falSave}
                    className={sms.IconFas}
                />
            </Tooltip>,
            style: {
                borderBottomLeftRadius: "0.25rem",
                display: props.isDisable ? 'none' : "block"
            }
        },
    ]

    static readonly menuOptsZoom = (t: TFunction<"translation">) => [
        {
            key: EMenuZoom.ZoomIn,
            icon:
                <Tooltip
                    placement="left"
                    title={t('text.zoomIn')}
                    arrowPointAtCenter={true}
                >
                    <FontAwesomeIcon
                        icon={falPlus}
                        className={sms.IconFas}
                    />
                </Tooltip>,
            style: {borderTopLeftRadius: "0.25rem"}
        },
        {
            key: EMenuZoom.ZoomOut,
            icon:
                <Tooltip
                    placement="left"
                    title={t('text.zoomOut')}
                    arrowPointAtCenter={true}
                >
                    <FontAwesomeIcon
                        icon={falMinus}
                        className={sms.IconFas}
                    />
                </Tooltip>,
            style: {borderBottomLeftRadius: "0.25rem"}
        },
    ]

    static readonly menuOptsControlView = (t: TFunction<"translation">) => [
        {
            key: EMenuControl.Normal,
            icon:
                <Tooltip
                    placement="left"
                    title={t('v3d.text.viewControl')}
                    arrowPointAtCenter={true}
                >
                    <FontAwesomeIcon
                        icon={farPlanetRinged}
                        className={sms.IconFas}
                    />
                </Tooltip>,
            style: {borderTopLeftRadius: "0.25rem"}
        },
        {
            key: EMenuControl.ByPoint,
            icon: <Tooltip
                placement="left"
                title={t('v3d.text.insertControlPoint')}
                arrowPointAtCenter={true}
            >
                <FontAwesomeIcon
                    icon={falArrowsAlt}
                    className={sms.IconFas}
                />
            </Tooltip>,
        },
        {
            key: EMenuControl.Center,
            icon: <Tooltip
                placement="left"
                title={t('v3d.text.focusControl')}
                arrowPointAtCenter={true}
            >
                <FontAwesomeIcon
                    icon={fasCompressArrowsAlt}
                    className={sms.IconFas}
                />
            </Tooltip>,
            style: {borderBottomLeftRadius: "0.25rem"}
        },
    ]
}



