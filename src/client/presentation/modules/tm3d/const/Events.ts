export class DeckEventName {
    public static readonly AfterRender = "after_render";
    public static readonly WebGLInitialized = "webgl_initialized";

    public static readonly TileLoad = "tile_load";
    public static readonly TileUnload = "tile_unload";
    public static readonly TileChanged = "tile_changed";

    public static readonly TileSets = "tile_sets";
    public static readonly TilesetLoad = "tile_set_load";
    public static readonly TileAdded = "tile_added";
    public static readonly TileRemoved = "tile_removed";

    public static readonly SceneAdded = 'scene_added';
    public static readonly SceneChanged = 'scene_changed';

    public static readonly FeatureSelected = 'feature_selected';
    public static readonly FeatureChanged = 'feature_changed'
    public static readonly FeatureMode = 'feature_mode'

    public static readonly BaseChanged = "base_changed";

    public static readonly ControllerChange = "controller_changed";
}
