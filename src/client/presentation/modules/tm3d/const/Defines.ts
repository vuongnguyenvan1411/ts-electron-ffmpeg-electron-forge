import {TFeature} from "../viewer/clim/CSSceneLayers";

export enum ETextureLayer {
    Scene = "scene",
    Tile = "tile",
}

export enum EMenuTool {
    AddScene = 'AddScene',
    Translate = 'Translate',
    Modify = 'Modify',
    Rotation = 'Rotation',
    SaveScene = 'SaveScene'
}

export enum EMenuZoom {
    ZoomIn = 'ZoomIn',
    ZoomOut = 'ZoomOut'
}

export type TMbs = {
    lat: number,
    lng: number
    min: number,
    max: number,
}

export type TModifyScene = {
    sceneName?: string,
    sceneId?: string,
    feature: TFeature,
    scale: number,
    orientation: number[]
}

export type TFormScene = {
    id?: number,
    name: string,
    lng: number,
    lat: number,
    altitude: number,
    scale: number,
    url?: string
}

export type TEditScene = {
    altitude: number,
    orientation: {
        xy: number,
        yz: number,
        zx: number,
    },
    scale: number,
    scene: string,
    translate: {
        lng: number,
        lat: number,
    }
}

export enum EMenuControl {
    ByPoint = 'ByPoint',
    Normal = 'Normal',
    Center = 'Center'
}
