import {CLTile} from "../../v2d/viewer/clim/CLTileTool";
import {DataNode, EventDataNode} from "rc-tree/lib/interface";
import {Key} from "react";

export interface IEDataNode extends DataNode {
    object?: CLTile
    children?: IEDataNode[];
    expanded?: boolean;
    pos?: string;
}

export interface IOTreeSelectInfo {
    event: 'select';
    selected: boolean;
    node: EventDataNode<IEDataNode>;
    selectedNodes: IEDataNode[];
    nativeEvent: MouseEvent;
}

export interface IEEventDataNode extends EventDataNode<any> {
    object?: CLTile
    props?: IEDataNode;
    node: IEEventDataNode;
    checked: boolean;
    children?: IEDataNode[];
    key: Key;
    selected: boolean;
    className: string;
    pos: string;
}

export interface IOTreeCheckInfo {
    event: 'check';
    node: IEEventDataNode;
    checked: boolean;
    nativeEvent: MouseEvent;
    checkedNodes: IEDataNode[];
    checkedNodesPositions?: {
        node: IEDataNode;
        pos: string;
    }[];
    halfCheckedKeys?: Key[];
}

export interface IBaseLayer {
    key?: string;
    visible: boolean
}
