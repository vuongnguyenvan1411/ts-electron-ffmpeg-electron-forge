import {ViewStateProps} from "@deck.gl/core/lib/deck";
import {BASEMAP} from "@deck.gl/carto/typed";
import {TFunction} from "react-i18next";
import IconNoBase from "../../../../assets/image/v2d/ic_no_basemap.png";
import IconVoyager from "../../../../assets/image/tm3d/im_voyager.png";
import IconDarkMatter from "../../../../assets/image/tm3d/im_dark_matter.png";
import IconPositron from "../../../../assets/image/tm3d/im_positron.png";
import {ControllerOptions} from "@deck.gl/core/controllers/controller";

export const BASE_STYLES = {
    ...BASEMAP,
    NONE: 'None'
}

function hex2rgb(hex: string) {
    const value = parseInt(hex, 16);
    return [16, 8, 0].map((shift) => ((value >> shift) & 0xff) / 255);
}

export const FEATURE_COLORS = [
    'CCDFE5',
    'B0592D',
    'FF0083',
    '3105B3',
    '04C227',
    'F7EF05',
].map(hex2rgb);

export class App {
    static readonly INITIAL_VIEW_STATE: ViewStateProps = {
        latitude: 22.3031710,
        longitude: 103.7775997,
        pitch: 45,
        bearing: 0,
        minZoom: 0,
        maxZoom: 24,
        zoom: 14.5,
    }

    static readonly INITIAL_CONTROLLER: ControllerOptions = {
        doubleClickZoom: false,
        scrollZoom: {speed: 0.003, smooth: true},
        keyboard: {
            moveSpeed: 1,
            zoomSpeed: 50,
            rotateSpeedX: 10,
            rotateSpeedY: 10
        },
        dragMode: "rotate",
        inertia: true,
        dragRotate: true,
        dragPan: true,
        touchRotate: true,
    }

    static readonly MAP_STYLES = {
        Light: 'https://basemaps.cartocdn.com/gl/positron-nolabels-gl-style/style.json',
        Dark: 'https://basemaps.cartocdn.com/gl/dark-matter-nolabels-gl-style/style.json'
    }
    static readonly INITIAL_MAP_STYLE = App.MAP_STYLES.Light

    // source: Natural Earth http://www.naturalearthdata.com/ via geojson.xyz
    static readonly AIR_PORTS = 'https://d2ad6b4ur7yvpq.cloudfront.net/naturalearth-3.3.0/ne_10m_airports.geojson';

    // Set your Google Maps API key here or via environment variable
    static readonly GOOGLE_MAPS_API_KEY = process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY;
    static readonly GOOGLE_MAP_ID = process.env.NEXT_PUBLIC_GOOGLE_MAPS_MAP_ID;
    static readonly GOOGLE_MAPS_API_URL = `https://maps.googleapis.com/maps/api/js?key=${this.GOOGLE_MAPS_API_KEY}&v=beta&map_ids=${this.GOOGLE_MAP_ID}`;
    static readonly MAPBOX_ACCESS_TOKEN = 'pk.eyJ1Ijoia2hvYWl0ZWkiLCJhIjoiY2toYTlpYXk1MTRubzJ0cnRhZWxxbDdzOCJ9.G6zCSIG4SKgwfPC2KymnsA'

    static readonly CARTO_BASE_MAP = (t: TFunction<"translation">) => [
        {
            key: "mapbox://styles/mapbox/satellite-streets-v12",
            title: 'Bản đồ vệ tinh',
            image: IconPositron
        },
        {
            key: "mapbox://styles/mapbox/navigation-day-v1",
            title: 'Bản đồ đường phố',
            image: IconVoyager
        },
        {
            key: "mapbox://styles/mapbox/outdoors-v12",
            title: 'Bản đồ địa hình',
            image: IconDarkMatter
        },
        {
            key: BASE_STYLES.NONE,
            title: t('text.noBaseMap'),
            image: IconNoBase,
        },
    ]
}

