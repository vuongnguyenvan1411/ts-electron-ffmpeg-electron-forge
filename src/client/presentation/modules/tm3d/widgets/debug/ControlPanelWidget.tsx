export const ControlPanelWidget = (props: {
    name: string,
    onExampleChange: Function,
    onMapStyleChange: Function,
    onToggleBuildingExplorer: Function,
    onUpdateSublayerVisibility: Function,
    selectedMapStyle: string,
    sublayers: any[],
    isBuildingExplorerShown: boolean,
    useTerrainLayer: boolean,
    toggleTerrain: Function
}) => {

    return (
        <>ControlPanelWidget</>
    )
}
