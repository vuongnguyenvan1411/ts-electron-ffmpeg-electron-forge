import React, {useEffect, useRef, useState} from "react";
import styled from "styled-components";
import StatsWidget from "@probe.gl/stats-widget";
import {lumaStats} from "@luma.gl/webgl";
import {sumTilesetsStats} from "../../helpers/stats";
import {Stats} from "@probe.gl/stats";
import {Tileset3D} from "@loaders.gl/tiles";
import EventEmitter from "eventemitter3";
import {DeckEventName} from "../../const/Events";
import {ControlPanelWidget} from "./ControlPanelWidget";
import {AttributesPanelWidget} from "./AttributesPanelWidget";
import {ViewStateProps} from "@deck.gl/core/lib/deck";
import {App} from "../../const/App";

const Flex = `
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  position: absolute;
`;

const Color = `
  background: #0E111A;
  color: white;
`;

const Font = `
  font-family: 'Uber Move' sans-serif;
  font-size: 16px;
  font-style: normal;
  font-weight: 500;
  line-height: 19px;
  letter-spacing: 0em;
  text-align: left;
`;

export const DebugStatsWidget = React.memo((props: {
    event: EventEmitter,
    viewState: ViewStateProps
}) => {
    const [tileset, setTileset] = useState(null)
    const [url, setUrl] = useState<string>()
    const [token, setToken] = useState(null)
    const [name, setName] = useState<string>("OK")
    const [viewState, setViewState] = useState<ViewStateProps>(props.viewState)
    const [selectedMapStyle, setSelectedMapStyle] = useState<string>(App.INITIAL_MAP_STYLE)
    const [selectedFeatureAttributes, setSelectedFeatureAttributes] = useState<any>()
    const [selectedFeatureIndex, setSelectedFeatureIndex] = useState(-1)
    const [selectedTilesetBasePath, setSelectedTilesetBasePath] = useState(null)
    const [isAttributesLoading, setIsAttributesLoading] = useState<boolean>(false)
    const [showBuildingExplorer, setShowBuildingExplorer] = useState<boolean>(false)
    const [flattenedSublayers, setFlattenedSublayers] = useState([])
    const [sublayers, setSublayers] = useState<any[]>([])
    const [sublayersUpdateCounter, setSublayersUpdateCounter] = useState(0)
    const [tilesetsStats, setTilesetsStats] = useState<Stats>()
    const [useTerrainLayer, setUseTerrainLayer] = useState<boolean>(false)
    const [terrainTiles, setTerrainTiles] = useState({})
    const [loadedTilesets, setLoadedTilesets] = useState<Tileset3D[]>([])

    const memWidgetRef = useRef<StatsWidget>()
    const tilesetStatsWidgetRef = useRef<StatsWidget>()
    const statsWidgetContainerRef = useRef<HTMLDivElement | null>()

    useEffect(() => {
        memWidgetRef.current = new StatsWidget(lumaStats.get('Memory Usage'), {
            framesPerUpdate: 1,
            formatters: {
                'GPU Memory': 'memory',
                'Buffer Memory': 'memory',
                'Renderbuffer Memory': 'memory',
                'Texture Memory': 'memory'
            },
            container: statsWidgetContainerRef.current!
        })

        // @ts-ignore
        tilesetStatsWidgetRef.current = new StatsWidget(null, {
            container: statsWidgetContainerRef.current
        })


        props.event.on(DeckEventName.AfterRender, onDeckAfterRender)

        props.event.on(DeckEventName.TileLoad, onDeckTileLoad)
        props.event.on(DeckEventName.TileUnload, onDeckTileUnload)

        return () => {
            props.event.off(DeckEventName.AfterRender, onDeckAfterRender)

            props.event.off(DeckEventName.TileLoad, onDeckTileLoad)
            props.event.off(DeckEventName.TileUnload, onDeckTileUnload)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const onDeckAfterRender = () => {
        _updateStatWidgets()
    }

    const onDeckTileLoad = () => {
        _updateStatWidgets()
    }

    const onDeckTileUnload = () => {
        _updateStatWidgets()
    }

    const StatsWidgetWrapperSC = styled.div`
      display: flex;
      @media (max-width: 768px) {
        display: none;
      }
    `
    const StatsWidgetContainerSC = styled.div`
      ${Flex}
      ${Color}
      ${Font}
      color: rgba(255, 255, 255, .6);
      z-index: 3;
      word-break: break-word;
      padding: 24px;
      border-radius: 8px;
      line-height: 135%;
      bottom: auto;
      width: 277px;
      left: 10px;
      max-height: calc(100% - 125px);
      overflow: auto;
    `
    const _renderStats = () => {
        const style = {
            display: 'flex',
            top: '150px'
        }

        if (showBuildingExplorer) {
            style.display = 'none';
        }

        if (sublayers.length) {
            style.top = '200px';
        }

        return (
            <StatsWidgetContainerSC
                style={style}
                ref={(ref) => (statsWidgetContainerRef.current = ref)}
            />
        );
    }

    const _updateStatWidgets = () => {
        if (memWidgetRef.current) {
            memWidgetRef.current.update();
        }

        if (tilesetStatsWidgetRef.current && tilesetsStats) {
            sumTilesetsStats(loadedTilesets, tilesetsStats);
            tilesetStatsWidgetRef.current.update();
        }
    }

    const handleClosePanel = () => {
        //
    }

    const _renderAttributesPanel = () => {
        const title = selectedFeatureAttributes.NAME || selectedFeatureAttributes.OBJECTID;

        return (
            <AttributesPanelWidget
                title={title}
                handleClosePanel={handleClosePanel}
                attributesObject={selectedFeatureAttributes}
                isControlPanelShown
            />
        )
    }

    const _onSelectTileset = () => {
        //
    }

    const _onSelectMapStyle = () => {
        //
    }

    const _onToggleBuildingExplorer = () => {
        //
    }

    const _updateSublayerVisibility = () => {
        //
    }

    const _toggleTerrain = () => {
        //
    }

    return (
        <>
            <ControlPanelWidget
                name={name}
                onExampleChange={_onSelectTileset}
                onMapStyleChange={_onSelectMapStyle}
                onToggleBuildingExplorer={_onToggleBuildingExplorer}
                onUpdateSublayerVisibility={_updateSublayerVisibility}
                selectedMapStyle={selectedMapStyle}
                sublayers={sublayers}
                isBuildingExplorerShown={showBuildingExplorer}
                useTerrainLayer={useTerrainLayer}
                toggleTerrain={_toggleTerrain}
            />
            {selectedFeatureAttributes && _renderAttributesPanel()}
            {/*@ts-ignore*/}
            <StatsWidgetWrapperSC showMemory={!showBuildingExplorer}>
                {_renderStats()}
            </StatsWidgetWrapperSC>
        </>
    )
})
