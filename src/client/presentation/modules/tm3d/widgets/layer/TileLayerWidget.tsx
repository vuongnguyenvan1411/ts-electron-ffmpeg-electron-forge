import React, {useEffect, useState} from "react";
import EventEmitter from "eventemitter3";
import {DeckEventName} from "../../const/Events";
import {Tree} from "antd";
import {IEDataNode, IOTreeCheckInfo, IOTreeSelectInfo} from "../../const/Types";
import {Key} from "antd/es/table/interface";
import {CLTile} from "../../viewer/clim/CSTileLayers";
import {Viewer} from "../../viewer/Viewer";
import {CLScene} from "../../viewer/clim/CSSceneLayers";
import {EMenuMapItem, EStatusTreeItem} from "../../../v2d/const/Defines";
import {reject, split} from "lodash";
import {setTreeDisplay} from "../../../v2d/components/MapLayerFC";
import {ETextureLayer} from "../../const/Defines";
import {changeViewState} from "../../components/ChangeViewState";

export const TileLayerWidget = React.memo((props: {
    event: EventEmitter,
    viewer: Viewer,
    keyLayer: EMenuMapItem
}) => {
    const [expandedKeys, setExpandedKeys] = useState<Key[]>([]);
    const [checkedKeys, setCheckedKeys] = useState<Key[]>([]);
    const [selectedKeys, setSelectedKeys] = useState<Key[]>([]);
    const [treeData, setTreeData] = useState<IEDataNode[]>([]);
    const [keyMenuLayer, setKeyMenuLayer] = useState<EMenuMapItem>()
    const [reRenderTree, setReRenderTree] = useState(false);

    useEffect(() => {
        const newData = treeData;
        const newChecked = checkedKeys;

        if (props.viewer.tileLayers && props.viewer.tileLayers.children.length > 0) {
            props.viewer.tileLayers.children.forEach(value => {
                const key = value.id

                newChecked.push(key)

                newData.push({
                    key: key,
                    title: value.title,
                    className: "tree-parent",
                })
            })
        }

        if (props.viewer.sceneLayers.children.length > 0) {
            props.viewer.sceneLayers.children.forEach(value => {
                const key = value.id

                newChecked.push(key)

                newData.push({
                    key: key,
                    title: value.title,
                    className: "tree-parent",
                })
            })
        }
        setTreeData([...treeData])
        setCheckedKeys([...newChecked])

        props.event.on(DeckEventName.TileAdded, _onEventAddTile)
        props.event.on(DeckEventName.SceneAdded, _onEventAddScene)

        return () => {
            props.event.off(DeckEventName.TileAdded, _onEventAddTile)
            props.event.off(DeckEventName.SceneAdded, _onEventAddScene)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        setKeyMenuLayer(props.keyLayer)

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.keyLayer])

    useEffect(() => {
        if (keyMenuLayer) {
            selectMenuLayer(keyMenuLayer)
        }
        setReRenderTree(!reRenderTree);

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [treeData, keyMenuLayer])

    const selectMenuLayer = (key: EMenuMapItem) => {
        const displayTree = (treeKey: ETextureLayer) => {
            const dataDisplay = reject(treeData, function (item) {
                const itemKey = split(item.key.toString(), '-').shift();
                if (treeKey !== itemKey) {
                    return item
                }
            })

            const dataHide = reject(treeData, function (item) {
                const itemKey = split(item.key.toString(), '-').shift();

                if (treeKey === itemKey) {
                    return item
                }
            })

            dataDisplay.forEach((v1: IEDataNode) => {
                setTreeDisplay([v1], EStatusTreeItem.Display)
            })

            dataHide.forEach((v1: IEDataNode) => {
                setTreeDisplay([v1], EStatusTreeItem.None)
            })
        }

        switch (key) {
            case EMenuMapItem.Map3DLayer:
                displayTree(ETextureLayer.Tile)

                break;
            case EMenuMapItem.SceneLayer:
                displayTree(ETextureLayer.Scene)

                break;
        }
    }

    const _onEventAddTile = (evt: CLTile) => {
        console.log('onEventAddTile', evt);

        _createTreeNode(evt);
    }

    const _onEventAddScene = (evt: CLScene) => {
        console.log('onEventAddScene', evt);

        _createTreeNode(evt);
    }

    const _createTreeNode = (data: CLTile | CLScene) => {
        const newData: IEDataNode[] = treeData;
        const newChecked = checkedKeys;

        const key = data.id

        newData.push({
            key: key,
            title: data.title,
            className: "tree-parent",
        })

        setTreeData([...newData])

        newChecked.push(key)

        setCheckedKeys([...newChecked])
    }

    const onTreeSelect = (keys: Key[], info: IOTreeSelectInfo) => {
        setSelectedKeys([...keys])

        const keyNode = split(info.node.key as ETextureLayer, '-').shift();

        switch (keyNode) {
            case ETextureLayer.Tile:
                const tileSelected = props.viewer.tileLayers.getChildById(info.node.key);

                if (tileSelected) {
                    const tileSet3d = tileSelected.object.state.tileset3d;

                    changeViewState({
                        cartographicCenter: tileSet3d.cartographicCenter,
                        viewer: props.viewer
                    })
                }
                break;

            case ETextureLayer.Scene:
                const sceneSelected = props.viewer.sceneLayers.getSceneById(info.node.key.toString())

                if (sceneSelected) {
                    changeViewState({
                        cartographicCenter: sceneSelected.props.data[0].geometry.coordinates,
                        viewer: props.viewer
                    })
                }
        }
    }

    const onTreeCheck = (keys: Key[], info: IOTreeCheckInfo) => {
        setCheckedKeys([...keys]);

        const layerType = split(info.node.key.toString(), '-').shift() as ETextureLayer;

        switch (layerType) {
            case ETextureLayer.Tile:
                props.viewer.updateTileLayer(info.node.key, info.checked)

                break;
            case ETextureLayer.Scene:
                props.viewer.sceneLayers.updateSceneLayer(info.node.key, info.checked)

                break;
        }
    }

    const onTreeExpand = (expandedKeysValue: Key[]) => setExpandedKeys(expandedKeysValue);

    return (
        <div className={"p-5 pl-0"}>
            <Tree
                className={`tree-v2d-layer`}
                checkable
                expandedKeys={expandedKeys}
                checkedKeys={checkedKeys}
                selectedKeys={selectedKeys}
                onExpand={onTreeExpand}
                onCheck={onTreeCheck as unknown as any}
                onSelect={onTreeSelect}
                treeData={treeData}
            />
        </div>
    )
})
