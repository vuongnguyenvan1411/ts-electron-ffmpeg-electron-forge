import React, {useEffect, useRef, useState} from "react";
import EventEmitter from "eventemitter3";
import {Drawer, Menu, Tabs} from "antd";
import {MenuInfo} from "rc-menu/lib/interface";
import Image from "next/image";
import {Images} from "../../../../const/Images";
import {TileLayerWidget} from "./layer/TileLayerWidget";
import {Viewer} from "../viewer/Viewer";
import {useTranslation} from "react-i18next";
import {TParamPartGeodetic} from "../../../../const/Types";
import {EMenuMapItem} from "../../v2d/const/Defines";
import {GeodeticPartMenuFC} from "../../../widgets/GeodeticPartMenuFC";
import {BaseTileSwitcherFC} from "../components/BaseTileSwitcherFC";
import {CustomTypography} from "../../../components/CustomTypography";
import {OptionsMenu} from "../const/OptionsMenu";
import {DetectMedia} from "../../v2d/const/App";
import {CustomButton} from "../../../components/CustomButton";

export const LayerWidget = React.memo((props: {
    event: EventEmitter,
    viewer: Viewer,
    parts?: TParamPartGeodetic,
    m3dId: number
}) => {
    const {t} = useTranslation();
    const divMasterLayerHeader = document.getElementById('MasterLayerHeader');
    const headerHeightRef = useRef<number>(divMasterLayerHeader ? divMasterLayerHeader.offsetHeight : 0);
    const menuRef = useRef<Menu>();

    const [drawerTitle, setDrawerTitle] = useState<React.ReactNode>()
    const [isMapLayerVisible, setIsMapLayerVisible] = useState<{
        visible: boolean,
        keyLayer?: EMenuMapItem,
    }>({
        visible: false,
        keyLayer: EMenuMapItem.Map3DLayer,
    });
    const [isModalPartVisible, setIsModalPartVisible] = useState(false);


    useEffect(() => {
        if (isMapLayerVisible.keyLayer) {
            switch (isMapLayerVisible.keyLayer as EMenuMapItem) {
                case EMenuMapItem.Map3DLayer:
                    setDrawerTitle(t('text.3DVisualization'))

                    break;
                case EMenuMapItem.SceneLayer:
                    setDrawerTitle(t('text.sceneLayer'))

                    break;
                case EMenuMapItem.BaseLayer:
                    setDrawerTitle(t('text.baseMap'))

                    break;
            }
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isMapLayerVisible.keyLayer]);

    const onClickMenu = (evt?: MenuInfo) => {
        if (evt) {
            if (isMapLayerVisible.visible && evt.key === isMapLayerVisible.keyLayer) {
                setIsMapLayerVisible({visible: false})
            } else {
                if (evt.key === EMenuMapItem.Part) {
                    setIsModalPartVisible(true)
                } else {
                    setIsMapLayerVisible({
                        visible: true,
                        keyLayer: evt.key as EMenuMapItem
                    })

                    // switch (evt.key as EMenuMapItem) {
                    //     case EMenuMapItem.Map3DLayer:
                    //         setDrawerTitle(t('text.3DVisualization'))
                    //
                    //         break;
                    //     case EMenuMapItem.SceneLayer:
                    //         setDrawerTitle(t('text.sceneLayer'))
                    //
                    //         break;
                    //     case EMenuMapItem.BaseLayer:
                    //         setDrawerTitle(t('text.baseMap'))
                    //
                    //         break;
                    // }
                }
            }
        } else {
            setIsMapLayerVisible({
                ...isMapLayerVisible,
                visible: true,
                keyLayer: EMenuMapItem.Map3DLayer
            })
        }
    }

    return (
        <>
            {
                DetectMedia()
                    ? <>
                        <CustomButton
                            className={'btn-popover-menu-web tm3d'}
                            type={'text'}
                            onClick={() => onClickMenu()}
                            icon={
                                <Image
                                    src={Images.iconList.data}
                                    alt={Images.iconList.atl}
                                    width={24}
                                    height={24}
                                />
                            }
                        />
                    </>
                    : <Menu
                        style={{top: 0, left: 0}}
                        ref={menuRef as any}
                        theme={'dark'}
                        className={'menu-v2d menu-main-geodetic'}
                        items={OptionsMenu.mainMenuTm3d({
                            parts: props.parts,
                            t: t
                        })}
                        onClick={onClickMenu}
                    />
            }
            {
                <Drawer
                    className={'drawer-v2d-main'}
                    width={!DetectMedia() ? '400px' : '240px'}
                    title={<CustomTypography
                        textStyle={"text-20-24"}
                        isStrong
                        color={"#FFFFFF"}
                    >
                        {drawerTitle}
                    </CustomTypography>}
                    style={{
                        height: `calc(100% - ${headerHeightRef.current}px)`,
                        top: `${headerHeightRef.current}px`,
                        left: `${menuRef.current?.menu!.list.offsetWidth}px`,
                    }}
                    zIndex={1}
                    placement={"left"}
                    mask={false}
                    maskClosable={false}
                    bodyStyle={{
                        padding: 0
                    }}
                    forceRender
                    closable
                    closeIcon={(
                        <Image
                            height={24}
                            width={24}
                            src={Images.iconXWhite.data}
                            alt={Images.iconXWhite.atl}
                        />
                    )}
                    onClose={() => setIsMapLayerVisible({
                        ...isMapLayerVisible,
                        visible: false
                    })}
                    visible={isMapLayerVisible.visible}
                    headerStyle={{
                        backgroundColor: "#DE9F59"
                    }}
                >
                    {
                        DetectMedia() && <Tabs
                            onChange={(key: EMenuMapItem) => onClickMenu({key: key} as MenuInfo)}
                            className={'tabs-main ant-tabs-v2d menu-item tabs-tm3d'}
                            size={'small'}
                            defaultActiveKey={isMapLayerVisible.keyLayer}
                        >
                            {
                                OptionsMenu.mainMenuTm3dMobile(t).map(item => {
                                    return (
                                        <Tabs.TabPane
                                            tab={item.icon}
                                            key={item.key}
                                        />
                                    )
                                })
                            }
                        </Tabs>
                    }
                    {
                        isMapLayerVisible.keyLayer
                            ? isMapLayerVisible.keyLayer === EMenuMapItem.BaseLayer
                                ? <BaseTileSwitcherFC viewer={props.viewer}/>
                                : <TileLayerWidget
                                    keyLayer={isMapLayerVisible.keyLayer}
                                    event={props.event}
                                    viewer={props.viewer}
                                />
                            : null
                    }
                </Drawer>
            }
            {
                isModalPartVisible && (
                    <GeodeticPartMenuFC
                        parts={props.parts}
                        active={{
                            type: 'm3d',
                            id: props.m3dId
                        }}
                        isVisible={isModalPartVisible}
                        onClose={() => setIsModalPartVisible(false)}
                    />
                )
            }
        </>
    )
})
