import {CompositeLayer} from "@deck.gl/core";
import {CompositeLayerProps} from "@deck.gl/core/lib/composite-layer";

export class LayerGroup<D, P extends CompositeLayerProps<D> = CompositeLayerProps<D>> extends CompositeLayer<D, P> {
    constructor(props: P, ...additionalProps: P[]) {
        super(props, ...additionalProps);
    }

    renderLayers(): any {
        return super.renderLayers();
    }
}
