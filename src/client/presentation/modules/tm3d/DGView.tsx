import React, {useEffect, useMemo, useRef, useState} from "react";
import {Map3DModel} from "../../../models/service/geodetic/Map3DModel";
import {TParamPartGeodetic} from "../../../const/Types";
import {Main} from "./Main";
import EventEmitter from "eventemitter3";
import {Color} from "../../../const/Color";
import {Viewer} from "./viewer/Viewer";
import sms from "./styles/MapView.module.scss";

type TArgs = {
    event: EventEmitter,
    viewer: Viewer
}

const DGView = React.memo((props: {
    m3dId: number,
    item: Map3DModel,
    parts?: TParamPartGeodetic
}) => {
    const [args, setArgs] = useState<TArgs>()

    const divRef = useRef<HTMLDivElement>()

    const divMasterLayerHeader = document.getElementById('MasterLayerHeader')
    const headerHeightRef = useRef<number>(divMasterLayerHeader ? divMasterLayerHeader.offsetHeight : 0)

    useEffect(() => {
        console.log('%cMount Screen: DGView', Color.ConsoleInfo)

        const event = new EventEmitter()

        const viewer = new Viewer({
            event: event,
            model: props.item,
            layer: []
        })

        setArgs({
            event: event,
            viewer: viewer
        })

        return () => {
            console.log('%cUnmount Screen: DGView', Color.ConsoleInfo)

            viewer.destroy()
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const getMainFC = useMemo(() => {
        if (args) {
            return (
                <Main
                    m3dId={props.m3dId}
                    item={props.item}
                    parts={props.parts}
                    divMap={divRef.current}
                    viewer={args.viewer}
                    event={args.event}
                />
            )
        }

        return null

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [args])

    return (
        <div
            id={'MapContainer'}
            className={sms.MapContainer}
            style={{
                width: "100%",
                height: `calc(100vh - ${headerHeightRef.current}px)`
            }}
        >
            {getMainFC}
        </div>
    )
})

export default DGView
