import EventEmitter from "eventemitter3";
import {ScenegraphLayer} from "@deck.gl/mesh-layers";
import {CLInterface, IOptCL} from "./CLInterface";
import {DeckEventName} from "../../const/Events";
import {Key} from "react";
import {filter, find} from "lodash";
import {GLTFLoader} from '@loaders.gl/gltf';
import {PitchYawRoll} from "@deck.gl/core/utils/positions";

type SceneOpts = {
    event: EventEmitter;
}

export type TFeature = {
    geometry: {
        coordinates: any[],
        type: string,
    },
    type: "Feature",
    properties: {
        altitude: number,
        id: string,
        orientation: number[],
        scale: number,
        url?: string,
    }
}

export type TValuesScene = {
    id: string
    data: TFeature,
    url: string,
    scale: number,
    name: string,
    orientation: number[],
}

export class CSSceneLayers extends EventEmitter {
    event: EventEmitter;
    children: CLScene[] = [];
    sceneGroupLayers: ScenegraphLayer<any>[] = [];
    position: any;

    constructor(opts: SceneOpts) {
        super();

        this.event = opts.event
    }

    setLayer(value: TValuesScene) {
        if (value.data) {
            const scenegraphLayer = new ScenegraphLayer({
                id: value.id,
                loaders: [GLTFLoader],
                loadOptions: {
                    gltf: {
                        normalize: true,
                        loadBuffers: true,
                    }
                },
                scenegraph: value.url as any,
                data: [value.data],
                getPosition: (f: any) => f.geometry.coordinates,
                opacity: 1,
                sizeScale: value.scale * 10,
                // Rotate with YZ, with XY, with XZ
                getOrientation: value.orientation as PitchYawRoll,
                getTranslation: [5, 0, 20],
                visible: true,
                positionFormat: 'XYZ',
                _lighting: 'pbr',
                wrapLongitude: true,
                
            });

            const sceneObject = {
                id: value.id,
                title: value.name,
                sceneLayer: scenegraphLayer,
                feature: value.data,
            }

            if (value.id !== 'axis') {
                this.sceneGroupLayers.push(scenegraphLayer)
            }

            this.children.push(sceneObject as CLScene);

            this.event.emit(DeckEventName.SceneAdded, sceneObject)
        }
    }

    updateSceneLayer(key: Key, visible: boolean) {
        const newSceneGroup: ScenegraphLayer<any>[] = this.sceneGroupLayers

        this.sceneGroupLayers.forEach((item: ScenegraphLayer<any>, index) => {

            if (item.id === key) {
                // @ts-ignore
                newSceneGroup[index] = item.clone({
                    visible: visible
                })
            }

            this.sceneGroupLayers = newSceneGroup

            this.event.emit(DeckEventName.SceneChanged)
        })
    }

    getSceneById(id: string) {
        if (this.sceneGroupLayers.length > 0) {
            return find(this.sceneGroupLayers, function (_) {
                return _.id === id
            })
        }
    }

    getChildById(id: string) {
        if (this.children.length > 0) {
            return find(this.children, function (_) {
                return _.id === id
            })
        }
    }

    removeSceneById(id: string) {
        const newChild = filter(this.children, function (_) {
            return _.id !== id
        })

        const newScene = filter(this.sceneGroupLayers, function (_) {
            return _.id !== id
        })

        this.children = newChild
        this.sceneGroupLayers = newScene

        this.event.emit(DeckEventName.SceneChanged)
    }
}

export interface IOptCLScene extends IOptCL {
    sceneLayer: ScenegraphLayer<any>
    feature: TFeature
}

export class CLScene extends CLInterface {
    sceneLayer: ScenegraphLayer<any>
    feature: TFeature

    constructor(opts: IOptCLScene) {
        super(opts)

        this.sceneLayer = opts.sceneLayer
        this.feature = opts.feature
    }
}

