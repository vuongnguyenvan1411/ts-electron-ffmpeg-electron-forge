import EventEmitter from "eventemitter3";
import {BASEMAP} from "@deck.gl/carto/typed";
import {DeckEventName} from "../../const/Events";

type BOpts = {
    event: EventEmitter;
}

export class CSBaseLayers extends EventEmitter {
    event: EventEmitter;
    keyLayer: string;
    visible: boolean;

    constructor(opts: BOpts) {
        super();

        this.event = opts.event
        this.keyLayer = BASEMAP.VOYAGER
        this.visible = true

    }

    setLayer(key: string) {
        this.keyLayer = key

        this.event.emit(DeckEventName.BaseChanged, {key: this.keyLayer, visible: true})
    }

    setVisible(status: boolean) {
        this.visible = status

        this.event.emit(DeckEventName.BaseChanged, {visible: false})
    }
}
