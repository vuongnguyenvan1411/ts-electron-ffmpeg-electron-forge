import {CompositeLayer, COORDINATE_SYSTEM} from "@deck.gl/core";
import {Map3DModel} from "../../../../../models/service/geodetic/Map3DModel";
import {Tile3DLayer} from "@deck.gl/geo-layers";
import EventEmitter from "eventemitter3";
import {DeckEventName} from "../../const/Events";
import {CLInterface, IOptCL} from "./CLInterface";
import {Key} from "react";
import {I3SLoader} from "@loaders.gl/i3s";
import {Tileset3D} from "@loaders.gl/tiles";

export type _TOpts = {
    id?: string
    item: Map3DModel
    event: EventEmitter
    onTilesetLoad?: (tileData: Object) => void
    onTileLoad?: (tileHeader: Object) => void
    onTileUnload?: (tileHeader: Object) => void
    onTileError?: (tileHeader: Object, url: string, message: string) => void
}

export class CSTileLayers extends CompositeLayer<any> {
    opts: _TOpts
    children: CLTile[] = []
    tileSets: Tileset3D[] = []

    constructor(opts: _TOpts) {
        super({
            id: opts.id ?? 'tiles'
        });

        this.opts = opts;
    }

    renderLayers = (): Tile3DLayer<any>[] => {
        const loadOptions = {
            i3s: {
                coordinateSystem: COORDINATE_SYSTEM.LNGLAT_OFFSETS,
                loadContent: true,
                tileset: {
                    lodMetricType: {
                        metricType: {
                            metricType: "maxScreenThreshold",
                            maxError: 34.87550189480981
                        }
                    }
                }
            },
        }

        if (this.opts.item.tm && this.opts.item.tm.length > 0) {
            const tileLayers: Tile3DLayer<any>[] = []

            this.opts.item.tm.map((item, index) => {
                const object = new Tile3DLayer({
                    id: `tile-${item.id}`,
                    data: `https://cdn-gdt.autotimelapse.com/v2/r/i3s/${item.id}/SceneServer/layers/0`,
                    loader: I3SLoader,
                    loadOptions,
                    onTilesetLoad: this.opts.onTilesetLoad,
                    onTileLoad: this.opts.onTileLoad,
                    onTileUnload: this.opts.onTileUnload,
                    onTileError: this.opts.onTileError,
                    wrapLongitude: true,
                    _subLayerProps: {
                        scenegraph: {
                            _lighting: "flat",
                        },
                    },
                    filterEnabled: true,
                    parameters: {
                        depthTest: true
                    },
                    positionFormat: 'XYZ',
                    visible: true,
                })

                const data = new CLTile({
                    id: item.id ? `tile-${item.id}` : index.toString(),
                    title: item.name ?? `Tile-${index}`,
                    object: object
                })

                tileLayers.push(object)
                this.children.push(data)
                this.opts.event.emit(DeckEventName.TileAdded, data);
            })

            return tileLayers
        }

        return [];
    }

    addTileSets = (tileset: Tileset3D) => {
        this.tileSets = [...this.tileSets, tileset]
    }

    addChild = (opts: IOptCLTile, isAddMap: boolean) => {
        const tile = new CLTile(opts)

        this.children.push(tile)

        if (isAddMap) {
            this.opts.event.emit(DeckEventName.TileAdded, tile);
        }
    }

    getChildById = (id: Key) => {
        return this.children.find(item => item.id === id)
    }
}

export interface IOptCLTile extends IOptCL {
    object: Tile3DLayer<any>
}

export class CLTile extends CLInterface {
    object: Tile3DLayer<any>

    constructor(opts: IOptCLTile) {
        super(opts)

        this.object = opts.object
    }
}

