export interface IOptCL {
    id: string;
    title: string;
    properties?: Record<string, any>
}

export interface CLT {
    getVisible: () => boolean;
    setVisible: (visible: boolean) => void;
}

export class CLInterface {
    id: string;
    title: string;

    protected _properties: Record<string, any>;

    constructor(opts: IOptCL) {
        this.id = opts.id;
        this.title = opts.title;

        if (opts.properties) {
            this._properties = opts.properties;
        } else {
            this._properties = {};
        }
    }

    get(key: string, df?: any) {
        if (this._properties.hasOwnProperty(key)) {
            df = this._properties[key];
        }

        return df;
    }

    set(key: string, value: any) {
        return this._properties[key] = value;
    }

    getKeys() {
        return Object.keys(this._properties);
    }

    getProperties() {
        return this._properties;
    }

    setProperties(data: Record<string, any>) {
        return this._properties = data;
    }

    hasProperties(key?: string) {
        if (key) {
            return this._properties.hasOwnProperty(key);
        } else {
            return this.getKeys.length > 0;
        }
    }
}
