import EventEmitter from "eventemitter3";
import {Deck} from "@deck.gl/core";
import {Map3DModel} from "../../../../models/service/geodetic/Map3DModel";
import {CSTileLayers} from "./clim/CSTileLayers";
import {Layer} from "deck.gl";
import {CSBaseLayers} from "./clim/CSBaseLayers";
import {Key} from "react";
import {Tile3DLayer} from "@deck.gl/geo-layers";
import {DeckEventName} from "../const/Events";
import {LayerProps} from "@deck.gl/core/lib/layer";
import {CSSceneLayers} from "./clim/CSSceneLayers";

type _TOps = {
    event: EventEmitter,
    model: Map3DModel,
    layer: []
}

export class Viewer extends EventEmitter {
    event: EventEmitter
    deck?: Deck
    layers: Layer<any, LayerProps<any>>[]

    tileLayers: CSTileLayers
    baseLayers: CSBaseLayers
    sceneLayers: CSSceneLayers

    constructor(opts: _TOps) {
        super();

        this.event = opts.event;
        this.baseLayers = new CSBaseLayers(opts)
        this.sceneLayers = new CSSceneLayers(opts)
        this.layers = []
    }

    setDeck = (deck: Deck) => {
        this.deck = deck
    }

    setTileLayers = (tileLayers: CSTileLayers) => {
        this.tileLayers = tileLayers
    }

    addLayer = (layers: Layer<any>) => {
        this.layers.push(layers)
    }

    updateTileLayer = (key: Key, visible: boolean) => {
        const newSubLayers: Tile3DLayer<any>[] = this.tileLayers.internalState.subLayers as Tile3DLayer<any>[]

        for (let i = 0; i < newSubLayers.length; i++) {

            const layer = newSubLayers[i]

            if (layer.id === key) {
                newSubLayers[i] = layer.clone({
                    visible: visible
                })
            }
        }

        this.tileLayers.internalState.subLayers = newSubLayers

        this.event.emit(DeckEventName.TileChanged, [this.tileLayers, this.sceneLayers.sceneGroupLayers])
    }

    destroy = () => {
        this.event.removeAllListeners();

        if (this.deck) {
            this.deck.canvas.remove()
        }
    }
}
