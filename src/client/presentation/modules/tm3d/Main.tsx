import React, {useEffect, useRef, useState} from "react";
import {Map3DModel} from "../../../models/service/geodetic/Map3DModel";
import {TParamPartGeodetic} from "../../../const/Types";
import {LinearInterpolator} from "@deck.gl/core";
import {registerLoaders} from "@loaders.gl/core";
import {DracoWorkerLoader} from "@loaders.gl/draco";
import {ViewStateProps} from "@deck.gl/core/lib/deck";
import EventEmitter from "eventemitter3";
import {DeckEventName} from "./const/Events";
import {App} from "./const/App";
import {Viewer} from "./viewer/Viewer";
import {LayerWidget} from "./widgets/LayerWidget";
import {LoadTile} from "./components/LoadTile";
import 'mapbox-gl/dist/mapbox-gl.css';
import {Layer} from "deck.gl";
import {LayerProps} from "@deck.gl/core/lib/layer";
import {TranslateMode} from "@nebula.gl/edit-modes";
import {CLScene} from "./viewer/clim/CSSceneLayers";
import {reject} from "lodash";
import {ScenegraphLayer} from "@deck.gl/mesh-layers";
import {LoadScene} from "./components/LoadScene";
import {EMenuTool, TEditScene, TMbs, TModifyScene} from "./const/Defines";
import {FormEditSceneLayer} from "./components/FormEditSceneLayer";
import {MapToolFC} from "./components/MapToolFC";
import {DeckGLFC} from "./components/DeckGLFC";
import {Tileset3D} from "@loaders.gl/tiles";

registerLoaders([DracoWorkerLoader]);

export const Main = React.memo((props: {
    m3dId: number,
    item: Map3DModel,
    parts?: TParamPartGeodetic,
    divMap?: HTMLDivElement
    viewer: Viewer,
    event: EventEmitter,
}) => {
    const [viewState, setViewState] = useState<ViewStateProps>(App.INITIAL_VIEW_STATE)
    const [mapLayers, setMapLayers] = useState<Layer<any, LayerProps<any>>[] | undefined>([])
    const [isInit, setIsInit] = useState(false)
    const [isModifyDrawer, setIsModifyDrawer] = useState<{
        initValue?: TModifyScene,
        type?: EMenuTool,
        visible: boolean
    }>({
        visible: false,
        type: EMenuTool.Modify
    })

    const divMasterLayerHeader = document.getElementById('MasterLayerHeader');
    const headerHeightRef = useRef<number>(divMasterLayerHeader ? divMasterLayerHeader.offsetHeight : 0);

    const mbsLimit = () => {
        const limit = limitRef.current

        if (!limit) {
            return
        }

        return {
            lng: (limit.lng.reduce((a, b) => a + b, 0)) / (limit.lng.length),
            lat: (limit.lat.reduce((a, b) => a + b, 0)) / (limit.lat.length),
            max: (limit.max.reduce((a, b) => a + b, 0)) / (limit.max.length),
            min: (limit.min.reduce((a, b) => a + b, 0)) / (limit.min.length),
        }
    }

    const limitRef = useRef<{
        lng: number[],
        lat: number[],
        min: number[],
        max: number[]
    }>({lng: [], lat: [], min: [], max: []})

    useEffect(() => {
        setIsInit(true)

        LoadTile({
            event: props.event,
            viewer: props.viewer,
            item: props.item,
        })

        setMapLayers(props.viewer.layers)

        const handleResize = () => {
            setViewState((v) => {
                return {
                    ...v,
                    width: window.innerWidth,
                    height: window.innerHeight
                };
            });
        };
        window.addEventListener("resize", handleResize);

        props.event.on(DeckEventName.TilesetLoad, _onTilesetLoad)
        props.event.on(DeckEventName.TileChanged, _onTileLayerChange);

        return () => {
            window.removeEventListener("resize", handleResize);

            props.event.off(DeckEventName.TilesetLoad, _onTilesetLoad)
            props.event.off(DeckEventName.TileChanged, _onTileLayerChange);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        props.event.on(DeckEventName.SceneAdded, _onSceneAdded);
        props.event.on(DeckEventName.SceneChanged, _onSceneChanged);

        return () => {
            props.event.off(DeckEventName.SceneAdded, _onSceneAdded);
            props.event.off(DeckEventName.SceneChanged, _onSceneChanged);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [mapLayers])

    useEffect(() => {
        if (isInit) {
            LoadScene({
                item: props.item,
                viewer: props.viewer
            })
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isInit])

    const onViewStateChange = ({viewState: viewState}) => setViewState(viewState)

    const _onTilesetLoad = (tileset: Tileset3D) => {

        tileset.tileset.root.then(res => {
            const mbs = res.mbs

            const [lng, lat, min, max] = mbs

            limitRef.current.lng.push(lng)
            limitRef.current.lat.push(lat)
            limitRef.current.min.push(min)
            limitRef.current.max.push(max)
        })

        setViewState({...viewState})
    }

    const _onSceneAdded = (object: CLScene) => {
        if (mapLayers && mapLayers.length > 0) {
            setMapLayers([...mapLayers, ...[object.sceneLayer]])
        }
    }

    const _onSceneChanged = () => {
        if (mapLayers && mapLayers.length > 0) {
            const tileLayer = reject(mapLayers, function (_: any) {
                return _ instanceof ScenegraphLayer
            })
            setMapLayers([...tileLayer, ...props.viewer.sceneLayers.sceneGroupLayers])
        }
    }

    const onSelectGeoJsonLayer = (agrs: {
        value: TEditScene,
        type: EMenuTool
    }) => {
        const selectedFeature = agrs.value.scene.split('-').pop();

        if (selectedFeature) {
            props.event.emit(DeckEventName.FeatureChanged, true)
            props.event.emit(DeckEventName.FeatureSelected, [parseFloat(selectedFeature)])

            setIsModifyDrawer({visible: false})
            onModifyScene({key: agrs.type as EMenuTool, mbs: mbsLimit()!})
        }
    }

    const _onTileLayerChange = (layers: Layer<any, LayerProps<any>>[] | undefined) => setMapLayers(layers)

    const onModifyScene = (item: { key: EMenuTool, mbs: TMbs }) => {
        const changeViewState = {
            ...viewState,
            latitude: item.mbs.lat,
            longitude: item.mbs.lng,
            transitionDuration: 2000,
            transitionInterpolator: new LinearInterpolator(),
        }

        switch (item.key) {
            case EMenuTool.Translate:
                props.event.emit(DeckEventName.FeatureChanged, true)

                props.event.emit(DeckEventName.FeatureMode, {
                    mode: new TranslateMode(),
                    modeConfig: {
                        maxElevation: item.mbs.min,
                        minElevation: item.mbs.min,
                    }
                })

                setViewState({
                    ...changeViewState,
                    bearing: 0,
                })

                break;
            case EMenuTool.Rotation:
                props.event.emit(DeckEventName.FeatureChanged, true)

                props.event.emit(DeckEventName.FeatureMode, {
                    mode: new TranslateMode(),
                    modeConfig: {
                        maxElevation: item.mbs.min,
                        minElevation: item.mbs.min,
                    }
                })

                setViewState({
                    ...changeViewState,
                    bearing: 180,
                })

                break;
            case EMenuTool.Modify:
                props.event.emit(DeckEventName.FeatureChanged, false)

                setViewState({
                    ...changeViewState,
                    bearing: 180,
                })

                if (props.viewer.sceneLayers.children.length > 0) {
                    const defaultScene = props.viewer.sceneLayers.children[0];

                    setIsModifyDrawer({
                        visible: true,
                        type: EMenuTool.Modify,
                        initValue: {
                            sceneName: defaultScene.title,
                            sceneId: defaultScene.id,
                            feature: defaultScene.feature,
                            orientation: defaultScene.feature.properties.orientation,
                            scale: defaultScene.feature.properties.scale
                        }
                    })
                }

                break;
        }
    }

    return (
        <div onContextMenu={evt => evt.preventDefault()}>
            <canvas
                id="mapCanvas"
            />
            <LayerWidget{...props}/>
            <DeckGLFC
                {...props}
                mapLayers={mapLayers ?? []}
                viewState={viewState}
                onViewStateChange={onViewStateChange}
            />
            <MapToolFC
                {...props}
                mbs={mbsLimit()!}
                onModifyScene={onModifyScene}
                setIsModifyDrawer={setIsModifyDrawer}
            />
            <FormEditSceneLayer
                {...props}
                mbs={mbsLimit()!}
                type={isModifyDrawer.type}
                initValue={isModifyDrawer.initValue}
                headerHeightRef={headerHeightRef.current}
                onClose={() => {
                    setIsModifyDrawer({visible: false})
                    props.viewer.sceneLayers.removeSceneById('axis')
                }}
                visible={isModifyDrawer.visible}
                onSelectGeoJsonLayer={onSelectGeoJsonLayer}
            />
        </div>
    )
})
