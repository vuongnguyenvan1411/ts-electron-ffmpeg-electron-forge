import {Coordinate} from "ol/coordinate";
import {Extent} from "ol/extent";
import {Size} from "ol/size";
import {OSM, TileImage, XYZ} from "ol/source";
import {App as RootApp} from "../../../../const/App";
import {EBaseMap} from "./Defines";
import proj4 from "proj4";
import TileLayer from "ol/layer/Tile";
import IconTraffic from "../../../../assets/image/v2d/ic_traffic-2x.png";
import IconTerrain from "../../../../assets/image/v2d/ic_terrain-2x.png";
import IconSatellite from "../../../../assets/image/v2d/ic_satellite-2x.png";
import IconOSM from "../../../../assets/image/v2d/ic_osm-2x.png";
import IconNoBase from "../../../../assets/image/v2d/ic_no_basemap.png";
import {FeatureSingleton} from "../../../../models/FeatureSingleton";
import {KML} from "ol/format";

export class App {
    static UrlCdnScIconMarker = `https://cdn-sc.${RootApp.Host}/files/icon/marker`;

    static DefaultEPSG = 'EPSG:4326';
    static EPSG4236Reverse = proj4.defs("EPSG:3346", "+proj=tmerc +lat_0=0 +lon_0=24 +k=0.9998 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +axis=neu +no_defs");

    static shadedReliefLayer = new TileLayer({
        opacity: 0.3,
        source: new XYZ({
            url: "https://{a-d}.tiles.mapbox.com/v3/aj.sf-dem/{z}/{x}/{y}.png"
        })
    })

    static BaseLayerGroup: TileLayer<TileImage>[] = [
        new TileLayer({
            source: new XYZ(),
            properties: {
                type: 'base',
                id: EBaseMap.None,
                img: IconNoBase
            }
        }),
        new TileLayer({
            visible: true,
            preload: Infinity,
            source: new XYZ({
                url: 'https://mt0.google.com/vt/lyrs=m&hl=vi&x={x}&y={y}&z={z}',
                crossOrigin: '',
                transition: 0,
            }),
            // @ts-ignore
            properties: {
                type: 'base',
                title: 'Google Roadmap',
                id: EBaseMap.GoogleRoadmap,
                img: IconTraffic
            }
        }),
        new TileLayer({
            visible: false,
            preload: Infinity,
            source: new XYZ({
                url: 'https://mt0.google.com/vt/lyrs=p&hl=vi&x={x}&y={y}&z={z}',
                crossOrigin: '',
                transition: 0,
            }),
            // @ts-ignore
            properties: {
                type: 'base',
                title: 'Google Terrain',
                id: EBaseMap.GoogleTerrain,
                img: IconTerrain,
            }
        }),
        new TileLayer({
            visible: false,
            preload: Infinity,
            source: new XYZ({
                url: 'https://mt0.google.com/vt/lyrs=s&hl=vi&x={x}&y={y}&z={z}',
                crossOrigin: '',
                transition: 0,
            }),
            // @ts-ignore
            properties: {
                type: 'base',
                title: 'Google Satellite only',
                id: EBaseMap.GoogleSatelliteOnly,
                img: IconSatellite
            }
        }),
        new TileLayer({
            visible: false,
            preload: Infinity,
            source: new OSM({
                crossOrigin: '',
                transition: 0,
            }),
            // @ts-ignore
            properties: {
                type: 'base',
                title: 'OpenStreet Map',
                id: EBaseMap.OpenStreetMap,
                img: IconOSM,
            },
        }),
    ];

    static GoogleMapKey = 'AIzaSyA7lClxgJVRg0xqLXjFlf4MDe-OYiidd1w';

    static mapMinZoom: number = 1;
    static mapMaxZoom: number = 24;
    static defaultZoom: number = 15;
    static defaultTileGridExtent: Extent = [-180, -90, 180, 90];
    static defaultTileGridOrigin: Coordinate = [-180, -90];
    static defaultMaxTileResolution: number = 0.00034332275390625;
    static defaultTileGridResolutions: number[] = [1.40625, 0.703125, 0.3515625, 0.17578125, 0.087890625, 0.0439453125, 0.02197265625, 0.010986328125, 0.0054931640625, 0.00274658203125, 0.001373291015625, 0.0006866455078125, 0.00034332275390625, 0.000171661376953125, 8.58306884765625e-05, 4.291534423828125e-05, 2.1457672119140625e-05, 1.07288360595703125e-05, 5.36441802978515625e-06, 2.68220901489257812e-06, 1.34110450744628906e-06, 6.70552253723144531e-07];
    static defaultTileSize: Size = [256, 256];
    static defaultContourStyle: string = 'Contour';
    static defaultCenter: Coordinate = [105.84308235369714, 21.006284117960863];
    static readonly TimeoutHideCopy: number = 4500;
}

export const DetectMedia = () => {
    if (FeatureSingleton.getInstance().isContributor) {
        return window.matchMedia('screen and (max-width: 1194px)').matches || false;
    } else {
        return true
    }
}

export class KMZ extends KML {

}
