import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import {Geometry} from "ol/geom";
import {DataNode, EventDataNode} from "rc-tree/lib/interface";
import React, {Key} from "react";
import LayerGroup from "ol/layer/Group";
import {CLTile} from "../viewer/clim/CLTileTool";
import {CLContour} from "../viewer/clim/CLContourTool";
import {CLMarker} from "../viewer/clim/CLMarkerTool";
import {CLMeasure} from "../viewer/clim/CLMeasureTool";
import {CLProfile} from "../viewer/clim/CLProfileTool";
import {CLShareRegion} from "../viewer/clim/CLShareRegionTool";

export interface IEDataNode extends DataNode {
    object?: LayerGroup | VectorLayer<VectorSource<Geometry>> | CLTile | CLContour | CLMarker | CLMeasure | CLProfile | CLShareRegion;
    children?: IEDataNode[];
    expanded?: boolean;
    pos?: string;
}

export interface IEEventDataNode extends EventDataNode<any> {
    object?: LayerGroup | VectorLayer<VectorSource<Geometry>> | CLTile | CLContour | CLMarker | CLMeasure | CLProfile;
    props?: IEDataNode;
    node: IEEventDataNode;
    checked: boolean;
    children?: IEDataNode[];
    key: Key;
    selected: boolean;
    className: string;
    pos: string;
}

export interface IOTreeCheckInfo {
    event: 'check';
    node: IEEventDataNode;
    checked: boolean;
    nativeEvent: MouseEvent;
    checkedNodes: IEDataNode[];
    checkedNodesPositions?: {
        node: IEDataNode;
        pos: string;
    }[];
    halfCheckedKeys?: Key[];
}

export interface IOTreeSelectInfo {
    event: 'select';
    selected: boolean;
    node: EventDataNode<IEDataNode>;
    selectedNodes: IEDataNode[];
    nativeEvent: MouseEvent;
}

export interface IEVentDragNode {
    event: React.DragEvent;
    node: IEEventDataNode;
}

export interface IEEventDropNode extends IEVentDragNode {
    dragNode: IEEventDataNode;
    dragNodesKeys: Key[];
    dropPosition: number;
    dropToGap: boolean;
}

export enum EMapLayer {
    BaseMap = "BaseMap",
    Tile = "Tile",
    Contour = "Contour",
    Marker = "Marker",
    Measure = "Measure",
    Profile = "Profile",
    ShowLocation = "ShowLocation",
    TrackLocation = "TrackLocation",
    ShareRegion = "ShareRegion",
    Upload = 'Upload'
}

export enum EBaseMap {
    None,
    GoogleRoadmap,
    GoogleTerrain,
    GoogleSatelliteOnly,
    OpenStreetMap
}

export enum EDrawType {
    LineString = 'LineString',
    Polygon = 'Polygon',
    Circle = 'Circle',
    GeometryCollection = 'GeometryCollection',
    Point = "Point",
    Region = 'Region',
}

export enum EProfileType {
    Draw = 'pDraw',
    Marker = 'pMarker',
    Annotation = 'pAnnotation',
    LineString = 'pLineString',
    Polygon = 'pPolygon',
    Circle = 'pCircle',
    Share = 'pShare',
    Upload = 'pUpload'
}

export enum EMarkerType {
    Vr360 = 'vr360',
}

export type TParamTableCoord = {
    x: number,
    y: number,
    key?: number,
}

export enum EPropertiesType {
    Number = 'number',
    String = 'string',
    Date = 'date',
}

export enum EStatusTreeItem {
    None = 'none',
    Display = 'flex'
}

export enum ESnapType {
    Edge = 'Edge',
    Middle = 'Middle',
    Center = 'Center',
    Vertex = 'Vertex',
    Intersect = 'Intersect',
}

export enum EPlaceBlock {
    None,
    Triangle,
    Square,
    Rectangle,
    Oblique
}

export enum EDrawingStyle {
    Default,
    FreeHand,
    Curve,
    Arcs,
}

export enum EDrawingWeight {
    Default,
    Bold
}

export enum EDrawingDashed {
    Default,
    Dashed
}

export type TColumnAttribute = {
    title: string;
    dataIndex: string;
    render?: (_: any, record: any) => JSX.Element;
    editable?: boolean;
    key: string,
    type?: string,
    width?: string,
    fixed?: string,
    sorter?: (a: any, b: any) => void,
    sortOrder?: any,
    ellipsis?: boolean,
}

export enum EEditType {
    Rotate = 'Rotate',
    Copy = 'Copy',
    Move = 'Move',
    Trim = 'Trim',
    Clip = 'Clip',
}

export enum EMenuMapItem {
    Part = 'Part',
    BaseLayer = 'BaseLayer',
    MeasureLayer = 'MeasureLayer',
    ProfileLayer = 'ProfileLayer',
    ContourLayer = 'ContourLayer',
    Map2DLayer = 'Map2dLayer',
    Map3DLayer = 'Map3dLayer',
    SceneLayer = 'SceneLayer'
}

export enum EToolMenuMap {
    TrackLocation = 'TrackLocation',
    ShowLocation = 'ShowLocation',
    Profile = 'Profile',
    Measure = 'Measure',
    Tools = 'Tools',
    Capture = 'Capture',
    Share = 'Share',
    Save = 'Save',
    Snap = 'Snap',
    Contour = 'Contour',
    Rotation = 'Rotation',
    KMLFile = 'KMLFile',
    Reset = 'Reset'
}

export enum EContextMap {
    Copy = 'Copy',
    Center = 'Center',
    Refresh = 'Refresh',
    Marker = 'Marker',
}

export enum EToolsEdit {
    Select = 'Select',
    Edit = 'Edit',
    Delete = 'Delete',
    Save = 'Save',
    AddFeature = 'AddFeature',
    EditGeometry = 'EditGeometry',
    EditCharacteristics = 'EditCharacteristics',
    OpenAttribute = 'OpenAttribute',
}

export enum EContourMap {
    WFS = 'wfs',
    WMS = 'wms',
}

export enum EContourGeom {
    Linestring = 'line',
    Annotation = 'ano'
}

export enum EFileType {
    KML = 'kml',
    GeoJSON = 'geojson',
    GPX = 'gpx',
    IGC = 'igc',
}
