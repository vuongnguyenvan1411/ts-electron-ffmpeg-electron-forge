import Image from "next/image";
import {Images} from "../../../../const/Images";
import {EContextMap, EDrawType, EEditType, EMenuMapItem, EPlaceBlock, EProfileType, ESnapType, EToolMenuMap, EToolsEdit} from "./Defines";
import React from "react";
import sms from "../styles/MapView.module.scss";
import {Tooltip} from "antd";
import {Viewer} from "../viewer/Viewer";
import {TFunction} from "react-i18next";
import {RouteAction} from "../../../../const/RouteAction";
import {NavigateFunction} from "react-router";
import {TParamPartGeodetic} from "../../../../const/Types";
import {FeatureSingleton} from "../../../../models/FeatureSingleton";
import {DetectMedia} from "./App";

export class OptionsMenu {
    static readonly placeBlockOptions = [
        {
            label: <Image
                src={Images.iconSquare.data}
                alt={Images.iconSquare.atl}
                width={16}
                height={20}
            />,
            value: EPlaceBlock.Square,
        },
        {
            label: <Image
                src={Images.iconRectangle.data}
                alt={Images.iconRectangle.atl}
                width={20}
                height={20}
            />,
            value: EPlaceBlock.Rectangle,
        },
        {
            label: <Image
                src={Images.iconTriangle.data}
                alt={Images.iconTriangle.atl}
                width={20}
                height={20}
            />,
            value: EPlaceBlock.Triangle,
        },
        {
            label: <Image
                src={Images.iconEllipse.data}
                alt={Images.iconEllipse.atl}
                width={20}
                height={20}
            />,
            value: EPlaceBlock.Oblique,
        },
    ];

    static readonly menuToolsHeader = (props: {
        viewer: Viewer,
        t: TFunction<"translation">,
        isDrawing: boolean
    }) => (
        DetectMedia()
            ? [
                {
                    key: EToolMenuMap.Contour,
                    icon: <Image
                        src={Images.iconContour.data}
                        alt={Images.iconContour.atl}
                        height={20}
                        width={20}
                        priority={true}
                    />,
                    disabled: props.viewer.contourTool.children.length < 1,
                },
                {
                    key: EToolMenuMap.Rotation,
                    icon: <Image
                        height={20}
                        width={20}
                        src={Images.iconArrowUpRight.data}
                        alt={Images.iconArrowUpRight.atl}
                    />,
                },
                {
                    key: EToolMenuMap.ShowLocation,
                    icon: <Image
                        height={20}
                        width={20}
                        src={Images.iconShowLocation.data}
                        alt={Images.iconShowLocation.atl}
                    />,
                },
                {
                    key: EToolMenuMap.TrackLocation,
                    icon: <Image
                        height={20}
                        width={20}
                        src={Images.iconTrackLocation.data}
                        alt={Images.iconTrackLocation.atl}
                    />,
                },
                {
                    key: EToolMenuMap.Measure,
                    icon: <Image
                        height={20}
                        width={20}
                        src={Images.iconMeasure.data}
                        alt={Images.iconMeasure.atl}
                    />,
                    popupClassName: `${sms.SubMenu}`,
                    popupOffset: [10, -10],
                    children: [
                        {
                            label: props.t('text.measureLine'),
                            key: EDrawType.LineString,
                            icon: <Image
                                height={16}
                                width={16}
                                src={Images.iconLine.data}
                                alt={Images.iconLine.atl}
                            />,
                        },
                        {
                            label: props.t('text.measureArea'),
                            key: EDrawType.Polygon,
                            icon: <Image
                                height={16}
                                width={16}
                                src={Images.iconPolygon.data}
                                alt={Images.iconPolygon.atl}
                            />,
                        },
                        {
                            label: props.t('text.measureCircle'),
                            key: EDrawType.Circle,
                            icon: <Image
                                height={16}
                                width={16}
                                src={Images.iconCircle.data}
                                alt={Images.iconCircle.atl}
                            />,
                        }
                    ]
                },
                {
                    key: EToolMenuMap.Reset,
                    icon: <Image
                        src={Images.iconCloseCircle.data}
                        alt={Images.iconCloseCircle.atl}
                        height={20}
                        width={20}
                    />,
                }
            ]
            : (
                FeatureSingleton.getInstance().v2DShare
                    ? [
                        {
                            key: EToolMenuMap.Rotation,
                            icon: <Image
                                height={20}
                                width={20}
                                src={Images.iconArrowUpRight.data}
                                alt={Images.iconArrowUpRight.atl}
                            />,
                        },
                        {
                            key: 'divider-0',
                            icon: <Image
                                src={Images.iconDivider.data}
                                alt={Images.iconDivider.atl}
                                width={0}
                                height={0}
                            />,
                            style: {
                                pointerEvents: 'none',
                            },
                        },
                        {
                            key: EToolMenuMap.ShowLocation,
                            icon:
                                <Tooltip
                                    title={props.t("text.showLocation")}
                                >
                                    <Image
                                        height={24}
                                        width={24}
                                        src={Images.iconShowLocation.data}
                                        alt={Images.iconShowLocation.atl}
                                    />
                                </Tooltip>,
                        },
                        {
                            key: EToolMenuMap.TrackLocation,
                            icon: <Tooltip
                                title={props.t("text.trackLocation")}
                            >
                                <Image
                                    height={24}
                                    width={24}
                                    src={Images.iconTrackLocation.data}
                                    alt={Images.iconTrackLocation.atl}
                                />
                            </Tooltip>,
                        },
                        {
                            key: EToolMenuMap.Measure,
                            icon:
                                <Tooltip
                                    title={props.t("text.measureTools")}
                                >
                                    <Image
                                        height={24}
                                        width={24}
                                        src={Images.iconMeasure.data}
                                        alt={Images.iconMeasure.atl}
                                    />
                                </Tooltip>,
                            popupClassName: `${sms.SubMenu}`,
                            popupOffset: [-150, 0],
                            children: [
                                {
                                    label: props.t('text.measureLine'),
                                    key: EDrawType.LineString,
                                    icon: <Image
                                        height={16}
                                        width={16}
                                        src={Images.iconLine.data}
                                        alt={Images.iconLine.atl}
                                    />,
                                },
                                {
                                    label: props.t('text.measureArea'),
                                    key: EDrawType.Polygon,
                                    icon: <Image
                                        height={16}
                                        width={16}
                                        src={Images.iconPolygon.data}
                                        alt={Images.iconPolygon.atl}
                                    />,
                                },
                                {
                                    label: props.t('text.measureCircle'),
                                    key: EDrawType.Circle,
                                    icon: <Image
                                        height={16}
                                        width={16}
                                        src={Images.iconCircle.data}
                                        alt={Images.iconCircle.atl}
                                    />,
                                }
                            ]
                        },
                    ]
                    : [
                        {
                            key: EToolMenuMap.Reset,
                            icon: <Tooltip
                                title={props.t("text.cancelActions")}
                            >
                                <Image
                                    height={20}
                                    width={20}
                                    src={Images.iconArrowUpRight.data}
                                    alt={Images.iconArrowUpRight.atl}
                                />
                            </Tooltip>,
                            style: {display: props.isDrawing ? "inline-block" : "none"}
                        },
                        {
                            key: 'divider-0',
                            icon: <Image
                                src={Images.iconDivider.data}
                                alt={Images.iconDivider.atl}
                                width={0}
                                height={0}
                            />,
                            style: {
                                pointerEvents: 'none',
                                display: props.isDrawing ? "inline-block" : "none"
                            },
                        },
                        {
                            key: EToolMenuMap.ShowLocation,
                            icon:
                                <Tooltip
                                    title={props.t("text.showLocation")}
                                >
                                    <Image
                                        height={24}
                                        width={24}
                                        src={Images.iconShowLocation.data}
                                        alt={Images.iconShowLocation.atl}
                                    />
                                </Tooltip>,
                        },
                        {
                            key: EToolMenuMap.TrackLocation,
                            icon: <Tooltip
                                title={props.t("text.trackLocation")}
                            >
                                <Image
                                    height={24}
                                    width={24}
                                    src={Images.iconTrackLocation.data}
                                    alt={Images.iconTrackLocation.atl}
                                />
                            </Tooltip>,
                        },
                        {
                            key: 'divider-1',
                            icon: <Image
                                src={Images.iconDivider.data}
                                alt={Images.iconDivider.atl}
                                width={0}
                                height={0}
                            />,
                            style: {
                                pointerEvents: 'none',
                            },
                        },
                        {
                            key: EToolMenuMap.Measure,
                            icon:
                                <Tooltip
                                    title={props.t("text.measureTools")}
                                >
                                    <Image
                                        height={24}
                                        width={24}
                                        src={Images.iconMeasure.data}
                                        alt={Images.iconMeasure.atl}
                                    />
                                </Tooltip>,
                            popupClassName: `${sms.SubMenu}`,
                            popupOffset: [-150, 0],
                            children: [
                                {
                                    label: props.t('text.measureLine'),
                                    key: EDrawType.LineString,
                                    icon: <Image
                                        height={16}
                                        width={16}
                                        src={Images.iconLine.data}
                                        alt={Images.iconLine.atl}
                                    />,
                                },
                                {
                                    label: props.t('text.measureArea'),
                                    key: EDrawType.Polygon,
                                    icon: <Image
                                        height={16}
                                        width={16}
                                        src={Images.iconPolygon.data}
                                        alt={Images.iconPolygon.atl}
                                    />,
                                },
                                {
                                    label: props.t('text.measureCircle'),
                                    key: EDrawType.Circle,
                                    icon: <Image
                                        height={16}
                                        width={16}
                                        src={Images.iconCircle.data}
                                        alt={Images.iconCircle.atl}
                                    />,
                                }
                            ]
                        },
                        {
                            key: EToolMenuMap.Profile,
                            icon:
                                <Tooltip
                                    title={props.t("text.profileTools")}
                                >
                                    <Image
                                        height={24}
                                        width={24}
                                        src={Images.iconProfile.data}
                                        alt={Images.iconProfile.atl}
                                    />
                                </Tooltip>,
                            popupClassName: `${sms.SubMenu}`,
                            popupOffset: [-150, 0],
                            children: [
                                {
                                    label: props.t('text.drawLine'),
                                    key: EProfileType.LineString,
                                    icon: <Image
                                        height={16}
                                        width={16}
                                        src={Images.iconLine.data}
                                        alt={Images.iconLine.atl}
                                    />,
                                },
                                {
                                    label: props.t('text.drawPolygon'),
                                    key: EProfileType.Polygon,
                                    icon: <Image
                                        height={16}
                                        width={16}
                                        src={Images.iconPolygon.data}
                                        alt={Images.iconPolygon.atl}
                                    />,
                                },
                                {
                                    label: props.t('text.drawCircle'),
                                    key: EProfileType.Circle,
                                    icon: <Image
                                        height={16}
                                        width={16}
                                        src={Images.iconCircle.data}
                                        alt={Images.iconCircle.atl}
                                    />,
                                },
                                {
                                    label: props.t('text.addAnnotation'),
                                    key: EProfileType.Annotation,
                                    icon: <Image
                                        height={16}
                                        width={16}
                                        src={Images.iconNote.data}
                                        alt={Images.iconNote.atl}
                                    />,
                                },
                                {
                                    label: props.t('text.addMarker'),
                                    key: EProfileType.Marker,
                                    icon: <Image
                                        height={16}
                                        width={16}
                                        src={Images.iconShowLocationBl.data}
                                        alt={Images.iconShowLocationBl.atl}
                                    />,
                                }
                            ]
                        },
                        {
                            key: EToolMenuMap.Tools,
                            popupClassName: `${sms.SubMenu}`,
                            popupOffset: [-150, 0],
                            icon: <Tooltip
                                title={props.t("text.editTools")}
                            >
                                <Image
                                    height={24}
                                    width={24}
                                    src={Images.iconWrenchWhite.data}
                                    alt={Images.iconWrenchWhite.atl}
                                />
                            </Tooltip>,
                            children: [
                                {
                                    label: props.t('text.trimFeature'),
                                    key: EEditType.Trim,
                                    icon: <Image
                                        height={16}
                                        width={16}
                                        src={Images.iconTrim.data}
                                        alt={Images.iconTrim.atl}
                                    />,
                                },
                                {
                                    label: props.t('text.clipLayer'),
                                    key: EEditType.Clip,
                                    icon: <Image
                                        height={16}
                                        width={16}
                                        src={Images.iconCLip.data}
                                        alt={Images.iconCLip.atl}
                                    />,
                                },
                                {
                                    label: props.t('text.copyFeature'),
                                    key: EEditType.Copy,
                                    icon: <Image
                                        height={16}
                                        width={16}
                                        src={Images.iconCopy.data}
                                        alt={Images.iconCopy.atl}
                                    />,
                                },
                                {
                                    label: props.t('text.moveFeature'),
                                    key: EEditType.Move,
                                    icon: <Image
                                        height={16}
                                        width={16}
                                        src={Images.iconArrowsOutCardinal.data}
                                        alt={Images.iconArrowsOutCardinal.atl}
                                    />
                                },
                                {
                                    label: props.t('text.rotateAndAdjust'),
                                    key: EEditType.Rotate,
                                    icon: <Image
                                        height={16}
                                        width={16}
                                        src={Images.iconModifyAndAdjust.data}
                                        alt={Images.iconModifyAndAdjust.atl}
                                    />,
                                },
                            ]
                        },
                        {
                            key: EToolMenuMap.Snap,
                            popupClassName: `${sms.SubMenu}`,
                            popupOffset: [-150, 0],
                            icon: <Tooltip
                                title={props.t("text.snapObject")}
                            >
                                <Image
                                    height={24}
                                    width={24}
                                    src={Images.iconSnap.data}
                                    alt={Images.iconSnap.atl}
                                />
                            </Tooltip>,
                            children: [
                                {
                                    label: props.t('text.snapToEdge'),
                                    key: ESnapType.Edge,
                                    icon: <Image
                                        height={16}
                                        width={16}
                                        src={Images.iconSnapEdge.data}
                                        alt={Images.iconSnapEdge.atl}
                                    />,
                                },
                                {
                                    label: props.t('text.snapToVertex'),
                                    key: ESnapType.Vertex,
                                    icon: <Image
                                        height={16}
                                        width={16}
                                        src={Images.iconSnapVertex.data}
                                        alt={Images.iconSnapVertex.atl}
                                    />,
                                },
                                {
                                    label: props.t('text.snapToCenter'),
                                    key: ESnapType.Center,
                                    icon: <Image
                                        height={16}
                                        width={16}
                                        src={Images.iconSnapCenter.data}
                                        alt={Images.iconSnapCenter.atl}
                                    />,
                                },
                                {
                                    label: props.t('text.snapToMiddle'),
                                    key: ESnapType.Middle,
                                    icon: <Image
                                        height={16}
                                        width={16}
                                        src={Images.iconSnapMiddle.data}
                                        alt={Images.iconSnapMiddle.atl}
                                    />,
                                },
                                {
                                    label: props.t('text.snapToIntersect'),
                                    key: ESnapType.Intersect,
                                    icon: <Image
                                        height={16}
                                        width={16}
                                        src={Images.iconSnapIntersect.data}
                                        alt={Images.iconSnapIntersect.atl}
                                    />,
                                },
                            ]
                        },
                        {
                            key: 'divider-2',
                            icon: <Image
                                src={Images.iconDivider.data}
                                alt={Images.iconDivider.atl}
                                width={0}
                                height={0}
                            />,
                            style: {pointerEvents: 'none'},
                        },
                        {
                            key: EToolMenuMap.KMLFile,
                            icon:
                                <Tooltip
                                    title={props.t("text.gisFileUpload")}
                                >
                                    <Image
                                        height={24}
                                        width={24}
                                        src={Images.iconUploadSimpleW.data}
                                        alt={Images.iconUploadSimpleW.atl}
                                    />
                                </Tooltip>,
                        },
                        {
                            key: EToolMenuMap.Capture,
                            icon:
                                <Tooltip
                                    title={props.t("text.captureScreen")}
                                >
                                    <Image
                                        height={24}
                                        width={24}
                                        src={Images.iconCapture.data}
                                        alt={Images.iconCapture.atl}
                                    />
                                </Tooltip>,
                        },
                        {
                            key: EToolMenuMap.Save,
                            icon: <Tooltip
                                title={props.t("button.saveProfile")}
                            >
                                <Image
                                    height={24}
                                    width={24}
                                    src={Images.iconSave.data}
                                    alt={Images.iconSave.atl}
                                />
                            </Tooltip>,
                        },
                        {
                            key: EToolMenuMap.Share,
                            icon:
                                <Tooltip
                                    title={props.t("text.share2D")}
                                >
                                    <Image
                                        height={24}
                                        width={24}
                                        src={Images.iconShare.data}
                                        alt={Images.iconShare.atl}
                                    />
                                </Tooltip>,
                        },
                    ]
            )
    )

    static readonly itemsContext = (t: TFunction<"translation">) => (
        [
            {
                label: t('text.copyCoordinates'),
                key: EContextMap.Copy,
                icon: <Image
                    src={Images.iconCopy.data}
                    alt={Images.iconCopy.atl}
                    width={16}
                    height={16}
                />
            },
            {
                label: t('text.refreshPage'),
                key: EContextMap.Refresh,
                icon: <Image
                    src={Images.iconRefreshBL.data}
                    alt={Images.iconRefreshBL.atl}
                    width={16}
                    height={16}
                />
            },
            {
                label: t('text.centerMapHere'),
                key: EContextMap.Center,
                icon: <Image
                    src={Images.iconTrackLocationBL.data}
                    alt={Images.iconTrackLocationBL.atl}
                    width={16}
                    height={16}
                />
            },
            {
                label: t('text.addMarker'),
                key: EContextMap.Marker,
                icon: <Image
                    src={Images.iconShowLocationBl.data}
                    alt={Images.iconShowLocationBl.atl}
                    width={16}
                    height={16}
                />,
                style: {display: (DetectMedia() || FeatureSingleton.getInstance().v2DShare) ? 'none' : "flex"}
            }
        ]
    )

    static readonly menuInfoHeader = (props: {
        t: TFunction<"translation">,
        navigate: NavigateFunction,
        mapTitle: () => JSX.Element | null,
    }) => (
        [
            {
                key: "back",
                icon: <Image
                    height={24}
                    width={24}
                    src={Images.iconCaretLeftCircle.data}
                    alt={Images.iconCaretLeftCircle.atl}
                />,
                onClick: () => props.navigate(RouteAction.GoBack()),
                style: {display: DetectMedia() ? 'none' : "inline-block"}
            },
            {
                key: "menu-title",
                label: props.mapTitle(),
                className: 'menu-title'
            },
        ]
    )

    static readonly mainMenuItemsMobile = (props: {
        t: TFunction<"translation">,
        parts?: TParamPartGeodetic,
        viewer: Viewer
    }) =>  (
        [
            {
                key: EMenuMapItem.Part,
                label: props.t('text.switchPart'),
                icon: <>
                    {
                        !props.parts
                            ? <Image
                                height={24}
                                width={24}
                                src={Images.iconSwitchPartsDisabled.data}
                                alt={Images.iconSwitchPartsDisabled.atl}
                            />
                            : <Image
                                height={24}
                                width={24}
                                src={Images.iconSwitchParts.data}
                                alt={Images.iconSwitchParts.atl}
                            />
                    }
                </>,
                disabled: !props.parts,
                style: {display: DetectMedia() ? 'none' : "flex"}
            },
            {
                key: EMenuMapItem.ContourLayer,
                label: props.t('text.contourMap'),
                icon: <>
                    {
                        props.viewer.contourTool.children.length < 1
                            ? <Image
                                height={24}
                                width={24}
                                src={Images.iconContourDisabled.data}
                                alt={Images.iconContourDisabled.atl}
                            />
                            : <Image
                                height={24}
                                width={24}
                                src={Images.iconContour.data}
                                alt={Images.iconContour.atl}
                            />
                    }
                </>,
                disabled: props.viewer.contourTool.children.length < 1,
            },
            {
                key: EMenuMapItem.Map2DLayer,
                label: props.t('text.orthogonalImage'),
                icon: <Image
                    height={24}
                    width={24}
                    src={Images.icon2DLayer.data}
                    alt={Images.icon2DLayer.atl}
                />,
            },
            {
                key: EMenuMapItem.BaseLayer,
                label: props.t('text.baseMap'),
                icon: <Image
                    height={24}
                    width={24}
                    src={Images.iconLayerGroup.data}
                    alt={Images.iconLayerGroup.atl}
                />,
            },
            {
                key: EMenuMapItem.MeasureLayer,
                label: props.t('text.layerMeasures'),
                icon: <Image
                    height={24}
                    width={24}
                    src={Images.iconLayerMeasure.data}
                    alt={Images.iconLayerMeasure.atl}
                />,
            },
            {
                key: EMenuMapItem.ProfileLayer,
                label: props.t('text.layerProfiles'),
                icon:  <Image
                    height={24}
                    width={24}
                    src={Images.iconLayerProfile.data}
                    alt={Images.iconLayerProfile.atl}
                />,
            },
        ]
    )

    static readonly mainMenuItems = (props: {
        t: TFunction<"translation">,
        parts?: TParamPartGeodetic,
        viewer: Viewer
    }) => (
        [
            {
                key: EMenuMapItem.Part,
                label: props.t('text.switchPart'),
                icon: <Tooltip
                    placement="right"
                    title={props.t('text.switchPart')}
                    arrowPointAtCenter={true}
                    overlayClassName={sms.MenuToolTip}

                >
                    {
                        !props.parts
                            ? <Image
                                height={24}
                                width={24}
                                src={Images.iconSwitchPartsDisabled.data}
                                alt={Images.iconSwitchPartsDisabled.atl}
                            />
                            : <Image
                                height={24}
                                width={24}
                                src={Images.iconSwitchParts.data}
                                alt={Images.iconSwitchParts.atl}
                            />
                    }
                </Tooltip>,
                disabled: !props.parts,
                style: {display: DetectMedia() ? 'none' : "flex"}
            },
            {
                key: EMenuMapItem.ContourLayer,
                label: props.t('text.contourMap'),
                icon: <Tooltip
                    overlayClassName={sms.MenuToolTip}
                    placement="right"
                    title={props.t('text.contourMap')}
                    arrowPointAtCenter={true}
                >
                    {
                        props.viewer.contourTool.children.length < 1
                            ? <Image
                                height={24}
                                width={24}
                                src={Images.iconContourDisabled.data}
                                alt={Images.iconContourDisabled.atl}
                            />
                            : <Image
                                height={24}
                                width={24}
                                src={Images.iconContour.data}
                                alt={Images.iconContour.atl}
                            />
                    }
                </Tooltip>,
                disabled: props.viewer.contourTool.children.length < 1,
            },
            {
                key: EMenuMapItem.Map2DLayer,
                label: props.t('text.orthogonalImage'),
                icon: <Tooltip
                    placement="right"
                    title={props.t('text.orthogonalImage')}
                    arrowPointAtCenter={true}
                    overlayClassName={sms.MenuToolTip}
                >
                    <Image
                        height={24}
                        width={24}
                        src={Images.icon2DLayer.data}
                        alt={Images.icon2DLayer.atl}
                    />
                </Tooltip>,
            },
            {
                key: EMenuMapItem.BaseLayer,
                label: props.t('text.baseMap'),
                icon: <Tooltip
                    overlayClassName={sms.MenuToolTip}
                    placement="right"
                    title={props.t('text.baseMap')}
                    arrowPointAtCenter={true}
                >
                    <Image
                        height={24}
                        width={24}
                        src={Images.iconLayerGroup.data}
                        alt={Images.iconLayerGroup.atl}
                    />
                </Tooltip>,
            },
            {
                key: EMenuMapItem.MeasureLayer,
                label: props.t('text.layerMeasures'),
                icon: <Tooltip
                    overlayClassName={sms.MenuToolTip}
                    placement="right"
                    title={props.t('text.layerMeasures')}
                    arrowPointAtCenter={true}
                >
                    <Image
                        height={24}
                        width={24}
                        src={Images.iconLayerMeasure.data}
                        alt={Images.iconLayerMeasure.atl}
                    />
                </Tooltip>,
            },
            {
                key: EMenuMapItem.ProfileLayer,
                label: props.t('text.layerProfiles'),
                icon: <Tooltip
                    overlayClassName={sms.MenuToolTip}
                    placement="right"
                    title={props.t('text.layerProfiles')}
                    arrowPointAtCenter={true}
                >
                    <Image
                        height={24}
                        width={24}
                        src={Images.iconLayerProfile.data}
                        alt={Images.iconLayerProfile.atl}
                    />
                </Tooltip>,
            },
        ]
    )

    static readonly measureMenuItems = (t: TFunction<"translation">) => (
        [
            {
                label: <Tooltip
                    title={t('text.measureLine')}
                    placement="bottomRight"
                >
                    <Image
                        src={Images.iconLine.data}
                        alt={Images.iconLine.atl}
                        width={24}
                        height={24}
                    />
                </Tooltip>,
                key: EDrawType.LineString,
            },
            {
                label: <Tooltip
                    title={t('text.measureArea')}
                    placement="bottomRight"
                >
                    <Image
                        src={Images.iconPolygon.data}
                        alt={Images.iconPolygon.atl}
                        width={24}
                        height={24}
                    />
                </Tooltip>,
                key: EDrawType.Polygon,
            },
            {
                label: <Tooltip
                    title={t('text.measureCircle')}
                    placement="bottomRight"
                >
                    <Image
                        src={Images.iconCircle.data}
                        alt={Images.iconCircle.atl}
                        width={24}
                        height={24}
                    />
                </Tooltip>,
                key: EDrawType.Circle,
            },
        ]
    )

    static readonly profileMenuItems = (t: TFunction<"translation">) => (
        [
            {
                label: <Tooltip
                    title={t('text.drawLine')}
                    placement="bottomRight"
                >
                    <Image
                        src={Images.iconLine.data}
                        alt={Images.iconLine.atl}
                        width={24}
                        height={24}
                    />
                </Tooltip>,
                key: EProfileType.LineString,
            },
            {
                label: <Tooltip
                    title={t('text.drawPolygon')}
                    placement="bottomRight"
                >
                    <Image
                        src={Images.iconPolygon.data}
                        alt={Images.iconPolygon.atl}
                        width={24}
                        height={24}
                    />
                </Tooltip>,
                key: EProfileType.Polygon,
            },
            {
                label: <Tooltip
                    title={t('text.drawCircle')}
                    placement="bottomRight"
                >
                    <Image
                        src={Images.iconCircle.data}
                        alt={Images.iconCircle.atl}
                        width={24}
                        height={24}
                    />
                </Tooltip>,
                key: EProfileType.Circle,
            },
            {
                label: <Tooltip
                    title={t('text.note')}
                    placement="bottomRight"
                >
                    <Image
                        src={Images.iconNote.data}
                        alt={Images.iconNote.atl}
                        width={24}
                        height={24}
                    />
                </Tooltip>,
                key: EProfileType.Annotation,
            },
            {
                label: <Tooltip
                    title={t('text.markerMap')}
                    placement="bottomRight"
                >
                    <Image
                        src={Images.iconShowLocationBl.data}
                        alt={Images.iconShowLocationBl.atl}
                        width={24}
                        height={24}
                    />
                </Tooltip>,
                key: EProfileType.Marker,
            },
        ]
    );

    static readonly itemsContextFeature = (props: {
        t: TFunction<"translation">,
        isRegion?: boolean
    }) => (
        [
            {
                label: props.t('text.editGeometry'),
                key: EToolsEdit.EditGeometry,
                icon: <Image
                    src={Images.iconPencilSimpleLineBold.data}
                    alt={Images.iconPencilSimpleLineBold.atl}
                    width={16}
                    height={16}
                />
            },
            {
                label: props.t('text.editCharacteristics'),
                key: EToolsEdit.EditCharacteristics,
                icon: <Image
                    src={Images.iconNotePencil.data}
                    alt={Images.iconNotePencil.atl}
                    width={16}
                    height={16}
                />,
                disabled: props.isRegion
            },
            {
                label: props.t('text.deleteFeature'),
                key: EToolsEdit.Delete,
                icon: <Image
                    src={Images.iconTrash.data}
                    alt={Images.iconTrash.atl}
                    width={16}
                    height={16}
                />
            },
        ]
    )

}
