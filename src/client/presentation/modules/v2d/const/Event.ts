import {CLTile} from "../viewer/clim/CLTileTool";
import {CLContour} from "../viewer/clim/CLContourTool";
import {CLMeasure} from "../viewer/clim/CLMeasureTool";
import {CLProfile} from "../viewer/clim/CLProfileTool";
import {TFormDrawerValues, TFormMarkerValues} from "../viewer/clim/CLTypeTool";
import {CLShareRegion} from "../viewer/clim/CLShareRegionTool";

export class MapEventName {
    public static readonly MoveEnd = "moveend";
    public static readonly SingleClick = "singleclick";
    public static readonly PointerMove = "pointermove";
    public static readonly RenderComplete = "rendercomplete";
    public static readonly Click = "click";
    public static readonly Contextmenu = "contextmenu";
    public static readonly ChangeResolution = "change:resolution";
    public static readonly AddFeature = "addfeature";
    public static readonly FeaturesLoadEnd = "featuresloadend"
    public static readonly FeaturesLoadStart = "featuresloadstart"
    public static readonly PostRender = "postrender";
    public static readonly Select = "select";

}

export class ModifyEventName {
    public static readonly ModifyStart = "modifystart";
    public static readonly ModifyEnd = "modifyend";
}

export class DrawEventName {
    public static readonly DrawStart = "drawstart";
    public static readonly DrawEnd = "drawend";
}

export class ViewerEventName {
    public static readonly LoadProjectDone = "load_project_done";

    public static readonly BaseMapAdded = "basemap_added";
    public static readonly BaseMapRemoved = "basemap_removed";

    public static readonly TileSets = "tile_sets";
    public static readonly TileAdded = "tile_added";
    public static readonly TileRemoved = "tile_removed";

    public static readonly ContourSets = "contour_sets";
    public static readonly ContourAdded = "contour_added";
    public static readonly ContourChangeStatus = "contour_change_status";
    public static readonly ContourRemoved = "contour_removed";

    public static readonly ProfileSets = "profile_sets";
    public static readonly ProfileAdded = "profile_added";
    public static readonly ProfileRemoved = "profile_removed";
    public static readonly ProfileModified = "profile_modified"

    public static readonly FeatureAdded = "feature_added";
    public static readonly FeatureRemoved = "feature_removed";
    public static readonly FeatureCreated = "feature_created"
    public static readonly FeatureSets = "feature_sets"

    public static readonly MarkerSets = "marker_sets";
    public static readonly MarkerAdded = "marker_added";
    public static readonly MarkerRemoved = "marker_removed";

    public static readonly MeasureAdded = "measure_added";
    public static readonly MeasureRemoved = "measure_removed";
    public static readonly MeasureSets = "measure_sets";

    public static readonly DrawerOpened = "drawer_opened";
    public static readonly DrawerClosed = "drawer_closed";

    public static readonly ShareRegionAdded = "share_region_added";
    public static readonly ShareRegionRemoved = "share_region_removed";
}

export type TEventTileAdded = CLTile;

export type TEventTileSets = {
    isReplace: boolean;
    data: CLTile[];
}

export type TEventContourAdded = CLContour;

export type TEventContourSets = {
    isReplace: boolean;
    data: CLContour[];
}

export type TEventProfileSets = {
    isReplace: boolean,
    isDrawer: boolean,
    data: CLProfile[],
    initValues?: TFormDrawerValues | TFormMarkerValues,
}
export type TEventProfileAdded = CLProfile;
export type TEventFeatureSets = CLProfile[];
export type TEventProfileRemoved = CLProfile;

export type TEventMeasureSets = {
    isReplace: boolean,
    data: CLMeasure[];
}
export type TEventMeasureAdded = CLMeasure;
export type TEventMeasureRemoved = CLMeasure;

export type TEventRegionSets = {
    isReplace: boolean,
    isDrawer: boolean,
    data: CLShareRegion[],
};
export type TEventRegionRemoved = CLShareRegion;






