import React, {useEffect, useLayoutEffect, useMemo, useRef, useState} from "react";
import {Map2DModel} from "../../../models/service/geodetic/Map2DModel";
import {Main} from "./Main";
import EventEmitter from "eventemitter3";
import {Color} from "../../../const/Color";
import styles from "./styles/MapView.module.scss";
import {TParamPartGeodetic} from "../../../const/Types";
import {Viewer} from "./viewer/Viewer";
import {MapToolFC} from "./components/MapToolFC";
import "ol/ol.css";
import {DetectMedia} from "./const/App";
import {FeatureSingleton} from "../../../models/FeatureSingleton";
import {MapEventName} from "./const/Event";

type TArgs = {
    viewer: Viewer,
    event: EventEmitter
}

const OLView = React.memo((props: {
    m2dId: number,
    item: Map2DModel,
    parts?: TParamPartGeodetic,
}) => {
    const divRef = useRef<HTMLDivElement>(null);
    const [args, setArgs] = useState<TArgs>();
    const [headerHeightRef, setHeaderHeightRef] = useState<number>();
    const [isResponsiveLayer, setIsResponsiveLayer] = useState<boolean>(false);
    const [isMapRender, setIsMapRender] = useState(false);

    useEffect(() => {
        console.log('%cMount Screen: OLView', Color.ConsoleInfo);

        const event = new EventEmitter();

        const viewer = new Viewer({
            targetRender: divRef.current!,
            event: event,
            model: props.item
        })

        setArgs({
            viewer: viewer,
            event: event
        })

        return () => {
            viewer.destroy();

            console.log('%cUnmount Screen: OLView', Color.ConsoleInfo)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useLayoutEffect(() => {
        if (args) {
            if (!DetectMedia()) {
                setHeaderHeightRef(60)
            } else {
                if (FeatureSingleton.getInstance().isContributor) {
                    setHeaderHeightRef(48)
                } else {
                    setHeaderHeightRef(0)
                }
            }
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [args])

    const onCloseMapLayer = () => {
        setIsResponsiveLayer(false)
    };

    const onOpenMapLayer = () => {
        setIsResponsiveLayer(true)
    };

    if (args) {
        args.viewer.map.on(MapEventName.RenderComplete, () => {
            setIsMapRender(true)
        })
    }

    const getToolFC = useMemo(() => {
        if (args) {
            return (
                <MapToolFC
                    viewer={args.viewer}
                    event={args.event}
                    m2dId={props.m2dId}
                    item={props.item}
                    isResponsiveLayer={isResponsiveLayer}
                    onCloseMapLayer={onCloseMapLayer}
                    onOpenMapLayer={onOpenMapLayer}
                />
            )
        }
        return null;

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [args, isMapRender])

    const getMainFC = useMemo(() => {
        if (args && headerHeightRef !== undefined) {
            return (
                <Main
                    m2dId={props.m2dId}
                    item={props.item}
                    parts={props.parts}
                    viewer={args.viewer}
                    event={args.event}
                    headerHeightRef={headerHeightRef}
                    onCloseMapLayer={onCloseMapLayer}
                />
            )
        }

        return null;

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [args, headerHeightRef])

    return (
        <div className={"Map"}>
            <div
                id={'MapContainer'}
                className={styles.MapContainer}
                ref={divRef}
            />
            {getMainFC}
            {getToolFC}
        </div>
    )
});

export default OLView;
