import {App} from "../const/App";
import {Circle, Geometry, LineString, Polygon} from "ol/geom";
import {getArea, getLength} from "ol/sphere";
import {Fill, RegularShape, Stroke, Style, Text} from "ol/style";
import CircleStyle from "ol/style/Circle";
import {Feature} from "ol";

export class Utils2D {
    static formatNumber(value: number, decimal: number = 2, sections: number = 3) {
        const re = '\\d(?=(\\d{' + (sections) + '})+' + (decimal > 0 ? '\\.' : '$') + ')';
        return value.toFixed(Math.max(0, ~~decimal)).replace(new RegExp(re, 'g'), '$&,');
    }

    static formatLength = function (
        line: LineString,
        options: {
            limitM: number,
        } = {
            limitM: 100,
        }
    ) {
        const length = getLength(line, {
            projection: App.DefaultEPSG,
        });

        let output;
        if (length > options.limitM) {
            output = Math.round((length / 1000) * 100) / 100 + ' km';
        } else {
            output = Math.round(length * 100) / 100 + ' m';
        }

        return output;
    };

    static formatArea = function (
        polygon: Polygon,
        options: {
            symbol: '\xB2' | '<sup>2</sup>',
            limitM?: number,
        } = {
            symbol: '\xB2',
            limitM: 1000000,
        }
    ) {
        const m2 = getArea(polygon, {
            projection: App.DefaultEPSG,
        });

        let output;

        if (m2 > 1000000000) { // km2
            output = Utils2D.formatNumber(m2 / 1000000) + ` km${options.symbol}`;
        } else if (m2 > 10000) { // ha
            output = Utils2D.formatNumber(m2 / 10000) + ' ha';
        } else {
            output = Utils2D.formatNumber(m2) + ` m${options.symbol}`;
        }

        return output;
    };

    static formatCircle = function (
        circle: Circle,
        options: {
            symbol: '\xB2' | '<sup>2</sup>',
            limitM?: number,
        } = {
            symbol: '\xB2',
            limitM: 1000,
        }
    ) {
        const radius = circle.getRadius();
        const m2 = Math.PI * (radius * 10000) * (radius * 10000);

        let output;

        if (m2 > 1000000000) { // km2
            output = Utils2D.formatNumber(m2 / 1000000) + ` km${options.symbol}`;
        } else if (m2 > 10000) { // ha
            output = Utils2D.formatNumber(m2 / 10000) + ' ha';
        } else {
            output = Utils2D.formatNumber(m2) + ` m${options.symbol}`;
        }

        return output;
    };

    static styleVectorLayer = (color: string = '#ffcc33', feature?: Feature<Geometry>) => {
        const noteStyleFeature: Style = feature?.get('style');

        return new Style({
            fill: new Fill({
                color: 'rgba(255, 255, 255, 0.2)',
            }),
            ...(
                noteStyleFeature && noteStyleFeature['text_']
                    ? {
                        text: new Text({
                            text: noteStyleFeature['text_'] instanceof Text ? noteStyleFeature['text_'].getText() : noteStyleFeature['text_'],
                            font: '14px Calibri,sans-serif',
                            fill: new Fill({color: color}),
                            stroke: new Stroke({
                                color: 'white',
                                width: 3,
                            }),
                        }),
                        stroke: new Stroke({
                            color: color,
                            width: 2,
                        }),
                    }
                    : {
                        stroke: new Stroke({
                            color: color,
                            width: 2,
                        }),
                        image: new CircleStyle({
                            radius: 7,
                            fill: new Fill({
                                color: color,
                            }),
                        }),
                        text: new Text({
                            text: undefined,
                        })
                    }
            ),
        });
    }

    static styleHideVectorLayer = () => new Style();

    static styleDrawTip = () => {
        return new Style({
            text: new Text({
                font: '12px Calibri,sans-serif',
                fill: new Fill({
                    color: 'rgba(255, 255, 255, 1)',
                }),
                backgroundFill: new Fill({
                    color: 'rgba(0, 0, 0, 0.4)',
                }),
                padding: [2, 2, 2, 2],
                textAlign: 'left',
                offsetX: 15,
            }),
        })
    }

    static styleDrawSegment = () => {
        return new Style({
            text: new Text({
                font: 'bold 14px Calibri,sans-serif',
                fill: new Fill({
                    color: 'rgba(0, 0, 0, 1)',
                }),
                textBaseline: 'ideographic',
                offsetY: 5,
                placement: 'line',
                stroke: new Stroke({
                    color: 'rgba(256, 256, 256, 1)',
                    width: 2,
                }),
            }),
        })
    }
    static styleDrawVector = () => {
        return new Style({
            fill: new Fill({
                color: 'rgba(255, 255, 255, 0.2)',
            }),
            stroke: new Stroke({
                color: 'rgba(0, 0, 0, 1)',
                width: 2,
            }),
            image: new CircleStyle({
                radius: 7,
                fill: new Fill({
                    color: 'rgba(0, 0, 0, 1)',
                }),
            }),
        })
    }
    static styleDrawLabel = () => {
        return new Style({
            text: new Text({
                font: '14px Calibri,sans-serif',
                fill: new Fill({
                    color: 'rgba(255, 255, 255, 1)',
                }),
                backgroundFill: new Fill({
                    color: 'rgba(0, 0, 0, 0.7)',
                }),
                padding: [3, 3, 3, 3],
                textBaseline: 'bottom',
                offsetY: -15,
            }),
            image: new RegularShape({
                radius: 8,
                points: 3,
                angle: Math.PI,
                displacement: [0, 10],
                fill: new Fill({
                    color: 'rgba(0, 0, 0, 0.7)',
                }),
            }),
        })
    }

    static styleDrawRegion = () => {
        return new Style({
            fill: new Fill({
                color: `rgba(255, 255, 255, 0.4)`
            }),
            stroke: new Stroke({
                color: '#DE9F59',
                width: 2,
            }),
            image: new CircleStyle({
                radius: 5,
                stroke: new Stroke({
                    color: 'rgba(0, 0, 0, 0.7)',
                }),
                fill: new Fill({
                    color: 'rgba(255, 255, 255, 0.2)',
                }),
            }),
        })
    }

    static styleMeasurePath = () => {
        return new Style({
            fill: new Fill({
                color: 'rgba(255, 255, 255, 0.2)',
            }),
            stroke: new Stroke({
                color: 'rgba(0, 0, 0, 0.5)',
                lineDash: [10, 10],
                width: 2,
            }),
            image: new CircleStyle({
                radius: 5,
                stroke: new Stroke({
                    color: 'rgba(0, 0, 0, 0.7)',
                }),
                fill: new Fill({
                    color: 'rgba(255, 255, 255, 0.2)',
                }),
            }),
        })
    }

    static styleDrawModify = () => {
        return new Style({
            image: new CircleStyle({
                radius: 5,
                stroke: new Stroke({
                    color: 'rgba(0, 0, 0, 0.7)',
                }),
                fill: new Fill({
                    color: 'rgba(0, 0, 0, 0.4)',
                }),
            }),
            text: new Text({
                font: '12px Calibri,sans-serif',
                fill: new Fill({
                    color: 'rgba(255, 255, 255, 1)',
                }),
                backgroundFill: new Fill({
                    color: 'rgba(0, 0, 0, 0.7)',
                }),
                padding: [2, 2, 2, 2],
                textAlign: 'left',
                offsetX: 15,
            }),
        })
    }

    static styleTrackLocationCursor = () => {
        return new Style({
            image: new CircleStyle({
                radius: 10,
                fill: new Fill({
                    color: '#EB5757',
                }),
                stroke: new Stroke({
                    color: '#fff',
                    width: 2,
                }),
            }),
            text: new Text({
                font: 'bold 14px Calibri,sans-serif',
                fill: new Fill({
                    color: '#EB5757',
                }),
                padding: [2, 2, 2, 2],
                textAlign: 'center',
                offsetY: 20,
                stroke: new Stroke({
                    color: '#fff',
                    width: 2,
                }),
            })
        });
    }

    static selectedStyle = () => {
        return new Style({
            fill: new Fill({
                color: '#DE9F5999',
            }),
            stroke: new Stroke({
                color: '#CC893D',
                width: 2,
            }),
        })
    }

    static styleImageCursor = () => {
        return new Style({
            image: new CircleStyle({
                radius: 5,
                stroke: new Stroke({
                    color: '#EB5757',
                }),
                fill: new Fill({
                    color: '#FFFFFF99',
                }),
            }),
        })
    }
    static styleDefault = () => {
        return ({
            'Point': new Style({
                image: new CircleStyle({
                    fill: new Fill({
                        color: 'rgba(255,255,0,0.4)',
                    }),
                    radius: 5,
                    stroke: new Stroke({
                        color: '#ff0',
                        width: 1,
                    }),
                }),
            }),
            'LineString': new Style({
                stroke: new Stroke({
                    color: '#f00',
                    width: 3,
                }),
            }),
            'MultiLineString': new Style({
                stroke: new Stroke({
                    color: '#0f0',
                    width: 3,
                }),
            }),
            'MultiPoint': new Style({
                image: new CircleStyle({
                    fill: new Fill({
                        color: 'rgba(255,255,0,0.4)',
                    }),
                    radius: 5,
                    stroke: new Stroke({
                        color: '#ff0',
                        width: 1,
                    }),
                }),
            }),
            'MultiPolygon': new Style({
                stroke: new Stroke({
                    color: 'yellow',
                    width: 1,
                }),
                fill: new Fill({
                    color: 'rgba(255, 255, 0, 0.1)',
                }),
            }),
            'Polygon': new Style({
                stroke: new Stroke({
                    color: 'blue',
                    lineDash: [2],
                    width: 2,
                }),
                fill: new Fill({
                    color: 'rgba(0, 0, 255, 0.1)',
                }),
            }),
            'GeometryCollection': new Style({
                stroke: new Stroke({
                    color: 'magenta',
                    width: 2,
                }),
                fill: new Fill({
                    color: 'magenta',
                }),
                image: new CircleStyle({
                    radius: 5,
                    fill: undefined,
                    stroke: new Stroke({
                        color: 'magenta',
                    }),
                }),
            }),
            'Circle': new Style({
                stroke: new Stroke({
                    color: 'red',
                    width: 2,
                }),
                fill: new Fill({
                    color: 'rgba(255,0,0,0.2)',
                }),
            }),
        })
    }
}
