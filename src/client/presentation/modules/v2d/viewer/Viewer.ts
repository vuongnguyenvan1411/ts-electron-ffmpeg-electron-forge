import {Map, View} from "ol";
import {App} from "../const/App";
import EventEmitter from "eventemitter3";
import {CLBaseMapTool} from "./clim/CLBaseMapTool";
import {CLTileTool} from "./clim/CLTileTool";
import {CLContourTool} from "./clim/CLContourTool";
import {CLMarkerTool} from "./clim/CLMarkerTool";
import {CLMeasureTool} from "./clim/CLMeasureTool";
import {CLProfileTool} from "./clim/CLProfileTool";
import {CLShareRegionTool} from "./clim/CLShareRegionTool";
import {Map2DModel} from "../../../../models/service/geodetic/Map2DModel";

type TViewerMapOptions = {
    targetRender: HTMLElement,
    event: EventEmitter,
    model: Map2DModel,
}

export class Viewer extends EventEmitter {
    map: Map;
    event: EventEmitter;

    baseMapTool: CLBaseMapTool;
    tileTool: CLTileTool;
    contourTool: CLContourTool;
    markerTool: CLMarkerTool;
    measureTool: CLMeasureTool;
    profileTool: CLProfileTool;
    shareTool: CLShareRegionTool;

    model: Map2DModel;

    constructor(opts: TViewerMapOptions) {
        super();

        this.map = new Map({
            target: opts.targetRender,
            view: new View({
                projection: App.DefaultEPSG,
                minZoom: App.mapMinZoom,
                maxZoom: App.mapMaxZoom,
            }),
            controls: [],
            layers: [],
            maxTilesLoading: 4,
            pixelRatio: 1,
        })

        this.event = opts.event;

        const optsTool = {
            map: this.map,
            event: this.event,
        }

        this.baseMapTool = new CLBaseMapTool(optsTool);
        this.tileTool = new CLTileTool(optsTool);
        this.contourTool = new CLContourTool(optsTool);
        this.markerTool = new CLMarkerTool(optsTool);
        this.measureTool = new CLMeasureTool(optsTool);
        this.profileTool = new CLProfileTool(optsTool);
        this.shareTool = new CLShareRegionTool(optsTool);

        this.model = opts.model;
    }

    destroy = () => {
        this.map.setTarget(undefined);
        this.map.dispose();
        this.event.removeAllListeners();
    }
}
