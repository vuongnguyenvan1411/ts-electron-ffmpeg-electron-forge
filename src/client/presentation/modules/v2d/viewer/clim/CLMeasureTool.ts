import {CLInterface, CLT, IOptCL} from "./CLInterface";
import {EDrawType} from "../../const/Defines";
import {Collection, Feature, Map} from "ol";
import {Geometry} from "ol/geom";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import EventEmitter from "eventemitter3";
import {ViewerEventName} from "../../const/Event";
import LayerGroup from "ol/layer/Group";
import {Modify} from "ol/interaction";

type TOpts = {
    event: EventEmitter;
    map: Map;
}

export class CLMeasureTool extends EventEmitter {
    layer?: LayerGroup;
    children: CLMeasure[];
    map: Map;
    event: EventEmitter;

    constructor(opts: TOpts) {
        super();

        this.event = opts.event;
        this.map = opts.map;

        this.children = [];
    }

    setLayerGroup(layer: LayerGroup) {
        this.layer = layer;
    }

    addFeature(opts: IOptCLMeasure) {
        const measure = new CLMeasure(opts);

        this.children.push(measure);

        this.event.emit(ViewerEventName.MeasureAdded, measure);

        return measure;
    }

    removeFeature(measure: CLMeasure) {
        const sourceObj = measure.layer.getSource();

        if (!sourceObj) {
            return
        }

        const features = sourceObj.getFeatures();
        const feature = measure.feature;

        if (!feature) {
            return
        }

        const index = features.indexOf(feature);

        if (index > -1) {
            features.splice(index, 1);

            sourceObj.removeFeature(feature);

            this.event.emit(ViewerEventName.MeasureRemoved, measure)
        }
    }

    setChild(opts: IOptCLMeasure[], isReplace: boolean = true, isAddMap: boolean = false) {
        const measures: CLMeasure[] = [];

        opts.forEach(item => measures.push(new CLMeasure(item)));

        if (isReplace) {
            this.children = measures;
        } else {
            measures.forEach(item => this.children.push(item));
        }

        if (isAddMap && this.layer) {
            if (isReplace) {
                const collect: Collection<VectorLayer<VectorSource<any>>> = new Collection();
                measures.forEach(item => collect.push(item.layer));

                this.layer.setLayers(collect)
            } else {
                measures.forEach(item => this.layer?.getLayers().push(item.layer));
            }
        }

        this.event.emit(ViewerEventName.MeasureSets, {
            isReplace: isReplace,
            data: measures
        });
    }

    getLayerById(id: EDrawType) {
        const child = this.children.find(item => item.id === id);
        if (child) return child.layer;
    }

    getChildByIdFeature(id: string) {
        let cm;

        for (let i = 0; i < this.children.length; i++) {
            const listFeatures = this.children[i].layer.getSource()?.getFeatures();

            if (listFeatures && listFeatures.find(item => item.get('id') === id)) return cm = this.children[i]
        }
    }
}

export interface IOptCLMeasure extends IOptCL {
    typeDraw: EDrawType;
    layer: VectorLayer<VectorSource<any>>;
    feature?: Feature<Geometry>;
    init: boolean;
}

export class CLMeasure extends CLInterface implements CLT {
    typeDraw: EDrawType;
    layer: VectorLayer<VectorSource<Geometry>>;
    init: boolean;
    feature?: Feature<Geometry>;

    protected _targetModify?: Modify;

    constructor(opts: IOptCLMeasure) {
        super(opts);

        this.typeDraw = opts.typeDraw;
        this.layer = opts.layer;
        this.feature = opts.feature;
    }

    getVisible() {
        return this.layer.getVisible();
    }

    setVisible(visible: boolean) {
        this.layer.setVisible(visible)
    }

    getFeature(): Feature<Geometry> | undefined {
        return this.layer.getSource()?.getFeatures().find(item => item.get('id') === this.id);
    }

    setTargetModify(targetModify: Modify) {
        this._targetModify = targetModify;
    }

    getTargetModify() {
        return this._targetModify;
    }
}
