import LayerGroup from "ol/layer/Group";
import TileLayer from "ol/layer/Tile";
import {TileImage} from "ol/source";
import {CLInterface, CLT, IOptCL} from "./CLInterface";
import EventEmitter from "eventemitter3";
import {Collection, Map} from "ol";
import {ViewerEventName} from "../../const/Event";

type TOpts = {
    event: EventEmitter;
    map: Map;
}

export class CLTileTool extends EventEmitter {
    layer?: LayerGroup;
    children: CLTile[];
    map: Map;
    event: EventEmitter;

    constructor(opts: TOpts) {
        super();

        this.event = opts.event;
        this.map = opts.map;

        this.children = [];
    }

    setLayer(layer: LayerGroup) {
        this.layer = layer;
    }

    clear() {
        if (this.layer) {
            this.layer.getLayers().forEach(layer => {
                const source = (layer as TileLayer<any>).getSource()
                source.clear()
            })
        }
    }

    addChild(opts: IOptCLTile, isAddMap: boolean = false) {
        const tile = new CLTile(opts);

        this.children.push(tile);

        if (isAddMap && this.layer) {
            this.layer.getLayers().push(tile.object)
        }

        this.event.emit(ViewerEventName.TileAdded, tile);
    }

    setChild(opts: IOptCLTile[], isReplace: boolean = true, isAddMap: boolean = false) {
        const tiles: CLTile[] = [];

        opts.forEach(item => tiles.push(new CLTile(item)));

        if (isReplace) {
            this.children = tiles;
        } else {
            tiles.forEach(item => this.children.push(item));
        }

        if (isAddMap && this.layer) {
            if (isReplace) {
                const collect: Collection<TileLayer<TileImage>> = new Collection();
                tiles.forEach(item => collect.push(item.object));

                this.layer.setLayers(collect);
            } else {
                tiles.forEach(item => this.layer?.getLayers().push(item.object));
            }
        }

        this.event.emit(ViewerEventName.TileSets, {
            isReplace: isReplace,
            data: tiles
        });
    }
}

export interface IOptCLTile extends IOptCL {
    object: TileLayer<TileImage>;
}

export class CLTile extends CLInterface implements CLT {
    object: TileLayer<TileImage>;

    constructor(opts: IOptCLTile) {
        super(opts);

        this.object = opts.object;
    }

    getVisible() {
        return this.object.getVisible();
    }

    setVisible(visible: boolean) {
        this.object.setVisible(visible)
    }
}
