import LayerGroup from "ol/layer/Group";
import TileLayer from "ol/layer/Tile";
import {TileWMS} from "ol/source";
import {CLInterface, CLT, IOptCL} from "./CLInterface";
import EventEmitter from "eventemitter3";
import {Collection, Map} from "ol";
import {ViewerEventName} from "../../const/Event";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import {EContourMap} from "../../const/Defines";
import {Geometry} from "ol/geom";

type TOpts = {
    event: EventEmitter;
    map: Map;
}

export class CLContourTool extends EventEmitter {
    layerGroup: LayerGroup;
    children: CLContour[];
    map: Map;
    event: EventEmitter;

    constructor(opts: TOpts) {
        super();
        this.event = opts.event;
        this.map = opts.map;

        this.children = [];
    }

    setGroupLayer(layerGroup: LayerGroup) {
        this.layerGroup = layerGroup;
    }

    getWFSLayer() {
        const cP = this.children.find(item => item.layer.get('type') === EContourMap.WFS);

        if (cP) {
            return cP.layer as VectorLayer<VectorSource<Geometry>>;
        }
    }

    getFeatureById(id: string) {
        let feature;

        const wfsLayer = this.getWFSLayer();

        if (wfsLayer) {
            const listFeatures = wfsLayer.getSource()?.getFeatures();

            feature = listFeatures ? listFeatures.find(item => item.get('fid') === id) : undefined

        }

        return feature;
    }

    addChild(opts: IOptCLContour, isWFS: boolean = false) {
        const contour = new CLContour(opts);

        this.children.push(contour);

        if (this.layerGroup) {
            this.layerGroup.getLayers().push(contour.layer)

            if (!isWFS) {
                this.event.emit(ViewerEventName.ContourAdded, contour);
            }
        }
    }

    setChild(opts: IOptCLContour[], isReplace: boolean = true, isAddMap: boolean = false) {
        const contours: CLContour[] = [];

        opts.forEach(item => contours.push(new CLContour(item)));

        if (isReplace) {
            this.children = contours;
        } else {
            contours.forEach(item => this.children.push(item));
        }

        if (isAddMap && this.layerGroup) {
            if (isReplace) {
                const collect: Collection<TileLayer<TileWMS> | VectorLayer<VectorSource<any>>> = new Collection();
                contours.forEach(item => collect.push(item.layer));

                this.layerGroup.setLayers(collect);
            } else {
                contours.forEach(item => {
                    console.log('item', item)
                    this.layerGroup.getLayers().push(item.layer)
                });
            }
        }

        this.event.emit(ViewerEventName.ContourSets, {
            isReplace: isReplace,
            data: contours
        });
    }
}

export interface IOptCLContour extends IOptCL {
    object: TileLayer<TileWMS> | VectorLayer<VectorSource<any>>;
}

export class CLContour extends CLInterface implements CLT {
    layer: TileLayer<TileWMS> | VectorLayer<VectorSource<any>>;

    constructor(opts: IOptCLContour) {
        super(opts);

        this.layer = opts.object;
    }

    getVisible() {
        return this.layer.getVisible();
    }

    setVisible(visible: boolean) {
        this.layer.setVisible(visible)
    }
}
