import LayerGroup from "ol/layer/Group";
import {CLInterface, CLT, IOptCL} from "./CLInterface";
import {EDrawType, EProfileType} from "../../const/Defines";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import {Geometry} from "ol/geom";
import EventEmitter from "eventemitter3";
import {Collection, Feature, Map} from "ol";
import {ViewerEventName} from "../../const/Event";
import {Modify} from "ol/interaction";
import {TFormDrawerValues, TFormMarkerValues} from "./CLTypeTool";

type TOpts = {
    event: EventEmitter;
    map: Map;
}

export class CLProfileTool extends EventEmitter {
    layer?: LayerGroup;
    children: CLProfile[];
    map: Map;
    event: EventEmitter;
    pid?: string;

    constructor(opts: TOpts) {
        super();

        this.event = opts.event;
        this.map = opts.map;

        this.children = [];
    }

    setPid(pid: string) {
        this.pid = pid;
    }

    setLayer(layer: LayerGroup) {
        this.layer = layer;
    }

    getChildByFeatureId(id: string) {
        let cp;

        for (let i = 0; i < this.children.length; i++) {
            const listFeatures = this.children[i].layer.getSource()?.getFeatures();

            if (listFeatures && listFeatures.find(item => item.get('id') === id)) return cp = this.children[i]
        }
    }

    getFeatureById(id: string) {
        let feature;

        const vectorLayer = this.getLayerByIdFeature(id);

        if (vectorLayer) {
            const listFeatures = vectorLayer.getSource()?.getFeatures();

            feature = listFeatures ? listFeatures.find(item => item.get('id') === id) : undefined
        }

        return feature;
    }

    getLayerByIdFeature(id: string) {
        let cp;

        for (let i = 0; i < this.children.length; i++) {
            const listFeatures = this.children[i].layer.getSource()?.getFeatures();

            if (listFeatures && listFeatures.find(item => item.get('id') === id)) return cp = this.children[i].layer;
        }
    }

    getLayerByTypeProfile(type: EProfileType) {
        const listLayer: VectorLayer<VectorSource<any>>[] = [];

        this.children.filter(item => item.type === type).forEach(item => listLayer.push(item.layer));

        return listLayer
    }

    setChild(opts: IOptCLProfile[], isReplace: boolean = true, isAddMap: boolean = false, isDrawer = false, initValues?: TFormDrawerValues | TFormMarkerValues) {
        const profiles: CLProfile[] = [];

        opts.forEach(item => profiles.push(new CLProfile(item)));

        if (isReplace) {
            this.children = profiles;
        } else {
            profiles.forEach(item => this.children.push(item));
        }

        if (isAddMap && this.layer) {
            if (isReplace) {
                const collect: Collection<VectorLayer<VectorSource<Geometry>>> = new Collection();

                profiles.forEach(item => collect.push(item.layer));

                this.layer.setLayers(collect);
            } else {
                profiles.forEach(item => this.layer!.getLayers().push(item.layer));
            }
        }

        this.event.emit(ViewerEventName.ProfileSets, {
            isReplace: isReplace,
            isDrawer: isDrawer,
            data: profiles,
            initValues: initValues
        });
    }

    removeChild(profile: CLProfile) {
        const index = this.children.indexOf(profile);

        if (index > -1) {
            this.children.splice(index, 1);

            if (this.layer) {
                this.layer.getLayers().remove(profile.layer);
            }

            this.event.emit(ViewerEventName.ProfileRemoved, profile)
        }
    }

    getFeatures(init?: boolean) {
        const features: Feature<Geometry>[] = [];

        this.children.forEach((item) => {
            let is = true;

            if (init !== undefined) {
                is = item.init === init;
            }

            if (is) {
                const feature = item.getFeature();

                if (feature) {
                    features.push(feature);
                }
            }
        });

        return features;
    }

    addFeature(opts: IOptCLProfile) {
        const profile = new CLProfile(opts);

        this.event.emit(ViewerEventName.FeatureAdded, profile, false);

        return profile;
    }

    setFeatures(opts: IOptCLProfile[]) {
        const features: CLProfile[] = []

        for (let i = 0, length = opts.length; i < length; i++) {
            features.push(new CLProfile(opts[i]));
        }

        this.event.emit(ViewerEventName.FeatureSets, features);
    }

    removeFeature(profile: CLProfile) {
        const source = profile.layer.getSource();

        if (source === null) {
            return
        }

        const features = source.getFeatures();
        const feature = profile.feature;
        if (!feature) {
            return
        }
        const index = features.indexOf(feature);

        if (index > -1) {
            if (feature.get('arrow')) {
                features.forEach(item => {
                    if (item.get('id') === feature.get('id')) {
                        source.removeFeature(feature);
                    }
                })
            } else {
                features.splice(index, 1);

                source.removeFeature(feature);
            }
            this.event.emit(ViewerEventName.FeatureRemoved, profile)
        }
    }

    getSource() {
        const sources: VectorSource<Geometry>[] = [];

        this.layer?.getLayers().forEach(layer => {
            if (layer) {
                const source = (layer as VectorLayer<VectorSource<Geometry>>).getSource();

                if (source) {
                    sources.push(source)
                }
            }
        })
        return sources;
    }

    getLayerByIdLayer(id: string, isChild: boolean) {
        const cP = this.children.find(item => item.id === id);

        if (cP) {
            return isChild ? cP : cP.layer;
        }
    }
}

export interface IOptCLProfile extends IOptCL {
    type: EProfileType;
    typeDraw: EDrawType;
    layer: VectorLayer<VectorSource<Geometry>>;
    init: boolean;
    feature?: Feature<Geometry>;
}

export class CLProfile extends CLInterface implements CLT {
    type: EProfileType;
    typeDraw: EDrawType;
    layer: VectorLayer<VectorSource<Geometry>>;
    init: boolean;
    feature?: Feature<Geometry>;

    protected _targetModify?: Modify;

    constructor(opts: IOptCLProfile) {
        super(opts);

        this.type = opts.type;
        this.typeDraw = opts.typeDraw;
        this.layer = opts.layer;
        this.init = opts.init;
        this.feature = opts.feature;
    }

    getVisible() {
        return this.layer.getVisible();
    }

    setVisible(visible: boolean) {
        this.layer.setVisible(visible)
    }

    getFeature(): Feature<Geometry> | undefined {
        return this.layer.getSource()?.getFeatures().find(item => item.get('id') === this.id);
    }

    setTargetModify(targetModify: Modify) {
        this._targetModify = targetModify;
    }

    getTargetModify() {
        return this._targetModify;
    }
}
