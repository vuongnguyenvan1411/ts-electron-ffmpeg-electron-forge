import {CLInterface, IOptCL} from "./CLInterface";
import {Feature, Map} from "ol";
import {Geometry} from "ol/geom";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import EventEmitter from "eventemitter3";
import {ViewerEventName} from "../../const/Event";

type TOpts = {
    event: EventEmitter;
    map: Map;
}

export class CLMarkerTool extends EventEmitter {
    layer?: VectorLayer<VectorSource<Geometry>>;
    children: CLMarker[];
    map: Map;
    event: EventEmitter;

    constructor(opts: TOpts) {
        super();

        this.event = opts.event;
        this.map = opts.map;
        this.children = [];
    }

    setLayer(layer: VectorLayer<VectorSource<Geometry>>) {
        this.layer = layer;
    }

    addChild(opts: IOptCLMarker, isAddMap: boolean = false) {
        const marker = new CLMarker(opts);

        this.children.push(marker);

        if (isAddMap && this.layer) {
            this.layer.getSource()?.addFeature(marker.object)
        }

        this.event.emit(ViewerEventName.MarkerAdded, marker);
    }

    setChild(opts: IOptCLMarker[], isReplace: boolean = true, isAddMap: boolean = false) {
        const markers: CLMarker[] = [];

        opts.forEach(item => markers.push(new CLMarker(item)));

        if (isReplace) {
            this.children = markers;
        } else {
            markers.forEach(item => this.children.push(item));
        }

        if (isAddMap && this.layer) {
            if (isReplace) {
                const collect: Feature<Geometry>[] = [];
                markers.forEach(item => collect.push(item.object));

                this.layer.setSource(new VectorSource({
                    features: collect
                }))
            } else {
                markers.forEach(item => this.layer?.getSource()?.addFeature(item.object));
            }
        }

        this.event.emit(ViewerEventName.MarkerSets, {
            isReplace: isReplace,
            data: markers
        });
    }
}

export interface IOptCLMarker extends IOptCL {
    object: Feature<Geometry>;
}

export class CLMarker extends CLInterface {
    object: Feature<Geometry>;

    constructor(opts: IOptCLMarker) {
        super(opts);

        this.object = opts.object;
    }
}
