import {RGBColor} from "react-color";
import {RawDraftContentState} from "react-draft-wysiwyg";
import {EDrawingDashed, EDrawingStyle, EDrawingWeight, EMenuMapItem, EPlaceBlock} from "../../const/Defines";
import {Coordinate} from "ol/coordinate";

export type TFormDrawerValues = {
    color?: {
        fillColor?: {
            rgb: RGBColor,
        },
        strokeColor?: {
            rgb: RGBColor,
        },
    },
    drawingStyle?: EDrawingStyle,
    textStyle?: {
        size: number,
        weight: EDrawingWeight,
        arrow: boolean,
    }
    stroke?: {
        width: number,
        dashed: EDrawingDashed,
    },
    transform?: {
        width?: number,
        height?: number,
        angle?: number,
    },
    position?: {},
    note?: string,
    placeBlock?: EPlaceBlock,
    modifyFeature?: {
        id?: string,
        coordinate?: Coordinate,
        type?: EMenuMapItem.MeasureLayer | EMenuMapItem.ProfileLayer;
    },
}

export type TFormMarkerValues = {
    id?: string,
    coordinate?: number[],
    title: string,
    description?: RawDraftContentState,
    images?: {
        link: string,
        description: string,
    }[],
    videos?: {
        link: string,
        description: string,
    }[],
    hyperlink?: {
        link: string,
        target: string,
    }
}