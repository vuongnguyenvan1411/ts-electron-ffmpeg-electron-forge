import {CLInterface, CLT, IOptCL} from "./CLInterface";
import {Feature, Map} from "ol";
import {Geometry, Polygon} from "ol/geom";
import {Utils2D} from "../../core/Utils";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import EventEmitter from "eventemitter3";
import {ViewerEventName} from "../../const/Event";
import {chunk} from "lodash";

type TOpts = {
    event: EventEmitter;
    map: Map;
}

export class CLShareRegionTool extends EventEmitter {
    layer?: VectorLayer<VectorSource<Geometry>>;
    children: CLShareRegion[];
    map: Map;
    event: EventEmitter;
    feature?: Feature<Geometry>;

    constructor(opts: TOpts) {
        super();

        this.event = opts.event;
        this.map = opts.map;

        this.children = [];
    }

    setLayer(layer: VectorLayer<VectorSource<any>>) {
        this.layer = layer;
    }

    addChild(opts: IOptCLShareRegion, isAddMap: boolean = false) {
        const shareRegion = new CLShareRegion(opts);

        this.children.push(shareRegion);

        if (isAddMap && this.layer) {
            this.layer.getSource()?.addFeature(shareRegion.feature)
        }

        this.feature = opts.feature;

        this.event.emit(ViewerEventName.ShareRegionAdded, {
            isReplace: false,
            isDrawer: false,
            data: [shareRegion],
        });
    }

    removeChild(shareRegion: CLShareRegion) {
        const index = this.children.indexOf(shareRegion);

        if (index > -1) {
            this.children.splice(index, 1);

            if (this.layer) this.layer.getSource()?.removeFeature(shareRegion.feature);

            this.event.emit(ViewerEventName.ShareRegionRemoved, shareRegion)
        }
    }

    removeAllChild() {
        this.children.forEach(item => this.removeChild(item));
    }

    getFlatCoordinates(): number[][] {
        if (this.feature) {
            const geometry: Polygon = this.feature.getGeometry() as Polygon;

            return chunk(geometry.getFlatCoordinates(), 2);
        }

        return [];
    }
}

export interface IOptCLShareRegion extends IOptCL {
    feature: Feature<Geometry>,
    layer: VectorLayer<VectorSource<any>>,
}

export class CLShareRegion extends CLInterface implements CLT {
    feature: Feature<Geometry>;
    layer: VectorLayer<VectorSource<any>>;

    protected _visible: boolean;

    constructor(opts: IOptCLShareRegion) {
        super(opts);

        this.feature = opts.feature;
        this.layer = opts.layer;
        this._visible = true;
    }

    getVisible() {
        return this._visible;
    }

    setVisible(visible: boolean) {
        this._visible = visible;

        if (visible) {
            this.feature.setStyle();
        } else {
            this.feature.setStyle(Utils2D.styleHideVectorLayer());
        }
    }
}
