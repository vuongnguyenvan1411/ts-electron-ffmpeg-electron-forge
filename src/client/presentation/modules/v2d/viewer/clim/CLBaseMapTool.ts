import LayerGroup from "ol/layer/Group";
import TileLayer from "ol/layer/Tile";
import {TileImage} from "ol/source";
import {CLInterface, IOptCL} from "./CLInterface";
import EventEmitter from "eventemitter3";
import {Map} from "ol";
import {ViewerEventName} from "../../const/Event";

type TOpts = {
    event: EventEmitter;
    map: Map;
}

export class CLBaseMapTool extends EventEmitter {
    layer?: LayerGroup;
    children: CLBaseMap[];
    map: Map;
    event: EventEmitter;

    constructor(opts: TOpts) {
        super();

        this.event = opts.event;
        this.map = opts.map;

        this.children = [];
    }

    setLayer(layer: LayerGroup) {
        this.layer = layer;
    }

    getActiveLayer() {
        let activeLayer;

        if (this.layer) {
            this.layer.getLayersArray().forEach(layer => {
                if (layer.getVisible()) {
                    activeLayer = layer;
                }
            })
        }

        return activeLayer
    }

    clear() {
        if (this.layer) {
            this.layer.getLayers().forEach(layer => {
                const source = (layer as TileLayer<any>).getSource()
                source.clear()
            })
        }
    }

    addChild(opts: IOptCLBaseMap, isAddMap: boolean = false) {
        const baseMap = new CLBaseMap(opts);

        this.children.push(baseMap);

        if (isAddMap && this.layer) {
            this.layer.getLayers().push(baseMap.object)
        }

        this.event.emit(ViewerEventName.BaseMapAdded, baseMap);
    }
}

export interface IOptCLBaseMap extends IOptCL {
    object: TileLayer<TileImage>;
}

export class CLBaseMap extends CLInterface {
    object: TileLayer<TileImage>;

    constructor(opts: IOptCLBaseMap) {
        super(opts);

        this.object = opts.object;
    }

    getVisible() {
        return this.object.getVisible();
    }

    setVisible(visible: boolean) {
        this.object.setVisible(visible)
    }
}
