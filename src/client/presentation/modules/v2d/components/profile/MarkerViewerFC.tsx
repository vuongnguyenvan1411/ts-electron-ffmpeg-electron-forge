import {Card, Carousel, Image, Modal, Popconfirm, Space, Typography} from "antd";
import React, {useEffect, useState} from "react";
import EventEmitter from "eventemitter3";
import {useTranslation} from "react-i18next";
import Plyr from "plyr";
import {DeleteOutlined, EditOutlined, GlobalOutlined, PictureOutlined, PlayCircleFilled, VideoCameraOutlined} from "@ant-design/icons";
import sms from "../../styles/MapView.module.scss";
import draftToHtml from "draftjs-to-html";
import {Color} from "../../../../../const/Color";
import {Viewer} from "../../viewer/Viewer";
import Meta from "antd/es/card/Meta";
import {CLProfile} from "../../viewer/clim/CLProfileTool";
import PlyrNormalFC from "../../../../widgets/video/PlyrNormalFc";
import {getYoutubeIdFromUrl, ProfileMarkerModalFC} from "./ProfileMarkerModalFC";
import {TFormMarkerValues} from "../../viewer/clim/CLTypeTool";
import {Feature} from "ol";
import RenderFeature from "ol/render/Feature";
import {Geometry, Point} from "ol/geom";
import {FeatureSingleton} from "../../../../../models/FeatureSingleton";
import {CustomButton} from "../../../../components/CustomButton";
import {CustomTypography} from "../../../../components/CustomTypography";

export const MarkerViewerFC = (props: {
    viewer: Viewer,
    onClose: () => void,
    feature: RenderFeature | Feature<Geometry>,
    event: EventEmitter,
}) => {
    const {t} = useTranslation();
    const feature = props.feature as Feature<Geometry>;
    const currentData = feature.getProperties() as TFormMarkerValues;
    const [isHyperLinkModal, setIsHyperLinkModal] = useState(false);

    const [isModalEditVisible, setIsModalEditVisible] = useState<{
        object?: CLProfile,
        initValues?: TFormMarkerValues,
        visible: boolean,
    }>({
        visible: false,
    });
    const [isModalViewVideoVisible, setIsModalViewVideoVisible] = useState<{
        visible: boolean,
        source?: Plyr.SourceInfo,
    }>({
        visible: false,
    });

    const onClickFormProfileModal = () => {
        const idFeature = currentData.id;

        if (!idFeature) {
            return
        }

        const object = props.viewer.profileTool.getChildByFeatureId(idFeature);

        setIsModalEditVisible({
            visible: true,
            object: object,
            initValues: {
                ...currentData,
                coordinate: (feature.getGeometry() as Point).getCoordinates(),
            }
        })
    }

    const onCloseFormProfileModal = () => {
        setIsModalEditVisible({
            visible: false
        });
    }

    useEffect(() => {
        console.log('%cMount Screen: MapViewProfileMarkerModalFC', Color.ConsoleInfo);

        return () => {
            console.log('%cUnmount Screen: MapViewProfileMarkerModalFC', Color.ConsoleInfo);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onConfirmDeleteMarker = () => {
        const idFeature = currentData.id;

        if (!idFeature) {
            console.log('%cError remove profile marker because id undefined', Color.ConsoleError);

            return;
        }

        const feature = props.viewer.profileTool.getFeatureById(idFeature);
        const vectorLayer = props.viewer.profileTool.getLayerByIdFeature(idFeature);

        if (!feature || !vectorLayer) {
            return;
        }

        const objectDelete = {
            layer: vectorLayer,
            init: false,
            feature: feature,
            id: idFeature
        } as CLProfile;

        props.viewer.profileTool.removeFeature(objectDelete);

        props.onClose();
    }

    const renderInfo = () => {
        return (
            <>
                {
                    <Card
                        title={
                            <CustomTypography
                                textStyle={'text-16-24'}
                                className={'text-center'}
                                isStrong
                            >
                                {currentData.title}
                            </CustomTypography>
                        }
                        type="inner"
                        headStyle={{
                            maxHeight: "20%",
                            textAlignLast: "center",
                            padding: 0,
                        }}
                        bodyStyle={{padding: '4px 2px'}}
                        actions={[
                            <>
                                {
                                    currentData.hyperlink && currentData.hyperlink.link
                                        ? <Typography.Link
                                            strong
                                            className={'text-center block'}
                                            key={'link'}
                                            onClick={() => {
                                                if (currentData.hyperlink!.target === 'in') {
                                                    setIsHyperLinkModal(true)
                                                } else {
                                                    window.open(currentData.hyperlink!.link, "_blank")
                                                }
                                            }}
                                        >
                                            <GlobalOutlined/> Đường dẫn
                                        </Typography.Link>
                                        : null
                                }
                                {
                                    FeatureSingleton.getInstance().v2DProfile && FeatureSingleton.getInstance().isContributor
                                        ? [
                                            <Space
                                                size={[0, 0]}
                                                className={'pt-4'}
                                                key={'footer-marker-viewer'}>
                                                <Popconfirm
                                                    overlayClassName={'popconfirm-content-main'}
                                                    key={'delete'}
                                                    title={t('text.confirmDelete')}
                                                    onConfirm={onConfirmDeleteMarker}
                                                    okText={t('button.ok')}
                                                    cancelText={t('button.cancel')}
                                                >
                                                    <CustomButton
                                                        className={'btn-marker-footer'}
                                                        usingFor={"geodetic"}
                                                        icon={<DeleteOutlined/>}
                                                        size={"small"}
                                                    >
                                                        {t('button.delete')}
                                                    </CustomButton>
                                                </Popconfirm>
                                                <CustomButton
                                                    className={'btn-marker-footer'}
                                                    usingFor={"geodetic"}
                                                    key="edit"
                                                    onClick={onClickFormProfileModal}
                                                    icon={<EditOutlined/>}
                                                    size={"small"}
                                                >
                                                    {t('text.edit')}
                                                </CustomButton>
                                            </Space>
                                        ]
                                        : null
                                }
                            </>,
                        ]}
                    >
                        {
                            (currentData.videos || currentData.images) &&
                            <Space
                                direction="vertical"
                                className={'justify-center w-full h-full'}
                            >
                                <Image.PreviewGroup>
                                    <Carousel
                                        effect="fade"
                                        infinite={false}
                                        className={'w-full h-full text-center px-4 mb-2'}
                                        swipeToSlide
                                    >
                                        {
                                            (currentData.images && currentData.images.length > 0) && currentData.images.map((item, index) => {
                                                return (
                                                    <Space
                                                        key={`${index}_image`}
                                                        style={{
                                                            color: '#fff',
                                                            textAlign: 'center',
                                                        }}
                                                    >
                                                        <Image
                                                            src={item.link}
                                                            alt={item.link}
                                                            className={'text-center max-h-fit max-w-fit rounded'}
                                                        />
                                                        <CustomTypography
                                                            textStyle={'text-14-18'}
                                                            key={`${index}_description`}
                                                            className={'text-center whitespace-nowrap overflow-hidden overflow-ellipsis block'}
                                                        >
                                                            <PictureOutlined/> {item.description || `${t('text.image')} ${index + 1}`}
                                                        </CustomTypography>
                                                    </Space>
                                                )
                                            })
                                        }
                                    </Carousel>
                                </Image.PreviewGroup>
                                <Carousel
                                    effect="fade"
                                    infinite={false}
                                    className={'w-full h-full text-center px-4 mb-2'}
                                    swipeToSlide
                                >
                                    {
                                        (currentData.videos && currentData.videos.length > 0) && currentData.videos.map((item, index) => {
                                            if (item.link !== null && item.link !== undefined) {
                                                return (
                                                    <div key={item.link}>
                                                        <div
                                                            key={`${index}_link`}
                                                            style={{
                                                                color: '#fff',
                                                                textAlign: 'center',
                                                            }}>
                                                            <Image
                                                                alt={item.link}
                                                                src={`https://i1.ytimg.com/vi/${getYoutubeIdFromUrl(item.link)}/hqdefault.jpg`}
                                                                preview={false}
                                                                width={384}
                                                                height={256}
                                                            />
                                                            <PlayCircleFilled
                                                                className={`${sms.ModalVideo}`}
                                                                onClick={() => setIsModalViewVideoVisible({
                                                                    ...isModalViewVideoVisible,
                                                                    visible: true,
                                                                    source: {
                                                                        type: "video",
                                                                        sources: [
                                                                            {
                                                                                src: item.link,
                                                                                provider: "youtube",
                                                                            }
                                                                        ]
                                                                    }
                                                                })}
                                                            />
                                                        </div>
                                                        <CustomTypography
                                                            key={`${index}_description`}
                                                            textStyle={'text-14-18'}
                                                            className={'text-center whitespace-nowrap overflow-hidden overflow-ellipsis block'}
                                                        >
                                                            <VideoCameraOutlined/> {item.description || `Video ${index + 1}`}
                                                        </CustomTypography>
                                                    </div>
                                                )
                                            } else {
                                                return null
                                            }
                                        })
                                    }
                                </Carousel>

                            </Space>
                        }
                        <Meta
                            className={'justify-center w-full px-2'}
                            description={
                                currentData.description
                                    ? <div
                                        style={{
                                            fontSize: 12,
                                            color: 'rgba(0, 0, 0, 0.8)',
                                            fontVariant: "tabular-nums",
                                            textAlign: "center",
                                            overflowY: "auto",
                                            maxHeight: '100px',
                                            wordBreak: "break-word"

                                        }}
                                        dangerouslySetInnerHTML={{__html: draftToHtml(currentData.description)}}
                                    />
                                    : null
                            }
                        />
                    </Card>
                }
                <Modal
                    className={sms.ModalMarker}
                    visible={isHyperLinkModal}
                    width={1200}
                    title={currentData.title}
                    onOk={() => setIsHyperLinkModal(false)}
                    cancelButtonProps={{
                        style: {
                            display: "none",
                        },
                    }}
                    onCancel={() => setIsHyperLinkModal(false)}
                >
                    {
                        currentData.hyperlink
                            ? <iframe
                                className={"w-full border-0"}
                                src={currentData.hyperlink.link}
                                title="Auto Timelapse"
                                height={'500px'}
                            />
                            : null
                    }
                </Modal>
            </>
        )
    }

    return (
        <>
            {
                isModalEditVisible.visible && isModalEditVisible.object
                    ? <ProfileMarkerModalFC
                        event={props.event}
                        viewer={props.viewer}
                        onClose={onCloseFormProfileModal}
                        initValues={isModalEditVisible.initValues}
                        selectedInfo={isModalEditVisible.object}
                    />
                    : null
            }
            {renderInfo()}
            <Modal
                visible={isModalViewVideoVisible.visible}
                onCancel={() => setIsModalViewVideoVisible({
                    ...isModalViewVideoVisible,
                    visible: false,
                })}
                onOk={() => setIsModalViewVideoVisible({
                    ...isModalViewVideoVisible,
                    visible: false,
                })}
                destroyOnClose={true}
                width={800}
                style={{height: 'full'}}
            >
                <PlyrNormalFC
                    source={isModalViewVideoVisible.source}
                />
            </Modal>
        </>
    )
}
