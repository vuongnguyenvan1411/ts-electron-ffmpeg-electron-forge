import {Draw} from "ol/interaction";
import {StyleProfileFn} from "./StyleDrawFn";
import {DrawEventName, MapEventName, ViewerEventName} from "../../const/Event";
import {DrawEvent} from "ol/interaction/Draw";
import {Feature} from "ol";
import {Geometry} from "ol/geom";
import {v4 as uuid} from "uuid";
import {EDrawType, EMapLayer, EMenuMapItem, EProfileType} from "../../const/Defines";
import {Viewer} from "../../viewer/Viewer";
import {TFunction} from "react-i18next";
import {PointerMoveHandler, removeAction} from "../tool/MapAction";
import EventEmitter from "eventemitter3";

export const DrawShareRegion = (props: {
    viewer: Viewer,
    t: TFunction<"translation">,
    onCloseShareModal: () => void,
    event: EventEmitter,
}) => {
    props.onCloseShareModal();
    removeAction(props.viewer);

    let activeTip = props.t('text.continueRegionMsg');
    let tip = props.t('text.clickToStartDraw');

    const regionLayer = props.viewer.shareTool.layer;
    if (!regionLayer) return;

    const regionSource = regionLayer.getSource();
    if (regionSource === null) return

    regionLayer.setProperties({
        id: EMapLayer.ShareRegion,
        title: props.t('text.shareRegion'),
        type: EProfileType.Share,
    })

    props.viewer.shareTool.removeAllChild();

    const draw = new Draw({
        source: regionSource,
        type: EDrawType.Polygon,
        style: (feature: any) => StyleProfileFn(props.viewer, feature, true, tip),
    });

    draw.on(DrawEventName.DrawStart, () => tip = activeTip)

    draw.on(DrawEventName.DrawEnd, (evt: DrawEvent) => {

        props.viewer.map.once(MapEventName.PointerMove, PointerMoveHandler);
        props.event.emit(ViewerEventName.DrawerOpened, EMenuMapItem.ProfileLayer);

        const feature: Feature<Geometry> = evt.feature;
        const id = uuid();

        feature.set('id', id);
        feature.set('visible', true);
        draw.setActive(false);

        props.viewer.shareTool.addChild({
            id: id,
            title: props.t('text.shareRegion'),
            feature: feature,
            layer: regionLayer,
        })
    });

    props.viewer.map.addInteraction(draw);
};