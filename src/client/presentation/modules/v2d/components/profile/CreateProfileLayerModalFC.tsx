import {Viewer} from "../../viewer/Viewer";
import {EDrawType, EProfileType, EPropertiesType} from "../../const/Defines";
import {Form, Input, Modal, Select, Space} from "antd";
import {useTranslation} from "react-i18next";
import {MinusCircleOutlined} from "@ant-design/icons";
import scm from "../../../../../styles/module/Common.module.scss";
import React, {useEffect, useState} from "react";
import {Color} from "../../../../../const/Color";
import {v4 as uuid} from "uuid";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import {getTitleDrawProfile} from "./GetTitleDrawPropfile";
import {CLProfile, IOptCLProfile} from "../../viewer/clim/CLProfileTool";
import {removeAction} from "../tool/MapAction";
import {ViewerEventName} from "../../const/Event";
import EventEmitter from "eventemitter3";
import {CustomButton} from "../../../../components/CustomButton";
import Image from "next/image";
import {Images} from "../../../../../const/Images";

export type TFormCreateProfile = {
    name: string,
    typeGeom: EProfileType,
    properties?: {
        name: string,
        values: string,
    }[],
    id?: string,
}

export const CreateProfileLayerModalFC = (props: {
    viewer: Viewer,
    onClose: () => void,
    pType?: EProfileType,
    event: EventEmitter,
    modifyValues?: {
        initValues?: TFormCreateProfile,
        selected?: CLProfile,
    },
}) => {
    const {t} = useTranslation();
    const [form] = Form.useForm();
    const isEdit = !!props.modifyValues?.initValues;

    removeAction(props.viewer);

    useEffect(() => {
        console.log('%cMount Screen: MapCreateDrawLayerModalFC', Color.ConsoleInfo);

        if (props.modifyValues) {
            form.setFieldsValue(props.modifyValues.initValues);
        }

        return () => {
            console.log('%cUnmount Screen: MapCreateDrawLayerModalFC', Color.ConsoleInfo);
        };

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onFormCreateLayerFinish = (values: TFormCreateProfile) => {
        const id = uuid();
        let typeDraw: EDrawType;
        let typeObject;
        let isSetProperties: boolean;

        isSetProperties = !(values.typeGeom === EProfileType.Annotation || values.typeGeom == EProfileType.Marker);

        if (values.typeGeom === EProfileType.Annotation || values.typeGeom === EProfileType.Marker) {
            typeDraw = EDrawType.Point
            typeObject = values.typeGeom
        } else {
            typeObject = EProfileType.Draw

            switch (values.typeGeom) {
                default:
                    typeDraw = EDrawType.Polygon

                    break;
                case EProfileType.Circle:
                    typeDraw = EDrawType.Circle

                    break;
                case EProfileType.LineString:
                    typeDraw = EDrawType.LineString

                    break;
            }
        }

        const titleProfile = getTitleDrawProfile({
            t: t,
            typeDraw: typeDraw,
            isNote: true,
            typeProfile: values.typeGeom,
            noteValues: values.name
        });

        const vectorLayer = new VectorLayer({
            source: new VectorSource(),
            properties: {
                id: id,
                title: titleProfile,
                typeDraw: typeDraw,
                type: typeObject,
                properties: isSetProperties ? values.properties : null,
            }
        });

        const listOpts: IOptCLProfile[] = [
            {
                id: id,
                title: titleProfile,
                type: typeObject,
                typeDraw: typeDraw,
                layer: vectorLayer,
                init: false
            }
        ]
        props.viewer.profileTool.setChild(listOpts, false, true, true)
    }

    const onFormModifyProfileLayer = (values: TFormCreateProfile) => {
        if (!props.modifyValues) {
            return
        }

        const selected = props.modifyValues.selected as CLProfile;

        selected.title = values.name;

        selected.layer.setProperties({
            ...selected.layer.getProperties(),
            title: values.name,
        });

        props.event.emit(ViewerEventName.ProfileModified, {
            object: selected,
            editLayer: values,
        })
    }

    return (
        <Modal
            className={'modal-main ant-modal-v2d middle'}
            onCancel={props.onClose}
            bodyStyle={{padding: "1rem"}}
            title={
                props.pType === EProfileType.Annotation ? t('text.addAnnotation')
                    : (props.pType === EProfileType.Marker ? t('text.addMarker') : t('text.createDrawLayer'))
            }
            closeIcon={<Image
                height={24}
                width={24}
                src={Images.iconXCircle.data}
                alt={Images.iconXCircle.atl}
            />}
            footer={
                <CustomButton
                    size={'small'}
                    usingFor={'geodetic'}
                    key={"save"}
                    onClick={() => {
                        form
                            .validateFields()
                            .then((values) => {
                                form.resetFields();
                                if (isEdit) {
                                    onFormModifyProfileLayer(values);
                                } else {
                                    onFormCreateLayerFinish(values);
                                }
                            })
                            .then(props.onClose)
                            .catch((info) => {
                                console.log('Validate Failed:', info);
                            });
                    }}
                >
                    {t("button.ok")}
                </CustomButton>
            }
            visible
            centered
            closable
        >
            <Form
                className={scm.ModalForm}
                form={form}
                labelCol={{span: 6}}
                labelAlign={"left"}
            >
                <FormCreateProfileLayer
                    pType={props.pType}
                    isEdit={isEdit}
                />
            </Form>
        </Modal>
    )
}

export const FormCreateProfileLayer = (props: {
    pType?: EProfileType,
    isEdit: boolean
}) => {
    const {t} = useTranslation();

    const [isPropertiesHide, setIsPropertiesHide] = useState(false);

    useEffect(() => {
        if (props.pType === EProfileType.Annotation || props.pType === EProfileType.Marker) {
            setIsPropertiesHide(true)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.pType])

    const onSelectTypeProfile = (value: string) => {
        if (value === EProfileType.Annotation || value === EProfileType.Marker) {
            setIsPropertiesHide(true)
        } else {
            setIsPropertiesHide(false)
        }
    }

    return (
        <>
            <Form.Item
                name="name"
                className={'form-item form-item-mb-4 '}
                label={t("text.name")}
                rules={[
                    {
                        max: 100,
                        min: 1,
                        message: t('validation.minAndMaxCharacter', {
                            label: t("text.name"),
                            min: '1',
                            max: '100'
                        }),
                    }
                ]}
            >
                <Input
                    className={'input-main input-v2d input-h-40 input-bg-white input-radius-4'}
                    allowClear={true}
                    placeholder={t("text.layerProfileName")}
                />
            </Form.Item>
            <Form.Item
                name="typeGeom"
                className={'form-item form-item-mb-4 '}
                label={t("text.drawType")}
                initialValue={props.pType ?? EProfileType.LineString}
                rules={[
                    {
                        required: true,
                        message: t('message.pleaseSelectGeomType')
                    }
                ]}
            >
                <Select
                    className={'select-main select-h-40 select-radius-4'}
                    placeholder={t('text.selectGeomType')}
                    onChange={(value) => onSelectTypeProfile(value)}
                    allowClear
                    disabled={props.isEdit ? props.isEdit : !!props.pType}
                >
                    <Select.Option value={EProfileType.LineString}>{t('text.drawLine')}</Select.Option>
                    <Select.Option value={EProfileType.Polygon}>{t('text.drawPolygon')}</Select.Option>
                    <Select.Option value={EProfileType.Circle}>{t('text.drawCircle')}</Select.Option>
                    <Select.Option value={EProfileType.Annotation}>{t('text.addAnnotation')}</Select.Option>
                    <Select.Option value={EProfileType.Marker}>{t('text.addMarker')}</Select.Option>
                </Select>
            </Form.Item>
            <Form.Item
                className={'form-item form-item-mb-4 '}
                label={t("text.properties")}
                hidden={isPropertiesHide}
            >
                <Form.List
                    name="properties"
                >
                    {(fields, {add, remove}) => (
                        <>
                            {fields.map(({key, name, ...restField}) => (
                                <Space
                                    key={key}
                                    style={{display: 'flex', marginBottom: 8}}
                                    size={[16, 16]}
                                >
                                    <Form.Item
                                        {...restField}
                                        name={[name, 'name']}
                                        rules={[
                                            {
                                                required: true,
                                                message: t('message.pleaseEnterAtrName'),
                                                whitespace: true,
                                            },
                                            {
                                                max: 100,
                                                message: t('validation.minAndMaxCharacter', {
                                                    label: t("text.name"),
                                                    min: '1',
                                                    max: '100'
                                                }),
                                            },
                                        ]}
                                    >
                                        <Input
                                            className={'input-main input-v2d input-h-40 input-bg-white input-radius-4'}
                                            allowClear={true}
                                            placeholder={t('text.atrName')}
                                            disabled={props.isEdit}
                                        />
                                    </Form.Item>
                                    <Form.Item
                                        {...restField}
                                        name={[name, 'type']}
                                        initialValue={EPropertiesType.Number}
                                    >
                                        <Select
                                            className={'select-main select-v2d select-h-40 select-radius-4'}
                                            dropdownClassName={'select-dropdown-v2d'}
                                            disabled={props.isEdit}
                                        >
                                            <Select.Option value={EPropertiesType.Number}> {t('text.numberFormat')} </Select.Option>
                                            <Select.Option value={EPropertiesType.String}>  {t('text.stringFormat')}</Select.Option>
                                            <Select.Option value={EPropertiesType.Date}>  {t('text.dateFormat')} </Select.Option>
                                        </Select>
                                    </Form.Item>
                                    <MinusCircleOutlined
                                        className="dynamic-delete-button"
                                        onClick={(evt) => {
                                            if (props.isEdit) {
                                                evt.preventDefault()
                                            } else {
                                                remove(name)
                                            }
                                        }}
                                    />
                                </Space>
                            ))}
                            <Form.Item>
                                <CustomButton
                                    onClick={() => add()}
                                    usingFor={'geodetic'}
                                    type={'outline'}
                                    icon={<Image
                                        src={Images.iconPlusCircle.data}
                                        alt={Images.iconPlusCircle.atl}
                                        width={16}
                                        height={16}
                                    />}
                                    style={{width: '60%'}}
                                    className={'btn-form'}
                                    disabled={props.isEdit}
                                >
                                    {t('text.addMoreAtrRow')}
                                </CustomButton>
                            </Form.Item>
                        </>
                    )}
                </Form.List>
            </Form.Item>
        </>
    )
}