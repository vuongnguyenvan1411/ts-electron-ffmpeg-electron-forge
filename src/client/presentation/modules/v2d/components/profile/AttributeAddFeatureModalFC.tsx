import {Form, Input, Modal, Popconfirm} from "antd";
import {Viewer} from "../../viewer/Viewer";
import {Geometry} from "ol/geom";
import React, {useEffect} from "react";
import {useTranslation} from "react-i18next";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import {Feature} from "ol";
import {EPropertiesType} from "../../const/Defines";
import {Color} from "../../../../../const/Color";
import Image from "next/image";
import {Images} from "../../../../../const/Images";
import {CustomButton} from "../../../../components/CustomButton";

export type TAttribute = {
    name: string,
    type: EPropertiesType;
    fid?: string;
}

export const AttributeAddFeatureModalFC = (props: {
    viewer: Viewer,
    onCloseRemove: () => void,
    onClose: () => void,
    vectorLayer: VectorLayer<VectorSource<Geometry>>,
    feature: Feature<Geometry>,
    onFormAddAttributeFinish: (values: any) => void,
}) => {

    useEffect(() => {
        console.log('%cMount Screen: AttributesFeatureModal', Color.ConsoleInfo);

        return () => {
            console.log('%cUnmount Screen: AttributesFeatureModal', Color.ConsoleInfo);
        };

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const [form] = Form.useForm();
    const {t} = useTranslation();
    const properties = props.vectorLayer.get('properties') as TAttribute[];

    const onAfterClose = () => {
        form.resetFields();
    }

    const onOk = () => {
        form
            .validateFields()
            .then((values) => {
                form.resetFields();
                props.onFormAddAttributeFinish(values);
            })
            .then(props.onClose)
    }

    const formAddAttribute = () => {
        return properties.map((property, index) => {
            return (
                <Form.Item
                    name={property.name}
                    key={index}
                    rules={[
                        {
                            required: true,
                            message: t('message.pleaseEnterAtr'),
                            whitespace: true,
                        },
                        {
                            max: 100,
                            message: t('validation.minAndMaxCharacter', {
                                label: t("text.name"),
                                min: '1',
                                max: '100'
                            }),
                        },
                    ]}
                    label={property.name}
                >
                    <Input
                        className={'input-main input-v2d input-h-40 input-bg-white input-radius-4'}
                        type={property.type}
                        allowClear
                        placeholder={t('text.enterAttributes')}
                    />
                </Form.Item>
            )
        })
    }

    return (
        <Modal
            className={'modal-main ant-modal-v2d middle'}
            key={'modal-form-save-attribute'}
            visible
            closable
            centered
            closeIcon={
                <Popconfirm
                    overlayClassName={'popconfirm-content-main'}
                    title={t('title.confirmBeforeDeleting')}
                    okText={t('button.ok')}
                    cancelText={t('button.cancel')}
                    key={'confirm'}
                    onConfirm={props.onCloseRemove}
                    placement="top"
                    arrowPointAtCenter
                >
                    <Image
                        height={24}
                        width={24}
                        src={Images.iconXCircle.data}
                        alt={Images.iconXCircle.atl}
                    />
                </Popconfirm>
            }
            title={t('text.addAttribute')}
            afterClose={onAfterClose}
            destroyOnClose
            onOk={onOk}
            footer={
                <CustomButton
                    key={"save"}
                    usingFor={"geodetic"}
                    className={'icon-vertical-base'}
                    onClick={onOk}
                    icon={
                        <Image
                            height={24}
                            width={24}
                            src={Images.iconSave.data}
                            alt={Images.iconSave.atl}
                        />
                    }

                >
                    {t("button.save")}
                </CustomButton>
            }
        >
            <Form form={form}>
                {formAddAttribute()}
            </Form>
        </Modal>
    )
}