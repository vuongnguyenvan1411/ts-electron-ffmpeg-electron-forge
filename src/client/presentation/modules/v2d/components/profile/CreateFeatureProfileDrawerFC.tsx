import {Button, Collapse, Drawer, Form, FormInstance, Input, Popover, Radio, Space, Switch} from "antd";
import {Viewer} from "../../viewer/Viewer";
import {EDrawingDashed, EDrawingStyle, EDrawingWeight, EDrawType, EMenuMapItem, EPlaceBlock, EProfileType} from "../../const/Defines";
import {TFormDrawerValues} from "../../viewer/clim/CLTypeTool";
import React, {useEffect, useState} from "react";
import {useTranslation} from "react-i18next";
import {ColorResult, RGBColor, SketchPicker} from "react-color";
import {Color} from "../../../../../const/Color";
import TextArea from "antd/es/input/TextArea";
import {CustomButton} from "../../../../components/CustomButton";
import {CustomTypography} from "../../../../components/CustomTypography";
import styles from "../../styles/MapView.module.scss";
import Image from "next/image";
import {Images} from "../../../../../const/Images";
import {CaretRightOutlined} from "@ant-design/icons";
import {CLProfile} from "../../viewer/clim/CLProfileTool";
import {AttributeAddFeatureModalFC} from "./AttributeAddFeatureModalFC";
import {Feature} from "ol";
import {removeAction} from "../tool/MapAction";
import {merge} from "lodash";
import {DrawProfileFeature} from "./DrawProfileFeature";
import {DrawAnnotationFeature} from "./DrawAnnotationFeature";
import EventEmitter from "eventemitter3";
import {ViewerEventName} from "../../const/Event";
import {ModifyFeatureDraw, ModifyFeatureProfile} from "./ModifyFeature";
import {Geometry} from "ol/geom";
import {OptionsMenu} from "../../const/OptionsMenu";

export const CreateFeatureProfileDrawerFC = (props: {
    viewer: Viewer,
    onClose: () => void,
    type: EDrawType,
    initValues?: TFormDrawerValues,
    headerHeightRef: number,
    object: CLProfile,
    event: EventEmitter,
}) => {
    const {t} = useTranslation();
    const [form] = Form.useForm();
    const checkEdit = {
        isEdit: !!props.initValues,
        typeEdit: props.initValues?.modifyFeature?.type
    };

    const [isAddAttributeVisible, setIsAddAttributeVisible] = useState<{
        visible: boolean,
        feature?: Feature<any>
    }>({
        visible: false
    });

    const defaultValues: TFormDrawerValues = {
        note: '',
        color: {
            fillColor: {
                rgb: {r: 0, g: 0, b: 0, a: 0}
            } as ColorResult,
            strokeColor: {
                rgb: {r: 0, g: 0, b: 0, a: 1}
            } as ColorResult,
        },
        drawingStyle: EDrawingStyle.Default,
        position: {},
        stroke: {
            dashed: EDrawingDashed.Default,
            width: 2,
        },
        textStyle: {
            weight: EDrawingWeight.Default,
            size: 12,
            arrow: false
        },
        placeBlock: EPlaceBlock.None,
    };

    const [valuesDraw, setValuesDraw] = useState<TFormDrawerValues>(props.initValues ?? defaultValues);
    const [isValuesChanged, setIsValuesChanged] = useState(false);
    const initValues = props.initValues ?? defaultValues;

    useEffect(() => {
        console.log('%cMount Screen: CreateFeatureProfileDrawerFC', Color.ConsoleInfo);
        form.setFieldsValue(initValues);

        return () => {
            console.log('%cUnmount Screen: CreateFeatureProfileDrawerFC', Color.ConsoleInfo);
        };

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (checkEdit.isEdit && checkEdit.typeEdit && props.object.feature) {

            props.event.emit(ViewerEventName.ProfileModified, {
                object: props.object,
                editFeature: valuesDraw,
            });

            if (checkEdit.typeEdit === EMenuMapItem.ProfileLayer) {
                switch (props.type) {
                    case EDrawType.LineString:
                    case EDrawType.Circle:
                    case EDrawType.Polygon:
                        ModifyFeatureProfile({
                            viewer: props.viewer,
                            feature: props.object.feature,
                            editValues: valuesDraw,
                            typeProfile: EProfileType.Draw
                        })

                        break;

                    case EDrawType.Point:
                        ModifyFeatureProfile({
                            viewer: props.viewer,
                            feature: props.object.feature,
                            editValues: valuesDraw,
                            typeProfile: EProfileType.Annotation
                        })
                        break;
                }
            } else {
                ModifyFeatureDraw({
                    viewer: props.viewer,
                    feature: props.object.feature,
                    editValues: valuesDraw,
                })
            }
        } else {
            switch (props.type) {
                case EDrawType.LineString:
                case EDrawType.Circle:
                case EDrawType.Polygon:

                    DrawProfileFeature({
                        values: valuesDraw,
                        t: t,
                        viewer: props.viewer,
                        typeDraw: props.type,
                        object: props.object,
                        onOpenAddAttribute: onOpenAddAttribute
                    })
                    break;

                case EDrawType.Point:
                    DrawAnnotationFeature({
                        t: t,
                        viewer: props.viewer,
                        values: valuesDraw,
                        object: props.object,
                    })

                    break;
            }
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isValuesChanged, props.object])

    const onValuesChange = (changedValues: any) => {
        const newValues = merge(valuesDraw, changedValues);

        setValuesDraw(newValues);
        setIsValuesChanged(!isValuesChanged)
    }

    //add attribute feature
    const onOpenAddAttribute = (feature: Feature<Geometry>) => {
        if (props.object.layer) {
            const objectProperties = props.object.layer.get('properties');

            if (objectProperties && objectProperties.length > 0) {

                setIsAddAttributeVisible({
                    visible: true,
                    feature: feature,
                });
            }
        }
    }

    const onCloseRemoveAddAttribute = () => {
        if (isAddAttributeVisible.feature) {
            const cancelFeature = {
                ...props.object,
                feature: isAddAttributeVisible.feature,
                id: isAddAttributeVisible.feature.get('id')
            };

            props.viewer.profileTool.removeFeature(cancelFeature as CLProfile);
            removeAction(props.viewer)
        }

        setIsAddAttributeVisible({
            visible: false
        });
    }

    const onCloseAddAttribute = () => {
        setIsAddAttributeVisible({
            visible: false
        });
    }

    const onCloseDrawer = () => {
        props.onClose();

        removeAction(props.viewer)
    }

    const onFormAddAttributeFinish = (values: any) => {
        if (!isAddAttributeVisible.feature) {
            return
        }
        isAddAttributeVisible.feature.set('properties', values);
    }

    return (
        <>
            <Drawer
                className={styles.MenuRight}
                width={'15rem'}
                style={{
                    height: `calc(100vh - ${props.headerHeightRef}px)`,
                    top: `${props.headerHeightRef}px`
                }}
                visible
                onClose={onCloseDrawer}
                closable
                mask={false}
                bodyStyle={{
                    padding: 0
                }}
                placement={'right'}
                forceRender
                closeIcon={<Image
                    height={16}
                    width={16}
                    src={Images.iconXCircle.data}
                    alt={Images.iconXCircle.atl}
                />}
            >
                <Form
                    requiredMark
                    form={form}
                    labelAlign="left"
                    colon={false}
                    onValuesChange={onValuesChange}
                    layout="horizontal"
                    size={'small'}
                >
                    <FormDrawFC
                        type={props.type}
                        initValues={initValues}
                        checkEdit={checkEdit}
                        form={form}
                    />
                </Form>
            </Drawer>
            {
                isAddAttributeVisible.visible && isAddAttributeVisible.feature
                    ? <AttributeAddFeatureModalFC
                        viewer={props.viewer}
                        onCloseRemove={onCloseRemoveAddAttribute}
                        onClose={onCloseAddAttribute}
                        vectorLayer={props.object.layer}
                        feature={isAddAttributeVisible.feature}
                        onFormAddAttributeFinish={onFormAddAttributeFinish}
                    />
                    : null
            }
        </>
    )
}

const FormDrawFC = (props: {
    type: EDrawType,
    initValues: TFormDrawerValues,
    checkEdit: {
        isEdit: boolean,
        typeEdit?: EMenuMapItem,
    },
    form: FormInstance<any>
}) => {

    const {t} = useTranslation();
    const [fillColor, setFillColor] = useState<RGBColor | undefined>(props.initValues.color?.fillColor?.rgb);
    const [strokeColor, setStrokeColor] = useState<RGBColor | undefined>(props.initValues.color?.strokeColor?.rgb);
    const [placeValue, setPlaceValue] = useState<EPlaceBlock>();
    const [transformValue, setTransformValue] = useState<string>('');

    const isEdit = props.checkEdit.isEdit;
    const typeEdit = props.checkEdit.typeEdit;

    const RGBToHex = (rgb?: RGBColor) => {
        if (rgb) {
            let rS = rgb.r.toString(16);
            let gS = rgb.g.toString(16);
            let bS = rgb.b.toString(16);

            if (rS.length == 1)
                rS = "0" + rS;
            if (gS.length == 1)
                gS = "0" + gS;
            if (bS.length == 1)
                bS = "0" + bS;

            return "#" + rS + gS + bS;
        } else {
            return undefined
        }
    }

    const drawActiveKeys = isEdit && typeEdit === EMenuMapItem.ProfileLayer
        ? ["note", "color", "stroke"]
        : ["note", "color", "drawingStyle", "stroke", "transform"];
    const annoActiveKeys = ["note", "color", "textStyle"];
    const measureActiveKeys = ["note"];
    const collapsiblePoint = props.type === EDrawType.Point ? "disabled" : "header";
    const collapsiblePolygon = isEdit && typeEdit === EMenuMapItem.ProfileLayer
        ? "disabled" : (props.type !== EDrawType.Polygon ? "disabled" : "header");

    useEffect(() => {
        if (placeValue === EPlaceBlock.Square && transformValue !== '') {
            props.form.setFieldsValue({
                ['transform']: {
                    height: transformValue
                }
            })
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [placeValue, transformValue])

    return (
        <Space>
            <Collapse
                ghost
                defaultActiveKey={
                    typeEdit === EMenuMapItem.MeasureLayer && isEdit ? measureActiveKeys :
                        (props.type !== EDrawType.Point ? drawActiveKeys : annoActiveKeys)
                }
                className={styles.CollapseMenuRight}
                expandIcon={
                    ({isActive}) =>
                        <CaretRightOutlined
                            style={{
                                color: '#DE9F59',
                                width: 4,
                                height: 6,
                            }}
                            rotate={isActive ? 90 : 0}
                        />}
            >
                <Collapse.Panel
                    header={props.type === EDrawType.Point ? t('text.inputNote') : t('text.layerProfileName')}
                    key="note"
                >
                    <Form.Item
                        className={'form-item form-item-mb-0 form-item-pb-3'}
                        name={'note'}
                        initialValue={props.initValues.note}
                    >
                        <TextArea
                            placeholder={t('text.inputNote')}
                            autoSize
                            allowClear
                            style={{
                                width: '11.25rem',
                            }}
                        />
                    </Form.Item>
                </Collapse.Panel>
                <Collapse.Panel
                    header={t('text.textStyle')}
                    key='textStyle'
                    collapsible={props.type === EDrawType.Point ? "header" : "disabled"}
                >
                    <Form.Item
                        className={'form-item form-item-mb-0'}
                        hidden={props.type !== EDrawType.Point}
                    >
                        <Input.Group
                            compact
                            className={styles.InputAddOn}
                        >
                            <Form.Item
                                className={'form-item form-item-mb-3'}
                                name={['textStyle', 'size']}
                                initialValue={props.initValues.textStyle?.size}
                                label={
                                    <CustomTypography textStyle={"text-14-18"}>
                                        {t('text.chooseSize')}
                                    </CustomTypography>
                                }
                            >
                                <Input
                                    size={"small"}
                                    addonAfter='px'
                                    type={'number'}
                                    max={14}
                                    min={1}
                                />
                            </Form.Item>
                            <Form.Item
                                name={['textStyle', 'weight']}
                                initialValue={props.initValues.textStyle?.weight}
                                className={'form-item form-item-mb-0'}
                            >
                                <Radio.Group className={'radio-main radio-v2d-main radio-mb-3'}>
                                    <Radio value={EDrawingWeight.Default}>
                                        <CustomTypography textStyle={"text-14-18"}>
                                            {t('text.textDefault')}
                                        </CustomTypography>
                                    </Radio>
                                    <Radio value={EDrawingWeight.Bold}>
                                        <CustomTypography textStyle={"text-14-18"}>
                                            {t('text.bold')}
                                        </CustomTypography>
                                    </Radio>
                                </Radio.Group>
                            </Form.Item>
                            <Form.Item
                                className={'form-item form-item-mb-0'}
                                name={['textStyle', 'arrow']}
                                initialValue={props.initValues.textStyle?.arrow}
                                label={
                                    <CustomTypography textStyle={"text-14-18"}>
                                        {t('text.arrowPointing')}
                                    </CustomTypography>
                                }
                                valuePropName="checked"
                            >
                                <Switch size={"small"} disabled={isEdit}/>
                            </Form.Item>
                        </Input.Group>
                    </Form.Item>
                </Collapse.Panel>
                <Collapse.Panel
                    header={t('text.chooseColor')}
                    key="color"
                    collapsible={(isEdit && typeEdit === EMenuMapItem.MeasureLayer) ? "disabled" : "header"}
                >
                    <Form.Item className={'form-item form-item-mb-0'}>
                        <Input.Group compact>
                            <Popover
                                content={
                                    <Form.Item
                                        name={['color', 'fillColor']}
                                        initialValue={props.initValues.color?.fillColor}
                                    >
                                        <SketchPicker
                                            color={fillColor as any}
                                            onChangeComplete={color => setFillColor(color.rgb)}
                                        />
                                    </Form.Item>
                                }
                                trigger="click"
                                overlayClassName={styles.PopoverColor}
                            >
                                <div style={{display: "block", paddingBottom: '0.75rem'}}>
                                    <CustomTypography textStyle={"text-14-18"}>
                                        {t('text.fillColor')}
                                    </CustomTypography>
                                    <Button
                                        style={{
                                            backgroundColor: RGBToHex(fillColor),
                                            opacity: fillColor && fillColor.a !== 0 ? fillColor.a : 0.1,
                                        }}
                                        className={styles.ColorPicker}
                                    >
                                        <div
                                            style={{
                                                backgroundColor: RGBToHex(fillColor),
                                                opacity: fillColor ? fillColor.a : undefined
                                            }}>
                                        </div>
                                    </Button>
                                </div>
                            </Popover>
                            <Popover
                                content={
                                    <Form.Item
                                        name={['color', 'strokeColor']}
                                        initialValue={props.initValues.color?.strokeColor}
                                    >
                                        <SketchPicker
                                            color={strokeColor as any}
                                            onChangeComplete={color => setStrokeColor(color.rgb)}
                                        />
                                    </Form.Item>
                                }
                                trigger="click"
                                overlayClassName={styles.PopoverColor}
                            >
                                <div style={{display: "block"}}>
                                    <CustomTypography textStyle={"text-14-18"}>
                                        {t('text.strokeColor')}
                                    </CustomTypography>
                                    <Button
                                        className={styles.ColorPicker}
                                        style={{
                                            border: `2px solid ${RGBToHex(strokeColor)}`
                                        }}
                                    >
                                <span
                                    className={styles.LineSlash}
                                >
                                    &#47;
                                </span>
                                    </Button>
                                </div>
                            </Popover>
                        </Input.Group>
                    </Form.Item>
                </Collapse.Panel>
                <Collapse.Panel
                    header={t('text.drawType')}
                    key="drawingStyle"
                    collapsible={(isEdit || props.type !== EDrawType.LineString) ? "disabled" : collapsiblePoint}
                >
                    <Form.Item
                        className={'form-item form-item-mb-0'}
                        name={'drawingStyle'}
                        initialValue={props.initValues.drawingStyle}
                        hidden={props.type === EDrawType.Point}
                    >
                        <Radio.Group>
                            <Space direction={"vertical"} size={12}>
                                <Radio value={EDrawingStyle.Default}>
                                    <CustomTypography textStyle={"text-14-18"}>
                                        {t('text.default')}
                                    </CustomTypography>
                                </Radio>
                                <Radio value={EDrawingStyle.FreeHand}>
                                    <CustomTypography textStyle={"text-14-18"}>
                                        {t('text.freehand')}
                                    </CustomTypography>
                                </Radio>
                                <Radio
                                    value={EDrawingStyle.Curve}
                                    disabled={props.type !== EDrawType.LineString}
                                >
                                    <CustomTypography textStyle={"text-14-18"}>
                                        {t('text.curveDraw')}
                                    </CustomTypography>
                                </Radio>
                                <Radio
                                    value={EDrawingStyle.Arcs}
                                    disabled={props.type !== EDrawType.LineString}
                                >
                                    <CustomTypography textStyle={"text-14-18"}>
                                        {t('text.arcDraw')}
                                    </CustomTypography>
                                </Radio>
                            </Space>
                        </Radio.Group>
                    </Form.Item>
                </Collapse.Panel>
                <Collapse.Panel
                    header={'Stroke'}
                    key='stroke'
                    collapsible={(isEdit && typeEdit === EMenuMapItem.MeasureLayer) ? "disabled" : collapsiblePoint}
                >
                    <Form.Item
                        className={'form-item form-item-mb-0'}
                        hidden={props.type === EDrawType.Point}
                    >
                        <Input.Group
                            compact
                            className={styles.InputAddOn}
                        >
                            <Space direction={"vertical"} size={0}>
                                <Form.Item
                                    className={'form-item form-item-mb-0'}
                                    name={['stroke', 'width']}
                                    initialValue={props.initValues.stroke?.width}
                                    label={
                                        <>
                                            <Image
                                                src={Images.iconStrokeWeight.data}
                                                alt={Images.iconStrokeWeight.atl}
                                                width={16}
                                                height={16}
                                            />
                                            <CustomTypography
                                                className={'ml-2'}
                                                type={"text"}
                                                textStyle={"text-14-18"}
                                            >
                                                {t('text.strokeWeight')}
                                            </CustomTypography>
                                        </>
                                    }
                                >
                                    <Input
                                        className={'input-main input-v2d input-h-18'}
                                        addonAfter="px"
                                        size={"small"}
                                        type={'number'}
                                        max={10}
                                        min={1}
                                    />
                                </Form.Item>
                                <Form.Item
                                    className={'form-item form-item-mb-0'}
                                    name={['stroke', 'dashed']}
                                    initialValue={props.initValues.stroke?.dashed}
                                >
                                    <Radio.Group>
                                        <Space direction={"vertical"} size={0}>
                                            <Radio value={EDrawingDashed.Default}>
                                                <CustomTypography
                                                    type={"text"}
                                                    textStyle={"text-14-18"}
                                                >
                                                    <Image
                                                        src={Images.iconStrokeDefault.data}
                                                        alt={Images.iconStrokeDefault.atl}
                                                        width={16}
                                                        height={16}
                                                    />
                                                    {t('text.solidLine')}
                                                </CustomTypography>
                                            </Radio>
                                            <Radio value={EDrawingDashed.Dashed}>
                                                <CustomTypography
                                                    type={"text"}
                                                    textStyle={"text-14-18"}
                                                >
                                                    <Image
                                                        src={Images.iconStrokeDashed.data}
                                                        alt={Images.iconStrokeDashed.atl}
                                                        width={16}
                                                        height={16}
                                                    />
                                                    {t('text.dashed')}
                                                </CustomTypography>
                                            </Radio>
                                        </Space>
                                    </Radio.Group>
                                </Form.Item>
                            </Space>
                        </Input.Group>
                    </Form.Item>
                </Collapse.Panel>
                <Collapse.Panel
                    header={'Transform'}
                    key='transform'
                    collapsible={
                        (placeValue === EPlaceBlock.Square || placeValue === EPlaceBlock.Triangle) ? "header" : "disabled"
                    }
                >
                    <Form.Item
                        className={'form-item form-item-mb-0'}
                        hidden={props.type === EDrawType.Point || props.type === EDrawType.Circle}
                    >
                        <Input.Group
                            compact
                            className={styles.InputTransform}
                        >
                            <Form.Item
                                className={'form-item form-item-mb-0'}
                                name={['transform', 'width']}
                                initialValue={props.initValues.transform?.width}
                                label={
                                    placeValue === EPlaceBlock.Triangle
                                        ? <Image
                                            src={Images.iconTriangleEdge.data}
                                            alt={Images.iconTriangleEdge.atl}
                                            width={16}
                                            height={16}
                                        />
                                        : <CustomTypography
                                            textStyle={"text-14-18"}
                                            isStrong
                                        >
                                            W
                                        </CustomTypography>
                                }
                            >
                                <Input
                                    type={'number'}
                                    min={0}
                                    size={"small"}
                                    addonAfter="km"
                                    onChange={(evt) => setTransformValue(evt.target.value)}
                                />
                            </Form.Item>
                            <Form.Item
                                className={'form-item form-item-mb-0'}
                                name={['transform', 'height']}
                                initialValue={props.initValues.transform?.height}
                                label={
                                    <CustomTypography
                                        textStyle={"text-14-18"}
                                        isStrong
                                    >
                                        H
                                    </CustomTypography>
                                }
                                hidden={placeValue !== EPlaceBlock.Square}
                            >
                                <Input
                                    type={'number'}
                                    min={0}
                                    size={"small"}
                                    addonAfter="km"
                                />
                            </Form.Item>
                            <Form.Item
                                className={'form-item form-item-mb-0'}
                                name={['transform', 'angle']}
                                initialValue={props.initValues.transform?.angle}
                                label={
                                    props.type === EDrawType.LineString
                                        ? <Image
                                            src={Images.iconAngle.data}
                                            alt={Images.iconAngle.atl}
                                            width={16}
                                            height={16}
                                        />
                                        : <Image
                                            src={Images.iconRefreshBL.data}
                                            alt={Images.iconRefreshBL.atl}
                                            width={16}
                                            height={16}
                                        />
                                }
                            >
                                <Input
                                    type={'number'}
                                    max={360}
                                    min={-360}
                                    className={styles.Input}
                                    size={"small"}
                                    addonAfter="deg"
                                />
                            </Form.Item>
                        </Input.Group>
                    </Form.Item>
                </Collapse.Panel>
                <Collapse.Panel
                    header={'Place Block'}
                    key='placeBlock'
                    collapsible={isEdit ? "disabled" : collapsiblePolygon}
                >
                    <Form.Item
                        className={'form-item form-item-mb-0'}
                        hidden={isEdit}
                        name={'placeBlock'}
                        initialValue={props.initValues.placeBlock}
                    >
                        <Radio.Group
                            className={styles.RadioGroup}
                            options={OptionsMenu.placeBlockOptions}
                            optionType={"button"}
                            disabled={props.type !== EDrawType.Polygon}
                            onChange={(evt) => setPlaceValue(evt.target.value)}
                        />
                    </Form.Item>
                </Collapse.Panel>
            </Collapse>
        </Space>

    )
}
