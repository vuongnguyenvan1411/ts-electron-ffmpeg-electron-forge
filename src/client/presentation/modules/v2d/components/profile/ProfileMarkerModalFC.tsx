import {Button, Divider, Form, FormInstance, Input, Modal, Radio, Tabs, Tooltip} from "antd";
import {useTranslation} from "react-i18next";
import React, {useEffect, useState} from "react";
import {Editor} from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import {MinusOutlined} from "@ant-design/icons";
import Plyr from "plyr";
import sms from "../../styles/MapView.module.scss";
import {Color} from "../../../../../const/Color";
import {Viewer} from "../../viewer/Viewer";
import scm from "../../../../../styles/module/Common.module.scss";
import {TFormMarkerValues} from "../../viewer/clim/CLTypeTool";
import PlyrNormalFC from "../../../../widgets/video/PlyrNormalFc";
import {DrawMarkerFeature} from "./DrawMarkerFeature";
import {CLProfile} from "../../viewer/clim/CLProfileTool";
import {RawDraftContentState} from "draft-js";
import {ViewerEventName} from "../../const/Event";
import EventEmitter from "eventemitter3";
import {Images} from "../../../../../const/Images";
import {CustomButton} from "../../../../components/CustomButton";
import Image from "next/image";

interface EditInfoMarkerInputProps {
    form?: FormInstance;
    initValues?: TFormMarkerValues;
}

export const ProfileMarkerModalFC = (props: {
    viewer: Viewer,
    onClose: () => void,
    selectedInfo: CLProfile,
    initValues?: TFormMarkerValues,
    event: EventEmitter,
}) => {
    const {t} = useTranslation();
    const [form] = Form.useForm();

    useEffect(() => {
        console.log('%cMount Screen: MapFormProfileMarkerModalFC', Color.ConsoleInfo);

        return () => {
            console.log('%cUnmount Screen: MapFormProfileMarkerModalFC', Color.ConsoleInfo);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onFormFinish = (values: TFormMarkerValues) => {
        if (props.initValues) {
            props.event.emit(ViewerEventName.ProfileModified, {
                object: props.selectedInfo,
                editFeature: values,
            })
        }

        DrawMarkerFeature({
            viewer: props.viewer,
            t: t,
            vectorLayer: props.selectedInfo.layer,
            values: values,
        })
    }

    return (
        <Modal
            className={'modal-main ant-modal-v2d large p-body-1'}
            visible
            title={t('button.info')}
            centered
            closable
            closeIcon={<Image
                height={24}
                width={24}
                src={Images.iconXCircle.data}
                alt={Images.iconXCircle.atl}
            />}
            onCancel={props.onClose}
            footer={
                <CustomButton
                    size={'small'}
                    usingFor={'geodetic'}
                    key={"save"}
                    onClick={() => {
                        form
                            .validateFields()
                            .then((values) => {
                                if (props.initValues && props.initValues.id) {
                                    values = {
                                        ...values,
                                        id: props.initValues.id
                                    }
                                }

                                if (values.longitude && values.latitude) {
                                    values = {
                                        ...values,
                                        coordinate: [values.longitude, values.latitude]
                                    }
                                }

                                onFormFinish(values);
                            })
                            .then(props.onClose)
                            .catch((errors) => {
                                console.log('errors', errors)
                            });
                    }}
                >
                    {t("button.ok")}
                </CustomButton>
            }
        >
            <Tabs
                className={'ant-tabs-v2d tabs-main'}
                defaultActiveKey={'info'}
                tabBarStyle={{
                    paddingLeft: "0.9rem"
                }}
            >
                <Tabs.TabPane tab={t('text.info')} key="info">
                    <FormInfoInputFC
                        form={form}
                        initValues={props.initValues}
                    />
                </Tabs.TabPane>
                <Tabs.TabPane tab='Video' key="video">
                    <FormVideoInputFC
                        form={form}
                        initValues={props.initValues}
                    />
                </Tabs.TabPane>
                <Tabs.TabPane tab={t('tab.timelapseImage')} key="image">
                    <FormImageInputFC
                        form={form}
                        initValues={props.initValues}
                    />
                </Tabs.TabPane>
                <Tabs.TabPane tab={t('text.inputUrl')} key="link">
                    <FormLinkInputFC
                        form={form}
                        initValues={props.initValues}
                    />
                </Tabs.TabPane>
            </Tabs>
        </Modal>
    )
}

const FormInfoInputFC: React.FC<EditInfoMarkerInputProps> = ({form, initValues}) => {
    const {t} = useTranslation();

    const [editorContentState, setEditorContentState] = useState<RawDraftContentState | undefined>(initValues?.description);

    const onContentStateChange = (evt: RawDraftContentState) => setEditorContentState(evt);

    useEffect(() => {
        console.log('%cMount Screen: FormInfoInputFC', Color.ConsoleInfo);

        return () => {
            console.log('%cUnmount Screen: FormInfoInputFC', Color.ConsoleInfo);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Form
            className={scm.ModalForm}
            form={form}
            labelCol={{span: 4}}
            labelAlign={"left"}
        >
            <Form.Item
                label={t('text.markerTitle')}
                name={'title'}
                initialValue={initValues?.title}
                rules={[
                    {required: true, message: t('text.pleaseInputValueNote')},
                    {type: 'string', max: 100, message: t('text.atMost100Characters')},
                ]}
            >
                <Input
                    className={'input-main input-v2d input-h-40 input-bg-white input-radius-4'}
                    placeholder={t('text.markerTitle')}
                    type={'text'}
                />
            </Form.Item>
            <Form.Item
                label={t('text.description')}
                name={'description'}
                valuePropName={'text'}
            >
                <Editor
                    defaultContentState={editorContentState}
                    onContentStateChange={onContentStateChange}
                    toolbarStyle={{
                        marginBottom: 0,
                    }}
                    editorStyle={{
                        borderTopWidth: 0,
                        padding: "0 1rem"
                    }}
                    editorClassName={sms.EditorWrapper}
                />
            </Form.Item>
            {
                initValues
                    ? <>
                        <Form.Item
                            className={'mt-3 mr-4 ml-1 '}
                            label={t('text.longitude')}
                            name={'longitude'}
                            initialValue={initValues.coordinate ? initValues.coordinate[0] : null}
                            rules={[
                                {required: true, message: t('validation.emptyData')},
                                () => ({
                                    validator(_, value) {
                                        if ((value <= 180) && (value >= -180)) {
                                            return Promise.resolve();
                                        } else {
                                            return Promise.reject(new Error(t('validation.latitudeRange')));
                                        }
                                    },
                                }),
                            ]}
                        >
                            <Input
                                className={'input-main input-v2d input-h-40 input-bg-white input-radius-4'}
                                addonBefore={'Longitude'}
                                placeholder={'000.00000'}
                                type={'number'}
                            />
                        </Form.Item>
                        <Form.Item
                            className={'mt-3 mr-4 ml-1'}
                            label={t('text.latitude')}
                            name={'latitude'}
                            initialValue={initValues.coordinate ? initValues.coordinate[1] : null}
                            rules={[
                                {required: true, message: t('validation.emptyData')},
                                () => ({
                                    validator(_, value) {
                                        if ((value <= 90) && (value >= -90)) {
                                            return Promise.resolve();
                                        } else {
                                            return Promise.reject(new Error(t('validation.latitudeRange')));
                                        }
                                    },
                                }),
                            ]}
                        >
                            <Input
                                className={'input-main input-v2d input-h-40 input-bg-white input-radius-4'}
                                addonBefore={'Latitude'}
                                placeholder={'00.00000'}
                                type={'number'}
                            />
                        </Form.Item>
                    </>
                    : null
            }
        </Form>
    )
}

const FormVideoInputFC: React.FC<EditInfoMarkerInputProps> = ({form, initValues}) => {
    const {t} = useTranslation();

    const [previewVideo, setPreviewVideo] = useState<{
        visible: boolean,
        source?: Plyr.SourceInfo,
    }>({
        visible: false,
    });

    useEffect(() => {
        console.log('%cMount Screen: FormVideoInputFC', Color.ConsoleInfo);

        return () => {
            console.log('%cUnmount Screen: FormVideoInputFC', Color.ConsoleInfo);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Form
            className={scm.ModalForm}
            form={form}
            initialValues={{
                videos: initValues?.videos?.map(_ => {
                        return {
                            link: _.link,
                            description: _.description,
                        }
                    }
                ) ?? []
            }}
            labelCol={{span: 4}}
            labelAlign={"left"}
        >
            <Form.List
                name={'videos'}
                rules={[
                    {
                        validator: async (_, video) => {
                            if (video === undefined) {
                                return Promise.resolve();
                            }
                            if (video && video.length > 10) {
                                return Promise.reject(new Error(`${t('text.atMost10Url')}`));
                            }
                            return Promise.resolve();
                        },
                    },
                ]}
            >
                {
                    (fields, {add, remove}, {errors}) =>
                        <>
                            {
                                fields.map(field => (
                                    <Form.Item
                                        className={'p-2'}
                                        labelAlign={'left'}
                                        key={field.key + 'video'}
                                    >
                                        <div className={"flex"}>
                                            <div className={"w-full"}>
                                                <Form.Item
                                                    {...field}
                                                    key={'videoDescription'}
                                                    validateTrigger={['onChange', 'onBlur']}
                                                    label={t('text.description')}
                                                    name={[field.name, 'description']}
                                                    rules={[
                                                        {type: 'string', max: 100, message: t('text.atMost100Characters')},
                                                    ]}
                                                    labelCol={{span: 4}}
                                                    wrapperCol={{span: 18}}
                                                >
                                                    <Input
                                                        className={'input-main input-v2d input-h-40 input-bg-white input-radius-4'}
                                                        placeholder={t('text.description')}
                                                        type={'text'}
                                                    />
                                                </Form.Item>
                                                <Form.Item
                                                    {...field}
                                                    key={'videoLink'}
                                                    label={t('text.inputUrl')}
                                                    validateTrigger={['onChange', 'onBlur']}
                                                    name={[field.name, 'link']}
                                                    // fieldKey={[field.key, "videoLink"]}
                                                    rules={[
                                                        {required: true, message: t('text.urlIsNotValidUrl')},
                                                        {type: 'string', min: 6, message: t('text.urlAtLess6Characters')},
                                                        {type: 'url', warningOnly: true, message: t('text.urlIsNotValidUrl')},
                                                        () => ({
                                                            validator(_, value) {
                                                                if (
                                                                    value.match(/^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=|\?v=)([^#&?]*).*/) !== null
                                                                    && value.match(/^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=|\?v=)([^#&?]*).*/) !== undefined
                                                                ) {

                                                                    return Promise.resolve();
                                                                } else if (
                                                                    value.match(/(?:https?:\/\/(?:www\.)?)?vimeo.com\/(?:channels\/|groups\/([^]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/) !== null
                                                                    && value.match(/(?:https?:\/\/(?:www\.)?)?vimeo.com\/(?:channels\/|groups\/([^]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/) !== undefined
                                                                ) {
                                                                    return Promise.resolve();
                                                                } else {
                                                                    return Promise.reject(new Error(t('text.urlIsNotValidUrl')));
                                                                }
                                                            },
                                                        }),
                                                    ]}
                                                    labelCol={{span: 4}}
                                                    wrapperCol={{span: 18}}
                                                >
                                                    <Input
                                                        className={'input-main input-v2d input-h-40 input-bg-white input-radius-4'}
                                                        placeholder={t('text.inputUrl')}
                                                        type={'text'}
                                                    />
                                                </Form.Item>
                                            </div>
                                        </div>
                                        {
                                            fields.length > 0
                                                ? <Button
                                                    type={"text"}
                                                    onClick={() => remove(field.name)}
                                                >
                                                    <MinusOutlined
                                                        className="dynamic-delete-button"
                                                    />
                                                    {t('button.delete')}
                                                </Button>
                                                : null
                                        }
                                    </Form.Item>
                                ))
                            }
                            <Divider className={'m-0'}>
                                <Tooltip title={t('button.add')}>
                                    <CustomButton
                                        usingFor={'geodetic'}
                                        key={"save"}
                                        onClick={() => add()}
                                        type={"text"}
                                    >
                                        <Image
                                            src={Images.iconPlusCircle.data}
                                            alt={Images.iconPlusCircle.atl}
                                            width={32}
                                            height={32}
                                        />
                                    </CustomButton>
                                </Tooltip>
                            </Divider>
                            <Form.ErrorList errors={errors}/>
                        </>
                }
            </Form.List>
            <Modal
                visible={previewVideo.visible}
                onCancel={() => setPreviewVideo({
                    ...previewVideo,
                    visible: false,
                })}
                onOk={() => setPreviewVideo({
                    ...previewVideo,
                    visible: false,
                })}
                destroyOnClose={true}
            >
                <PlyrNormalFC
                    source={previewVideo.source}
                />
            </Modal>
        </Form>
    )
}

const FormImageInputFC: React.FC<EditInfoMarkerInputProps> = ({form, initValues}) => {
    const {t} = useTranslation();

    useEffect(() => {
        console.log('%cMount Screen: FormImageInputFC', Color.ConsoleInfo);

        return () => {
            console.log('%cUnmount Screen: FormImageInputFC', Color.ConsoleInfo);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Form
            className={`${scm.ModalForm} mt-2`}
            labelCol={{span: 4}}
            labelAlign={"left"}
            form={form}
            initialValues={{
                images: initValues?.images?.map(item => {
                        return {
                            link: item.link,
                            description: item.description,
                        }
                    }
                ) ?? []
            }}
        >
            <Form.List
                name={'images'}
                rules={[
                    {
                        validator: async (_, imageLinkMore) => {
                            if (imageLinkMore === undefined) {
                                return Promise.resolve();
                            }

                            if (!imageLinkMore && imageLinkMore.length > 10) {
                                return Promise.reject(new Error(`${t('text.atMost10Url')}`));
                            }

                            return Promise.resolve();
                        },
                    },
                ]}
            >
                {(fields, {add, remove}, {errors}) =>
                    <>
                        {
                            fields.map(field => (
                                <Form.Item
                                    className={'p-2'}
                                    labelAlign={'left'}
                                    key={field.key + 'image'}
                                >
                                    <div className={"flex"}>
                                        <div className={"w-full"}>
                                            <Form.Item
                                                validateTrigger={['onChange', 'onBlur']}
                                                label={t('text.description')}
                                                name={[field.name, 'description']}
                                                // fieldKey={[field.key, "imageDescription"]}
                                                rules={[
                                                    {type: 'string', max: 100, message: t('text.atMost100Characters')},
                                                ]}
                                                labelCol={{span: 4}}
                                                wrapperCol={{span: 18}}
                                            >
                                                <Input
                                                    className={'input-main input-v2d input-h-40 input-bg-white input-radius-4'}
                                                    placeholder={t('text.description')}
                                                    type={'text'}
                                                />
                                            </Form.Item>
                                            <Form.Item
                                                label={t('text.inputUrl')}
                                                validateTrigger={['onChange', 'onBlur']}
                                                name={[field.name, 'link']}
                                                // fieldKey={[field.key, "imageLink"]}
                                                rules={[
                                                    {type: 'string', min: 6, message: t('text.urlAtLess6Characters')},
                                                    {type: 'url', warningOnly: true, message: t('text.urlIsNotValidUrl')},
                                                    () => ({
                                                        validator(_, value) {
                                                            if (value.match(/\.(jpeg|jpg|gif|png)$/) !== null) {
                                                                return Promise.resolve();
                                                            } else {
                                                                return Promise.reject(new Error(t('text.urlIsNotImageUrl')));
                                                            }
                                                        },
                                                    }),
                                                ]}
                                                labelCol={{span: 4}}
                                                wrapperCol={{span: 18}}
                                            >
                                                <Input
                                                    className={'input-main input-v2d input-h-40 input-bg-white input-radius-4'}
                                                    placeholder={t('text.inputUrl')}
                                                    type={'text'}
                                                />
                                            </Form.Item>
                                        </div>
                                    </div>
                                    {
                                        fields.length > 0
                                            ? <Button
                                                type={"text"}
                                                onClick={() => remove(field.name)}
                                            >
                                                <MinusOutlined
                                                    className="dynamic-delete-button"
                                                />
                                                {t('button.delete')}
                                            </Button>
                                            : null
                                    }
                                </Form.Item>
                            ))
                        }
                        <Divider className={'m-0'}>
                            <Tooltip title={t('button.add')}>
                                <CustomButton
                                    usingFor={'geodetic'}
                                    key={"save"}
                                    onClick={() => add()}
                                    type={"text"}
                                >
                                    <Image
                                        src={Images.iconPlusCircle.data}
                                        alt={Images.iconPlusCircle.atl}
                                        width={32}
                                        height={32}
                                    />
                                </CustomButton>
                            </Tooltip>
                        </Divider>
                        <Form.ErrorList errors={errors}/>
                    </>
                }
            </Form.List>
        </Form>
    )
}

const FormLinkInputFC: React.FC<EditInfoMarkerInputProps> = ({form, initValues}) => {
    const {t} = useTranslation();

    useEffect(() => {
        console.log('%cMount Screen: FormLinkInputFC', Color.ConsoleInfo);

        return () => {
            console.log('%cUnmount Screen: FormLinkInputFC', Color.ConsoleInfo);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Form
            className={`${scm.ModalForm} mt-2 p-2`}
            form={form}
            labelCol={{span: 4}}
            labelAlign={"left"}
        >
            <Form.Item
                label={t('text.inputUrl')}
                name={['hyperlink', 'link']}
                rules={[
                    {type: 'url', message: t('text.urlIsNotValidUrl')},
                ]}
                initialValue={initValues?.hyperlink?.link}
            >
                <Input
                    className={'input-main input-v2d input-h-40 input-bg-white input-radius-4'}
                    placeholder={t('text.inputUrl')}
                    type={'text'}
                />
            </Form.Item>
            <Form.Item
                label={t('button.openLink')}
                name={['hyperlink', 'target']}
                initialValue={initValues?.hyperlink?.target ?? "in"}
            >
                <Radio.Group
                    className={'radio-main radio-v2d-main'}
                >
                    <Radio value="in">{t('text.openUrlHere')}</Radio>
                    <Radio value="out">{t('text.openUrlNewTab')}</Radio>
                </Radio.Group>
            </Form.Item>
        </Form>
    )
}

export const getYoutubeIdFromUrl = (url: string) => {
    const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
    const match = url.match(regExp);

    return (match && match[7].length === 11) ? match[7] : false;
}
