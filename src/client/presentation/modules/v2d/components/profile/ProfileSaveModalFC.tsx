import {Alert, Card, Checkbox, Col, Form, Input, Modal, notification, Row, Space, Table, Tabs, Typography} from "antd";
import React, {Key, useCallback, useEffect, useState} from "react";
import {useTranslation} from "react-i18next";
import {EDrawType, EMapLayer, EProfileType} from "../../const/Defines";
import {find, includes} from "lodash";
import {useParams} from "react-router-dom";
import {CHashids} from "../../../../../core/CHashids";
import {SendingStatus} from "../../../../../const/Events";
import {RouteAction} from "../../../../../const/RouteAction";
import {RouteConfig} from "../../../../../config/RouteConfig";
import {ColumnsType} from "antd/es/table";
import {PlusCircleOutlined} from "@ant-design/icons";
import {Color} from "../../../../../const/Color";
import {Geometry} from "ol/geom";
import {GeoJSON} from "ol/format";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import {Utils} from "../../../../../core/Utils";
import {ProfileModel, TConfig, TProfile, TProfileSaveV0} from "../../../../../models/service/geodetic/ProfileModel";
import {Viewer} from "../../viewer/Viewer";
import {useLocation, useNavigationType} from "react-router";
import {Map2DProfileAction} from "../../../../../recoil/service/geodetic/map_2d_profile/Map2DProfileAction";
import {MapLoadGeoJsonProfile} from "./MapLoadGeoJsonProfile";
import {CustomButton} from "../../../../components/CustomButton";
import Image from "next/image";
import {Images} from "../../../../../const/Images";
import {CustomTypography} from "../../../../components/CustomTypography";
import {GeoJSONFeatureCollection} from "ol/format/GeoJSON";
import {v4 as uuid} from "uuid";

type TFormProfileValues = {
    name: string;
    layers: string[];
}

enum EKeyTabs {
    Profile = 'profile',
    Measure = 'measure'
}

export const EditorSaveModal = React.memo((props: {
    viewer: Viewer,
    onClose: () => void,
    isSaveFeatureVisible: boolean,
    m2dId: number,
}) => {
    const {t} = useTranslation();
    const [keyTabs, setKeyTabs] = useState<EKeyTabs>(EKeyTabs.Profile)

    const tabPanel = (
        <TabsPanelCardData
            viewer={props.viewer}
            tabs={keyTabs}
            onCloseModal={props.onClose}
            m2dId={props.m2dId}
        />
    )

    const tabItems = [
        {
            tab: t('text.layerProfiles'),
            key: EKeyTabs.Profile,
        },
        {
            tab: t('text.layerMeasures'),
            key: EKeyTabs.Measure,
        },
    ]
    return (
        <Modal
            className={'modal-main ant-modal-v2d large'}
            centered
            visible={props.isSaveFeatureVisible}
            onCancel={props.onClose}
            title={t('button.saveList')}
            destroyOnClose={true}
            key={'modal-form-save-feature'}
            onOk={props.onClose}
            closeIcon={<Image
                height={18}
                width={18}
                src={Images.iconXCircle.data}
                alt={Images.iconXCircle.atl}
            />}
            footer={null}
        >
            <Tabs
                className={'tabs-main ant-tabs-v2d'}
                defaultActiveKey={EKeyTabs.Profile}
                onChange={(key: EKeyTabs) => setKeyTabs(key)}
                centered
            >
                {
                    tabItems.map(item => {
                        return (
                            <Tabs.TabPane tab={item.tab} key={item.key}>
                                {tabPanel}
                            </Tabs.TabPane>
                        )
                    })
                }
            </Tabs>
        </Modal>
    )
})

const TabsPanelCardData = (props: {
    viewer: Viewer,
    tabs: EKeyTabs,
    onCloseModal: () => void,
    m2dId: number
}) => {
    const {t} = useTranslation();

    const {
        vm,
        onLoadItems,
        onDeleteItem,
    } = Map2DProfileAction();

    const [queryParams, setQueryParams] = useState<{ page: number, limit: number }>({
        page: vm.query.page,
        limit: vm.query.limit,
    });

    useEffect(() => {
        console.log('%cMount Screen: ProfileSaveModalFC', Color.ConsoleInfo);

        if (
            vm.items === null || vm.key !== props.m2dId.toString()
            || (vm.timestamp !== undefined && Utils.checkHourState(vm.timestamp))
        ) {
            onLoadItems(props.m2dId, queryParams);
        }

        return () => {
            console.log('%cUnmount Screen: ProfileSaveModalFC', Color.ConsoleInfo);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.tabs]);

    const onChangePage = (page: number) => {
        setQueryParams({
            ...queryParams,
            page: page,
            limit: vm.query.limit,
        })

        if (vm.items && vm.items?.slice((page - 1) * vm.query.limit, page * vm.query.limit).length === 0) {
            onLoadItems(props.m2dId, {
                page: page,
                limit: queryParams.limit,
            })
        }
    };

    const isUpdatingError = vm.isUpdating === SendingStatus.error;

    const [isSaveProfileVisible, setIsSaveProfileVisible] = useState<{
        visible: boolean,
        initValues?: ProfileModel,
        saveAs?: boolean,
    }>({visible: false});

    const showModalEditProfile = (item: ProfileModel) => {
        setIsSaveProfileVisible({
            ...isSaveProfileVisible,
            visible: true,
            initValues: item,
            saveAs: undefined
        })
    }

    const showModalSaveAsProfile = (item: ProfileModel) => {
        setIsSaveProfileVisible({
            ...isSaveProfileVisible,
            visible: true,
            initValues: item,
            saveAs: true
        })
    }

    const showModalSaveProfile = () => {
        setIsSaveProfileVisible({
            ...isSaveProfileVisible,
            visible: true,
            initValues: undefined,
            saveAs: undefined
        })
    }

    const onPreviewProfile = (data: ProfileModel) => {
        MapLoadGeoJsonProfile({
            viewer: props.viewer,
            data: data,
        });

        props.onCloseModal();
    }

    const [visibleDelete, setVisibleDelete] = useState<{
        id?: string,
        visible: boolean,
        config?: TConfig,
    }>({
        visible: false,
    })

    const onHandleCancelDelete = () => {
        setVisibleDelete({
            ...visibleDelete,
            visible: false,
        })
    }

    useEffect(() => {
        if (vm.isDeleting === SendingStatus.success) {
            setVisibleDelete(
                {
                    ...visibleDelete,
                    visible: false,
                }
            )

            notification.success({
                className: "notification-main with-icon",
                message: <CustomTypography textStyle={"text-12-18"} isStrong>
                    {t('success.delete')}
                </CustomTypography>,
            });
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.isDeleting])

    const onHandleOkDelete = useCallback(() => {
        if (visibleDelete.id) {
            onDeleteItem(props.m2dId, visibleDelete.id);

            if (visibleDelete.config) {
                if (visibleDelete.config.profile) {
                    visibleDelete.config.profile.forEach(layer => {
                        props.viewer.profileTool.children.forEach(profile => {

                            if (layer.vector && profile.id === layer.vector.id) {
                                props.viewer.profileTool.removeChild(profile)
                            }
                        })
                    })
                }

                if (visibleDelete.config.measures) {
                    visibleDelete.config.measures.features.forEach((feature) => {

                        const properties = feature.properties;

                        if (properties) {
                            const layer = props.viewer.measureTool.getLayerById(properties.typeDraw as EDrawType);

                            if (layer) {
                                const removeFeature = find(layer.getSource().getFeatures(), function (_) {
                                    return _.get('id') === properties.id
                                });

                                if (removeFeature) layer.getSource().removeFeature(removeFeature)
                            }
                        }
                    })
                }
            }
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [visibleDelete, visibleDelete.id])

    const onShowDeleteProfile = (id: string, config?: TConfig) => {

        setVisibleDelete({
            ...visibleDelete,
            id: id,
            visible: true,
            config: config,
        });
    }

    const columns: ColumnsType<ProfileModel> = [
        {
            key: 'name',
            title: t('text.name'),
            dataIndex: 'name',
            width: 300,
        },
        {
            title: t('text.createAt'),
            key: 'createdAt',
            render: (_, item) => (
                <Typography.Text>
                    {item.dateCreatedAtFormatted(t('format.dateTimeShort'))}
                </Typography.Text>
            ),
            width: 140,
        },
        {
            key: "action",
            title: t("text.actions"),
            render: (_, item, __) => {
                return (
                    <Space>
                        <CustomButton
                            type={"text"}
                            icon={
                                <Image
                                    src={Images.iconSaveBg.data}
                                    alt={Images.iconSaveBg.atl}
                                    height={16}
                                    width={16}
                                />
                            }
                            onClick={() => showModalEditProfile(item)}
                        >
                            {t("button.edit")}
                        </CustomButton>
                        <CustomButton
                            type={"text"}
                            icon={
                                <Image
                                    src={Images.iconSaveBg.data}
                                    alt={Images.iconSaveBg.atl}
                                    height={16}
                                    width={16}
                                />
                            }
                            onClick={() => showModalSaveAsProfile(item)}
                        >
                            {t('text.saveAs')}
                        </CustomButton>
                        <CustomButton
                            type={"text"}
                            icon={
                                <Image
                                    src={Images.iconEyeBg.data}
                                    alt={Images.iconEyeBg.atl}
                                    height={16}
                                    width={16}
                                />
                            }
                            onClick={() => onPreviewProfile(item)}
                        >
                            {t("button.view")}
                        </CustomButton>
                        <CustomButton
                            type={"text"}
                            icon={
                                <Image
                                    src={Images.iconTrashBg.data}
                                    alt={Images.iconTrashBg.atl}
                                    height={16}
                                    width={16}
                                />
                            }
                            onClick={() => onShowDeleteProfile(item.id, item.config)}
                        >
                            {t("button.delete")}
                        </CustomButton>
                    </Space>
                )
            }
        }
    ];

    return (
        <>
            {
                props.tabs === EKeyTabs.Profile && <CustomButton
                    usingFor={"geodetic"}
                    className={'btn-v2d-save-small'}
                    key={"btnAddNew"}
                    size={"small"}
                    onClick={() => showModalSaveProfile()}
                    icon={<PlusCircleOutlined/>}
                >
                    {t("button.saveNew")}
                </CustomButton>
            }
            <Card
                bodyStyle={{
                    padding: '0 1rem 1rem'
                }}
            >
                {
                    isUpdatingError && vm.error && vm.error.hasOwnProperty('warning')
                        ? <Alert message={vm.error['warning']} type={'error'} showIcon={true} className={'mb-5'}/>
                        : null
                }
                <Table
                    className={'table-v2d-profile-save'}
                    loading={vm.isLoading === SendingStatus.loading}
                    pagination={
                        {
                            total: vm.oMeta?.totalCount ?? 0,
                            defaultPageSize: 3,
                            defaultCurrent: 1,
                            onChange: (page) => onChangePage(page),
                        }
                    }
                    columns={columns}
                    dataSource={vm.items}
                    rowKey={'id'}
                />
            </Card>
            <ModalSaveNewEditor
                viewer={props.viewer}
                onClose={() => setIsSaveProfileVisible({visible: false})}
                isSaveProfileVisible={isSaveProfileVisible.visible}
                initValues={isSaveProfileVisible.initValues}
                saveAs={isSaveProfileVisible.saveAs}
                tabs={props.tabs}
            />
            <Modal
                className={'modal-main ant-modal-v2d small'}
                key={"modal_type_delete_profile_save"}
                centered
                title={t("title.confirmBeforeDeleting")}
                visible={visibleDelete.visible}
                onCancel={onHandleCancelDelete}
                confirmLoading={vm.isDeleting === SendingStatus.loading}
                onOk={onHandleOkDelete}
                closeIcon={<Image
                    height={24}
                    width={24}
                    src={Images.iconXCircle.data}
                    alt={Images.iconXCircle.atl}
                />}
                bodyStyle={{
                    padding: '1rem'
                }}
                footer={
                    <CustomButton
                        size={'small'}
                        usingFor={'geodetic'}
                        key={"save"}
                        onClick={onHandleOkDelete}
                        loading={vm.isDeleting === SendingStatus.loading}
                    >
                        {t('button.ok')}
                    </CustomButton>
                }
            >
                {t("message.confirmBeforeDeleting")}
            </Modal>
        </>
    )
}

const ModalSaveNewEditor = (props: {
    viewer: Viewer,
    onClose: () => void,
    isSaveProfileVisible: boolean,
    initValues?: ProfileModel,
    saveAs?: boolean,
    tabs: EKeyTabs,
}) => {
    const {t} = useTranslation()
    const navigateType = useNavigationType()
    const location = useLocation()
    const {hash} = useParams<{ hash: string }>()

    const decode = CHashids.decode(hash!)
    const [form] = Form.useForm()

    const m2dId = decode[1] as number;

    const {
        vm,
        onAddItem,
        onEditItem,
        onClearState,
    } = Map2DProfileAction();

    useEffect(() => {
        console.log('%cMount Screen: ModalSaveNewProfile', Color.ConsoleInfo);

        return () => {
            if (
                navigateType === RouteAction.PUSH
                || navigateType === RouteAction.REPLACE
                || navigateType === RouteAction.POP
            ) {
                if (location.pathname.indexOf(RouteConfig.GEODETIC_VIEW_2D) !== 0) {
                    onClearState();
                }
            }

            console.log('%cUnmount Screen: ModalSaveNewProfile', Color.ConsoleInfo);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [navigateType]);

    useEffect(() => {
        if (vm.isUpdating === SendingStatus.success) {
            props.onClose();

            notification.success({
                className: "notification-main with-icon",
                message: <CustomTypography textStyle={"text-12-18"} isStrong>
                    {t('text.successSetting')}
                </CustomTypography>
            });
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.isUpdating]);

    const layerProfile = props.viewer.profileTool.layer?.getLayers().getArray() as VectorLayer<VectorSource<Geometry>>[];
    const layerMeasure = props.viewer.measureTool.layer?.getLayers().getArray() as VectorLayer<VectorSource<Geometry>>[];

    const onFinishSaveNewFeature = (values: TFormProfileValues) => {
        if (!m2dId) {
            console.log('%cError save profile because m2dId undefined', Color.ConsoleError);

            return;
        }

        const getConfigColl = () => {
            const configMeasure: GeoJSONFeatureCollection = {
                type: "FeatureCollection",
                features: []
            }
            const configProfile: TProfile[] = [];

            const addLayerProfileShare = layerProfile.map(layer => {
                if (includes(values.layers, layer.get('id'))) {
                    return layer
                }
            });

            const addLayerMeasureShare = layerMeasure.map(layer => {
                if (includes(values.layers, layer.get('id'))) {
                    return layer
                }
            });

            addLayerMeasureShare.forEach((layer: VectorLayer<VectorSource<any>> | undefined) => {
                if (layer) {
                    const sourceLayer = layer.getSource();

                    if (sourceLayer === null || sourceLayer.getFeatures().length === 0) return

                    const featuresObj = geoJson.writeFeaturesObject(sourceLayer.getFeatures());

                    featuresObj.features.forEach(feature => configMeasure.features.push(feature))
                }
            })

            addLayerProfileShare.forEach((layer: VectorLayer<VectorSource<any>> | undefined) => {
                if (layer) {
                    const sourceLayer = layer.getSource();

                    if (sourceLayer === null) {
                        return
                    }

                    const addFeatColl = geoJson.writeFeaturesObject(sourceLayer.getFeatures());

                    const vectorColl = {
                        vector: {
                            id: layer.get('id'),
                            title: layer.get('title'),
                            typeDraw: layer.get('typeDraw'),
                            type: layer.get('type'),
                            properties: layer.get('properties'),
                        }
                    }

                    const featureColl = {
                        features: addFeatColl
                    }

                    const coll = {...vectorColl, ...featureColl};

                    configProfile.push(coll)
                }
            })

            return {measures: configMeasure, profile: configProfile} as TConfig
        }

        const geoJson = new GeoJSON();

        if (props.initValues) {
            if (props.saveAs === true) {
                const configColl = getConfigColl();

                // rewrite feature with new uuid
                //create new coll
                const configSaveAsProfile: TProfile[] = [];
                const configSaveAsMeasure: GeoJSONFeatureCollection = {
                    type: "FeatureCollection",
                    features: []
                }

                if (configColl) {
                    if (configColl.profile) {
                        configColl.profile.forEach(item => {
                            const idVector = uuid();

                            if (item.vector) item.vector.id = idVector;

                            if (item.features) item.features.features.forEach(feature => {
                                if (feature.properties && feature.properties.hasOwnProperty('id')) {

                                    feature.properties.id = uuid();
                                }
                            })

                            configSaveAsProfile.push({
                                vector: item.vector,
                                features: item.features,
                            })
                        })
                    }

                    if (configColl.measures) {
                        if (configColl.measures.features) configColl.measures.features.forEach(feature => {
                            if (feature.properties) {
                                feature.properties.id = uuid();
                                configSaveAsMeasure.features.push(feature);
                            }
                        })
                    }
                }

                const data: TProfileSaveV0 = {
                    name: values.name,
                    config: {
                        profile: configSaveAsProfile,
                        measures: configSaveAsMeasure,
                    }
                };

                onAddItem(m2dId, data);
            } else {

                const data: TProfileSaveV0 = {
                    name: values.name,
                    config: getConfigColl(),
                }

                onEditItem(m2dId, props.initValues.id, data);
            }
        } else {
            const data: TProfileSaveV0 = {
                name: values.name,
                config: getConfigColl(),
            }

            onAddItem(m2dId, data);
        }
    }

    const onAfterClose = () => {
        form.resetFields();
    }

    const onOk = () => {
        form.submit();
    }

    const dataDraws = {
        data: props.viewer.profileTool.getLayerByTypeProfile(EProfileType.Draw),
        key: EProfileType.Draw
    };
    const dataMarkers = {
        data: props.viewer.profileTool.getLayerByTypeProfile(EProfileType.Marker),
        key: EProfileType.Marker
    };
    const dataAnnotations = {
        data: props.viewer.profileTool.getLayerByTypeProfile(EProfileType.Annotation),
        key: EProfileType.Annotation
    };

    const dataMeasure = {
        data: props.viewer.measureTool.layer?.getLayersArray() as VectorLayer<VectorSource<Geometry>>[],
        key: EMapLayer.Measure,
    }

    const defaultChecked: Key[] = [];

    const getColProfileData = (clEditor: {
        data: VectorLayer<VectorSource<Geometry>>[],
        key: EProfileType | EMapLayer
    }) => {
        let titleProfile

        switch (clEditor.key) {
            case EProfileType.Annotation:
                titleProfile = t('text.layerAnnotations')

                break;
            case EProfileType.Draw:
                titleProfile = t('text.layerDraws')

                break;
            case EProfileType.Marker:
                titleProfile = t('text.markerMap')

                break;

            case EMapLayer.Measure:
                titleProfile = t('text.layerMeasures')

                break;

        }

        return (
            <Col
                span={6}
                key={clEditor.key}
            >
                <Space direction={"vertical"}>
                    <CustomTypography
                        textStyle={'text-v2d-highlight-semibold'}
                        isStrong
                        type={'title'}
                    >
                        {titleProfile}
                    </CustomTypography>
                    {
                        clEditor.data && clEditor.data.map(item => {

                            defaultChecked.push(item.get('id'))

                            return <Checkbox
                                value={item.get('id')}
                                key={item.get('id')}
                            >
                                <CustomTypography
                                    textStyle={'text-title-v2d-highlight-regular'}
                                >
                                    {item.get('title')}
                                </CustomTypography>
                            </Checkbox>
                        })
                    }
                </Space>
            </Col>
        )
    }

    return (
        <Modal
            className={'modal-main ant-modal-v2d large'}
            centered
            visible={props.isSaveProfileVisible}
            onCancel={props.onClose}
            title={props.initValues ? (props.saveAs ? t('button.saveAsNew') : t('button.edit')) : t('button.saveNew')}
            destroyOnClose
            key={'modal-form-save-feature'}
            onOk={onOk}
            afterClose={onAfterClose}
            confirmLoading={vm.isUpdating === SendingStatus.loading}
            closeIcon={
                <Image
                    height={24}
                    width={24}
                    src={Images.iconXCircle.data}
                    alt={Images.iconXCircle.atl}
                />
            }
            footer={
                <CustomButton
                    usingFor={"geodetic"}
                    key={"save"}
                    size={"small"}
                    onClick={() => onOk()}
                    loading={vm.isUpdating === SendingStatus.loading}
                >
                    {t("button.save")}
                </CustomButton>
            }
        >
            <Form
                form={form}
                labelCol={{span: 8}}
                labelAlign={"left"}
                onFinish={onFinishSaveNewFeature}
            >
                <Form.Item
                    className={'form-item label-typography-font-14-line-18 form-item-mb-3 form-item-ml-2 form-item-mt-4 w-4/5'}
                    key={"name"}
                    label={
                        <CustomTypography textStyle={'text-14-18'}>
                            {t("text.saveName")}
                        </CustomTypography>
                    }
                    name={'name'}
                    initialValue={props.initValues ? (props.saveAs ? `Copy ${props.initValues.name}` : props.initValues.name) : ''}
                    rules={[
                        {
                            max: 100,
                            message: t('validation.minAndMaxCharacter', {
                                label: t("text.name"),
                                min: '1',
                                max: '100'
                            }),
                        }
                    ]}
                >
                    <Input
                        className={'input-main input-v2d input-h-40 input-bg-white input-radius-4'}
                        allowClear
                        placeholder={t('text.fillName')}
                    />
                </Form.Item>
                <CustomTypography
                    className={'ml-2 w-full'}
                    textStyle={'text-14-18'}
                    isStrong
                >
                    {t('text.layerProfiles')} :
                </CustomTypography>
                <Row
                    gutter={[8, 8]}
                    className={'row-v2d-profile'}
                >
                    <Form.Item
                        className={'form-item'}
                        style={{width: 768}}
                        key={'profile'}
                        name={'layers'}
                        initialValue={defaultChecked}
                        tooltip={t('text.listProfile')}
                        rules={[
                            {
                                required: true,
                                message: t("validation.emptyProfile"),
                            }
                        ]}
                    >
                        <Checkbox.Group className={'checkbox-v2d-main display-flex label-ellipse'}>
                            {getColProfileData(dataDraws)}
                            {getColProfileData(dataAnnotations)}
                            {getColProfileData(dataMarkers)}
                            {getColProfileData(dataMeasure)}
                        </Checkbox.Group>
                    </Form.Item>
                </Row>
            </Form>
        </Modal>
    )
}
