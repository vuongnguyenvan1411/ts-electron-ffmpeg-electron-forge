import {v4 as uuid} from "uuid";
import {Fill, Stroke, Style, Text} from "ol/style";
import {Feature} from "ol";
import {Geometry, LineString, Point} from "ol/geom";
import {EDrawingWeight, EDrawType, EProfileType} from "../../const/Defines";
import {Draw} from "ol/interaction";
import {DrawEventName} from "../../const/Event";
import {DrawEvent} from "ol/interaction/Draw";
import {chunk} from "lodash";
import CircleStyle from "ol/style/Circle";
import {Viewer} from "../../viewer/Viewer";
import {TFunction} from "react-i18next";
import {TFormDrawerValues} from "../../viewer/clim/CLTypeTool";
import {removeAction, updateView} from "../tool/MapAction";
import {CLProfile} from "../../viewer/clim/CLProfileTool";
import {message} from "antd";
import {getColorStyle} from "./DrawProfileFeature";
import {StyleProfileFn} from "./StyleDrawFn";

export const DrawAnnotationFeature = (props: {
    values: TFormDrawerValues,
    viewer: Viewer,
    object: CLProfile,
    t: TFunction<"translation">,
}) => {
    console.log('onFormProfileAnnotationFinish', props.values);

    removeAction(props.viewer);

    const id = uuid();
    const vectorLayer = props.object.layer;
    const color = props.values.color;
    const textStyle = props.values.textStyle;
    const note = props.values.note

    if (!note) {
        return;
    } else if (note.trim().length === 0) {
        message.error(props.t('text.pleaseInputValueNote')).then();
        return;
    }

    const newStyle = new Style({
        text: new Text({
            text: note,
            scale: 1.5,
            fill: new Fill({
                color: getColorStyle(color?.fillColor!),
            }),
            stroke: new Stroke({
                width: 0.1,
                color: getColorStyle(color?.strokeColor!),
            }),
            // rotation: props.values.rotation,
            offsetX: 0,
            offsetY: 0,
            font: `${textStyle?.weight === EDrawingWeight.Default ? 'normal' : 'bold'} ${textStyle?.size}px Arial, Verdana, Helvetica, sans-serif`
        }),
    });

    if (props.values.modifyFeature) {
        const modifyValues = props.values.modifyFeature;

        if (modifyValues.id) {
            const feature = props.viewer.profileTool.getFeatureById(modifyValues.id);

            if (!feature) {
                return
            }

            if (modifyValues.coordinate) {
                (feature.getGeometry() as Point).setCoordinates(modifyValues.coordinate)
            }

            feature.setStyle(newStyle);
            feature.set('style', newStyle);

            updateView(props.viewer);
        }
    } else {
        const setFeatureProperties = (feature: Feature<Geometry>, idFeature?: string) => {

            feature.setProperties({
                id: idFeature ?? id,
                type: EProfileType.Annotation,
                style: newStyle,
                arrow: textStyle?.arrow,
                title: note,
                typeDraw: EDrawType.Point
            })
            feature.setStyle(newStyle);

            props.viewer.profileTool.addFeature({
                feature: feature,
                id: id,
                title: note,
                type: EProfileType.Annotation,
                typeDraw: EDrawType.Point,
                layer: vectorLayer,
                init: false
            })

            removeAction(props.viewer)
        }

        if (textStyle?.arrow) {
            const drawArrow = new Draw({
                source: vectorLayer.getSource()!,
                type: EDrawType.LineString,
                style: (feature: any) => StyleProfileFn(props.viewer, feature, false, props.t('text.clickToPutTextIntoMap')),
            });

            drawArrow.on(DrawEventName.DrawEnd, (evt: DrawEvent) => {
                const geometry = evt.feature.getGeometry() as LineString;
                const last = geometry.getLastCoordinate();
                const start = geometry.getFirstCoordinate();
                const end = chunk(geometry.getFlatCoordinates(), 2)[1]

                const dx = start[0] - end[0];
                const dy = start[1] - end[1];
                const rotation = Math.atan2(dy, dx);

                const lineStr1 = new LineString([start, [start[0] - 0.00008, start[1] - 0.00008]]);
                const lineStr2 = new LineString([start, [start[0] - 0.00008, start[1] + 0.00008]]);
                lineStr1.rotate(rotation, start);
                lineStr2.rotate(rotation, start);

                const style: Style[] = [];
                style.push(new Style({
                    geometry: lineStr1,
                    stroke: new Stroke({
                        color: getColorStyle(color?.fillColor!),
                        width: 1
                    })
                }))
                style.push(new Style({
                    geometry: lineStr2,
                    stroke: new Stroke({
                        color: getColorStyle(color?.fillColor!),
                        width: 1
                    })
                }))
                style.push(new Style({
                    stroke: new Stroke({
                        color: getColorStyle(color?.fillColor!),
                        width: 1
                    })
                }))
                evt.feature.setStyle(style);
                evt.feature.set('id', id);
                evt.feature.set('arrowColor', getColorStyle(color?.fillColor!));
                evt.feature.set('arrowGeom', {
                    last: last,
                    end: end,
                    start: start,
                })

                const feature = new Feature({
                    geometry: new Point(last),
                    population: 4000,
                    rainfall: 500,
                })
                vectorLayer.getSource()!.addFeature(feature);

                setFeatureProperties(feature, evt.feature.get('id'))
            });

            props.viewer.map.addInteraction(drawArrow);
        } else {

            const annotationDraw = new Draw({
                source: vectorLayer.getSource()!,
                type: EDrawType.Point,
                style: new Style({
                    fill: new Fill({
                        color: 'rgba(255, 255, 255, 0.2)',
                    }),
                    image: new CircleStyle({
                        radius: 5,
                        stroke: new Stroke({
                            color: 'rgba(0, 0, 0, 0.7)',
                        }),
                        fill: new Fill({
                            color: 'rgba(255, 255, 255, 0.2)',
                        }),
                    }),
                    text: new Text({
                        text: props.t('text.clickToPutTextIntoMap'),
                        offsetX: 50,
                        offsetY: 20,
                        scale: 1.2,
                    })
                }),
            });

            props.viewer.map.addInteraction(annotationDraw);

            annotationDraw.on(DrawEventName.DrawEnd, (evt) => {
                setFeatureProperties(evt.feature)
            });
        }
    }
    updateView(props.viewer);
}