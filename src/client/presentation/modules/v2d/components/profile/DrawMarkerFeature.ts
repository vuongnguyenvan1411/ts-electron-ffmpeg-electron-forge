import {TFormMarkerValues} from "../../viewer/clim/CLTypeTool";
import VectorSource from "ol/source/Vector";
import {v4 as uuid} from "uuid";
import VectorLayer from "ol/layer/Vector";
import {Fill, Icon, Stroke, Style, Text} from "ol/style";
import {Draw} from "ol/interaction";
import {EDrawType, EProfileType} from "../../const/Defines";
import {DrawEventName} from "../../const/Event";
import {Feature} from "ol";
import {Geometry, Point} from "ol/geom";
import {Viewer} from "../../viewer/Viewer";
import {TFunction} from "react-i18next";
import {removeAction, updateView} from "../tool/MapAction";
import {StyleProfileFn} from "./StyleDrawFn";
import IconMarkerInfo from "../../../../../assets/image/v2d/icon_marker/6-b-1x.png";

export const DrawMarkerFeature = (props: {
    values: TFormMarkerValues,
    viewer: Viewer,
    t: TFunction<"translation">,
    vectorLayer?: VectorLayer<VectorSource<Geometry>>,
}) => {
    const setPropertiesFeature = (feature: Feature<Point>) => {
        if (props.values.images) {
            feature.set('images', props.values.images)
        }

        if (props.values.videos) {
            feature.set('videos', props.values.videos)
        }

        if (props.values.hyperlink) {
            feature.set('hyperlink', props.values.hyperlink)
        }

        if (props.values.description !== null && props.values.description !== undefined) {
            feature.set('description', props.values.description);
        }
    }

    if (props.values.id) {
        const feature = props.viewer.profileTool.getFeatureById(props.values.id);

        if (!feature) {
            return
        }

        if (props.values.coordinate) {
            (feature.getGeometry() as Point).setCoordinates(props.values.coordinate)
        }

        feature.get('style')['text_']['text_'] = props.values.title;

        setPropertiesFeature(feature as Feature<Point>);

        feature.setProperties({
            ...feature.getProperties(),
            title: props.values.title,
            style: feature.get('style'),
        });

    } else {
        const vectorLayer = props.vectorLayer;

        if (!vectorLayer) {
            return
        }

        const addFeature = (feature: Feature<Point>) => {
            setPropertiesFeature(feature);

            const id = uuid();

            const style = new Style({
                image: new Icon({
                    anchor: [0.5, 46],
                    anchorXUnits: 'fraction',
                    anchorYUnits: 'pixels',
                    crossOrigin: '',
                    src: IconMarkerInfo.src,
                }),
                text: new Text({
                    text: props.values.title,
                    offsetX: 35,
                    offsetY: -20,
                    scale: 1.2,
                    stroke: new Stroke({
                        width: 2,
                        color: 'rgba(255, 255, 255, 1)',
                    }),
                    fill: new Fill({
                        color: 'rgba(23, 156, 154, 1)',
                    }),
                }),
            });

            feature.setProperties({
                ...feature.getProperties(),
                id: id,
                title: props.values.title,
                type: EProfileType.Marker,
                style: style,
            })

            feature.setStyle(style);

            props.viewer.profileTool.addFeature({
                feature: feature,
                id: id,
                title: props.values.title,
                type: EProfileType.Marker,
                typeDraw: EDrawType.Point,
                layer: vectorLayer,
                init: true
            })

            removeAction(props.viewer);
        }

        if (props.values.coordinate) {

            const feature = new Feature({
                geometry: new Point(props.values.coordinate),
            });

            vectorLayer.getSource()?.addFeature(feature);

            addFeature(feature);

        } else {
            const draw = new Draw({
                source: vectorLayer.getSource()!,
                type: EDrawType.Point,
                style: (feature: Feature<any>) => StyleProfileFn(props.viewer, feature, false, props.t('text.clickToPutMarker')),
            });

            props.viewer.map.addInteraction(draw);

            draw.on(DrawEventName.DrawEnd, function (evt) {
                const feature = evt.feature as Feature<Point>;
                addFeature(feature);
            });
        }
    }
    updateView(props.viewer);
};
