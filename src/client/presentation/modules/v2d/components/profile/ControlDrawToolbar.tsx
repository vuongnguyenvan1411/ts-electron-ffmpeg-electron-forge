import {EMenuMapItem, EProfileType, EToolsEdit} from "../../const/Defines";
import {Viewer} from "../../viewer/Viewer";
import {CLProfile} from "../../viewer/clim/CLProfileTool";
import {CLMeasure} from "../../viewer/clim/CLMeasureTool";
import EventEmitter from "eventemitter3";
import {useTranslation} from "react-i18next";
import React, {useEffect, useState} from "react";
import {Utils2D} from "../../core/Utils";
import {CreateProfileLayerModalFC, TFormCreateProfile} from "./CreateProfileLayerModalFC";
import {removeAction, updateView} from "../tool/MapAction";
import {ItemType} from "antd/es/menu/hooks/useItems";
import {Menu, Popconfirm, Tooltip} from "antd";
import Image from "next/image";
import {Images} from "../../../../../const/Images";
import {ViewerEventName} from "../../const/Event";
import styles from "../../styles/MapView.module.scss";
import {ModifyCharacteristics, ModifyGeometry, RemoveUserDraw} from "../tool/InteractiveFeature";
import {CLShareRegion} from "../../viewer/clim/CLShareRegionTool";
import {FeatureSingleton} from "../../../../../models/FeatureSingleton";

export const ControlDrawToolbarFC = (props: {
    keyMenuLayer: EMenuMapItem,
    viewer: Viewer,
    selected?: CLProfile | CLMeasure | CLShareRegion
    onOpenAttributesTable: () => void,
    event: EventEmitter,
}) => {
    const {t} = useTranslation();
    const [selectedTool, setSelectedTool] = useState<EToolsEdit>();
    const [isPModalVisible, setIsPModalVisible] = useState<{
        visible: boolean,
        initValues?: TFormCreateProfile
    }>({
        visible: false
    });

    const modifyStyle = Utils2D.styleDrawModify();
    modifyStyle.getText().setText(t('text.dragToModify'));

    let disableFeatureTools = true;
    let disableObjectTools = true;
    let disableAttributesTable = true;
    let disableModify = true;
    const disableCharacteristics = props.selected instanceof CLShareRegion;

    if (props.selected) {
        disableFeatureTools = !props.selected.feature;

        if (props.selected instanceof CLMeasure) {
            if (props.selected.feature) {
                disableModify = false;
            }
        } else {
            disableModify = false;

            disableObjectTools = props.selected.feature ? true : (props.selected.layer.get('type') === EProfileType.Upload ? true : !props.selected.layer);
        }

        const propertiesObject = props.selected.layer.get('properties');

        disableAttributesTable = (!propertiesObject || propertiesObject.length < 1) ? true : disableObjectTools
    }

    useEffect(() => {
        setSelectedTool(undefined);

    }, [props.selected])

    const controlItems: ItemType[] = [
        {
            label: <Tooltip
                title={t('text.addFeature')}
                placement="bottomRight"
            >
                <Image
                    src={Images.iconPlus.data}
                    alt={Images.iconPlus.atl}
                    width={24}
                    height={24}
                />
            </Tooltip>,
            key: EToolsEdit.AddFeature,
            disabled: disableObjectTools,
        },
        {
            label: <Tooltip
                title={t('text.openAttributesTable')}
                placement="bottomRight"
            >
                <Image
                    src={Images.iconFolderNotchOpen.data}
                    alt={Images.iconFolderNotchOpen.atl}
                    width={24}
                    height={24}
                />
            </Tooltip>,
            key: EToolsEdit.OpenAttribute,
            disabled: disableAttributesTable,
        },
        {
            label: <Tooltip
                title={t('text.editCharacteristics')}
                placement="bottomRight"
            >
                <Image
                    src={Images.iconNotePencil.data}
                    alt={Images.iconNotePencil.atl}
                    width={24}
                    height={24}
                />
            </Tooltip>,
            key: EToolsEdit.EditCharacteristics,
            disabled: disableModify || disableCharacteristics,
        },
        {
            label: <Tooltip
                title={t('text.editGeometry')}
                placement="bottomRight"
            >
                <Image
                    src={Images.iconPencilSimpleLineBold.data}
                    alt={Images.iconPencilSimpleLineBold.atl}
                    width={24}
                    height={24}
                />
            </Tooltip>,
            key: EToolsEdit.EditGeometry,
            disabled: disableFeatureTools,
        },
        {
            label:
                <Popconfirm
                    overlayClassName={'popconfirm-content-main'}
                    title={props.selected instanceof CLMeasure ? t('text.confirmDeleteMeasure') : t('text.confirmDeleteProfile')}
                    onConfirm={() => {
                        RemoveUserDraw({
                            viewer: props.viewer,
                            selected: props.selected,
                        })
                        setSelectedTool(undefined);
                    }}
                    okText={t('button.ok')}
                    cancelText={t('text.no')}
                    onCancel={() => setSelectedTool(undefined)}
                    disabled={disableModify}
                >
                    <Tooltip
                        title={t('button.delete')}
                        placement="bottomRight"
                    >
                        <Image
                            src={Images.iconTrash.data}
                            alt={Images.iconTrash.atl}
                            width={24}
                            height={24}
                        />
                    </Tooltip>
                </Popconfirm>,
            key: EToolsEdit.Delete,
            disabled: disableModify,
        },
    ]

    const removeSelected = () => {
        setSelectedTool(undefined);
        removeAction(props.viewer);
        updateView(props.viewer);
    }

    const onOpenCreateProfileModal = (initValues: TFormCreateProfile) => {
        setIsPModalVisible({
            visible: true,
            initValues: initValues
        })
    }

    const onClickControl = (evt: any) => {
        switch (evt.key as EToolsEdit) {
            case EToolsEdit.EditGeometry:
                removeAction(props.viewer);
                updateView(props.viewer);

                if (selectedTool === EToolsEdit.EditGeometry) {
                    removeSelected();

                } else {
                    setSelectedTool(EToolsEdit.EditGeometry);
                    ModifyGeometry({
                        viewer: props.viewer,
                        selected: props.selected,
                        modifyStyle: modifyStyle,
                    });
                }

                break;

            case EToolsEdit.AddFeature:
                if (selectedTool === EToolsEdit.EditGeometry) {
                    removeSelected();

                } else {
                    setSelectedTool(EToolsEdit.AddFeature);
                    props.event.emit(ViewerEventName.FeatureCreated, {
                        object: props.selected,
                    });
                }

                break;

            case EToolsEdit.OpenAttribute:

                if (selectedTool === EToolsEdit.OpenAttribute) {
                    removeSelected();

                } else {
                    setSelectedTool(EToolsEdit.OpenAttribute);
                    props.onOpenAttributesTable();
                }

                break;

            case EToolsEdit.EditCharacteristics:

                if (selectedTool === EToolsEdit.EditCharacteristics) {
                    removeSelected();

                } else {
                    setSelectedTool(EToolsEdit.EditCharacteristics);

                    ModifyCharacteristics({
                        selected: props.selected,
                        viewer: props.viewer,
                        event: props.event,
                        onOpenCreateProfileModal: onOpenCreateProfileModal,
                    })
                }

                break;
        }
    }

    const getControlDrawBar = () => {
        return (
            <>
                <Menu
                    items={controlItems}
                    mode={'horizontal'}
                    className={styles.ControlDrawToolBar}
                    onClick={onClickControl}
                    selectedKeys={[selectedTool as string]}
                />
                {
                    (isPModalVisible && isPModalVisible.visible) && (
                        <CreateProfileLayerModalFC
                            viewer={props.viewer}
                            onClose={() => setIsPModalVisible({visible: false})}
                            modifyValues={{
                                initValues: isPModalVisible.initValues,
                                selected: props.selected as CLProfile,
                            }}
                            event={props.event}
                        />
                    )
                }
            </>
        )
    }

    return (
        <>
            {
                (FeatureSingleton.getInstance().v2DProfile || (props.selected && props.selected instanceof CLMeasure)) && (
                    getControlDrawBar()
                )
            }
        </>
    )
}
