import {Form, Input, InputNumber, Select} from "antd";
import {Viewer} from "../../viewer/Viewer";
import {CLProfile} from "../../viewer/clim/CLProfileTool";
import {useTranslation} from "react-i18next";
import React, {HTMLAttributes, ReactNode, useEffect, useState} from "react";
import {getCenter} from "ol/extent";
import {LineString, Point, Polygon} from "ol/geom";
import {EDrawType, EMapLayer, EPropertiesType} from "../../const/Defines";
import {Coordinate} from "ol/coordinate";
import {Fill, Stroke, Style, Text} from "ol/style";
import {TAttribute} from "./AttributeAddFeatureModalFC";
import {updateView} from "../tool/MapAction";
import {Color} from "../../../../../const/Color";
import {CustomModalAttributes} from "../contour/CustomModalAttributes";
import {CustomFormAttributes} from "../contour/CustomFormAttributes";
import {View} from "ol";
import {App} from "../../const/App";

interface IEditableCellProps extends HTMLAttributes<HTMLElement> {
    editing: boolean;
    dataIndex: string;
    title: string;
    inputType: EPropertiesType;
    record: any;
    index: number;
    children: ReactNode;
}

export const AttributeTableModalFC = (props: {
    viewer: Viewer,
    onCancel: () => void,
    object: CLProfile
}) => {
    const {t} = useTranslation();
    const [labelValue, setLabelValue] = useState('')
    const [mergedColumns, setMergedColumns] = useState<any[]>([]);

    const properties = props.object.layer.get('properties') as TAttribute[];
    const features = props.object.layer.getSource()!.getFeatures();
    const dataFeature: any = [];

    useEffect(() => {
        console.log('%cMount Screen: AttributeTableModalFC', Color.ConsoleInfo);

        return () => {
            console.log('%cUnmount Screen: AttributeTableModalFC', Color.ConsoleInfo);
        };

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    features.forEach((feature, index) => {
        const data = feature.get('properties');
        data.key = index + 1;
        data.id = feature.get('id');

        dataFeature.push(data)
    });


    const zoomToFeature = (record: any) => {
        const idFeature = record.id;

        if (!idFeature) {
            return
        }

        const feature = props.viewer.profileTool.getFeatureById(idFeature);
        const typeDraw = props.object.typeDraw;

        if (feature && typeDraw) {
            let centerGeom;

            switch (typeDraw) {
                default:
                    centerGeom = feature.get('center') as Coordinate

                    break;
                case EDrawType.LineString:

                    centerGeom = (feature.getGeometry() as LineString).getFlatMidpoint()
                    break;
                case EDrawType.Polygon:

                    centerGeom = getCenter((feature.getGeometry() as Polygon).getExtent())
                    break;
                case EDrawType.Point:

                    centerGeom = (feature.getGeometry() as Point).getCoordinates()

                    break;
                case EDrawType.Circle:

                    centerGeom = feature.get('circle').center;

                    break;
            }
            props.viewer.map.setView(new View({
                center: centerGeom,
                zoom: props.viewer.map.getView().getZoom(),
                projection: App.DefaultEPSG,
            }))
        }
    };

    const removeFeature = (record: any) => {
        const feature = props.viewer.profileTool.getFeatureById(record.id);

        const removeFeature = {
            ...props.object,
            feature: feature,
            id: record.id
        };
        props.viewer.profileTool.removeFeature(removeFeature as CLProfile);

        props.onCancel();
    };

    const chooseLabelFeature = (value: string) => {
        if (value) {
            setLabelValue(value)
        }
    }

    const onOk = () => {
        if (labelValue) {
            props.object.layer.getSource()!.getFeatures().forEach(feature => {

                (feature.getStyle() as Style).setText(
                    new Text({
                        font: '12px Calibri,sans-serif',
                        text: feature.get('properties')[labelValue],
                        placement: 'line',
                        fill: new Fill({
                            color: '#000'
                        }),
                        stroke: new Stroke({
                            color: '#fff',
                            width: 3
                        })
                    })
                )
                feature.set('label', {
                    isLabel: true,
                    tag: labelValue
                })
            })

            updateView(props.viewer);
        }

        props.onCancel();
    }

    return (
        <CustomModalAttributes
            onOk={onOk}
            onCancel={props.onCancel}
            form={
                <CustomFormAttributes
                    viewer={props.viewer}
                    properties={properties}
                    zoomToFeature={zoomToFeature}
                    removeFeature={removeFeature}
                    setMergedColumns={setMergedColumns}
                    dataFeature={dataFeature}
                    type={EMapLayer.Profile}
                />
            }
            select={
                <Select
                    className={'select-main select-v2d select-h-40 select-radius-4'}
                    dropdownClassName={'select-dropdown-v2d'}
                    style={{width: '100%'}}
                    placeholder={t('text.chooseAttributeToLabel')}
                    onChange={chooseLabelFeature}
                >
                    {
                        mergedColumns.map(item => {
                            if (!item.fixed) {
                                return (
                                    <Select.Option key={item.title}>
                                        {item.title}
                                    </Select.Option>
                                )
                            }
                        })
                    }
                </Select>
            }
        />
    )
}

export const EditableCell: React.FC<IEditableCellProps> = ({
                                                               editing,
                                                               dataIndex,
                                                               title,
                                                               inputType,
                                                               record,
                                                               index,
                                                               children,
                                                               ...restProps
                                                           }) => {
    const {t} = useTranslation();

    const inputNode = inputType === EPropertiesType.Number ?
        <InputNumber/> : (inputType === EPropertiesType.Date ? <Input type={EPropertiesType.Date}/> : <Input/>);

    return (
        <td {...restProps}>
            {editing ? (
                <Form.Item
                    name={dataIndex}
                    style={{margin: 0}}
                    rules={[
                        {
                            required: true,
                            message: t("validation.emptyData")
                        },
                    ]}
                >
                    {inputNode}
                </Form.Item>
            ) : (children)}
        </td>
    );
};