import {EDrawType, EProfileType} from "../../const/Defines";
import {TFunction} from "react-i18next";

export const getTitleDrawProfile = (props: {
    isNote: boolean,
    noteValues?: string,
    typeDraw?: EDrawType,
    typeProfile?: EProfileType,
    t: TFunction<"translation">,
}) => {
    let title;

    if (props.isNote && props.noteValues) {
        title = props.noteValues
    } else {
        if (props.typeDraw === EDrawType.LineString) {
            title = props.t('text.drawLine')
        } else if (props.typeDraw === EDrawType.Polygon) {
            title = props.t('text.drawPolygon')
        } else if (props.typeDraw === EDrawType.Circle || props.typeDraw === EDrawType.GeometryCollection) {
            title = props.t('text.drawCircle')
        } else if (props.typeProfile === EProfileType.Marker) {
            title = props.t('text.markerMap')
        } else {
            title = props.t('text.annotationPoint')
        }
    }
    return title;
}
