import {Viewer} from "../../viewer/Viewer";
import {Feature} from "ol";
import {Geometry, Point} from "ol/geom";
import {TFormDrawerValues} from "../../viewer/clim/CLTypeTool";
import {EDrawingDashed, EDrawingWeight, EDrawType, EProfileType} from "../../const/Defines";
import {Fill, Stroke, Style, Text} from "ol/style";
import {getColorStyle} from "./DrawProfileFeature";
import CircleStyle from "ol/style/Circle";
import {updateView} from "../tool/MapAction";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import {v4 as uuid} from "uuid";

export const ModifyFeatureProfile = (props: {
    viewer: Viewer,
    editValues: TFormDrawerValues,
    typeProfile: EProfileType,
    feature?: Feature<Geometry>,
    vectorLayer?: VectorLayer<VectorSource<any>>,
}) => {
    const color = props.editValues.color;
    let newStyle;
    let feature;

    if (props.typeProfile === EProfileType.Draw) {
        const stroke = props.editValues.stroke;
        const lineDash = stroke?.dashed === EDrawingDashed.Dashed ? [10, 10] : undefined;

        newStyle = new Style({
            fill: new Fill({
                color: getColorStyle(color?.fillColor!),
            }),
            stroke: new Stroke({
                color: getColorStyle(color?.strokeColor!),
                width: (stroke && stroke.width) ? stroke.width : 2,
                lineDash: lineDash,
            }),
            image: new CircleStyle({
                radius: 7,
                fill: new Fill({
                    color: getColorStyle(color?.fillColor!),
                }),
            }),
        })
    } else {
        const textStyle = props.editValues.textStyle;

        if (!textStyle) {
            return
        }

        newStyle = new Style({
            text: new Text({
                text: props.editValues.note,
                scale: 1.5,
                fill: new Fill({
                    color: getColorStyle(color?.fillColor!),
                }),
                stroke: new Stroke({
                    width: 0.1,
                    color: getColorStyle(color?.strokeColor!),
                }),
                // rotation: props.values.rotation,
                offsetX: 0,
                offsetY: 0,
                font: `${textStyle.weight === EDrawingWeight.Default ? 'normal' : 'bold'} ${textStyle?.size}px Arial, Verdana, Helvetica, sans-serif`
            }),
        });
    }

    if (props.feature) {
        feature = props.feature;

    } else {
        if (!props.editValues.modifyFeature || !props.editValues.modifyFeature.coordinate || !props.vectorLayer) {
            return;
        }

        feature = new Feature({
            geometry: new Point(props.editValues.modifyFeature.coordinate)
        });

        feature.setProperties({
            id: uuid(),
            type: EProfileType.Annotation,
            arrow: false,
            typeDraw: EDrawType.Point
        })

        props.vectorLayer.getSource()?.addFeature(feature);
    }

    feature.setStyle(newStyle);

    feature.setProperties({
        ...feature.getProperties(),
        style: newStyle,
        title: props.editValues.note
    })

    updateView(props.viewer);
}

export const ModifyFeatureDraw = (props: {
    viewer: Viewer,
    editValues: TFormDrawerValues,
    feature: Feature<Geometry>,
}) => {

    props.feature.setProperties({
        ...props.feature.getProperties(),
        title: props.editValues.note
    })

}
