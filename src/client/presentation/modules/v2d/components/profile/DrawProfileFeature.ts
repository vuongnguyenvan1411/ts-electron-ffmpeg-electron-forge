import {v4 as uuid} from "uuid";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import {Fill, Stroke, Style} from "ol/style";
import CircleStyle from "ol/style/Circle";
import {TFunction} from "react-i18next";
import {EDrawingDashed, EDrawingStyle, EDrawType, EPlaceBlock, EProfileType} from "../../const/Defines";
import {createBox, DrawEvent} from "ol/interaction/Draw";
import {Coordinate} from "ol/coordinate";
import {Circle, LineString, Point, Polygon} from "ol/geom";
import {fromCircle} from "ol/geom/Polygon";
import {Draw, Snap} from "ol/interaction";
import {DrawEventName, MapEventName} from "../../const/Event";
import {Feature} from "ol";
import {GeoJSON} from "ol/format";
import {along, bearing, bezierSpline, destination, distance, lineArc, lineString, point} from "@turf/turf";
import {chunk, slice} from "lodash";
import {Viewer} from "../../viewer/Viewer";
import {RGBColor} from "react-color";
import {TFormDrawerValues} from "../../viewer/clim/CLTypeTool";
import {PointerMoveHandler, removeAction, updateView} from "../tool/MapAction";
import {StyleProfileFn} from "./StyleDrawFn";
import {CLProfile} from "../../viewer/clim/CLProfileTool";

export const DrawProfileFeature = (props: {
    values: TFormDrawerValues,
    typeDraw: EDrawType,
    viewer: Viewer,
    t: TFunction<"translation">,
    object: CLProfile,
    onOpenAddAttribute: (feature: Feature<any>) => void
}) => {
    console.log('onFormProfileDrawFinish', props.values);

    removeAction(props.viewer);

    const id = uuid();
    const stroke = props.values.stroke;
    const lineDash = (stroke && stroke.dashed) === EDrawingDashed.Dashed ? [10, 10] : undefined;
    const freehand = props.values.drawingStyle === EDrawingStyle.FreeHand;
    const vectorLayer = props.object.layer;
    const placeBlock = props.values.placeBlock;
    const drawingStyle = props.values.drawingStyle;
    const color = props.values.color;
    const transform = props.values.transform;

    let geometryFunction;
    let typeDrawShape;

    //styles draw feature
    const style = new Style({
        fill: new Fill({
            color: getColorStyle(color?.fillColor!),
        }),
        stroke: new Stroke({
            color: getColorStyle(color?.strokeColor!),
            width: stroke?.width,
            lineDash: lineDash,
        }),
        image: new CircleStyle({
            radius: 7,
            fill: new Fill({
                color: color?.fillColor ? getColorStyle(color.fillColor) : '#000',
            }),
        }),
    })

    let tip = props.t('text.clickToStartDraw');
    let activeTip = '';
    let maxPoint = Infinity

    if (props.typeDraw === EDrawType.LineString &&
        (transform?.width || transform?.angle || drawingStyle === EDrawingStyle.Curve)) {
        maxPoint = 2;
    } else if (
        placeBlock === EPlaceBlock.Triangle ||
        placeBlock === EPlaceBlock.Oblique
    ) {
        maxPoint = 3;
    }

    const equilateralPolygon = (vertex: number) => {
        typeDrawShape = EDrawType.Circle;

        geometryFunction = function (coordinates: Coordinate[], geometry: Polygon) {
            const center = coordinates[0];
            const first = coordinates[1];

            let dx = center[0] - first[0];
            let dy = center[1] - first[1];
            let radius

            if (coordinates.length > 2) {
                const last = coordinates[2];
                dx = center[0] - last[0];
                dy = center[1] - last[1];
            }

            if (transform && transform.width && transform.width !== 0) {
                if (vertex === 3) {
                    radius = ((transform.width / 100) * Math.sqrt(3)) / 3
                } else if (vertex === 4) {
                    radius = (Math.sqrt(Math.pow(transform.width / 100, 2) * 2)) / 2
                } else {
                    radius = undefined
                }
            }

            const radius1 = Math.sqrt(dx * dx + dy * dy);
            const radius2 = Math.sqrt(dx * dx + dy * dy);

            const rotation = transform && transform.angle ? ((transform.angle - 45) * Math.PI) / 180 : Math.atan2(dy, dx);
            const polygon = fromCircle(new Circle(coordinates[0], radius ?? radius1), vertex);

            if (!radius) {
                polygon.scale(radius2 / radius1, 1);
            }

            polygon.rotate(rotation, center);

            if (!geometry) {
                geometry = polygon;
            } else {
                geometry.setCoordinates(polygon.getCoordinates());
            }

            return geometry
        }
    }

    //calculate point 2 of triangle base on 2 sides of right angle
    const pointCalculate = (variables: {
        angle: number,
        hypotenuse: number,
        x1: number,
        y1: number,
    }) => {
        let x;
        let y;

        const adjacent = variables.hypotenuse * Math.cos((variables.angle * Math.PI) / 180);
        const opposite = Math.sqrt(Math.pow(variables.hypotenuse, 2) - Math.pow(adjacent, 2));
        if (variables.angle < 90) {

            x = variables.x1 + opposite;
            y = variables.y1 + adjacent;
        } else if (variables.angle === 90) {

            x = variables.x1;
            y = variables.y1 + variables.hypotenuse;
        } else if (90 < variables.angle && variables.angle < 180) {

            x = variables.x1 - opposite;
            y = variables.y1 - adjacent;
        } else if (variables.angle === 180) {

            x = variables.x1 - variables.hypotenuse;
            y = variables.y1;
        } else if (180 < variables.angle && variables.angle < 270) {

            x = variables.x1 - opposite;
            y = variables.y1 + adjacent;

        } else if (variables.angle === 270) {
            x = variables.x1;
            y = variables.y1 - variables.hypotenuse;
        } else {
            x = variables.x1 + opposite;
            y = variables.y1 - adjacent;
        }

        return [x, y];
    };

    if (props.typeDraw === EDrawType.Polygon) {

        switch (placeBlock) {
            default:
                typeDrawShape = props.typeDraw;

                break;
            case EPlaceBlock.Rectangle:
                geometryFunction = createBox();
                typeDrawShape = EDrawType.Circle;

                break;
            case EPlaceBlock.Square:
                equilateralPolygon(4);

                break;
            case EPlaceBlock.Triangle:
                equilateralPolygon(3);

                break;
            case EPlaceBlock.Oblique:
                typeDrawShape = EDrawType.LineString;
                geometryFunction = function (coordinates: Coordinate[], geometry: Polygon) {
                    const center = coordinates[0];
                    const first = coordinates[1];

                    let dx = center[0] - first[0];
                    let dy = center[1] - first[1];
                    const radius1 = Math.sqrt(dx * dx + dy * dy);
                    if (coordinates.length > 2) {
                        const last = coordinates[2];
                        dx = center[0] - last[0];
                        dy = center[1] - last[1];
                    }

                    const radius2 = Math.sqrt(dx * dx + dy * dy);
                    const rotation = Math.atan2(dy, dx);
                    const circle = new Circle(center, radius1);
                    const polygon = fromCircle(circle, 64);

                    polygon.scale(radius2 / radius1, 1);
                    polygon.rotate(rotation, center);
                    if (!geometry) {
                        geometry = polygon;
                    } else {
                        geometry.setCoordinates(polygon.getCoordinates());
                    }
                    return geometry;
                };

                break;
        }
    } else {
        typeDrawShape = props.typeDraw;
    }

    const draw = new Draw({
        source: vectorLayer.getSource()!,
        type: typeDrawShape,
        freehand: freehand,
        style: (feature: any) => StyleProfileFn(props.viewer, feature, false, tip),
        maxPoints: maxPoint,
        snapTolerance: 0,
        geometryFunction: geometryFunction,
    });

    props.viewer.map.addInteraction(draw);

    if (props.typeDraw === EDrawType.Polygon) {
        activeTip = props.t('text.continuePolygonMsg');
    } else if (props.typeDraw === EDrawType.LineString) {
        activeTip = props.t('text.continueLineMsg');
    } else if (props.typeDraw === EDrawType.Circle) {
        activeTip = props.t('text.continueCircleMsg');
    } else {
        return;
    }

    let snap: Snap;

    draw.on(DrawEventName.DrawStart, (evt: DrawEvent) => {
        tip = activeTip;

        if (props.typeDraw === EDrawType.LineString) {
            const geom = evt.feature.getGeometry();

            const firstOfLine = (geom as LineString).getFirstCoordinate();

            if (transform) {
                if (transform.width && !transform.angle) {

                    const circleBound = new VectorLayer({
                        source: new VectorSource({
                            features: [
                                new Feature({
                                    geometry: new Circle(firstOfLine, transform.width / 100)
                                })
                            ]
                        })
                    })
                    snap = new Snap({
                        source: circleBound.getSource()!,
                        edge: true,
                        pixelTolerance: 1000,
                    })
                    snap.setActive(true);

                    props.viewer.map.addInteraction(snap);
                } else if (transform.angle && !transform.width) {

                    const target = pointCalculate({
                        angle: transform.angle,
                        hypotenuse: 1000,
                        x1: firstOfLine[0],
                        y1: firstOfLine[1],
                    })
                    const circleBound = new VectorLayer({
                        source: new VectorSource({
                            features: [
                                new Feature({
                                    geometry: new LineString([
                                        firstOfLine,
                                        target
                                    ])
                                })
                            ]
                        })
                    })
                    snap = new Snap({
                        source: circleBound.getSource()!,
                        pixelTolerance: 100,
                        vertex: true,
                        edge: true,
                    })
                    props.viewer.map.addInteraction(snap)
                } else if (transform.angle && transform.width) {

                    const target = pointCalculate({
                        angle: transform.angle,
                        hypotenuse: transform.width / 100,
                        x1: firstOfLine[0],
                        y1: firstOfLine[1],
                    })
                    const circleBound = new VectorLayer({
                        source: new VectorSource({
                            features: [
                                new Feature({
                                    geometry: new Point(target)
                                })
                            ]
                        })
                    })

                    snap = new Snap({
                        source: circleBound.getSource()!,
                        pixelTolerance: 1000,
                        edge: true,
                    })
                    props.viewer.map.addInteraction(snap)
                }
            }
        }
    })

    draw.on(DrawEventName.DrawEnd, (evt: DrawEvent) => {
        const feature = evt.feature;

        if (snap && snap.getActive()) {
            props.viewer.map.removeInteraction(snap)
        }

        props.viewer.map.once(MapEventName.PointerMove, PointerMoveHandler);

        if (drawingStyle === EDrawingStyle.Curve && props.typeDraw === EDrawType.LineString) {
            const geom = (evt.feature as Feature<LineString>).getGeometry();

            if (!geom) {
                return;
            }

            const coord = geom.getFlatCoordinates();

            const p1 = slice(coord, 0, 2);
            const p2 = slice(coord, 2, 4);

            const line = lineString([p1, p2]);
            const d = distance(p1, p2);
            const pMid = along(line, (d / 2));
            const lineBearing = bearing(p1, p2);
            const centerPoint = destination(pMid, (2 * d), (lineBearing - 90));

            const r = distance(centerPoint, point(p1));
            const bear1 = bearing(centerPoint, point(p1));
            const bear2 = bearing(centerPoint, point(p2));
            const arc2 = lineArc(centerPoint, r, bear2, bear1, {steps: 256});

            const readFeature = (new GeoJSON).readFeatures(arc2)[0];

            evt.feature.setGeometry(readFeature.getGeometry());

        } else if (drawingStyle === EDrawingStyle.Arcs && props.typeDraw === EDrawType.LineString) {
            const geom = (evt.feature as Feature<LineString>).getGeometry();

            if (!geom) {
                return;
            }

            const coord = chunk(geom.getFlatCoordinates(), 2);
            const line = lineString(coord);
            const curved = bezierSpline(line, {
                resolution: 10000,
                sharpness: 0.7,
            });

            const readFeature = (new GeoJSON()).readFeatures(curved)[0];

            evt.feature.setGeometry(readFeature.getGeometry());
        }

        const getTitleFeature = () => {
            if (props.values.note && props.values.note.trim().length !== 0) {
                return props.values.note
            } else {
                switch (props.typeDraw) {
                    default:
                        return props.t('text.drawLine')
                    case EDrawType.Circle:
                        return props.t('text.drawCircle')
                    case EDrawType.Polygon:
                        return props.t('text.drawPolygon')
                }
            }
        };

        const profileFeature = {
            id: id,
            type: EProfileType.Draw,
            typeDraw: props.typeDraw,
            style: style,
            title: getTitleFeature(),
        };

        feature.setProperties(profileFeature);
        feature.setStyle(style);

        if (props.typeDraw === EDrawType.Circle) {
            const circle = feature.getGeometry() as Circle;

            if (circle.getCenter() && circle.getRadius()) {
                feature.set('circle', {
                    center: circle.getCenter(),
                    radius: circle.getRadius(),
                })
            }
        }

        props.viewer.profileTool.addFeature({
            feature: feature,
            id: id,
            title: getTitleFeature(),
            type: EProfileType.Draw,
            typeDraw: props.typeDraw,
            layer: vectorLayer,
            init: false
        });

        props.onOpenAddAttribute(feature);

        updateView(props.viewer);
        DrawProfileFeature({...props});
    })
}

export const getColorStyle = (color: {
    rgb: RGBColor
}) => {

    let dColor
    const colorResult = color.rgb;
    dColor = `rgba(${colorResult.r},${colorResult.g},${colorResult.b},${colorResult.a})`
    return dColor
}

