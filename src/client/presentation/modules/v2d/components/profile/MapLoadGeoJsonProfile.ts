import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import {EDrawType, EProfileType} from "../../const/Defines";
import {Feature} from "ol";
import {Circle, Geometry, LineString} from "ol/geom";
import {GeoJSON} from "ol/format";
import {ProfileModel} from "../../../../../models/service/geodetic/ProfileModel";
import {Viewer} from "../../viewer/Viewer";
import {IOptCLProfile} from "../../viewer/clim/CLProfileTool";
import {find} from "lodash";
import {Coordinate} from "ol/coordinate";
import {Fill, Icon, Stroke, Style, Text} from "ol/style";
import CircleStyle from "ol/style/Circle";

export const MapLoadGeoJsonProfile = (props: {
    data: ProfileModel,
    viewer: Viewer
}) => {
    const newGeoJSONForm = new GeoJSON();
    const geoJson = props.data.config;
    const listOpts: IOptCLProfile[] = [];
    const listOptsFeature: IOptCLProfile[] = [];

    if (!geoJson) {
        return
    }

    if (geoJson.profile) {
        for (let i = 0; i < geoJson.profile.length; i++) {
            const profile = geoJson.profile[i]

            const propertiesVector = profile.vector;

            if (!propertiesVector) return

            const vectorLayer = new VectorLayer({
                source: new VectorSource(),
                declutter: true,
                properties: {
                    id: propertiesVector.id,
                    title: propertiesVector.title,
                    typeDraw: propertiesVector.typeDraw,
                    type: propertiesVector.type,
                    properties: propertiesVector.properties
                }
            })

            listOpts.push({
                id: propertiesVector.id,
                title: propertiesVector.title,
                type: propertiesVector.type as EProfileType,
                typeDraw: propertiesVector.typeDraw as EDrawType,
                layer: vectorLayer,
                init: true
            })

            props.viewer.profileTool.setChild(listOpts, true, true, false);
            props.viewer.profileTool.setPid(props.data.id);
        }

        const profileLayer = props.viewer.profileTool.layer!.getLayersArray()

        for (let i = 0; i < profileLayer.length; i++) {
            const layer = profileLayer[i] as VectorLayer<VectorSource<Geometry>>

            const layerProfile = find(geoJson.profile, function (_) {
                return _.vector?.id === layer.get('id')
            })

            const featuresProfile = newGeoJSONForm.readFeatures(layerProfile?.features);

            for (let i = 0; i < featuresProfile.length; i++) {
                const item = featuresProfile[i]

                const featureProperties = JSON.parse(JSON.stringify(item.getProperties()));
                delete featureProperties.geometry;

                const featureStyle = featureProperties.style;

                const clonedProperties = {...featureProperties};
                delete clonedProperties.style;

                let feature: Feature<Geometry> = new Feature();

                if (item.get('circle')) {
                    feature = new Feature(new Circle(item.get('circle')['center'], item.get('circle')['radius']));
                    feature.setProperties(clonedProperties);
                } else if (item.get('type')) {
                    feature = item;
                    feature.setProperties(clonedProperties);
                } else if (!item.get('type')) {
                    feature = item;
                    const arrowColor = item.get('arrowColor');
                    const arrowGeom = item.get('arrowGeom') as {
                        last: Coordinate,
                        end: Coordinate,
                        start: Coordinate,
                    };

                    const dx = arrowGeom.start[0] - arrowGeom.end[0];
                    const dy = arrowGeom.start[1] - arrowGeom.end[1];
                    const rotation = Math.atan2(dy, dx);

                    const lineStr1 = new LineString([arrowGeom.start, [arrowGeom.start[0] - 0.00008, arrowGeom.start[1] - 0.00008]]);
                    const lineStr2 = new LineString([arrowGeom.start, [arrowGeom.start[0] - 0.00008, arrowGeom.start[1] + 0.00008]]);
                    lineStr1.rotate(rotation, arrowGeom.start);
                    lineStr2.rotate(rotation, arrowGeom.start);

                    const style: Style[] = [];
                    style.push(new Style({
                        geometry: lineStr1,
                        stroke: new Stroke({
                            color: arrowColor,
                            width: 1
                        })
                    }))
                    style.push(new Style({
                        geometry: lineStr2,
                        stroke: new Stroke({
                            color: arrowColor,
                            width: 1
                        })
                    }))
                    style.push(new Style({
                        stroke: new Stroke({
                            color: arrowColor,
                            width: 1
                        })
                    }))
                    feature.setStyle(style);
                    feature.setProperties(clonedProperties);
                    feature.set('style', style);
                }

                switch (featureProperties.type) {
                    case EProfileType.Draw: {
                        const styleDraw = new Style({
                            fill: new Fill({
                                color: featureStyle['fill_']['color_']
                            }),
                            stroke: new Stroke({
                                color: featureStyle['stroke_']['color_'],
                                lineDash: featureStyle['stroke_']['lineDash_'],
                                width: featureStyle['stroke_']['width_']
                            }),
                            image: new CircleStyle({
                                radius: 7,
                                fill: new Fill({
                                    color: featureStyle['image_']['fill_']['color_'] ? featureStyle['image_']['fill_']['color_']['rgb'] : undefined,
                                })
                            })
                        });

                        feature.setStyle(styleDraw);
                        feature.set('style', styleDraw);

                        break;
                    }
                    case EProfileType.Marker: {
                        const iconStyle = new Style({
                            image: new Icon({
                                anchor: [0.5, 46],
                                anchorXUnits: 'fraction',
                                anchorYUnits: 'pixels',
                                opacity: 0.85,
                                crossOrigin: '',
                                src: `https://cdn-sc.autotimelapse.com/files/icon/marker/${featureProperties.icon ?? '6-b-1x.png'}`
                            }),
                            text: new Text({
                                text: item.get('title'),
                                offsetX: 35,
                                offsetY: -20,
                                scale: 1.2,
                                stroke: new Stroke({
                                    width: 2,
                                    color: 'rgba(255, 255, 255, 1)'
                                }),
                                fill: new Fill({
                                    color: 'rgba(23, 156, 154, 1)'
                                })
                            })
                        });

                        feature.setStyle((_, resolution) => {
                            const zoom = props.viewer.map.getView().getZoom()

                            if (zoom) {
                                iconStyle.getImage().setScale(props.viewer.map.getView().getResolutionForZoom(zoom) / resolution);
                            }

                            return iconStyle;
                        })
                        feature.set('style', iconStyle)

                        layer.setZIndex(Infinity);

                        break;
                    }
                    case EProfileType.Annotation: {
                        const textStyle = featureStyle['text_'];
                        const styleAnnotation = new Style({
                            text: new Text({
                                text: textStyle['text_'],
                                scale: 1.5,
                                fill: new Fill({
                                    color: textStyle['fill_']['color_']
                                }),
                                rotation: textStyle['rotation_'],
                                offsetX: 0,
                                offsetY: 0,
                                font: textStyle["font_"]
                            })
                        });

                        feature.setStyle(styleAnnotation);
                        feature.set('style', styleAnnotation);

                        layer.setZIndex(Infinity);

                        break;
                    }
                }

                layer.getSource()!.addFeature(feature);

                const optsFeature: IOptCLProfile = {
                    id: featureProperties.id,
                    type: EProfileType.Marker,
                    properties: featureProperties,
                    layer: layer,
                    feature: feature,
                    typeDraw: featureProperties.typeDraw as EDrawType,
                    title: featureProperties.title,
                    init: true
                }

                listOptsFeature.push(optsFeature)
            }

            props.viewer.profileTool.setFeatures(listOptsFeature)
        }
    }

    if (geoJson.measures) {
        const featuresMeasure = newGeoJSONForm.readFeatures(geoJson.measures);

        featuresMeasure.forEach((item) => {
            const featureProperties = JSON.parse(JSON.stringify(item.getProperties()));
            delete featureProperties.geometry;

            const cloneProperties = {...featureProperties}

            let feature: Feature<Geometry>;

            if (item.get('circle')) {
                feature = new Feature(new Circle(item.get('circle')['center'], item.get('circle')['radius']));
            } else {
                feature = item
            }

            feature.setProperties(cloneProperties)

            const vectorLayer = props.viewer.measureTool.getLayerById(feature.get('typeDraw') as EDrawType);

            if (vectorLayer && vectorLayer.getSource()) {
                vectorLayer.getSource().addFeature(feature)

                props.viewer.measureTool.addFeature({
                    id: featureProperties.id,
                    title: featureProperties.title,
                    typeDraw: featureProperties.typeDraw,
                    layer: vectorLayer,
                    feature: feature,
                    init: false
                })
            }
        })
    }
}
