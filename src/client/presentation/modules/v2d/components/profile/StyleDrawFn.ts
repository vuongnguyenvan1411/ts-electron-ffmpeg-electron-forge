import {Feature} from "ol";
import {Circle, Geometry, LineString, Point, Polygon} from "ol/geom";
import GeometryType from "ol/geom/GeometryType";
import {Utils2D} from "../../core/Utils";
import {Viewer} from "../../viewer/Viewer";

const stylePath = Utils2D.styleMeasurePath();
const styleRegion = Utils2D.styleDrawRegion();
const tipStyle = Utils2D.styleDrawTip();
const vectorDrawStyle = Utils2D.styleDrawVector();
const labelStyle = Utils2D.styleDrawLabel();

export const StyleProfileFn = (viewer: Viewer, feature: Feature<Geometry>, shareRegion?: boolean, tip?: string) => {
    const styles = [shareRegion === true ? styleRegion : stylePath];
    const geom = feature.getGeometry();

    if (geom) {
        const type = geom.getType();

        if (
            tip
            && type === GeometryType.POINT
            && viewer.shareTool.layer
        ) {

            tipStyle.getText().setText(tip);
            styles.push(tipStyle);
        }
    }

    return styles;
};

export const StyleMeasureFn = (feature: Feature<Geometry>, segments?: any, vector?: boolean, drawType?: string, tip?: string) => {
    const styles = [stylePath];
    const geom = feature.getGeometry();

    if (geom) {
        const type = geom.getType();
        let point, label, line;

        const segmentStyle = Utils2D.styleDrawSegment();
        const segmentStyles = [segmentStyle];

        if (vector === true) {
            styles.splice(0, styles.length);
            styles.push(vectorDrawStyle);
        }
        if (!drawType || drawType === type) {
            if (type === GeometryType.POLYGON && geom instanceof Polygon) {

                point = geom.getInteriorPoint();
                label = Utils2D.formatArea(geom);
                line = new LineString(geom.getCoordinates()[0]);
            } else if (type === GeometryType.LINE_STRING && geom instanceof LineString) {

                point = new Point(geom.getLastCoordinate());
                label = Utils2D.formatLength(geom);
                line = geom;
            } else if (type === GeometryType.CIRCLE && geom instanceof Circle) {

                label = Utils2D.formatCircle(geom);
                point = new Point(geom.getCenter());
                line = geom;
            }
        }

        if (segments && line) {
            if (line instanceof LineString) {
                let count = 0;

                line.forEachSegment((a: number[], b: number[]) => {
                    const segment = new LineString([a, b]);
                    const label = Utils2D.formatLength(segment);

                    let first = segment.getFirstCoordinate();
                    let last = segment.getLastCoordinate();
                    let rotation = -Math.atan((last[1] - first[1]) / (last[0] - first[0]));
                    segmentStyle.getText().setRotation(rotation)

                    if (segmentStyles.length - 1 < count) {
                        segmentStyles.push(segmentStyle.clone());
                    }

                    const segmentPoint = new Point(segment.getCoordinateAt(0.5));
                    segmentStyles[count].setGeometry(segmentPoint);
                    segmentStyles[count].getText().setText(label);
                    styles.push(segmentStyles[count]);

                    count++;
                });
            }
        }

        if (label && point) {
            labelStyle.setGeometry(point);
            labelStyle.getText().setText(label);
            styles.push(labelStyle);
        }

        if (
            tip
            && type === GeometryType.POINT
        ) {
            tipStyle.getText().setText(tip);
            styles.push(tipStyle);
        }
    }

    return styles;
};

