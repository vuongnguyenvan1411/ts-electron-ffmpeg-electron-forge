import {ChangeEvent} from "react";
import {message} from "antd";
import VectorSource from "ol/source/Vector";
import {Geometry} from "ol/geom";
import {EDrawType, EFileType, EProfileType} from "../../const/Defines";
import {v4 as uuid} from "uuid";
import {GeoJSON, GPX, IGC, KML} from "ol/format";
import VectorImageLayer from "ol/layer/VectorImage";
import {Utils2D} from "../../core/Utils";
import {IOptCLProfile} from "../../viewer/clim/CLProfileTool";
import {MapEventName} from "../../const/Event";
import {Viewer} from "../../viewer/Viewer";
import {TFunction} from "react-i18next";
import {tile} from 'ol/loadingstrategy';
import {createXYZ} from "ol/tilegrid";
import sms from "../../styles/MapView.module.scss";

export const onUploadKMLFile = (props: {
    event: ChangeEvent<HTMLInputElement>,
    viewer: Viewer,
    t: TFunction<"translation">,
}) => {
    const reader = new FileReader()

    props.event.stopPropagation()
    props.event.preventDefault()

    if (props.event.target.files) {
        const file = props.event.target.files[0];

        if (!file) return

        if (file.size / 1024 / 1024 > 10) {
            message.error(props.t('error.maxSize10mb')).then()

            return
        }

        const fileType = file.name.split(".").pop();
        let fileSrc: VectorSource<Geometry>;

        if (!fileType || !(Object.values(EFileType) as string[]).includes(fileType)) {
            message.error(props.t('error.filetypeError')).then()

            return
        }

        reader.readAsDataURL(file);

        reader.onload = function () {
            const id = uuid();
            const enc = new TextDecoder("utf-8");
            const urlFileReader = reader.result instanceof ArrayBuffer ? enc.decode(reader.result) : reader.result;

            if (!urlFileReader) {
                message.error(props.t('error.filetypeError')).then()

                return
            }

            const properties = {
                id: id,
                title: file.name.split(".").shift() as string,
                type: EProfileType.Upload,
                typeDraw: EDrawType.GeometryCollection,
            }

            switch (fileType as EFileType) {
                case EFileType.KML:
                    fileSrc = new VectorSource({
                        format: new KML({
                            writeStyles: true,
                            showPointNames: true,
                            extractStyles: true,
                        }),
                        url: urlFileReader,
                        wrapX: true,
                        strategy: tile(createXYZ({maxZoom: 20})),
                    })
                    break;

                case EFileType.GPX:
                    fileSrc = new VectorSource({
                        format: new GPX(),
                        wrapX: true,
                        url: urlFileReader,
                    })

                    break;

                case EFileType.IGC:
                    fileSrc = new VectorSource({
                        format: new IGC(),
                        wrapX: true,
                        url: urlFileReader,
                        strategy: tile(createXYZ({maxZoom: 20})),
                    })

                    break;
                case EFileType.GeoJSON:
                    fileSrc = new VectorSource({
                        format: new GeoJSON(),
                        wrapX: true,
                        url: urlFileReader,
                        strategy: tile(createXYZ({maxZoom: 20})),
                    })

                    break;
            }

            const fileLayer = new VectorImageLayer({
                source: fileSrc,
                declutter: true,
                style: fileType !== EFileType.KML ? function (feature) {
                    return Utils2D.styleDefault()[feature.getGeometry()!.getType()];
                } : null,
                properties: properties
            })

            const listOpts: IOptCLProfile[] = [
                {
                    ...properties,
                    layer: fileLayer as any,
                    init: false
                }
            ]

            props.viewer.profileTool.setChild(listOpts, false, true, false)

            fileSrc.on(MapEventName.FeaturesLoadStart, function (){
                props.viewer.map.getTargetElement().classList.add(sms.SpinnerLoading)
            })

            fileSrc.on(MapEventName.FeaturesLoadEnd, function () {
                props.viewer.map.getTargetElement().classList.remove(sms.SpinnerLoading)

                if (fileSrc.getState() === 'ready') {
                    props.viewer.map.getView().fit(fileLayer.getSource().getExtent());
                }
            })
        }
    }
}
