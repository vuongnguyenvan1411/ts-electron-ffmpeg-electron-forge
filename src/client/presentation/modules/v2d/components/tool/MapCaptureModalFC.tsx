import {useTranslation} from "react-i18next";
import React, {useEffect, useState} from "react";
import {Color} from "../../../../../const/Color";
import {Input, Modal, Radio, Space, Tabs} from "antd";
import {MapEventName} from "../../const/Event";
import {Viewer} from "../../viewer/Viewer";
import {updateView} from "./MapAction";
import Image from "next/image";
import {Images} from "../../../../../const/Images";
import {CustomButton} from "../../../../components/CustomButton";
import {CustomTypography} from "../../../../components/CustomTypography";

enum EPDFSize {
    A0 = 'a0',
    A1 = 'a1',
    A2 = 'a2',
    A3 = 'a3',
    A4 = 'a4',
    A5 = 'a5',
}

enum EResolution {
    DPI72 = 72,
    DPI150 = 150,
    DPI300 = 300,
}

type TPdfSize = Record<EPDFSize, number[]>

export const MapCaptureModalFC = (props: {
    viewer: Viewer,
    onClose: Function,
}) => {
    const {t} = useTranslation();

    useEffect(() => {
        console.log('%cMount FC: MapCaptureModalFC', Color.ConsoleInfo);

        return () => {
            props.viewer.map.un(MapEventName.RenderComplete, () => {
                console.log(`MapCaptureModalFC UN ${MapEventName.RenderComplete}`);
            });

            console.log('%cUnmount FC: MapCaptureModalFC', Color.ConsoleInfo);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const [isModalVisible, setIsModalVisible] = useState(true);

    const handleClose = () => {
        setIsModalVisible(false);
        props.onClose();
    };

    const [formatImage, setFormatImage] = useState('png');
    const [mapExportWidth, setMapExportWidth] = useState<number>(1920);
    const [mapExportHeight, setMapExportHeight] = useState<number>(1080);
    const [pageSizeMapPdfExport, setPageSizeMapPdfExport] = useState<EPDFSize>(EPDFSize.A4);
    const [resolutionSizeMapPdfExport, setResolutionSizeMapPdfExport] = useState<number>(EResolution.DPI72);
    const dims: TPdfSize = {
        [EPDFSize.A0]: [1189, 841],
        [EPDFSize.A1]: [841, 594],
        [EPDFSize.A2]: [594, 420],
        [EPDFSize.A3]: [420, 297],
        [EPDFSize.A4]: [297, 210],
        [EPDFSize.A5]: [210, 148],
    }

    const handleCaptureModalOk = () => {
        props.onClose();

        if (formatImage === 'png' || formatImage === 'jpeg') {
            props.viewer.map.once(MapEventName.RenderComplete, () => {
                const mapCanvas = document.createElement('canvas');

                mapCanvas.width = mapExportWidth;
                mapCanvas.height = mapExportHeight;

                const mapContext = mapCanvas.getContext('2d');

                if (!mapContext) {
                    return
                }

                Array.prototype.forEach.call(
                    props.viewer.map.getViewport().querySelectorAll('.ol-layer canvas, canvas.ol-layer'),
                    function (canvas) {
                        if (canvas.width > 0) {
                            const opacity =
                                canvas.parentNode.style.opacity || canvas.style.opacity;
                            mapContext.globalAlpha = opacity === '' ? 1 : Number(opacity);
                            let matrix;
                            const transform = canvas.style.transform;
                            if (transform) {
                                // Get the transform parameters from the style's transform matrix
                                matrix = transform
                                    .match(/^matrix\(([^]*)\)$/)[1]
                                    .split(',')
                                    .map(Number);
                            } else {
                                matrix = [
                                    parseFloat(canvas.style.width) / canvas.width,
                                    0,
                                    0,
                                    parseFloat(canvas.style.height) / canvas.height,
                                    0,
                                    0,
                                ];
                            }
                            // Apply the transform to the export map context
                            CanvasRenderingContext2D.prototype.setTransform.apply(
                                mapContext,
                                matrix
                            );
                            const backgroundColor = canvas.parentNode.style.backgroundColor;
                            if (backgroundColor) {
                                mapContext.fillStyle = backgroundColor;
                                mapContext.fillRect(0, 0, canvas.width, canvas.height);
                            }
                            mapContext.drawImage(canvas, 0, 0);
                        }
                    }
                );

                mapContext.globalAlpha = 1;
                mapContext.setTransform(1, 0, 0, 1, 0, 0);

                if (navigator.msSaveBlob) {
                    // link download attribute does not work on MS browsers
                    navigator.msSaveBlob(mapCanvas.toDataURL(), `map.${formatImage}`);

                } else {
                    const tagDownload = document.createElement('a');
                    tagDownload.setAttribute('id', 'image-download');
                    tagDownload.setAttribute('download', `map.${formatImage}`);
                    console.log('mapCanvas', mapCanvas)
                    tagDownload.href = mapCanvas.toDataURL();
                    tagDownload.click();
                }
            });
            props.viewer.map.renderSync();

        } else if (formatImage === 'pdf') {
            document.body.style.cursor = 'progress';
            const dim = dims[pageSizeMapPdfExport];
            const width = Math.round((dim[0] * resolutionSizeMapPdfExport) / 16.4);
            const height = Math.round((dim[1] * resolutionSizeMapPdfExport) / 16.4);
            const size = props.viewer.map.getSize();
            const viewResolution = props.viewer.map.getView().getResolution();

            props.viewer.map.once(MapEventName.RenderComplete, () => {
                const mapCanvas = document.createElement('canvas');
                mapCanvas.width = width;
                mapCanvas.height = height;
                const mapContext = mapCanvas.getContext('2d');

                if (!mapContext) {
                    return
                }

                Array.prototype.forEach.call(
                    props.viewer.map.getViewport().querySelectorAll('.ol-layer canvas, canvas.ol-layer'),
                    function (canvas) {
                        if (canvas.width > 0) {
                            const opacity =
                                canvas.parentNode.style.opacity || canvas.style.opacity;
                            mapContext.globalAlpha = opacity === '' ? 1 : Number(opacity);
                            let matrix;
                            const transform = canvas.style.transform;
                            if (transform) {
                                // Get the transform parameters from the style's transform matrix
                                matrix = transform
                                    .match(/^matrix\(([^]*)\)$/)[1]
                                    .split(',')
                                    .map(Number);
                            } else {
                                matrix = [
                                    parseFloat(canvas.style.width) / canvas.width,
                                    0,
                                    0,
                                    parseFloat(canvas.style.height) / canvas.height,
                                    0,
                                    0,
                                ];
                            }
                            // Apply the transform to the export map context
                            CanvasRenderingContext2D.prototype.setTransform.apply(
                                mapContext,
                                matrix
                            );
                            const backgroundColor = canvas.parentNode.style.backgroundColor;
                            if (backgroundColor) {
                                mapContext.fillStyle = backgroundColor;
                                mapContext.fillRect(0, 0, canvas.width, canvas.height);
                            }
                            mapContext.drawImage(canvas, 0, 0);
                        }
                    }
                );

                mapContext.globalAlpha = 1;
                mapContext.setTransform(1, 0, 0, 1, 0, 0);

                import("jspdf")
                    .then(({jsPDF}) => {
                        const pdf = new jsPDF('landscape', undefined, pageSizeMapPdfExport);
                        pdf.addImage(
                            mapCanvas.toDataURL('image/jpeg'),
                            'JPEG',
                            0,
                            0,
                            dim[0],
                            dim[1],
                        );
                        pdf.save('map.pdf');

                        // Reset original map size
                        props.viewer.map.setSize(size);
                        props.viewer.map.getView().setResolution(viewResolution);
                        props.onClose();
                        document.body.style.cursor = 'auto';
                    });
            });

            props.viewer.map.render();

            const printSize = [width, height];
            props.viewer.map.setSize(printSize);

            if (size && viewResolution) {
                const scaling = Math.min(width / size[0], height / size[1]);
                props.viewer.map.getView().setResolution(viewResolution / scaling);
            }

            updateView(props.viewer);
        }
    };

    const getImageSize = (
        <Space className={'mt-4'}>
            <Input
                addonBefore={t('text.width')}
                type={'number'}
                className={'input-main input-h-40'}
                defaultValue={mapExportWidth}
                onChange={(evt) => setMapExportWidth(parseFloat(evt.target.value))}
            />
            <Input
                addonBefore={t('text.height')}
                type={'number'}
                className={'input-main input-h-40'}
                defaultValue={mapExportHeight}
                onChange={(evt) => setMapExportHeight(parseFloat(evt.target.value))}
            />
        </Space>
    )

    return (
        <Modal
            centered
            className={'modal-main ant-modal-v2d middle'}
            key={'modal-capture'}
            visible={isModalVisible}
            onCancel={handleClose}
            cancelText={t('button.close')}
            closeIcon={<Image
                height={24}
                width={24}
                src={Images.iconXCircle.data}
                alt={Images.iconXCircle.atl}
            />}
            title={t('text.captureScreen')}
            footer={
                <CustomButton
                    size={'small'}
                    usingFor={'geodetic'}
                    key={"save"}
                    onClick={handleCaptureModalOk}
                >
                    {t("button.ok")}
                </CustomButton>
            }
        >
            <Tabs
                defaultActiveKey={'png'}
                onChange={(key: string) => {
                    setFormatImage(key)
                }}
                className={'tabs-main ant-tabs-v2d'}
            >
                <Tabs.TabPane tab={'PNG'} key={'png'}>
                    {getImageSize}
                </Tabs.TabPane>
                <Tabs.TabPane tab={'JPEG'} key={'jpeg'}>
                    {getImageSize}
                </Tabs.TabPane>
                <Tabs.TabPane tab={'PDF'} key={'pdf'}>
                    <CustomTypography type={'title'}>
                        {t('text.chooseSize')}
                    </CustomTypography>
                    <Radio.Group
                        className={'radio-main radio-v2d-main'}
                        buttonStyle="solid"
                        value={pageSizeMapPdfExport}
                        defaultValue={EPDFSize.A4}
                        onChange={(e) => {
                            setPageSizeMapPdfExport(e.target.value)
                        }}
                    >
                        <Space direction={'horizontal'}>
                            <Radio value={EPDFSize.A0}>A0 Paper</Radio>
                            <Radio value={EPDFSize.A1}>A1 Paper</Radio>
                            <Radio value={EPDFSize.A2}>A2 Paper</Radio>
                            <Radio value={EPDFSize.A3}>A3 Paper</Radio>
                            <Radio value={EPDFSize.A4}>A4 Paper</Radio>
                            <Radio value={EPDFSize.A5}>A5 Paper</Radio>
                        </Space>
                    </Radio.Group>
                    <CustomTypography type={'title'} className={'mt-2'}>
                        {t('text.resolution')}
                    </CustomTypography>
                    <Radio.Group
                        className={'radio-main radio-v2d-main'}
                        buttonStyle="solid"
                        value={resolutionSizeMapPdfExport}
                        onChange={(e) => {
                            setResolutionSizeMapPdfExport(e.target.value)
                        }}
                        defaultValue={EResolution.DPI72}
                    >
                        <Space direction={'horizontal'}>
                            <Radio value={EResolution.DPI72}>72 dpi (fast)</Radio>
                            <Radio value={EResolution.DPI150}>150 dpi</Radio>
                            <Radio value={EResolution.DPI300}>300 dpi (slow)</Radio>
                        </Space>
                    </Radio.Group>
                </Tabs.TabPane>
            </Tabs>
        </Modal>
    );
}
