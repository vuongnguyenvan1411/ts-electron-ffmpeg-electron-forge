import {Viewer} from "../../viewer/Viewer";
import {EDrawType, EMenuMapItem, EProfileType} from "../../const/Defines";
import {useTranslation} from "react-i18next";
import {Menu} from "antd";
import styles from "../../styles/MapView.module.scss";
import {MeasureInteraction} from "../measure/MeasureInteraction";
import React, {useState} from "react";
import {CreateProfileLayerModalFC} from "../profile/CreateProfileLayerModalFC";
import EventEmitter from "eventemitter3";
import {OptionsMenu} from "../../const/OptionsMenu";

export const DrawToolsPanelFC = (props: {
    viewer: Viewer,
    keyMenuLayer: EMenuMapItem,
    event: EventEmitter,
}) => {
    if (props.keyMenuLayer === EMenuMapItem.MeasureLayer) {
        return <MeasureDrawToolBarFC {...props} />
    } else if (props.keyMenuLayer === EMenuMapItem.ProfileLayer) {
        return <ProfileDrawToolBarFC {...props} />
    } else {
        return null
    }
}

const MeasureDrawToolBarFC = (props: {
    viewer: Viewer,
    keyMenuLayer: EMenuMapItem
}) => {
    const {t} = useTranslation();
    const [selectedKey, setSelectedKey] = useState<EDrawType>();

    const onClickMeasure = (evt: any) => {
        setSelectedKey(evt.key);

        MeasureInteraction({
            viewer: props.viewer,
            t: t,
            format: evt.key as EDrawType
        })
    }

    return (
        <Menu
            items={OptionsMenu.measureMenuItems(t)}
            mode={'horizontal'}
            className={`${styles.MeasureDrawToolBar}`}
            onClick={onClickMeasure}
            selectedKeys={[selectedKey as string]}
        />
    )

}

const ProfileDrawToolBarFC = (props: {
    viewer: Viewer,
    keyMenuLayer: EMenuMapItem,
    event: EventEmitter,
}) => {

    const {t} = useTranslation();
    const [isPModalVisible, setIsPModalVisible] = useState<{
        isVisible: boolean,
        pType?: EProfileType
    }>();

    const onClosePModal = () => {
        setIsPModalVisible({
            isVisible: false
        })
    }

    const onClickProfile = (evt: any) => {
        setIsPModalVisible({
            isVisible: true,
            pType: evt.key as EProfileType
        })
    }

    return (
        <>
            <Menu
                items={OptionsMenu.profileMenuItems(t)}
                mode={'horizontal'}
                className={styles.ProfileDrawToolBar}
                onClick={onClickProfile}
            />
            {
                (isPModalVisible && isPModalVisible.isVisible) && <CreateProfileLayerModalFC
                    viewer={props.viewer}
                    onClose={onClosePModal}
                    pType={isPModalVisible.pType}
                    event={props.event}
                />
            }
        </>
    )
}
