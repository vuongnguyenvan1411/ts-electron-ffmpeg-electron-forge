import {Coordinate} from "ol/coordinate";
import {getCenter, getHeight, getWidth} from "ol/extent";
import {Collection, Feature, MapBrowserEvent, Overlay} from "ol";
import {Geometry, MultiPoint, Point, SimpleGeometry} from "ol/geom";
import {Fill, Stroke, Style} from "ol/style";
import CircleStyle from "ol/style/Circle";
import {Modify, Select, Translate} from "ol/interaction";
import {never, platformModifierKeyOnly, primaryAction} from "ol/events/condition";
import {Viewer} from "../../viewer/Viewer";
import {MapEventName, ModifyEventName} from "../../const/Event";
import {removeAction, updateView} from "./MapAction";
import {SelectEvent} from "ol/interaction/Select";
import {isArray} from "lodash";
import {TFunction} from "react-i18next";
import {RefObject} from "react";
import sms from "../../styles/MapView.module.scss";

export const RotateFeatures = (props: {
    viewer: Viewer,
    onClose: () => void,
    t: TFunction<"translation">,
    modifyStyle: Style,
    popupRef: RefObject<HTMLElement>,
}) => {

    removeAction(props.viewer);

    const popup = new Overlay({
        element: props.popupRef.current!,
        positioning: 'bottom-center',
        stopEvent: false,
    });

    const setToolTip = (evt: MapBrowserEvent<UIEvent>) => {
        const position = evt.coordinate;

        if (position) {
            popup.setPosition(position);
        }
        props.popupRef.current!.innerHTML = `<div class="${sms.TipOverlay}">${props.t('text.selectFeature')}</div>`;
    }

    const calculateCenter = (geometry: any) => {
        let center: Coordinate, coordinates, minRadius;
        const type = geometry.getType();

        if (type === 'Polygon') {
            let x = 0;
            let y = 0;
            let i = 0;
            coordinates = geometry.getCoordinates()[0].slice(1);
            coordinates.forEach(function (coordinate: Coordinate) {
                x += coordinate[0];
                y += coordinate[1];
                i++;
            });
            center = [x / i, y / i];
        } else if (type === 'LineString') {
            center = geometry.getCoordinateAt(0.5);
            coordinates = geometry.getCoordinates();
        } else {
            center = getCenter(geometry.getExtent());
        }
        let sqDistances;
        if (coordinates) {
            sqDistances = coordinates.map(function (coordinate: Coordinate) {
                const dx = coordinate[0] - center[0];
                const dy = coordinate[1] - center[1];
                return dx * dx + dy * dy;
            });
            minRadius = Math.sqrt(Math.max.apply(Math, sqDistances)) / 3;
        } else {
            minRadius =
                Math.max(
                    getWidth(geometry.getExtent()),
                    getHeight(geometry.getExtent())
                ) / 3;
        }
        return {
            center: center,
            coordinates: coordinates,
            minRadius: minRadius,
            sqDistances: sqDistances,
        };
    };

    const styleSelect = (feature: Feature<Geometry>) => {
        const styles = [
            new Style({
                stroke: new Stroke({
                    color: '#CC893D',
                    width: 2,
                }),
                geometry: function (feature) {
                    const modifyGeometry = feature.get('modifyGeometry');
                    return modifyGeometry ? modifyGeometry.geometry : feature.getGeometry();
                },
            })
        ];
        const modifyGeometry = feature.get('modifyGeometry');
        const geometry = modifyGeometry
            ? modifyGeometry.geometry
            : feature.getGeometry();

        const result = calculateCenter(geometry);
        const center = result.center;

        if (center) {
            styles.push(
                new Style({
                    geometry: new Point(center),
                    image: new CircleStyle({
                        radius: 4,
                        fill: new Fill({
                            color: '#ff3333',
                        }),
                    }),
                })
            );

            const coordinates = result.coordinates;

            if (coordinates) {
                const minRadius = result.minRadius;
                const sqDistances = result.sqDistances;
                const rsq = minRadius * minRadius;
                const points = coordinates.filter(function (coordinate: Coordinate, index: number) {
                    return sqDistances[index] > rsq;
                });
                styles.push(
                    new Style({
                        geometry: new MultiPoint(points),
                        image: new CircleStyle({
                            radius: 4,
                            fill: new Fill({
                                color: '#09B377',
                            }),
                        }),
                    })
                );
            }
        }
        return styles;
    }

    const rotateFeature = (evt: SelectEvent) => {
        const feature = evt.selected[0];

        if (feature && evt.deselected) {
            props.viewer.map.un(MapEventName.PointerMove, setToolTip);
            props.viewer.map.removeOverlay(popup);

            const id = feature.get('id');
            const layerSelected = feature.get('type') ? props.viewer.profileTool.getLayerByIdFeature(id) : props.viewer.measureTool.getLayerById(id);
            const defaultStyle = new Modify({features: new Collection([feature])}).getOverlay().getStyleFunction();

            if (layerSelected && layerSelected.getSource() !== null) {
                const modify = new Modify({
                    features: new Collection([feature]),
                    condition: function (event) {
                        return primaryAction(event) && !platformModifierKeyOnly(event);
                    },
                    deleteCondition: never,
                    insertVertexCondition: never,
                    snapToPointer: true,
                    style: function (feature) {
                        feature.get('features').forEach(function (modifyFeature: Feature<Geometry>) {
                            const modifyGeometry = modifyFeature.get('modifyGeometry');

                            if (modifyGeometry) {
                                const point = (feature.getGeometry() as SimpleGeometry).getCoordinates();
                                let modifyPoint = modifyGeometry.point;

                                if (!modifyPoint) {
                                    // save the initial geometry and vertex position
                                    modifyPoint = point;
                                    modifyGeometry.point = modifyPoint;
                                    modifyGeometry.geometry0 = modifyGeometry.geometry;
                                    // get anchor and minimum radius of vertices to be used

                                    const result = calculateCenter(modifyGeometry.geometry0);
                                    modifyGeometry.center = result.center;
                                    modifyGeometry.minRadius = result.minRadius;
                                }

                                const center = modifyGeometry.center;
                                const minRadius = modifyGeometry.minRadius;

                                let dx, dy;
                                dx = modifyPoint[0] - center[0];
                                dy = modifyPoint[1] - center[1];
                                const initialRadius = Math.sqrt(dx * dx + dy * dy);

                                if (initialRadius > minRadius && point) {
                                    const initialAngle = Math.atan2(dy, dx);
                                    dx = point[0] - center[0];
                                    dy = point[1] - center[1];
                                    const currentRadius = Math.sqrt(dx * dx + dy * dy);
                                    if (currentRadius > 0) {
                                        const currentAngle = Math.atan2(dy, dx);
                                        const geometry = modifyGeometry.geometry0.clone();
                                        geometry.scale(currentRadius / initialRadius, undefined, center);
                                        geometry.rotate(currentAngle - initialAngle, center);
                                        modifyGeometry.geometry = geometry;
                                    }
                                }
                            }
                        });

                        const styleModify = defaultStyle!(feature, -10);

                        if (isArray(styleModify)) {
                            styleModify[0].setImage(props.modifyStyle.getImage());
                            styleModify[0].setText(props.modifyStyle.getText());
                        }

                        return styleModify;
                    },
                });

                modify.on(ModifyEventName.ModifyStart, function (event) {
                    event.features.getArray().forEach(function (feature: Feature<any>) {
                        feature.set(
                            'modifyGeometry',
                            {geometry: (feature.getGeometry()).clone()},
                            true
                        );
                    });
                });

                modify.on(ModifyEventName.ModifyEnd, function (event) {
                    event.features.forEach(function (feature: Feature<Geometry>) {
                        const modifyGeometry = feature.get('modifyGeometry');
                        if (modifyGeometry) {
                            feature.setGeometry(modifyGeometry.geometry);
                            feature.unset('modifyGeometry', true);

                            props.onClose();
                            removeAction(props.viewer);
                            props.viewer.map.removeInteraction(modify);

                        }
                    });
                });

                props.viewer.map.addInteraction(modify);

                const translatingFeature = new Translate({
                    condition: function (event) {
                        return primaryAction(event) && platformModifierKeyOnly(event);
                    },
                    layers: [layerSelected]
                });
                props.viewer.map.addInteraction(translatingFeature);
            }
        }
    }

    const selectSingleClick = new Select({
        style: styleSelect,
    });

    props.viewer.map.addOverlay(popup);
    props.viewer.map.on(MapEventName.PointerMove, setToolTip);
    props.viewer.map.addInteraction(selectSingleClick);
    selectSingleClick.once(MapEventName.Select, rotateFeature);

    updateView(props.viewer);
}