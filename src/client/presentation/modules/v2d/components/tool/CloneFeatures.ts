import {Feature, MapBrowserEvent, Overlay} from "ol";
import {v4 as uuid} from "uuid";
import {Circle, Geometry, LineString, Point, Polygon} from "ol/geom";
import {CLProfile} from "../../viewer/clim/CLProfileTool";
import {EDrawType, EProfileType} from "../../const/Defines";
import {chunk, isEqual} from "lodash";
import {GeoJSON} from "ol/format";
import {Style} from "ol/style";
import {RefObject} from "react";
import {Viewer} from "../../viewer/Viewer";
import {Coordinate} from "ol/coordinate";
import {MapEventName} from "../../const/Event";
import {removeAction, updateView} from "./MapAction";
import {getTitleDrawProfile} from "../profile/GetTitleDrawPropfile";
import {TFunction} from "react-i18next";
import {centroid, polygon} from "@turf/turf";
import {Utils2D} from "../../core/Utils";
import sms from "../../styles/MapView.module.scss";
import {CLMeasure} from "../../viewer/clim/CLMeasureTool";

export const CloneFeatures = (props: {
    popupRef: RefObject<HTMLElement>,
    viewer: Viewer,
    isCut: boolean,
    t: TFunction<"translation">,
    onClose: () => void,
}) => {
    removeAction(props.viewer);

    const selectedStyle = Utils2D.selectedStyle();
    let oddFeatureSelected: Feature<Geometry>;
    let newFeature: Feature<Geometry>;
    let objectSelected: CLProfile | CLMeasure | undefined;
    let selectedFeature: Feature<Geometry> | null;
    let styleFeature: Style;
    let idSelected: string;

    const popup = new Overlay({
        element: props.popupRef.current!,
        positioning: 'bottom-center',
        stopEvent: false,
    });

    props.viewer.map.addOverlay(popup);

    const setToolTip = (evt: MapBrowserEvent<UIEvent>) => {
        const position = evt.coordinate;
        if (position) {
            popup.setPosition(position)
        }

        props.popupRef.current!.innerHTML = `<div class="${sms.TipOverlay}">${props.t('text.selectFeature')}</div>`;

        if (newFeature && newFeature.getGeometry()) {
            props.popupRef.current!.innerHTML = `<div class="${sms.TipOverlay}">${props.t('text.clickToSetFeature')}</div>`;
        }

        //hover selected
        if (selectedFeature) {
            if (selectedFeature.get('type')) {
                if (selectedFeature.getStyle() && styleFeature) {
                    selectedFeature.setStyle(styleFeature);
                    props.viewer.map.once(MapEventName.SingleClick, onCloneFeature);
                }
                selectedFeature = null;
            } else {
                props.viewer.map.once(MapEventName.SingleClick, onCloneFeature);
            }
        }

        props.viewer.map.forEachFeatureAtPixel(evt.pixel, function (f: Feature<any>) {
            selectedFeature = f;

            if (!idSelected && selectedFeature.get('type')) {
                if (f.getStyle()) styleFeature = f.getStyle() as Style;

                selectedStyle.getFill().setColor(f.get('COLOR') || '#eeeeee');

                (f as Feature<any>).setStyle(selectedStyle);

                return true;
            }
        })
    }

    const onCloneFeature = (evt: MapBrowserEvent<UIEvent>) => {
        const feature = props.viewer.map.forEachFeatureAtPixel(evt.pixel, feature => feature) as Feature<any>;

        if (feature) {
            const featureId = idSelected = feature.get('id');
            oddFeatureSelected = feature;

            const clone = (feature: Feature<Geometry>) => {
                const clone = new Feature(
                    feature.hasProperties() ? feature.getProperties() : []
                );

                clone.setGeometryName(feature.getGeometryName());
                const geometry = feature.getGeometry();
                if (geometry) {
                    clone.setGeometry(geometry.clone());
                }
                return clone;
            }

            if (feature.get('type')) {
                objectSelected = props.viewer.profileTool.getChildByFeatureId(featureId);

            } else {
                objectSelected = props.viewer.measureTool.getChildByIdFeature(featureId);
            }


            if (!objectSelected) {
                return
            }

            newFeature = clone(oddFeatureSelected);

            props.viewer.map.once(MapEventName.SingleClick, placeFeature);
        }
    }

    const placeFeature = (evt: MapBrowserEvent<UIEvent>) => {
        props.popupRef.current!.innerHTML = ''
        props.viewer.map.un(MapEventName.PointerMove, setToolTip);
        popup.setPosition(undefined);

        const evtCoord = evt.coordinate;

        if (!evtCoord || !objectSelected) {
            return
        }

        const typeDraw = (oddFeatureSelected.getGeometry() as Geometry).getType();

        let circleCloneCenter!: Coordinate;
        let geom: Geometry;
        let deltaX: number = 0;
        let deltaY: number = 0;

        switch (typeDraw) {
            case EDrawType.Polygon:
                const profileCoord = (oddFeatureSelected.getGeometry() as Polygon).getFlatCoordinates();
                const polygonTurf = polygon([chunk(profileCoord, 2)]);
                const centroidPoint = centroid(polygonTurf);

                const central = (new GeoJSON().readFeature(centroidPoint).getGeometry() as Point).getCoordinates();

                deltaX = evtCoord[0] - central [0];
                deltaY = evtCoord[1] - central [1];

                break;
            case EDrawType.LineString:
                // profileCoord = (profileFeature.getGeometry() as LineString).getFlatCoordinates();
                const midPoint = (oddFeatureSelected.getGeometry() as LineString).getFlatMidpoint()

                deltaX = evtCoord[0] - midPoint[0];
                deltaY = evtCoord[1] - midPoint[1];

                break;

            case EDrawType.Point:
                const point = (oddFeatureSelected.getGeometry() as Point).getCoordinates();

                deltaX = evtCoord[0] - point[0];
                deltaY = evtCoord[1] - point[1];

                break;

            case EDrawType.GeometryCollection:
            case EDrawType.Circle:
                const center = oddFeatureSelected.get('circle').center

                deltaX = evtCoord[0] - center[0];
                deltaY = evtCoord[1] - center[1];

                circleCloneCenter = [
                    oddFeatureSelected.get('circle').center[0] + deltaX,
                    oddFeatureSelected.get('circle').center[1] + deltaY,
                ]

                break;
        }
        const id = uuid();
        const sourceProfile = objectSelected.layer.getSource();

        if (sourceProfile === null) {
            return
        }

        if (isEqual(oddFeatureSelected.getStyle(), selectedStyle)) {
            if (styleFeature) {
                oddFeatureSelected.setStyle(styleFeature)
            } else {
                evt.preventDefault()
            }
        }

        if (props.isCut) {
            if (typeDraw === EDrawType.Circle) {
                (oddFeatureSelected.getGeometry() as Circle).setCenter(circleCloneCenter)

            } else if (typeDraw === EDrawType.GeometryCollection) {
                sourceProfile.getFeatures().forEach(item => {

                    if ((item.get('geometry') as Geometry).getType() === EDrawType.Circle) {
                        (item.getGeometry() as Circle).setCenter(circleCloneCenter)
                    }
                })

            } else {
                geom = oddFeatureSelected.getGeometry() as Geometry;
                geom.translate(deltaX, deltaY);
            }
        } else {
            let title;
            const styleFeature = oddFeatureSelected.getStyle();
            const text = styleFeature instanceof Style ? styleFeature.getText() : null;

            if (typeDraw === EDrawType.Circle || typeDraw === EDrawType.GeometryCollection) {
                newFeature = new Feature({
                    geometry: new Circle(circleCloneCenter, oddFeatureSelected.get('circle').radius)
                });
            } else {
                geom = newFeature.getGeometry() as Geometry;
                geom.translate(deltaX, deltaY);
            }

            if (styleFeature !== null) newFeature.setStyle(styleFeature);

            if (objectSelected instanceof CLProfile) {
                switch (oddFeatureSelected.get('type')) {
                    default:
                        newFeature.set('type', typeDraw);
                        break;
                    case EProfileType.Marker:
                        newFeature.set('type', EProfileType.Marker);
                        break;
                    case EProfileType.Annotation:
                        newFeature.set('type', EProfileType.Annotation);
                        break;
                }
            }

            if (text !== null) {
                title = getTitleDrawProfile({
                    isNote: true,
                    t: props.t,
                    noteValues: text.getText() as string
                });
            } else {
                title = getTitleDrawProfile({
                    isNote: false,
                    typeDraw: typeDraw,
                    t: props.t
                })
            }

            const clonedProperties = JSON.parse(JSON.stringify(oddFeatureSelected.getProperties()));
            delete clonedProperties.geometry;
            clonedProperties.id = id;

            if (typeDraw === EDrawType.Circle || typeDraw === EDrawType.GeometryCollection) {
                clonedProperties.circle = {
                    center: circleCloneCenter,
                    radius: oddFeatureSelected.get('circle').radius,
                }
            }

            newFeature.setProperties(clonedProperties);
            sourceProfile.addFeature(newFeature);

            const newObjectFeature = {
                id: id,
                title: title ?? EProfileType.Draw,
                typeDraw: typeDraw,
                feature: newFeature,
                init: false
            }

            if (objectSelected instanceof CLProfile) {
                props.viewer.profileTool.addFeature({
                    ...newObjectFeature,
                    type: EProfileType.Draw,
                    layer: objectSelected.layer,
                })
            } else {
                props.viewer.measureTool.addFeature({
                    ...newObjectFeature,
                    layer: objectSelected.layer,
                })
            }
        }

        removeAction(props.viewer);
        props.onClose();
    }

    props.viewer.map.on(MapEventName.PointerMove, setToolTip);

    updateView(props.viewer);
}
