import {EDrawType, ESnapType} from "../../const/Defines";
import {Snap} from "ol/interaction";
import {GeoJSON} from "ol/format";
import {Geometry, LineString, Point, Polygon} from "ol/geom";
import {Collection, Feature} from "ol";
import {getCenter} from "ol/extent";
import VectorSource from "ol/source/Vector";
import {Viewer} from "../../viewer/Viewer";
import {lineIntersect} from "@turf/turf";

export const SnapInteraction = (props: {
    format: ESnapType,
    viewer: Viewer
}) => {

    if (!props.viewer.profileTool.layer) {
        return
    }

    let snapProfile: Snap = new Snap({});
    const profileFeatures = props.viewer.profileTool.getFeatures();
    const profileSources = props.viewer.profileTool.getSource();
    const formatGeoJSON = new GeoJSON();

    switch (props.format) {
        case ESnapType.Edge:
        case ESnapType.Vertex:
            profileSources.forEach(source => {
                snapProfile = new Snap({
                    source: source,
                    vertex: props.format === ESnapType.Vertex,
                    edge: props.format === ESnapType.Edge,
                    pixelTolerance: 12,
                });
                props.viewer.map.addInteraction(snapProfile);
            })

            break;
        case ESnapType.Center:
            profileFeatures.forEach(feature => {
                if (feature.getGeometry()?.getType() === EDrawType.LineString) {
                    const midPoint = (feature.getGeometry() as LineString).getFlatMidpoint();
                    const featureSnap = new Feature({
                        geometry: new Point(midPoint),
                    })
                    snapProfile = new Snap({
                        features: new Collection([featureSnap]),
                        pixelTolerance: 12,
                    })
                    props.viewer.map.addInteraction(snapProfile);
                }

                if (feature.getGeometry()?.getType() === EDrawType.Polygon) {

                    const line = new LineString((feature.getGeometry() as Polygon).getCoordinates()[0]);

                    line.forEachSegment((a, b) => {
                        const segment = new LineString([a, b]);
                        const midPoint = segment.getFlatMidpoint();

                        const featureSnap = new Feature({
                            geometry: new Point(midPoint),
                        });
                        snapProfile = new Snap({
                            features: new Collection([featureSnap]),
                            pixelTolerance: 12,
                        });
                        props.viewer.map.addInteraction(snapProfile);
                    })
                }
            })

            break
        case ESnapType.Middle:
            profileFeatures.forEach(feature => {
                const featureType = feature.getGeometry()?.getType();
                let center;

                if (featureType === EDrawType.Circle || featureType === EDrawType.GeometryCollection) {
                    center = feature.get('circle').center;

                }
                if (featureType === EDrawType.Polygon) {
                    center = getCenter((feature.getGeometry() as Polygon).getExtent());
                }

                if (center) {
                    const featureSnap = new Feature({
                        geometry: new Point(center),
                    });

                    snapProfile = new Snap({
                        features: new Collection([featureSnap]),
                        pixelTolerance: 20,
                    })
                    props.viewer.map.addInteraction(snapProfile);
                }
            })
            break;

        case ESnapType.Intersect:
            profileSources.forEach((source: VectorSource<Geometry>) => {
                const extentSource = source.getExtent();

                profileFeatures.forEach(feature => {
                    const type = feature.getGeometry()?.getType();

                    if (type === EDrawType.LineString || type === EDrawType.Polygon) {
                        const geoP = formatGeoJSON.writeFeatureObject(feature);

                        source.forEachFeatureIntersectingExtent(extentSource, function (featureE) {
                            const typeE = featureE.getGeometry()?.getType();

                            if (typeE === EDrawType.LineString || typeE === EDrawType.Polygon) {
                                const geoE = formatGeoJSON.writeFeaturesObject([featureE]);
                                // @ts-ignore
                                const intersects = lineIntersect(geoE, geoP);

                                const pointIntersects = formatGeoJSON.readFeatures(intersects);

                                snapProfile = new Snap({
                                    features: new Collection(pointIntersects),
                                    pixelTolerance: 20,
                                })
                                props.viewer.map.addInteraction(snapProfile);
                            }
                        })
                    }
                })
            })

            break;
    }
}
