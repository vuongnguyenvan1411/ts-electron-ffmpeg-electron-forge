import {Viewer} from "../../viewer/Viewer";
import {MapEventName} from "../../const/Event";
import {DoubleClickZoom, DragPan, DragRotate, DragRotateAndZoom, DragZoom, Draw, KeyboardPan, KeyboardZoom, Modify, MouseWheelZoom, PinchRotate, PinchZoom, Snap} from "ol/interaction";
import {MutableRefObject} from "react";
import {Geolocation, MapBrowserEvent} from "ol";
import {Utils2D} from "../../core/Utils";
import {find} from "lodash";
import {EMapLayer} from "../../const/Defines";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import {Rotate} from "ol/control";

export const removeAction = (viewer: Viewer, trackLocation?: MutableRefObject<Geolocation | undefined>) => {
    const mapEventSingleClick = viewer.map.getListeners(MapEventName.SingleClick);
    const mapEventPointerMove = viewer.map.getListeners(MapEventName.PointerMove);

    const mapOverLay = viewer.map.getOverlays();
    const mapInteraction = viewer.map.getInteractions().getArray();

    if (mapEventSingleClick) {
        mapEventSingleClick.forEach(evt => {
            viewer.map.removeEventListener(MapEventName.SingleClick, evt)
        })
    }
    if (mapEventPointerMove) {
        mapEventPointerMove.forEach(evt => {
            viewer.map.removeEventListener(MapEventName.PointerMove, evt)
        })
    }
    if (mapOverLay) {
        mapOverLay.forEach(overlay => {
            viewer.map.removeOverlay(overlay)
        })
    }
    if (mapInteraction) {
        mapInteraction.forEach(interaction => {
            if (!(interaction instanceof MouseWheelZoom) &&
                !(interaction instanceof DragPan) &&
                !(interaction instanceof DragZoom) &&
                !(interaction instanceof KeyboardPan) &&
                !(interaction instanceof KeyboardZoom) &&
                !(interaction instanceof PinchZoom) &&
                !(interaction instanceof Snap) &&
                !(interaction instanceof Rotate) &&
                !(interaction instanceof DragRotateAndZoom) &&
                !(interaction instanceof PinchRotate) &&
                !(interaction instanceof DragRotate) &&
                !(interaction instanceof DoubleClickZoom)

            ) {
                viewer.map.removeInteraction(interaction)

                if (interaction instanceof Draw ||
                    interaction instanceof Modify
                ) {
                    interaction.getOverlay().setStyle(null)
                }
            }
        })
    }
    if (trackLocation && trackLocation.current) {
        trackLocation.current.setTracking(false);
    }
    //show location
    const showLocationVector = find(viewer.map.getLayers().getArray(), function (layer) {
        return layer.get('id') === EMapLayer.ShowLocation;
    });
    const trackLocationVector = find(viewer.map.getLayers().getArray(), function (layer) {
        return layer.get('id') === EMapLayer.TrackLocation;
    });

    if (showLocationVector) viewer.map.removeLayer(showLocationVector);
    if (trackLocationVector) (trackLocationVector as VectorLayer<VectorSource<any>>).getSource()?.clear(true);
}

export const updateView = (viewer: Viewer) => {
    viewer.map.redrawText();
    viewer.map.updateSize();
    viewer.map.render();
}

export const PointerMoveHandler = (evt: MapBrowserEvent<UIEvent>) => {
    if (evt.dragging) {
        return;
    }
    const modifyStyle = Utils2D.styleDrawModify();

    // @ts-ignore
    modifyStyle.setGeometry();
}
