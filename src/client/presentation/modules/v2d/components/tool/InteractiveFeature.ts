import {CLMeasure} from "../../viewer/clim/CLMeasureTool";
import {CLProfile} from "../../viewer/clim/CLProfileTool";
import {Modify} from "ol/interaction";
import {Collection} from "ol";
import {Viewer} from "../../viewer/Viewer";
import {Style} from "ol/style";
import {EDrawingDashed, EDrawingWeight, EMenuMapItem, EProfileType} from "../../const/Defines";
import {ViewerEventName} from "../../const/Event";
import {isNumber, pickBy} from "lodash";
import {inputToRGB} from "@ctrl/tinycolor";
import {Point} from "ol/geom";
import {TFormCreateProfile} from "../profile/CreateProfileLayerModalFC";
import EventEmitter from "eventemitter3";
import {removeAction} from "./MapAction";
import {CLShareRegion} from "../../viewer/clim/CLShareRegionTool";

export const ModifyGeometry = (props: {
    selected: CLProfile | CLMeasure | CLShareRegion | undefined,
    viewer: Viewer,
    modifyStyle: Style
}) => {
    if (props.selected) {
        const featureModify = props.selected.feature;

        if (!featureModify) {
            return
        }

        const modify = new Modify({
            features: new Collection([
                featureModify
            ]),
            style: props.modifyStyle,
            snapToPointer: true
        })
        props.viewer.map.addInteraction(modify)
    }
}

export const ModifyCharacteristics = (props: {
    selected: CLProfile | CLMeasure | CLShareRegion | undefined,
    viewer: Viewer,
    event: EventEmitter,
    onOpenCreateProfileModal?: (initValues: TFormCreateProfile) => void,
}) => {
    if (!props.selected) {
        return
    }

    const feature = props.selected.feature;
    const layer = props.selected.layer;
    let initFeatureValues!: any;

    if (feature) {
        const idFeature = feature.get('id');

        if (props.selected instanceof CLMeasure) {

            initFeatureValues = {
                modifyFeature: {
                    id: idFeature,
                    type: EMenuMapItem.MeasureLayer
                },
                note: feature.get('title'),
            }
            props.event.emit(ViewerEventName.FeatureCreated, {
                object: props.selected as CLMeasure,
                initValues: initFeatureValues,
            });

        } else {
            const typeProfile = feature.get('type') as EProfileType;

            let fillColor;
            let strokeColor;

            switch (typeProfile) {
                case EProfileType.Draw:
                    const styleDraw = feature.getStyle() as Style;
                    fillColor = pickBy(inputToRGB(styleDraw.getFill().getColor()), isNumber) as any;
                    strokeColor = pickBy(inputToRGB(styleDraw.getFill().getColor()), isNumber) as any;

                    initFeatureValues = {
                        modifyFeature: {
                            id: idFeature,
                            type: EMenuMapItem.ProfileLayer,
                        },
                        color: {
                            fillColor: {rgb: fillColor},
                            strokeColor: {rgb: strokeColor},
                        },
                        note: feature.get('title'),
                        stroke: {
                            dashed: styleDraw.getStroke().getLineDash() ? EDrawingDashed.Dashed : EDrawingDashed.Default,
                            width: styleDraw.getStroke().getWidth() ?? 2,
                        },
                    }

                    break;
                case EProfileType.Annotation:
                    const styleAnnotation = feature.getStyle() as Style;
                    const textStyle = styleAnnotation.getText();

                    fillColor = pickBy(inputToRGB(textStyle.getFill().getColor()), isNumber) as any;
                    strokeColor = pickBy(inputToRGB(textStyle.getFill().getColor()), isNumber) as any;
                    const fontText = textStyle.getFont();

                    if (!fontText) {
                        return
                    }

                    const weight = (fontText.substring(0, fontText.indexOf(' ')) === 'normal') ? EDrawingWeight.Default : EDrawingWeight.Bold;

                    initFeatureValues = {
                        modifyFeature: {
                            id: idFeature,
                            type: EMenuMapItem.ProfileLayer
                        },
                        color: {
                            fillColor: {rgb: fillColor},
                            strokeColor: {rgb: strokeColor},
                        },
                        note: feature.get('title'),
                        textStyle: {
                            weight: weight,
                            arrow: feature.get('arrow'),
                            size: parseInt(fontText.substring(1, fontText.indexOf(' '))),
                        }
                    }

                    break;
                case EProfileType.Marker:
                    initFeatureValues = {
                        id: feature.get('id'),
                        coordinate: (feature.getGeometry() as Point).getCoordinates(),
                        title: feature.get('title'),
                        description: feature.get('description'),
                        images: feature.get('images'),
                        videos: feature.get('videos'),
                        hyperlink: feature.get('hyperlink'),
                    }

                    break;
            }

            props.event.emit(ViewerEventName.FeatureCreated, {
                object: props.selected as CLProfile,
                initValues: initFeatureValues,
            });
        }
    } else if (props.onOpenCreateProfileModal) {
        const initValues: TFormCreateProfile = {
            id: layer.get('id'),
            name: layer.get('title'),
            properties: layer.get('properties'),
            typeGeom: layer.get('typeDraw')
        }

        props.onOpenCreateProfileModal(initValues)
    }
}

export const RemoveUserDraw = (props: {
    selected: CLProfile | CLMeasure | CLShareRegion | undefined,
    viewer: Viewer,
}) => {
    if (props.selected instanceof CLMeasure) {
        props.viewer.measureTool.removeFeature(props.selected)

    } else if (props.selected instanceof CLProfile) {
        props.selected.feature ? props.viewer.profileTool.removeFeature(props.selected) : props.viewer.profileTool.removeChild(props.selected)

    } else {
        props.viewer.shareTool.removeAllChild()
    }
    removeAction(props.viewer);
}


