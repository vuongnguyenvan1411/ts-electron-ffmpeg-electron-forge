import {Form, message, Modal, Select, TreeSelect} from "antd";
import {Viewer} from "../../viewer/Viewer";
import {Geometry} from "ol/geom";
import {EDrawType} from "../../const/Defines";
import React, {useEffect} from "react";
import scm from "../../../../../styles/module/Common.module.scss";
import {Color} from "../../../../../const/Color";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import {BaseOptionType} from "rc-select/es/Select";
import {CommonEmptyFC} from "../../../../widgets/CommonFC";
import {Fill, Style} from "ol/style";
import {getVectorContext} from "ol/render";
import {removeAction, updateView} from "./MapAction";
import Image from "next/image";
import {Images} from "../../../../../const/Images";
import {CustomButton} from "../../../../components/CustomButton";
import {useTranslation} from "react-i18next";
import {MapEventName} from "../../const/Event";
import {CLProfile} from "../../viewer/clim/CLProfileTool";

export type EValuesClipFeature = {
    baseId: string,
    clipId: string,
}

export const MapClipFeatureModalFC = (props: {
    onClose: () => void,
    viewer: Viewer,
    onStopProgress: () => void,
}) => {

    const [form] = Form.useForm();
    const {t} = useTranslation();
    const treeClipFeature: BaseOptionType[] = [];

    useEffect(() => {
        console.log('%cMount Screen: MapClipFeatureModal', Color.ConsoleInfo);

        return () => {
            console.log('%cUnmount Screen: MapClipFeatureModal', Color.ConsoleInfo);
        };

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onFormFinish = (values: EValuesClipFeature) => {
        const inputLayer = props.viewer.profileTool.getLayerByIdLayer(values.baseId, false) as VectorLayer<VectorSource<any>>;

        if (!inputLayer) {
            return
        }

        inputLayer.getSource()!.getFeatures().forEach(feature => {
            if (feature.get('id') === values.clipId) {
                message.warning(t('message.clipBelongingSameLayer')).then(
                    () => removeAction(props.viewer)
                )
            }
        });

        onClipLayer({
            viewer: props.viewer,
            values: values,
            onStopProgress: props.onStopProgress,
        })
    }

    const onOk = () => {
        form
            .validateFields()
            .then((values) => {
                form.resetFields();

                onFormFinish(values);
            })
            .then(props.onClose)
    }

    props.viewer.profileTool.layer?.getLayers().forEach((layer: VectorLayer<VectorSource<Geometry>>) => {

        if (layer.get('typeDraw') === EDrawType.LineString || layer.get('type') === EDrawType.Point) {
            return
        }

        const children: BaseOptionType[] = [];
        layer.getSource()!.getFeatures().forEach((feature, index) => {
            children.push({
                title: feature.get('title') + ' ' + (index + 1),
                value: feature.get('id'),
                isLeaf: true,
            })
        })
        treeClipFeature.push({
            title: layer.get('title'),
            value: layer.get('id'),
            children: children,
            selectable: false,
        })
    });

    return (
        <Modal
            visible
            centered
            className={'modal-main ant-modal-v2d middle'}
            onCancel={props.onClose}
            closeIcon={<Image
                height={24}
                width={24}
                src={Images.iconXCircle.data}
                alt={Images.iconXCircle.atl}
            />}
            footer={
                <CustomButton
                    size={'small'}
                    usingFor={'geodetic'}
                    key={"save"}
                    onClick={onOk}
                >
                    {t("button.ok")}
                </CustomButton>
            }
            title={t('text.clipLayer')}
        >
            <Form
                className={scm.ModalForm}
                form={form}
                labelCol={{span: 8}}
                labelAlign={"left"}
            >
                <Form.Item
                    name="baseId"
                    label={t('text.profileLayer')}
                    rules={[{
                        required: true,
                        message: t('message.pleaseSpecifyValues', {
                            label: t("text.profileLayer").toLowerCase(),
                        })
                    }]}
                >
                    <Select
                        placeholder={t('text.specifyLayerClip')}
                        allowClear
                        className={'select-main select-v2d select-h-40 select-radius-4'}
                        dropdownClassName={'select-dropdown-v2d'}>
                        {
                            props.viewer.profileTool.layer
                                ? props.viewer.profileTool.layer.getLayers().getArray().map(layer => {
                                    return (
                                        <Select.Option
                                            key={layer.get('id')}
                                            value={layer.get('id')}
                                        >
                                            {layer.get('title')}
                                        </Select.Option>
                                    )
                                })
                                : <CommonEmptyFC/>
                        }
                    </Select>
                </Form.Item>
                <Form.Item
                    name="clipId"
                    label={t('text.boundsFeature')}
                    rules={[{
                        required: true,
                        message: t('message.pleaseSpecifyValues', {
                            label: t("text.boundsFeature").toLowerCase(),
                        }),
                    }]}
                >
                    <TreeSelect
                        className={'select-main select-v2d select-h-40 select-radius-4'}
                        dropdownClassName={'select-dropdown-v2d'}
                        bordered
                        dropdownStyle={{maxHeight: 400, overflow: 'auto'}}
                        treeData={treeClipFeature}
                        placeholder={t('text.specifyBounds')}
                        treeLine={{showLeafIcon: false}}
                        allowClear
                    />
                </Form.Item>
            </Form>
        </Modal>
    )
}

export const onClipLayer = (props: {
    viewer: Viewer,
    values: EValuesClipFeature,
    onStopProgress: () => void,
}) => {
    const baseProfile = props.viewer.profileTool.getLayerByIdLayer(props.values.baseId, true) as CLProfile;
    const clipFeature = props.viewer.profileTool.getFeatureById(props.values.clipId);

    if (!clipFeature || !baseProfile) {
        return;
    }

    const baseLayer = baseProfile.layer;
    const clipLayer = new VectorLayer({
        source: new VectorSource({
            features: [
                clipFeature
            ]
        })
    });
    const sourceClipLayer = clipLayer.getSource();

    if (sourceClipLayer === null) {
        return
    }

    sourceClipLayer.on(MapEventName.AddFeature, function () {
        baseLayer.setExtent(sourceClipLayer.getExtent());
    });

    const styleContext = new Style({
        fill: new Fill({
            color: 'black',
        }),
    });

    baseLayer.on(MapEventName.PostRender, function (e) {
        const vectorContext = getVectorContext(e);

        (e.context as CanvasRenderingContext2D).globalCompositeOperation = 'destination-in';

        sourceClipLayer.forEachFeature(function (feature) {
            vectorContext.drawFeature(feature, styleContext);
        });

        (e.context as CanvasRenderingContext2D).globalCompositeOperation = 'source-over';
    });

    props.onStopProgress();
    updateView(props.viewer);
}
