import {Form, message, Modal, TreeSelect} from "antd";
import {Viewer} from "../../viewer/Viewer";
import {Geometry, LineString, Polygon} from "ol/geom";
import {EDrawType, EProfileType} from "../../const/Defines";
import React, {RefObject, useEffect} from "react";
import {Color} from "../../../../../const/Color";
import {BaseOptionType} from "rc-select/es/Select";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import {Feature, MapBrowserEvent, Overlay} from "ol";
import {MapEventName} from "../../const/Event";
import {booleanWithin, difference, lineIntersect, lineOffset, lineString, lineToPolygon, point, polygon, Position, Properties} from "@turf/turf";
import {chunk} from "lodash";
import {Feature as FeatureTurf, Polygon as PolyTurf} from "@turf/helpers/dist/js/lib/geojson";
import {GeoJSON} from "ol/format";
import {CLProfile} from "../../viewer/clim/CLProfileTool";
import {updateView} from "./MapAction";
import {getTitleDrawProfile} from "../profile/GetTitleDrawPropfile";
import {TFunction, useTranslation} from "react-i18next";
import {CustomButton} from "../../../../components/CustomButton";
import Image from "next/image";
import {Images} from "../../../../../const/Images";
import sms from "../../styles/MapView.module.scss";

export type EValuesTrimFeature = {
    polygonId: string,
    lineId: string,
}

export const MapTrimFeatureModalFC = (props: {
    onClose: () => void,
    viewer: Viewer,
    popupRef: RefObject<HTMLElement>,
    onStopProgress: () => void,
}) => {
    const polygonTreeData: BaseOptionType[] = [];
    const lineTreeData: BaseOptionType[] = [];
    const [form] = Form.useForm();
    const {t} = useTranslation();

    useEffect(() => {
        console.log('%cMount Screen: MapTrimFeatureModal', Color.ConsoleInfo);

        return () => {
            console.log('%cUnmount Screen: MapTrimFeatureModal', Color.ConsoleInfo);
        };

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onOk = () => {
        form
            .validateFields()
            .then((values) => {
                onTrimFeature({
                    values: values,
                    viewer: props.viewer,
                    popupRef: props.popupRef,
                    t: t,
                    onStopProgress: props.onStopProgress
                });

                form.resetFields();
            })
            .then(props.onClose)
    }

    props.viewer.profileTool.layer?.getLayers().forEach((layer: VectorLayer<VectorSource<Geometry>>) => {
        const children: BaseOptionType[] = [];
        layer.getSource()!.getFeatures().forEach(feature => {
            children.push({
                title: feature.get('title'),
                value: feature.get('id'),
                isLeaf: true,
            })
        })

        switch (layer.get('typeDraw') as EDrawType) {
            case EDrawType.LineString:
                lineTreeData.push({
                    title: layer.get('title'),
                    value: layer.get('id'),
                    children: children,
                    selectable: false,
                })
                break;
            case EDrawType.Polygon:
                polygonTreeData.push({
                    title: layer.get('title'),
                    value: layer.get('id'),
                    children: children,
                    selectable: false,
                })
                break;
        }
    });

    return (
        <Modal
            className={'modal-main ant-modal-v2d middle'}
            centered
            visible
            onCancel={props.onClose}
            closeIcon={<Image
                height={24}
                width={24}
                src={Images.iconXCircle.data}
                alt={Images.iconXCircle.atl}
            />}
            footer={
                <CustomButton
                    size={'small'}
                    usingFor={'geodetic'}
                    key={"save"}
                    onClick={onOk}
                >
                    {t("button.ok")}
                </CustomButton>
            }
            title={t('text.trimFeature')}
        >
            <Form
                form={form}
                labelCol={{span: 8}}
                labelAlign={"left"}
            >
                <Form.Item
                    name="polygonId"
                    label={t('text.polygonTRIM')}
                    rules={[{
                        required: true,
                        message: t('message.pleaseSpecifyValues', {
                            label: t("text.polygonTRIM").toLowerCase(),
                        }),
                    }]}
                    className={'form-item'}
                >
                    <TreeSelect
                        className={'select-main select-v2d select-h-40 select-radius-4'}
                        dropdownClassName={'select-dropdown-v2d'}
                        bordered
                        dropdownStyle={{maxHeight: 400, overflow: 'auto'}}
                        treeData={polygonTreeData}
                        placeholder={t('text.specifyTargetTrim')}
                        treeLine={{showLeafIcon: false}}
                        allowClear
                    />
                </Form.Item>
                <Form.Item
                    name="lineId"
                    label={t('text.lineTRIM')}
                    rules={[{
                        required: true,
                        message: t('message.pleaseSpecifyValues', {
                            label: t("text.lineTRIM").toLowerCase(),
                        }),
                    }]}
                    className={'form-item'}
                    tooltip={t('text.lineStringIntersectWith2Points')}
                >
                    <TreeSelect
                        className={'select-main select-v2d select-h-40 select-radius-4'}
                        dropdownClassName={'select-dropdown-v2d'}
                        bordered
                        dropdownStyle={{maxHeight: 400, overflow: 'auto'}}
                        treeData={lineTreeData}
                        placeholder={t('text.specifyLineIntersect')}
                        treeLine={{showLeafIcon: false}}
                        allowClear
                    />
                </Form.Item>
            </Form>
        </Modal>
    )
}

export const onTrimFeature = (props: {
    viewer: Viewer,
    values: EValuesTrimFeature,
    popupRef: RefObject<HTMLElement>,
    t: TFunction<"translation">,
    onStopProgress: () => void,
}) => {
    const featurePoly = props.viewer.profileTool.getFeatureById(props.values.polygonId);
    const featureLine = props.viewer.profileTool.getFeatureById(props.values.lineId);
    const layerPoly = props.viewer.profileTool.getLayerByIdFeature(props.values.polygonId);

    const popup = new Overlay({
        element: props.popupRef.current!,
        positioning: 'bottom-center',
        stopEvent: false,
    });

    const setToolTip = (evt: MapBrowserEvent<UIEvent>) => {
        const position = evt.coordinate;

        if (position) {
            popup.setPosition(position);
        }

        props.popupRef.current!.innerHTML = `<div class="${sms.TipOverlay}">"${props.t('text.specifyPathAfterTrim')}"</div>`;
    }

    props.viewer.map.addOverlay(popup);
    props.viewer.map.on(MapEventName.PointerMove, setToolTip)

    if (!featurePoly || !featureLine || !featurePoly.getGeometry()) {
        return
    }
    const typePoly = featurePoly.getGeometry();

    if (!typePoly) {
        return
    }
    let polyGeom;
    const lineGeom = (featureLine as Feature<LineString>).getGeometry() as LineString;
    const lineTurf = lineString(chunk(lineGeom.getFlatCoordinates(), 2));

    const checkSectionGeom = (polyTurf: FeatureTurf<PolyTurf, Properties>) => {
        if ((booleanWithin(point(lineGeom.getFirstCoordinate()), polyTurf)) || (booleanWithin(point(lineGeom.getLastCoordinate()), polyTurf))) {
            message.error(props.t('message.featureMustHave2Intersect')).then();
            return
        }
        const intersectPoints = lineIntersect(lineTurf, polyTurf);

        if (intersectPoints.features.length === 0) {
            message.error(props.t('text.specifyLineIntersect')).then();
            return
        } else if (intersectPoints.features.length > 2) {
            message.error(props.t('message.featureMustHave2Intersect')).then();
            return
        } else {
            return intersectPoints;

        }
    }
    const offsetLine = [lineOffset(lineTurf, 0.001, {units: 'meters'}), lineOffset(lineTurf, -0.001, {units: 'meters'})]
    const polyCoords: Position[] = [];

    lineTurf.geometry.coordinates.forEach(item => polyCoords.push(item))

    polyCoords.push(offsetLine[0].geometry.coordinates[0]);
    polyCoords.push(offsetLine[1].geometry.coordinates[0]);
    polyCoords.push(lineTurf.geometry.coordinates[0]);

    const thickLineString = lineString(polyCoords);
    const thickLinePolygon = lineToPolygon(thickLineString);

    polyGeom = (featurePoly as Feature<Polygon>).getGeometry() as Polygon;

    if (!polyGeom || !lineGeom || !layerPoly) {
        return
    }
    const polyTurf = polygon([chunk(polyGeom.getFlatCoordinates(), 2) as Position[]]);

    checkSectionGeom(polyTurf);

    const clipped = difference(polyTurf, thickLinePolygon);
    const cutPolyGeom: Position[][][] = [];

    if (clipped) {
        for (let i = 0; i <= 1; i++) {
            const forSelect = (i + 1) % 2;

            clipped.geometry.coordinates.forEach(item => {

                const polyg = polygon(item as Position[][]);

                const intersect = lineIntersect(polyg, offsetLine[forSelect]);

                if (intersect.features.length > 0) {
                    cutPolyGeom.push(polyg.geometry.coordinates);
                }
            })
        }
        const functionTrimFeature = (evt: MapBrowserEvent<UIEvent>) => {

            props.popupRef.current!.innerHTML = '';
            props.viewer.map.un(MapEventName.PointerMove, setToolTip);

            cutPolyGeom.forEach(geom => {
                const polyCut = polygon(geom);

                if (booleanWithin(point(evt.coordinate), polyCut)) {

                    const feature = new GeoJSON().readFeature(polyCut);
                    const clonedProperties = JSON.parse(JSON.stringify(featurePoly.getProperties()));
                    delete clonedProperties.geometry;

                    feature.setStyle(featurePoly.getStyle());
                    feature.setProperties(clonedProperties);

                    props.viewer.profileTool.removeFeature({
                        id: featurePoly.get('id'),
                        feature: featurePoly,
                        layer: layerPoly,
                    } as CLProfile);

                    const sourceLayerPoly = layerPoly.getSource();

                    if (sourceLayerPoly !== null) {
                        sourceLayerPoly.addFeature(feature)

                        props.viewer.profileTool.addFeature({
                            feature: feature,
                            id: clonedProperties.id,
                            title: getTitleDrawProfile({
                                t: props.t,
                                isNote: true,
                                noteValues: featurePoly.get('title'),
                            }),
                            type: EProfileType.Draw,
                            typeDraw: typePoly.getType(),
                            layer: layerPoly,
                            init: true,
                        });

                        props.onStopProgress();
                        updateView(props.viewer);
                    }
                }
            })
        }
        props.viewer.map.on(MapEventName.SingleClick, functionTrimFeature)
    }
}