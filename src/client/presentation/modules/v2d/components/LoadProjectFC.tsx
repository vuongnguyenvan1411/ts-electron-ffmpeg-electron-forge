import React, {useCallback, useEffect} from "react";
import {Map2DModel} from "../../../../models/service/geodetic/Map2DModel";
import {TileImage, TileWMS} from "ol/source";
import {App} from "../const/App";
import {Size} from "ol/size";
import TileGrid from "ol/tilegrid/TileGrid";
import {Utils as RootUtils} from "../../../../core/Utils";
import {Feature, Tile} from "ol";
import {Point} from "ol/geom";
import {Icon, Style} from "ol/style";
import IconAnchorUnits from "ol/style/IconAnchorUnits";
import {MapEventName, ViewerEventName} from "../const/Event";
import {EContourMap, EDrawType} from "../const/Defines";
import EventEmitter from "eventemitter3";
import {getCenter} from "ol/extent";
import {Viewer} from "../viewer/Viewer";
import {Color} from "../../../../const/Color";
import {IOptCLTile} from "../viewer/clim/CLTileTool";
import {IOptCLContour} from "../viewer/clim/CLContourTool";
import {IOptCLMarker} from "../viewer/clim/CLMarkerTool";
import {MapLoadGeoJsonProfile} from "./profile/MapLoadGeoJsonProfile";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import {useTranslation} from "react-i18next";
import {IOptCLMeasure} from "../viewer/clim/CLMeasureTool";
import {StyleMeasureFn} from "./profile/StyleDrawFn";
import IconVr360 from "../../../../assets/image/v2d/icon_marker/5-b-1x.png";
import TileLayer from "ol/layer/Tile";

export const LoadProjectFC = React.memo((props: {
    item: Map2DModel,
    viewer: Viewer
    event: EventEmitter,
}) => {
    const {t} = useTranslation();

    useEffect(() => {
        console.log('%cMount FC: LoadProjectFC', Color.ConsoleInfo);

        initBaseMap();
        initTile();
        initContour();
        initMarker();
        initMeasure();
        initProfile();
        initShareRegion();

        props.event.emit(ViewerEventName.LoadProjectDone);

        return () => {
            console.log('%cUnmount FC: LoadProjectFC', Color.ConsoleInfo);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    //initBaseMap
    const initBaseMap = useCallback(() => {
        console.log('LoadProject: InitBaseMap');

        if (!props.viewer.baseMapTool.layer) {
            console.log("%cLayer BaseMap not initialized", Color.ConsoleError);

            return;
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    //initTile
    const initTile = useCallback(() => {
        console.log('LoadProject: InitTile');

        if (!props.viewer.tileTool.layer) {
            console.log("%cLayer Tile not initialized", Color.ConsoleError);

            return;
        }

        if (props.item.info?.tiles) {
            const listOpts: IOptCLTile[] = [];

            const listMinx: number[] = [];
            const listMiny: number[] = [];
            const listMaxx: number[] = [];
            const listMaxy: number[] = [];

            props.item.info?.tiles.forEach((item, index) => {
                let isRaster = false;
                let resolutions: number[] = [];

                if (item.tileSets !== undefined) {
                    if (item.tileSets.profile === 'raster') {
                        isRaster = true;
                    }

                    if (item.tileSets.tileSet!.length > 0) {
                        item.tileSets.tileSet!.forEach((item) => {
                            resolutions.push(item.unitsPerPixel!)
                        });
                    }
                }

                if (resolutions.length === 0) {
                    resolutions = App.defaultTileGridResolutions;
                }
                let tileSize: Size = App.defaultTileSize;

                if (item.tileFormat !== undefined && item.tileFormat.height !== undefined && item.tileFormat.width !== undefined) {
                    tileSize = [item.tileFormat.width, item.tileFormat.height];
                }

                let origin = App.defaultTileGridOrigin;
                let extent = App.defaultTileGridExtent;
                let center: number[] = [];

                if (
                    isRaster
                    && item.boundingBox !== undefined
                    && item.boundingBox.minx !== undefined
                    && item.boundingBox.miny !== undefined
                    && item.boundingBox.maxy !== undefined
                    && item.boundingBox.maxx !== undefined
                ) {
                    origin = [item.boundingBox.minx, item.boundingBox.miny];
                    extent = [
                        item.boundingBox.minx,
                        item.boundingBox.miny,
                        item.boundingBox.maxx,
                        item.boundingBox.maxy,
                    ];

                    center = getCenter(extent);

                    listMinx.push(item.boundingBox?.minx!);
                    listMiny.push(item.boundingBox?.miny!);
                    listMaxx.push(item.boundingBox?.maxx!);
                    listMaxy.push(item.boundingBox?.maxy!);
                }

                const tileLayer = new TileLayer({
                    className: "ol-layer",
                    // @ts-ignore
                    properties: {
                        id: item.id,
                        title: item.name,
                        center: center
                    },
                    useInterimTilesOnError: false,
                    minResolution: 0,
                    maxResolution: App.defaultMaxTileResolution,
                    // @ts-ignore
                    title: item.name,
                    preload: Infinity,
                    source: new TileImage({
                        attributions: '',
                        crossOrigin: 'anonymous',
                        projection: App.DefaultEPSG,
                        // @ts-ignore
                        minZoom: App.mapMinZoom,
                        maxZoom: App.mapMaxZoom,
                        tileGrid: new TileGrid({
                            extent: extent,
                            origin: origin,
                            resolutions: resolutions,
                            tileSize: tileSize,
                        }),
                        wrapX: true,
                        tilePixelRatio: 1,
                        tileUrlFunction: (tileCoord) => {
                            return RootUtils.cdnGdtAsset({
                                path: "tile",
                                data: {
                                    i: item.tile?.id!,
                                    p: {
                                        z: tileCoord[0],
                                        x: tileCoord[1],
                                        y: -1 - tileCoord[2]
                                    }
                                }
                            })
                        },
                        transition: 0,
                    })
                });

                listOpts.push({
                    id: item.id,
                    title: item.name ?? `Image ${index + 1}`,
                    object: tileLayer,
                })
            });

            props.viewer.tileTool.layer.set('center', [
                (RootUtils.arrayAvg(listMinx) + RootUtils.arrayAvg(listMaxx)) / 2,
                (RootUtils.arrayAvg(listMiny) + RootUtils.arrayAvg(listMaxy)) / 2
            ]);

            props.viewer.tileTool.setChild(listOpts, true, true);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const initContour = useCallback(() => {
        console.log('LoadProject: InitContour');

        if (!props.viewer.contourTool.layerGroup) {
            console.log("%cLayer Contour not initialized", Color.ConsoleError);

            return;
        }

        if (props.item.info?.contours) {
            const listOpts: IOptCLContour[] = [];

            //InitMapServer

            // const sourceWMSMapServer = new TileWMS({
            //     url: "http://123.30.234.99:8000/?map=/mapfiles/flamingo_tienyen.map&SERVICE=WMS",
            //     params: {
            //         LAYERS: 'flamingo_tienyen_line',
            //         TILED: true,
            //     },
            //     cacheSize: 0,
            //     projection: App.DefaultEPSG,
            //     transition: 0,
            //     serverType: 'mapserver',
            // });
            //
            // const tileLayer = new TileLayer({
            //     className: 'ContourItem',
            //     useInterimTilesOnError: false,
            //     // cacheSize: 0,
            //     // @ts-ignore
            //     properties: {
            //         id: '11',
            //         title: 'flamingo',
            //         // ws: item.ws,
            //         // layer: item.layer,
            //         type: EContourMap.WMS,
            //         name: 'coco_danang_ano'
            //         // center: [
            //         //     (item.boundingBox?.minx! + item.boundingBox?.maxx!) / 2,
            //         //     (item.boundingBox?.miny! + item.boundingBox?.maxy!) / 2
            //         // ]
            //         // TODO: vị trí center đang bị bay sang Lào
            //     },
            //     // extent: extent,
            //     // source: sourceWMS,
            //     source: sourceWMSMapServer,
            // });
            //
            // listOpts.push({
            //     id: '11',
            //     title: '11',
            //     object: tileLayer
            // })

            // const wfsLayer = new VectorLayer({
            //     className: EContourMap.WFS,
            //     style: new Style({
            //         fill: new Fill({
            //             color: "#000",
            //         }),
            //         stroke: new Stroke({
            //             color:"#000",
            //             width: 8,
            //         }),
            //         image: new CircleStyle({
            //             radius: 7,
            //             fill: new Fill({
            //                 color: "#000",
            //             }),
            //         }),
            //     }),
            //     properties: {
            //         id: EContourMap.WFS,
            //         type: EContourMap.WFS,
            //     },
            // })
            //
            // const wfsOpts: IOptCLContour = {
            //     id: EContourMap.WFS,
            //     title: EContourMap.WFS,
            //     object: wfsLayer,
            // }
            // props.viewer.contourTool.addChild(wfsOpts, true);


            props.item.info?.contours.forEach((item, index) => {
                const sourceWMS = new TileWMS({
                    url: item.ws ? '__OK__' : item.url!,
                    params: {
                        LAYERS: item.layer,
                    },
                    projection: App.DefaultEPSG,
                    transition: 0,
                    serverType: ( // geoserver
                        "g".toLowerCase()
                        + "e".toLowerCase()
                        + "o".toLowerCase()
                        + "s".toLowerCase()
                        + "e".toLowerCase()
                        + "r".toLowerCase()
                        + "v".toLowerCase()
                        + "e".toLowerCase()
                        + "r".toLowerCase()
                    ),
                    crossOrigin: '',
                    tileLoadFunction: (tile: Tile, src: string) => {
                        if (item.ws) {
                            const url = new URLSearchParams(src.replace('__OK__', ''));
                            const eu: any = {
                                ws: item.ws,
                                q: {}
                            };

                            url.forEach((value, key) => eu.q[key] = value);

                            (tile as any).getImage().setAttribute('src', RootUtils.cdnGdtAsset({
                                path: "gs",
                                data: eu
                            }));
                        }
                    }
                });

                if (
                    item.boundingBox !== undefined
                    && item.boundingBox.minx !== undefined
                    && item.boundingBox.miny !== undefined
                    && item.boundingBox.maxy !== undefined
                    && item.boundingBox.maxx !== undefined
                ) {
                    //INIT CONTOUR WMS
                    const tileLayer = new TileLayer({
                        className: 'ContourItem',
                        useInterimTilesOnError: false,
                        // cacheSize: 0,
                        // @ts-ignore
                        properties: {
                            id: item.id,
                            title: item.name,
                            ws: item.ws,
                            layer: item.layer,
                            type: EContourMap.WMS,
                            name: item.name
                            // center: [
                            //     (item.boundingBox?.minx! + item.boundingBox?.maxx!) / 2,
                            //     (item.boundingBox?.miny! + item.boundingBox?.maxy!) / 2
                            // ]
                            // TODO: vị trí center đang bị bay sang Lào
                        },
                        // extent: extent,
                        source: sourceWMS,
                    });

                    if (item.layer && item.name) {
                        listOpts.push({
                            id: item.id,
                            title: item.name ?? `Contour ${index + 1}`,
                            object: tileLayer
                        })
                    }
                }
            })

            props.viewer.contourTool.setChild(listOpts, true, true);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const initMarker = useCallback(() => {
        console.log('LoadProject: InitMarker');

        if (!props.viewer.markerTool.layer) {
            console.log("%cLayer Marker not initialized", Color.ConsoleError);

            return;
        }

        if (props.item.info?.markers) {
            const listOpts: IOptCLMarker[] = [];

            props.item.info?.markers.forEach((item, index) => {
                const iconFeature = new Feature({
                    geometry: new Point([item.data?.coord?.lon!, item.data?.coord?.lat!]),
                    name: `Marker${item.type ?? 'NaN'}`,
                    type: 'vr360',
                    population: 4000,
                    rainfall: 500,
                    id: item.id,
                    title: item.data?.title,
                    properties: item
                });
                const iconStyle = new Style({
                    image: new Icon({
                        crossOrigin: 'anonymous',
                        anchor: [0.5, 46],
                        anchorXUnits: IconAnchorUnits.FRACTION,
                        anchorYUnits: IconAnchorUnits.PIXELS,
                        opacity: 0.85,
                        src: IconVr360.src,
                    }),
                });

                iconFeature.setStyle(iconStyle)

                listOpts.push({
                    id: item.id,
                    title: item.data?.title ?? `Marker ${index + 1}`,
                    object: iconFeature
                })
            });

            props.viewer.markerTool.setChild(listOpts, true, true);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const initMeasure = useCallback(() => {
        console.log('LoadProject: InitMeasure');

        if (!props.viewer.measureTool.layer) {
            console.log("%cLayer Measure not initialized", Color.ConsoleError);

            return;
        }
        const listOpts: IOptCLMeasure[] = [];

        const measureLayerOrigin = [
            new VectorLayer({
                className: 'measure-line',
                source: new VectorSource({}),
                properties: {
                    id: EDrawType.LineString,
                    title: t('text.measureLine')
                },
                visible: true,
                zIndex: 999,
                style: (feature: any) => StyleMeasureFn(feature, true, false, EDrawType.LineString),
            }),
            new VectorLayer({
                className: 'measure-area',
                source: new VectorSource({}),
                properties: {
                    id: EDrawType.Polygon,
                    title: t('text.measureArea')
                },
                visible: true,
                zIndex: 999,
                style: (feature: any) => StyleMeasureFn(feature, true, false, EDrawType.Polygon),
            }),
            new VectorLayer({
                className: 'measure-circle',
                source: new VectorSource({}),
                properties: {
                    id: EDrawType.Circle,
                    title: t('text.measureCircle')
                },
                visible: true,
                zIndex: 999,
                style: (feature: any) => StyleMeasureFn(feature, true, false, EDrawType.Circle),
            })
        ]

        measureLayerOrigin.forEach(item => {
            listOpts.push({
                id: item.get('id'),
                title: item.get('title'),
                layer: item,
                typeDraw: item.get('id') as EDrawType,
                init: true
            })
        })

        props.viewer.measureTool.setChild(listOpts, true, true)

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const initProfile = useCallback(() => {
        console.log('LoadProject: InitProfile');

        if (!props.viewer.profileTool.layer) {
            console.log("%cLayer Profile not initialized", Color.ConsoleError);

            return;
        }

        props.viewer.map.once(MapEventName.RenderComplete, () => {
            if (props.item.info && props.item.info.profile) {
                const data = props.item.info.profile;

                if (data !== undefined) {
                    MapLoadGeoJsonProfile({
                        data: data,
                        viewer: props.viewer
                    })
                }
            }
        })

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const initShareRegion = useCallback(() => {
        console.log('LoadProject: InitShareRegion');

        if (!props.viewer.shareTool.layer) {
            console.log("%cLayer ShareRegion not initialized", Color.ConsoleError);

            return;
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return null
});
