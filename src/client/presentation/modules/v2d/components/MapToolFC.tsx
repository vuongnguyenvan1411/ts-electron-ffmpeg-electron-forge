import React, {useEffect, useMemo, useRef, useState} from "react";
import {Feature, Geolocation, MapBrowserEvent, Overlay} from "ol";
import {useTranslation} from "react-i18next";
import {App, DetectMedia} from "../const/App";
import {Card, Input, InputRef, Menu, message, notification, Space} from "antd";
import {EContextMap, EDrawType, EEditType, EMapLayer, EMarkerType, EMenuMapItem, EProfileType, ESnapType, EToolMenuMap} from "../const/Defines";
import GeometryType from "ol/geom/GeometryType";
import {Draw, Snap} from "ol/interaction";
import {DrawEventName, MapEventName, ViewerEventName} from "../const/Event";
import {Coordinate, toStringHDMS} from "ol/coordinate";
import {Geometry, Point} from "ol/geom";
import {DrawEvent} from "ol/interaction/Draw";
import {Utils2D} from "../core/Utils";
import VectorLayer from "ol/layer/Vector";
import EventEmitter from "eventemitter3";
import VectorSource from "ol/source/Vector";
import {Fill, Stroke, Style} from "ol/style";
import CircleStyle from "ol/style/Circle";
import {Map2DModel, MapInfoLocationModel} from "../../../../models/service/geodetic/Map2DModel";
import {remove} from "lodash";
import IconNoStreetView from "../../../../assets/image/v2d/no_street_view_1x.png";
import {AxiosClient} from "../../../../repositories/AxiosClient";
import {ApiService} from "../../../../repositories/ApiService";
import {Viewer} from "../viewer/Viewer";
import {Color} from "../../../../const/Color";
import {StyleProfileFn} from "./profile/StyleDrawFn";
import {removeAction, updateView} from "./tool/MapAction";
import {SnapInteraction} from "./tool/SnapInteraction";
import {Images} from "../../../../const/Images";
import Image from "next/image";
import {useNavigate} from "react-router";
import {CustomTypography} from "../../../components/CustomTypography";
import sms from "../styles/MapView.module.scss";
import ReactDOM from "react-dom";
import {RecoilBridge, useRecoilBridgeAcrossReactRoots_UNSTABLE} from "recoil";
import {Utils} from "../../../../core/Utils";
import {KeyEventName} from "../../pc3d/event/EventNames";
import {ItemType} from "antd/es/menu/hooks/useItems";
import {MeasureInteraction} from "./measure/MeasureInteraction";
import TileLayer from "ol/layer/Tile";
import {getTitleDrawProfile} from "./profile/GetTitleDrawPropfile";
import {IOptCLProfile} from "../viewer/clim/CLProfileTool";
import {TFormMarkerValues} from "../viewer/clim/CLTypeTool";
import {MapTrimFeatureModalFC} from "./tool/MapTrimFeatureModalFC";
import {MapClipFeatureModalFC} from "./tool/MapClipFeatureModalFC";
import {TileImage} from "ol/source";
import {unByKey} from "ol/Observable";
import {getVectorContext} from "ol/render";
import {easeOut} from "ol/easing";
import {CustomButton} from "../../../components/CustomButton";
import {CloneFeatures} from "./tool/CloneFeatures";
import {RotateFeatures} from "./tool/RotateFeatures";
import {CreateProfileLayerModalFC} from "./profile/CreateProfileLayerModalFC";
import {Map2DShareModalFC} from "./Map2DShareModalFC";
import {DrawShareRegion} from "./profile/DrawShareRegion";
import {MapCaptureModalFC} from "./tool/MapCaptureModalFC";
import {EditorSaveModal} from "./profile/ProfileSaveModalFC";
import {OptionsMenu} from "../const/OptionsMenu";
import {FeatureSingleton} from "../../../../models/FeatureSingleton";
import {Vr360ModalFC} from "./Vr360ModalFC";
import Map from "ol/Map";
import RenderFeature from "ol/render/Feature";
import {MarkerViewerFC} from "./profile/MarkerViewerFC";
import {onUploadKMLFile} from "./tool/UploadKMZFile";

export const MapToolFC = React.memo((props: {
        viewer: Viewer,
        event: EventEmitter,
        m2dId: number,
        item: Map2DModel,
        isResponsiveLayer: boolean,
        onCloseMapLayer: () => void,
        onOpenMapLayer: () => void,
    }) => {
        const {t} = useTranslation();
        const navigate = useNavigate();

        const popupContextRef = useRef<HTMLDivElement>(null);
        const popupMarkerRef = useRef<HTMLDivElement>(null);
        const popupRef = useRef<HTMLDivElement>(null);
        const trackLocationRef = useRef<Geolocation>();
        const importKML = useRef<InputRef>(null);

        const [activeHeaderTool, setActiveHeaderTool] = useState<EToolMenuMap>();
        const [isMapCaptureModalVisible, setIsMapCaptureModalVisible] = useState(false);
        const [isSaveProfileVisible, setIsSaveProfileVisible] = useState(false);
        const [isShare2dModalVisible, setIsShare2dModalVisible] = useState(false);
        const [isPopupContextVisible, setIsPopupContextVisible] = useState(false);
        const [isPopupMarkerVisible, setIsPopupMarkerVisible] = useState(false);
        const [isCopyOk, setIsCopyOk] = useState(false);
        const [isMapRotate, setIsMapRotate] = useState(false);
        const [snapToolType, setSnapToolType] = useState<ESnapType>();
        const [isDrawing, setIsDrawing] = useState<boolean>(false);
        const [isViewMarker, setIsViewMarker] = useState<{
            feature: RenderFeature | Feature<Geometry>,
            type: string,
        }>();
        const [editToolType, setEditToolType] = useState<{
            visible: boolean,
            tool?: EEditType
        }>({visible: false});

        const [isPModalVisible, setIsPModalVisible] = useState<{
            visible: boolean,
            pType?: EProfileType
        }>({visible: false});

        const modifyStyle = Utils2D.styleDrawModify();
        modifyStyle.getText().setText(t('text.dragToModify'));

        const removeAllAction = () => {
            removeAction(props.viewer, trackLocationRef)
            setEditToolType({visible: false, tool: undefined})
            setIsDrawing(false)

            if (trackLocationRef.current) {
                trackLocationRef.current.setTracking(false)
                props.viewer.map.getTargetElement().classList.remove(sms.SpinnerLoading)
            }
        }

        const eventESCRemove = (evt: KeyboardEvent) => {
            if (evt.code.toLowerCase() === "escape") {
                evt.preventDefault();
                removeAllAction();
                setActiveHeaderTool(undefined);
            }
        }

        const RecoilBridge = useRecoilBridgeAcrossReactRoots_UNSTABLE();

        const onResetRotateMap = () => {
            props.viewer.map.getView().setRotation(0);

            setIsMapRotate(false)
        };

        useEffect(() => {
            console.log('%cMount FC: MapToolFC', Color.ConsoleInfo);

            if (props.viewer.shareTool.layer) {
                props.viewer.shareTool.layer.setStyle((feature: any) => StyleProfileFn(props.viewer, feature, true))
            }

            document.addEventListener(KeyEventName.KeyDown, eventESCRemove);

            if (popupContextRef.current) {
                const divPopup = document.getElementById('popupContext');

                if (divPopup && FeatureSingleton.getInstance().isContributor) {
                    ReactDOM.render(
                        <RecoilBridge>
                            <Menu
                                className={'menu-v2d menu-context'}
                                id={'menu-context'}
                                selectedKeys={[]}
                                items={OptionsMenu.itemsContext(t)}
                                onClick={({key}) => {
                                    const overlay = props.viewer.map.getOverlayById('popup-context')
                                    const overlayPosition = overlay.getPosition() as Coordinate;
                                    const resetOverlay = () => overlay.setPosition(undefined);

                                    switch (key as EContextMap) {
                                        case EContextMap.Refresh:
                                            updateView(props.viewer);
                                            removeAllAction();
                                            setActiveHeaderTool(undefined);

                                            props.viewer.map.getAllLayers().forEach(layer => {
                                                if (layer instanceof TileLayer) layer.getSource()!.refresh()
                                            })

                                            break;
                                        case EContextMap.Copy:
                                            if (overlayPosition && overlayPosition.length > 0) {
                                                Utils.copyClipboard(overlayPosition.toString()).then(() => {
                                                    notification.success({
                                                        className: "notification-main with-icon",
                                                        message: <CustomTypography textStyle={"text-12-18"} isStrong>
                                                            {t('text.successCopy')}
                                                        </CustomTypography>,
                                                        description: <CustomTypography textStyle={"text-12-18"} isStrong>
                                                            {overlayPosition.toString()}
                                                        </CustomTypography>,
                                                    });
                                                });
                                            }

                                            break;
                                        case EContextMap.Center:
                                            if (overlayPosition) {
                                                props.viewer.map.getView().setCenter(overlayPosition)
                                            }

                                            break;
                                        case EContextMap.Marker:
                                            addQContextMarker(overlayPosition)

                                            break;
                                    }
                                    resetOverlay();
                                }}
                            />
                        </RecoilBridge>,
                        divPopup
                    );
                }
            }

            //context-menu
            const getContextMenu = (evt: MouseEvent) => {
                evt.preventDefault();

                const position = props.viewer.map.getEventCoordinate(evt);

                const popup = new Overlay({
                    element: popupContextRef.current!,
                    positioning: 'bottom-right',
                    stopEvent: true,
                    id: 'popup-context',
                    position: undefined
                });

                if (position) {
                    setIsPopupContextVisible(true)
                    popup.setPosition(position);
                }
                props.viewer.map.addOverlay(popup);
            }

            props.viewer.map.getViewport().addEventListener(MapEventName.Contextmenu, getContextMenu);

            props.viewer.map.on(MapEventName.MoveEnd, () => {
                const mapRotate = props.viewer.map.getView().getRotation();

                if (mapRotate !== 0) {
                    setIsMapRotate(true)
                }
            })

            //drawing interaction
            props.viewer.map.on(MapEventName.RenderComplete, function () {
                if (!isDrawing) {
                    props.viewer.map.getInteractions().forEach(interaction => {
                        if (interaction instanceof Draw) {
                            setIsDrawing(true)
                        }
                    })

                    if (trackLocationRef.current?.getTracking()) {
                        setIsDrawing(true)
                    }
                }
            })

            //marker interaction
            props.viewer.map.on(MapEventName.Click, onClickMarker);
            props.viewer.map.on(MapEventName.PointerMove, onHoverMarker);

            return () => {
                console.log('%cUnmount FC: MapToolFC', Color.ConsoleInfo);

                removeAllAction();
                document.removeEventListener(KeyEventName.KeyDown, eventESCRemove);
                props.viewer.map.getViewport().removeEventListener(MapEventName.Contextmenu, getContextMenu);

                props.viewer.map.un(MapEventName.Click, onClickMarker);
                props.viewer.map.un(MapEventName.PointerMove, onHoverMarker);
            }

            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, []);

        useEffect(() => {
            if (!snapToolType) {
                return
            }
            const mapInteraction = props.viewer.map.getInteractions().getArray()

            remove(mapInteraction, e => {
                return (e instanceof Snap)
            })

            SnapInteraction({
                viewer: props.viewer,
                format: snapToolType,
            });

            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [snapToolType])

        const animatedFeature = (feature: Feature<any>) => {
            const start = Date.now();
            const flashGeom = (feature.getGeometry() as Point).clone();
            const activeLayer = props.viewer.baseMapTool.getActiveLayer() as TileLayer<TileImage> | undefined;

            if (!activeLayer) {
                return
            }

            const listenerKey = activeLayer.on(MapEventName.PostRender, function (event) {
                const frameState = event.frameState;

                if (frameState) {
                    const elapsed = frameState.time - start;
                    const duration = 3000;
                    if (elapsed >= duration) {
                        unByKey(listenerKey);
                        return;
                    }
                    const vectorContext = getVectorContext(event);
                    const elapsedRatio = elapsed / duration;
                    const radius = easeOut(elapsedRatio) * 25 + 5;
                    const opacity = easeOut(1 - elapsedRatio);

                    const style = new Style({
                        image: new CircleStyle({
                            radius: radius,
                            stroke: new Stroke({
                                color: 'rgba(235, 87, 87, ' + opacity + ')',
                                width: 0.25 + opacity,
                            }),
                        }),
                    });

                    vectorContext.setStyle(style);
                    vectorContext.drawGeometry(flashGeom);
                    props.viewer.map.render();
                }
            });
        }

        const onClickShowLocation = () => {
            removeAllAction();

            const source = new VectorSource();
            const vector = new VectorLayer({
                source: source,
                style: new Style({
                    fill: new Fill({
                        color: 'rgba(235, 87, 87, 0.2)',
                    }),
                    stroke: new Stroke({
                        color: '#EB5757',
                        width: 2,
                    }),
                    image: new CircleStyle({
                        radius: 7,
                        fill: new Fill({
                            color: '#EB5757',
                        }),
                    }),
                }),
                properties: {
                    title: t('text.showLocation'),
                    id: EMapLayer.ShowLocation
                },
            });

            source.on(MapEventName.AddFeature, function (e) {
                if (!e.feature) {
                    return
                }
                animatedFeature(e.feature);
            });

            const draw = new Draw({
                source: source,
                type: GeometryType.POINT,
                style: Utils2D.styleImageCursor,
            });
            draw.on(DrawEventName.DrawStart, () => source.clear());

            draw.on(DrawEventName.DrawEnd, async (evt: DrawEvent) => {
                draw.setActive(false);
                notification.destroy();

                const coordinate = (evt.feature.getGeometry() as Point).getFlatCoordinates();
                const hdms = toStringHDMS(coordinate);
                let dataSearch: any;

                await AxiosClient
                    .get(ApiService.mapGetInfoLocation, {
                        lat: coordinate[1],
                        lon: coordinate[0],
                    })
                    .then(r => {
                        if (r.success && r.data) {
                            dataSearch = new MapInfoLocationModel(r.data);
                        }
                    });

                draw.setActive(true);

                if (dataSearch && dataSearch instanceof MapInfoLocationModel && dataSearch.displayName) {
                    const split = dataSearch.displayName.split(', ');

                    const p1 = split[0];
                    remove(split, (value, idx) => {
                        if (idx === 0) {
                            return true;
                        } else {
                            return /^\d+$/.test(value);
                        }
                    });
                    const p2 = split.join(' - ');

                    notification.open({
                        message: null,
                        closeIcon: <Image
                            height={16}
                            width={16}
                            src={Images.iconXCircle.data}
                            alt={Images.iconXCircle.atl}
                        />,
                        description: (
                            <Card.Meta
                                className={'card-meta-v2d'}
                                avatar={
                                    <Image
                                        src={IconNoStreetView.src}
                                        alt={"show_location"}
                                        width={'80%'}
                                        height={'80%'}
                                    />
                                }
                                title={
                                    <CustomTypography
                                        textStyle={"text-14-18"}
                                        isStrong
                                    >
                                        {p1}
                                    </CustomTypography>
                                }
                                description={
                                    <Space
                                        direction="vertical"
                                        size={0}
                                    >
                                        <CustomTypography textStyle={"text-12-18"}>
                                            {p2}
                                        </CustomTypography>
                                        <div>
                                            <CustomTypography textStyle={"text-12-18"}>
                                                {t('text.coords')} {hdms}
                                            </CustomTypography>
                                            <CustomButton
                                                onClick={() => {
                                                    Utils.copyClipboard(coordinate.toString()).then(() => {
                                                        setIsCopyOk(true);

                                                        notification.success({
                                                            className: "notification-main with-icon",
                                                            message: <CustomTypography textStyle={"text-12-18"} isStrong>
                                                                {t('text.successCopy')}
                                                            </CustomTypography>,
                                                            description: <CustomTypography textStyle={"text-12-18"}>
                                                                {t('text.coords')} &nbsp; {coordinate}
                                                            </CustomTypography>,
                                                        });

                                                        setTimeout(_ => setIsCopyOk(false), App.TimeoutHideCopy)
                                                    })
                                                }}
                                                type={"text"}
                                            >
                                                <Image
                                                    src={Images.iconCopy.data}
                                                    alt={Images.iconCopy.atl}
                                                    width={12}
                                                    height={12}
                                                />
                                            </CustomButton>
                                        </div>
                                    </Space>
                                }
                            />
                        ),
                        duration: 3,
                        placement: "bottomRight",
                        className: "notification-main",
                        style: {
                            padding: "0.4rem"
                        }
                    });
                } else {
                    notification.open({
                        message: (
                            <Space>
                                <CustomTypography
                                    textStyle={"text-title-v2d-highlight-regular"}
                                >
                                    {t('text.coords')} {hdms}
                                </CustomTypography>
                                <CustomButton
                                    onClick={() => {
                                        Utils.copyClipboard(coordinate.toString()).then(() => {
                                            setIsCopyOk(true);

                                            notification.success({
                                                className: "notification-main with-icon",
                                                message: <CustomTypography textStyle={"text-12-18"} isStrong>
                                                    {t('text.successCopy')}
                                                </CustomTypography>,
                                                description: <CustomTypography textStyle={"text-12-18"}>
                                                    {t('text.coords')} &nbsp; {coordinate}
                                                </CustomTypography>,
                                            });

                                            if (isCopyOk) {
                                                setTimeout(_ => setIsCopyOk(false), App.TimeoutHideCopy)
                                            }

                                        })
                                    }}
                                    type={"text"}
                                >
                                    <Image
                                        src={Images.iconCopy.data}
                                        alt={Images.iconCopy.atl}
                                        width={12}
                                        height={12}
                                    />
                                </CustomButton>
                            </Space>
                        ),
                        duration: 3,
                        placement: "bottomRight",
                        className: "notification-main",
                    });
                }
            })

            props.viewer.map.addInteraction(draw);
            props.viewer.map.addLayer(vector);
        }

        const onClickTrackLocation = () => {
            props.viewer.map.getTargetElement().classList.add(sms.SpinnerLoading)

            if (trackLocationRef.current) {
                trackLocationRef.current.setTracking(true);
            } else {
                const gL = new Geolocation({
                    // enableHighAccuracy must be set to true to have the heading value.
                    trackingOptions: {
                        enableHighAccuracy: true,
                    },
                    projection: props.viewer.map.getView().getProjection(),
                });

                const vector = new VectorLayer({
                    source: new VectorSource(),
                    properties: {
                        title: t('text.trackLocation'),
                        id: EMapLayer.TrackLocation
                    },
                });

                props.viewer.map.addLayer(vector);

                gL.setTracking(true);

                const onGLChange = () => {
                    const coordinate = gL.getPosition();
                    const vectorSource = vector.getSource();

                    if (!coordinate || !vectorSource) {
                        return;
                    }

                    vectorSource.on(MapEventName.AddFeature, function (e) {
                        if (!e.feature) {
                            return
                        }
                        animatedFeature(e.feature);

                        if (props.viewer.map.getTargetElement().classList.contains(sms.SpinnerLoading))
                            props.viewer.map.getTargetElement().classList.remove(sms.SpinnerLoading)
                    });

                    console.log('TrackLocation');
                    console.log('coordinate:', coordinate);
                    console.log('accuracy:', `${gL.getAccuracy()} m`);
                    console.log('altitude:', `${gL.getAltitude()} m`);
                    console.log('speed:', `${gL.getSpeed()} m/s`);
                    console.log('heading:', `${gL.getHeading()} rad`);
                    console.log('-------------');

                    props.viewer.map.getView().animate({
                        center: coordinate,
                        duration: 1000,
                        zoom: props.viewer.map.getView().getZoom(),
                    });
                    vectorSource.clear();

                    const positionFeature = new Feature();
                    positionFeature.setStyle(Utils2D.styleTrackLocationCursor());
                    positionFeature.setGeometry(new Point(coordinate));
                    (positionFeature.getStyle() as Style).getText().setText(toStringHDMS(coordinate));

                    vectorSource.addFeature(positionFeature);

                    props.viewer.map.getInteractions().forEach(interaction => {
                        if (interaction instanceof Draw && trackLocationRef.current) {
                            trackLocationRef.current.setTracking(false);
                        }
                    })
                }
                const onError = (error) => {
                    props.viewer.map.getTargetElement().classList.remove(sms.SpinnerLoading)

                    if (error.code == GeolocationPositionError.PERMISSION_DENIED) {
                        message.error(t('message.permissionLocation')).then();

                        return;
                    } else if (error.code == GeolocationPositionError.TIMEOUT) {
                        message.error(t('message.confirmError')).then();

                        return;
                    } else if (error.code == GeolocationPositionError.POSITION_UNAVAILABLE) {
                        message.error(t('text.noData')).then();

                        return;
                    }
                }

                gL.on('change', onGLChange);
                gL.on('error', onError);

                trackLocationRef.current = gL;
            }
        }

        const addQContextMarker = (coords: Coordinate) => {
            const id = `context_${EProfileType.Marker}`;
            const typeDraw = EDrawType.Point;

            const initValues = {
                title: '',
                coordinate: coords,
            } as TFormMarkerValues;

            const titleProfile = getTitleDrawProfile({
                t: t,
                typeDraw: typeDraw,
                isNote: true,
                typeProfile: EProfileType.Marker,
            });

            const vectorLayer = new VectorLayer({
                source: new VectorSource(),
                properties: {
                    id: id,
                    title: titleProfile,
                    typeDraw: typeDraw,
                    type: EProfileType.Marker,
                }
            });

            const listOpts: IOptCLProfile[] = [
                {
                    id: id,
                    title: titleProfile,
                    type: EProfileType.Marker,
                    typeDraw: typeDraw,
                    layer: vectorLayer,
                    init: false
                }
            ]
            props.viewer.profileTool.setChild(listOpts, false, true, true, initValues)
        }

        const onClickSaveProfile = () => {
            setIsSaveProfileVisible(true);
        }

        const onCloseSaveProfile = () => {
            setIsSaveProfileVisible(false);
            setActiveHeaderTool(undefined)
        }

        const onCloseShareProfile = () => {
            setIsShare2dModalVisible(false);
            setActiveHeaderTool(undefined)
        }


        const getMapTitle = () => {
            if (!props.item.name) {
                return null
            }

            const indexHyphen = props.item.name.indexOf('-');

            if (indexHyphen === -1) {
                return (
                    <CustomTypography
                        textStyle={"text-title-header-v2d"}
                        isStrong
                    >
                        {props.item.name}
                    </CustomTypography>
                )
            } else {
                return (
                    <>
                        <CustomTypography
                            textStyle={"text-title-header-v2d"}
                            isStrong
                        >
                            {props.item.name.slice(0, indexHyphen)}
                        </CustomTypography>
                        <CustomTypography
                            textStyle={"text-title-header-v2d"}
                        >
                            {'/' + props.item.name.slice(indexHyphen + 1)}
                        </CustomTypography>
                    </>
                )
            }
        };

        const onClickMenuTools = (evt: any) => {
            if (DetectMedia() && evt.keyPath[0]) {
                props.event.emit(ViewerEventName.DrawerClosed);
            }

            if (evt.keyPath && evt.keyPath.length > 1) {
                const method = evt.keyPath[1] as EToolMenuMap;

                switch (method) {
                    case EToolMenuMap.Measure:
                        removeAllAction();
                        const typeDraw = evt.keyPath[0] as EDrawType;

                        if (!DetectMedia()) {
                            props.event.emit(ViewerEventName.DrawerOpened, EMenuMapItem.MeasureLayer);
                        }

                        MeasureInteraction({
                            format: typeDraw,
                            t: t,
                            viewer: props.viewer
                        });

                        break;
                    case EToolMenuMap.Profile:
                        removeAllAction();
                        const pType = evt.keyPath[0] as EProfileType;
                        props.event.emit(ViewerEventName.DrawerOpened, EMenuMapItem.ProfileLayer);

                        setIsPModalVisible({
                            visible: true,
                            pType: pType,
                        });

                        break;
                    case EToolMenuMap.Tools:
                        setEditToolType({
                            visible: true,
                            tool: evt.keyPath[0] as EEditType,
                        });

                        break;
                    case EToolMenuMap.Snap:
                        setSnapToolType(evt.keyPath[0] as ESnapType);

                        break;
                }
            } else {
                removeAllAction();

                switch (evt.key as EToolMenuMap) {
                    case EToolMenuMap.Reset:
                        setActiveHeaderTool(undefined);
                        removeAllAction();

                        break;
                    case EToolMenuMap.Rotation:
                        onResetRotateMap();
                        removeAllAction();

                        break;
                    case EToolMenuMap.Contour:

                        props.event.emit(ViewerEventName.ContourChangeStatus);

                        break;
                    case EToolMenuMap.Capture:
                        setIsMapCaptureModalVisible(true);
                        setActiveHeaderTool(undefined);

                        break;
                    case EToolMenuMap.ShowLocation:
                        if (activeHeaderTool === EToolMenuMap.ShowLocation) {
                            removeAllAction();
                            setActiveHeaderTool(undefined);
                        } else {
                            setActiveHeaderTool(EToolMenuMap.ShowLocation)
                            onClickShowLocation();
                        }

                        break;
                    case EToolMenuMap.TrackLocation:
                        if (activeHeaderTool === EToolMenuMap.TrackLocation) {
                            removeAllAction();
                            setActiveHeaderTool(undefined);
                        } else {
                            removeAllAction();
                            setActiveHeaderTool(EToolMenuMap.TrackLocation)
                            onClickTrackLocation();
                        }

                        break;
                    case EToolMenuMap.Save:
                        setActiveHeaderTool(EToolMenuMap.Save);
                        onClickSaveProfile();

                        break;
                    case EToolMenuMap.Share:
                        removeAllAction();
                        setActiveHeaderTool(EToolMenuMap.Share);
                        setIsShare2dModalVisible(true);

                        break;
                    case EToolMenuMap.KMLFile:
                        if (importKML.current && importKML.current.input) importKML.current.input.click()

                        break;
                }
            }
        }

        const pathfinderDraw = () => {
            if (!editToolType) {
                return null
            }
            const onCloseModal = () => {
                setEditToolType({
                    visible: false
                });
            }

            switch (editToolType.tool) {
                case EEditType.Trim:
                    return <MapTrimFeatureModalFC
                        viewer={props.viewer}
                        onClose={onCloseModal}
                        popupRef={popupRef}
                        onStopProgress={removeAllAction}
                    />

                case EEditType.Clip:
                    return <MapClipFeatureModalFC
                        viewer={props.viewer}
                        onClose={onCloseModal}
                        onStopProgress={removeAllAction}
                    />

                case EEditType.Copy:
                    CloneFeatures({
                        viewer: props.viewer,
                        t: t,
                        isCut: false,
                        popupRef: popupRef,
                        onClose: removeAllAction,
                    })
                    break;

                case EEditType.Move:
                    CloneFeatures({
                        t: t,
                        viewer: props.viewer,
                        isCut: true,
                        popupRef: popupRef,
                        onClose: removeAllAction,
                    })
                    break;

                case EEditType.Rotate:
                    RotateFeatures({
                        t: t,
                        viewer: props.viewer,
                        onClose: removeAllAction,
                        modifyStyle: modifyStyle,
                        popupRef: popupRef,
                    })
                    break;
            }
        };

        const onHoverMarker = (evt: MapBrowserEvent<UIEvent>) => {
            if (evt.dragging) {
                return;
            }

            const feature = props.viewer.map.forEachFeatureAtPixel(evt.pixel, feature => feature);

            if (FeatureSingleton.getInstance().v2DShare && feature && feature.get('type') === 'annotation') {
                return;
            }

            const map = evt.target as Map;

            const pixel = map.getEventPixel(evt.originalEvent);
            const hit = map.hasFeatureAtPixel(pixel);

            map.getTargetElement().style.cursor = (hit ? 'pointer' : '');
        }

        const onClickMarker = (evt: MapBrowserEvent<UIEvent>) => {
            const popupOverlay = props.viewer.map.getOverlayById('popup-context');

            if (popupOverlay !== null) {
                popupOverlay.setPosition(undefined)
            }

            const feature = props.viewer.map.forEachFeatureAtPixel(evt.pixel, feature => feature);

            if (feature) {
                switch (feature.get('type')) {
                    case EMarkerType.Vr360:
                        setIsViewMarker({
                            ...isViewMarker,
                            feature: feature,
                            type: feature.get('type'),
                        })

                        break;

                    case EProfileType.Marker:
                        if (FeatureSingleton.getInstance().v2DShare) {
                            const hyperlink = feature.get('hyperlink');

                            if (hyperlink && hyperlink.link && hyperlink.target) {
                                if (hyperlink.link.length > 0 && hyperlink.target === 'out') {
                                    window.open(hyperlink.link, "_blank");
                                }
                            }
                        }

                        if (popupMarkerRef.current) {
                            const divPopup = document.getElementById('popupMarker');

                            if (divPopup) {
                                ReactDOM.render(
                                    <RecoilBridge>
                                        <MarkerViewerFC
                                            viewer={props.viewer}
                                            onClose={() => setIsViewMarker(undefined)}
                                            feature={feature}
                                            event={props.event}
                                        />
                                    </RecoilBridge>,
                                    divPopup
                                );
                            }
                        }

                        const popupOverlay = props.viewer.map.getOverlayById('popup-marker');

                        if (popupOverlay !== null) {
                            popupOverlay.setPosition(undefined)
                        }

                        const popup = new Overlay({
                            element: popupMarkerRef.current!,
                            positioning: 'bottom-right',
                            stopEvent: true,
                            id: 'popup-marker',
                            position: undefined
                        });

                        setIsPopupMarkerVisible(true)
                        popup.setPosition(evt.coordinate);

                        props.viewer.map.addOverlay(popup);

                        break;
                }
            } else {
                setIsPopupMarkerVisible(false)
            }
        }

        const getVr360Modal = useMemo(() => {
            if (isViewMarker && isViewMarker.type === EMarkerType.Vr360) {
                return (
                    <Vr360ModalFC
                        onCloseModalVr360={() => setIsViewMarker(undefined)}
                        feature={isViewMarker.feature}
                    />
                )
            }

            return null;

            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [isViewMarker]);

        return (
            <>
                <div
                    id={'popupContext'}
                    ref={popupContextRef}
                    style={{display: isPopupContextVisible ? 'block' : 'none'}}
                />
                <div
                    id={'popupMarker'}
                    className={sms.MarkerPopup}
                    ref={popupMarkerRef}
                    style={{display: isPopupMarkerVisible ? 'block' : 'none'}}
                />
                <div
                    id={'popup'}
                    ref={popupRef}
                />
                <Input
                    type={'file'}
                    ref={importKML}
                    style={{display: 'none'}}
                    onChange={(event) => {
                        onUploadKMLFile({
                            event,
                            t: t,
                            viewer: props.viewer
                        })
                    }}
                    accept={".kml, .geojson, .gpx, .igc, .json"}
                    multiple={false}
                />
                {getVr360Modal}
                <div className={'flex justify-items-center absolute top-0 right-0 left-0'}>
                    {
                        FeatureSingleton.getInstance().isContributor && (
                            <Menu
                                mode="horizontal"
                                className={'menu-v2d menu-header-v2d menu-info'}
                                items={OptionsMenu.menuInfoHeader({
                                    t: t,
                                    navigate: navigate,
                                    mapTitle: getMapTitle,
                                })}
                            />
                        )
                    }
                    <Menu
                        mode={DetectMedia() ? "vertical" : "horizontal"}
                        className={'menu-v2d menu-header-v2d menu-tool'}
                        onClick={onClickMenuTools}
                        selectedKeys={[activeHeaderTool as string]}
                        items={OptionsMenu.menuToolsHeader({
                            t: t,
                            viewer: props.viewer,
                            isDrawing: isDrawing
                        }) as ItemType[]}
                        triggerSubMenuAction={'click'}
                    />
                </div>
                <div id="popup" className="ol-popup">
                    <div id="popup-content"></div>
                </div>
                {
                    isMapCaptureModalVisible && (
                        <MapCaptureModalFC
                            viewer={props.viewer}
                            onClose={() => setIsMapCaptureModalVisible(false)}
                        />
                    )
                }
                {
                    isSaveProfileVisible && (
                        <EditorSaveModal
                            viewer={props.viewer}
                            onClose={onCloseSaveProfile}
                            isSaveFeatureVisible={isSaveProfileVisible}
                            m2dId={props.m2dId}
                        />
                    )
                }
                {
                    (isShare2dModalVisible && FeatureSingleton.getInstance().v2DProfile) && (
                        <Map2DShareModalFC
                            viewer={props.viewer}
                            onClose={onCloseShareProfile}
                            isShare2dModalVisible={isShare2dModalVisible}
                            drawShareRegion={() => {
                                DrawShareRegion({
                                    viewer: props.viewer,
                                    onCloseShareModal: onCloseShareProfile,
                                    t: t,
                                    event: props.event,
                                })
                            }}
                            m2dId={props.m2dId}
                        />
                    )
                }
                {
                    (isPModalVisible.visible && FeatureSingleton.getInstance().v2DProfile) && (
                        <CreateProfileLayerModalFC
                            viewer={props.viewer}
                            onClose={() => setIsPModalVisible({
                                visible: false
                            })}
                            event={props.event}
                            pType={isPModalVisible.pType}
                        />
                    )
                }
                {
                    (isMapRotate && !DetectMedia()) && (
                        <CustomButton
                            className={`btn-shortcut-v2d rotate`}
                            type={'outline'}
                            icon={<Image
                                height={24}
                                width={24}
                                src={Images.iconArrowUpRight2D.data}
                                alt={Images.iconArrowUpRight2D.atl}
                            />}
                            onClick={onResetRotateMap}
                        />
                    )
                }
                {pathfinderDraw()}
            </>
        )
    }
)
