import {Modal} from "antd";
import {lazy, Suspense, useEffect, useMemo, useState} from "react";
import {useTranslation} from "react-i18next";
import styles from "../styles/MapView.module.scss";
import {Feature} from "ol";
import RenderFeature from "ol/render/Feature";
import {Map2DMarkerModel} from "../../../../models/service/geodetic/Map2DModel";
import {CommonLoadingSpinFC} from "../../../widgets/CommonFC";
import {toStringHDMS} from "ol/coordinate";

const EmbedPano = lazy(() => import("../../../modules/vr360/EmbedPano"));

export const Vr360ModalFC = (props: {
    onCloseModalVr360: () => void,
    feature?: RenderFeature | Feature<any>,
}) => {
    const {t} = useTranslation();
    const properties: Map2DMarkerModel = props.feature?.get('properties');

    const [divPano, setDivPano] = useState<HTMLDivElement>();

    useEffect(() => {
        setDivPano(document.querySelector('#MIP') as HTMLDivElement);
    }, []);

    const getEmbedPano = useMemo(() => {
        if (divPano) {
            return (
                <Suspense fallback={<CommonLoadingSpinFC/>}>
                    <EmbedPano
                        vr360Id={parseInt(properties.data?.link?.vr360?.id ?? "0")}
                        name={properties.data?.link?.vr360?.name ?? ''}
                        scene={properties.data?.link?.vr360?.scene}
                        bodyRef={divPano}
                    />
                </Suspense>
            )
        }

        return null;

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [divPano]);

    return (
        <Modal
            className={styles.ModalHeader}
            visible={true}
            onOk={() => props.onCloseModalVr360()}
            onCancel={() => props.onCloseModalVr360()}
            width={'100%'}
            bodyStyle={{
                padding: 0,
                height: '88vh'
            }}
            title={toStringHDMS([properties.data?.coord?.lon!, properties.data?.coord?.lat!])}
            // closable={false}
            cancelButtonProps={{
                style: {
                    display: "none",
                },
            }}
            okText={t('button.close')}
            okButtonProps={{
                size: "small"
            }}
        >
            <div id={"MIP"} className={"w-full h-full"}>
                {getEmbedPano}
            </div>
        </Modal>
    )
}
