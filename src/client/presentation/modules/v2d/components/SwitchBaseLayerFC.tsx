import {Viewer} from "../viewer/Viewer";
import {CheckboxOptionType, Radio, RadioChangeEvent, Space} from "antd";
import {App, DetectMedia} from "../const/App";
import {useTranslation} from "react-i18next";
import {EBaseMap} from "../const/Defines";
import {useState} from "react";
import Image from "next/image";
import {CustomTypography} from "../../../components/CustomTypography";
import BaseLayer from "ol/layer/Base";

export const SwitchBaseLayerFC = (props: {
    viewer: Viewer
}) => {
    const {t} = useTranslation();
    const baseActiveLayer = props.viewer.baseMapTool.getActiveLayer();

    const [baseValue, setBaseValues] = useState<EBaseMap>(baseActiveLayer ? (baseActiveLayer as BaseLayer).get('id') as EBaseMap : EBaseMap.GoogleRoadmap)
    const swOpts: CheckboxOptionType[] = [];


    App.BaseLayerGroup.forEach(base => {
        const layerBaseId = base.get('id') as EBaseMap;
        let title;

        switch (layerBaseId) {
            case EBaseMap.None:
                title = t('text.noBaseMap')

                break;
            case EBaseMap.GoogleRoadmap:
                title = t('text.googleRoadmap')

                break;
            case EBaseMap.OpenStreetMap:
                title = t('text.openStreetMap')

                break;
            case EBaseMap.GoogleTerrain:
                title = t('text.googleTerrain');

                break;
            case EBaseMap.GoogleSatelliteOnly:
                title = t('text.googleSatelliteOnly');

                break;
        }

        swOpts.push({
            value: base.get('id') as EBaseMap,
            label: <Space>
                <Image
                    src={base.get('img').src}
                    alt={title}
                    width={64}
                    height={64}
                />
                <CustomTypography textStyle={!DetectMedia() ? 'text-16-24' : 'text-12-22'}>
                    {title}
                </CustomTypography>
            </Space>
        })
    })

    const onChangeBaseLayer = (e: RadioChangeEvent) => {
        setBaseValues(e.target.value);

        App.BaseLayerGroup.forEach(base => {
            base.setVisible(e.target.value === base.get('id'))
        })
    }

    return <Radio.Group
        className={'radio-main radio-v2d-main radio-switch-base radio-mb-3'}
        onChange={onChangeBaseLayer}
        value={baseValue}
        options={swOpts}
        optionType={"button"}
        defaultValue={baseValue}
    />
}