import {Viewer} from "../../viewer/Viewer";
import {EDrawType} from "../../const/Defines";
import GeometryType from "ol/geom/GeometryType";
import {Draw} from "ol/interaction";
import {StyleMeasureFn} from "../profile/StyleDrawFn";
import {DrawEventName, MapEventName} from "../../const/Event";
import {DrawEvent} from "ol/interaction/Draw";
import {PointerMoveHandler, removeAction, updateView} from "../tool/MapAction";
import {Feature} from "ol";
import {Circle, Geometry} from "ol/geom";
import {v4 as uuid} from "uuid";
import {TFunction} from "react-i18next";

export const MeasureInteraction = (props: {
    viewer: Viewer,
    format: EDrawType,
    t: TFunction<"translation">,
}) => {
    removeAction(props.viewer);

    const layerInteraction = props.viewer.measureTool.getLayerById(props.format);

    if (!props.viewer.measureTool.layer || !layerInteraction) {
        return;
    }

    let drawType = '';
    let activeTip = '';
    let title = '';

    if (props.format === EDrawType.Polygon) {
        drawType = GeometryType.POLYGON;
        activeTip = props.t('text.continuePolygonMsg');
        title = props.t('text.measureArea');

    } else if (props.format === EDrawType.LineString) {
        drawType = GeometryType.LINE_STRING;
        activeTip = props.t('text.continueLineMsg');
        title = props.t('text.measureLine');

    } else if (props.format === EDrawType.Circle) {
        drawType = GeometryType.CIRCLE;
        activeTip = props.t('text.continueCircleMsg');
        title = props.t('text.measureCircle');
    } else {
        return;
    }

    let tip = props.t('text.clickToStartDraw');

    const draw = new Draw({
        source: layerInteraction.getSource()!,
        type: drawType,
        style: (feature: any) => StyleMeasureFn(feature, true, false, drawType, tip),
    });

    draw.on(DrawEventName.DrawStart, () => {
        tip = activeTip;
    })

    draw.on(DrawEventName.DrawEnd, (evt: DrawEvent) => {
        props.viewer.map.once(MapEventName.PointerMove, PointerMoveHandler);

        const feature: Feature<Geometry> = evt.feature;
        const id = uuid();
        const featureProperties = {
            id: id,
            visible: true,
            title: title,
            typeDraw: props.format
        }

        if (feature.getGeometry()?.getType() === EDrawType.Circle) {
            const circle = feature.getGeometry() as Circle;

            if (circle.getCenter() && circle.getRadius()) {
                feature.set('circle', {
                    center: circle.getCenter(),
                    radius: circle.getRadius(),
                })
            }
        }

        feature.setProperties(featureProperties);

        tip = props.t('text.clickToStartDraw');

        props.viewer.measureTool.addFeature({
            id: id,
            title: title,
            typeDraw: props.format,
            layer: layerInteraction,
            feature: feature,
            init: false
        })
    })

    props.viewer.map.addInteraction(draw);

    updateView(props.viewer)
}