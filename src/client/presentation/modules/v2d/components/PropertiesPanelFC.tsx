import {Feature} from "ol";
import {Circle, Geometry, LineString, Point, Polygon} from "ol/geom";
import {Divider, notification, Space, Table} from "antd";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {farEdit} from "../../../../const/FontAwesome";
import {useTranslation} from "react-i18next";
import {Utils as RootUtils} from "../../../../core/Utils";
import {chunk, includes, remove} from "lodash";
import GeometryType from "ol/geom/GeometryType";
import {Utils2D} from "../core/Utils";
import {Viewer} from "../viewer/Viewer";
import {CLMeasure} from "../viewer/clim/CLMeasureTool";
import {CLProfile} from "../viewer/clim/CLProfileTool";
import {Breakpoint} from "antd/lib/_util/responsiveObserve";
import {TParamTableCoord} from "../const/Defines";
import {Draw, Modify, Snap} from "ol/interaction";
import React, {useEffect} from "react";
import VectorLayer from "ol/layer/Vector";
import Image from "next/image";
import {Images} from "../../../../const/Images";
import {CustomButton} from "../../../components/CustomButton";
import styles from "../styles/MapView.module.scss";
import {CustomTypography} from "../../../components/CustomTypography";

export const PropertiesPanelFC = (props: {
    viewer: Viewer,
    object: any,
}) => {

    if (props.object instanceof CLMeasure) {
        return <MeasurePanelFC {...props} />
    } else if (props.object instanceof CLProfile) {
        return <ProfilePanelFC {...props} />
    }

    return null;
}

const MeasurePanelFC = (props: {
    viewer: Viewer,
    object: CLMeasure,
}) => {
    const {t} = useTranslation();
    if (!props.object.feature) {
        return null
    }

    return (
        <Space
            direction={"vertical"}
            className={styles.MeasurePropertiesPanel}
            size={0}
        >
            <Divider
                orientation="left"
                plain
                style={{margin: 0}}
            >
                <Image
                    src={Images.iconLayerMeasure.data}
                    alt={Images.iconLayerMeasure.atl}
                    width={24}
                    height={24}
                />
                <CustomTypography
                    textStyle={"text-title-v2d-body-text-2-semibold"}
                    isStrong
                    color={"#FFFFFF"}
                >
                    {t('text.info')} - {props.object.title}
                </CustomTypography>
            </Divider>
            <CoordTable
                feature={props.object.feature}
                showTotal={true}
                viewer={props.viewer}
            />
        </Space>
    )
}

const ProfilePanelFC = (props: {
    viewer: Viewer,
    object: CLProfile,
}) => {
    const {t} = useTranslation();

    const feature = props.object.feature
    const layer = props.object.layer

    if (!feature) return null

    return (
        <div>
            <Divider
                orientation="left"
                plain
                style={{
                    marginTop: 0,
                    marginBottom: "0.5rem",
                }}
            >
                <FontAwesomeIcon icon={farEdit} className={"mr-1"}/>
                <CustomTypography
                    textStyle={"text-title-v2d-body-text-2-semibold"}
                    isStrong
                >
                    {t('text.info')} - {props.object.title}
                </CustomTypography>
            </Divider>
            <CoordTable
                feature={feature}
                showTotal={false}
                viewer={props.viewer}
                object={layer}
            />
        </div>
    )
}

export const CoordTable = (props: {
    feature: Feature<Geometry>,
    showTotal?: boolean,
    viewer: Viewer,
    object?: VectorLayer<any>
}) => {
    const {t} = useTranslation();

    const dataSource: TParamTableCoord[] = [];

    useEffect(() => {
        removeModify();

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.feature])

    const geometry = props.feature.getGeometry();

    if (!geometry) {
        return null;
    }

    // show list coord
    if (includes([GeometryType.LINE_STRING, GeometryType.POLYGON], geometry.getType())) {
        const geometry: LineString | Polygon = props.feature.getGeometry() as any;
        const flatCoordinates = chunk(geometry.getFlatCoordinates(), 2);

        flatCoordinates.forEach((item, index) => {
            dataSource.push({
                x: item[0],
                y: item[1],
                key: index,
            })
        })
    } else if (includes([GeometryType.GEOMETRY_COLLECTION, GeometryType.CIRCLE], geometry.getType())) {
        const center = props.feature.get('circle').center;

        dataSource.push({
            x: center[0],
            y: center[1],
            key: props.feature.get('id'),
        })
    } else {
        const center = (props.feature.getGeometry() as Point).getCoordinates();

        dataSource.push({
            x: center[0],
            y: center[1],
            key: props.feature.get('id'),
        })
    }

    let total: string | undefined;

    if (props.showTotal) {
        switch (geometry.getType()) {
            case GeometryType.LINE_STRING:
                total = Utils2D.formatLength(props.feature.getGeometry() as LineString);

                break;
            case GeometryType.POLYGON:
                total = Utils2D.formatArea(props.feature.getGeometry() as Polygon, {
                    symbol: "<sup>2</sup>",
                });

                break;
            case GeometryType.GEOMETRY_COLLECTION:
                if ((props.feature.getGeometry() as Circle).getRadius() !== undefined) {
                    total = Utils2D.formatCircle(props.feature.getGeometry() as Circle, {
                        symbol: "<sup>2</sup>",
                    });
                }

                break;
        }
    }

    const removeModify = () => {
        const mapInteraction = props.viewer.map.getInteractions().getArray()

        remove(mapInteraction, e => {
            return (e instanceof Snap || e instanceof Modify || e instanceof Draw)
        })
    }

    const columnInfo = [
        {
            title: <CustomTypography
                textStyle={"text-title-v2d-body-text-2-semibold"}
                isStrong
            >
                X
            </CustomTypography>,
            dataIndex: 'x',
            key: 'x',
            responsive: ['xxl', 'xl', 'lg', 'md', 'sm', 'xs'] as Breakpoint[],
            width: '40%'
        },
        {
            title: <CustomTypography
                textStyle={"text-title-v2d-body-text-2-semibold"}
                isStrong
            >
                Y
            </CustomTypography>,
            dataIndex: 'y',
            key: 'y',
            responsive: ['xxl', 'xl', 'lg', 'md', 'sm', 'xs'] as Breakpoint[],
            width: '40%'
        },
        {
            title: '',
            dataIndex: 'c',
            key: 'c',
            align: 'right' as 'right',
            render: (_: string, record: TParamTableCoord) => {
                return (
                    <CustomButton
                        usingFor={'geodetic'}
                        type={"text"}
                        onClick={() => {
                            const msg = `${record.x},${record.y}`;

                            RootUtils
                                .copyClipboard(msg)
                                .then(() => {
                                    notification.success({
                                        className: "notification-main with-icon",
                                        message: <CustomTypography textStyle={"text-12-18"} isStrong>
                                            {t('text.successCopy')}
                                        </CustomTypography>,
                                        description: <CustomTypography textStyle={"text-12-18"}>
                                            {msg}
                                        </CustomTypography>,
                                    });
                                });
                        }}
                    >
                        <Image
                            src={Images.iconCopy.data}
                            alt={Images.iconCopy.atl}
                            width={24}
                            height={24}
                        />
                    </CustomButton>
                )
            }
        }
    ];

    return (
        <>
            {
                dataSource.length > 0
                    ? <Table
                        className={styles.TableCoord}
                        dataSource={dataSource}
                        size="small"
                        bordered={false}
                        columns={columnInfo}
                        pagination={false}
                        footer={() => (
                            total
                                ? <div dangerouslySetInnerHTML={{
                                    __html: `${t('text.total')}: ${total}`
                                }}/>
                                : undefined
                        )}
                        scroll={{y: '30vh'}}
                    />
                    : null
            }
        </>
    )
}
