import {Card, Checkbox, Col, DatePicker, Divider, Form, Input, Modal, notification, Pagination, Radio, Space, Switch, Typography} from "antd";
import {useTranslation} from "react-i18next";
import React, {useCallback, useEffect, useRef, useState} from "react";
import {SendingStatus} from "../../../../const/Events";
import {PlusCircleOutlined, SettingOutlined, ShareAltOutlined} from "@ant-design/icons";
import moment from "moment";
import {Utils} from "../../../../core/Utils";
import {Color} from "../../../../const/Color";
import {App} from "../../../../const/App";
import {BaseShareModel, Map2DShareModel, TMap2DShareV0} from "../../../../models/ShareModel";
import {Globals} from "csstype";
import {RouteAction} from "../../../../const/RouteAction";
import {RouteConfig} from "../../../../config/RouteConfig";
import {Viewer} from "../viewer/Viewer";
import {useLocation, useNavigationType} from "react-router";
import {Map2DShareAction} from "../../../../recoil/service/geodetic/map_2d_share/Map2DShareAction";
import {Map2DProfileAction} from "../../../../recoil/service/geodetic/map_2d_profile/Map2DProfileAction";
import Image from "next/image";
import {Images} from "../../../../const/Images";
import {CustomButton} from "../../../components/CustomButton";
import {CustomTypography} from "../../../components/CustomTypography";
import styles from "../../../../styles/module/Share.module.scss";
import {ShareItemFC, ShareItemSkeleton} from "../../../widgets/ShareItemFC";
import {CommonEmptyFC} from "../../../widgets/CommonFC";
import {MediaQuery} from "../../../../core/MediaQuery";
import {Style} from "../../../../const/Style";
import {ErrorItemFC} from "../../../widgets/ErrorItemFC";
import {GetQrCodeModalFC} from "../../../widgets/GetQrCodeModalFC";

interface ModalFormMainProps {
    viewer: Viewer,
    m2dId: number,
    shareId?: string,
    visible: boolean,
    preview: boolean,
    onCancel: () => void,
    initFormValues?: Map2DShareModel,
    featureDraw?: number[][],
}

enum ShareRegion {
    Bounds,
    All
}

export const Map2DShareModalFC = React.memo((props: {
    viewer: Viewer,
    onClose: () => void,
    isShare2dModalVisible: boolean,
    drawShareRegion: () => void,
    m2dId: number,
}) => {
    const {t} = useTranslation()
    const navigateType = useNavigationType()
    const location = useLocation()

    const {
        vm,
        onLoadItems,
        onDeleteItem,
        onClearState,
    } = Map2DShareAction();

    const [formVisible, setFormVisible] = useState<{
        shareId?: string,
        visible: boolean,
        preview: boolean,
        initValues?: Map2DShareModel,
    }>({
        visible: false,
        preview: false,
    });
    const [visibleDelete, setVisibleDelete] = useState<{
        shareId?: string,
        visible: boolean,
    }>({
        visible: false,
    });
    const [isModalVisible, setIsModalVisible] = useState<{
        sid: string,
        visible: boolean,
    }>({
        sid: '',
        visible: false,
    });

    useEffect(() => {
        console.log('%cMount Screen: Map2DShareModalFC', Color.ConsoleInfo);

        if (
            vm.items === null || vm.key !== props.m2dId.toString()
            || (vm.timestamp !== undefined && Utils.checkHourState(vm.timestamp))
        ) {
            onLoadItems(props.m2dId, queryParams);
        }

        return () => {
            console.log('%cUnmount Screen: Map2DShareModalFC', Color.ConsoleInfo);
        };

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (vm.isUpdating === SendingStatus.success) {
            setFormVisible(
                {
                    ...formVisible,
                    visible: false,
                }
            )

            notification.success({
                className: "notification-main with-icon",
                message: <CustomTypography textStyle={"text-12-18"} isStrong>
                    {t('text.successSetting')}
                </CustomTypography>,
            });
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.isUpdating])

    useEffect(() => {
        return () => {
            if (
                navigateType === RouteAction.PUSH
                || navigateType === RouteAction.REPLACE
                || navigateType === RouteAction.POP
            ) {
                if (location.pathname.indexOf(RouteConfig.GEODETIC_VIEW_2D) !== 0) {
                    onClearState();
                }
            }
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [navigateType])

    useEffect(() => {
        if (vm.isDeleting === SendingStatus.success) {
            setVisibleDelete(
                {
                    ...visibleDelete,
                    visible: false,
                }
            )

            notification.success({
                className: "notification-main with-icon",
                message: <CustomTypography textStyle={"text-12-18"} isStrong>
                    {t('success.delete')}
                </CustomTypography>,
            });
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.isDeleting])

    const [, forceUpdate] = useState({})

    useEffect(() => {
        forceUpdate({});
    }, [])

    const onHandleCancelDelete = () => {
        setVisibleDelete({
            ...visibleDelete,
            visible: false,
        })
    }

    const onHandleOkDelete = useCallback(() => {
        if (visibleDelete.shareId) {
            onDeleteItem(props.m2dId, visibleDelete.shareId)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [visibleDelete, visibleDelete.shareId])

    const onShowDelete = (shareId: string) => {
        setVisibleDelete({
            ...visibleDelete,
            shareId: shareId,
            visible: true,
        })
    }

    const showUserModal = (shareId?: string) => {
        setFormVisible({
            ...formVisible,
            shareId: shareId,
            visible: true,
            preview: false,
            initValues: undefined,
        })
    }

    const hideUserModal = () => {
        setFormVisible({
            ...formVisible,
            visible: false,
            preview: false,
        })
    }

    const onPreviewRowTable = (item: Map2DShareModel, _: number) => {
        setFormVisible({
            ...formVisible,
            shareId: item.sid,
            visible: true,
            preview: true,
            initValues: item,
        })
    }

    const onEditTable = (item: Map2DShareModel, _: number) => {
        setFormVisible({
            ...formVisible,
            shareId: item.sid,
            visible: true,
            preview: false,
            initValues: item,
        })
    }


    const onClickGetQrCode = (item: BaseShareModel, _: number) => {
        setIsModalVisible({
            sid: item.sid,
            visible: true,
        });
    }

    const [queryParams, setQueryParams] = useState<{ page: number, limit: number }>({
        page: vm.query.page,
        limit: vm.query.limit,
    })

    const _getLoading = () => {
        return <>
            {
                Array((new MediaQuery(Style.Grid2dShareItemModal)).getPoint(2)).fill(0).map((_, i) => {
                    return <ShareItemSkeleton key={i}/>;
                })
            }
        </>
    }

    const onCopy = (item: Map2DShareModel, _: number) => {
        Utils.copyClipboard(item.link).then(() => {

            notification.success({
                className: "notification-main with-icon",
                message: <CustomTypography textStyle={"text-12-18"} isStrong>
                    {t('text.successCopy')}
                </CustomTypography>,
                description: <CustomTypography textStyle={"text-12-18"} isStrong>
                    {item.link}
                </CustomTypography>,
            });

        });
    }

    const onClickLink = (item: Map2DShareModel, _: number) => {
        window.open(item.link, '_blank');
    }

    const onDelete = (item: Map2DShareModel, _: number) => {
        onShowDelete(item.sid);
    }

    const onEdit = (item: Map2DShareModel, _: number) => {
        onEditTable(item, _);
    }

    const onView = (item: Map2DShareModel, _: number) => {
        onPreviewRowTable(item, _);
    }

    const isNextAvailable = vm.oMeta?.nextPage;

    const onChangePage = (page: number) => {
        if (vm.items && vm.items?.slice((page - 1) * vm.query.limit, page * vm.query.limit).length === 0) {
            onLoadItems(props.m2dId, {
                page: page,
                limit: queryParams.limit,
            })
        }
        setQueryParams({
            ...queryParams,
            page: page,
            limit: vm.query.limit,
        })

    }

    return (
        <>
            <Modal
                className={'modal-main ant-modal-v2d large p-body-1'}
                key={"modal-share-profile"}
                visible={props.isShare2dModalVisible}
                title={t('text.createLink')}
                closable
                centered
                closeIcon={<Image
                    height={24}
                    width={24}
                    src={Images.iconXCircle.data}
                    alt={Images.iconXCircle.atl}
                />}
                footer={null}
                onCancel={props.onClose}
            >
                <Space
                    className={"mb-6"}
                    direction={'horizontal'}
                    size={10}
                >
                    <CustomButton
                        size={'small'}
                        usingFor={'geodetic'}
                        key={"new-profile"}
                        onClick={() => showUserModal()}
                        icon={<PlusCircleOutlined/>}
                    >
                        {t("text.addSharingLink")}
                    </CustomButton>
                    <CustomButton
                        size={'small'}
                        usingFor={'geodetic'}
                        key={"share-region"}
                        onClick={props.drawShareRegion}
                        icon={<ShareAltOutlined/>}
                    >
                        {t("text.drawShareRegion")}
                    </CustomButton>
                </Space>
                <div className={`${styles.ShareContainer} ${styles.Map2DContainer}`}>
                    <ErrorItemFC
                        status={vm.isLoading}
                    >
                        {
                            vm.isLoading === SendingStatus.loading && isNextAvailable ?
                                <div
                                    className={styles.GridItems}>{_getLoading()}</div> : vm.items.length > 0 ?
                                    <div className={styles.GridItems}>
                                        {
                                            vm.items.slice((queryParams.page - 1) * vm.query.limit, queryParams.page * vm.query.limit).map((e, i) => {
                                                return (
                                                    <>
                                                        <ShareItemFC
                                                            key={i}
                                                            index={i}
                                                            item={e}
                                                            onQrCode={onClickGetQrCode}
                                                            onCopy={onCopy}
                                                            onLink={onClickLink}
                                                            onDelete={onDelete}
                                                            onEdit={onEdit}
                                                            onView={onView}
                                                        />
                                                        {
                                                            isModalVisible.visible
                                                                ? <GetQrCodeModalFC
                                                                    sid={isModalVisible.sid}
                                                                    name={e.name}
                                                                    onClose={() => setIsModalVisible({
                                                                        ...isModalVisible,
                                                                        visible: false
                                                                    })}
                                                                />
                                                                : null
                                                        }
                                                    </>
                                                )
                                            })
                                        }
                                    </div>
                                    : <CommonEmptyFC/>
                        }
                    </ErrorItemFC>
                    {
                        isNextAvailable ?
                            <div className={"flex w-full justify-center p-6"}>
                                <Pagination
                                    size={'small'}
                                    total={vm.oMeta?.totalCount ?? 0}
                                    defaultCurrent={1}
                                    onChange={(page) => onChangePage(page)}
                                    defaultPageSize={2}
                                />
                            </div>
                            : null
                    }
                </div>
            </Modal>
            <ModalShare2D
                key={"edit"}
                visible={formVisible.visible}
                shareId={formVisible.shareId}
                m2dId={props.m2dId}
                onCancel={hideUserModal}
                initFormValues={formVisible.initValues}
                preview={formVisible.preview}
                viewer={props.viewer}
            />
            <Modal
                className={'modal-main ant-modal-v2d'}
                key={"delete"}
                title={t("title.confirmBeforeDeleting")}
                visible={visibleDelete.visible}
                onCancel={onHandleCancelDelete}
                confirmLoading={vm.isDeleting === SendingStatus.loading}
                closeIcon={<Image
                    height={24}
                    width={24}
                    src={Images.iconXCircle.data}
                    alt={Images.iconXCircle.atl}
                />}
                footer={
                    [
                        <CustomButton
                            size={'small'}
                            usingFor={'geodetic'}
                            key={"ok-delete"}
                            onClick={onHandleOkDelete}
                            loading={vm.isDeleting === SendingStatus.loading}
                        >
                            {t("button.ok")}
                        </CustomButton>
                    ]
                }
            >
                {t("message.confirmBeforeDeleting")}
            </Modal>
        </>
    )
})

const ModalShare2D: React.FC<ModalFormMainProps> = ({
                                                        viewer,
                                                        preview,
                                                        shareId,
                                                        m2dId,
                                                        visible,
                                                        onCancel,
                                                        initFormValues,
                                                    }) => {
    const {t} = useTranslation();
    const [formMain] = Form.useForm();
    const [isPreview, setIsPreview] = useState(false);

    useEffect(() => {
        setIsPreview(preview);

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [preview])

    const [rdSPl, setRdSPl] = useState<boolean>(initFormValues?.config?.sPl ?? false)
    const [isTableShow, setIsTableShow] = useState<{
        visible: Globals | "collapse" | "hidden" | "visible",
        values?: number[][]
    }>({
        visible: 'hidden'
    });

    const listTilesRef = useRef<{ label: string, value: number }[]>([]);
    const listContoursRef = useRef<{ label: string, value: string }[]>([]);

    useEffect(() => {
        const tilesData: { label: string, value: number }[] = [];
        const contoursData: { label: string, value: string }[] = [];

        viewer.model.info?.tiles?.forEach(item => tilesData.push({
            label: item.name!,
            value: item.tile?.id!
        }))

        viewer.model.info?.contours?.forEach(item => contoursData.push({
            label: item.name!,
            value: item.layer!
        }))

        listTilesRef.current = tilesData;
        listContoursRef.current = contoursData;

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const {
        vm,
        onAddItem,
        onEditItem,
    } = Map2DShareAction()

    const {
        vm: vmProfile,
        onLoadItems: onLoadProfiles,
    } = Map2DProfileAction()

    useEffect(() => {
        if (
            vmProfile.items === null || vmProfile.key !== m2dId!.toString()
            || (vmProfile.timestamp !== undefined && Utils.checkHourState(vmProfile.timestamp))
        ) {
            onLoadProfiles(m2dId);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])


    const onAfterClose = () => {
        formMain.resetFields();
    }

    const onOk = () => {
        formMain.submit();
        setIsPreview(false);
    }

    const onChangeToEdit = () => {
        setIsPreview(false);
    }
    const shareBArea = initFormValues?.config?.bArea;

    const onFinish = (values: any) => {

        const shareRegionValues = (values.bArea === ShareRegion.Bounds && isTableShow.values) ? isTableShow.values : [];

        const serializable: TMap2DShareV0 = {
            name: values.name,
            password: values.password,
            dateStart: values['range-time-picker'] && values['range-time-picker'][0] ? moment(values['range-time-picker'][0]).format('YYYY-MM-DD HH:mm:ss').toString() : '',
            dateEnd: values['range-time-picker'] && values['range-time-picker'][1] ? moment(values['range-time-picker'][1]).format('YYYY-MM-DD HH:mm:ss').toString() : '',
            status: values.isShare,
            sInfo: values.shareInfo,
            config: {
                tiles: values.tiles,
                contours: values.contours,
                bArea: shareRegionValues,
                pId: values.pId,
                sPl: values.sPl ?? false,
            }
        }

        if (m2dId) {
            if (shareId) {
                onEditItem(m2dId, shareId, serializable)
            } else {
                onAddItem(m2dId, serializable)
            }
        }
    }

    const tableShareBArea = () => {
        const flatCoordinates = viewer.shareTool.getFlatCoordinates();

        if (flatCoordinates && flatCoordinates.length < 6) {
            notification.warning({
                className: "notification-main with-icon",
                message: <CustomTypography textStyle={"text-12-18"} isStrong>
                    {t('text.sharedPolygonAtLeast5Sides')}
                </CustomTypography>,
                description: <CustomTypography textStyle={"text-12-18"} isStrong>
                    {t('text.makeNewPolygon')}
                </CustomTypography>,
            });
            onCancel();
            setIsPreview(false);
        } else {
            setIsTableShow({
                visible: 'visible',
                values: flatCoordinates,
            });
        }
    }

    const defaultTilesValues = () => {
        const listTile: number[] = [];

        if (listTilesRef.current) {
            listTilesRef.current.forEach(item => {
                listTile.push(item.value)
            })
        }

        return listTile
    }

    const defaultContourValues = () => {
        const listContour: string[] = [];

        if (listContoursRef.current) {
            listContoursRef.current.forEach(item => {
                listContour.push(item.value)
            })
        }

        return listContour
    }

    return (
        <Modal
            className={'modal-main ant-modal-v2d middle padding-large'}
            closeIcon={<Image
                height={24}
                width={24}
                src={Images.iconXCircle.data}
                alt={Images.iconXCircle.atl}
            />}
            visible={visible}
            onCancel={() => {
                onCancel()
                setIsPreview(false)
            }}
            title={isPreview ? t("text.detailInfo") : !shareId ? t("text.createLink") : t("text.editLink")}
            onOk={onOk}
            afterClose={onAfterClose}
            destroyOnClose
            confirmLoading={vm.isLoading === SendingStatus.loading}
            footer={
                isPreview
                    ? <CustomButton
                        className={'icon-vertical-base'}
                        key={"edit-profile"}
                        onClick={onChangeToEdit}
                        usingFor={"geodetic"}
                    >
                        {t("button.edit")}
                    </CustomButton>
                    : <CustomButton
                        className={'icon-vertical-base'}
                        key={"save-as"}
                        onClick={() => onOk()}
                        usingFor={"geodetic"}
                    >
                        {shareId ? t("button.save") : t("button.saveNew")}
                    </CustomButton>
            }
        >
            <Form
                key={'form_share_m2d'}
                form={formMain}
                labelCol={{span: 8}}
                labelAlign={"left"}
                onFinish={onFinish}
            >
                <Form.Item
                    key={"k_name"}
                    label={t("text.name")}
                    name={"name"}
                    className={'form-item label-typography-font-14-line-18 margin-left-2 form-item-mb-3'}
                    rules={[
                        {
                            required: true,
                            message: t("validation.emptyData"),
                        },
                        {
                            max: 100,
                            message: t('validation.minAndMaxCharacter', {
                                label: t("text.name"),
                                min: '1',
                                max: '100'
                            }),
                        }
                    ]}
                    initialValue={initFormValues?.name}
                >
                    <Input
                        className={'input-main input-v2d input-h-40 input-bg-white input-radius-4'}
                        disabled={isPreview}
                        allowClear
                    />
                </Form.Item>
                <Form.Item
                    className={'form-item label-typography-font-14-line-18 form-item-mb-3'}
                    key={"k_password"}
                    label={t("label.password")}
                    name={"password"}
                    initialValue={initFormValues?.password}
                    rules={[
                        {
                            min: 6,
                            message: t('validation.password', {
                                label: t("label.password"),
                                min: '6',
                                max: '100'
                            }),
                        }
                    ]}
                >
                    <Input
                        className={'input-main input-v2d input-h-40 input-bg-white input-radius-4'}
                        disabled={isPreview}
                        allowClear={true}
                    />
                </Form.Item>
                <Form.Item
                    className={'form-item label-typography-font-14-line-18 form-item-mb-3'}
                    key={"k_range-time-picker"}
                    name="range-time-picker"
                    label={t("text.activeTime")}
                    initialValue={[
                        initFormValues?.expiryDate?.start ? moment(initFormValues.expiryDate.start, App.FormatISOFromMoment) : null,
                        initFormValues?.expiryDate?.end ? moment(initFormValues.expiryDate.end, App.FormatISOFromMoment) : null
                    ]}
                >
                    <DatePicker.RangePicker
                        allowEmpty={[true, true]}
                        style={{width: '100%'}}
                        disabled={isPreview}
                        showTime
                        format={t("format.dateTime")}
                        className={'input-main input-v2d input-h-40 input-bg-white input-radius-4'}
                    />
                </Form.Item>
                <Form.Item className={'form-item form-item-mb-3'}>
                    <Divider className={'divider-main divider-horizontal-m-12 divider-border-half-dark50'}/>
                    <Space>
                        <SettingOutlined/>
                        <CustomTypography textStyle={"text-14-18"} isStrong>
                            {t("title.setUpBasicPermissions")}
                        </CustomTypography>
                    </Space>
                </Form.Item>
                <Form.Item
                    className={'form-item label-typography-font-14-line-18 form-item-mb-3'}
                    key={"isShare"}
                    label={t('text.sharingAllow')}
                    labelAlign={'left'}
                    name={"isShare"}
                    valuePropName={"checked"}
                    initialValue={initFormValues?.status ?? true}
                >
                    <Switch
                        className={'switch-v2d-main w-30-h-16'}
                        disabled={isPreview}
                        defaultChecked={false}
                        size={"small"}
                    />
                </Form.Item>
                <Form.Item
                    className={'form-item label-typography-font-14-line-18 form-item-mb-3'}
                    key={"sInfo"}
                    label={t('text.displayInformation')}
                    labelAlign={'left'}
                    name={"shareInfo"}
                    valuePropName={"checked"}
                    initialValue={initFormValues?.sInfo ?? true}
                >
                    <Switch
                        className={'switch-v2d-main w-30-h-16'}
                        disabled={isPreview}
                        defaultChecked={false}
                        size={"small"}
                    />
                </Form.Item>
                <Form.Item className={'form-item form-item-mb-3'}>
                    <Divider className={'divider-main divider-horizontal-m-12 divider-border-half-dark50'}/>
                    <Space>
                        <SettingOutlined/>
                        <CustomTypography textStyle={"text-14-18"} isStrong>
                            {t("title.setUpAdvancedPermissions")}
                        </CustomTypography>
                    </Space>
                </Form.Item>
                <Form.Item
                    className={'form-item label-typography-font-14-line-18 form-item-mb-3'}
                    key={`tiles`}
                    label={t('text.tileMap')}
                    labelAlign={'left'}
                    name={"tiles"}
                    initialValue={initFormValues?.config?.tiles ?? defaultTilesValues()}
                >
                    <Checkbox.Group
                        disabled={isPreview}
                        className={'overflow-auto w-full max-h-32 w-24-h-24'}
                    >
                        {
                            listTilesRef.current.length > 0
                                ? listTilesRef.current.map((item, index) => {
                                    return (
                                        <Col key={index}>
                                            <Checkbox
                                                className={'checkbox-v2d-main text-14-18 margin-b-8 w-24-h-24'}
                                                value={item.value}
                                                style={{lineHeight: '32px'}}
                                            >
                                                {item.label}
                                            </Checkbox>
                                        </Col>
                                    )
                                })
                                : null
                        }
                    </Checkbox.Group>
                </Form.Item>
                <Form.Item
                    key={`contours`}
                    label={t('text.contourMap')}
                    labelAlign={'left'}
                    name={"contours"}
                    initialValue={initFormValues?.config?.contours ?? defaultContourValues()}
                    className={'form-item label-typography-font-14-line-18 form-item-mb-3'}
                >
                    <Checkbox.Group
                        disabled={isPreview}
                        className={'overflow-auto w-full max-h-32'}
                    >
                        {
                            listContoursRef.current.length > 0
                                ? listContoursRef.current.map((item, index) => {
                                    if (item.value && item.value !== '') {
                                        return (
                                            <Col flex={'500px'} key={`tiles_&${index}`}>
                                                <Checkbox
                                                    className={'checkbox-v2d-main text-14-18 margin-b-8 w-24-h-24'}
                                                    value={item.value}
                                                >
                                                    {item.value}
                                                </Checkbox>
                                            </Col>
                                        )
                                    }
                                })
                                : null
                        }
                    </Checkbox.Group>
                </Form.Item>
                <Form.Item
                    key={"sPl"}
                    label={t('text.shareProfileLatest')}
                    labelAlign={'left'}
                    name={"sPl"}
                    valuePropName={"checked"}
                    initialValue={initFormValues?.config?.sPl ?? true}
                    className={'form-item label-typography-font-14-line-18 form-item-mb-3'}
                >
                    <Switch
                        disabled={isPreview}
                        className={'switch-v2d-main w-30-h-16'}
                        size={"small"}
                        onChange={checked => setRdSPl(checked)}
                    />
                </Form.Item>
                <Form.Item
                    key={`pId`}
                    label={t('text.layerProfiles')}
                    labelAlign={'left'}
                    name={"pId"}
                    initialValue={initFormValues?.config?.pId ?? ''}
                    className={'form-item label-typography-font-14-line-18 form-item-mb-3'}
                >
                    <Radio.Group
                        disabled={isPreview || rdSPl}
                        className={'radio-main radio-v2d-main'}
                    >
                        {
                            vmProfile && vmProfile.items !== undefined
                                ? vmProfile.items.map(item => {
                                    return (
                                        <Radio
                                            value={item.id}
                                            key={item.id}
                                        >
                                            {item.name ?? item.dateCreatedAtFormatted()}
                                        </Radio>
                                    )
                                })
                                : null
                        }
                    </Radio.Group>
                </Form.Item>
                <Form.Item
                    key={`bArea`}
                    label={t('text.shareWithBoundingBox')}
                    labelAlign={'left'}
                    name={"bArea"}
                    initialValue={(shareBArea && shareBArea.length > 0) ? ShareRegion.Bounds : ShareRegion.All}
                    className={'form-item label-typography-font-14-line-18 form-item-mb-3'}
                >
                    <Radio.Group
                        disabled={isPreview}
                        className={'radio-main radio-v2d-main'}
                        onChange={(e) => {
                            if (e.target.value === ShareRegion.Bounds) {
                                tableShareBArea();
                            } else {
                                setIsTableShow({
                                    visible: 'hidden'
                                });
                            }
                        }}
                    >
                        <Radio value={ShareRegion.Bounds}>
                            {t('text.shareWithBoundingBox')}
                        </Radio>
                        <Radio value={ShareRegion.All}>{t('text.shareWithoutBoundingBox')}</Radio>
                    </Radio.Group>
                </Form.Item>
            </Form>
            {
                viewer.shareTool.getFlatCoordinates() && !isPreview && isTableShow.visible === 'visible'
                    ? <Card
                        title={t('text.shareWithBoundingBox')}
                        size="small"
                    >
                        {
                            viewer.shareTool.getFlatCoordinates().map((item, index) => {
                                return (
                                    <Card.Grid
                                        key={index}
                                        style={{
                                            textAlign: 'center',
                                            padding: "0.3rem",
                                            fontSize: '14px',
                                        }}
                                    >
                                        <Typography.Paragraph
                                            copyable={{
                                                text: `${item[0]},${item[1]}`
                                            }}
                                            style={{
                                                marginBottom: 0
                                            }}
                                        >
                                            <span className={"mr-0.5 text-white bg-green-600 rounded-sm pl-0.5 pr-0.5"}>{index + 1}</span>
                                            {item[0].toFixed(6)},{item[1].toFixed(6)}
                                        </Typography.Paragraph>
                                    </Card.Grid>
                                )
                            })
                        }
                    </Card>
                    : null
            }
            {
                isPreview && initFormValues && initFormValues.config?.bArea && initFormValues.config.bArea.length > 0
                    ? <Card
                        title={t('text.shareWithOldBoundingBox')}
                        size="small"
                    >
                        {
                            initFormValues.config.bArea.map((item, index) => {
                                return (
                                    <Card.Grid
                                        key={index}
                                        style={{
                                            textAlign: 'center',
                                            padding: "0.3rem",
                                            fontSize: '14px',
                                        }}
                                    >
                                        <Typography.Paragraph
                                            copyable={{
                                                text: `${item[0]},${item[1]}`
                                            }}
                                            style={{
                                                marginBottom: 0
                                            }}
                                        >
                                            <span className={"mr-0.5 text-white bg-green-600 rounded-sm pl-0.5 pr-0.5"}>{index + 1}</span>
                                            {item[0].toFixed(6)},{item[1].toFixed(6)}
                                        </Typography.Paragraph>
                                    </Card.Grid>
                                )
                            })
                        }
                    </Card>
                    : null
            }
        </Modal>
    )
}
