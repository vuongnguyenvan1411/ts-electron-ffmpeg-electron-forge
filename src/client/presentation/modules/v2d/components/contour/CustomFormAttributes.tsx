import {Form, message, Popconfirm, Table, Typography} from "antd";
import {ColumnsType} from "antd/es/table";
import React, {Key, useEffect, useState} from "react";
import {EditableCell} from "../profile/AttributeTableModalFC";
import {FilterValue, SorterResult, TablePaginationConfig} from "antd/lib/table/interface";
import {useTranslation} from "react-i18next";
import {EMapLayer, EPropertiesType, EToolsEdit, TColumnAttribute} from "../../const/Defines";
import {DeleteOutlined, EditOutlined, ZoomInOutlined} from "@ant-design/icons";
import {Viewer} from "../../viewer/Viewer";
import {TAttribute} from "../profile/AttributeAddFeatureModalFC";

export const CustomFormAttributes = (props: {
    viewer: Viewer,
    properties: TAttribute[],
    zoomToFeature: (record: any) => void,
    removeFeature: (record: any) => void,
    setMergedColumns?: React.Dispatch<React.SetStateAction<TAttribute[]>>,
    dataFeature: any,
    type: EMapLayer,
    WFSTransaction?: (mode: EToolsEdit) => void
}) => {
    const {t} = useTranslation();
    const [form] = Form.useForm();

    const [data, setData] = useState(props.dataFeature);
    const [editingKey, setEditingKey] = useState('');
    const [sortedInfo, setSortedInfo] = useState<SorterResult<any>>({});

    const isEditing = (record: any) => record.key === editingKey;

    const columns: TColumnAttribute[] = [{
        title: 'STT',
        key: 'stt',
        dataIndex: 'key',
        width: '57px',
        fixed: 'left',
        sorter: (a, b) => (a.key - b.key),
        sortOrder: sortedInfo.columnKey === 'stt' && sortedInfo.order,
        ellipsis: true,
    }];

    props.properties.forEach((property) => {
        const index = property.name.toString();

        columns.push({
            title: property.name,
            dataIndex: property.name,
            key: property.name,
            editable: !(props.type === EMapLayer.Contour && index === 'ID'),
            type: property.type,
            width: '80px',
            sorter: (a, b) => (
                property.type === EPropertiesType.String ?
                    a[index].toString().length - b[index].toString().length :
                    (property.type === EPropertiesType.Date ? ((new Date(a[index]) as any) - (new Date(b[index]) as any)) : a[index] - b[index])
            ),
            sortOrder: sortedInfo.columnKey === property.name && sortedInfo.order,
            ellipsis: true,
        });
    });

    const onCancelEdit = () => setEditingKey('');

    const save = async (key: Key) => {
        try {
            const row = (await form.validateFields()) as any;
            const newData = [...data];

            const index = newData.findIndex(item => key === item.key);
            if (index > -1) {
                const item = newData[index];
                newData.splice(index, 1, {
                    ...item,
                    ...row,
                });
                setData(newData);
                setEditingKey('');
            } else {
                newData.push(row);
                setData(newData);
                setEditingKey('');
            }
            newData.forEach(item => {
                if (props.type === EMapLayer.Profile) {
                    const featureProfile = props.viewer.profileTool.getFeatureById(item.id);
                    if (featureProfile) {
                        featureProfile.set('properties', item)
                    } else {
                        message.error(t('message.confirmError'))
                    }
                } else {
                    const featureWFS = props.viewer.contourTool.getFeatureById(item.ID);

                    if (featureWFS) {
                        featureWFS.set('text', item[t('v3d.text.annotation')]);

                        props.WFSTransaction?.(EToolsEdit.Save)
                    }
                }
            })
        } catch (errInfo) {
            console.log('Validate Failed:', errInfo);
        }
    };

    if (props.dataFeature.length > 0) {
        columns.push({
            title: t('text.advanced'),
            dataIndex: 'operation',
            key: 'operation',
            width: '100px',
            fixed: 'right',
            render: (_: any, record: any) => {
                const editable = isEditing(record);
                return editable
                    ? <span>
                            <Typography.Link
                                onClick={() => save(record.key)}
                                style={{marginRight: 8}}
                            >
                                {t('button.save')}
                            </Typography.Link>
                               <Typography.Link
                                   onClick={onCancelEdit}
                                   style={{marginRight: 8}}
                               >
                                {t('button.cancel')}
                            </Typography.Link>
                            <Popconfirm
                                title={t('title.confirmBeforeDeleting')}
                                onConfirm={onCancelEdit}
                                onCancel={onCancelEdit}
                                overlayClassName={'popconfirm-content-main'}
                            >
                                <a></a>
                            </Popconfirm>
                        </span>
                    : <>
                        <Typography.Link
                            disabled={editingKey !== ''}
                            onClick={() => props.zoomToFeature(record)}
                        >
                            <ZoomInOutlined/>
                        </Typography.Link>
                        &ensp;
                        <Typography.Link
                            disabled={editingKey !== ''}
                            onClick={() => edit(record)}
                        >
                            <EditOutlined/>
                        </Typography.Link>
                        &ensp;
                        <Popconfirm
                            overlayClassName={'popconfirm-content-main'}
                            disabled={editingKey !== ''}
                            title={t('title.confirmBeforeDeleting')}
                            onConfirm={() => props.removeFeature(record)}
                        >
                            <Typography.Link disabled={editingKey !== ''}>
                                <DeleteOutlined/>
                            </Typography.Link>
                        </Popconfirm>
                    </>
            },
        })
    }

    const mergedColumns = columns.map(col => {
        if (!col.editable) {
            return col;
        }

        return {
            ...col,
            onCell: (record: any) => ({
                record,
                inputType: col.type,
                dataIndex: col.dataIndex,
                title: col.title,
                editing: isEditing(record),
            }),
        };
    });


    useEffect(() => {

        props.setMergedColumns?.(mergedColumns as any)

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const edit = (record: any) => {

        form.setFieldsValue({
            ...record,
        });
        setEditingKey(record.key);
    };

    const handleChangeSort = (pagination: TablePaginationConfig, filters: Record<string, FilterValue | null>, sorter: any) => {
        setSortedInfo(sorter)
    }

    return (
        <Form form={form} component={false}>
            <Table
                components={{
                    body: {
                        cell: EditableCell,
                    },
                }}
                scroll={{x: 200, y: 400}}
                dataSource={data}
                columns={mergedColumns as ColumnsType}
                bordered
                rowClassName="editable-row"
                className={'table-v2d-profile-save'}
                onChange={handleChangeSort}
            />
        </Form>
    )
}