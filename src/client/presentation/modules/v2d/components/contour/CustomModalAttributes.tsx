import {Modal} from "antd";
import Image from "next/image";
import {Images} from "../../../../../const/Images";
import {CustomTypography} from "../../../../components/CustomTypography";
import Draggable, {DraggableData, DraggableEvent} from "react-draggable";
import {CustomButton} from "../../../../components/CustomButton";
import React, {useEffect, useRef, useState} from "react";
import {useTranslation} from "react-i18next";
import {Color} from "../../../../../const/Color";

export const CustomModalAttributes = (props: {
    form?: JSX.Element,
    select?: JSX.Element,
    onOk?: ((e: React.MouseEvent<HTMLElement, MouseEvent>) => void),
    onCancel: ((e: React.MouseEvent<HTMLElement, MouseEvent>) => void),
}) => {

    const {t} = useTranslation();

    const [disabled, setDisabled] = useState(false);
    const draggableRef = useRef<HTMLDivElement>(null);
    const [bounds, setBounds] = useState({left: 0, top: 0, bottom: 0, right: 0});

    useEffect(() => {
        console.log('%cMount FC: ModalAttributesFC', Color.ConsoleInfo);

        return () => {
            console.log('%cUnmount FC: ModalAttributesFC', Color.ConsoleInfo);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onDragging = (_event: DraggableEvent, uiData: DraggableData) => {
        const {clientWidth, clientHeight} = window.document.documentElement;
        const targetRect = draggableRef.current?.getBoundingClientRect();

        if (!targetRect) {
            return;
        }
        setBounds({
            left: -targetRect.left + uiData.x,
            right: clientWidth - (targetRect.right - uiData.x),
            top: -targetRect.top + uiData.y,
            bottom: clientHeight - (targetRect.bottom - uiData.y),
        });
    };

    return (
        <Modal
            key={'modal-form-save-attribute'}
            className={'modal-main ant-modal-v2d middle header-cursor-draggable'}
            onCancel={props.onCancel}
            closeIcon={<Image
                height={24}
                width={24}
                src={Images.iconXCircle.data}
                alt={Images.iconXCircle.atl}
            />}
            title={
                <div
                    onDragStart={() => {
                        if (disabled) {
                            setDisabled(false);
                        }
                    }}
                    onDragEnd={() => {
                        setDisabled(true);
                    }}
                    onMouseOver={() => {
                        if (disabled) {
                            setDisabled(false);
                        }
                    }}
                    onMouseOut={() => {
                        setDisabled(true);
                    }}
                >
                    <CustomTypography>
                        {t('text.attributesTable')}
                    </CustomTypography>
                </div>
            }
            modalRender={modal => (
                <Draggable
                    disabled={disabled}
                    bounds={bounds}
                    onDrag={(event, uiData) => onDragging(event, uiData)}
                >
                    <div ref={draggableRef}>
                        {modal}
                    </div>
                </Draggable>
            )}
            mask={false}
            maskClosable={false}
            maskStyle={{display: 'none'}}
            visible
            closable
            destroyOnClose
            centered
            footer={
                props.onOk
                    ? <CustomButton
                        size={'small'}
                        usingFor={'geodetic'}
                        key={"save"}
                        onClick={props.onOk}
                        className={'icon-vertical-base'}
                    >
                        {t("button.save")}
                    </CustomButton>
                    : null
            }
        >
            {props.form && props.form}
            {props.select && props.select}
        </Modal>
    )
}