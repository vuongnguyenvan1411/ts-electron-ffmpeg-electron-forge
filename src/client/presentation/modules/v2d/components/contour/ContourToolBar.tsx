import {CLContour} from "../../viewer/clim/CLContourTool";
import styles from "../../styles/MapView.module.scss";
import {Menu, message, Popconfirm, Tooltip} from "antd";
import React, {useEffect, useState} from "react";
import {EDrawType, EMapLayer, EPropertiesType, EToolsEdit} from "../../const/Defines";
import Image from "next/image";
import {Images} from "../../../../../const/Images";
import {useTranslation} from "react-i18next";
import {find, indexOf} from "lodash";
import {GeoJSON, WFS} from "ol/format";
import {Viewer} from "../../viewer/Viewer";
import {App} from "../../const/App";
import VectorSource from "ol/source/Vector";
import {bbox} from "ol/loadingstrategy";
import {removeAction, updateView} from "../tool/MapAction";
import {Fill, Stroke, Style, Text} from "ol/style";
import {and as andFilter, equalTo as equalToFilter, like as likeFilter,} from "ol/format/filter";
import {Modify} from "ol/interaction";
import CircleStyle from "ol/style/Circle";
import {WriteTransactionOptions} from "ol/format/WFS";
import {MapEventName} from "../../const/Event";
import {TileWMS} from "ol/source";
import {FeatureWMSModel} from "../../../../../models/service/geodetic/ProfileModel";
import {CustomModalAttributes} from "./CustomModalAttributes";
import {CustomFormAttributes} from "./CustomFormAttributes";
import {TAttribute} from "../profile/AttributeAddFeatureModalFC";
import {Feature, View} from "ol";
import {Point} from "ol/geom";

export const ContourToolBar = (props: {
    selected?: CLContour,
    viewer: Viewer
}) => {
    const {t} = useTranslation();

    const [selectedTool, setSelectedTool] = useState<EToolsEdit[]>([]);
    const [isEdit, setIsEdit] = useState<boolean>(false);
    const [isSelect, setIsSelect] = useState<{
        isSelected: boolean,
        feature?: any,
    }>({
        isSelected: false,
    });
    const [featureType, setFeatureType] = useState<string>();
    const [featurePrefix, setFeaturePrefix] = useState<string>();

    //Characteristics table modal
    const [isCharVisible, setIsCharVisible] = useState<{
        visible: boolean,
        dataFeature: any
    }>({
        visible: false,
        dataFeature: [],
    });

    const layerWFS = props.viewer.contourTool.getWFSLayer();
    const formatWFS = new WFS();
    const selected = props.selected;
    const properties: TAttribute[] = [
        {
            type: EPropertiesType.String,
            name: 'ID',
        },
        {
            type: EPropertiesType.String,
            name: t('v3d.text.annotation')
        }
    ];

    useEffect(() => {
        setSelectedTool([])
        setIsSelect({isSelected: false})

        if (selected) {
            const wmsName: string = selected.layer.get('name');

            if (wmsName) {
                setFeatureType(wmsName.split("_").pop() as string)
                setFeaturePrefix(wmsName.substring(0, wmsName.lastIndexOf("_")))
            }

            if (!featureType || !featurePrefix) {
                return
            }
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selected]);

    const onSelectFeature = () => {
        if (!props.selected) {
            message.warn(t("message.confirmError")).then()

            return
        }

        onLoadWFS()

        const wmsSource = props.selected.layer.getSource();

        props.viewer.map.on(MapEventName.SingleClick, (evt) => {
            const viewResolution = props.viewer.map.getView().getResolution();

            if (wmsSource && viewResolution) {
                const url = (wmsSource as TileWMS).getFeatureInfoUrl(
                    evt.coordinate,
                    viewResolution,
                    App.DefaultEPSG,
                    {
                        INFO_FORMAT: 'geojson',
                        QUERY_LAYERS: 'coco_danang_cad_ano',
                        SRS: App.DefaultEPSG,
                    }
                )

                if (url) {
                    fetch(url, {
                        method: 'get',
                        headers: {
                            'Content-Type': 'geojson',
                        },
                    })
                        .then(function (res) {
                            return new FeatureWMSModel(res)
                        })
                        .then((data) => {
                            console.log('data', data)
                        })
                        .catch(err => console.log('errr', err))
                }
            }

            // onLoadWFS()
        })
    }

    //TEST WITH FID

    const fid = 1489;

    const onLoadWFS = () => {
        if (!props.selected || !layerWFS || !featureType) {
            return
        }

        const featureRequest = formatWFS.writeGetFeature({
            featureNS: "http://localhost:3001/",
            featurePrefix: 'coco_danang',
            featureTypes: [featureType],
            outputFormat: 'application/json',
            // bbox: extent,
            // bbox: fbgBB(props.viewer.map.getView().getCenter() as number []),
            srsName: App.DefaultEPSG,
            geometryName: 'geom',
            maxFeatures: 10,
            filter: andFilter(
                likeFilter('text', '592'),
                equalToFilter('fid', fid)
            )
        });

        const sourceWFS = new VectorSource({
            format: new GeoJSON(),
            strategy: bbox,
            loader: function (extent, resolution, projection) {
                fetch
                ('http://e0aa-222-252-10-203.ap.ngrok.io/cgi-bin/tinyows.exe?service=wfs', {
                    method: 'POST',
                    body: new XMLSerializer().serializeToString(featureRequest),
                })
                    .then(r => {
                        return r.json();
                    })
                    .then(json => {
                        const features = new GeoJSON().readFeatures(json);

                        sourceWFS.clear(true);

                        features.forEach(feature => {
                            feature.setStyle(new Style({
                                text: new Text({
                                    text: feature.get('text')
                                }),
                                fill: new Fill({
                                    color: "#029",
                                }),
                                stroke: new Stroke({
                                    color: "#000",
                                    width: 8,
                                }),
                                image: new CircleStyle({
                                    radius: 7,
                                    fill: new Fill({
                                        color: "#029",
                                    }),
                                }),
                            }))
                        })
                        sourceWFS.addFeatures(features);

                    })
                    .catch(err => message.warn(t("message.confirmError")).then(() => console.log('error', err)))
            },
        });

        layerWFS.setSource(sourceWFS)
    }

    const unLoadWFS = () => {
        if (layerWFS) {
            const sourceWFS = layerWFS.getSource();
            if (sourceWFS) sourceWFS.clear(true)
        }

        setIsEdit(false);
        setSelectedTool([EToolsEdit.Select]);

        removeAction(props.viewer)
        updateView(props.viewer)

        if (selected) {
            const params = (selected.layer.getSource() as TileWMS).getParams() as any

            // (selected.layer as TileLayer<any>).getSource()?.updateParams(params)
            selected.layer.getSource()?.refresh();
        }
    }

    const WFSTransaction = (mode: EToolsEdit) => {
        let node;
        const selected = props.selected

        if (!featureType || !featurePrefix || !selected) {
            message.warn(t("message.confirmError")).then();

            return
        }

        const formatGML = {
            featureNS: "http://localhost:3001/",
            featurePrefix: featurePrefix,
            featureType: featureType,
            srsName: App.EPSG4236Reverse,
            hasZ: true,
        } as WriteTransactionOptions;

        if (layerWFS) {
            layerWFS.getSource()?.getFeatures().forEach(feature => {
                const clone = feature.clone();
                clone.setId(feature.getId());
                clone.setGeometryName('geom');

                switch (mode) {
                    case EToolsEdit.AddFeature:
                        node = formatWFS.writeTransaction([clone], [], [], formatGML);
                        break;
                    case EToolsEdit.Save:
                        node = formatWFS.writeTransaction([], [clone], [], formatGML);
                        break;
                    case EToolsEdit.Delete:
                        node = formatWFS.writeTransaction([], [], [clone], formatGML);
                        break;
                }
            })
        }

        if (node) {
            fetch(
                "http://e0aa-222-252-10-203.ap.ngrok.io/cgi-bin/tinyows.exe?service=wfs",
                {
                    method: "POST",
                    body: new XMLSerializer().serializeToString(node),
                }
            )
                .then(data => {
                    if (data.ok) {
                        unLoadWFS();

                    }
                })
                .catch(err => {
                    unLoadWFS();
                    message.warn(t("message.confirmError")).then(() => console.log('error', err))
                })

            setSelectedTool([])
        }
    }

    const onInteractionWFS = (key: EToolsEdit) => {

        if (!layerWFS) {
            return
        }

        setIsEdit(true);
        const sourceWFS = layerWFS.getSource() as VectorSource<any>;

        if (sourceWFS.getFeatures()) {
            switch (key) {
                case EToolsEdit.EditGeometry:
                    const modify = new Modify({source: sourceWFS})
                    props.viewer.map.addInteraction(modify)

                    break;

                case EToolsEdit.EditCharacteristics:
                    const properties: any = [];

                    sourceWFS.getFeatures().forEach((feature, index) => {
                        const property = feature.getProperties();

                        if (property) {
                            properties.push({
                                key: index + 1,
                                ID: property.fid,
                                [t('v3d.text.annotation')]: property.text,
                            })
                        }
                    });
                    setIsCharVisible({
                        visible: true,
                        dataFeature: properties,
                    });

                    break;
            }
        }
    }

    const onClickControl = (evt: any) => {
        switch (evt.key) {
            case EToolsEdit.Select:
                if (!isSelect) {
                    setSelectedTool([])
                    setIsSelect({isSelected: false})
                    setIsEdit(false)

                } else {
                    setSelectedTool([EToolsEdit.Select])
                    setIsSelect({isSelected: true})
                    onSelectFeature()
                }

                break;

            case EToolsEdit.EditGeometry:
                if (indexOf(selectedTool, EToolsEdit.EditGeometry) === -1) {
                    setSelectedTool([EToolsEdit.Edit, EToolsEdit.EditGeometry])
                    onInteractionWFS(EToolsEdit.EditGeometry);

                } else {
                    unLoadWFS();
                }

                break;
            case EToolsEdit.EditCharacteristics:
                if (indexOf(selectedTool, EToolsEdit.EditCharacteristics) === -1) {
                    setSelectedTool([EToolsEdit.Edit, EToolsEdit.EditCharacteristics])
                    onInteractionWFS(EToolsEdit.EditCharacteristics);

                } else {
                    unLoadWFS();
                }

                break;

            case EToolsEdit.Save:

                setSelectedTool([EToolsEdit.Select]);
                WFSTransaction(EToolsEdit.Save)

                break;
        }
    }

    const WFSMenuItems = [
        {
            label: <Tooltip
                title={t('text.selectFeature')}
                placement="bottomRight"
            >
                <Image
                    src={Images.iconArrowsOutCardinal.data}
                    alt={Images.iconArrowsOutCardinal.atl}
                    width={24}
                    height={24}
                />
            </Tooltip>,
            key: EToolsEdit.Select,
            disabled: !props.selected,
        },
        {
            label: <Tooltip
                title={t('text.editCharacteristics')}
                placement="bottomRight"
            >
                <Image
                    src={Images.iconNotePencil.data}
                    alt={Images.iconNotePencil.atl}
                    width={24}
                    height={24}
                />
            </Tooltip>,
            key: EToolsEdit.EditCharacteristics,
            disabled: false
            // disabled: !isSelect.isSelected && !isSelect.feature,
        },
        {
            label: <Tooltip
                title={t('text.editGeometry')}
                placement="bottomRight"
            >
                <Image
                    src={Images.iconPencilSimpleLineBold.data}
                    alt={Images.iconPencilSimpleLineBold.atl}
                    width={24}
                    height={24}
                />
            </Tooltip>,
            key: EToolsEdit.EditGeometry,
            disabled: false
            // disabled: !isSelect.isSelected && !isSelect.feature,
        },
        {
            label: <Popconfirm
                overlayClassName={'popconfirm-content-main'}
                title={t('title.confirmBeforeDeleting')}
                okText={t('button.ok')}
                cancelText={t('text.no')}
                onCancel={() => setSelectedTool([EToolsEdit.Select])}
                onConfirm={() => WFSTransaction(EToolsEdit.Delete)}
            >
                <Image
                    src={Images.iconTrash.data}
                    alt={Images.iconTrash.atl}
                    width={24}
                    height={24}
                />
            </Popconfirm>,
            key: EToolsEdit.Delete,
            disabled: false
            // disabled: !isSelect.isSelected && !isSelect.feature,
        },
        {
            label: <Tooltip
                title={t('button.save')}
                placement="bottomRight"
            >
                <Image
                    src={Images.iconSaveBL.data}
                    alt={Images.iconSaveBL.atl}
                    width={24}
                    height={24}
                />
            </Tooltip>,
            key: EToolsEdit.Save,
            disabled: !isEdit,
        },
    ]

    const onCancelChar = () => setIsCharVisible({
        visible: false,
        dataFeature: [],
    });

    const zoomToFeature = (record: any) => {
        if (!layerWFS) {
            return
        }

        const feature: Feature<any> | undefined = find(layerWFS.getSource()?.getFeatures(), function (feature) {
            return feature.get('fid') === record.ID;
        })

        if (feature) {
            const geomName = feature.getGeometryName();
            let center;

            switch (geomName) {

                default:
                    center = (feature.getGeometry() as Point).getFlatCoordinates();
                    break;
                case EDrawType.LineString:

                    break;
            }

            props.viewer.map.setView(new View({
                center: center,
                zoom: props.viewer.map.getView().getZoom(),
                projection: App.DefaultEPSG,
            }))
        }
    }

    const removeFeature = (record: any) => {
        const featureWFS = props.viewer.contourTool.getFeatureById(record.ID);

        if (featureWFS) {
            featureWFS.set('text', record[t('v3d.text.annotation')]);

            WFSTransaction(EToolsEdit.Delete);
            onCancelChar();
        }
    };

    return (
        <>
            <Menu
                items={WFSMenuItems}
                mode={'horizontal'}
                className={styles.ControlDrawToolBar}
                onClick={onClickControl}
                selectedKeys={selectedTool}
            />
            {
                isCharVisible.visible && <CustomModalAttributes
                    onCancel={onCancelChar}
                    form={
                        <CustomFormAttributes
                            viewer={props.viewer}
                            properties={properties}
                            zoomToFeature={zoomToFeature}
                            dataFeature={isCharVisible.dataFeature}
                            type={EMapLayer.Contour}
                            WFSTransaction={WFSTransaction}
                            removeFeature={removeFeature}
                        />
                    }
                />
            }
        </>
    )
}
