import {Tree} from "antd";
import React, {useEffect, useLayoutEffect, useMemo, useState} from "react";
import {Key} from "antd/es/table/interface";
import {EDrawType, EMapLayer, EMenuMapItem, EStatusTreeItem, IEDataNode, IEEventDataNode, IEEventDropNode, IEVentDragNode, IOTreeCheckInfo, IOTreeSelectInfo} from "../const/Defines";
import {Utils as RootUtils} from "../../../../core/Utils";
import {filter, findKey, indexOf, isEqual, reject} from "lodash";
import LayerGroup from "ol/layer/Group";
import EventEmitter from "eventemitter3";
import {TEventContourAdded, TEventContourSets, TEventFeatureSets, TEventMeasureAdded, TEventMeasureRemoved, TEventMeasureSets, TEventProfileAdded, TEventProfileRemoved, TEventProfileSets, TEventRegionRemoved, TEventRegionSets, TEventTileAdded, TEventTileSets, ViewerEventName} from "../const/Event";
import VectorLayer from "ol/layer/Vector";
import {PropertiesPanelFC} from "./PropertiesPanelFC";
import {Viewer} from "../viewer/Viewer";
import {CLMeasure} from "../viewer/clim/CLMeasureTool";
import {CLInterface} from "../viewer/clim/CLInterface";
import {CLTile} from "../viewer/clim/CLTileTool";
import {CLContour} from "../viewer/clim/CLContourTool";
import {CLProfile} from "../viewer/clim/CLProfileTool";
import {Color} from "../../../../const/Color";
import {Map2DModel} from "../../../../models/service/geodetic/Map2DModel";
import {LoadProjectFC} from "./LoadProjectFC";
import {getCenter} from "ol/extent";
import {Point} from "ol/geom";
import {DrawToolsPanelFC} from "./tool/DrawToolsPanelFC";
import {removeAction, updateView} from "./tool/MapAction";
import {AttributeTableModalFC} from "./profile/AttributeTableModalFC";
import {TFormCreateProfile} from "./profile/CreateProfileLayerModalFC";
import {StyleMeasureFn} from "./profile/StyleDrawFn";
import {TFormDrawerValues, TFormMarkerValues} from "../viewer/clim/CLTypeTool";
import {Feature} from "ol";
import {Style} from "ol/style";
import {AntTreeNodeProps} from "antd/lib/tree/Tree";
import {ControlDrawToolbarFC} from "./profile/ControlDrawToolbar";
import {CLShareRegion} from "../viewer/clim/CLShareRegionTool";
import {SwitchBaseLayerFC} from "./SwitchBaseLayerFC";
import {FeatureSingleton} from "../../../../models/FeatureSingleton";
import {DetectMedia} from "../const/App";

export const MapLayerFC = React.memo((props: {
        item: Map2DModel,
        viewer: Viewer,
        event: EventEmitter,
        keyLayer?: EMenuMapItem,
        setSelectObj: (object: any) => void,
        selectInfo: { object: any },
        statusContour: boolean,
        setStatusContour: (status: boolean) => void,
        onRightClickFeature: (info: { event: React.MouseEvent, node: IEDataNode }) => void,
    }) => {
        const [expandedKeys, setExpandedKeys] = useState<Key[]>([]);
        const [checkedKeys, setCheckedKeys] = useState<Key[]>([]);
        const [selectedKeys, setSelectedKeys] = useState<Key[]>([]);
        const [treeData, setTreeData] = useState<IEDataNode[]>([]);
        const [reRenderTree, setReRenderTree] = useState(false);
        const [keyMenuLayer, setKeyMenuLayer] = useState<EMenuMapItem>()
        const [isAttributesTableVisible, setIsAttributesTableVisible] = useState(false);
        const [isInit, setIsInit] = useState(false);

        useLayoutEffect(() => {
            console.log('%cMount FC: MapLayerFC', Color.ConsoleInfo);

            if (
                props.viewer.baseMapTool.layer
                && props.viewer.tileTool.layer
                && props.viewer.contourTool.layerGroup
                && props.viewer.markerTool.layer
                && props.viewer.measureTool.layer
                && props.viewer.profileTool.layer
                && props.viewer.shareTool.layer
            ) {
                setTreeData([
                    {
                        title: props.viewer.baseMapTool.layer.get('title'),
                        key: props.viewer.baseMapTool.layer.get('id'),
                        className: "tree-parent",
                        children: [],
                        object: props.viewer.baseMapTool.layer,
                    },
                    {
                        title: props.viewer.tileTool.layer.get('title'),
                        key: props.viewer.tileTool.layer.get('id'),
                        className: "tree-parent",
                        children: [],
                        object: props.viewer.tileTool.layer,
                    },
                    {
                        title: props.viewer.contourTool.layerGroup.get('title'),
                        key: props.viewer.contourTool.layerGroup.get('id'),
                        children: [],
                        disabled: !(props.item.info && props.item.info.contours && props.item.info.contours.length > 0),
                        object: props.viewer.contourTool.layerGroup,
                    },
                    {
                        title: props.viewer.markerTool.layer.get('title'),
                        key: props.viewer.markerTool.layer.get('id'),
                        className: "tree-parent",
                        children: [],
                        disabled: !(props.item.info && props.item.info.markers && props.item.info.markers.length > 0),
                        object: props.viewer.markerTool.layer
                    },
                    {
                        title: props.viewer.profileTool.layer.get('title'),
                        key: props.viewer.profileTool.layer.get('id'),
                        children: [],
                        object: props.viewer.profileTool.layer,
                    },
                    {
                        title: props.viewer.shareTool.layer.get('title'),
                        key: props.viewer.shareTool.layer.get('id'),
                        children: [],
                        object: props.viewer.shareTool.layer,
                    },
                    {
                        title: props.viewer.measureTool.layer.get('title'),
                        key: props.viewer.measureTool.layer.get('id'),
                        children: [],
                        object: props.viewer.measureTool.layer,
                    },
                ])

                setExpandedKeys([
                    props.viewer.baseMapTool.layer.get('id'),
                    props.viewer.tileTool.layer.get('id'),
                    props.viewer.shareTool.layer.get('id'),
                    props.viewer.profileTool.layer.get('id'),
                    props.viewer.measureTool.layer.get('id'),
                    props.viewer.contourTool.layerGroup.get('id'),
                ])

                setCheckedKeys(() => {
                    const keys: Key[] = [];
                    [
                        props.viewer.baseMapTool.layer,
                        props.viewer.tileTool.layer,
                        props.viewer.contourTool.layerGroup,
                        props.viewer.markerTool.layer,
                        props.viewer.measureTool.layer,
                        props.viewer.profileTool.layer,
                        props.viewer.shareTool.layer,
                    ].forEach((item) => {
                        if (item && item.getVisible()) {
                            keys.push(item.get('id'));
                        }
                    })

                    return keys;
                });
            } else {
                console.log("%cAll Layers not initialized", Color.ConsoleError);
            }

            setIsInit(true);

            return () => {
                console.log('%cUnmount FC: MapLayerFC', Color.ConsoleInfo);
            }

            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, []);

        const selectMenuLayer = (key: EMenuMapItem) => {
            console.log('onSelectMenuLayer', key);

            const displayTree = (treeKey: EMapLayer[]) => {
                let treeDataDisplay;

                const dataDisplay = reject(treeData, function (item) {
                    if (indexOf(treeKey, item.key) === -1) {
                        return item
                    }
                })

                const dataHide = reject(treeData, function (item) {
                    if (indexOf(treeKey, item.key) > -1) {
                        return item
                    }
                })

                if (isEqual(treeKey, [EMapLayer.Profile]) || isEqual(treeKey, [EMapLayer.Measure]) || isEqual(treeKey, [EMapLayer.Contour])) {
                    treeDataDisplay = (dataDisplay as IEDataNode[])[0].children!;
                } else {
                    treeDataDisplay = dataDisplay
                }

                treeDataDisplay.forEach((v1: IEDataNode) => {
                    setTreeDisplay([v1], EStatusTreeItem.Display)
                })

                dataHide.forEach((v1: IEDataNode) => {
                    setTreeDisplay([v1], EStatusTreeItem.None)
                })
            }

            switch (key) {
                case EMenuMapItem.ContourLayer:
                    displayTree([EMapLayer.Contour])

                    break;
                case EMenuMapItem.Map2DLayer:
                    displayTree([EMapLayer.Marker, EMapLayer.Tile])

                    break;
                case EMenuMapItem.ProfileLayer:
                    displayTree([EMapLayer.Profile])

                    break;
                case EMenuMapItem.MeasureLayer:
                    displayTree([EMapLayer.Measure])

                    break;

                case EMenuMapItem.BaseLayer:
                    displayTree([])

                    break;
            }
        }

        useEffect(() => {

            setKeyMenuLayer(props.keyLayer)

        }, [props.keyLayer])

        useEffect(() => {

            if (keyMenuLayer) {
                selectMenuLayer(keyMenuLayer)
            }

            setReRenderTree(!reRenderTree);

            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [treeData, keyMenuLayer])

        useLayoutEffect(() => {
            if (isInit) {
                props.event.on(ViewerEventName.TileSets, onEventSetsTile);
                props.event.on(ViewerEventName.TileAdded, onEventAddTile);

                props.event.on(ViewerEventName.ContourSets, onEventSetsContour);
                props.event.on(ViewerEventName.ContourAdded, onEventAddContour);

                props.event.on(ViewerEventName.MeasureSets, onEventSetsMeasure);
                props.event.on(ViewerEventName.MeasureAdded, onEventAddMeasure);
                props.event.on(ViewerEventName.MeasureRemoved, onEventRemoveMeasure);

                props.event.on(ViewerEventName.ProfileSets, onEventSetsProfile);
                props.event.on(ViewerEventName.ProfileAdded, onEventAddProfile);
                props.event.on(ViewerEventName.ProfileRemoved, onEventRemoveProfile);

                props.event.on(ViewerEventName.ShareRegionAdded, onEventAddShare);
                props.event.on(ViewerEventName.ShareRegionRemoved, onEventRemoveRegion);

                props.event.on(ViewerEventName.FeatureAdded, onEventAddFeature);
                props.event.on(ViewerEventName.FeatureSets, onEventSetsFeature);
                props.event.on(ViewerEventName.FeatureRemoved, onEventRemoveFeature);
                props.event.on(ViewerEventName.ProfileModified, onUpdateTreeNode);

                return () => {
                    props.event.off(ViewerEventName.TileSets, onEventSetsTile);
                    props.event.off(ViewerEventName.TileAdded, onEventAddTile);

                    props.event.off(ViewerEventName.ContourSets, onEventSetsContour);
                    props.event.off(ViewerEventName.ContourAdded, onEventAddContour);

                    props.event.off(ViewerEventName.MeasureSets, onEventSetsMeasure);
                    props.event.off(ViewerEventName.MeasureAdded, onEventAddMeasure);
                    props.event.off(ViewerEventName.MeasureRemoved, onEventRemoveMeasure);

                    props.event.off(ViewerEventName.ProfileSets, onEventSetsProfile);
                    props.event.off(ViewerEventName.ProfileAdded, onEventAddProfile);
                    props.event.off(ViewerEventName.ProfileRemoved, onEventRemoveProfile);

                    props.event.off(ViewerEventName.ShareRegionAdded, onEventAddShare);
                    props.event.off(ViewerEventName.ShareRegionRemoved, onEventRemoveRegion);

                    props.event.off(ViewerEventName.FeatureAdded, onEventAddFeature);
                    props.event.off(ViewerEventName.FeatureSets, onEventSetsFeature);
                    props.event.off(ViewerEventName.FeatureRemoved, onEventRemoveFeature);
                    props.event.off(ViewerEventName.ProfileModified, onUpdateTreeNode);
                }
            }

            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [isInit]);

        useEffect(() => {
            const contourData = treeData.find((item) => item.key === props.viewer.contourTool.layerGroup.get('id'));
            const keys: Key[] = [];

            if (!contourData) {
                return
            }

            (contourData.object as LayerGroup).setVisible(props.statusContour);

            contourData.children!.forEach((item: IEDataNode) => {
                if (item.object && "setVisible" in item.object) {
                    item.object.setVisible(props.statusContour);
                }
            })

            if (!props.statusContour) {
                setCheckedKeys(() => {
                    checkedKeys.map(key => {
                        if (key.toString().split('_')[0] !== EMapLayer.Contour) {
                            keys.push(key);
                        }
                    })
                    return keys;
                })
            } else {
                contourData.children!.map(item => {
                    keys.push(item.key);
                })
                setCheckedKeys([...checkedKeys, ...keys]);
            }

            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [props.statusContour])

        const _createTreeNodes = (objects: CLInterface[], isReplace: boolean, layer: EMapLayer, isDrawer?: boolean, initValues?: TFormDrawerValues | TFormMarkerValues) => {
            const keyTree = findKey(treeData, ['key', layer]);
            const newData: IEDataNode[] = treeData;

            if (keyTree && !newData[parseInt(keyTree)].disabled && newData[parseInt(keyTree)].children) {
                const list: IEDataNode[] = [];
                const listVectorCL = newData[parseInt(keyTree)].children;

                //set Z-index target
                if (listVectorCL) {
                    if (listVectorCL.length > 0) {
                        if (objects.length === 1) {
                            if (objects[0] instanceof CLProfile) objects[0].layer.setZIndex(listVectorCL.length + 1)

                        } else {
                            objects.forEach((profile, index) => {
                                if (profile instanceof CLProfile) profile.layer.setZIndex(index + 1)
                            })
                        }
                    } else {
                        if (objects[0] instanceof CLProfile) objects[0].layer.setZIndex(1)
                    }
                }

                objects.forEach(item => {
                    const id = item.id;
                    const key = `${layer}_${id}`;
                    const className = item instanceof CLProfile ? "tree-parent draw profile" :
                        ((item instanceof CLMeasure || item instanceof CLContour || item instanceof CLShareRegion) ? "tree-parent draw" : "tree-child");

                    list.push({
                        title: item.title,
                        key: key,
                        object: item as any,
                        className: className,
                    });
                })

                if (isReplace) {
                    newData[parseInt(keyTree)].children = list;
                } else {
                    list.forEach(item => newData[parseInt(keyTree)].children!.push(item));
                }

                setTreeData([...newData]);

                if (layer === EMapLayer.Profile && isDrawer) {
                    props.event.emit(ViewerEventName.FeatureCreated, {
                        object: objects[0],
                        initValues: initValues,
                    });
                }
            }
        }

        const onUpdateTreeNode = (evt: {
            object: CLProfile | CLMeasure,
            editFeature?: any,
            editLayer?: TFormCreateProfile,
        }) => {
            const newData: IEDataNode[] = treeData;

            if (evt.object instanceof CLProfile) {
                const keyTree = findKey(treeData, ['key', EMapLayer.Profile]);

                if (keyTree) {
                    if (evt.editFeature && evt.object.feature) {
                        const keyObj = findKey(newData[parseInt(keyTree)].children, ['key', `${EMapLayer.Profile}_${evt.object.layer.get('id')}`]);

                        if (keyObj && evt.editFeature) {
                            const childData = newData[parseInt(keyTree)].children![parseInt(keyObj)].children;

                            if (childData && childData.length > 0) {
                                const newC = childData.filter(value => value.key === `${EMapLayer.Profile}_${evt.object.id}`);
                                newC[0].title = evt.editFeature.note ?? evt.editFeature.title;

                                newData[parseInt(keyTree)].children![parseInt(keyObj)].children = [...childData];

                                setTreeData([...newData]);
                            }
                        }
                    } else if (evt.editLayer) {
                        const keyObj = findKey(newData[parseInt(keyTree)].children, ['key', `${EMapLayer.Profile}_${evt.object.layer.get('id')}`]);

                        if (keyObj) {
                            const newC = newData[parseInt(keyTree)].children![parseInt(keyObj)];

                            newC.title = evt.editLayer.name;

                            newData[parseInt(keyTree)].children![parseInt(keyObj)] = newC;

                            setTreeData([...newData]);
                        }
                    }
                }
            } else {
                const keyTree = findKey(treeData, ['key', EMapLayer.Measure]);

                if (keyTree) {
                    const keyObj = findKey(newData[parseInt(keyTree)].children, ['key', `${EMapLayer.Measure}_${evt.object.typeDraw}`]);

                    if (keyObj && evt.editFeature) {
                        const childData = newData[parseInt(keyTree)].children![parseInt(keyObj)].children;

                        if (childData && childData.length > 0) {
                            const newC = childData.filter(value => value.key === `${EMapLayer.Measure}_${evt.object.id}`);

                            newC[0].title = evt.editFeature.note ?? evt.editFeature.title;

                            newData[parseInt(keyTree)].children![parseInt(keyObj)].children = [...childData];

                            setTreeData([...newData]);
                        }
                    }
                }
            }
        }

        const _createTreeNode = (object: any, layer: EMapLayer, isFeature?: boolean) => {
            const newData: IEDataNode[] = treeData;
            const keyTree = findKey(treeData, ['key', layer]);
            const feature = object.feature as (Feature<any> | undefined);

            if (keyTree && newData[parseInt(keyTree)].children) {
                if (isFeature && feature) {
                    const idFeature = object.id;
                    const idLayer = object.layer.get('id');
                    const keyObj = findKey(newData[parseInt(keyTree)].children, ['key', `${layer}_${idLayer}`]);
                    const key = `${layer}_${idFeature}`;

                    if (keyObj) {
                        const cData = newData[parseInt(keyTree)].children![parseInt(keyObj)].children
                        const className = feature.get('type') ? "tree-child draw profile" : "tree-child draw"

                        if (!cData) {
                            newData[parseInt(keyTree)].children![parseInt(keyObj)].children = [
                                {
                                    title: object.title,
                                    key: key,
                                    object: object as any,
                                    className: className
                                }
                            ]
                        } else {
                            newData[parseInt(keyTree)].children![parseInt(keyObj)].children!.push({
                                title: object.title,
                                key: key,
                                object: object as any,
                                className: className
                            })

                            const index = cData.findIndex(object => object.key === key)

                            if (feature.get('type') && feature.getStyle()) {
                                if (feature.getStyle() instanceof Style) {
                                    (feature.getStyle() as Style).setZIndex(index + 1)
                                } else {
                                    const styleFeature = feature.get('style') as Style

                                    styleFeature.setZIndex(index + 1)

                                    feature.setStyle(styleFeature)
                                }
                            }
                        }

                        setTreeData([...newData])

                        if (!object.init) {
                            if (object instanceof CLMeasure) {
                                setExpandedKeys(prev => [...prev, `${layer}_${object.typeDraw}`])
                            } else if (object instanceof CLProfile) {
                                setExpandedKeys(prev => [...prev, `${layer}_${idLayer}`])
                            }
                        }
                    }
                } else {
                    const id = object.id;
                    const key = `${layer}_${id}`;

                    newData[parseInt(keyTree)].children!.push({
                        title: object.title,
                        key: key,
                        object: object as any
                    });
                    setTreeData([...newData]);
                }
            }
        }

        const _removeTreeNode = (object: CLInterface, layer: EMapLayer, isFeature?: boolean) => {
            const newData: IEDataNode[] = treeData;

            const keyTree = findKey(treeData, ['key', layer]);

            if (keyTree) {
                if (isFeature) {
                    const keyObj = findKey(newData[parseInt(keyTree)].children, ['key', `${layer}_${(object as CLProfile).layer.get('id')}`]);

                    if (keyObj) {
                        let newC = newData[parseInt(keyTree)].children![parseInt(keyObj)].children;

                        if (newC && newC.length > 0) {
                            newC = newC.filter(value => value.key !== `${layer}_${object.id}`);

                            newData[parseInt(keyTree)].children![parseInt(keyObj)].children = [...newC];

                            setTreeData([...newData]);
                            props.setSelectObj(undefined);
                        }
                    }
                } else {
                    let newC = newData[parseInt(keyTree)].children;

                    if (newC && newC.length > 0) {
                        newC = newC.filter(value => value.key !== `${layer}_${object.id}`);

                        newData[parseInt(keyTree)].children = [...newC];

                        setTreeData([...newData]);
                        props.setSelectObj(undefined);
                    }
                }
            }
        }

        const onEventSetsTile = (evt: TEventTileSets) => {
            console.log('onEventSetsTile', evt);

            _createTreeNodes(evt.data, evt.isReplace, EMapLayer.Tile)
        }

        const onEventSetsContour = (evt: TEventContourSets) => {
            console.log('onEventSetsContour', evt);

            _createTreeNodes(evt.data, evt.isReplace, EMapLayer.Contour)
        }

        const onEventSetsMeasure = (evt: TEventMeasureSets) => {
            console.log('onEventMeasureSet', evt);

            _createTreeNodes(evt.data, evt.isReplace, EMapLayer.Measure)
        }

        const onEventSetsProfile = (evt: TEventProfileSets) => {
            console.log('onEventSetsProfile', evt);

            _createTreeNodes(evt.data, evt.isReplace, EMapLayer.Profile, evt.isDrawer, evt.initValues)
        }

        const onEventSetsFeature = (evt: TEventFeatureSets) => {
            const newData: IEDataNode[] = treeData;

            for (let i = 0, length = evt.length; i < length; i++) {
                const object = evt[i]
                const layer = EMapLayer.Profile

                const keyTree = findKey(treeData, ['key', layer]);
                const feature = object.feature as (Feature<any> | undefined);

                if (keyTree && newData[parseInt(keyTree)].children) {
                    if (feature) {
                        const idFeature = object.id;
                        const idLayer = object.layer.get('id');
                        const keyObj = findKey(newData[parseInt(keyTree)].children, ['key', `${layer}_${idLayer}`]);
                        const key = `${layer}_${idFeature}`;

                        if (keyObj) {
                            const className = feature.get('type') ? "tree-child draw profile" : "tree-child draw"

                            newData[parseInt(keyTree)].children![parseInt(keyObj)].children = [
                                {
                                    title: object.title,
                                    key: key,
                                    object: object as any,
                                    className: className
                                }
                            ]
                        }
                    }
                }
            }
            setTreeData([...newData])
        }

        const onEventAddProfile = (evt: TEventProfileAdded) => {
            console.log('onEventAddProfile', evt);

            _createTreeNode(evt, EMapLayer.Profile);
        }

        const onEventRemoveProfile = (evt: TEventProfileRemoved) => {
            console.log('onEventRemoveProfile', evt);

            _removeTreeNode(evt, EMapLayer.Profile);
        }

        const onEventAddFeature = (evt: TEventProfileAdded) => {
            console.log('onEventAddFeature', evt);

            _createTreeNode(evt, EMapLayer.Profile, true);
        }

        const onEventRemoveFeature = (evt: TEventProfileRemoved) => {
            console.log('onEventRemoveFeature', evt);

            _removeTreeNode(evt, EMapLayer.Profile, true);
        }

        const onEventAddShare = (evt: TEventRegionSets) => {
            console.log('onEventSetsShare', evt);

            _createTreeNodes(evt.data, evt.isReplace, EMapLayer.Profile, evt.isDrawer)
        }

        const onEventRemoveRegion = (evt: TEventRegionRemoved) => {
            console.log('onEventRemoveShare', evt);

            _removeTreeNode(evt, EMapLayer.Profile, false);
        }

        const onEventAddMeasure = (evt: TEventMeasureAdded) => {
            console.log('onEventAddMeasure', evt);

            _createTreeNode(evt, EMapLayer.Measure, true);
        }

        const onEventRemoveMeasure = (evt: TEventMeasureRemoved) => {
            console.log('onEventRemoveMeasure', evt);

            _removeTreeNode(evt, EMapLayer.Measure, true);
        }

        const onEventAddTile = (evt: TEventTileAdded) => {
            console.log('onEventAddTile', evt);

            _createTreeNode(evt, EMapLayer.Tile);
        }

        const onEventAddContour = (evt: TEventContourAdded) => {
            console.log('onEventAddContour', evt);

            _createTreeNode(evt, EMapLayer.Contour);
        }

        const onTreeSelect = (selectedKeys: Key[], info: IOTreeSelectInfo) => {
            console.log('onTreeSelect', info.node.object)

            const object = info.node.object;

            if (!object) {
                return;
            }

            const split = info.node.key.toString().split('_');

            if (split.length === 1) {

                props.setSelectObj(undefined);

                switch (split[0]) {
                    case EMapLayer.Tile:
                    case EMapLayer.Contour:
                        if (object instanceof LayerGroup && object.getVisible()) {
                            const c1 = object.get('center');

                            if (c1 && RootUtils.isArray(c1, 0) && !isNaN(c1[0]) && !isNaN(c1[1])) {
                                removeAction(props.viewer);

                                props.viewer.map.getView().animate({
                                    center: c1,
                                    duration: 1000,
                                    zoom: props.viewer.map.getView().getZoom(),
                                });
                            }
                        }

                        break;
                }
            } else if (split.length === 2) {

                const selected = !info.node.selected;

                if (!selected) {
                    props.setSelectObj(undefined);
                } else {
                    let center: number[] | undefined;

                    if (
                        object instanceof CLMeasure
                        || object instanceof CLProfile
                        || object instanceof CLShareRegion
                        || object instanceof CLContour
                    ) {
                        props.setSelectObj({
                            ...props.selectInfo,
                            object: object,
                        })

                        if (object.getVisible() && !(object instanceof CLContour)) {
                            const feature = object.feature;

                            if (feature) {
                                let typeGeom: EDrawType

                                if (object instanceof CLProfile) {
                                    typeGeom = feature.get('typeDraw')

                                } else {
                                    typeGeom = feature.getGeometry()?.getType()

                                }

                                if (typeGeom === EDrawType.Circle) {
                                    if (feature.get('circle').center) center = feature.get('circle').center

                                } else if (typeGeom === EDrawType.LineString || typeGeom === EDrawType.Polygon) {
                                    const geometry = feature.getGeometry();

                                    if (geometry) center = getCenter(geometry.getExtent())

                                } else {
                                    center = (feature.getGeometry() as Point).getCoordinates()
                                }
                            }
                        }
                    }

                    if (object instanceof CLTile) {
                        if (object.getVisible()) {
                            const c2 = object.object.get('center');

                            if (c2 && RootUtils.isArray(c2, 0)) {
                                center = c2;
                            }
                        }
                    }

                    if (center && center.length > 1 && !isNaN(center[0]) && !isNaN(center[1])) {
                        removeAction(props.viewer);

                        props.viewer.map.getView().animate({
                            center: center,
                            duration: 1000,
                            zoom: props.viewer.map.getView().getZoom(),
                        });

                    }
                }
            }
            setSelectedKeys(selectedKeys);
        }

        const onTreeExpand = (expandedKeysValue: Key[]) => setExpandedKeys(expandedKeysValue);

        const onTreeCheck = (keys: Key[], info: IOTreeCheckInfo) => {
            const split = info.node.key.toString().split('_');

            if (split.length === 1) {
                const checked = !info.node.checked;

                if (split[0] !== EMapLayer.Profile) {
                    const object = info.node.object;

                    if (
                        object
                        && (
                            object instanceof LayerGroup
                            || object instanceof VectorLayer
                        )
                    ) {
                        object.setVisible(checked);
                    }
                }

                // set all children by parent
                if (info.node.children && info.node.children.length > 0) {
                    info.node.children.forEach((item: IEDataNode) => {

                        if (item.object && "setVisible" in item.object) {
                            item.object.setVisible(checked);
                        }
                    })
                }
            } else if (split.length === 2) {
                const checked = !info.node.checked;
                const object = info.node.object;

                if (object instanceof CLProfile && object.feature) {

                    object.feature.setStyle(function (feature) {
                        if (feature.get('hidden')) {
                            return null
                        } else {
                            return feature.get('style')
                        }
                    })

                    if (checked) {
                        object.feature.set('hidden', false)
                    } else {
                        object.feature.set('hidden', true)
                    }

                } else if (object instanceof CLMeasure && object.feature) {

                    object.feature.setStyle(function (feature) {

                        if (feature.get('hidden')) {
                            return null
                        } else {
                            return (StyleMeasureFn((feature as any), true, false, object.typeDraw) as any)
                        }
                    })

                    if (checked) {
                        object.feature.set('hidden', false)
                    } else {
                        object.feature.set('hidden', true)
                    }
                } else {
                    if (checked) {
                        const parent: IEDataNode | undefined = treeData.find((item) => item.key === split[0]);

                        // show tree parent if parent hidden
                        if (parent) {
                            if (parent.object instanceof VectorLayer || parent.object instanceof LayerGroup) {
                                if (!parent.object.getVisible()) {

                                    parent.object.setVisible(true)
                                }
                            }
                        }
                    }

                    if (object && "setVisible" in object) {
                        object.setVisible(checked);
                    }

                    const contourCl = props.viewer.contourTool.children;

                    const clVisible = filter(contourCl, function (clContour) {
                        return !clContour.layer.getVisible()
                    });

                    if (clVisible.length === contourCl.length) {
                        props.setStatusContour(false)
                    } else if (clVisible.length === 0) {
                        props.setStatusContour(true)
                    }
                }
            }

            setCheckedKeys([...keys]);
        }

        const onOpenAttributesTable = () => setIsAttributesTableVisible(true)

        const getPropertiesPanel = useMemo(() => {
            console.log('selectInfo', props.selectInfo)

            if (props.selectInfo) {
                const propertiesPanel = (
                    <PropertiesPanelFC
                        viewer={props.viewer}
                        object={props.selectInfo.object}
                    />
                )

                if (props.selectInfo.object instanceof CLProfile && keyMenuLayer === EMenuMapItem.ProfileLayer) {
                    return propertiesPanel

                } else if (props.selectInfo.object instanceof CLMeasure && keyMenuLayer === EMenuMapItem.MeasureLayer) {
                    return propertiesPanel

                }
            } else {
                return null;
            }

            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [props.selectInfo, keyMenuLayer])

        const getDrawToolBar = useMemo(() => {
            if (
                (keyMenuLayer === EMenuMapItem.MeasureLayer || keyMenuLayer === EMenuMapItem.ProfileLayer)
                && !DetectMedia()
            ) {
                return (
                    <DrawToolsPanelFC
                        viewer={props.viewer}
                        keyMenuLayer={keyMenuLayer}
                        event={props.event}
                    />
                );
            } else {
                return null;
            }

            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [keyMenuLayer])

        const getControlDrawToolBar = useMemo(() => {
            if (
                keyMenuLayer === EMenuMapItem.MeasureLayer
                || (keyMenuLayer === EMenuMapItem.ProfileLayer && FeatureSingleton.getInstance().v2DProfile)
            ) {
                return <ControlDrawToolbarFC
                    viewer={props.viewer}
                    keyMenuLayer={keyMenuLayer}
                    selected={props.selectInfo ? props.selectInfo.object : undefined}
                    event={props.event}
                    onOpenAttributesTable={onOpenAttributesTable}
                />
            }
                // else if (keyMenuLayer === EMenuMapItem.ContourLayer && IsFunc.v2DProfile) {
                //
                //     return <ContourToolBar
                //         selected={props.selectInfo ? props.selectInfo.object : undefined}
                //         viewer={props.viewer}
                //
                //     />
            // }
            else {
                return null
            }

            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [props.selectInfo, keyMenuLayer])

        const getBaseLayer = useMemo(() => {
            if (keyMenuLayer === EMenuMapItem.BaseLayer) {
                return <SwitchBaseLayerFC viewer={props.viewer}/>
            }

            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [keyMenuLayer])

        const onDragEnd = (info: IEVentDragNode) => {
            const keyTree = findKey(treeData, ['key', EMapLayer.Profile]);
            const objectNode = info.node.object;

            if (!keyTree || !(objectNode instanceof CLProfile)) {
                return
            }

            const idCLProfile = `${EMapLayer.Profile}_${objectNode.layer.get('id')}`;

            if (info.node.className && info.node.className.split(' ')[0] === 'tree-parent') {

                const childData = treeData[parseInt(keyTree)].children;

                if (childData && childData.length > 0) {
                    childData.forEach((data, index) => {

                        (data.object as CLProfile).layer.setZIndex(index + 1)
                    })
                }
            } else {
                const keyObj = findKey(treeData[parseInt(keyTree)].children, ['key', idCLProfile]);

                if (keyObj) {
                    const childData = treeData[parseInt(keyTree)].children![parseInt(keyObj)].children;

                    if (childData && childData.length > 0) {
                        childData.forEach((data, index) => {
                            const feature = (data.object as CLProfile).feature;

                            if (!feature) {
                                return
                            }
                            (feature.getStyle() as Style).setZIndex(index + 1)
                        })
                    }
                }
            }
            updateView(props.viewer)
        }

        const onDrop = (info: IEEventDropNode) => {
            const dropKey = info.node.key;
            const dragKey = info.dragNode.key;
            const dropPos = info.node.pos.split('-');
            const dropPosition = info.dropPosition - Number(dropPos[dropPos.length - 1]);
            const dragNode = info.dragNode;
            const dropNode = info.node;

            if (!(dropNode.object instanceof CLProfile)) {
                return
            }

            const isSameLevel = (a: IEEventDataNode, b: IEEventDataNode) => {
                if (!a.props || !a.props.pos || !b.props || !b.props.pos) {
                    return false
                }

                const aLevel = a.props.pos.split('-').length
                const bLevel = b.props.pos.split('-').length
                return aLevel === bLevel
            }

            const isSameParent = (a: IEEventDataNode, b: IEEventDataNode) => {
                if (!a.props || !a.props.pos || !b.props || !b.props.pos) {
                    return false
                }
                const aLevel = a.props.pos.split('-')
                const bLevel = b.props.pos.split('-')
                aLevel.pop()
                bLevel.pop()
                return aLevel.join('') === bLevel.join('')
            }

            const canDrop = isSameLevel(dragNode, dropNode) && isSameParent(dragNode, dropNode) && info.dropToGap;

            if (!canDrop) {
                return
            }

            const data = [...treeData]; // Find dragObject
            let dragObj: any;

            const loop = (data: any, key: any, callback: any) => {
                for (let i = 0; i < data.length; i++) {
                    if (data[i].key === key) {
                        return callback(data[i], i, data);
                    }

                    if (data[i].children) {
                        loop(data[i].children, key, callback);
                    }
                }
            };

            loop(data, dragKey, (item: any, index: any, arr: any) => {
                arr.splice(index, 1);
                dragObj = item;
            });

            if (!info.dropToGap) {
                // Drop on the content
                loop(data, dropKey, (item: any) => {
                    item.children = item.children || []; // where to insert 示例添加到头部，可以是随意位置

                    item.children.unshift(dragObj);
                });
            } else if (info.node && info.node.props && (info.node.props.children || []).length > 0 &&
                info.node.props.expanded && dropPosition === 1 // On the bottom gap
            ) {
                loop(data, dropKey, (item: any) => {
                    item.children = item.children || []; // where to insert 示例添加到头部，可以是随意位置

                    item.children.unshift(dragObj); // in previous version, we use item.children.push(dragObj) to insert the
                    // item to the tail of the children
                });
            } else {
                let ar: any = [];
                let i: any;
                loop(data, dropKey, (_item: any, index: any, arr: any) => {
                    ar = arr;
                    i = index;
                });

                if (dropPosition === -1) {
                    ar.splice(i, 0, dragObj);
                } else {
                    ar.splice(i + 1, 0, dragObj);
                }
            }

            setTreeData(data);
        };

        const loadProject = useMemo(() => {
            if (isInit) {
                return <LoadProjectFC {...props}/>;
            }

            return null;

            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [isInit]);

        return (
            <>
                {getDrawToolBar}
                {loadProject}
                <Tree
                    className={`tree-v2d-layer ${keyMenuLayer}`}
                    autoExpandParent={false}
                    onExpand={onTreeExpand}
                    checkable
                    expandedKeys={expandedKeys}
                    onCheck={onTreeCheck as unknown as any}
                    checkedKeys={checkedKeys}
                    onSelect={onTreeSelect}
                    selectedKeys={selectedKeys}
                    treeData={treeData}
                    showIcon={true}
                    draggable={(node: AntTreeNodeProps) => {
                        if (!node.key) {
                            return false
                        } else {
                            const key = node.key.toString().split('_')[0];

                            return (key === EMapLayer.Profile || key === EMapLayer.ShareRegion)
                        }
                    }}
                    blockNode
                    onDragEnd={onDragEnd as any}
                    onDrop={onDrop as any}
                    onRightClick={props.onRightClickFeature as any}
                />
                {DetectMedia() ? null : getPropertiesPanel}
                {getControlDrawToolBar}
                {getBaseLayer}
                {
                    isAttributesTableVisible && props.selectInfo
                        ? <AttributeTableModalFC
                            viewer={props.viewer}
                            onCancel={() => setIsAttributesTableVisible(false)}
                            object={props.selectInfo.object}/>
                        : null
                }
            </>
        )
    }
)

export const setTreeDisplay = (data: IEDataNode[], status: EStatusTreeItem) => {
    data.forEach(v1 => {
        v1.style = {
            display: status
        }
        if (v1.children) {
            v1.children.forEach(v2 => {
                v2.style = {
                    display: status
                }
                if (v2.children) {
                    v2.children.forEach(v3 => {
                        v3.style = {
                            display: status
                        }
                    })
                }
            })
        }
    })
}
