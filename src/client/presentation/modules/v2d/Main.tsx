import React, {useCallback, useEffect, useLayoutEffect, useMemo, useState} from "react";
import {useLocation, useNavigate} from "react-router";
import {Map2DModel} from "../../../models/service/geodetic/Map2DModel";
import {App, DetectMedia} from "./const/App";
import {Coordinate} from "ol/coordinate";
import {Drawer, Menu, Tabs, Tooltip} from "antd";
import {MapLayerFC} from "./components/MapLayerFC";
import {UrlHash} from "../../../core/UrlHash";
import EventEmitter from "eventemitter3";
import {TParamPartGeodetic} from "../../../const/Types";
import {Viewer} from "./viewer/Viewer";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import {EDrawType, EMapLayer, EMenuMapItem, EProfileType, EToolsEdit, IEDataNode} from "./const/Defines";
import {useTranslation} from "react-i18next";
import LayerGroup from "ol/layer/Group";
import {MapEventName, ViewerEventName} from "./const/Event";
import {MenuInfo} from "rc-menu/lib/interface";
import Image from "next/image";
import {Images} from "../../../const/Images";
import {StyleMeasureFn} from "./components/profile/StyleDrawFn";
import {CreateFeatureProfileDrawerFC} from "./components/profile/CreateFeatureProfileDrawerFC";
import {CLProfile} from "./viewer/clim/CLProfileTool";
import {TFormDrawerValues, TFormMarkerValues} from "./viewer/clim/CLTypeTool";
import {removeAction} from "./components/tool/MapAction";
import {ProfileMarkerModalFC} from "./components/profile/ProfileMarkerModalFC";
import {View} from "ol";
import {Utils2D} from "./core/Utils";
import {CustomButton} from "../../components/CustomButton";
import sms from "./styles/MapView.module.scss";
import {CLMeasure} from "./viewer/clim/CLMeasureTool";
import {PropertiesPanelFC} from "./components/PropertiesPanelFC";
import {CustomTypography} from "../../components/CustomTypography";
import {OptionsMenu} from "./const/OptionsMenu";
import {ModifyCharacteristics, ModifyGeometry, RemoveUserDraw} from "./components/tool/InteractiveFeature";
import {CLShareRegion} from "./viewer/clim/CLShareRegionTool";
import {GeodeticPartMenuFC} from "../../widgets/GeodeticPartMenuFC";
import {FeatureSingleton} from "../../../models/FeatureSingleton";
import {useConfigContext} from "../../contexts/ConfigContext";
import {Color} from "../../../const/Color";
import {DoubleClickZoom, KeyboardZoom, MouseWheelZoom} from "ol/interaction";

export const Main = React.memo((props: {
        m2dId: number,
        item: Map2DModel,
        parts?: TParamPartGeodetic,
        viewer: Viewer
        event: EventEmitter,
        headerHeightRef: number,
        onCloseMapLayer: () => void,
    }) => {
        const {t} = useTranslation()
        const navigate = useNavigate()
        const location = useLocation()
        const [config] = useConfigContext()

        const URLH = new UrlHash(location.hash);
        const urlCoord = URLH.get('c', []);
        const urlZoom = URLH.get('s', '');
        const [hashParams, setHashParams] = useState({
            c: urlCoord,
            s: urlZoom,
        });

        const defaultWidthDrawer = localStorage.getItem("drawer-main-width");

        const [isInit, setIsInit] = useState<boolean>(false);
        const [isModalPartVisible, setIsModalPartVisible] = useState(false);
        const [selectInfo, setSelectInfo] = useState<{ object: any }>({object: undefined});
        const [width, setWidth] = useState(defaultWidthDrawer !== null ? parseInt(defaultWidthDrawer) : 300);
        const [drawerTitle, setDrawerTitle] = useState<React.ReactNode>();
        const [isResizing, setIsResizing] = useState(false);
        const [isPanelModalVisible, setIsPanelModalVisible] = useState(false);
        const [isContourVisible, setIsContourVisible] = useState(false);
        const [isContourHelpVisible, setIsContourHelpVisible] = useState(true);

        const [isCreateFeature, setIsCreateFeature] = useState<{
            visible: boolean,
            object?: any,
            initValues?: any
        }>({visible: false});
        const [isMapLayerVisible, setIsMapLayerVisible] = useState<{
            visible: boolean,
            keyLayer?: EMenuMapItem,
        }>({
            visible: false,
            keyLayer: EMenuMapItem.Map2DLayer,
        });

        const [isContextFeature, setIsContextFeature] = useState<{
            visible: boolean,
            x: number,
            y: number,
            object?: CLProfile | CLMeasure | CLShareRegion,
            isRegion?: boolean
        }>({
            visible: false,
            x: 0,
            y: 0
        })

        const removeContextFeature = () => setIsContextFeature({...isContextFeature, visible: false, x: 0, y: 0})

        // Init coord & zoom
        useEffect(() => {
            let zoom: number;
            let center: Coordinate;

            if (urlZoom.length > 0) {
                zoom = urlZoom;
            } else if (props.item.info?.zoom !== undefined) {
                zoom = props.item.info?.zoom;
            } else {
                zoom = App.defaultZoom;
            }

            if (urlCoord.length > 0) {
                center = [parseFloat(urlCoord[0]), parseFloat(urlCoord[1])];
            } else if (props.item.info?.center !== undefined) {
                center = [props.item.info?.center.lon!, props.item.info?.center.lat!];
            } else {
                center = App.defaultCenter;
            }

            props.viewer.map.setView(new View({
                center: center,
                zoom: zoom,
                projection: App.DefaultEPSG,
            }));

            // disable set navigate hash WebAppInApp
            if (config.agent?.isWebAppInApp()) {
                console.log(`%cDisable Init & Tracking`, Color.ConsoleWarning)
            } else {
                props.viewer.map.on(MapEventName.MoveEnd, updateHashMap)
            }

            if (isContextFeature.visible) {
                window.onclick = () => removeContextFeature();
            }

            //implement zoom
            props.viewer.map.addInteraction(new MouseWheelZoom({duration: 1000}))
            props.viewer.map.addInteraction(new DoubleClickZoom({duration: 1000}))
            props.viewer.map.addInteraction(new KeyboardZoom({duration: 1000}))

            return () => {
                if (!config.agent?.isWebAppInApp()) {
                    props.viewer.map.un(MapEventName.MoveEnd, updateHashMap)
                }
            }

            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, []);

        useEffect(() => {
            props.event.on(ViewerEventName.ContourChangeStatus, onChangeStatusContour);

            return () => {
                props.event.off(ViewerEventName.ContourChangeStatus, onChangeStatusContour);
            }

            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [isContourVisible])

        useLayoutEffect(() => {
            if (isInit) {

                props.event.on(ViewerEventName.DrawerOpened, switchMenuLayer);
                props.event.on(ViewerEventName.DrawerClosed, onCloseLayerDrawer);

                props.event.on(ViewerEventName.FeatureCreated, onCreateFeature);

                return () => {

                    props.event.off(ViewerEventName.DrawerOpened, switchMenuLayer);
                    props.event.off(ViewerEventName.DrawerClosed, onCloseLayerDrawer);

                    props.event.off(ViewerEventName.FeatureCreated, onCreateFeature);
                }
            }
            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [isInit]);

        useEffect(() => {
            localStorage.setItem("drawer-main-width", width.toString());

            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [width])

        useEffect(() => {
            setIsCreateFeature({
                visible: false
            })

            removeAction(props.viewer)

            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [selectInfo]);


        useEffect(() => {
            document.addEventListener('mousemove', onDrawerMouseMove);
            document.addEventListener('mouseup', onDrawerMouseUp);

            return () => {
                document.removeEventListener('mousemove', onDrawerMouseMove);
                document.removeEventListener('mouseup', onDrawerMouseUp);
            };

            // eslint-disable-next-line react-hooks/exhaustive-deps
        });

        useEffect(() => {
            if (isMapLayerVisible.keyLayer) {
                switch (isMapLayerVisible.keyLayer) {
                    case EMenuMapItem.Map2DLayer:
                        setDrawerTitle(t('text.orthogonalImage'))

                        break;
                    case EMenuMapItem.BaseLayer:

                        setDrawerTitle(t('text.baseMap'))

                        break;
                    case EMenuMapItem.ProfileLayer:
                        setDrawerTitle(
                            <>
                                {t('text.layerProfiles')}
                                {
                                    (!DetectMedia() && FeatureSingleton.getInstance().v2DProfile) && (
                                        <Tooltip
                                            title={t('text.guide2dEditor')}
                                            arrowPointAtCenter={false}
                                            placement={"right"}
                                            className={'ml-2'}
                                        >
                                            <Image
                                                src={Images.iconQuestion.data}
                                                alt={Images.iconQuestion.atl}
                                                width={16}
                                                height={16}
                                            />
                                        </Tooltip>
                                    )
                                }
                            </>
                        )

                        break;
                    case EMenuMapItem.MeasureLayer:
                        setDrawerTitle(t('text.layerMeasures'))

                        break;
                    case EMenuMapItem.ContourLayer:
                        setDrawerTitle(t('text.contourMap'))

                        break;
                }
            }

            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [isMapLayerVisible.keyLayer]);

        const updateHashMap = useCallback(() => {
            const mapView = props.viewer.map.getView();
            const urlHashParams = new UrlHash(hashParams);

            const center = mapView.getCenter();

            if (center) {
                urlHashParams.set('c', center);
            }

            const zoom = mapView.getZoom();

            if (zoom) {
                urlHashParams.set('s', zoom.toFixed(2));
            }

            navigate({
                hash: urlHashParams.toString()
            }, {
                replace: true
            })

            setHashParams(urlHashParams.toObject());

            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [props.viewer]);

        // Create Layers
        useEffect(() => {
            // Create BaseMap
            {
                const layerGroup = new LayerGroup({
                    // @ts-ignore
                    className: 'ol-layer',
                    crossOrigin: 'anonymous',
                    layers: App.BaseLayerGroup,
                    properties: {
                        title: t('text.baseMap'),
                        id: EMapLayer.BaseMap
                    }
                });

                props.viewer.map.addLayer(layerGroup);
                props.viewer.baseMapTool.setLayer(layerGroup);
            }
            // Create Tile
            {
                const layerGroup = new LayerGroup({
                    // @ts-ignore
                    className: 'ol-layer',
                    crossOrigin: 'anonymous',
                    layers: [],
                    properties: {
                        title: t('text.tileMap'),
                        id: EMapLayer.Tile,
                    }
                })

                props.viewer.map.addLayer(layerGroup);
                props.viewer.tileTool.setLayer(layerGroup);
            }
            // Create Contour
            {
                const layerGroup = new LayerGroup({
                    // @ts-ignore
                    className: 'ol-layer',
                    layers: [],
                    preload: Infinity,
                    visible: false,
                    properties: {
                        title: t('text.contourMap'),
                        id: EMapLayer.Contour
                    }
                });

                props.viewer.map.addLayer(layerGroup);
                props.viewer.contourTool.setGroupLayer(layerGroup);
            }
            // Create Marker
            {
                const vectorLayer = new VectorLayer({
                    className: 'ol-layer',
                    source: new VectorSource({
                        features: []
                    }),
                    properties: {
                        title: t('text.vr360Marker'),
                        id: EMapLayer.Marker
                    },
                    visible: !!props.item.info?.markers,
                    declutter: true,
                    zIndex: 999,
                });

                props.viewer.map.addLayer(vectorLayer);
                props.viewer.markerTool.setLayer(vectorLayer);
            }
            // Create Measure
            {
                const layerGroup = new LayerGroup({
                    // @ts-ignore
                    className: 'ol-layer',
                    layers: [],
                    visible: true,
                    properties: {
                        title: t('text.layerMeasures'),
                        id: EMapLayer.Measure
                    }
                })

                props.viewer.map.addLayer(layerGroup);
                props.viewer.measureTool.setLayerGroup(layerGroup);
            }
            // Create Profile
            {
                const layerGroup = new LayerGroup({
                    // @ts-ignore
                    className: 'ol-layer',
                    layers: [],
                    visible: true,
                    properties: {
                        title: t('text.layerProfiles'),
                        id: EMapLayer.Profile
                    }
                })

                props.viewer.map.addLayer(layerGroup);
                props.viewer.profileTool.setLayer(layerGroup);
            }
            // Create ShareRegion
            {
                const vectorLayer = new VectorLayer({
                    className: 'ol-layer',
                    source: new VectorSource({
                        features: []
                    }),
                    properties: {
                        id: EMapLayer.ShareRegion,
                        title: t('text.share2D')
                    },
                    style: Utils2D.styleDrawRegion(),
                    visible: true,
                });

                props.viewer.map.addLayer(vectorLayer);
                props.viewer.shareTool.setLayer(vectorLayer);
            }

            setIsInit(true);

            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [])

        const setSelectObj = (data: any) => {
            setSelectInfo(data);

            if (data && data.object instanceof CLMeasure && data.object.feature) setIsPanelModalVisible(true);
        };

        const onCloseLayerDrawer = () => setIsMapLayerVisible({
            ...isMapLayerVisible,
            visible: false
        });

        const onCreateFeature = (evt: {
            object: CLProfile,
            initValues?: TFormDrawerValues | TFormMarkerValues
        }) => {
            setIsCreateFeature({
                visible: true,
                object: evt.object,
                initValues: evt.initValues
            });
        };

        const switchMenuLayer = (evt: EMenuMapItem) => {
            setIsMapLayerVisible({
                visible: true,
                keyLayer: evt,
            })
        };

        const onClickMenuItem = (evt?: MenuInfo) => {
            if (evt) {
                if (isMapLayerVisible.visible && evt.key === isMapLayerVisible.keyLayer) {
                    setIsMapLayerVisible({visible: false})
                } else {
                    switch (evt.key as EMenuMapItem) {
                        default:
                            setIsMapLayerVisible({
                                visible: true,
                                keyLayer: evt.key as EMenuMapItem
                            })

                            break;
                        case EMenuMapItem.Part:
                            setIsModalPartVisible(true)

                            break;
                    }
                }
            } else {
                setIsMapLayerVisible({
                    ...isMapLayerVisible,
                    visible: true
                    // keyLayer: EMenuMapItem.Map2DLayer
                })
            }
        }

        const onClosePartModal = () => setIsModalPartVisible(false);

        useEffect(() => {
            if (props.viewer.measureTool.layer) {
                props.viewer.measureTool.layer.getLayers().forEach((layer: VectorLayer<VectorSource<any>>) => {
                    layer.setStyle((feature: any) => StyleMeasureFn(feature, true, false, EDrawType.LineString))
                })
            }

            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [])

        const onDrawerMouseDown = () => setIsResizing(true);

        const onDrawerMouseUp = () => setIsResizing(false);

        const onDrawerMouseMove = (e: MouseEvent) => {
            if (isResizing) {
                let offsetLeft = e.clientX - document.body.offsetLeft;

                const minWidth = 300;
                const maxWidth = 600;
                if (offsetLeft > minWidth && offsetLeft < maxWidth) {
                    setWidth(offsetLeft);
                }
            }
        };

        const getPanelDrawer = useMemo(() => {
            if (
                selectInfo
                && selectInfo.object instanceof CLMeasure
                && selectInfo.object.feature
                && DetectMedia()
            ) {
                if (isMapLayerVisible.visible) setIsMapLayerVisible({visible: false})

                return (
                    <Drawer
                        className={'drawer-v2d-main'}
                        visible={isPanelModalVisible}
                        width={'100vh'}
                        closable
                        destroyOnClose
                        bodyStyle={{padding: 0}}
                        footer={null}
                        mask={false}
                        placement={"bottom"}
                        onClose={() => setIsPanelModalVisible(false)}
                        closeIcon={<Image
                            height={24}
                            width={24}
                            src={Images.iconXWhite.data}
                            alt={Images.iconXWhite.atl}
                        />}
                    >
                        <PropertiesPanelFC
                            viewer={props.viewer}
                            object={selectInfo.object}
                        />
                    </Drawer>
                )
            }

            return null;

            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [selectInfo, isPanelModalVisible]);

        const onChangeStatusContour = () => {
            setIsContourVisible(!isContourVisible)
            setIsContourHelpVisible(false)
        };

        const onRightClickFeature = (info: {
            event: React.MouseEvent;
            node: IEDataNode;
        }) => {
            info.event.preventDefault();

            if (((info.node.object instanceof CLMeasure || info.node.object instanceof CLProfile) && info.node.object.feature)
                || info.node.object instanceof CLShareRegion) {

                const isRegion = info.node.object instanceof CLShareRegion;

                setIsContextFeature({
                    ...isContextFeature,
                    visible: true,
                    x: info.event.clientX,
                    y: info.event.clientY,
                    object: info.node.object,
                    isRegion: isRegion
                });
            } else {
                removeContextFeature();
            }
        }

        return (
            isInit
                ? <>
                    {
                        isContextFeature.visible && (
                            <div
                                key={'menu-context-feature'}
                                style={{
                                    left: `${isContextFeature.x - 55}px`,
                                    position: "absolute",
                                    top: `${isContextFeature.y - 55}px`,
                                    zIndex: 20,
                                    border: "1px solid #f0f0f0"
                                }}
                            >
                                <Menu
                                    items={OptionsMenu.itemsContextFeature({
                                        t: t,
                                        isRegion: isContextFeature.isRegion
                                    })}
                                    className={'menu-v2d menu-context'}
                                    selectedKeys={[]}
                                    onClick={({key}) => {
                                        switch (key as EToolsEdit) {
                                            case EToolsEdit.Delete:
                                                RemoveUserDraw({
                                                    viewer: props.viewer,
                                                    selected: isContextFeature.object
                                                })

                                                break;
                                            case EToolsEdit.EditGeometry:

                                                const modifyStyle = Utils2D.styleDrawModify();
                                                modifyStyle.getText().setText(t('text.dragToModify'));

                                                ModifyGeometry({
                                                    viewer: props.viewer,
                                                    selected: isContextFeature.object,
                                                    modifyStyle: modifyStyle,
                                                })

                                                break;

                                            case EToolsEdit.EditCharacteristics:

                                                ModifyCharacteristics({
                                                    viewer: props.viewer,
                                                    selected: isContextFeature.object,
                                                    event: props.event,
                                                })

                                                break;
                                        }

                                        removeContextFeature()
                                    }}
                                />
                            </div>
                        )
                    }
                    {
                        DetectMedia()
                            ? <>
                                {
                                    FeatureSingleton.getInstance().isContributor
                                        ? <CustomButton
                                            className={'btn-popover-menu-web'}
                                            type={'text'}
                                            onClick={() => onClickMenuItem()}
                                            icon={
                                                <Image
                                                    src={Images.iconList.data}
                                                    alt={Images.iconList.atl}
                                                    width={24}
                                                    height={24}
                                                />
                                            }
                                        />
                                        : <CustomButton
                                            className={'btn-popover-menu-app'}
                                            onClick={() => onClickMenuItem()}
                                            type={'outline'}
                                            icon={
                                                <Image
                                                    src={Images.iconList.data}
                                                    alt={Images.iconList.atl}
                                                    width={24}
                                                    height={24}
                                                />
                                            }
                                        />
                                }
                            </>
                            : <Menu
                                style={{height: `calc(100vh - ${props.headerHeightRef}px)`}}
                                theme={'dark'}
                                onClick={onClickMenuItem}
                                className={'menu-v2d menu-main-geodetic'}
                                selectedKeys={[isMapLayerVisible.keyLayer as string]}
                                items={OptionsMenu.mainMenuItems({
                                    t: t,
                                    viewer: props.viewer,
                                    parts: props.parts
                                })}
                            />
                    }
                    <Drawer
                        className={'drawer-v2d-main'}
                        width={!DetectMedia() ? width : '240px'}
                        style={{
                            height: `calc(100vh - ${props.headerHeightRef}px)`,
                            top: `${props.headerHeightRef}px`,
                            zIndex: 1,
                        }}
                        visible={isMapLayerVisible.visible}
                        title={
                            <CustomTypography
                                textStyle={"text-20-24"}
                                isStrong
                                color={"#FFFFFF"}
                            >
                                {drawerTitle}
                            </CustomTypography>
                        }
                        onClose={() => setIsMapLayerVisible({
                            ...isMapLayerVisible,
                            visible: false
                        })}
                        closable
                        mask={DetectMedia()}
                        maskStyle={{
                            backgroundColor: "transparent"
                        }}
                        bodyStyle={{padding: 0}}
                        placement="left"
                        closeIcon={<Image
                            height={24}
                            width={24}
                            src={Images.iconXWhite.data}
                            alt={Images.iconXWhite.atl}
                        />}
                        forceRender
                        maskClosable={DetectMedia()}
                    >
                        <div
                            className={sms.CursorResizeDrawer}
                            onMouseDown={onDrawerMouseDown}
                        />
                        {
                            DetectMedia() && <Tabs
                                onChange={(key: EMenuMapItem) => onClickMenuItem({key: key} as MenuInfo)}
                                className={'tabs-main ant-tabs-v2d menu-item'}
                                size={'small'}
                                defaultActiveKey={isMapLayerVisible.keyLayer}
                            >
                                {
                                    OptionsMenu.mainMenuItemsMobile({
                                        t,
                                        parts: props.parts,
                                        viewer: props.viewer
                                    }).map(item => {
                                        return (
                                            <Tabs.TabPane
                                                tab={item.icon}
                                                key={item.key}
                                            />
                                        )
                                    })
                                }
                            </Tabs>
                        }
                        <MapLayerFC
                            item={props.item}
                            viewer={props.viewer}
                            event={props.event}
                            keyLayer={isMapLayerVisible.keyLayer}
                            setSelectObj={setSelectObj}
                            selectInfo={selectInfo}
                            statusContour={isContourVisible}
                            setStatusContour={(status: boolean) => setIsContourVisible(status)}
                            onRightClickFeature={onRightClickFeature}
                        />
                    </Drawer>
                    {
                        (isCreateFeature.visible && isCreateFeature.object) && (
                            <>
                                {
                                    isCreateFeature.object.type !== EProfileType.Marker
                                        ? <CreateFeatureProfileDrawerFC
                                            viewer={props.viewer}
                                            onClose={() => setIsCreateFeature({
                                                visible: false
                                            })}
                                            type={isCreateFeature.object.typeDraw}
                                            headerHeightRef={props.headerHeightRef}
                                            object={isCreateFeature.object}
                                            initValues={isCreateFeature.initValues as TFormDrawerValues}
                                            event={props.event}
                                        />
                                        : <ProfileMarkerModalFC
                                            viewer={props.viewer}
                                            onClose={() => setIsCreateFeature({visible: false})}
                                            selectedInfo={isCreateFeature.object}
                                            initValues={isCreateFeature.initValues as TFormMarkerValues}
                                            event={props.event}
                                        />
                                }
                            </>
                        )
                    }
                    {
                        isModalPartVisible && (
                            <GeodeticPartMenuFC
                                parts={props.parts}
                                active={{
                                    type: 'm2d',
                                    id: props.m2dId
                                }}
                                isVisible={isModalPartVisible}
                                onClose={onClosePartModal}
                            />
                        )
                    }
                    {
                        getPanelDrawer
                    }
                    {
                        (props.viewer.contourTool.children.length >= 1) && (
                            <>
                                <CustomButton
                                    type={'outline'}
                                    className={'btn-shortcut-v2d contour'}
                                    icon={
                                        isContourVisible
                                            ? <Image
                                                className={'btn-ct-active'}
                                                height={35}
                                                width={35}
                                                src={Images.iconContourActive.data}
                                                alt={Images.iconContourActive.atl}
                                            />
                                            : <Tooltip
                                                title={t('text.contourMap')}
                                                overlayClassName={sms.ContourHelper}
                                                arrowPointAtCenter
                                                visible={isContourHelpVisible}
                                            >
                                                <Image
                                                    className={'btn-ct-disable'}
                                                    height={35}
                                                    width={35}
                                                    src={Images.iconContourDisabled.data}
                                                    alt={Images.iconContourDisabled.atl}
                                                />
                                            </Tooltip>
                                    }
                                    onClick={onChangeStatusContour}
                                />
                            </>
                        )
                    }
                </>
                :
                null
        )
    }
)
