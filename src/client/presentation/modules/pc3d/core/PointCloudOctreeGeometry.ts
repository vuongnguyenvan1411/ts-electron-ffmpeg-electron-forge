import {Box3, BufferGeometry, Sphere, Vector3} from "three";
import {PointCloudTreeNode} from "./PointCloudTree";
import {XHRFactory} from "./XHRFactory";
import {Utils} from "./Utils";
import {App} from "./App";
import {PointAttributes} from "../loader/PointAttributes";
import {TPointCloud} from "../../../../models/service/geodetic/Map3DModel";
import {Utils as RootUtils} from "../../../../core/Utils";
import {BinaryLoader} from "../loader/BinaryLoader";
import {LasLazLoader} from "../loader/LasLazLoader";

export class PointCloudOctreeGeometry {
    url: string;
    octreeDir: string;
    spacing: number;
    boundingBox: Box3;
    root: PointCloudOctreeGeometryNode;
    nodes: Record<string, PointCloudOctreeGeometryNode>;
    pointAttributes: PointAttributes;
    hierarchyStepSize: number;
    loader: BinaryLoader | LasLazLoader;
    projection: string;
    tightBoundingBox: Box3;
    boundingSphere: Sphere;
    tightBoundingSphere: Sphere;
    offset: Vector3;
    fallbackProjection: any;

    constructor() {
        // this.url = null;
        // this.octreeDir = null;
        this.spacing = 0;
        // this.boundingBox = null;
        // this.root = null;
        // this.nodes = null;
        // this.pointAttributes = null;
        this.hierarchyStepSize = -1;
        // this.loader = null;
    }
}

export class PointCloudOctreeGeometryNode extends PointCloudTreeNode {
    static IDCount: number = 0;

    id: number;
    name: string;
    index: number;
    pcoGeometry: PointCloudOctreeGeometry;
    geometry: BufferGeometry | null;
    boundingBox: Box3;
    boundingSphere: Sphere;
    children: Record<number, PointCloudOctreeGeometryNode>;
    numPoints: number;
    level: number;
    loaded: boolean;
    oneTimeDisposeHandlers: any[];
    loading: boolean;
    hasChildren: boolean;
    parent: PointCloudOctreeGeometryNode;
    spacing: number;
    mean: Vector3;
    tightBoundingBox: Box3;
    estimatedSpacing: GLfloat;
    buffer: any;
    density: any;
    protected model: TPointCloud;

    constructor(name: string, pcoGeometry: PointCloudOctreeGeometry, boundingBox: Box3) {
        super();

        this.id = PointCloudOctreeGeometryNode.IDCount++;
        this.name = name;
        this.index = parseInt(name.charAt(name.length - 1));
        this.pcoGeometry = pcoGeometry;
        this.geometry = null;
        this.boundingBox = boundingBox;
        this.boundingSphere = boundingBox.getBoundingSphere(new Sphere());
        this.children = {};
        this.numPoints = 0;
        // this.level = null;
        this.loaded = false;
        this.oneTimeDisposeHandlers = [];
    }

    setModel(model: TPointCloud) {
        this.model = model;
    }

    getModel(): TPointCloud {
        return this.model
    }

    isGeometryNode() {
        return true;
    }

    getLevel() {
        return this.level;
    }

    isTreeNode() {
        return false;
    }

    isLoaded() {
        return this.loaded;
    }

    getBoundingSphere() {
        return this.boundingSphere;
    }

    getChildren() {
        const children = [];

        for (let i = 0; i < 8; i++) {
            if (this.children[i]) {
                children.push(this.children[i]);
            }
        }

        return children;
    }

    getBoundingBox() {
        return this.boundingBox;
    }

    getURL() {
        let url = '';

        let version = this.pcoGeometry.loader.version;

        if (version.equalOrHigher('1.5')) {
            url = this.pcoGeometry.octreeDir + '/' + this.getHierarchyPath() + '/' + this.name;
        } else if (version.equalOrHigher('1.4')) {
            url = this.pcoGeometry.octreeDir + '/' + this.name;
        } else if (version.upTo('1.3')) {
            url = this.pcoGeometry.octreeDir + '/' + this.name;
        }

        return url;
    }

    getHierarchyPath() {
        let path = 'r/';

        let hierarchyStepSize = this.pcoGeometry.hierarchyStepSize;
        let indices = this.name.substr(1);

        let numParts = Math.floor(indices.length / hierarchyStepSize);
        for (let i = 0; i < numParts; i++) {
            path += indices.substr(i * hierarchyStepSize, hierarchyStepSize) + '/';
        }

        path = path.slice(0, -1);

        return path;
    }

    addChild(child: PointCloudOctreeGeometryNode) {
        this.children[child.index] = child;
        child.parent = this;
    }

    load() {
        if (this.loading === true || this.loaded === true || App.numNodesLoading >= App.maxNodesLoading) {
            return;
        }

        this.loading = true;

        App.numNodesLoading++;

        if (this.pcoGeometry.loader.version.equalOrHigher('1.5')) {
            if ((this.level % this.pcoGeometry.hierarchyStepSize) === 0 && this.hasChildren) {
                this.loadHierarchyThenPoints();
            } else {
                this.loadPoints();
            }
        } else {
            this.loadPoints();
        }
    }

    loadPoints() {
        this.pcoGeometry.loader.load(this);
    }

    loadHierarchyThenPoints() {
        const node = this;

        // load hierarchy
        const callback = (node: PointCloudOctreeGeometryNode, hBuffer: ArrayBufferLike) => {
            let tStart = performance.now();
            let view = new DataView(hBuffer);
            let stack = [];
            let children = view.getUint8(0);
            let numPoints = view.getUint32(1, true);

            node.numPoints = numPoints;
            stack.push({children: children, numPoints: numPoints, name: node.name});

            const decoded = [];
            let offset = 5;

            while (stack.length > 0) {
                let sNode: any = stack.shift();
                let mask = 1;

                for (let i = 0; i < 8; i++) {
                    if ((sNode.children & mask) !== 0) {
                        let childName = sNode.name + i;
                        let childChildren = view.getUint8(offset);
                        let childNumPoints = view.getUint32(offset + 1, true);

                        stack.push({children: childChildren, numPoints: childNumPoints, name: childName});
                        decoded.push({children: childChildren, numPoints: childNumPoints, name: childName});

                        offset += 5;
                    }

                    mask = mask * 2;
                }

                if (offset === hBuffer.byteLength) {
                    break;
                }
            }

            // console.log(decoded);

            let nodes: any = {};
            nodes[node.name] = node;
            let pco = node.pcoGeometry;

            for (let i = 0; i < decoded.length; i++) {
                let name = decoded[i].name;
                let decodedNumPoints = decoded[i].numPoints;
                let index = parseInt(name.charAt(name.length - 1));
                let parentName = name.substring(0, name.length - 1);
                let parentNode = nodes[parentName];
                let level = name.length - 1;
                let boundingBox = Utils.createChildAABB(parentNode.boundingBox, index);
                let currentNode = new PointCloudOctreeGeometryNode(name, pco, boundingBox);
                currentNode.setModel(node.getModel());

                currentNode.level = level;
                currentNode.numPoints = decodedNumPoints;
                currentNode.hasChildren = decoded[i].children > 0;
                currentNode.spacing = pco.spacing / Math.pow(2, level);
                parentNode.addChild(currentNode);
                nodes[name] = currentNode;
            }

            let duration = performance.now() - tStart;

            if (duration > 5) {
                let msg = `duration: ${duration}ms, numNodes: ${decoded.length}`;
                console.log(msg);
            }

            node.loadPoints();
        };

        if ((node.level % node.pcoGeometry.hierarchyStepSize) === 0) {
            // const hurl = node.pcoGeometry.octreeDir + "/../hierarchy/" + node.name + ".hrc";
            let hurl: string;

            if (node.model) {
                hurl = RootUtils.cdnGdtAsset({
                    path: "pc",
                    data: {
                        i: this.model.id,
                        p: `${node.pcoGeometry.octreeDir}/${node.getHierarchyPath()}/${node.name}.hrc`,
                        v: node.model.v
                    }
                })

                console.log('Load hrc:', `${node.getHierarchyPath()}/${node.name}.hrc`);
            } else {
                hurl = `${node.pcoGeometry.octreeDir}/${node.getHierarchyPath()}/${node.name}.hrc`;
            }

            const xhr = XHRFactory.createXMLHttpRequest();

            xhr.open('GET', hurl, true);
            xhr.responseType = 'arraybuffer';
            xhr.overrideMimeType('text/plain; charset=x-user-defined');
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200 || xhr.status === 0) {
                        const hBuffer = xhr.response;
                        callback(node, hBuffer);
                    } else {
                        console.log('Failed to load file! HTTP status: ' + xhr.status + ', file: ' + hurl);

                        App.numNodesLoading--;
                    }
                }
            };

            try {
                xhr.send(null);
            } catch (e) {
                console.log('Error loading point cloud: ' + e);
            }
        }
    }

    getNumPoints() {
        return this.numPoints;
    }

    dispose() {
        if (this.geometry && this.parent != null) {
            this.geometry.dispose();
            this.geometry = null;
            this.loaded = false;

            this.dispatchEvent({type: 'dispose'});

            for (let i = 0; i < this.oneTimeDisposeHandlers.length; i++) {
                let handler = this.oneTimeDisposeHandlers[i];
                handler();
            }

            this.oneTimeDisposeHandlers = [];
        }
    }
}
