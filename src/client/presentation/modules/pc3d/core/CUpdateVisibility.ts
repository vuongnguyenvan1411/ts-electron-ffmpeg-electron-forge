import {Frustum, Matrix4, PerspectiveCamera, Plane, Vector3, WebGLRenderer} from "three";
import {ClipMethod, ClipTask} from "./Defines";
import {Box3Helper} from "../utils/Box3Helper";
import {App} from "./App";
import {PointCloudOctree} from "./PointCloudOctree";
import {BinaryHeap} from "./BinaryHeap";

export function updatePointClouds(pointClouds: PointCloudOctree[], camera: PerspectiveCamera, renderer: WebGLRenderer) {
    for (let pointcloud of pointClouds) {
        const start = performance.now();

        for (let profileRequest of pointcloud.profileRequests) {
            profileRequest.update();

            const duration = performance.now() - start;

            if (duration > 5) {
                break;
            }
        }

        // let duration = performance.now() - start;
    }

    const result = updateVisibility(pointClouds, camera, renderer);

    for (let pointcloud of pointClouds) {
        pointcloud.updateMaterial(pointcloud.material, camera, renderer);
        pointcloud.updateVisibleBounds();
    }

    App.lru.freeMemory();

    return result;
}

export function updateVisibilityStructures(pointClouds: PointCloudOctree[], camera: PerspectiveCamera) {
    const frustums = [];
    const camObjPositions = [];
    const priorityQueue = new BinaryHeap(function (x: any) {
        return 1 / x.weight;
    });

    for (let i = 0; i < pointClouds.length; i++) {
        const pointCloud = pointClouds[i];

        if (!pointCloud.initialized()) {
            continue;
        }

        pointCloud.numVisibleNodes = 0;
        pointCloud.numVisiblePoints = 0;
        pointCloud.deepestVisibleLevel = 0;
        pointCloud.visibleNodes = [];
        pointCloud.visibleGeometry = [];

        // frustum in object space
        camera.updateMatrixWorld();
        const frustum = new Frustum();
        const viewI = camera.matrixWorldInverse;
        const world = pointCloud.matrixWorld;

        // use close near plane for frustum intersection
        const frustumCam = camera.clone();
        frustumCam.near = Math.min(camera.near, 0.1);
        frustumCam.updateProjectionMatrix();
        const proj = camera.projectionMatrix;

        const fm = new Matrix4().multiply(proj).multiply(viewI).multiply(world);
        frustum.setFromProjectionMatrix(fm);
        frustums.push(frustum);

        // camera position in object space
        const view = camera.matrixWorld;
        const worldI = world.clone().invert();
        const camMatrixObject = new Matrix4().multiply(worldI).multiply(view);
        const camObjPos = new Vector3().setFromMatrixPosition(camMatrixObject);
        camObjPositions.push(camObjPos);

        if (pointCloud.visible && pointCloud.root !== null) {
            priorityQueue.push({
                pointcloud: i,
                node: pointCloud.root,
                weight: Number.MAX_VALUE
            });
        }

        // hide all previously visible nodes
        // if(pointCloud.root instanceof PointCloudOctreeNode){
        //	pointCloud.hideDescendants(pointCloud.root.sceneNode);
        // }
        if (pointCloud.root && pointCloud.root.isTreeNode()) {
            pointCloud.hideDescendants(pointCloud.root.sceneNode);
        }

        for (let j = 0; j < pointCloud.boundingBoxNodes.length; j++) {
            pointCloud.boundingBoxNodes[j].visible = false;
        }
    }

    return {
        frustums: frustums,
        camObjPositions: camObjPositions,
        priorityQueue: priorityQueue
    };
}


export function updateVisibility(pointClouds: PointCloudOctree[], camera: PerspectiveCamera, renderer: WebGLRenderer) {
    // let numVisibleNodes = 0;
    let numVisiblePoints = 0;

    const numVisiblePointsInPointClouds = new Map(pointClouds.map((pc: any) => [pc, 0]));

    const visibleNodes = [];
    const visibleGeometry = [];
    const unloadedGeometry = [];

    let lowestSpacing = Infinity;

    // calculate object space frustum and cam pos and setup priority queue
    const s = updateVisibilityStructures(pointClouds, camera);
    const frustums = s.frustums;
    const camObjPositions = s.camObjPositions;
    const priorityQueue = s.priorityQueue;

    let loadedToGPUThisFrame = 0;

    // let domWidth = renderer.domElement.clientWidth;
    const domHeight = renderer.domElement.clientHeight;

    // check if pointcloud has been transformed
    // some code will only be executed if changes have been detected
    if (!App._pointcloudTransformVersion) {
        App._pointcloudTransformVersion = new Map();
    }

    const pointcloudTransformVersion = App._pointcloudTransformVersion;

    for (let pointcloud of pointClouds) {
        if (!pointcloud.visible) {
            continue;
        }

        pointcloud.updateMatrixWorld();

        if (!pointcloudTransformVersion.has(pointcloud)) {
            pointcloudTransformVersion.set(pointcloud, {number: 0, transform: pointcloud.matrixWorld.clone()});
        } else {
            const version = pointcloudTransformVersion.get(pointcloud);

            if (!version.transform.equals(pointcloud.matrixWorld)) {
                version.number++;
                version.transform.copy(pointcloud.matrixWorld);

                pointcloud.dispatchEvent({
                    type: "transformation_changed",
                    target: pointcloud
                });
            }
        }
    }

    while (priorityQueue.size() > 0) {
        const element = priorityQueue.pop();
        let node = element.node;
        const parent = element.parent;
        const pointcloud = pointClouds[element.pointcloud];

        // { // restrict to certain nodes for debugging
        //	let allowedNodes = ["r", "r0", "r4"];
        //	if(!allowedNodes.includes(node.name)){
        //		continue;
        //	}
        // }

        const box = node.getBoundingBox();
        const frustum = frustums[element.pointcloud];
        const camObjPos = camObjPositions[element.pointcloud];

        const insideFrustum = frustum.intersectsBox(box);
        const maxLevel = pointcloud.maxLevel || Infinity;
        const level = node.getLevel();
        let visible = insideFrustum;
        visible = visible && !(numVisiblePoints + node.getNumPoints() > App.pointBudget);
        visible = visible && !(numVisiblePointsInPointClouds.get(pointcloud) + node.getNumPoints() > pointcloud.pointBudget);
        visible = visible && level < maxLevel;
        visible = visible || node.getLevel() <= 2;

        const clipBoxes = pointcloud.material.clipBoxes;

        if (clipBoxes.length > 0) {
            let numIntersecting = 0;
            let numIntersectionVolumes = 0;

            //if(node.name === "r60"){
            //	var a = 10;
            //}

            clipBoxes.forEach(() => {
                const pcWorldInverse = pointcloud.matrixWorld.clone().invert();
                // let toPCObject = pcWorldInverse.multiply(clipBox.box.matrixWorld);

                const px = new Vector3(+0.5, 0, 0).applyMatrix4(pcWorldInverse);
                const nx = new Vector3(-0.5, 0, 0).applyMatrix4(pcWorldInverse);
                const py = new Vector3(0, +0.5, 0).applyMatrix4(pcWorldInverse);
                const ny = new Vector3(0, -0.5, 0).applyMatrix4(pcWorldInverse);
                const pz = new Vector3(0, 0, +0.5).applyMatrix4(pcWorldInverse);
                const nz = new Vector3(0, 0, -0.5).applyMatrix4(pcWorldInverse);

                const pxN = new Vector3().subVectors(nx, px).normalize();
                const nxN = pxN.clone().multiplyScalar(-1);
                const pyN = new Vector3().subVectors(ny, py).normalize();
                const nyN = pyN.clone().multiplyScalar(-1);
                const pzN = new Vector3().subVectors(nz, pz).normalize();
                const nzN = pzN.clone().multiplyScalar(-1);

                const pxPlane = new Plane().setFromNormalAndCoplanarPoint(pxN, px);
                const nxPlane = new Plane().setFromNormalAndCoplanarPoint(nxN, nx);
                const pyPlane = new Plane().setFromNormalAndCoplanarPoint(pyN, py);
                const nyPlane = new Plane().setFromNormalAndCoplanarPoint(nyN, ny);
                const pzPlane = new Plane().setFromNormalAndCoplanarPoint(pzN, pz);
                const nzPlane = new Plane().setFromNormalAndCoplanarPoint(nzN, nz);

                //if(window.debugdraw !== undefined && window.debugdraw === true && node.name === "r60"){

                //	Utils.debugPlane(viewer.scene.scene, pxPlane, 1, 0xFF0000);
                //	Utils.debugPlane(viewer.scene.scene, nxPlane, 1, 0x990000);
                //	Utils.debugPlane(viewer.scene.scene, pyPlane, 1, 0x00FF00);
                //	Utils.debugPlane(viewer.scene.scene, nyPlane, 1, 0x009900);
                //	Utils.debugPlane(viewer.scene.scene, pzPlane, 1, 0x0000FF);
                //	Utils.debugPlane(viewer.scene.scene, nzPlane, 1, 0x000099);

                //	Utils.debugBox(viewer.scene.scene, box, new Matrix4(), 0x00FF00);
                //	Utils.debugBox(viewer.scene.scene, box, pointcloud.matrixWorld, 0xFF0000);
                //	Utils.debugBox(viewer.scene.scene, clipBox.box.boundingBox, clipBox.box.matrixWorld, 0xFF0000);

                //	window.debugdraw = false;
                //}

                const frustum = new Frustum(pxPlane, nxPlane, pyPlane, nyPlane, pzPlane, nzPlane);
                const intersects = frustum.intersectsBox(box);

                if (intersects) {
                    numIntersecting++;
                }

                numIntersectionVolumes++;
            })

            // for (let clipBox of clipBoxes) {
            //
            // }

            const insideAny = numIntersecting > 0;
            const insideAll = numIntersecting === numIntersectionVolumes;

            if (pointcloud.material.clipTask === ClipTask.SHOW_INSIDE) {
                if (pointcloud.material.clipMethod === ClipMethod.INSIDE_ANY && insideAny) {
                    //node.debug = true
                } else if (pointcloud.material.clipMethod === ClipMethod.INSIDE_ALL && insideAll) {
                    //node.debug = true;
                } else {
                    visible = false;
                }
            } else if (pointcloud.material.clipTask === ClipTask.SHOW_OUTSIDE) {
                //if(pointcloud.material.clipMethod === ClipMethod.INSIDE_ANY && !insideAny){
                //	//visible = true;
                //	let a = 10;
                //}else if(pointcloud.material.clipMethod === ClipMethod.INSIDE_ALL && !insideAll){
                //	//visible = true;
                //	let a = 20;
                //}else{
                //	visible = false;
                //}
            }
        }

        // visible = ["r", "r0", "r06", "r060"].includes(node.name);
        // visible = ["r"].includes(node.name);

        if (node.spacing) {
            lowestSpacing = Math.min(lowestSpacing, node.spacing);
        } else if (node.geometryNode && node.geometryNode.spacing) {
            lowestSpacing = Math.min(lowestSpacing, node.geometryNode.spacing);
        }

        if (numVisiblePoints + node.getNumPoints() > App.pointBudget) {
            break;
        }

        if (!visible) {
            continue;
        }

        // TODO: not used, same as the declaration?
        // numVisibleNodes++;
        numVisiblePoints += node.getNumPoints();
        const numVisiblePointsInPointcloud = numVisiblePointsInPointClouds.get(pointcloud);
        numVisiblePointsInPointClouds.set(pointcloud, numVisiblePointsInPointcloud + node.getNumPoints());

        pointcloud.numVisibleNodes++;
        pointcloud.numVisiblePoints += node.getNumPoints();

        if (node.isGeometryNode() && (!parent || parent.isTreeNode())) {
            if (node.isLoaded() && loadedToGPUThisFrame < 2) {
                node = pointcloud.toTreeNode(node, parent);
                loadedToGPUThisFrame++;
            } else {
                unloadedGeometry.push(node);
                visibleGeometry.push(node);
            }
        }

        if (node.isTreeNode()) {
            App.lru.touch(node.geometryNode);
            node.sceneNode.visible = true;
            node.sceneNode.material = pointcloud.material;

            visibleNodes.push(node);
            pointcloud.visibleNodes.push(node);

            if (node._transformVersion === undefined) {
                node._transformVersion = -1;
            }

            const transformVersion = pointcloudTransformVersion.get(pointcloud);

            if (node._transformVersion !== transformVersion.number) {
                node.sceneNode.updateMatrix();
                node.sceneNode.matrixWorld.multiplyMatrices(pointcloud.matrixWorld, node.sceneNode.matrix);
                node._transformVersion = transformVersion.number;
            }

            if (pointcloud.showBoundingBox && !node.boundingBoxNode && node.getBoundingBox) {
                const boxHelper = new Box3Helper(node.getBoundingBox());
                boxHelper.matrixAutoUpdate = false;
                pointcloud.boundingBoxNodes.push(boxHelper);
                node.boundingBoxNode = boxHelper;
                node.boundingBoxNode.matrix.copy(pointcloud.matrixWorld);
            } else if (pointcloud.showBoundingBox) {
                node.boundingBoxNode.visible = true;
                node.boundingBoxNode.matrix.copy(pointcloud.matrixWorld);
            } else if (!pointcloud.showBoundingBox && node.boundingBoxNode) {
                node.boundingBoxNode.visible = false;
            }

            // if(node.boundingBoxNode !== undefined && exports.debug.allowedNodes !== undefined){
            // 	if(!exports.debug.allowedNodes.includes(node.name)){
            // 		node.boundingBoxNode.visible = false;
            // 	}
            // }
        }

        // add child nodes to priorityQueue
        const children = node.getChildren();

        for (let i = 0; i < children.length; i++) {
            const child = children[i];

            let weight = 0;

            if (camera.isPerspectiveCamera) {
                const sphere = child.getBoundingSphere();
                const center = sphere.center;
                //let distance = sphere.center.distanceTo(camObjPos);

                const dx = camObjPos.x - center.x;
                const dy = camObjPos.y - center.y;
                const dz = camObjPos.z - center.z;

                const dd = dx * dx + dy * dy + dz * dz;
                const distance = Math.sqrt(dd);

                const radius = sphere.radius;

                const fov = (camera.fov * Math.PI) / 180;
                const slope = Math.tan(fov / 2);
                const projFactor = (0.5 * domHeight) / (slope * distance);
                const screenPixelRadius = radius * projFactor;

                if (screenPixelRadius < pointcloud.minimumNodePixelSize) {
                    continue;
                }

                weight = screenPixelRadius;

                if (distance - radius < 0) {
                    weight = Number.MAX_VALUE;
                }
            } else {
                // TODO ortho visibility
                const bb = child.getBoundingBox();
                // let distance = child.getBoundingSphere().center.distanceTo(camObjPos);
                // let diagonal = bb.max.clone().sub(bb.min).length();
                // weight = diagonal / distance;

                weight = bb.max.clone().sub(bb.min).length();
            }

            priorityQueue.push({pointcloud: element.pointcloud, node: child, parent: node, weight: weight});
        }
    } // end priority queue loop

    { // update DEM
        const maxDEMLevel = 4;
        // let candidates = pointClouds.filter(p => (p.generateDEM && p.dem instanceof Potree.DEM));
        const candidates = pointClouds.filter((p: any) => (p.generateDEM));

        for (let pointcloud of candidates) {
            const updatingNodes = pointcloud.visibleNodes.filter((n: any) => n.getLevel() <= maxDEMLevel);
            pointcloud.dem.update(updatingNodes);
        }
    }

    for (let i = 0; i < Math.min(App.maxNodesLoading, unloadedGeometry.length); i++) {
        unloadedGeometry[i].load();
    }

    return {
        visibleNodes: visibleNodes,
        numVisiblePoints: numVisiblePoints,
        lowestSpacing: lowestSpacing
    };
}
