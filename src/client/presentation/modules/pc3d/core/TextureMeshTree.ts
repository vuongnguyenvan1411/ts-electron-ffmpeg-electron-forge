import {EventDispatcher} from "./EventDispatcher";
import {DoubleSide, Mesh, Texture} from "three";
import {Viewer} from "../viewer/viewer";

export class TextureMeshTree extends EventDispatcher {
    name: string;
    mesh: Mesh;
    texture: Texture;
    position: Float32Array;
    scene: any;

    constructor(result: any, viewer: Viewer) {
        super();

        const name = result.name;
        const mesh = result.mesh;
        const texture = result.texture;
        const position = result.position;

        this.name = name;
        this.position = position;

        if (mesh instanceof Mesh) {
            if (texture instanceof Texture) {
                mesh.traverse((child: any) => {
                    texture.anisotropy = viewer.renderer.capabilities.getMaxAnisotropy();
                    child.material.map = texture;
                    child.material.map.needsUpdate = true;

                    child.castShadow = true;
                    child.receiveShadow = true;
                    child.material.side = DoubleSide;
                })
            }

            this.scene = mesh;
        } else {
            this.scene = mesh.scene;
        }

        if (position != null) {
            this.scene.position.set(...position);
            this.scene.position.z += 1;
        }


        this.scene.visible = true;
    }

    getName() {
        return this.name;
    }

    getPosition() {
        return this.position;
    }

    getScene() {
        return this.scene;
    }
}
