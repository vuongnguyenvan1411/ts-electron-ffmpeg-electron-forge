export class EnumItem {
    name: any;
    value: any;

    constructor(object: any) {
        for (let key of Object.keys(object)) {
            // @ts-ignore
            this[key] = object[key];
        }
    }

    inspect() {
        return `Enum(${this.name}: ${this.value})`;
    }
}

export class Enum {
    object: any;

    constructor(object: any) {
        this.object = object;

        for (let key of Object.keys(object)) {
            let value = object[key];

            if (typeof value === "object") {
                value.name = key;
            } else {
                value = {name: key, value: value};
            }

            // @ts-ignore
            this[key] = new EnumItem(value);
        }
    }

    fromValue(value: any) {
        for (let key of Object.keys(this.object)) {
            // @ts-ignore
            if (this[key].value === value) {
                // @ts-ignore
                return this[key];
            }
        }

        throw new Error(`No enum for value: ${value}`);
    }
}
