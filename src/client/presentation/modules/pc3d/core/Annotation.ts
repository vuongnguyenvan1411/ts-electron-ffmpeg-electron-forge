import {Box3, MathUtils, Vector3} from "three";
import {Action} from "./Actions";
import {Utils} from "./Utils";
import {EventDispatcher} from "./EventDispatcher";
import {Viewer} from "../viewer/viewer";
import {Easing, Tween} from "@tweenjs/tween.js";
import {AnnotationEvent, MouseEventName, ViewerEventName} from "./Event";
import IconClose from "../../../../assets/image/v3d/close.svg";

export class Annotation extends EventDispatcher {
    scene: any;
    protected _title: string;
    protected _description: string;
    offset: Vector3;
    uuid: string;
    position: Vector3 | null;
    cameraPosition: Vector3;
    cameraTarget: Vector3;
    radius: number;
    view: any;
    keepOpen: boolean;
    descriptionVisible: boolean;
    showDescription: boolean;
    actions: any[];
    isHighlighted: boolean;
    protected _visible: boolean;
    protected __visible: boolean;
    protected _display: boolean;
    protected _expand: boolean;
    collapseThreshold: any | number;
    children: Annotation[];
    parent: any;
    boundingBox: Box3;
    domElement: HTMLDivElement;
    elTitleBar: HTMLDivElement;
    elTitle: HTMLSpanElement;
    elDescription: HTMLDivElement;
    elDescriptionClose: HTMLSpanElement;
    elDescriptionContent: HTMLSpanElement;
    handles: any;
    name: string = '';
    protected _properties: Record<string, any> = {};

    constructor(args: any = {}) {
        super();

        this.scene = null;
        this._title = args.title || 'No Title';
        this._description = args.description || '';
        this.offset = new Vector3();
        this.uuid = MathUtils.generateUUID();

        if (!args.position) {
            this.position = null;
        } else if (args.position.x != null) {
            this.position = args.position;
        } else {
            this.position = new Vector3(...args.position);
        }

        this.cameraPosition = (args.cameraPosition instanceof Array) ? new Vector3().fromArray(args.cameraPosition) : args.cameraPosition;
        this.cameraTarget = (args.cameraTarget instanceof Array) ? new Vector3().fromArray(args.cameraTarget) : args.cameraTarget;
        this.radius = args.radius;
        this.view = args.view || null;
        this.keepOpen = false;
        this.descriptionVisible = false;
        this.showDescription = true;
        this.actions = args.actions || [];
        this.isHighlighted = false;
        this._visible = true;
        this.__visible = true;
        this._display = true;
        this._expand = false;
        this.collapseThreshold = [args.collapseThreshold, 100].find(e => e !== undefined);

        this.children = [];
        this.parent = null;
        this.boundingBox = new Box3();

        this.domElement = document.createElement('div');
        this.domElement.className = "annotation";
        this.domElement.setAttribute("oncontextmenu", "return false;");

        this.elTitleBar = document.createElement('div');
        this.elTitleBar.className = "annotation-title-bar";
        this.domElement.append(this.elTitleBar);

        this.elTitle = document.createElement('span');
        this.elTitle.className = "annotation-label";
        this.elTitle.append(this._title);
        this.elTitleBar.append(this.elTitle);

        this.elDescription = document.createElement('div');
        this.elDescription.className = "annotation-description";
        this.domElement.append(this.elDescription);

        this.elDescriptionClose = document.createElement('span');
        this.elDescriptionClose.className = "annotation-description-close";
        this.elDescriptionClose.innerHTML = `<img src="${IconClose}" alt="" width="16px" style="cursor: pointer;" />`;
        this.elDescription.append(this.elDescriptionClose);

        this.elDescriptionContent = document.createElement('span');
        this.elDescriptionContent.className = "annotation-description-content";
        this.elDescriptionContent.append(this._description);
        this.elDescription.append(this.elDescriptionContent);

        this.elTitle.addEventListener(MouseEventName.Click, this.clickTitle);

        this.actions = this.actions.map(a => {
            if (a instanceof Action) {
                return a;
            } else {
                return new Action(a);
            }
        });

        for (let action of this.actions) {
            action.pairWith(this);
        }

        this.actions.filter(a => a.showIn === undefined || a.showIn.includes('scene')).forEach((action) => {
            const elButton: HTMLImageElement = document.createElement('img');
            elButton.className = "annotation-action-icon";
            elButton.src = action.icon;
            this.elTitleBar.append(elButton);

            elButton.addEventListener(MouseEventName.Click, () => action.onclick({annotation: this}));
        });

        this.elDescriptionClose.addEventListener(MouseEventName.Over, () => this.elDescriptionClose.style.opacity = '1');
        this.elDescriptionClose.addEventListener(MouseEventName.Out, () => this.elDescriptionClose.style.opacity = '0.5');

        this.elDescriptionClose.addEventListener(MouseEventName.Click, () => this.setHighlighted(false));

        this.domElement.addEventListener(MouseEventName.Enter, () => this.setHighlighted(true));
        this.domElement.addEventListener(MouseEventName.Leave, () => this.setHighlighted(false));

        this.domElement.addEventListener('touchstart', () => this.setHighlighted(!this.isHighlighted));

        this.display = false;
        //this.display = true;

        // this.installHandles();
    }

    protected clickTitle = () => {
        if (this.hasView()) {
            this.moveHere(this.scene.getActiveCamera());
        }

        this.dispatchEvent({
            type: MouseEventName.Click,
            target: this
        });
    }

    installHandles(viewer: Viewer) {
        console.log('Annotation.installHandles', viewer);

        if (this.handles !== undefined) {
            return;
        }

        // let domElement = $(`
        // 	<div style="position: absolute; left: 300px; top: 200px; pointer-events: none">
        // 		<svg width="300" height="600">
        // 			<line x1="0" y1="0" x2="1200" y2="200" style="stroke: black; stroke-width:2" />
        // 			<circle cx="50" cy="50" r="4" stroke="black" stroke-width="2" fill="gray" />
        // 			<circle cx="150" cy="50" r="4" stroke="black" stroke-width="2" fill="gray" />
        // 		</svg>
        // 	</div>
        // `);
        //
        // let svg = domElement.find("svg")[0];
        // let elLine = domElement.find("line")[0];
        // let elStart = domElement.find("circle")[0];
        // let elEnd = domElement.find("circle")[1];
        //
        // let setCoordinates = (start: any, end: any) => {
        //     elStart.setAttribute("cx", `${start.x}`);
        //     elStart.setAttribute("cy", `${start.y}`);
        //
        //     elEnd.setAttribute("cx", `${end.x}`);
        //     elEnd.setAttribute("cy", `${end.y}`);
        //
        //     elLine.setAttribute("x1", start.x);
        //     elLine.setAttribute("y1", start.y);
        //     elLine.setAttribute("x2", end.x);
        //     elLine.setAttribute("y2", end.y);
        //
        //     let box = svg.getBBox();
        //     svg.setAttribute("width", `${box.width}`);
        //     svg.setAttribute("height", `${box.height}`);
        //     svg.setAttribute("viewBox", `${box.x} ${box.y} ${box.width} ${box.height}`);
        //
        //     let ya = start.y - end.y;
        //     let xa = start.x - end.x;
        //
        //     if (ya > 0) {
        //         start.y = start.y - ya;
        //     }
        //     if (xa > 0) {
        //         start.x = start.x - xa;
        //     }
        //
        //     domElement.css("left", `${start.x}px`);
        //     domElement.css("top", `${start.y}px`);
        //
        // };
        //
        // $(viewer.renderArea).append(domElement);
        //
        //
        // let annotationStartPos = this.position.clone();
        // let annotationStartOffset = this.offset.clone();
        //
        // $(this.domElement).draggable({
        //     start: (event, ui) => {
        //         annotationStartPos = this.position.clone();
        //         annotationStartOffset = this.offset.clone();
        //         $(this.domElement).find(".annotation-titlebar").css("pointer-events", "none");
        //
        //         console.log($(this.domElement).find(".annotation-titlebar"));
        //     },
        //     stop: () => {
        //         $(this.domElement).find(".annotation-titlebar").css("pointer-events", "");
        //     },
        //     drag: (event, ui) => {
        //         let renderAreaWidth = viewer.renderer.getSize(new Vector2()).width;
        //         //let renderAreaHeight = viewer.renderer.getSize().height;
        //
        //         let diff = {
        //             x: ui.originalPosition.left - ui.position.left,
        //             y: ui.originalPosition.top - ui.position.top
        //         };
        //
        //         let nDiff = {
        //             x: -(diff.x / renderAreaWidth) * 2,
        //             y: (diff.y / renderAreaWidth) * 2
        //         };
        //
        //         let camera = viewer.scene.getActiveCamera();
        //         let oldScreenPos = new Vector3()
        //             .addVectors(annotationStartPos, annotationStartOffset)
        //             .project(camera);
        //
        //         let newScreenPos = oldScreenPos.clone();
        //         newScreenPos.x += nDiff.x;
        //         newScreenPos.y += nDiff.y;
        //
        //         let newPos = newScreenPos.clone();
        //         newPos.unproject(camera);
        //
        //         let newOffset = new Vector3().subVectors(newPos, this.position);
        //         this.offset.copy(newOffset);
        //     }
        // });
        //
        // const updateCallback = () => {
        //     let position = this.position;
        //     let scene = viewer.scene;
        //
        //     const renderAreaSize = viewer.renderer.getSize(new Vector2());
        //     let renderAreaWidth = renderAreaSize.width;
        //     let renderAreaHeight = renderAreaSize.height;
        //
        //     let start = this.position.clone();
        //     let end = new Vector3().addVectors(this.position, this.offset);
        //
        //     let toScreen = (position: any) => {
        //         let camera = scene.getActiveCamera();
        //         let screenPos = new Vector3();
        //
        //         let worldView = new Matrix4().multiplyMatrices(camera.projectionMatrix, camera.matrixWorldInverse);
        //         let ndc = new Vector4(position.x, position.y, position.z, 1.0).applyMatrix4(worldView);
        //         // limit w to small positive value, in case position is behind the camera
        //         ndc.w = Math.max(ndc.w, 0.1);
        //         ndc.divideScalar(ndc.w);
        //
        //         // @ts-ignore
        //         screenPos.copy(ndc);
        //         screenPos.x = renderAreaWidth * (screenPos.x + 1) / 2;
        //         screenPos.y = renderAreaHeight * (1 - (screenPos.y + 1) / 2);
        //
        //         return screenPos;
        //     };
        //
        //     start = toScreen(start);
        //     end = toScreen(end);
        //
        //     setCoordinates(start, end);
        //
        // };
        //
        // viewer.addEventListener(ViewerEventName.Update, updateCallback);
        //
        // this.handles = {
        //     domElement: domElement,
        //     setCoordinates: setCoordinates,
        //     updateCallback: updateCallback
        // };
    }

    removeHandles(viewer: Viewer) {
        if (this.handles === undefined) {
            return;
        }

        //$(viewer.renderArea).remove(this.handles.domElement);
        this.handles.domElement.remove();
        viewer.removeEventListener(ViewerEventName.Update, this.handles.updateCallback);

        delete this.handles;
    }

    get visible() {
        return this._visible;
    }

    set visible(value) {
        if (this._visible === value) {
            return;
        }

        this._visible = value;

        //this.traverse(node => {
        //	node.display = value;
        //});

        this.dispatchEvent({
            type: AnnotationEvent.VisibilityChanged,
            annotation: this
        });
    }

    get display() {
        return this._display;
    }

    set display(display) {
        if (this._display === display) {
            return;
        }

        this._display = display;

        if (display) {
            this.domElement.style.display = "block";
        } else {
            this.domElement.style.display = "none";
        }
    }

    get expand() {
        return this._expand;
    }

    set expand(expand) {
        if (this._expand === expand) {
            return;
        }

        if (expand) {
            this.display = false;
        } else {
            this.display = true;
            this.traverseDescendants((node: any) => {
                node.display = false;
            });
        }

        this._expand = expand;
    }

    get title() {
        return this._title;
    }

    set title(title) {
        if (this._title === title) {
            return;
        }

        this._title = title;

        this.elTitle.innerText = this._title;

        this.dispatchEvent({
            type: AnnotationEvent.AnnotationChanged,
            annotation: this,
        });
    }

    get description() {
        return this._description;
    }

    set description(description) {
        if (this._description === description) {
            return;
        }

        this._description = description;

        this.elDescriptionContent.innerText = description;

        this.dispatchEvent({
            type: AnnotationEvent.AnnotationChanged,
            annotation: this,
        });
    }

    add(annotation: Annotation) {
        if (!this.children.includes(annotation)) {
            this.children.push(annotation);
            annotation.parent = this;

            let descendants: any[] = [];
            annotation.traverse((a: any) => {
                descendants.push(a);
            });

            for (let descendant of descendants) {
                let c = this;

                while (c !== null) {
                    c.dispatchEvent({
                        type: AnnotationEvent.AnnotationAdded,
                        annotation: descendant
                    });

                    c = c.parent;
                }
            }
        }
    }

    level() {
        if (this.parent === null) {
            return 0;
        } else {
            return this.parent.level() + 1;
        }
    }

    hasChild(annotation: Annotation) {
        return this.children.includes(annotation);
    }

    remove(annotation: Annotation) {
        if (this.hasChild(annotation)) {
            annotation.removeAllChildren();
            annotation.dispose();
            this.children = this.children.filter(e => e !== annotation);
            annotation.parent = null;
        }
    }

    removeAllChildren() {
        this.children.forEach((child) => {
            if (child.children.length > 0) {
                child.removeAllChildren();
            }

            this.remove(child);
        });
    }

    updateBounds() {
        const box = new Box3();

        if (this.position) {
            box.expandByPoint(this.position);
        }

        for (let child of this.children) {
            child.updateBounds();
            box.union(child.boundingBox);
        }

        this.boundingBox.copy(box);
    }

    traverse(handler: Function) {
        const expand = handler(this);

        if (expand === undefined || expand === true) {
            for (const child of this.children) {
                child.traverse(handler);
            }
        }
    }

    traverseDescendants(handler: any) {
        for (const child of this.children) {
            child.traverse(handler);
        }
    }

    flatten() {
        const annotations: any[] = [];

        this.traverse((annotation: any) => {
            annotations.push(annotation);
        });

        return annotations;
    }

    descendants() {
        let annotations: any[] = [];

        this.traverse((annotation: any) => {
            if (annotation !== this) {
                annotations.push(annotation);
            }
        });

        return annotations;
    }

    setHighlighted(highlighted: boolean) {
        if (highlighted) {
            this.domElement.style.opacity = '0.8';
            this.elTitleBar.style.boxShadow = '0 0 5px #fff';
            this.domElement.style.zIndex = '1000';

            if (this._description) {
                this.descriptionVisible = true;
                this.elDescription.style.display = 'block';
                // this.elDescription.fadeIn(200);
                this.elDescription.style.position = 'relative';
            }
        } else {
            this.domElement.style.opacity = '0.5';
            this.elTitleBar.style.boxShadow = '';
            this.domElement.style.zIndex = '100';
            this.descriptionVisible = false;
            this.elDescription.style.display = 'none';
        }

        this.isHighlighted = highlighted;
    }

    hasView() {
        if (this.cameraTarget === undefined) {
            return;
        }

        let hasPosTargetView = this.cameraTarget.x != null;
        hasPosTargetView = hasPosTargetView && this.cameraPosition.x != null;

        let hasRadiusView = this.radius !== undefined;

        return hasPosTargetView || hasRadiusView;
    }

    moveHere(camera: any) {
        console.log('Annotation.moveHere', camera);

        if (!this.hasView()) {
            return;
        }

        const view = this.scene.view;
        const animationDuration = 500;
        const easing = Easing.Quartic.Out;

        let endTarget;
        if (this.cameraTarget) {
            endTarget = this.cameraTarget;
        } else if (this.position) {
            endTarget = this.position;
        } else {
            endTarget = this.boundingBox.getCenter(new Vector3());
        }

        if (this.cameraPosition) {
            const endPosition = this.cameraPosition;

            Utils.moveTo(this.scene, endPosition, endTarget);
        } else if (this.radius) {
            const direction = view.direction;
            const endPosition = endTarget.clone().add(direction.multiplyScalar(-this.radius));
            const startRadius = view.radius;
            const endRadius = this.radius;

            { // animate camera position
                let tween = new Tween(view.position).to(endPosition, animationDuration);
                tween.easing(easing);
                tween.start();
            }

            { // animate radius
                const t = {x: 0};

                const tween = new Tween(t)
                    .to({x: 1}, animationDuration)
                    .onUpdate(function () {
                        // @ts-ignore
                        view.radius = this.x * endRadius + (1 - this.x) * startRadius;
                    });
                tween.easing(easing);
                tween.start();
            }
        }
    }

    dispose() {
        if (this.domElement) {
            this.domElement.remove();
        }
    }

    toString() {
        return 'Annotation: ' + this._title;
    }

    get(key: string) {
        return this._properties[key];
    }

    set(key: string, value: any) {
        return this._properties[key] = value;
    }

    getKeys() {
        return Object.keys(this._properties);
    }

    getProperties() {
        return this._properties;
    }

    setProperties(data: Record<string, any>) {
        return this._properties = data;
    }

    hasProperties(key?: string) {
        if (key) {
            return this._properties.hasOwnProperty(key);
        } else {
            return this.getKeys.length > 0;
        }
    }
}
