export class Version {
    version: string;
    versionMajor: number;
    versionMinor: number;

    constructor(version: string) {
        this.version = version;
        const vmLength = (version.indexOf('.') === -1) ? version.length : version.indexOf('.');
        this.versionMajor = parseInt(version.substr(0, vmLength));
        this.versionMinor = parseInt(version.substr(vmLength + 1));

        if (this.versionMinor.toString().length === 0) {
            this.versionMinor = 0;
        }
    }

    newerThan(version: string) {
        const v = new Version(version);

        if (this.versionMajor > v.versionMajor) {
            return true;
        } else return this.versionMajor === v.versionMajor && this.versionMinor > v.versionMinor;
    }

    equalOrHigher(version: any) {
        let v = new Version(version);

        if (this.versionMajor > v.versionMajor) {
            return true;
        } else return this.versionMajor === v.versionMajor && this.versionMinor >= v.versionMinor;
    }

    upTo(version: string) {
        return !this.newerThan(version);
    }
}


