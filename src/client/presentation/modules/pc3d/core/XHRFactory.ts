export const XHRFactory: any = {
    config: {
        withCredentials: false,
        customHeaders: [
            {
                header: null,
                value: null
            }
        ]
    },

    createXMLHttpRequest: function () {
        const xhr = new XMLHttpRequest();

        if (this.config.customHeaders && Array.isArray(this.config.customHeaders) && this.config.customHeaders.length > 0) {
            const baseOpen = xhr.open;
            const customHeaders: any = this.config.customHeaders;

            xhr.open = function () {
                // @ts-ignore
                baseOpen.apply(this, [].slice.call(arguments));
                customHeaders.forEach(function (customHeader: any) {
                    if (!!customHeader.header && !!customHeader.value) {
                        xhr.setRequestHeader(customHeader.header, customHeader.value);
                    }
                });
            };
        }

        return xhr;
    }
}
