import {Measure} from "../utils/Measure";
import {VScene} from "../viewer/VScene";
import {BoxVolume, SphereVolume} from "../utils/Volume";
import {Profile} from "../utils/Profile";
import {Annotation} from "./Annotation";
import {CameraAnimation} from "../modules/CameraAnimation/CameraAnimation";
import {Viewer} from "../viewer/viewer";
import {PointCloudOctree} from "./PointCloudOctree";

export const CameraMode = {
    ORTHOGRAPHIC: 0,
    PERSPECTIVE: 1,
    VR: 2,
};

export const ClipTask = {
    NONE: 0,
    HIGHLIGHT: 1,
    SHOW_INSIDE: 2,
    SHOW_OUTSIDE: 3
};

export const ClipMethod = {
    INSIDE_ANY: 0,
    INSIDE_ALL: 1
};

export const ElevationGradientRepeat = {
    CLAMP: 0,
    REPEAT: 1,
    MIRRORED_REPEAT: 2,
};

export const MOUSE = {
    LEFT: 0b0001,
    RIGHT: 0b0010,
    MIDDLE: 0b0100
};

export const PointSizeType = {
    FIXED: 0,
    ATTENUATED: 1,
    ADAPTIVE: 2
};

export const PointShape = {
    SQUARE: 0,
    CIRCLE: 1,
    PARABOLOID: 2
};

export const TreeType = {
    OCTREE: 0,
    KDTREE: 1
};

export const LengthUnits = {
    METER: {code: 'm', unitspermeter: 1.0},
    FEET: {code: 'ft', unitspermeter: 3.28084},
    INCH: {code: '\u2033', unitspermeter: 39.3701}
};

export const ViewType = {
    Front: "F",
    Back: "B",
    Left: "L",
    Right: "R",
    Top: "U",
    Bottom: "D",
}

export const BackgroundType: Record<string, string> = {
    Map: "map",
    Skybox: "skybox",
    Gradient: "gradient",
    Black: "black",
    White: "white",
}

export enum ETreeLayer {
    PointCloud,
    TextureMesh,
    Measure,
    Annotation,
    Other,
    Vector,
    Image
}

export enum EBackground {
    Map,
    Skybox,
    Gradient,
    Black,
    White
}

export const DFConfig = {
    PointBudget: {
        min: 100 * 1000,
        max: 10 * 1000 * 1000,
        step: 1000,
    },
    FOV: {
        min: 20,
        max: 100,
        step: 1,
    },
    EDLRadius: {
        min: 1,
        max: 4,
        step: 0.01,
    },
    EDLStrength: {
        min: 0,
        max: 5,
        step: 0.01,
    },
    EDLOpacity: {
        min: 0,
        max: 1,
        step: 0.01,
    },
    MinNodeSize: {
        min: 0,
        max: 1000,
        step: 0.01,
    },
    MoveSpeed: {
        min: 0,
        max: 1,
        step: 0.01,
    },
    PointSize: {
        min: 0,
        max: 3,
        step: 0.01,
    },
    MinPointSize: {
        min: 0,
        max: 3,
        step: 0.01,
    },
    PointOpacity: {
        min: 0,
        max: 1,
        step: 0.01,
    },
    RGBGamma: {
        min: 0,
        max: 4,
        step: 0.01,
    },
    RGBBrightness: {
        min: -1,
        max: 1,
        step: 0.01,
    },
    RGBContrast: {
        min: -1,
        max: 1,
        step: 0.01,
    },
    WeightRGB: {
        min: 0,
        max: 1,
        step: 0.01,
    },
    WeightIntensity: {
        min: 0,
        max: 1,
        step: 0.01,
    },
    WeightElevation: {
        min: 0,
        max: 1,
        step: 0.01,
    },
    WeightClassification: {
        min: 0,
        max: 1,
        step: 0.01,
    },
    WeightReturnNumber: {
        min: 0,
        max: 1,
        step: 0.01,
    },
    WeightSourceID: {
        min: 0,
        max: 1,
        step: 0.01,
    },
    ElevationHeightRange: {
        min: 0,
        max: 1000,
        step: 0.01,
        values: [0, 1000] as [number, number]
    },
    IntensityGamma: {
        min: 0,
        max: 4,
        step: 0.01,
    },
    IntensityBrightness: {
        min: -1,
        max: 1,
        step: 0.01,
    },
    IntensityContrast: {
        min: -1,
        max: 1,
        step: 0.01,
    },
    ExtraGamma: {
        min: 0,
        max: 4,
        step: 0.01,
    },
    ExtraBrightness: {
        min: -1,
        max: 1,
        step: 0.01,
    },
    ExtraContrast: {
        min: -1,
        max: 1,
        step: 0.01,
    },
    ProfileWidth: {
        min: 0,
        max: 10 * 1000 * 1000,
        step: 5.01,
    },
    DurationAnimation: {
        min: 0,
        max: 300,
        step: 0.01,
    },
    PlayAnimation: {
        min: 0,
        max: 1,
        step: 0.001,
        value: 0,
    },
}

export enum EViewerControl {
    Earth,
    Orbit,
    Flight,
    Heli,
}

export const EViewerTool = {
    MeasureAngle: "MeasureAngle",
    Point: "Point",
    MeasureDistance: "MeasureDistance",
    MeasureHeight: "MeasureHeight",
    MeasureCircle: "MeasureCircle",
    MeasureAzimuth: "MeasureAzimuth",
    MeasureArea: "MeasureArea",
    MeasureBoxVolume: "MeasureBoxVolume",
    MeasureSphereVolume: "MeasureSphereVolume",
    Profile: "Profile",
    RemoveMeasure: "RemoveMeasure",
    ClipVolume: "ClipVolume",
    ClipPolygon: "ClipPolygon",
    ClipScreenBox: "ClipScreenBox",
    RemoveClip: "RemoveClip",
    Animation: "Animation",
    Annotation: "Annotation"
}

export type TEventToolAdded = {
    scene: VScene,
    target: VScene,
    type: string
}

export type TEventPointCloud = {
    type: string,
    pointcloud: PointCloudOctree
}

export type TEventMeasurement = TEventToolAdded & {
    measurement: Measure,
}

export type TEventVolume = TEventToolAdded & {
    volume: BoxVolume | SphereVolume,
}

export type TEventProfile = TEventToolAdded & {
    profile: Profile,
}

export type TEventCameraAnimation = TEventToolAdded & {
    animation: CameraAnimation,
}

export type TEventAnnotation = TEventToolAdded & {
    annotation: Annotation,
    target: Annotation,
}

export type TEventBackgroundChanged = {
    type: string;
    viewer: Viewer;
    background: string;
}

export type TSliderOption = {
    min: number,
    max: number,
    step: number,
}

export type TSliderRange = TSliderOption & {
    values: [number, number]
}

export type ParamTableCoord = {
    x: number,
    y: number,
    z: number,
    key?: number,
}
