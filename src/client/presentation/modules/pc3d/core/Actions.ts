import {EventDispatcher} from "./EventDispatcher";

export class Action extends EventDispatcher {
    icon: any;
    tooltip: any;

    constructor(args: any = {}) {
        super();

        this.icon = args.icon || '';
        this.tooltip = args.tooltip;

        if (args.onclick !== undefined) {
            this.onclick = args.onclick;
        }
    }

    onclick(event: any) {
        console.log('Action.onclick', event);
    }

    pairWith(object: any) {
        console.log('Action.pairWith', object);
    }

    setIcon(newIcon: any) {
        const oldIcon = this.icon;

        if (newIcon === oldIcon) {
            return;
        }

        this.icon = newIcon;

        this.dispatchEvent({
            type: 'icon_changed',
            action: this,
            icon: newIcon,
            oldIcon: oldIcon
        });
    }
}
