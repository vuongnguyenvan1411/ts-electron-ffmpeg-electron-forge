export class WorkerPool {
    workers: Record<string, Worker[]>;

    constructor() {
        this.workers = {};
    }

    getWorker(url: string) {
        if (!this.workers[url]) {
            this.workers[url] = [];
        }

        if (this.workers[url].length === 0) {
            this.workers[url].push(new Worker(url));
        }

        return this.workers[url].pop();
    }

    returnWorker(url: string, worker: Worker) {
        this.workers[url].push(worker);
    }
}
