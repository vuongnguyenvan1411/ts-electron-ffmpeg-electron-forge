import "../extensions/OrthographicCamera";
import "../extensions/PerspectiveCamera";
import "../extensions/Ray";

import {LRU} from "./LRU";
import {WorkerPool} from "./WorkerPool";
import {EptLoader} from "../loader/EptLoader";
import {PointCloudOctree} from "./PointCloudOctree";
import {POCLoader} from "../loader/POCLoader";
import {OctreeLoader} from "../modules/loader/2.0/OctreeLoader";
import {PointCloudArena4DGeometry} from "../arena4d/PointCloudArena4DGeometry";
import {PointCloudArena4D} from "../arena4d/PointCloudArena4D";
import {TextureMeshModel} from "../models/MapModel";
import {DRACOLoader} from "three/examples/jsm/loaders/DRACOLoader";
import {DDSLoader} from "three/examples/jsm/loaders/DDSLoader";
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import {BufferGeometry, LoadingManager, Mesh, TextureLoader} from "three";
import {XYZLoader} from "three/examples/jsm/loaders/XYZLoader";
import {PointCloudOctreeGeometry} from "./PointCloudOctreeGeometry";
import {ViewerEventName} from "./Event";
import {TPointCloud} from "../../../../models/service/geodetic/Map3DModel";
import {EBackground} from "./Defines";

type TLoadPointCloudResolve = {
    type: string;
    pointcloud: PointCloudOctree;
}

export class App {
    static workerPool = new WorkerPool();
    static lru = new LRU();
    static version = {
        major: 1,
        minor: 0,
        suffix: '.0'
    };

    static pointBudget: number = 1000 * 1000;
    static frameNumber: number = 0;
    static numNodesLoading: number = 0;
    static maxNodesLoading: number = 4;
    static scriptPath: string = "";
    static resourcePath: string = `${App.scriptPath}/v3d/resources`;

    static debug: any = {};

    static measureTimings: any;
    static pointLoadLimit: number;

    static BackgroundGroup = [
        {
            id: EBackground.Map,
            name: 'map',
            color: 0x000000
        },
        {
            id: EBackground.Skybox,
            name: 'map',
            color: 0xff0000
        },
        {
            id: EBackground.Gradient,
            name: 'map',
            color: 0x00ff00
        },
        {
            id: EBackground.Black,
            name: 'map',
            color: 0x000000
        },
        {
            id: EBackground.White,
            name: 'map',
            color: 0xFFFFFF
        },
    ]

    static _pointcloudTransformVersion: Map<any, any>;

    static setPath(value: any) {
        App.scriptPath = value;
        App.resourcePath = `${App.scriptPath}/resources`;
    }

    static getAsset(path?: string) {
        return `${window.location.origin}/v3d/${path ?? ''}`;
    }

    static async loadPointCloud(item: TPointCloud, callback?: Function): Promise<TLoadPointCloudResolve | undefined> {
        const promise = new Promise((resolve) => {
            if (!item.type) {
                throw Error('failed path');
            }

            switch (item.type) {
                case 'cloud.js':
                    POCLoader.load(item, (geometry: PointCloudOctreeGeometry) => {
                        if (!geometry) {
                            console.error(new Error(`failed to load point cloud from URL: ${item.url}`));
                        } else {
                            resolve({
                                type: ViewerEventName.PointCloudLoaded,
                                pointcloud: customPointCloud(new PointCloudOctree(geometry))
                            });
                        }
                    });

                    break;
                case 'ept.json':
                    EptLoader.load(item.url!, (geometry: any) => {
                        if (!geometry) {
                            console.error(new Error(`failed to load point cloud from URL: ${item.url}`));
                        } else {
                            resolve({
                                type: ViewerEventName.PointCloudLoaded,
                                pointcloud: customPointCloud(new PointCloudOctree(geometry))
                            });
                        }
                    });

                    break;
                case 'metadata.json':
                    OctreeLoader.load(item.url!).then(({geometry}) => {
                        if (!geometry) {
                            console.error(new Error(`failed to load point cloud from URL: ${item.url}`));
                        } else {
                            const pointcloud = new PointCloudOctree(geometry);
                            const aPosition = pointcloud.getAttribute("position");
                            const material = pointcloud.material;

                            material.elevationRange = [
                                aPosition.range[0][2],
                                aPosition.range[1][2],
                            ];

                            // loaded(pointcloud);
                            resolve({
                                type: ViewerEventName.PointCloudLoaded,
                                pointcloud: customPointCloud(pointcloud)
                            });
                        }
                    });

                    OctreeLoader.load(item.url!).then(({geometry}) => {
                        if (!geometry) {
                            console.error(new Error(`failed to load point cloud from URL: ${item.url}`));
                        } else {
                            resolve({
                                type: ViewerEventName.PointCloudLoaded,
                                pointcloud: customPointCloud(new PointCloudOctree(geometry))
                            });
                        }
                    })

                    break;
                case '.vpc':
                    PointCloudArena4DGeometry.load(item.url!, (geometry: any) => {
                        if (!geometry) {
                            console.error(new Error(`failed to load point cloud from URL: ${item.url}`));
                        } else {
                            resolve({
                                type: ViewerEventName.PointCloudLoaded,
                                pointcloud: customPointCloud(new PointCloudArena4D(geometry))
                            });
                        }
                    });

                    break;

                default:
                    throw Error(`failed to load point cloud from URL: ${item.url}`);
            }
        });

        const customPointCloud = (pointCloud: any): PointCloudOctree => {
            if (item.name && item.name.length > 0) {
                pointCloud.name = item.name;
            }

            return pointCloud;
        }

        if (callback) {
            promise.then((result) => callback(result));
        } else {
            // @ts-ignore
            return promise.then((r: TLoadPointCloudResolve) => r);
        }
    }

    static async loadTextureMesh(item: TextureMeshModel, options: any = {}): Promise<any> {
        const loaderManager = new LoadingManager();

        if (options.hasOwnProperty('onStart')) {
            loaderManager.onStart = function (url, loaded, itemsTotal) {
                options.onStart(url, loaded, itemsTotal);
            };
        }

        if (options.hasOwnProperty('onLoad')) {
            loaderManager.onLoad = function () {
                options.onLoad();
            };
        }

        if (options.hasOwnProperty('onProgress')) {
            loaderManager.onProgress = function (url, loaded, itemsTotal) {
                options.onProgress(url, loaded, itemsTotal);
            };
        }

        if (options.hasOwnProperty('onError')) {
            loaderManager.onError = function (url) {
                options.onError(url);
            };
        }

        const dracoLoader = new DRACOLoader(loaderManager);
        dracoLoader.setDecoderPath('https://www.gstatic.com/draco/versioned/decoders/1.4.1/');
        dracoLoader.preload();

        const listPromise: any[] = [];

        if (item.object.indexOf('.drc') > 0) {
            listPromise.push(dracoLoader.loadAsync(`${item.object}?v=${item.v}`));
        } else if (item.object.indexOf('.gltf') > 0) {
            const gltfLoader = new GLTFLoader(loaderManager);

            gltfLoader.setDRACOLoader(dracoLoader);
            listPromise.push(gltfLoader.loadAsync(`${item.object}?v=${item.v}`));
        }

        if (item.texture.indexOf('.jpg') > 0) {
            const textureLoader = new TextureLoader(loaderManager);

            listPromise.push(textureLoader.loadAsync(`${item.texture}?v=${item.v}`));
        } else if (item.texture.indexOf('.dds') > 0) {
            const ddsLoader: DDSLoader = new DDSLoader(loaderManager);

            listPromise.push(ddsLoader.loadAsync(`${item.texture}?v=${item.v}`));
        }

        const xyzLoader = new XYZLoader(loaderManager);

        listPromise.push(xyzLoader.loadAsync(`${item.offset}?v=${item.v}`));

        return await Promise.all(listPromise).then((result) => {
            const [geometry, texture, offset] = result;

            // Mesh
            let mesh;

            if (geometry instanceof BufferGeometry) {
                geometry.computeVertexNormals();
                mesh = new Mesh(geometry);
                mesh.castShadow = true;
                mesh.receiveShadow = true;
            } else if (typeof geometry === "object") {
                mesh = geometry;
            }

            dracoLoader.dispose();

            return {
                type: ViewerEventName.TextureMeshLoaded,
                name: item.name,
                mesh: mesh,
                texture: texture,
                position: offset instanceof BufferGeometry ? offset.getAttribute('position').array : null
            }
        })
    }
}
