import {Object3D} from "three";
import {EventDispatcher} from "./EventDispatcher";
import {PointCloudOctreeNode} from "./PointCloudOctree";
import {PointCloudArena4DNode} from "../arena4d/PointCloudArena4D";

export class PointCloudTreeNode extends EventDispatcher {
    needsTransformUpdate: boolean;

    constructor() {
        super();

        this.needsTransformUpdate = true;
    }

    getChildren() {
        throw new Error('override function');
    }

    getBoundingBox() {
        throw new Error('override function');
    }

    isLoaded() {
        throw new Error('override function');
    }

    isGeometryNode() {
        throw new Error('override function');
    }

    isTreeNode() {
        throw new Error('override function');
    }

    getLevel() {
        throw new Error('override function');
    }

    getBoundingSphere() {
        throw new Error('override function');
    }
}

export class PointCloudTree extends Object3D {
    root: PointCloudOctreeNode | PointCloudArena4DNode | null;

    initialized() {
        return this.root !== null;
    }
}
