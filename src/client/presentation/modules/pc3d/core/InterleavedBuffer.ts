export class InterleavedBufferAttribute {
    name: any;
    bytes: any;
    numElements: any;
    normalized: any;
    type: any;

    constructor(name: any, bytes: any, numElements: any, type: any, normalized: any) {
        this.name = name;
        this.bytes = bytes;
        this.numElements = numElements;
        this.normalized = normalized;
        this.type = type; // gl type without prefix, e.g. "FLOAT", "UNSIGNED_INT"
    }
}

export class InterleavedBuffer {
    data: any;
    attributes: any;
    stride: number;
    numElements: any;

    constructor(data: any, attributes: any, numElements: any) {
        this.data = data;
        this.attributes = attributes;
        this.stride = attributes.reduce((a: any, att: any) => a + att.bytes, 0);
        this.stride = Math.ceil(this.stride / 4) * 4;
        this.numElements = numElements;
    }

    offset(name: any) {
        let offset = 0;

        for (let att of this.attributes) {
            if (att.name === name) {
                return offset;
            }

            offset += att.bytes;
        }

        return null;
    }
}
