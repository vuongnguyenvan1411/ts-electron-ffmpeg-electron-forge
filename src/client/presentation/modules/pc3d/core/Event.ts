import {PointCloudOctree} from "./PointCloudOctree";
import {VScene} from "../viewer/VScene";
import {BoxVolume, SphereVolume} from "../utils/Volume";

type TR_ViewerEvent = {
    target: VScene;
    type: string;
}

export type TR_PointCloudAdded = TR_ViewerEvent & {
    pointcloud: PointCloudOctree;
}

export type TR_VolumeRemoved = TR_ViewerEvent & {
    volume: BoxVolume | SphereVolume;
}

export type TR_SceneChanged = {
    type: string;
    oldScene: VScene;
    scene: VScene;
}

export class InputHandlerEvent {
    public static readonly SelectionChanged = "selection_changed";
    public static readonly Delete = "delete";
    public static readonly Keydown = "keydown";
}

export class VolumeEvent {
    public static readonly Select = "select";
    public static readonly Deselect = "deselect";
}

export class VSceneEvent {
    public static readonly ClipVolumeAdded = "clip_volume_added";
    public static readonly ClipVolumeRemoved = "clip_volume_removed";
    public static readonly PolygonClipVolumeAdded = "polygon_clip_volume_added";
    public static readonly PolygonClipVolumeRemoved = "polygon_clip_volume_removed";
}

export class MeasureEvent {
    public static readonly MarkerAdded = "marker_added";
    public static readonly MarkerRemoved = "marker_removed";
    public static readonly MarkerDropped = "marker_dropped";
    public static readonly MarkerMoved = "marker_moved";
    public static readonly WidthChanged = "width_changed";
    public static readonly PositionChanged = "position_changed";
    public static readonly OrientationChanged = "orientation_changed";
    public static readonly ScaleChanged = "scale_changed";
    public static readonly ClipChanged = "clip_changed";
}

export class CameraAnimationEvent {
    public static readonly ControlPointAdded = "controlpoint_added";
    public static readonly ControlPointRemoved = "controlpoint_removed";
    public static readonly Start = "start";
    public static readonly Stop = "stop";
    public static readonly Playing = "playing";
}

export class AnnotationEvent {
    public static readonly AnnotationAdded = "annotation_added";
    public static readonly AnnotationRemoved = "annotation_removed";
    public static readonly AnnotationChanged = "annotation_changed";
    public static readonly VisibilityChanged = "visibility_changed";
}

export class ViewerEventName {
    public static readonly Update = "update";
    public static readonly UpdateStart = "update_start";
    public static readonly PointCloudAdded = "pointcloud_added";
    public static readonly PointCloudLoaded = "pointcloud_loaded";
    public static readonly TextureMeshAdded = "texturemesh_added";
    public static readonly TextureMeshLoaded = "texturemesh_loaded";
    public static readonly LayerMeshAdded = "layermesh_added";
    public static readonly VolumeAdded = "volume_added";
    public static readonly VolumeRemoved = "volume_removed";
    public static readonly MinNodeSizeChanged = "minnodesize_changed";
    public static readonly BackgroundChanged = "background_changed";
    public static readonly ShowBoundingBoxChanged = "show_boundingbox_changed";
    public static readonly MoveSpeedChanged = "move_speed_changed";
    public static readonly AttributeWeightsChanged = "attribute_weights_changed";
    public static readonly FreezeChanged = "freeze_changed";
    public static readonly ClipTaskChanged = "cliptask_changed";
    public static readonly ClipMethodChanged = "clipmethod_changed";
    public static readonly ElevationGradientRepeatChanged = "elevation_gradient_repeat_changed";
    public static readonly PointBudgetChanged = "point_budget_changed";
    public static readonly ShowAnnotationsChanged = "show_annotations_changed";
    public static readonly UseDemCollisionsChanged = "use_demcollisions_changed";
    public static readonly UseEdlChanged = "use_edl_changed";
    public static readonly EdlRadiusChanged = "edl_radius_changed";
    public static readonly EdlStrengthChanged = "edl_strength_changed";
    public static readonly EdlOpacityChanged = "edl_opacity_changed";
    public static readonly FovChanged = "fov_changed";
    public static readonly ClassificationsChanged = "classifications_changed";
    public static readonly ClassificationVisibilityChanged = "classification_visibility_changed";
    public static readonly FilterReturnNumberRangeChanged = "filter_return_number_range_changed";
    public static readonly FilterNumberOfReturnsRangeChanged = "filter_number_of_returns_range_changed";
    public static readonly FilterGpsTimeRangeChanged = "filter_gps_time_range_changed";
    public static readonly FilterPointSourceIdRangeChanged = "filter_point_source_id_range_changed";
    public static readonly FilterPointSourceIdExtentChanged = "filter_point_source_id_extent_changed";
    public static readonly LengthUnitChanged = "length_unit_changed";
    public static readonly FocusingFinished = "focusing_finished";
    public static readonly FocusingStarted = "focusing_started";
    public static readonly CameraChanged = "camera_changed";
    public static readonly SceneChanged = "scene_changed";
    public static readonly OrientedImagesAdded = "oriented_images_added";
    public static readonly OrientedImagesRemoved = "oriented_images_removed";
    public static readonly Vr360ImagesAdded = "360_images_added";
    public static readonly Vr360ImagesRemoved = "360_images_removed";
    public static readonly GeoPackageAdded = "geopackage_added";
    public static readonly GeoPackageRemoved = "geopackage_removed";
    public static readonly CameraAnimationAdded = "camera_animation_added";
    public static readonly CameraAnimationRemoved = "camera_animation_removed";
    public static readonly PolygonClipVolumeAdded = "polygon_clip_volume_added";
    public static readonly PolygonClipVolumeRemoved = "polygon_clip_volume_removed";
    public static readonly MeasurementAdded = "measurement_added";
    public static readonly MeasurementRemoved = "measurement_removed";
    public static readonly StartInsertingMeasurement = "start_inserting_measurement";
    public static readonly StartInsertingVolume = "start_inserting_volume";
    public static readonly StartInsertingProfile = "start_inserting_profile";
    public static readonly StartInsertingAnnotation = "start_inserting_annotation";
    public static readonly StartInsertingClippingVolume = "start_inserting_clipping_volume";
    public static readonly CancelInsertions = "cancel_insertions";
    public static readonly ProfileAdded = "profile_added";
    public static readonly ProfileRemoved = "profile_removed";
    public static readonly VrStart = "vr_start";
    public static readonly VrEnd = "vr_end";
    public static readonly Render_Pass_End = "render.pass.end";
    public static readonly Render_Pass_PerspectiveOverlay = "render.pass.perspective_overlay";
    public static readonly Render_Pass_Scene = "render.pass.scene";
    public static readonly Render_Pass_Begin = "render.pass.begin";
    public static readonly MaterialPropertyChanged = "material_property_changed";
}

export class MouseEventName {
    // public static readonly Click = "pointertap";
    public static readonly Click = "click";
    public static readonly Down = "pointerdown";
    // public static readonly Down = "mousedown";
    public static readonly Move = "pointermove";
    // public static readonly Move = "mousemove";
    // public static readonly Out = "pointerout";
    public static readonly Out = "mouseout";
    // public static readonly Over = "pointerover";
    public static readonly Over = "mouseover";
    public static readonly Up = "pointerup";
    // public static readonly Up = "mouseup";
    public static readonly UpOutside = "pointerupoutside";
    public static readonly RightClick = "rightclick";
    public static readonly RightDown = "rightdown";
    public static readonly RightUp = "rightup";
    public static readonly RightOutside = "rightupoutside";
    public static readonly Enter = "mouseenter";
    public static readonly Leave = "mouseleave";
    public static readonly Hover = "hover";
}

export class CanvasEventName {
    public static readonly WebglContextLost = 'webglcontextlost';
}

export class DocumentEventName {
    public static readonly VisibilityChange = 'visibilitychange';
}

export class WindowEventName {
    public static readonly DeviceOrientationAbsolute = 'deviceorientationabsolute';
    public static readonly DeviceOrientation = 'deviceorientation';
    public static readonly OrientationChange = 'orientationchange';
}
