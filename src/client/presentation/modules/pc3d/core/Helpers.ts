export class Helpers {
    static isSupportWebGL2() {
        return typeof WebGL2RenderingContext !== 'undefined';
    }

    static validateLatitude(lat: any) {
        return isFinite(lat) && Math.abs(lat) <= 90;
    }

    static validateLongitude(long: any) {
        return isFinite(long) && Math.abs(long) <= 180;
    }

    static validateURL(url: any) {
        try {
            new URL(url);
        } catch (_) {
            return false;
        }

        return true;
    }

    static isNumeric(val: any): boolean {
        return !(val instanceof Array) && (val - parseFloat(val) + 1) >= 0;
    }

    static getIDMaxKeyArray(arr: any[]) {
        if (arr.length === 0) {
            return 0;
        } else {
            const listKeyMarker = Object.keys(arr).map((item) => parseInt(item));

            const max = listKeyMarker.reduce(function (a, b) {
                return Math.max(a, b);
            });

            return max + 1;
        }
    }
}
