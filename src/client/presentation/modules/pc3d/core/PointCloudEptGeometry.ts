import {PointCloudTreeNode} from "./PointCloudTree";
import {PointAttribute, PointAttributes, PointAttributeTypes} from "../loader/PointAttributes";
import {Box3, Sphere, Vector3} from "three";
import {Version} from "./Version";
import proj4 from "proj4";
import {EptLaszipLoader} from "../loader/ept/LaszipLoader";
import {EptBinaryLoader} from "../loader/ept/BinaryLoader";
import {EptZstandardLoader} from "../loader/ept/ZstandardLoader";
import {App} from "./App";

class U {
    static toVector3(v: number[] | ArrayLike<number>, offset?: number) {
        return new Vector3().fromArray(v, offset ?? 0);
    }

    static toBox3(b: number[] | ArrayLike<number>) {
        return new Box3(U.toVector3(b), U.toVector3(b, 3));
    };

    static findDim(schema: any, name: string) {
        const dim = schema.find((dim: any) => dim.name === name);

        if (!dim) throw new Error('Failed to find ' + name + ' in schema');

        return dim;
    }

    static sphereFrom(b: Box3) {
        return b.getBoundingSphere(new Sphere());
    }
}

export class PointCloudEptGeometry {
    eptScale: Vector3;
    eptOffset: Vector3;
    url: string;
    info: any;
    type: string;
    schema: any;
    span: any;
    boundingBox: Box3;
    tightBoundingBox: Box3;
    offset: Vector3;
    boundingSphere: any;
    tightBoundingSphere: any;
    version: Version;
    projection: any;
    fallbackProjection: any;
    pointAttributes: PointAttributes;
    spacing: number;
    loader: EptLaszipLoader | EptBinaryLoader | EptZstandardLoader | Error;
    root?: PointCloudEptGeometryNode;

    constructor(url: string, info: any) {
        // let version = info.version;
        const schema = info.schema;
        const bounds = info.bounds;
        const boundsConforming = info.boundsConforming;
        const xyz = [
            U.findDim(schema, 'X'),
            U.findDim(schema, 'Y'),
            U.findDim(schema, 'Z')
        ];
        const scale = xyz.map((d) => d.scale || 1);
        const offset = xyz.map((d) => d.offset || 0);

        this.eptScale = U.toVector3(scale);
        this.eptOffset = U.toVector3(offset);

        this.url = url;
        this.info = info;
        this.type = 'ept';

        this.schema = schema;
        this.span = info.span || info.ticks;
        this.boundingBox = U.toBox3(bounds);
        this.tightBoundingBox = U.toBox3(boundsConforming);
        this.offset = U.toVector3([0, 0, 0]);
        this.boundingSphere = U.sphereFrom(this.boundingBox);
        this.tightBoundingSphere = U.sphereFrom(this.tightBoundingBox);
        this.version = new Version('1.7');

        this.projection = null;
        this.fallbackProjection = null;

        if (info.srs && info.srs.horizontal) {
            this.projection = info.srs.authority + ':' + info.srs.horizontal;
        }

        if (info.srs.wkt) {
            if (!this.projection) this.projection = info.srs.wkt;
            else this.fallbackProjection = info.srs.wkt;
        }

        // TODO [mschuetz]: named projections that proj4 can't handle seem to cause problems.
        // remove them for now
        try {
            proj4(this.projection);
        } catch (e) {
            this.projection = null;
        }

        {
            const attributes = new PointAttributes();

            attributes.add(PointAttribute.POSITION_CARTESIAN);
            attributes.add(new PointAttribute("rgba", PointAttributeTypes.DATA_TYPE_UINT8, 4));
            attributes.add(new PointAttribute("intensity", PointAttributeTypes.DATA_TYPE_UINT16, 1));
            attributes.add(new PointAttribute("classification", PointAttributeTypes.DATA_TYPE_UINT8, 1));
            attributes.add(new PointAttribute("gps-time", PointAttributeTypes.DATA_TYPE_DOUBLE, 1));
            attributes.add(new PointAttribute("returnNumber", PointAttributeTypes.DATA_TYPE_UINT8, 1));
            attributes.add(new PointAttribute("number of returns", PointAttributeTypes.DATA_TYPE_UINT8, 1));
            attributes.add(new PointAttribute("return number", PointAttributeTypes.DATA_TYPE_UINT8, 1));
            attributes.add(new PointAttribute("source id", PointAttributeTypes.DATA_TYPE_UINT16, 1));

            this.pointAttributes = attributes;
        }


        this.spacing = (this.boundingBox.max.x - this.boundingBox.min.x) / this.span;

        // let hierarchyType = info.hierarchyType || 'json';

        const dataType = info.dataType;

        if (dataType === 'laszip') {
            this.loader = new EptLaszipLoader();
        } else if (dataType === 'binary') {
            this.loader = new EptBinaryLoader();
        } else if (dataType === 'zstandard') {
            this.loader = new EptZstandardLoader();
        } else {
            throw new Error('Could not read data type: ' + dataType);
        }
    }
}

export class EptKey {
    ept: any;
    b: any;
    d: any;
    x: number;
    y: number;
    z: number;

    constructor(ept: any, b: any, d: any, x?: number, y?: number, z?: number) {
        this.ept = ept;
        this.b = b;
        this.d = d;
        this.x = x || 0;
        this.y = y || 0;
        this.z = z || 0;
    }

    name() {
        return this.d + '-' + this.x + '-' + this.y + '-' + this.z;
    }

    step(a: number, b: number, c: number) {
        const min = this.b.min.clone();
        const max = this.b.max.clone();
        const dst = new Vector3().subVectors(max, min);

        if (a) min.x += dst.x / 2;
        else max.x -= dst.x / 2;

        if (b) min.y += dst.y / 2;
        else max.y -= dst.y / 2;

        if (c) min.z += dst.z / 2;
        else max.z -= dst.z / 2;

        return new EptKey(
            this.ept,
            new Box3(min, max),
            this.d + 1,
            this.x * 2 + a,
            this.y * 2 + b,
            this.z * 2 + c
        );
    }

    children() {
        let result: any[] = [];

        for (let a = 0; a < 2; ++a) {
            for (let b = 0; b < 2; ++b) {
                for (let c = 0; c < 2; ++c) {
                    const add = this.step(a, b, c).name();

                    if (!result.includes(add)) result = result.concat(add);
                }
            }
        }

        return result;
    }
}

export class PointCloudEptGeometryNode extends PointCloudTreeNode {
    static IDCount: number = 0;

    ept: any;
    key: EptKey;
    id: number;
    geometry: any;
    boundingBox: any;
    tightBoundingBox: any;
    spacing: any;
    boundingSphere: any;
    hasChildren: boolean;
    children: any;
    numPoints: number;
    level: any;
    loaded: boolean;
    loading: boolean;
    oneTimeDisposeHandlers: any[];
    name: string;
    index: number;
    mean: any;
    parent?: boolean;

    constructor(ept: any, b?: any, d?: any, x?: number, y?: number, z?: number) {
        super();

        this.ept = ept;
        this.key = new EptKey(
            this.ept,
            b || this.ept.boundingBox,
            d || 0,
            x,
            y,
            z
        );

        this.id = PointCloudEptGeometryNode.IDCount++;
        this.geometry = null;
        this.boundingBox = this.key.b;
        this.tightBoundingBox = this.boundingBox;
        this.spacing = this.ept.spacing / Math.pow(2, this.key.d);
        this.boundingSphere = U.sphereFrom(this.boundingBox);

        // These are set during hierarchy loading.
        this.hasChildren = false;
        this.children = {};
        this.numPoints = -1;

        this.level = this.key.d;
        this.loaded = false;
        this.loading = false;
        this.oneTimeDisposeHandlers = [];

        const k = this.key;
        this.name = this.toName(k.d, k.x, k.y, k.z);
        this.index = parseInt(this.name.charAt(this.name.length - 1));
    }

    isGeometryNode() {
        return true;
    }

    getLevel() {
        return this.level;
    }

    isTreeNode() {
        return false;
    }

    isLoaded() {
        return this.loaded;
    }

    getBoundingSphere() {
        return this.boundingSphere;
    }

    getBoundingBox() {
        return this.boundingBox;
    }

    url() {
        return this.ept.url + 'ept-data/' + this.filename();
    }

    getNumPoints() {
        return this.numPoints;
    }

    filename() {
        return this.key.name();
    }

    getChildren() {
        const children = [];

        for (let i = 0; i < 8; i++) {
            if (this.children[i]) {
                children.push(this.children[i]);
            }
        }

        return children;
    }

    addChild(child: any) {
        this.children[child.index] = child;
        child.parent = this;
    }

    load() {
        if (this.loaded || this.loading) return;
        if (App.numNodesLoading >= App.maxNodesLoading) return;

        this.loading = true;

        ++App.numNodesLoading;

        if (this.numPoints === -1) this.loadHierarchy().then();
        this.loadPoints();
    }

    loadPoints() {
        this.ept.loader.load(this);
    }

    async loadHierarchy() {
        const nodes: any = {};
        nodes[this.filename()] = this;
        this.hasChildren = false;

        const eptHierarchyFile = `${this.ept.url}ept-hierarchy/${this.filename()}.json`;
        const response = await fetch(eptHierarchyFile);
        const hier = await response.json();

        // Since we want to traverse top-down, and 10 comes
        // lexicographically before 9 (for example), do a deep sort.
        const keys = Object.keys(hier).sort((a, b) => {
            const [da, xa, ya, za] = a.split('-').map((n) => parseInt(n, 10));
            const [db, xb, yb, zb] = b.split('-').map((n) => parseInt(n, 10));

            if (da < db) return -1;
            if (da > db) return 1;
            if (xa < xb) return -1;
            if (xa > xb) return 1;
            if (ya < yb) return -1;
            if (ya > yb) return 1;
            if (za < zb) return -1;
            if (za > zb) return 1;

            return 0;
        });

        keys.forEach((v) => {
            const [d, x, y, z] = v.split('-').map((n) => parseInt(n, 10));
            const a = x & 1, b = y & 1, c = z & 1;
            const parentName = (d - 1) + '-' + (x >> 1) + '-' + (y >> 1) + '-' + (z >> 1);

            const parentNode = nodes[parentName];

            if (!parentNode) return;

            parentNode.hasChildren = true;

            const key = parentNode.key.step(a, b, c);

            const node = new PointCloudEptGeometryNode(
                this.ept,
                key.b,
                key.d,
                key.x,
                key.y,
                key.z
            );

            node.level = d;
            node.numPoints = hier[v];

            parentNode.addChild(node);
            nodes[key.name()] = node;
        });
    }

    doneLoading(bufferGeometry: any, tightBoundingBox: any, np: any, mean: any) {
        bufferGeometry.boundingBox = this.boundingBox;
        this.geometry = bufferGeometry;
        this.tightBoundingBox = tightBoundingBox;
        this.numPoints = np;
        this.mean = mean;
        this.loaded = true;
        this.loading = false;

        --App.numNodesLoading;
    }

    toName(d: any, x: number, y: number, z: number) {
        let name = 'r';

        for (let i = 0; i < d; ++i) {
            const shift = d - i - 1;
            const mask = 1 << shift;
            let step = 0;

            if (x & mask) step += 4;
            if (y & mask) step += 2;
            if (z & mask) step += 1;

            name += step;
        }

        return name;
    }

    dispose() {
        if (this.geometry && this.parent != null) {
            this.geometry.dispose();
            this.geometry = null;
            this.loaded = false;

            // this.dispatchEvent( { type: 'dispose' } );
            for (let i = 0; i < this.oneTimeDisposeHandlers.length; i++) {
                const handler = this.oneTimeDisposeHandlers[i];

                handler();
            }

            this.oneTimeDisposeHandlers = [];
        }
    }
}

