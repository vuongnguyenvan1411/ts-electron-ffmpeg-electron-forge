import {
    BackSide,
    Box3,
    BoxGeometry,
    BufferAttribute,
    BufferGeometry,
    Camera,
    Color,
    DataTexture,
    Line,
    LineBasicMaterial,
    LineSegments,
    Matrix4,
    Mesh,
    MeshBasicMaterial,
    MeshNormalMaterial,
    NearestFilter,
    Object3D,
    PerspectiveCamera,
    PlaneBufferGeometry,
    PlaneHelper,
    Ray,
    Raycaster,
    RGBAFormat,
    Scene,
    SphereGeometry,
    TextureLoader,
    Vector2,
    Vector3,
    Vector4,
    WebGLRenderer,
    WebGLRenderTarget
} from "three";
import {XHRFactory} from "./XHRFactory";
import {Easing, Tween} from "@tweenjs/tween.js";
import proj4 from "proj4";
import * as shapefile from "shapefile";
import {PointCloudOctree} from "./PointCloudOctree";
import {Viewer} from "../viewer/viewer";
import {Material} from "three/src/materials/Material";

export class Utils {
    static async loadShapefileFeatures(file: any, callback: any) {
        const features: any[] = [];

        const handleFinish = () => {
            callback(features);
        }

        const source = await shapefile.open(file);

        while (true) {
            const result = await source.read();

            if (result.done) {
                handleFinish();
                break;
            }

            if (result.value && result.value.type === 'Feature' && result.value.geometry !== undefined) {
                features.push(result.value);
            }
        }
    }

    static toString(value: any) {
        if (value.x != null) {
            return value.x.toFixed(2) + ', ' + value.y.toFixed(2) + ', ' + value.z.toFixed(2);
        } else {
            return '' + value + '';
        }
    }

    static normalizeURL(url: any) {
        const u = new URL(url);

        return u.protocol + '//' + u.hostname + u.pathname.replace(/\/+/g, '/');
    }

    static pathExists(url: any) {
        const req = XHRFactory.createXMLHttpRequest();
        req.open('GET', url, false);
        req.send();

        return req.status === 200;
    }

    static debugSphere(parent: any, position: any, scale: any, color: any) {
        const geometry = new SphereGeometry(1, 8, 8);
        let material;

        if (color !== undefined) {
            material = new MeshBasicMaterial({color: color});
        } else {
            material = new MeshNormalMaterial();
        }
        const sphere = new Mesh(geometry, material);
        sphere.position.copy(position);
        sphere.scale.set(scale, scale, scale);
        parent.add(sphere);

        return sphere;
    }

    static debugLine(parent: any, start: any, end: any, color: any) {
        const material = new LineBasicMaterial({color: color});
        const geometry = new BufferGeometry();

        const p1 = new Vector3(0, 0, 0);
        const p2 = end.clone().sub(start);

        const vertices = new Float32Array([
            ...(p1.toArray()),
            ...(p2.toArray()),
        ]);

        geometry.setAttribute('position', new BufferAttribute(vertices, 3));

        const tl = new Line(geometry, material);
        tl.position.copy(start);

        parent.add(tl);

        return {
            node: tl,
            set: (start: any, end: any) => {
                // geometry.vertices[0].copy(start);
                // geometry.vertices[1].copy(end);
                p1.copy(start);
                p2.copy(end);
                geometry.getAttribute('position').needsUpdate = true;
            }
        }
    }

    static debugCircle(parent: any, center: any, radius: any, color: any) {
        const material = new LineBasicMaterial({color: color});
        const geometry = new BufferGeometry();
        const n = 32;
        const arr: any[] = [];

        for (let i = 0; i <= n; i++) {
            const u0 = 2 * Math.PI * (i / n);
            const u1 = 2 * Math.PI * (i + 1) / n;
            const p0 = new Vector3(
                Math.cos(u0),
                Math.sin(u0),
                0
            );
            const p1 = new Vector3(
                Math.cos(u1),
                Math.sin(u1),
                0
            );

            arr.concat(p0.toArray(), p1.toArray());
        }

        const vertices = new Float32Array(arr);

        geometry.setAttribute('position', new BufferAttribute(vertices, 3));

        const tl = new Line(geometry, material);
        tl.position.copy(center);
        tl.scale.set(radius, radius, radius);

        parent.add(tl);
    }

    static debugBox(parent: any, box: any, transform = new Matrix4(), color = 0xFFFF00) {
        const vertices = [
            [box.min.x, box.min.y, box.min.z],
            [box.min.x, box.min.y, box.max.z],
            [box.min.x, box.max.y, box.min.z],
            [box.min.x, box.max.y, box.max.z],

            [box.max.x, box.min.y, box.min.z],
            [box.max.x, box.min.y, box.max.z],
            [box.max.x, box.max.y, box.min.z],
            [box.max.x, box.max.y, box.max.z],
        ].map(v => new Vector3(...v));

        const edges = [
            [0, 4], [4, 5], [5, 1], [1, 0],
            [2, 6], [6, 7], [7, 3], [3, 2],
            [0, 2], [4, 6], [5, 7], [1, 3]
        ];

        const center = box.getCenter(new Vector3());

        const centroids = [
            {position: [box.min.x, center.y, center.z], color: 0xFF0000},
            {position: [box.max.x, center.y, center.z], color: 0x880000},

            {position: [center.x, box.min.y, center.z], color: 0x00FF00},
            {position: [center.x, box.max.y, center.z], color: 0x008800},

            {position: [center.x, center.y, box.min.z], color: 0x0000FF},
            {position: [center.x, center.y, box.max.z], color: 0x000088},
        ];

        for (let vertex of vertices) {
            const pos = vertex.clone().applyMatrix4(transform);

            Utils.debugSphere(parent, pos, 0.1, 0xFF0000);
        }

        for (let edge of edges) {
            const start = vertices[edge[0]].clone().applyMatrix4(transform);
            const end = vertices[edge[1]].clone().applyMatrix4(transform);

            Utils.debugLine(parent, start, end, color);
        }

        for (let centroid of centroids) {
            const pos = new Vector3(...centroid.position).applyMatrix4(transform);

            Utils.debugSphere(parent, pos, 0.1, centroid.color);
        }
    }

    static debugPlane(parent: any, plane: any, size = 1, color = 0x0000FF) {
        const planeHelper = new PlaneHelper(plane, size, color);

        parent.add(planeHelper);
    }

    /** @see adapted from mhluska at https://github.com/mrdoob/three.js/issues/1561 */
    static computeTransformedBoundingBox(box: Box3, transform: Matrix4) {
        const vertices = [
            new Vector3(box.min.x, box.min.y, box.min.z).applyMatrix4(transform),
            new Vector3(box.min.x, box.min.y, box.min.z).applyMatrix4(transform),
            new Vector3(box.max.x, box.min.y, box.min.z).applyMatrix4(transform),
            new Vector3(box.min.x, box.max.y, box.min.z).applyMatrix4(transform),
            new Vector3(box.min.x, box.min.y, box.max.z).applyMatrix4(transform),
            new Vector3(box.min.x, box.max.y, box.max.z).applyMatrix4(transform),
            new Vector3(box.max.x, box.max.y, box.min.z).applyMatrix4(transform),
            new Vector3(box.max.x, box.min.y, box.max.z).applyMatrix4(transform),
            new Vector3(box.max.x, box.max.y, box.max.z).applyMatrix4(transform)
        ];

        const boundingBox = new Box3();
        boundingBox.setFromPoints(vertices);

        return boundingBox;
    }

    static addCommas(nStr: string | number, sep: string = '.') {
        if (typeof nStr === 'number') {
            nStr = nStr.toString();
        }

        nStr = nStr.replace('.', ',');

        nStr += '';
        const x = nStr.split('.');
        let x1 = x[0];
        const x2 = x.length > 1 ? '.' + x[1] : '';
        const rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, `$1${sep}$2`);
        }

        return x1 + x2;
    };

    static removeCommas(str: any) {
        return str.replace(/,/g, '');
    }

    /**
     * create worker from a string
     *
     * code from http://stackoverflow.com/questions/10343913/how-to-create-a-web-worker-from-a-string
     */
    static createWorker(code: any) {
        const blob = new Blob([code], {type: 'application/javascript'});

        return new Worker(URL.createObjectURL(blob));
    }

    static moveTo(scene: any, endPosition: any, endTarget: any) {
        const view = scene.view;
        const camera = scene.getActiveCamera();
        const animationDuration = 500;
        const easing = Easing.Quartic.Out;

        { // animate camera position
            const tween = new Tween(view.position).to(endPosition, animationDuration);
            tween.easing(easing);
            tween.start();
        }

        { // animate camera target
            const camTargetDistance = camera.position.distanceTo(endTarget);
            const target = new Vector3().addVectors(
                camera.position,
                camera.getWorldDirection(new Vector3()).clone().multiplyScalar(camTargetDistance)
            );
            const tween = new Tween(target).to(endTarget, animationDuration);
            tween.easing(easing);
            tween.onUpdate(() => {
                view.lookAt(target);
            });
            tween.onComplete(() => {
                view.lookAt(target);
            });
            tween.start();
        }
    }

    static loadSkybox(path: string) {
        const parent = new Object3D();
        parent.name = 'skybox_root'

        // @ts-ignore
        const camera = new PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 100000);
        camera.up.set(0, 0, 1);
        const scene = new Scene();

        const format = '.jpg';
        const urls = [
            path + 'px' + format, path + 'nx' + format,
            path + 'py' + format, path + 'ny' + format,
            path + 'pz' + format, path + 'nz' + format
        ];

        const materialArray: any[] = [];

        for (let i = 0; i < 6; i++) {
            const material = new MeshBasicMaterial({
                map: null,
                side: BackSide,
                depthTest: false,
                depthWrite: false,
                color: 0x424556
            });

            materialArray.push(material);

            const loader = new TextureLoader();

            loader.load(urls[i],
                function loaded(texture) {
                    material.map = texture;
                    material.needsUpdate = true;
                    material.color.setHex(0xffffff);
                }, function progress(xhr) {
                    console.log((xhr.loaded / xhr.total * 100) + '% loaded');
                }, function error(xhr) {
                    console.log('An error happened', xhr);
                }
            );
        }

        const skyGeometry = new BoxGeometry(700, 700, 700);
        const skybox = new Mesh(skyGeometry, materialArray);

        scene.add(skybox);

        scene.traverse(n => n.frustumCulled = false);

        // z up
        scene.rotation.x = Math.PI / 2;

        parent.children.push(camera);
        camera.parent = parent;

        return {camera, scene, parent}
    }

    static createGrid(width: any, length: any, spacing: any, color?: any) {
        let material = new LineBasicMaterial({
            color: color || 0x888888
        });

        let geometry = new BufferGeometry();

        const arr: any[] = [];

        for (let i = 0; i <= length; i++) {
            arr.concat(
                (new Vector3(-(spacing * width) / 2, i * spacing - (spacing * length) / 2, 0)).toArray(),
                (new Vector3(+(spacing * width) / 2, i * spacing - (spacing * length) / 2, 0)).toArray(),
            );
        }

        for (let i = 0; i <= width; i++) {
            arr.concat(
                (new Vector3(i * spacing - (spacing * width) / 2, -(spacing * length) / 2, 0)).toArray(),
                (new Vector3(i * spacing - (spacing * width) / 2, +(spacing * length) / 2, 0)).toArray(),
            )
        }

        const vertices = new Float32Array(arr);

        geometry.setAttribute('position', new BufferAttribute(vertices, 3));

        const line = new LineSegments(geometry, material);
        line.receiveShadow = true;

        return line;
    }

    static createBackgroundTexture(width: number, height: number) {
        function gauss(x: number, y: number) {
            return (1 / (2 * Math.PI)) * Math.exp(-(x * x + y * y) / 2);
        }

        // map.magFilter = NearestFilter;
        const size = width * height;
        const data = new Uint8Array(3 * size);

        const chroma = [1, 1.5, 1.7];
        const max = gauss(0, 0);

        for (let x = 0; x < width; x++) {
            for (let y = 0; y < height; y++) {
                const u = 2 * (x / width) - 1;
                const v = 2 * (y / height) - 1;

                const i = x + width * y;
                const d = gauss(2 * u, 2 * v) / max;
                let r = (Math.random() + Math.random() + Math.random()) / 3;
                r = (d * 0.5 + 0.5) * r * 0.03;
                r = r * 0.4;

                // d = Math.pow(d, 0.6);

                data[3 * i] = 255 * (d / 15 + 0.05 + r) * chroma[0];
                data[3 * i + 1] = 255 * (d / 15 + 0.05 + r) * chroma[1];
                data[3 * i + 2] = 255 * (d / 15 + 0.05 + r) * chroma[2];
            }
        }

        const texture = new DataTexture(data, width, height, RGBAFormat);
        texture.needsUpdate = true;

        return texture;
    }

    static getMousePointCloudIntersection(mouse: Vector2, camera: any, viewer: Viewer, pointClouds: PointCloudOctree[], params: { pickClipped?: boolean } = {}) {
        const renderer = viewer.renderer;

        const nMouse = {
            x: (mouse.x / renderer.domElement.clientWidth) * 2 - 1,
            y: -(mouse.y / renderer.domElement.clientHeight) * 2 + 1
        };

        const pickParams: any = {};

        if (params.pickClipped) {
            pickParams.pickClipped = params.pickClipped;
        }

        pickParams.x = mouse.x;
        pickParams.y = renderer.domElement.clientHeight - mouse.y;

        const raycaster = new Raycaster();
        raycaster.setFromCamera(nMouse, camera);
        const ray = raycaster.ray;

        let selectedPointcloud: any = null;
        let closestDistance = Infinity;
        let closestIntersection = null;
        let closestPoint = null;

        for (let pointcloud of pointClouds) {
            const point = pointcloud.pick(viewer, camera, ray, pickParams);

            if (!point) {
                continue;
            }

            const distance = camera.position.distanceTo(point.position);

            if (distance < closestDistance) {
                closestDistance = distance;
                selectedPointcloud = pointcloud;
                closestIntersection = point.position;
                closestPoint = point;
            }
        }

        if (selectedPointcloud) {
            return {
                location: closestIntersection,
                distance: closestDistance,
                pointcloud: selectedPointcloud,
                point: closestPoint
            };
        } else {
            return null;
        }
    }

    static pixelsArrayToImage(pixels: any, width: number, height: number) {
        const canvas = document.createElement('canvas');
        canvas.width = width;
        canvas.height = height;

        const context: any = canvas.getContext('2d');

        pixels = new pixels.constructor(pixels);

        for (let i = 0; i < pixels.length; i++) {
            pixels[i * 4 + 3] = 255;
        }

        const imageData = context.createImageData(width, height);
        imageData.data.set(pixels);
        context.putImageData(imageData, 0, 0);

        const img = new Image();
        img.src = canvas.toDataURL();
        // img.style.transform = "scaleY(-1)";

        return img;
    }

    static pixelsArrayToDataUrl(pixels: any, width: number, height: number) {
        const canvas = document.createElement('canvas');
        canvas.width = width;
        canvas.height = height;

        const context: any = canvas.getContext('2d');

        pixels = new pixels.constructor(pixels);

        for (let i = 0; i < pixels.length; i++) {
            pixels[i * 4 + 3] = 255;
        }

        const imageData = context.createImageData(width, height);
        imageData.data.set(pixels);
        context.putImageData(imageData, 0, 0);

        return canvas.toDataURL();
    }

    static pixelsArrayToCanvas(pixels: any, width: number, height: number) {
        const canvas = document.createElement('canvas');
        canvas.width = width;
        canvas.height = height;

        const context: any = canvas.getContext('2d');

        pixels = new pixels.constructor(pixels);

        //for (let i = 0; i < pixels.length; i++) {
        //	pixels[i * 4 + 3] = 255;
        //}

        // flip vertically
        const bytesPerLine = width * 4;
        for (let i = 0; i < (parseInt(height.toString()) / 2); i++) {
            const j = height - i - 1;

            const lineI = pixels.slice(i * bytesPerLine, i * bytesPerLine + bytesPerLine);
            const lineJ = pixels.slice(j * bytesPerLine, j * bytesPerLine + bytesPerLine);

            pixels.set(lineJ, i * bytesPerLine);
            pixels.set(lineI, j * bytesPerLine);
        }

        const imageData = context.createImageData(width, height);
        imageData.data.set(pixels);
        context.putImageData(imageData, 0, 0);

        return canvas;
    }

    static removeListeners(dispatcher: any, type: string) {
        if (dispatcher._listeners === undefined) {
            return;
        }

        if (dispatcher._listeners[type]) {
            delete dispatcher._listeners[type];
        }
    }

    static mouseToRay(mouse: any, camera: any, width: number, height: number) {
        const normalizedMouse = {
            x: (mouse.x / width) * 2 - 1,
            y: -(mouse.y / height) * 2 + 1
        };

        const vector = new Vector3(normalizedMouse.x, normalizedMouse.y, 0.5);
        const origin = camera.position.clone();
        vector.unproject(camera);
        const direction = new Vector3().subVectors(vector, origin).normalize();

        return new Ray(origin, direction);
    }

    static projectedRadius(radius: any, camera: any, distance: number, screenWidth: number, screenHeight: number) {
        // if (camera instanceof OrthographicCamera) {
        if (camera.isOrthographicCamera) {
            return Utils.projectedRadiusOrtho(radius, camera.projectionMatrix, screenWidth, screenHeight);
            // @ts-ignore
        } else if (camera instanceof PerspectiveCamera) {
            return Utils.projectedRadiusPerspective(radius, camera.fov * Math.PI / 180, distance, screenHeight);
        } else {
            throw new Error("invalid parameters");
        }
    }

    static projectedRadiusPerspective(radius: any, fov: any, distance: number, screenHeight: number) {
        let projFactor = (1 / Math.tan(fov / 2)) / distance;
        projFactor = projFactor * screenHeight / 2;

        return radius * projFactor;
    }

    static projectedRadiusOrtho(radius: any, proj: Matrix4, screenWidth: number, screenHeight: number) {
        let p1: Vector3 | Vector4 = new Vector4(0);
        let p2: Vector3 | Vector4 = new Vector4(radius);

        p1.applyMatrix4(proj);
        p2.applyMatrix4(proj);
        p1 = new Vector3(p1.x, p1.y, p1.z);
        p2 = new Vector3(p2.x, p2.y, p2.z);
        p1.x = (p1.x + 1.0) * 0.5 * screenWidth;
        p1.y = (p1.y + 1.0) * 0.5 * screenHeight;
        p2.x = (p2.x + 1.0) * 0.5 * screenWidth;
        p2.y = (p2.y + 1.0) * 0.5 * screenHeight;

        return p1.distanceTo(p2);
    }

    static topView(camera: any, node: any) {
        camera.position.set(0, 1, 0);
        camera.rotation.set(-Math.PI / 2, 0, 0);
        camera.zoomTo(node, 1);
    }

    static frontView(camera: any, node: any) {
        camera.position.set(0, 0, 1);
        camera.rotation.set(0, 0, 0);
        camera.zoomTo(node, 1);
    }

    static leftView(camera: any, node: any) {
        camera.position.set(-1, 0, 0);
        camera.rotation.set(0, -Math.PI / 2, 0);
        camera.zoomTo(node, 1);
    }

    static rightView(camera: any, node: any) {
        camera.position.set(1, 0, 0);
        camera.rotation.set(0, Math.PI / 2, 0);
        camera.zoomTo(node, 1);
    }

    static findClosestGpsTime(target: any, viewer: Viewer) {
        const start = performance.now();

        const nodes: any[] = [];

        for (const pc of viewer.scene.pointClouds) {
            nodes.push(pc.root);

            for (const child of pc.root?.children!) {
                if (child) {
                    nodes.push(child);
                }
            }
        }

        let closestNode: any = null;
        let closestIndex = Infinity;
        let closestDistance = Infinity;
        // let closestValue = 0;

        for (const node of nodes) {
            const isOkay = node.geometryNode != null
                && node.geometryNode.geometry != null
                && node.sceneNode != null;

            if (!isOkay) {
                continue;
            }

            const geometry = node.geometryNode!.geometry;
            const gpsTime = geometry.attributes["gps-time"];
            const range = gpsTime.potree.range;

            for (let i = 0; i < gpsTime.array.length; i++) {
                const value = gpsTime.array[i] * ((range[1] - range[0]) + range[0]);
                const distance = Math.abs(target - value);

                if (distance < closestDistance) {
                    closestIndex = i;
                    closestDistance = distance;
                    // closestValue = value;
                    closestNode = node;
                    //console.log("found a closer one: " + value);
                }
            }
        }

        const geometry = closestNode.geometryNode.geometry;
        const position = new Vector3(
            geometry.attributes.position.array[3 * closestIndex],
            geometry.attributes.position.array[3 * closestIndex + 1],
            geometry.attributes.position.array[3 * closestIndex + 2],
        );

        position.applyMatrix4(closestNode.sceneNode.matrixWorld);

        const end = performance.now();
        const duration = (end - start);
        console.log(`duration: ${duration.toFixed(3)}ms`);

        return {
            node: closestNode,
            index: closestIndex,
            position: position
        }
    }

    /**
     *
     * 0: no intersection
     * 1: intersection
     * 2: fully inside
     */
    static frustumSphereIntersection(frustum: any, sphere: any) {
        const planes = frustum.planes;
        const center = sphere.center;
        const negRadius = -sphere.radius;

        let minDistance = Number.MAX_VALUE;

        for (let i = 0; i < 6; i++) {
            let distance = planes[i].distanceToPoint(center);

            if (distance < negRadius) {
                return 0;
            }

            minDistance = Math.min(minDistance, distance);
        }

        return (minDistance >= sphere.radius) ? 2 : 1;
    }

    // code taken from three.js
    // ImageUtils - generateDataTexture()
    static generateDataTexture(width: number, height: number, color: Color) {
        const size = width * height;
        const data = new Uint8Array(4 * width * height);

        const r = Math.floor(color.r * 255);
        const g = Math.floor(color.g * 255);
        const b = Math.floor(color.b * 255);

        for (let i = 0; i < size; i++) {
            data[i * 3] = r;
            data[i * 3 + 1] = g;
            data[i * 3 + 2] = b;
        }

        const texture = new DataTexture(data, width, height, RGBAFormat);
        texture.needsUpdate = true;
        texture.magFilter = NearestFilter;

        return texture;
    }

    /** @see http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript */
    static getParameterByName(name: string) {
        name = name.replace(/[[]/, '\\[').replace(/[\]]/, '\\]');
        const regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        const results = regex.exec(document.location.search);

        return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, ' '));
    }

    static createChildAABB(aabb: Box3, index: number) {
        const min = aabb.min.clone();
        const max = aabb.max.clone();
        const size = new Vector3().subVectors(max, min);

        if ((index & 0b0001) > 0) {
            min.z += size.z / 2;
        } else {
            max.z -= size.z / 2;
        }

        if ((index & 0b0010) > 0) {
            min.y += size.y / 2;
        } else {
            max.y -= size.y / 2;
        }

        if ((index & 0b0100) > 0) {
            min.x += size.x / 2;
        } else {
            max.x -= size.x / 2;
        }

        return new Box3(min, max);
    }

    static lineToLineIntersection(P0: Vector3, P1: Vector3, P2: Vector3, P3: Vector3) {
        const P = [P0, P1, P2, P3];

        const d = (m: any, n: any, o: any, p: any) => {
            return (P[m].x - P[n].x) * (P[o].x - P[p].x)
                + (P[m].y - P[n].y) * (P[o].y - P[p].y)
                + (P[m].z - P[n].z) * (P[o].z - P[p].z);
        };

        let mua = (d(0, 2, 3, 2) * d(3, 2, 1, 0) - d(0, 2, 1, 0) * d(3, 2, 3, 2))
            / (d(1, 0, 1, 0) * d(3, 2, 3, 2) - d(3, 2, 1, 0) * d(3, 2, 1, 0));
        // Fix error division by zero resulting in NaN results for some calculations.
        mua = isNaN(mua) ? 0 : mua;

        let mub = (d(0, 2, 3, 2) + mua * d(3, 2, 1, 0))
            / d(3, 2, 3, 2);
        // Fix error division by zero resulting in NaN results for some calculations.
        mub = isNaN(mub) ? 0 : mub;

        const P01 = P1.clone().sub(P0);
        const P23 = P3.clone().sub(P2);

        const Pa = P0.clone().add(P01.multiplyScalar(mua));
        const Pb = P2.clone().add(P23.multiplyScalar(mub));

        return Pa.clone().add(Pb).multiplyScalar(0.5);
    }

    static computeCircleCenter(A: Vector3, B: Vector3, C: Vector3) {
        const AB = B.clone().sub(A);
        const AC = C.clone().sub(A);

        const N = AC.clone().cross(AB).normalize();

        const ab_dir = AB.clone().cross(N).normalize();
        const ac_dir = AC.clone().cross(N).normalize();

        const ab_origin = A.clone().add(B).multiplyScalar(0.5);
        const ac_origin = A.clone().add(C).multiplyScalar(0.5);

        const P0 = ab_origin;
        const P1 = ab_origin.clone().add(ab_dir);

        const P2 = ac_origin;
        const P3 = ac_origin.clone().add(ac_dir);

        return Utils.lineToLineIntersection(P0, P1, P2, P3);

        // Utils.debugLine(viewer.scene.scene, P0, P1, 0x00ff00);
        // Utils.debugLine(viewer.scene.scene, P2, P3, 0x0000ff);

        // Utils.debugSphere(viewer.scene.scene, center, 0.03, 0xff00ff);

        // const radius = center.distanceTo(A);
        // Utils.debugCircle(viewer.scene.scene, center, radius, new Vector3(0, 0, 1), 0xff00ff);
    }

    static getNorthVec(p1: Vector3, distance: number, projection?: string) {
        if (projection) {
            // if there is a projection, transform coordinates to WGS84
            // and compute angle to north there

            proj4.defs("pointcloud", projection);
            const transform = proj4("pointcloud", "WGS84");

            const llP1 = transform.forward(p1.toArray());
            let llP2 = transform.forward([p1.x, p1.y + distance]);
            const polarRadius = Math.sqrt((llP2[0] - llP1[0]) ** 2 + (llP2[1] - llP1[1]) ** 2);
            llP2 = [llP1[0], llP1[1] + polarRadius];

            const northVec = transform.inverse(llP2);

            return new Vector3(...northVec, p1.z).sub(p1);
        } else {
            // if there is no projection, assume [0, 1, 0] as north direction
            return new Vector3(0, 1, 0).multiplyScalar(distance);
        }
    }

    static computeAzimuth(p1: Vector3, p2: Vector3, projection?: string) {
        let azimuth: number;

        if (projection) {
            // if there is a projection, transform coordinates to WGS84
            // and compute angle to north there

            let transform;

            if (projection.includes('EPSG')) {
                transform = proj4(projection, "WGS84");
            } else {
                proj4.defs("pointcloud", projection);
                transform = proj4("pointcloud", "WGS84");
            }

            const llP1 = transform.forward(p1.toArray());
            const llP2 = transform.forward(p2.toArray());
            const dir = [
                llP2[0] - llP1[0],
                llP2[1] - llP1[1],
            ];

            azimuth = Math.atan2(dir[1], dir[0]) - Math.PI / 2;
        } else {
            // if there is no projection, assume [0, 1, 0] as north direction

            const dir = [p2.x - p1.x, p2.y - p1.y];
            azimuth = Math.atan2(dir[1], dir[0]) - Math.PI / 2;
        }

        // make clockwise
        azimuth = -azimuth;

        return azimuth;
    }

    static async loadScript(url: any) {
        return new Promise((resolve: any) => {
            const element = document.getElementById(url);

            if (element) {
                resolve();
            } else {
                const script = document.createElement("script");

                script.id = url;

                script.onload = () => {
                    resolve();
                };
                script.src = url;

                document.body.appendChild(script);
            }
        });
    }

    static createSvgGradient(scheme: any) {
        // this is what we are creating:
        //
        //<svg width="1em" height="3em"  xmlns="http://www.w3.org/2000/svg">
        //	<defs>
        //		<linearGradient id="gradientID" gradientTransform="rotate(90)">
        //		<stop offset="0%"  stop-color="rgb(93, 78, 162)" />
        //		...
        //		<stop offset="100%"  stop-color="rgb(157, 0, 65)" />
        //		</linearGradient>
        //	</defs>
        //
        //	<rect width="100%" height="100%" fill="url('#myGradient')" stroke="black" stroke-width="0.1em"/>
        //</svg>


        const gradientId = `${Math.random()}_${Date.now()}`;

        const svgN = "http://www.w3.org/2000/svg";
        const svg = document.createElementNS(svgN, "svg");
        svg.setAttributeNS(null, "width", "2em");
        svg.setAttributeNS(null, "height", "3em");

        { // <defs>
            const defs = document.createElementNS(svgN, "defs");

            const linearGradient = document.createElementNS(svgN, "linearGradient");
            linearGradient.setAttributeNS(null, "id", gradientId);
            linearGradient.setAttributeNS(null, "gradientTransform", "rotate(90)");

            for (let i = scheme.length - 1; i >= 0; i--) {
                const stopVal = scheme[i];
                // @ts-ignore
                const percent = parseInt(100 - stopVal[0] * 100);
                // @ts-ignore
                const [r, g, b] = stopVal[1].toArray().map(v => parseInt(v * 255));

                const stop = document.createElementNS(svgN, "stop");
                stop.setAttributeNS(null, "offset", `${percent}%`);
                stop.setAttributeNS(null, "stop-color", `rgb(${r}, ${g}, ${b})`);

                linearGradient.appendChild(stop);
            }

            defs.appendChild(linearGradient);
            svg.appendChild(defs);
        }

        const rect = document.createElementNS(svgN, "rect");
        rect.setAttributeNS(null, "width", `100%`);
        rect.setAttributeNS(null, "height", `100%`);
        rect.setAttributeNS(null, "fill", `url("#${gradientId}")`);
        rect.setAttributeNS(null, "stroke", `black`);
        rect.setAttributeNS(null, "stroke-width", `0.1em`);

        svg.appendChild(rect);

        return svg;
    }

    static async waitAny(promises: any) {
        return new Promise((resolve: any) => {
            promises.map((promise: any) => promise.then(() => {
                resolve();
            }));
        })
    }

    static screenPassRender(renderer: WebGLRenderer, material: Material, target?: WebGLRenderTarget) {
        const screenScene = new Scene();

        const screenQuad = new Mesh(new PlaneBufferGeometry(2, 2, 1), material);
        screenQuad.material.depthTest = true;
        screenQuad.material.depthWrite = true;
        screenQuad.material.transparent = true;

        screenScene.add(screenQuad);

        const camera = new Camera();

        if (target) {
            renderer.setRenderTarget(target);
        }

        renderer.render(screenScene, camera);
    }
}
