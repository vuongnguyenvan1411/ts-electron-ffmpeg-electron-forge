import {
    AddEquation,
    AlphaFormat,
    BufferGeometry,
    ByteType,
    CanvasTexture,
    ClampToEdgeWrapping,
    DataTexture,
    DepthFormat,
    DepthStencilFormat,
    DstAlphaFactor,
    DstColorFactor,
    FloatType,
    HalfFloatType,
    IntType,
    LinearFilter,
    LinearMipMapLinearFilter,
    LinearMipMapNearestFilter,
    LuminanceAlphaFormat,
    LuminanceFormat,
    Matrix4,
    MaxEquation,
    MinEquation,
    MirroredRepeatWrapping,
    NearestFilter,
    NearestMipMapLinearFilter,
    NearestMipMapNearestFilter,
    Object3D,
    OneFactor,
    OneMinusDstAlphaFactor,
    OneMinusDstColorFactor,
    OneMinusSrcAlphaFactor,
    OneMinusSrcColorFactor,
    OrthographicCamera,
    PerspectiveCamera,
    RepeatWrapping,
    ReverseSubtractEquation,
    RGB_ETC1_Format,
    RGB_PVRTC_2BPPV1_Format,
    RGB_PVRTC_4BPPV1_Format,
    RGB_S3TC_DXT1_Format,
    RGBA_PVRTC_2BPPV1_Format,
    RGBA_PVRTC_4BPPV1_Format,
    RGBA_S3TC_DXT1_Format,
    RGBA_S3TC_DXT3_Format,
    RGBA_S3TC_DXT5_Format,
    RGBAFormat,
    ShortType,
    SrcAlphaFactor,
    SrcAlphaSaturateFactor,
    SrcColorFactor,
    SubtractEquation,
    Texture,
    UnsignedByteType,
    UnsignedInt248Type,
    UnsignedIntType,
    UnsignedShort4444Type,
    UnsignedShort5551Type,
    UnsignedShortType,
    WebGLRenderer,
    WebGLRenderTarget,
    ZeroFactor,
} from "three";
import {PointCloudTree} from "./PointCloudTree";
import {ClipTask, ElevationGradientRepeat, PointSizeType} from "./Defines";
import {Helpers} from "./Helpers";
import {App} from "./App";
import {BufferAttribute} from "three/src/core/BufferAttribute";
import {PointCloudOctree, PointCloudOctreeNode} from "./PointCloudOctree";
import {Camera} from "three/src/cameras/Camera";
import {Scene as ThreeScene} from "three/src/scenes/Scene";

// Copied from three.js: WebGLRenderer.js
function paramThreeToGL(_gl: WebGLRenderingContext, p: number) {
    let extension;

    if (p === RepeatWrapping) return _gl.REPEAT;
    if (p === ClampToEdgeWrapping) return _gl.CLAMP_TO_EDGE;
    if (p === MirroredRepeatWrapping) return _gl.MIRRORED_REPEAT;

    if (p === NearestFilter) return _gl.NEAREST;
    if (p === NearestMipMapNearestFilter) return _gl.NEAREST_MIPMAP_NEAREST;
    if (p === NearestMipMapLinearFilter) return _gl.NEAREST_MIPMAP_LINEAR;

    if (p === LinearFilter) return _gl.LINEAR;
    if (p === LinearMipMapNearestFilter) return _gl.LINEAR_MIPMAP_NEAREST;
    if (p === LinearMipMapLinearFilter) return _gl.LINEAR_MIPMAP_LINEAR;

    if (p === UnsignedByteType) return _gl.UNSIGNED_BYTE;
    if (p === UnsignedShort4444Type) return _gl.UNSIGNED_SHORT_4_4_4_4;
    if (p === UnsignedShort5551Type) return _gl.UNSIGNED_SHORT_5_5_5_1;
    // if (p === UnsignedShort565Type) return _gl.UNSIGNED_SHORT_5_6_5;

    if (p === ByteType) return _gl.BYTE;
    if (p === ShortType) return _gl.SHORT;
    if (p === UnsignedShortType) return _gl.UNSIGNED_SHORT;
    if (p === IntType) return _gl.INT;
    if (p === UnsignedIntType) return _gl.UNSIGNED_INT;
    if (p === FloatType) return _gl.FLOAT;

    if (p === HalfFloatType) {
        // @ts-ignore
        extension = extensions.get('OES_texture_half_float');

        if (extension !== null) return extension.HALF_FLOAT_OES;
    }

    if (p === AlphaFormat) return _gl.ALPHA;
    // if (p === RGBFormat) return _gl.RGB;
    if (p === RGBAFormat) return _gl.RGBA;
    if (p === LuminanceFormat) return _gl.LUMINANCE;
    if (p === LuminanceAlphaFormat) return _gl.LUMINANCE_ALPHA;
    if (p === DepthFormat) return _gl.DEPTH_COMPONENT;
    if (p === DepthStencilFormat) return _gl.DEPTH_STENCIL;

    if (p === AddEquation) return _gl.FUNC_ADD;
    if (p === SubtractEquation) return _gl.FUNC_SUBTRACT;
    if (p === ReverseSubtractEquation) return _gl.FUNC_REVERSE_SUBTRACT;

    if (p === ZeroFactor) return _gl.ZERO;
    if (p === OneFactor) return _gl.ONE;
    if (p === SrcColorFactor) return _gl.SRC_COLOR;
    if (p === OneMinusSrcColorFactor) return _gl.ONE_MINUS_SRC_COLOR;
    if (p === SrcAlphaFactor) return _gl.SRC_ALPHA;
    if (p === OneMinusSrcAlphaFactor) return _gl.ONE_MINUS_SRC_ALPHA;
    if (p === DstAlphaFactor) return _gl.DST_ALPHA;
    if (p === OneMinusDstAlphaFactor) return _gl.ONE_MINUS_DST_ALPHA;

    if (p === DstColorFactor) return _gl.DST_COLOR;
    if (p === OneMinusDstColorFactor) return _gl.ONE_MINUS_DST_COLOR;
    if (p === SrcAlphaSaturateFactor) return _gl.SRC_ALPHA_SATURATE;

    if (p === RGB_S3TC_DXT1_Format || p === RGBA_S3TC_DXT1_Format ||
        p === RGBA_S3TC_DXT3_Format || p === RGBA_S3TC_DXT5_Format) {

        // @ts-ignore
        extension = extensions.get('WEBGL_compressed_texture_s3tc');

        if (extension !== null) {
            if (p === RGB_S3TC_DXT1_Format) return extension.COMPRESSED_RGB_S3TC_DXT1_EXT;
            if (p === RGBA_S3TC_DXT1_Format) return extension.COMPRESSED_RGBA_S3TC_DXT1_EXT;
            if (p === RGBA_S3TC_DXT3_Format) return extension.COMPRESSED_RGBA_S3TC_DXT3_EXT;
            if (p === RGBA_S3TC_DXT5_Format) return extension.COMPRESSED_RGBA_S3TC_DXT5_EXT;
        }
    }

    if (p === RGB_PVRTC_4BPPV1_Format || p === RGB_PVRTC_2BPPV1_Format ||
        p === RGBA_PVRTC_4BPPV1_Format || p === RGBA_PVRTC_2BPPV1_Format) {

        // @ts-ignore
        extension = extensions.get('WEBGL_compressed_texture_pvrtc');

        if (extension !== null) {
            if (p === RGB_PVRTC_4BPPV1_Format) return extension.COMPRESSED_RGB_PVRTC_4BPPV1_IMG;
            if (p === RGB_PVRTC_2BPPV1_Format) return extension.COMPRESSED_RGB_PVRTC_2BPPV1_IMG;
            if (p === RGBA_PVRTC_4BPPV1_Format) return extension.COMPRESSED_RGBA_PVRTC_4BPPV1_IMG;
            if (p === RGBA_PVRTC_2BPPV1_Format) return extension.COMPRESSED_RGBA_PVRTC_2BPPV1_IMG;
        }

    }

    if (p === RGB_ETC1_Format) {
        // @ts-ignore
        extension = extensions.get('WEBGL_compressed_texture_etc1');

        if (extension !== null) return extension.COMPRESSED_RGB_ETC1_WEBGL;
    }

    if (p === MinEquation || p === MaxEquation) {
        // @ts-ignore
        extension = extensions.get('EXT_blend_minmax');

        if (extension !== null) {

            if (p === MinEquation) return extension.MIN_EXT;
            if (p === MaxEquation) return extension.MAX_EXT;

        }

    }

    if (p === UnsignedInt248Type) {
        // @ts-ignore
        extension = extensions.get('WEBGL_depth_texture');

        if (extension !== null) return extension.UNSIGNED_INT_24_8_WEBGL;
    }

    return 0;
}

const attributeLocations: any = {
    "position": {name: "position", location: 0},
    "color": {name: "color", location: 1},
    "rgba": {name: "color", location: 1},
    "intensity": {name: "intensity", location: 2},
    "classification": {name: "classification", location: 3},
    "returnNumber": {name: "returnNumber", location: 4},
    "return number": {name: "returnNumber", location: 4},
    "returns": {name: "returnNumber", location: 4},
    "numberOfReturns": {name: "numberOfReturns", location: 5},
    "number of returns": {name: "numberOfReturns", location: 5},
    "pointSourceID": {name: "pointSourceID", location: 6},
    "source id": {name: "pointSourceID", location: 6},
    "point source id": {name: "pointSourceID", location: 6},
    "indices": {name: "indices", location: 7},
    "normal": {name: "normal", location: 8},
    "spacing": {name: "spacing", location: 9},
    "gps-time": {name: "gpsTime", location: 10},
    "aExtra": {name: "aExtra", location: 11},
};

class Shader {
    gl: WebGLRenderingContext;
    name: string;
    vsSource: string;
    fsSource: string;
    cache: Map<any, any>;
    vs: any;
    fs: any;
    program: any;
    uniformLocations: any;
    attributeLocations: any;
    uniformBlockIndices: any;
    uniformBlocks: any;
    uniforms: any;

    constructor(gl: WebGLRenderingContext, name: string, vsSource: string, fsSource: string) {
        this.gl = gl;
        this.name = name;
        this.vsSource = vsSource;
        this.fsSource = fsSource;

        this.cache = new Map();

        this.vs = null;
        this.fs = null;
        this.program = null;

        this.uniformLocations = {};
        this.attributeLocations = {};
        this.uniformBlockIndices = {};
        this.uniformBlocks = {};
        this.uniforms = {};

        this.update(vsSource, fsSource);
    }

    update(vsSource: string, fsSource: string) {
        this.vsSource = vsSource;
        this.fsSource = fsSource;

        this.linkProgram();
    }

    compileShader(shader: WebGLShader, source: string) {
        const gl = this.gl;

        gl.shaderSource(shader, source);
        gl.compileShader(shader);

        const success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);

        if (!success) {
            let info = gl.getShaderInfoLog(shader);
            let numberedSource = source.split("\n").map((a: any, i: any) => `${i + 1}`.padEnd(5) + a).join("\n");

            throw Error(`could not compile shader ${this.name}: ${info}, \n${numberedSource}`);
        }
    }

    linkProgram() {
        const tStart = performance.now();

        const gl = this.gl;

        this.uniformLocations = {};
        this.attributeLocations = {};
        this.uniforms = {};

        gl.useProgram(null);

        const cached = this.cache.get(`${this.vsSource}, ${this.fsSource}`);
        if (cached) {
            this.program = cached.program;
            this.vs = cached.vs;
            this.fs = cached.fs;
            this.attributeLocations = cached.attributeLocations;
            this.uniformLocations = cached.uniformLocations;
            this.uniformBlocks = cached.uniformBlocks;
            this.uniforms = cached.uniforms;

            return;
        } else {

            this.vs = gl.createShader(gl.VERTEX_SHADER);
            this.fs = gl.createShader(gl.FRAGMENT_SHADER);
            this.program = gl.createProgram();

            for (let name of Object.keys(attributeLocations)) {
                let location = attributeLocations[name].location;
                let glslName = attributeLocations[name].name;
                gl.bindAttribLocation(this.program, location, glslName);
            }

            this.compileShader(this.vs, this.vsSource);
            this.compileShader(this.fs, this.fsSource);

            let program = this.program;

            gl.attachShader(program, this.vs);
            gl.attachShader(program, this.fs);

            gl.linkProgram(program);

            gl.detachShader(program, this.vs);
            gl.detachShader(program, this.fs);

            let success = gl.getProgramParameter(program, gl.LINK_STATUS);
            if (!success) {
                const info = gl.getProgramInfoLog(program);
                throw Error(`could not link program ${this.name}: ${info}`);
            }

            { // attribute locations
                let numAttributes = gl.getProgramParameter(program, gl.ACTIVE_ATTRIBUTES);

                for (let i = 0; i < numAttributes; i++) {
                    let attribute: any = gl.getActiveAttrib(program, i);

                    this.attributeLocations[attribute.name] = gl.getAttribLocation(program, attribute.name);
                }
            }

            { // uniform locations
                let numUniforms = gl.getProgramParameter(program, gl.ACTIVE_UNIFORMS);

                for (let i = 0; i < numUniforms; i++) {
                    let uniform: any = gl.getActiveUniform(program, i);
                    let location = gl.getUniformLocation(program, uniform.name);

                    this.uniformLocations[uniform.name] = location;
                    this.uniforms[uniform.name] = {
                        location: location,
                        value: null,
                    };
                }
            }

            // uniform blocks
            if (Helpers.isSupportWebGL2() && gl instanceof WebGL2RenderingContext) {
                const numBlocks = gl.getProgramParameter(program, gl.ACTIVE_UNIFORM_BLOCKS);

                for (let i = 0; i < numBlocks; i++) {
                    const blockName: any = gl.getActiveUniformBlockName(program, i);
                    const blockIndex = gl.getUniformBlockIndex(program, blockName);

                    this.uniformBlockIndices[blockName] = blockIndex;

                    gl.uniformBlockBinding(program, blockIndex, blockIndex);
                    const dataSize = gl.getActiveUniformBlockParameter(program, blockIndex, gl.UNIFORM_BLOCK_DATA_SIZE);

                    const uBuffer = gl.createBuffer();
                    gl.bindBuffer(gl.UNIFORM_BUFFER, uBuffer);
                    gl.bufferData(gl.UNIFORM_BUFFER, dataSize, gl.DYNAMIC_READ);

                    gl.bindBufferBase(gl.UNIFORM_BUFFER, blockIndex, uBuffer);

                    gl.bindBuffer(gl.UNIFORM_BUFFER, null);

                    this.uniformBlocks[blockName] = {
                        name: blockName,
                        index: blockIndex,
                        dataSize: dataSize,
                        buffer: uBuffer
                    };
                }
            }

            const cached = {
                program: this.program,
                vs: this.vs,
                fs: this.fs,
                attributeLocations: this.attributeLocations,
                uniformLocations: this.uniformLocations,
                uniforms: this.uniforms,
                uniformBlocks: this.uniformBlocks,
            };

            this.cache.set(`${this.vsSource}, ${this.fsSource}`, cached);
        }

        const tEnd = performance.now();
        const duration = tEnd - tStart;

        console.log(`shader compile duration: ${duration.toFixed(3)}`);
    }

    setUniformMatrix4(name: string, value: Matrix4) {
        const gl = this.gl;
        const location = this.uniformLocations[name];

        if (location == null) {
            return;
        }

        const tmp = new Float32Array(value.elements);
        gl.uniformMatrix4fv(location, false, tmp);
    }

    setUniform1f(name: string, value: number) {
        const gl = this.gl;
        const uniform = this.uniforms[name];

        if (uniform === undefined) {
            return;
        }

        if (uniform.value === value) {
            return;
        }

        uniform.value = value;

        gl.uniform1f(uniform.location, value);
    }

    setUniformBoolean(name: string, value: any) {
        const gl = this.gl;
        const uniform = this.uniforms[name];

        if (uniform === undefined) {
            return;
        }

        if (uniform.value === value) {
            return;
        }

        uniform.value = value;

        gl.uniform1i(uniform.location, value);
    }

    setUniformTexture(name: string, value: any) {
        const gl = this.gl;
        const location = this.uniformLocations[name];

        if (location == null) {
            return;
        }

        gl.uniform1i(location, value);
    }

    setUniform2f(name: string, value: number[]) {
        const gl = this.gl;
        const location = this.uniformLocations[name];

        if (location == null) {
            return;
        }

        gl.uniform2f(location, value[0], value[1]);
    }

    setUniform3f(name: string, value: number[]) {
        const gl = this.gl;
        const location = this.uniformLocations[name];

        if (location == null) {
            return;
        }

        gl.uniform3f(location, value[0], value[1], value[2]);
    }

    setUniform(name: string, value: any) {
        if (value.constructor === Matrix4) {
            this.setUniformMatrix4(name, value);
        } else if (typeof value === "number") {
            this.setUniform1f(name, value);
        } else if (typeof value === "boolean") {
            this.setUniformBoolean(name, value);
        } else if (value instanceof WebGLTexture) {
            this.setUniformTexture(name, value);
        } else if (value instanceof Array) {
            if (value.length === 2) {
                this.setUniform2f(name, value);
            } else if (value.length === 3) {
                this.setUniform3f(name, value);
            }
        } else {
            console.error("unhandled uniform type: ", name, value);
        }
    }

    setUniform1i(name: string, value: number) {
        let gl = this.gl;
        let location = this.uniformLocations[name];

        if (location == null) {
            return;
        }

        gl.uniform1i(location, value);
    }
}

class WebGLTexture {
    gl: WebGLRenderingContext;
    texture: CanvasTexture | Texture | any;
    id: WebGLTexture;
    target: GLenum;
    version: number;

    constructor(gl: WebGLRenderingContext, texture: CanvasTexture) {
        this.gl = gl;

        this.texture = texture;
        this.id = gl.createTexture() as any;

        this.target = gl.TEXTURE_2D;
        this.version = -1;

        this.update();
    }

    update() {
        if (!this.texture.image) {
            this.version = this.texture.version;

            return;
        }

        const gl = this.gl;
        const texture = this.texture;

        if (this.version === texture.version) {
            return;
        }

        this.target = gl.TEXTURE_2D;

        gl.bindTexture(this.target, this.id);

        let level = 0;
        let internalFormat = paramThreeToGL(gl, texture.format);
        let width = texture.image.width;
        let height = texture.image.height;
        let border = 0;
        let srcFormat = internalFormat;
        let srcType = paramThreeToGL(gl, texture.type);
        let data;

        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, texture.flipY);
        gl.pixelStorei(gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, texture.premultiplyAlpha);
        gl.pixelStorei(gl.UNPACK_ALIGNMENT, texture.unpackAlignment);

        if (texture instanceof DataTexture) {
            data = texture.image.data;

            gl.texParameteri(this.target, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
            gl.texParameteri(this.target, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

            gl.texParameteri(this.target, gl.TEXTURE_MAG_FILTER, paramThreeToGL(gl, texture.magFilter));
            gl.texParameteri(this.target, gl.TEXTURE_MIN_FILTER, paramThreeToGL(gl, texture.minFilter));

            gl.texImage2D(this.target, level, internalFormat, width, height, border, srcFormat, srcType, data);
        } else if ((texture instanceof CanvasTexture) || (texture instanceof Texture)) {
            data = texture.image;

            gl.texParameteri(this.target, gl.TEXTURE_WRAP_S, paramThreeToGL(gl, texture.wrapS));
            gl.texParameteri(this.target, gl.TEXTURE_WRAP_T, paramThreeToGL(gl, texture.wrapT));

            gl.texParameteri(this.target, gl.TEXTURE_MAG_FILTER, paramThreeToGL(gl, texture.magFilter));
            gl.texParameteri(this.target, gl.TEXTURE_MIN_FILTER, paramThreeToGL(gl, texture.minFilter));

            gl.texImage2D(this.target, level, internalFormat, internalFormat, srcType, data);

            if ((texture as any) instanceof Texture) {
                gl.generateMipmap(gl.TEXTURE_2D);
            }
        }

        gl.bindTexture(this.target, null);

        this.version = texture.version;
    }
}

class WebGLBuffer {
    numElements: number;
    vao: any;
    vbos: Map<any, any>;

    constructor() {
        this.numElements = 0;
        this.vao = null;
        this.vbos = new Map();
    }
}

export class CRenderer {
    threeRenderer: WebGLRenderer;
    gl: WebGLRenderingContext;
    buffers: Map<any, any>;
    shaders: Map<any, any>;
    textures: Map<any, any>;
    glTypeMapping: Map<any, any>;
    toggle: number;

    constructor(threeRenderer: WebGLRenderer) {
        this.threeRenderer = threeRenderer;
        this.gl = this.threeRenderer.getContext();

        this.buffers = new Map();
        this.shaders = new Map();
        this.textures = new Map();

        this.glTypeMapping = new Map();
        this.glTypeMapping.set(Float32Array, this.gl.FLOAT);
        this.glTypeMapping.set(Uint8Array, this.gl.UNSIGNED_BYTE);
        this.glTypeMapping.set(Uint16Array, this.gl.UNSIGNED_SHORT);

        this.toggle = 0;
    }

    deleteBuffer(geometry: BufferGeometry) {
        const gl = this.gl;
        const webglBuffer = this.buffers.get(geometry);

        if (webglBuffer != null) {
            for (let attributeName in geometry.attributes) {
                if (!geometry.attributes.hasOwnProperty(attributeName)) {
                    continue;
                }

                gl.deleteBuffer(webglBuffer.vbos.get(attributeName).handle);
            }

            this.buffers.delete(geometry);
        }
    }

    createBuffer(geometry: BufferGeometry) {
        const gl = this.gl as WebGL2RenderingContext;
        const webglBuffer = new WebGLBuffer();

        webglBuffer.vao = gl.createVertexArray();
        webglBuffer.numElements = geometry.attributes.position.count;

        gl.bindVertexArray(webglBuffer.vao);

        for (let attributeName in geometry.attributes) {
            if (!geometry.attributes.hasOwnProperty(attributeName)) {
                continue;
            }

            const bufferAttribute = geometry.attributes[attributeName];

            const vbo = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
            // @ts-ignore
            gl.bufferData(gl.ARRAY_BUFFER, bufferAttribute.array, gl.STATIC_DRAW);

            const normalized = bufferAttribute.normalized;
            const type = this.glTypeMapping.get(bufferAttribute.array.constructor);

            if (attributeLocations[attributeName] === undefined) {
                //attributeLocation = attributeLocations["aExtra"];
            } else {
                let attributeLocation = attributeLocations[attributeName].location;

                gl.vertexAttribPointer(attributeLocation, bufferAttribute.itemSize, type, normalized, 0, 0);
                gl.enableVertexAttribArray(attributeLocation);
            }


            webglBuffer.vbos.set(attributeName, {
                handle: vbo,
                name: attributeName,
                count: bufferAttribute.count,
                itemSize: bufferAttribute.itemSize,
                type: geometry.attributes.position.array.constructor,
                version: 0
            });
        }

        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        gl.bindVertexArray(null);

        const disposeHandler = () => {
            this.deleteBuffer(geometry);
            geometry.removeEventListener("dispose", disposeHandler);
        };
        geometry.addEventListener("dispose", disposeHandler);

        return webglBuffer;
    }

    updateBuffer(geometry: BufferGeometry) {
        const gl = this.gl as WebGL2RenderingContext;

        const webglBuffer = this.buffers.get(geometry);

        gl.bindVertexArray(webglBuffer.vao);

        for (let attributeName in geometry.attributes) {
            if (!geometry.attributes.hasOwnProperty(attributeName)) {
                continue;
            }

            const bufferAttribute = geometry.attributes[attributeName] as BufferAttribute;

            const normalized = bufferAttribute.normalized;
            const type = this.glTypeMapping.get(bufferAttribute.array.constructor);

            let vbo;
            if (!webglBuffer.vbos.has(attributeName)) {
                vbo = gl.createBuffer();

                webglBuffer.vbos.set(attributeName, {
                    handle: vbo,
                    name: attributeName,
                    count: bufferAttribute.count,
                    itemSize: bufferAttribute.itemSize,
                    type: geometry.attributes.position.array.constructor,
                    version: bufferAttribute.version
                });
            } else {
                vbo = webglBuffer.vbos.get(attributeName).handle;
                webglBuffer.vbos.get(attributeName).version = bufferAttribute.version;
            }

            gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
            // @ts-ignore
            gl.bufferData(gl.ARRAY_BUFFER, bufferAttribute.array, gl.STATIC_DRAW);

            if (attributeLocations[attributeName] === undefined) {
                //attributeLocation = attributeLocations["aExtra"];
            } else {
                const attributeLocation = attributeLocations[attributeName].location;

                gl.vertexAttribPointer(attributeLocation, bufferAttribute.itemSize, type, normalized, 0, 0);
                gl.enableVertexAttribArray(attributeLocation);
            }
        }

        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        gl.bindVertexArray(null);
    }

    traverse(scene: ThreeScene) {
        const octrees: any = [];
        const stack = [scene];

        while (stack.length > 0) {
            const node = stack.pop();

            if ((node as any) instanceof PointCloudTree) {
                octrees.push(node);

                continue;
            }

            if (node) {
                const visibleChildren = node.children.filter((c: Object3D) => c.visible);

                // @ts-ignore
                stack.push(...visibleChildren);
            }
        }

        return {
            octrees: octrees
        };
    }

    renderNodes(octree: PointCloudOctree, nodes: PointCloudOctreeNode[], visibilityTextureData: any, camera: Camera, shader: Shader, params: any) {
        if (App.measureTimings) performance.mark("renderNodes-start");

        const gl = this.gl as WebGL2RenderingContext;
        const material = params.material ? params.material : octree.material;
        const shadowMaps = params.shadowMaps == null ? [] : params.shadowMaps;
        let view = camera.matrixWorldInverse;

        if (params.viewOverride) {
            view = params.viewOverride;
        }

        const worldView = new Matrix4();
        const mat4holder = new Float32Array(16);
        let i = 0;

        for (let node of nodes) {
            if (App.debug.allowedNodes !== undefined) {
                if (!App.debug.allowedNodes.includes(node.name)) {
                    continue;
                }
            }

            let world = node.sceneNode.matrixWorld;
            worldView.multiplyMatrices(view, world);

            if (visibilityTextureData) {
                let vnStart = visibilityTextureData.offsets.get(node);
                shader.setUniform1f("uVNStart", vnStart);
            }


            const level = node.getLevel();

            if (node.debug) {
                shader.setUniform("uDebug", true);
            } else {
                shader.setUniform("uDebug", false);
            }

            // let isLeaf = false;
            // if(node instanceof PointCloudOctreeNode){
            // 	isLeaf = Object.keys(node.children).length === 0;
            // }else if(node instanceof PointCloudArena4DNode){
            // 	isLeaf = node.geometryNode.isLeaf;
            // }
            // shader.setUniform("uIsLeafNode", isLeaf);

            // let isLeaf = node.children.filter(n => n != null).length === 0;
            // if(!isLeaf){
            // 	continue;
            // }


            // TODO consider passing matrices in an array to avoid uniformMatrix4fv overhead
            const lModel = shader.uniformLocations["modelMatrix"];
            if (lModel) {
                mat4holder.set(world.elements);
                gl.uniformMatrix4fv(lModel, false, mat4holder);
            }

            const lModelView = shader.uniformLocations["modelViewMatrix"];
            //mat4holder.set(worldView.elements);
            // faster than set in Chrome 63
            for (let j = 0; j < 16; j++) {
                mat4holder[j] = worldView.elements[j];
            }
            gl.uniformMatrix4fv(lModelView, false, mat4holder);

            // Clip Polygons
            if (material.clipPolygons && material.clipPolygons.length > 0) {
                const clipPolygonVCount = [];
                const worldViewProjMatrices = [];

                for (let clipPolygon of material.clipPolygons) {
                    const view = clipPolygon.viewMatrix;
                    const proj = clipPolygon.projMatrix;

                    const worldViewProj = proj.clone().multiply(view).multiply(world);

                    clipPolygonVCount.push(clipPolygon.markers.length);
                    worldViewProjMatrices.push(worldViewProj);
                }

                const flattenedMatrices = [].concat(...worldViewProjMatrices.map(m => m.elements));
                const flattenedVertices = new Array(8 * 3 * material.clipPolygons.length);

                for (let i = 0; i < material.clipPolygons.length; i++) {
                    const clipPolygon = material.clipPolygons[i];

                    for (let j = 0; j < clipPolygon.markers.length; j++) {
                        flattenedVertices[i * 24 + (j * 3)] = clipPolygon.markers[j].position.x;
                        flattenedVertices[i * 24 + (j * 3 + 1)] = clipPolygon.markers[j].position.y;
                        flattenedVertices[i * 24 + (j * 3 + 2)] = clipPolygon.markers[j].position.z;
                    }
                }

                const lClipPolygonVCount = shader.uniformLocations["uClipPolygonVCount[0]"];
                gl.uniform1iv(lClipPolygonVCount, clipPolygonVCount);

                const lClipPolygonVP = shader.uniformLocations["uClipPolygonWVP[0]"];
                gl.uniformMatrix4fv(lClipPolygonVP, false, flattenedMatrices);

                const lClipPolygons = shader.uniformLocations["uClipPolygonVertices[0]"];
                gl.uniform3fv(lClipPolygons, flattenedVertices);
            }

            //shader.setUniformMatrix4("modelMatrix", world);
            //shader.setUniformMatrix4("modelViewMatrix", worldView);
            shader.setUniform1f("uLevel", level);
            shader.setUniform1f("uNodeSpacing", node.geometryNode.estimatedSpacing);

            shader.setUniform1f("uPCIndex", i);
            // uBBSize

            if (shadowMaps.length > 0) {
                const lShadowMap = shader.uniformLocations["uShadowMap[0]"];

                shader.setUniform3f("uShadowColor", material.uniforms.uShadowColor.value);

                const bindingStart = 5;
                const bindingPoints = new Array(shadowMaps.length).fill(bindingStart).map((a, i) => (a + i));

                gl.uniform1iv(lShadowMap, bindingPoints);

                for (let i = 0; i < shadowMaps.length; i++) {
                    const shadowMap = shadowMaps[i];
                    const bindingPoint = bindingPoints[i];
                    const glTexture = this.threeRenderer.properties.get(shadowMap.target.texture).__webglTexture;

                    // @ts-ignore
                    gl.activeTexture(gl[`TEXTURE${bindingPoint}`]);
                    gl.bindTexture(gl.TEXTURE_2D, glTexture);
                }

                {
                    const worldViewMatrices = shadowMaps
                        .map((sm: any) => sm.camera.matrixWorldInverse)
                        .map((view: any) => new Matrix4().multiplyMatrices(view, world))

                    const flattenedMatrices = [].concat(...worldViewMatrices.map((c: any) => c.elements));
                    const lWorldView = shader.uniformLocations["uShadowWorldView[0]"];
                    gl.uniformMatrix4fv(lWorldView, false, flattenedMatrices);
                }

                {
                    const flattenedMatrices = [].concat(...shadowMaps.map((sm: any) => sm.camera.projectionMatrix.elements));
                    const lProj = shader.uniformLocations["uShadowProj[0]"];
                    gl.uniformMatrix4fv(lProj, false, flattenedMatrices);
                }
            }

            const geometry = node.geometryNode.geometry!;

            if (geometry.attributes["gps-time"]) {
                // const bufferAttribute = geometry.attributes["gps-time"];
                const attGPS = octree.getAttribute("gps-time");

                const initialRange = attGPS.initialRange;
                const initialRangeSize = initialRange[1] - initialRange[0];

                const globalRange = attGPS.range;
                const globalRangeSize = globalRange[1] - globalRange[0];

                let scale = initialRangeSize / globalRangeSize;
                let offset = -(globalRange[0] - initialRange[0]) / initialRangeSize;

                scale = Number.isNaN(scale) ? 1 : scale;
                offset = Number.isNaN(offset) ? 0 : offset;

                shader.setUniform1f("uGpsScale", scale);
                shader.setUniform1f("uGpsOffset", offset);
                //shader.setUniform2f("uFilterGPSTimeClipRange", [-Infinity, Infinity]);

                const uFilterGPSTimeClipRange = material.uniforms.uFilterGPSTimeClipRange.value;
                // let gpsCliPRangeMin = uFilterGPSTimeClipRange[0]
                // let gpsCliPRangeMax = uFilterGPSTimeClipRange[1]
                // shader.setUniform2f("uFilterGPSTimeClipRange", [gpsCliPRangeMin, gpsCliPRangeMax]);

                const normalizedClipRange = [
                    (uFilterGPSTimeClipRange[0] - globalRange[0]) / globalRangeSize,
                    (uFilterGPSTimeClipRange[1] - globalRange[0]) / globalRangeSize,
                ];

                shader.setUniform2f("uFilterGPSTimeClipRange", normalizedClipRange);


                // // ranges in full gps coordinate system
                // const globalRange = attGPS.range;
                // const bufferRange = bufferAttribute.potree.range;

                // // ranges in [0, 1]
                // // normalizedGlobalRange = [0, 1]
                // // normalizedBufferRange: norm buffer within norm global range e.g. [0.2, 0.8]
                // const globalWidth = globalRange[1] - globalRange[0];
                // const normalizedBufferRange = [
                // 	(bufferRange[0] - globalRange[0]) / globalWidth,
                // 	(bufferRange[1] - globalRange[0]) / globalWidth,
                // ];

                // shader.setUniform2f("uNormalizedGpsBufferRange", normalizedBufferRange);

                // let uFilterGPSTimeClipRange = material.uniforms.uFilterGPSTimeClipRange.value;
                // let gpsCliPRangeMin = uFilterGPSTimeClipRange[0]
                // let gpsCliPRangeMax = uFilterGPSTimeClipRange[1]
                // shader.setUniform2f("uFilterGPSTimeClipRange", [gpsCliPRangeMin, gpsCliPRangeMax]);

                // shader.setUniform1f("uGpsScale", bufferAttribute.potree.scale);
                // shader.setUniform1f("uGpsOffset", bufferAttribute.potree.offset);
            }

            {
                const uFilterReturnNumberRange = material.uniforms.uFilterReturnNumberRange.value;
                const uFilterNumberOfReturnsRange = material.uniforms.uFilterNumberOfReturnsRange.value;
                const uFilterPointSourceIDClipRange = material.uniforms.uFilterPointSourceIDClipRange.value;

                shader.setUniform2f("uFilterReturnNumberRange", uFilterReturnNumberRange);
                shader.setUniform2f("uFilterNumberOfReturnsRange", uFilterNumberOfReturnsRange);
                shader.setUniform2f("uFilterPointSourceIDClipRange", uFilterPointSourceIDClipRange);
            }

            let webglBuffer;

            if (!this.buffers.has(geometry)) {
                webglBuffer = this.createBuffer(geometry);
                this.buffers.set(geometry, webglBuffer);
            } else {
                webglBuffer = this.buffers.get(geometry);

                for (let attributeName in geometry.attributes) {
                    if (!geometry.attributes.hasOwnProperty(attributeName)) {
                        continue;
                    }

                    const attribute = geometry.attributes[attributeName] as BufferAttribute;

                    if (attribute.version > webglBuffer.vbos.get(attributeName).version) {
                        this.updateBuffer(geometry);
                    }
                }
            }

            gl.bindVertexArray(webglBuffer.vao);

            const isExtraAttribute =
                attributeLocations[material.activeAttributeName] === undefined
                && Object.keys(geometry.attributes).includes(material.activeAttributeName);

            if (isExtraAttribute) {
                const attributeLocation = attributeLocations["aExtra"].location;

                for (const attributeName in geometry.attributes) {
                    if (!geometry.attributes.hasOwnProperty(attributeName)) {
                        continue;
                    }

                    // const bufferAttribute = geometry.attributes[attributeName];
                    const vbo = webglBuffer.vbos.get(attributeName);

                    gl.bindBuffer(gl.ARRAY_BUFFER, vbo.handle);
                    gl.disableVertexAttribArray(attributeLocation);
                }

                const attName = material.activeAttributeName;
                const bufferAttribute = geometry.attributes[attName];
                const vbo = webglBuffer.vbos.get(attName);

                if (bufferAttribute !== undefined && vbo !== undefined) {
                    const type = this.glTypeMapping.get(bufferAttribute.array.constructor);
                    const normalized = bufferAttribute.normalized;

                    gl.bindBuffer(gl.ARRAY_BUFFER, vbo.handle);
                    gl.vertexAttribPointer(attributeLocation, bufferAttribute.itemSize, type, normalized, 0, 0);
                    gl.enableVertexAttribArray(attributeLocation);
                }

                {
                    const attExtra = octree.pcoGeometry.pointAttributes.attributes
                        .find((a: any) => a.name === attName);

                    let range = material.getRange(attName);
                    if (!range) {
                        range = attExtra.range;
                    }

                    if (!range) {
                        range = [0, 1];
                    }

                    const initialRange = attExtra.initialRange;
                    const initialRangeSize = initialRange[1] - initialRange[0];

                    const globalRange = range;
                    const globalRangeSize = globalRange[1] - globalRange[0];

                    let scale = initialRangeSize / globalRangeSize;
                    let offset = -(globalRange[0] - initialRange[0]) / initialRangeSize;

                    scale = Number.isNaN(scale) ? 1 : scale;
                    offset = Number.isNaN(offset) ? 0 : offset;

                    shader.setUniform1f("uExtraScale", scale);
                    shader.setUniform1f("uExtraOffset", offset);
                }
            } else {
                for (const attributeName in geometry.attributes) {
                    if (!geometry.attributes.hasOwnProperty(attributeName)) {
                        continue;
                    }

                    const bufferAttribute = geometry.attributes[attributeName];
                    const vbo = webglBuffer.vbos.get(attributeName);

                    if (attributeLocations[attributeName] !== undefined) {
                        const attributeLocation = attributeLocations[attributeName].location;

                        const type = this.glTypeMapping.get(bufferAttribute.array.constructor);
                        const normalized = bufferAttribute.normalized;

                        gl.bindBuffer(gl.ARRAY_BUFFER, vbo.handle);
                        gl.vertexAttribPointer(attributeLocation, bufferAttribute.itemSize, type, normalized, 0, 0);
                        gl.enableVertexAttribArray(attributeLocation);

                    }
                }
            }

            const numPoints = webglBuffer.numElements;
            gl.drawArrays(gl.POINTS, 0, numPoints);

            i++;
        }

        gl.bindVertexArray(null);

        if (App.measureTimings) {
            performance.mark("renderNodes-end");
            performance.measure("render.renderNodes", "renderNodes-start", "renderNodes-end");
        }
    }

    renderOctree(octree: PointCloudOctree, nodes: PointCloudOctreeNode[], camera: PerspectiveCamera | OrthographicCamera, target: WebGLRenderTarget, params: any = {}) {
        const gl = this.gl;

        const material = params.material ? params.material : octree.material;
        const shadowMaps = params.shadowMaps == null ? [] : params.shadowMaps;
        let view = camera.matrixWorldInverse;
        let viewInv = camera.matrixWorld;

        if (params.viewOverride) {
            view = params.viewOverride;
            viewInv = view.clone().invert();
        }

        const proj = camera.projectionMatrix;
        const projInv = proj.clone().invert();
        //let worldView = new Matrix4();

        let shader;
        let visibilityTextureData = null;

        let currentTextureBindingPoint = 0;

        if (material.pointSizeType >= 0) {
            if (material.pointSizeType === PointSizeType.ADAPTIVE ||
                material.activeAttributeName === "level of detail") {

                const vnNodes = (params.vnTextureNodes != null) ? params.vnTextureNodes : nodes;
                visibilityTextureData = octree.computeVisibilityTextureData(vnNodes);

                const vnt = material.visibleNodesTexture;
                const data = vnt.image.data;
                data.set(visibilityTextureData.data);
                vnt.needsUpdate = true;
            }
        }

        // UPDATE SHADER AND TEXTURES
        if (!this.shaders.has(material)) {
            const [vs, fs] = [material.vertexShader, material.fragmentShader];
            const shader = new Shader(gl, "pointcloud", vs, fs);

            this.shaders.set(material, shader);
        }

        shader = this.shaders.get(material);

        //if(material.needsUpdate){
        {
            let [vs, fs] = [material.vertexShader, material.fragmentShader];

            const numSnapshots = material.snapEnabled ? material.numSnapshots : 0;
            const numClipBoxes = (material.clipBoxes && material.clipBoxes.length) ? material.clipBoxes.length : 0;
            const numClipSpheres = (params.clipSpheres && params.clipSpheres.length) ? params.clipSpheres.length : 0;
            const numClipPolygons = (material.clipPolygons && material.clipPolygons.length) ? material.clipPolygons.length : 0;

            const defines = [
                `#define num_shadowmaps ${shadowMaps.length}`,
                `#define num_snapshots ${numSnapshots}`,
                `#define num_clipboxes ${numClipBoxes}`,
                `#define num_clipspheres ${numClipSpheres}`,
                `#define num_clippolygons ${numClipPolygons}`,
            ];

            if (octree.pcoGeometry.root.isLoaded()) {
                const attributes = octree.pcoGeometry.root.geometry.attributes;

                if (attributes["gps-time"]) {
                    defines.push("#define clip_gps_enabled");
                }

                if (attributes["return number"]) {
                    defines.push("#define clip_return_number_enabled");
                }

                if (attributes["number of returns"]) {
                    defines.push("#define clip_number_of_returns_enabled");
                }

                if (attributes["source id"] || attributes["point source id"]) {
                    defines.push("#define clip_point_source_id_enabled");
                }

            }

            const definesString = defines.join("\n");

            const vsVersionIndex = vs.indexOf("#version ");
            const fsVersionIndex = fs.indexOf("#version ");

            if (vsVersionIndex >= 0) {
                vs = vs.replace(/(#version .*)/, `$1\n${definesString}`)
            } else {
                vs = `${definesString}\n${vs}`;
            }

            if (fsVersionIndex >= 0) {
                fs = fs.replace(/(#version .*)/, `$1\n${definesString}`)
            } else {
                fs = `${definesString}\n${fs}`;
            }


            shader.update(vs, fs);

            material.needsUpdate = false;
        }

        for (let uniformName of Object.keys(material.uniforms)) {
            const uniform = material.uniforms[uniformName];

            if (uniform.type === "t") {
                const texture = uniform.value;

                if (!texture) {
                    continue;
                }

                if (!this.textures.has(texture)) {
                    const webglTexture = new WebGLTexture(gl, texture);

                    this.textures.set(texture, webglTexture);
                }

                const webGLTexture = this.textures.get(texture);
                webGLTexture.update();
            }
        }

        gl.useProgram(shader.program);

        let transparent;
        if (params.transparent !== undefined) {
            transparent = params.transparent && material.opacity < 1;
        } else {
            transparent = material.opacity < 1;
        }

        if (transparent) {
            gl.enable(gl.BLEND);
            gl.blendFunc(gl.SRC_ALPHA, gl.ONE);
            gl.depthMask(false);
            gl.disable(gl.DEPTH_TEST);
        } else {
            gl.disable(gl.BLEND);
            gl.depthMask(true);
            gl.enable(gl.DEPTH_TEST);
        }

        if (params.blendFunc !== undefined) {
            gl.enable(gl.BLEND);
            // @ts-ignore
            gl.blendFunc(...params.blendFunc);
        }

        if (params.depthTest !== undefined) {
            if (params.depthTest === true) {
                gl.enable(gl.DEPTH_TEST);
            } else {
                gl.disable(gl.DEPTH_TEST);
            }
        }

        if (params.depthWrite !== undefined) {
            if (params.depthWrite === true) {
                gl.depthMask(true);
            } else {
                gl.depthMask(false);
            }

        }

        { // UPDATE UNIFORMS
            shader.setUniformMatrix4("projectionMatrix", proj);
            shader.setUniformMatrix4("viewMatrix", view);
            shader.setUniformMatrix4("uViewInv", viewInv);
            shader.setUniformMatrix4("uProjInv", projInv);

            const screenWidth = target ? target.width : material.screenWidth;
            const screenHeight = target ? target.height : material.screenHeight;

            shader.setUniform1f("uScreenWidth", screenWidth);
            shader.setUniform1f("uScreenHeight", screenHeight);
            // @ts-ignore
            if (camera instanceof PerspectiveCamera) {
                // @ts-ignore
                shader.setUniform1f("fov", Math.PI * camera.fov / 180);
            }

            shader.setUniform1f("near", camera.near);
            shader.setUniform1f("far", camera.far);

            // @ts-ignore
            if (camera instanceof OrthographicCamera) {
                const _camera = camera as OrthographicCamera;
                shader.setUniform("uUseOrthographicCamera", true);
                shader.setUniform("uOrthoWidth", _camera.right - _camera.left);
                shader.setUniform("uOrthoHeight", _camera.top - _camera.bottom);
            } else {
                shader.setUniform("uUseOrthographicCamera", false);
            }

            if (material.clipBoxes.length + material.clipPolygons.length === 0) {
                shader.setUniform1i("clipTask", ClipTask.NONE);
            } else {
                shader.setUniform1i("clipTask", material.clipTask);
            }

            shader.setUniform1i("clipMethod", material.clipMethod);

            if (material.clipBoxes && material.clipBoxes.length > 0) {
                //let flattenedMatrices = [].concat(...material.clipBoxes.map(c => c.inverse.elements));

                //const lClipBoxes = shader.uniformLocations["clipBoxes[0]"];
                //gl.uniformMatrix4fv(lClipBoxes, false, flattenedMatrices);

                const lClipBoxes = shader.uniformLocations["clipBoxes[0]"];
                gl.uniformMatrix4fv(lClipBoxes, false, material.uniforms.clipBoxes.value);
            }

            // TODO CLIPSPHERES
            if (params.clipSpheres && params.clipSpheres.length > 0) {
                const clipSpheres = params.clipSpheres;
                const matrices: any[] = [];

                for (let clipSphere of clipSpheres) {
                    //let mScale = new Matrix4().makeScale(...clipSphere.scale.toArray());
                    //let mTranslate = new Matrix4().makeTranslation(...clipSphere.position.toArray());

                    //let clipToWorld = new Matrix4().multiplyMatrices(mTranslate, mScale);
                    const clipToWorld = clipSphere.matrixWorld;
                    const viewToWorld = camera.matrixWorld
                    const worldToClip = clipToWorld.clone().invert();

                    const viewToClip = new Matrix4().multiplyMatrices(worldToClip, viewToWorld);

                    matrices.push(viewToClip);
                }

                const flattenedMatrices = [].concat(...matrices.map(matrix => matrix.elements));

                const lClipSpheres = shader.uniformLocations["uClipSpheres[0]"];
                gl.uniformMatrix4fv(lClipSpheres, false, flattenedMatrices);

                //const lClipSpheres = shader.uniformLocations["uClipSpheres[0]"];
                //gl.uniformMatrix4fv(lClipSpheres, false, material.uniforms.clipSpheres.value);
            }

            shader.setUniform1f("size", material.size);
            shader.setUniform1f("maxSize", material.uniforms.maxSize.value);
            shader.setUniform1f("minSize", material.uniforms.minSize.value);

            // uniform float uPCIndex
            shader.setUniform1f("uOctreeSpacing", material.spacing);
            shader.setUniform("uOctreeSize", material.uniforms.octreeSize.value);


            //uniform vec3 uColor;
            shader.setUniform3f("uColor", material.color.toArray());
            //uniform float opacity;
            shader.setUniform1f("uOpacity", material.opacity);

            shader.setUniform2f("elevationRange", material.elevationRange);
            shader.setUniform2f("intensityRange", material.intensityRange);


            shader.setUniform3f("uIntensity_gbc", [
                material.intensityGamma,
                material.intensityBrightness,
                material.intensityContrast
            ]);

            shader.setUniform3f("uRGB_gbc", [
                material.rgbGamma,
                material.rgbBrightness,
                material.rgbContrast
            ]);

            shader.setUniform1f("uTransition", material.transition);
            shader.setUniform1f("wRGB", material.weightRGB);
            shader.setUniform1f("wIntensity", material.weightIntensity);
            shader.setUniform1f("wElevation", material.weightElevation);
            shader.setUniform1f("wClassification", material.weightClassification);
            shader.setUniform1f("wReturnNumber", material.weightReturnNumber);
            shader.setUniform1f("wSourceID", material.weightSourceID);

            shader.setUniform("backfaceCulling", material.uniforms.backfaceCulling.value);

            const vnWebGLTexture = this.textures.get(material.visibleNodesTexture);
            if (vnWebGLTexture) {
                shader.setUniform1i("visibleNodesTexture", currentTextureBindingPoint);
                gl.activeTexture(gl.TEXTURE0 + currentTextureBindingPoint);
                gl.bindTexture(vnWebGLTexture.target, vnWebGLTexture.id);
                currentTextureBindingPoint++;
            }

            const gradientTexture = this.textures.get(material.gradientTexture);
            shader.setUniform1i("gradient", currentTextureBindingPoint);
            gl.activeTexture(gl.TEXTURE0 + currentTextureBindingPoint);
            gl.bindTexture(gradientTexture.target, gradientTexture.id);

            const repeat = material.elevationGradientRepeat;
            if (repeat === ElevationGradientRepeat.REPEAT) {
                gl.texParameteri(gradientTexture.target, gl.TEXTURE_WRAP_S, gl.REPEAT);
                gl.texParameteri(gradientTexture.target, gl.TEXTURE_WRAP_T, gl.REPEAT);
            } else if (repeat === ElevationGradientRepeat.MIRRORED_REPEAT) {
                gl.texParameteri(gradientTexture.target, gl.TEXTURE_WRAP_S, gl.MIRRORED_REPEAT);
                gl.texParameteri(gradientTexture.target, gl.TEXTURE_WRAP_T, gl.MIRRORED_REPEAT);
            } else {
                gl.texParameteri(gradientTexture.target, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
                gl.texParameteri(gradientTexture.target, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
            }
            currentTextureBindingPoint++;

            const classificationTexture = this.textures.get(material.classificationTexture);
            shader.setUniform1i("classificationLUT", currentTextureBindingPoint);
            gl.activeTexture(gl.TEXTURE0 + currentTextureBindingPoint);
            gl.bindTexture(classificationTexture.target, classificationTexture.id);
            currentTextureBindingPoint++;

            const matcapTexture = this.textures.get(material.matcapTexture);
            shader.setUniform1i("matcapTextureUniform", currentTextureBindingPoint);
            gl.activeTexture(gl.TEXTURE0 + currentTextureBindingPoint);
            gl.bindTexture(matcapTexture.target, matcapTexture.id);
            currentTextureBindingPoint++;

            if (material.snapEnabled === true) {
                {
                    const lSnapshot = shader.uniformLocations["uSnapshot[0]"];
                    const lSnapshotDepth = shader.uniformLocations["uSnapshotDepth[0]"];

                    const lSnapshotBindingPoints = new Array(5).fill(currentTextureBindingPoint).map((a, i) => (a + i));
                    const lSnapshotDepthBindingPoints = new Array(5)
                        .fill(1 + Math.max(...lSnapshotBindingPoints))
                        .map((a, i) => (a + i));
                    // currentTextureBindingPoint = 1 + Math.max(...lSnapshotDepthBindingPoints);

                    gl.uniform1iv(lSnapshot, lSnapshotBindingPoints);
                    gl.uniform1iv(lSnapshotDepth, lSnapshotDepthBindingPoints);

                    for (let i = 0; i < 5; i++) {
                        const texture = material.uniforms[`uSnapshot`].value[i];
                        const textureDepth = material.uniforms[`uSnapshotDepth`].value[i];

                        if (!texture) {
                            break;
                        }

                        const snapTexture = this.threeRenderer.properties.get(texture).__webglTexture;
                        const snapTextureDepth = this.threeRenderer.properties.get(textureDepth).__webglTexture;

                        const bindingPoint = lSnapshotBindingPoints[i];
                        const depthBindingPoint = lSnapshotDepthBindingPoints[i];

                        // @ts-ignore
                        gl.activeTexture(gl[`TEXTURE${bindingPoint}`]);
                        gl.bindTexture(gl.TEXTURE_2D, snapTexture);

                        // @ts-ignore
                        gl.activeTexture(gl[`TEXTURE${depthBindingPoint}`]);
                        gl.bindTexture(gl.TEXTURE_2D, snapTextureDepth);
                    }
                }

                {
                    const flattenedMatrices = [].concat(...material.uniforms.uSnapView.value.map((c: any) => c.elements));
                    const lSnapView = shader.uniformLocations["uSnapView[0]"];
                    gl.uniformMatrix4fv(lSnapView, false, flattenedMatrices);
                }
                {
                    const flattenedMatrices = [].concat(...material.uniforms.uSnapProj.value.map((c: any) => c.elements));
                    const lSnapProj = shader.uniformLocations["uSnapProj[0]"];
                    gl.uniformMatrix4fv(lSnapProj, false, flattenedMatrices);
                }
                {
                    const flattenedMatrices = [].concat(...material.uniforms.uSnapProjInv.value.map((c: any) => c.elements));
                    const lSnapProjInv = shader.uniformLocations["uSnapProjInv[0]"];
                    gl.uniformMatrix4fv(lSnapProjInv, false, flattenedMatrices);
                }
                {
                    const flattenedMatrices = [].concat(...material.uniforms.uSnapViewInv.value.map((c: any) => c.elements));
                    const lSnapViewInv = shader.uniformLocations["uSnapViewInv[0]"];
                    gl.uniformMatrix4fv(lSnapViewInv, false, flattenedMatrices);
                }

            }
        }

        this.renderNodes(octree, nodes, visibilityTextureData, camera, shader, params);

        gl.activeTexture(gl.TEXTURE2);
        gl.bindTexture(gl.TEXTURE_2D, null);
        gl.activeTexture(gl.TEXTURE0);
    }

    render(scene: ThreeScene, camera: PerspectiveCamera | OrthographicCamera, target: WebGLRenderTarget | null = null, params: any = {}) {
        const gl = this.gl as WebGL2RenderingContext;

        // PREPARE
        if (target != null) {
            this.threeRenderer.setRenderTarget(target);
        }

        // camera.updateProjectionMatrix();
        // camera.matrixWorldInverse.invert(camera.matrixWorld);

        const traversalResult = this.traverse(scene);

        // RENDER
        for (const octree of traversalResult.octrees) {
            this.renderOctree(octree, octree.visibleNodes, camera, target!, params);
        }

        // CLEANUP
        gl.activeTexture(gl.TEXTURE1);
        gl.bindTexture(gl.TEXTURE_2D, null);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        gl.bindVertexArray(null);

        this.threeRenderer.resetState();
    }
}
