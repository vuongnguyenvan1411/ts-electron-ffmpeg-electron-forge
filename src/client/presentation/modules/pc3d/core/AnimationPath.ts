import {BufferAttribute, BufferGeometry, CatmullRomCurve3, Vector3} from "three";
import {Easing, remove as TweenRemove, Tween} from "@tweenjs/tween.js";

export class PathAnimation {
    path: AnimationPath;
    length: number;
    speed: any;
    callback: any;
    tween: Tween<any> | null;
    startPoint: number;
    endPoint: number;
    t: number;
    repeat: any;

    constructor(path: any, start: any, end: any, speed: any, callback: any) {
        this.path = path;
        this.length = this.path.spline.getLength();
        this.speed = speed;
        this.callback = callback;
        this.tween = null;
        this.startPoint = Math.max(start, 0);
        this.endPoint = Math.min(end, this.length);
        this.t = 0.0;
    }

    start(resume = false) {
        if (this.tween) {
            this.tween.stop();
            this.tween = null;
        }

        let tStart;
        if (resume) {
            tStart = this.t;
        } else {
            tStart = this.startPoint / this.length;
        }
        const tEnd = this.endPoint / this.length;
        const animationDuration = (tEnd - tStart) * this.length * 1000 / this.speed;

        const progress = {t: tStart};
        this.tween = new Tween(progress).to({t: tEnd}, animationDuration);
        this.tween.easing(Easing.Linear.None);
        this.tween.onUpdate(() => {
            this.t = progress.t;
            this.callback(progress.t);
        });

        this.tween.onComplete(() => {
            if (this.repeat) {
                this.start();
            }
        });

        setTimeout(() => {
            this.tween!.start();
        }, 0);
    }

    stop() {
        if (!this.tween) {
            return;
        }
        this.tween.stop();
        this.tween = null;
        this.t = 0;
    }

    pause() {
        if (!this.tween) {
            return;
        }

        this.tween.stop();
        TweenRemove(this.tween);
        this.tween = null;
    }

    resume() {
        this.start(true);
    }

    getPoint(t: any) {
        return this.path.spline.getPoint(t);
    }
}

export class AnimationPath {
    points: Vector3[];
    spline: CatmullRomCurve3;
    tween: any;

    constructor(points: Vector3[] = []) {
        this.points = points;
        this.spline = new CatmullRomCurve3(points);
        //this.spline.reparametrizeByArcLength(1 / this.spline.getLength().total);
    }

    get(t: any) {
        return this.spline.getPoint(t);
    }

    getLength() {
        return this.spline.getLength();
    }

    animate(start: any, end: any, speed: any, callback: any) {
        let animation = new PathAnimation(this, start, end, speed, callback);
        animation.start();

        return animation;
    }

    pause() {
        if (this.tween) {
            this.tween.stop();
        }
    }

    resume() {
        if (this.tween) {
            this.tween.start();
        }
    }

    getGeometry() {
        let geometry = new BufferGeometry();

        let samples = 500;
        let i = 0;

        const arr = [];

        for (let u = 0; u <= 1; u += 1 / samples) {
            let position = this.spline.getPoint(u);
            arr[i] = new Vector3(position.x, position.y, position.z);

            i++;
        }

        if (this.closed) {
            let position = this.spline.getPoint(0);
            arr[i] = new Vector3(position.x, position.y, position.z);
        }

        const arr2: any[] = [];

        if (arr.length > 0) {
            arr.forEach((value) => {
                arr2.concat(value.toArray());
            })
        }

        if (arr2.length > 0) {
            const vertices = new Float32Array(arr2);

            geometry.setAttribute('position', new BufferAttribute(vertices, 3));
        }

        return geometry;
    }

    get closed() {
        // @ts-ignore
        return this.spline.closed;
    }

    set closed(value) {
        // @ts-ignore
        this.spline.closed = value;
    }
}
