import {Box2, Box3, Line3, LinearFilter, Matrix4, NearestFilter, NoBlending, Object3D, OrthographicCamera, PerspectiveCamera, Points, Ray, RGBAFormat, Scene, Sphere, Vector2, Vector3, Vector4, WebGLRenderer, WebGLRenderTarget} from "three";
import {PointCloudTree, PointCloudTreeNode} from "./PointCloudTree";
import {PointCloudOctreeGeometry, PointCloudOctreeGeometryNode} from "./PointCloudOctreeGeometry";
import {Utils} from "./Utils";
import {PointCloudMaterial} from "../materials/PointCloudMaterial";
import {ProfileRequest} from "./ProfileRequest";
import {ClipTask, PointShape} from "./Defines";
import {App} from "./App";
import {OctreeGeometry} from "../modules/loader/2.0/OctreeGeometry";
import {Viewer} from "../viewer/viewer";
import {PointAttribute} from "../loader/PointAttributes";

export class PointCloudOctreeNode extends PointCloudTreeNode {
    children?: any[];
    sceneNode: Points;
    octree: any;
    geometryNode: PointCloudOctreeGeometryNode;
    pointcloud?: PointCloudOctree;
    debug: any;
    boundingBoxNode: any;
    pcIndex: any;

    constructor() {
        super();

        this.children = [];
        // this.sceneNode = null;
        this.octree = null;
    }

    getNumPoints() {
        return this.geometryNode?.numPoints;
    }

    isLoaded() {
        return true;
    }

    isTreeNode() {
        return true;
    }

    isGeometryNode() {
        return false;
    }

    getLevel() {
        return this.geometryNode?.level;
    }

    getBoundingSphere() {
        return this.geometryNode?.boundingSphere;
    }

    getBoundingBox() {
        return this.geometryNode?.boundingBox;
    }

    getChildren() {
        let children: any[] = [];

        for (let i = 0; i < 8; i++) {
            if (this.children && this.children[i]) {
                children.push(this.children[i]);
            }
        }

        return children;
    }

    getPointsInBox(boxNode: any) {
        if (!this.sceneNode) {
            return null;
        }

        const buffer = this.geometryNode.buffer;

        const posOffset = buffer.offset("position");
        const stride = buffer.stride;
        const view = new DataView(buffer.data);

        const worldToBox = boxNode.matrixWorld.clone().invert();
        const objectToBox = new Matrix4().multiplyMatrices(worldToBox, this.sceneNode.matrixWorld);

        const inBox: any[] = [];

        const pos = new Vector4();

        for (let i = 0; i < buffer.numElements; i++) {
            const x = view.getFloat32(i * stride + posOffset + 0, true);
            const y = view.getFloat32(i * stride + posOffset + 4, true);
            const z = view.getFloat32(i * stride + posOffset + 8, true);

            pos.set(x, y, z, 1);
            pos.applyMatrix4(objectToBox);

            if (-0.5 < pos.x && pos.x < 0.5) {
                if (-0.5 < pos.y && pos.y < 0.5) {
                    if (-0.5 < pos.z && pos.z < 0.5) {
                        pos.set(x, y, z, 1).applyMatrix4(this.sceneNode.matrixWorld);
                        inBox.push(new Vector3(pos.x, pos.y, pos.z));
                    }
                }
            }
        }

        return inBox;
    }

    get name() {
        return this.geometryNode.name;
    }
}

export class PointCloudOctree extends PointCloudTree {
    pointBudget: number;
    pcoGeometry: PointCloudOctreeGeometry | OctreeGeometry;
    boundingBox: Box3;
    boundingSphere: Sphere;
    material: PointCloudMaterial;
    visiblePointsTarget: number;
    minimumNodePixelSize: number;
    level: number;
    showBoundingBox: boolean;
    boundingBoxNodes: any[];
    loadQueue: any[];
    visibleBounds: Box3;
    visibleNodes: PointCloudOctreeNode[];
    visibleGeometry: any[];
    generateDEM: boolean;
    profileRequests: any[];
    protected _visible: boolean;
    projection: string;
    fallbackProjection: any;
    visibleNodeTextureOffsets: any;
    signedDistanceField: any;
    pickState: any;
    screenWidth: number;
    screenHeight: number;
    maxLevel: number;
    numVisibleNodes: number;
    numVisiblePoints: number;
    deepestVisibleLevel: number;
    dem: any;

    constructor(geometry: PointCloudOctreeGeometry | OctreeGeometry, material?: any) {
        super();

        this.pointBudget = Infinity;
        this.pcoGeometry = geometry;
        this.boundingBox = this.pcoGeometry.boundingBox;
        this.boundingSphere = this.boundingBox.getBoundingSphere(new Sphere());
        this.material = material || new PointCloudMaterial();
        this.visiblePointsTarget = 2 * 1000 * 1000;
        this.minimumNodePixelSize = 150;
        this.level = 0;
        this.position.copy(geometry.offset);
        this.updateMatrix();

        {
            const priorityQueue = ["rgba", "rgb", "intensity", "classification"];
            let selected = "rgba";

            for (let attributeName of priorityQueue) {
                const attribute = this.pcoGeometry.pointAttributes.attributes.find((a: any) => a.name === attributeName);

                if (!attribute) {
                    continue;
                }

                const min = attribute.range[0].constructor.name === "Array" ? attribute.range[0] : [attribute.range[0]];
                const max = attribute.range[1].constructor.name === "Array" ? attribute.range[1] : [attribute.range[1]];

                const range_min = new Vector3(...min);
                const range_max = new Vector3(...max);
                const range = range_min.distanceTo(range_max);

                if (range === 0) {
                    continue;
                }

                selected = attributeName;

                break;
            }

            this.material.activeAttributeName = selected;
        }

        this.showBoundingBox = false;
        this.boundingBoxNodes = [];
        this.loadQueue = [];
        this.visibleBounds = new Box3();
        this.visibleNodes = [];
        this.visibleGeometry = [];
        this.generateDEM = false;
        this.profileRequests = [];
        this.name = '';
        this._visible = true;

        {
            let box = [this.pcoGeometry.tightBoundingBox, this.getBoundingBoxWorld()]
                .find(v => v !== undefined);

            this.updateMatrixWorld(true);
            box = Utils.computeTransformedBoundingBox(box, this.matrixWorld);

            const bMin = box.min.z;
            const bMax = box.max.z;
            this.material.heightMin = bMin;
            this.material.heightMax = bMax;
        }

        // TODO read projection from file instead
        this.projection = geometry.projection;
        this.fallbackProjection = geometry.fallbackProjection;

        this.root = this.pcoGeometry.root;
    }

    getId() {
        return this.uuid;
    }

    setName(name: any) {
        if (this.name !== name) {
            this.name = name;
            this.dispatchEvent({type: 'name_changed', name: name, pointcloud: this});
        }
    }

    getName() {
        return this.name;
    }

    getAttribute(name: string) {
        const attribute = this.pcoGeometry.pointAttributes.attributes.find((a: PointAttribute) => a.name === name);

        return attribute ? attribute : null;
    }

    getAttributes() {
        return this.pcoGeometry.pointAttributes;
    }

    toTreeNode(geometryNode: PointCloudOctreeGeometryNode, parent: PointCloudOctreeNode) {
        const node = new PointCloudOctreeNode();
        const sceneNode = new Points(geometryNode.geometry!, this.material);

        sceneNode.name = geometryNode.name;
        sceneNode.position.copy(geometryNode.boundingBox.min);
        sceneNode.frustumCulled = false;
        sceneNode.onBeforeRender = (_this, _, __, ___, material: any) => {
            if (material.program) {
                _this.getContext().useProgram(material.program.program);

                if (material.program.getUniforms().map.level) {
                    const level = geometryNode.getLevel();
                    material.uniforms.level.value = level;
                    material.program.getUniforms().map.level.setValue(_this.getContext(), level);
                }

                if (this.visibleNodeTextureOffsets && material.program.getUniforms().map.vnStart) {
                    const vnStart = this.visibleNodeTextureOffsets.get(node);
                    material.uniforms.vnStart.value = vnStart;
                    material.program.getUniforms().map.vnStart.setValue(_this.getContext(), vnStart);
                }

                if (material.program.getUniforms().map.pcIndex) {
                    const i = node.pcIndex ? node.pcIndex : this.visibleNodes.indexOf(node);
                    material.uniforms.pcIndex.value = i;
                    material.program.getUniforms().map.pcIndex.setValue(_this.getContext(), i);
                }
            }
        };

        // { // DEBUG
        //	let sg = new SphereGeometry(1, 16, 16);
        //	let sm = new MeshNormalMaterial();
        //	let s = new Mesh(sg, sm);
        //	s.scale.set(5, 5, 5);
        //	s.position.copy(geometryNode.mean)
        //		.add(this.position)
        //		.add(geometryNode.boundingBox.min);
        //
        //	viewer.scene.scene.add(s);
        // }

        node.geometryNode = geometryNode;
        node.sceneNode = sceneNode;
        node.pointcloud = this;
        node.children = [];
        //for (let key in geometryNode.children) {
        //	node.children[key] = geometryNode.children[key];
        //}
        for (let i = 0; i < 8; i++) {
            node.children[i] = geometryNode.children[i];
        }

        if (!parent) {
            this.root = node;
            this.add(sceneNode);
        } else {
            const childIndex = parseInt(geometryNode.name[geometryNode.name.length - 1]);
            parent.sceneNode.add(sceneNode);

            if (parent.children) {
                parent.children[childIndex] = node;
            }
        }

        const disposeListener = function () {
            const childIndex = parseInt(geometryNode.name[geometryNode.name.length - 1]);
            parent.sceneNode.remove(node.sceneNode);

            if (parent.children) {
                parent.children[childIndex] = geometryNode;
            }
        }

        geometryNode.oneTimeDisposeHandlers.push(disposeListener);

        return node;
    }

    updateVisibleBounds() {
        const leafNodes: any[] = [];

        for (let i = 0; i < this.visibleNodes.length; i++) {
            const node = this.visibleNodes[i];
            let isLeaf = true;

            if (node.children) {
                for (let j = 0; j < node.children.length; j++) {
                    const child = node.children[j];

                    if (child instanceof PointCloudOctreeNode) {
                        isLeaf = isLeaf && !child.sceneNode.visible;
                    } else if (child instanceof PointCloudOctreeGeometryNode) {
                        isLeaf = true;
                    }
                }
            }

            if (isLeaf) {
                leafNodes.push(node);
            }
        }

        this.visibleBounds.min = new Vector3(Infinity, Infinity, Infinity);
        this.visibleBounds.max = new Vector3(-Infinity, -Infinity, -Infinity);

        for (let i = 0; i < leafNodes.length; i++) {
            const node = leafNodes[i];

            this.visibleBounds.expandByPoint(node.getBoundingBox().min);
            this.visibleBounds.expandByPoint(node.getBoundingBox().max);
        }
    }

    updateMaterial(material: PointCloudMaterial, camera: PerspectiveCamera | OrthographicCamera, renderer: WebGLRenderer) {
        if ('fov' in camera) {
            material.fov = camera.fov * (Math.PI / 180);
        }
        material.screenWidth = renderer.domElement.clientWidth;
        material.screenHeight = renderer.domElement.clientHeight;
        material.spacing = this.pcoGeometry.spacing; // * Math.max(this.scale.x, this.scale.y, this.scale.z);
        material.near = camera.near;
        material.far = camera.far;
        material.uniforms.octreeSize.value = this.pcoGeometry.boundingBox.getSize(new Vector3()).x;
    }

    computeVisibilityTextureData(nodes: PointCloudOctreeNode[]) {
        if (App.measureTimings) performance.mark("computeVisibilityTextureData-start");

        const data = new Uint8Array(nodes.length * 4);
        const visibleNodeTextureOffsets = new Map();

        // copy array
        nodes = nodes.slice();

        // sort by level and index, e.g. r, r0, r3, r4, r01, r07, r30, ...
        const sort = function (a: PointCloudOctreeNode, b: PointCloudOctreeNode) {
            const na = a.geometryNode.name;
            const nb = b.geometryNode.name;

            if (na.length !== nb.length) return na.length - nb.length;
            if (na < nb) return -1;
            if (na > nb) return 1;

            return 0;
        };
        nodes.sort(sort);

        // let worldDir = new Vector3();

        const nodeMap = new Map();
        const offsetsToChild = new Array(nodes.length).fill(Infinity);

        for (let i = 0; i < nodes.length; i++) {
            const node = nodes[i];

            nodeMap.set(node.name, node);
            visibleNodeTextureOffsets.set(node, i);

            if (i > 0) {
                const index = parseInt(node.name.slice(-1));
                const parentName = node.name.slice(0, -1);
                const parent = nodeMap.get(parentName);
                const parentOffset = visibleNodeTextureOffsets.get(parent);

                const parentOffsetToChild = (i - parentOffset);

                offsetsToChild[parentOffset] = Math.min(offsetsToChild[parentOffset], parentOffsetToChild);

                data[parentOffset * 4] = data[parentOffset * 4] | (1 << index);
                data[parentOffset * 4 + 1] = (offsetsToChild[parentOffset] >> 8);
                data[parentOffset * 4 + 2] = (offsetsToChild[parentOffset] % 256);
            }

            const density = node.geometryNode.density;

            if (typeof density === "number" && !Number.isNaN(density)) {
                const lodOffset = Math.log2(density) / 2 - 1.5;
                // const offsetUint8 = (lodOffset + 10) * 10;

                data[i * 4 + 3] = (lodOffset + 10) * 10;
            } else {
                data[i * 4 + 3] = 100;
            }
        }

        if (App.measureTimings) {
            performance.mark("computeVisibilityTextureData-end");
            performance.measure("render.computeVisibilityTextureData", "computeVisibilityTextureData-start", "computeVisibilityTextureData-end");
        }

        return {
            data: data,
            offsets: visibleNodeTextureOffsets
        };
    }

    nodeIntersectsProfile(node: any, profile: any) {
        const bbWorld = node.boundingBox.clone().applyMatrix4(this.matrixWorld);
        const bsWorld = bbWorld.getBoundingSphere(new Sphere());

        let intersects = false;

        for (let i = 0; i < profile.points.length - 1; i++) {
            const start = new Vector3(profile.points[i].x, profile.points[i].y, bsWorld.center.z);
            const end = new Vector3(profile.points[i + 1].x, profile.points[i + 1].y, bsWorld.center.z);

            const closest = new Line3(start, end).closestPointToPoint(bsWorld.center, true, new Vector3());
            const distance = closest.distanceTo(bsWorld.center);

            intersects = intersects || (distance < (bsWorld.radius + profile.width));
        }

        return intersects;
    }

    deepestNodeAt(position: Vector3) {
        const toObjectSpace = this.matrixWorld.clone().invert();
        const objPos = position.clone().applyMatrix4(toObjectSpace);

        let current = this.root!;

        while (true) {
            let containingChild;

            for (const child of current.children!) {
                if (child !== undefined) {
                    if (child.getBoundingBox().containsPoint(objPos)) {
                        containingChild = child;
                    }
                }
            }

            if (containingChild && containingChild instanceof PointCloudOctreeNode) {
                current = containingChild;
            } else {
                break;
            }
        }

        return current;
    }

    nodesOnRay(nodes: PointCloudOctreeNode[], ray: Ray) {
        const nodesOnRay: any[] = [];
        const _ray = ray.clone();

        for (let i = 0; i < nodes.length; i++) {
            const node = nodes[i];
            const sphere = node.getBoundingSphere().clone().applyMatrix4(this.matrixWorld);

            if (_ray.intersectsSphere(sphere)) {
                nodesOnRay.push(node);
            }
        }

        return nodesOnRay;
    }

    updateMatrixWorld(force?: boolean) {
        if (this.matrixAutoUpdate) this.updateMatrix();

        if (this.matrixWorldNeedsUpdate || force === true) {
            if (!this.parent) {
                this.matrixWorld.copy(this.matrix);
            } else {
                this.matrixWorld.multiplyMatrices(this.parent.matrixWorld, this.matrix);
            }

            this.matrixWorldNeedsUpdate = false;

            // force = true;
        }
    }

    hideDescendants(object: any) {
        const stack: any[] = [];

        for (let i = 0; i < object.children.length; i++) {
            const child = object.children[i];

            if (child.visible) {
                stack.push(child);
            }
        }

        while (stack.length > 0) {
            const object: any = stack.shift();

            object.visible = false;

            for (let i = 0; i < object.children.length; i++) {
                const child = object.children[i];

                if (child.visible) {
                    stack.push(child);
                }
            }
        }
    }

    moveToOrigin() {
        this.position.set(0, 0, 0);
        this.updateMatrixWorld(true);
        const box = this.boundingBox;
        const transform = this.matrixWorld;
        const tBox = Utils.computeTransformedBoundingBox(box, transform);
        this.position.set(0, 0, 0).sub(tBox.getCenter(new Vector3()));
    };

    moveToGroundPlane() {
        this.updateMatrixWorld(true);
        const box = this.boundingBox;
        const transform = this.matrixWorld;
        const tBox = Utils.computeTransformedBoundingBox(box, transform);
        this.position.y += -tBox.min.y;
    };

    getBoundingBoxWorld() {
        this.updateMatrixWorld(true);
        const box = this.boundingBox;
        const transform = this.matrixWorld;

        return Utils.computeTransformedBoundingBox(box, transform);
    };

    /**
     * returns points inside the profile points
     *
     * maxDepth:        search points up to the given octree depth
     *
     *
     * The return value is an array with all segments of the profile path
     *    let segment = {
     *		start:	Vector3,
     *		end:	Vector3,
     *		points: {}
     *		project: function()
     *	};
     *
     * The project() function inside each segment can be used to transform
     * that segments point coordinates to line up along the x-axis.
     *
     *
     */
    getPointsInProfile(profile: any, maxDepth: any, callback: any) {
        if (callback) {
            let request = new ProfileRequest(this, profile, maxDepth, callback);
            this.profileRequests.push(request);

            return request;
        }

        const points: any = {
            segments: [],
            boundingBox: new Box3(),
            projectedBoundingBox: new Box2()
        };

        // evaluate segments
        for (let i = 0; i < profile.points.length - 1; i++) {
            const start = profile.points[i];
            const end = profile.points[i + 1];
            const ps = this.getProfile(start, end, profile.width, maxDepth);

            const segment: any = {
                start: start,
                end: end,
                points: ps,
                project: null
            };

            points.segments.push(segment);

            // @ts-ignore
            points.boundingBox.expandByPoint(ps.boundingBox.min);
            // @ts-ignore
            points.boundingBox.expandByPoint(ps.boundingBox.max);
        }

        // add projection functions to the segments
        const mileage = new Vector3();
        for (let i = 0; i < points.segments.length; i++) {
            const segment = points.segments[i];
            const start = segment.start;
            const end = segment.end;

            segment.project = (function (_start, _end, _mileage, _boundingBox) {
                const start = _start;
                const end = _end;
                const mileage = _mileage;
                const boundingBox = _boundingBox;

                const xAxis = new Vector3(1, 0, 0);
                const dir = new Vector3().subVectors(end, start);
                dir.y = 0;
                dir.normalize();
                let alpha = Math.acos(xAxis.dot(dir));
                if (dir.z > 0) {
                    alpha = -alpha;
                }

                return function (position: any) {
                    const toOrigin = new Matrix4().makeTranslation(-start.x, -boundingBox.min.y, -start.z);
                    const alignWithX = new Matrix4().makeRotationY(-alpha);
                    const applyMileage = new Matrix4().makeTranslation(mileage.x, 0, 0);

                    const pos = position.clone();
                    pos.applyMatrix4(toOrigin);
                    pos.applyMatrix4(alignWithX);
                    pos.applyMatrix4(applyMileage);

                    return pos;
                };
            }(start, end, mileage.clone(), points.boundingBox.clone()));

            mileage.x += new Vector3(start.x, 0, start.z).distanceTo(new Vector3(end.x, 0, end.z));
            mileage.y += end.y - start.y;
        }

        points.projectedBoundingBox.min.x = 0;
        points.projectedBoundingBox.min.y = points.boundingBox.min.y;
        points.projectedBoundingBox.max.x = mileage.x;
        points.projectedBoundingBox.max.y = points.boundingBox.max.y;

        return points;
    }

    /**
     * returns points inside the given profile bounds.
     *
     * start:
     * end:
     * width:
     * depth:        search points up to the given octree depth
     * callback:    if specified, points are loaded before searching
     *
     *
     */
    getProfile(start: any, end: any, width: any, depth: any, callback?: any) {
        // @ts-ignore
        const request = new ProfileRequest(start, end, width, depth, callback);
        this.profileRequests.push(request);
    };

    getVisibleExtent() {
        return this.visibleBounds.applyMatrix4(this.matrixWorld);
    };

    intersectsPoint(position: any) {
        const rootAvailable = this.pcoGeometry.root && this.pcoGeometry.root.geometry;

        if (!rootAvailable) {
            return false;
        }

        if (typeof this.signedDistanceField === "undefined") {
            const resolution = 32;
            const field = new Float32Array(resolution ** 3).fill(Infinity);

            const positions = this.pcoGeometry.root.geometry.attributes.position;
            const boundingBox = this.boundingBox;

            const n = positions.count;

            for (let i = 0; i < n; i = i + 3) {
                const x = positions.array[3 * i];
                const y = positions.array[3 * i + 1];
                const z = positions.array[3 * i + 2];

                // @ts-ignore
                const ix = parseInt(Math.min(resolution * (x / boundingBox.max.x), resolution - 1));
                // @ts-ignore
                const iy = parseInt(Math.min(resolution * (y / boundingBox.max.y), resolution - 1));
                // @ts-ignore
                const iz = parseInt(Math.min(resolution * (z / boundingBox.max.z), resolution - 1));

                const index = ix + iy * resolution + iz * resolution * resolution;

                field[index] = 0;
            }

            this.signedDistanceField = {
                resolution: resolution,
                field: field,
            };
        }

        {
            const sdf = this.signedDistanceField;
            const boundingBox = this.boundingBox;

            const toObjectSpace = this.matrixWorld.clone().invert();

            const objPos = position.clone().applyMatrix4(toObjectSpace);

            const resolution = sdf.resolution;
            // @ts-ignore
            const ix = parseInt(resolution * (objPos.x / boundingBox.max.x));
            // @ts-ignore
            const iy = parseInt(resolution * (objPos.y / boundingBox.max.y));
            // @ts-ignore
            const iz = parseInt(resolution * (objPos.z / boundingBox.max.z));

            if (ix < 0 || iy < 0 || iz < 0) {
                return false;
            }
            if (ix >= resolution || iy >= resolution || iz >= resolution) {
                return false;
            }

            const index = ix + iy * resolution + iz * resolution * resolution;

            const value = sdf.field[index];

            if (value === 0) {
                return true;
            }
        }

        return false;
    }

    /**
     *
     *
     *
     * params.pickWindowSize:    Look for points inside a pixel window of this size.
     *                            Use odd values: 1, 3, 5, ...
     *
     *
     * TODO: only draw pixels that are actually read with readPixels().
     *
     */
    pick(viewer: Viewer, camera: PerspectiveCamera | OrthographicCamera, ray: Ray, params: any = {}) {
        const renderer = viewer.renderer;
        const pRenderer = viewer.pRenderer;

        performance.mark("pick-start");

        const getVal = (a: any, b: any) => a !== undefined ? a : b;

        const pickWindowSize = getVal(params.pickWindowSize, 65);
        // let pickOutsideClipRegion = getVal(params.pickOutsideClipRegion, false);

        const size = renderer.getSize(new Vector2());

        const width = Math.ceil(getVal(params.width, size.width));
        const height = Math.ceil(getVal(params.height, size.height));

        const pointSizeType = getVal(params.pointSizeType, this.material.pointSizeType);
        const pointSize = getVal(params.pointSize, this.material.size);

        const nodes = this.nodesOnRay(this.visibleNodes, ray);

        if (nodes.length === 0) {
            return null;
        }

        if (!this.pickState) {
            const scene = new Scene();

            const material = new PointCloudMaterial();
            material.activeAttributeName = "indices";

            const renderTarget = new WebGLRenderTarget(
                1, 1,
                {
                    minFilter: LinearFilter,
                    magFilter: NearestFilter,
                    format: RGBAFormat
                }
            );

            this.pickState = {
                renderTarget: renderTarget,
                material: material,
                scene: scene
            };
        }

        const pickState = this.pickState;
        const pickMaterial = pickState.material;

        // update pick material
        pickMaterial.pointSizeType = pointSizeType;
        //pickMaterial.shape = this.material.shape;
        pickMaterial.shape = PointShape.PARABOLOID;

        pickMaterial.uniforms.uFilterReturnNumberRange.value = this.material.uniforms.uFilterReturnNumberRange.value;
        pickMaterial.uniforms.uFilterNumberOfReturnsRange.value = this.material.uniforms.uFilterNumberOfReturnsRange.value;
        pickMaterial.uniforms.uFilterGPSTimeClipRange.value = this.material.uniforms.uFilterGPSTimeClipRange.value;
        pickMaterial.uniforms.uFilterPointSourceIDClipRange.value = this.material.uniforms.uFilterPointSourceIDClipRange.value;

        pickMaterial.activeAttributeName = "indices";

        pickMaterial.size = pointSize;
        pickMaterial.uniforms.minSize.value = this.material.uniforms.minSize.value;
        pickMaterial.uniforms.maxSize.value = this.material.uniforms.maxSize.value;
        pickMaterial.classification = this.material.classification;
        pickMaterial.recomputeClassification();

        if (params.pickClipped) {
            pickMaterial.clipBoxes = this.material.clipBoxes;
            pickMaterial.uniforms.clipBoxes = this.material.uniforms.clipBoxes;
            if (this.material.clipTask === ClipTask.HIGHLIGHT) {
                pickMaterial.clipTask = ClipTask.NONE;
            } else {
                pickMaterial.clipTask = this.material.clipTask;
            }
            pickMaterial.clipMethod = this.material.clipMethod;
        } else {
            pickMaterial.clipBoxes = [];
        }

        this.updateMaterial(pickMaterial, camera, renderer);

        pickState.renderTarget.setSize(width, height);

        const pixelPos = new Vector2(params.x, params.y);

        const gl = renderer.getContext();
        gl.enable(gl.SCISSOR_TEST);
        gl.scissor(
            pixelPos.x - (pickWindowSize - 1) / 2,
            pixelPos.y - (pickWindowSize - 1) / 2,
            parseInt(pickWindowSize),
            parseInt(pickWindowSize)
        );

        renderer.state.buffers.depth.setTest(pickMaterial.depthTest);
        renderer.state.buffers.depth.setMask(pickMaterial.depthWrite);
        renderer.state.setBlending(NoBlending);

        { // RENDER
            renderer.setRenderTarget(pickState.renderTarget);
            gl.clearColor(0, 0, 0, 0);
            renderer.clear(true, true, true);

            const tmp = this.material;
            this.material = pickMaterial;

            pRenderer.renderOctree(this, nodes, camera, pickState.renderTarget);

            this.material = tmp;
        }

        const clamp = (number: number, min: number, max: number) => Math.min(Math.max(min, number), max);

        const x = clamp(pixelPos.x - (pickWindowSize - 1) / 2, 0, width);
        const y = clamp(pixelPos.y - (pickWindowSize - 1) / 2, 0, height);
        const w = Math.min(x + pickWindowSize, width) - x;
        const h = Math.min(y + pickWindowSize, height) - y;

        const pixelCount = w * h;
        const buffer = new Uint8Array(4 * pixelCount);

        gl.readPixels(x, y, pickWindowSize, pickWindowSize, gl.RGBA, gl.UNSIGNED_BYTE, buffer);

        renderer.setRenderTarget(null);
        renderer.state.reset();
        renderer.setScissorTest(false);
        gl.disable(gl.SCISSOR_TEST);

        const pixels = buffer;
        const iBuffer = new Uint32Array(buffer.buffer);

        // find closest hit inside pixelWindow boundaries
        // let min = Number.MAX_VALUE;
        const hits: any[] = [];

        for (let u = 0; u < pickWindowSize; u++) {
            for (let v = 0; v < pickWindowSize; v++) {
                const offset = (u + v * pickWindowSize);
                const distance = Math.pow(u - (pickWindowSize - 1) / 2, 2) + Math.pow(v - (pickWindowSize - 1) / 2, 2);

                const pcIndex = pixels[4 * offset + 3];
                pixels[4 * offset + 3] = 0;
                const pIndex = iBuffer[offset];

                if (!(pcIndex === 0 && pIndex === 0) && (pcIndex !== undefined) && (pIndex !== undefined)) {
                    const hit = {
                        pIndex: pIndex,
                        pcIndex: pcIndex,
                        distanceToCenter: distance
                    };

                    if (params.all) {
                        hits.push(hit);
                    } else {
                        if (hits.length > 0) {
                            if (distance < hits[0].distanceToCenter) {
                                hits[0] = hit;
                            }
                        } else {
                            hits.push(hit);
                        }
                    }
                }
            }
        }


        // { // DEBUG: show panel with pick image
        // 	let img = Utils.pixelsArrayToImage(buffer, w, h);
        // 	let screenshot = img.src;

        // 	if(!this.debugDIV){
        // 		this.debugDIV = $(`
        // 			<div id="pickDebug"
        // 			style="position: absolute;
        // 			right: 400px; width: 300px;
        // 			bottom: 44px; width: 300px;
        // 			z-index: 1000;
        // 			"></div>`);
        // 		$(document.body).append(this.debugDIV);
        // 	}

        // 	this.debugDIV.empty();
        // 	this.debugDIV.append($(`<img src="${screenshot}"
        // 		style="transform: scaleY(-1); width: 300px"/>`));
        // 	//$(this.debugWindow.document).append($(`<img src="${screenshot}"/>`));
        // 	//this.debugWindow.document.write('<img src="'+screenshot+'"/>');
        // }


        for (let hit of hits) {
            const point: any = {};

            if (!nodes[hit.pcIndex]) {
                return null;
            }

            const node = nodes[hit.pcIndex];
            const pc = node.sceneNode;
            const geometry = node.geometryNode.geometry!;

            for (let attributeName in geometry.attributes) {
                if (!geometry.attributes.hasOwnProperty(attributeName)) {
                    continue;
                }

                const attribute = geometry.attributes[attributeName] as any;

                if (attributeName === 'position') {
                    const x = attribute.array[3 * hit.pIndex];
                    const y = attribute.array[3 * hit.pIndex + 1];
                    const z = attribute.array[3 * hit.pIndex + 2];

                    const position = new Vector3(x, y, z);
                    position.applyMatrix4(pc.matrixWorld);

                    point[attributeName] = position;
                } else if (attributeName === 'indices') {
                    //
                } else {
                    let values = attribute.array.slice(attribute.itemSize * hit.pIndex, attribute.itemSize * (hit.pIndex + 1));

                    if (attribute.potree) {
                        const {scale, offset} = attribute.potree;
                        values = values.map((v: any) => v / scale + offset);
                    }

                    point[attributeName] = values;

                    //debugger;
                    //if (values.itemSize === 1) {
                    //	point[attribute.name] = values.array[hit.pIndex];
                    //} else {
                    //	let value = [];
                    //	for (let j = 0; j < values.itemSize; j++) {
                    //		value.push(values.array[values.itemSize * hit.pIndex + j]);
                    //	}
                    //	point[attribute.name] = value;
                    //}
                }
            }

            hit.point = point;
        }

        performance.mark("pick-end");
        performance.measure("pick", "pick-start", "pick-end");

        if (params.all) {
            return hits.map(hit => hit.point);
        } else {
            if (hits.length === 0) {
                return null;
            } else {
                return hits[0].point;
            }
        }
    }

    * getFittedBoxGen(boxNode: Object3D) {
        const start = performance.now();

        const shrinkedLocalBounds = new Box3();
        const worldToBox = boxNode.matrixWorld.clone().invert();

        for (let node of this.visibleNodes) {
            if (!node.sceneNode) {
                continue;
            }

            const buffer = node.geometryNode.buffer;
            const posOffset = buffer.offset("position");
            const stride = buffer.stride;
            const view = new DataView(buffer.data);
            const objectToBox = new Matrix4().multiplyMatrices(worldToBox, node.sceneNode.matrixWorld);
            const pos = new Vector3();

            for (let i = 0; i < buffer.numElements; i++) {
                const x = view.getFloat32(i * stride + posOffset + 0, true);
                const y = view.getFloat32(i * stride + posOffset + 4, true);
                const z = view.getFloat32(i * stride + posOffset + 8, true);

                pos.set(x, y, z);
                pos.applyMatrix4(objectToBox);

                if (-0.5 < pos.x && pos.x < 0.5) {
                    if (-0.5 < pos.y && pos.y < 0.5) {
                        if (-0.5 < pos.z && pos.z < 0.5) {
                            shrinkedLocalBounds.expandByPoint(pos);
                        }
                    }
                }
            }

            yield;
        }

        const fittedPosition = shrinkedLocalBounds.getCenter(new Vector3()).applyMatrix4(boxNode.matrixWorld);

        const fitted = new Object3D();
        fitted.position.copy(fittedPosition);
        fitted.scale.copy(boxNode.scale);
        fitted.rotation.copy(boxNode.rotation);

        const ds = new Vector3().subVectors(shrinkedLocalBounds.max, shrinkedLocalBounds.min);
        fitted.scale.multiply(ds);

        const duration = performance.now() - start;
        console.log("duration: ", duration);

        yield fitted;
    }

    getFittedBox(boxNode: Object3D, maxLevel: number) {
        maxLevel = Infinity;

        const start = performance.now();

        const shrinkedLocalBounds = new Box3();
        const worldToBox = boxNode.matrixWorld.clone().invert();

        for (let node of this.visibleNodes) {
            if (!node.sceneNode || node.getLevel() > maxLevel) {
                continue;
            }

            const buffer = node.geometryNode.buffer;
            const posOffset = buffer.offset("position");
            const stride = buffer.stride;
            const view = new DataView(buffer.data);
            const objectToBox = new Matrix4().multiplyMatrices(worldToBox, node.sceneNode.matrixWorld);
            const pos = new Vector3();

            for (let i = 0; i < buffer.numElements; i++) {
                const x = view.getFloat32(i * stride + posOffset + 0, true);
                const y = view.getFloat32(i * stride + posOffset + 4, true);
                const z = view.getFloat32(i * stride + posOffset + 8, true);

                pos.set(x, y, z);
                pos.applyMatrix4(objectToBox);

                if (-0.5 < pos.x && pos.x < 0.5) {
                    if (-0.5 < pos.y && pos.y < 0.5) {
                        if (-0.5 < pos.z && pos.z < 0.5) {
                            shrinkedLocalBounds.expandByPoint(pos);
                        }
                    }
                }
            }
        }

        const fittedPosition = shrinkedLocalBounds.getCenter(new Vector3()).applyMatrix4(boxNode.matrixWorld);

        const fitted = new Object3D();
        fitted.position.copy(fittedPosition);
        fitted.scale.copy(boxNode.scale);
        fitted.rotation.copy(boxNode.rotation);

        const ds = new Vector3().subVectors(shrinkedLocalBounds.max, shrinkedLocalBounds.min);
        fitted.scale.multiply(ds);

        const duration = performance.now() - start;

        console.log("duration: ", duration);

        return fitted;
    }

    get progress() {
        return this.visibleNodes.length / this.visibleGeometry.length;
    }

    find(name: string) {
        let node: any = null;

        for (let char of name) {
            if (char === "r") {
                node = this.root;
            } else {
                node = node.children[char];
            }
        }

        return node;
    }

    // @ts-ignore
    get visible() {
        return this._visible;
    }

    set visible(value: boolean) {
        if (value !== this._visible) {
            this._visible = value;

            this.dispatchEvent({type: 'visibility_changed', pointcloud: this});
        }
    }
}
