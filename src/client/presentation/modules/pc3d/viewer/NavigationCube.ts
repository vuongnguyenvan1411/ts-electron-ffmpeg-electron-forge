import {Viewer} from "./viewer";
import {App} from "../core/App";
import {DoubleSide, Euler, Mesh, MeshBasicMaterial, Object3D, OrthographicCamera, PlaneGeometry, Raycaster, TextureLoader, Vector2, Vector3} from "three";
import {ViewType} from "../core/Defines";

export class NavigationCube extends Object3D {
    viewer: Viewer;
    front: Mesh;
    back: Mesh;
    left: Mesh;
    right: Mesh;
    bottom: Mesh;
    top: Mesh;
    width: number;
    height: number;
    camera: OrthographicCamera;
    pickedFace: string | null;

    constructor(viewer: Viewer) {
        super();

        this.viewer = viewer;

        const createPlaneMaterial = (img: string) => {
            const material = new MeshBasicMaterial({
                depthTest: true,
                depthWrite: true,
                side: DoubleSide
            });

            new TextureLoader().load(App.getAsset(`resources/textures/navigation/${img}`), (texture) => {
                    texture.anisotropy = viewer.renderer.capabilities.getMaxAnisotropy();
                    material.map = texture;
                    material.needsUpdate = true;
                }
            );

            return material;
        };

        const planeGeometry = new PlaneGeometry(1, 1);

        this.front = new Mesh(planeGeometry, createPlaneMaterial(`${ViewType.Front}.png`));
        this.front.position.y = -0.5;
        this.front.rotation.x = Math.PI / 2.0;
        this.front.updateMatrixWorld();
        this.front.name = ViewType.Front;
        this.add(this.front);

        this.back = new Mesh(planeGeometry, createPlaneMaterial(`${ViewType.Back}.png`));
        this.back.position.y = 0.5;
        this.back.rotation.x = Math.PI / 2.0;
        this.back.updateMatrixWorld();
        this.back.name = ViewType.Back;
        this.add(this.back);

        this.left = new Mesh(planeGeometry, createPlaneMaterial(`${ViewType.Left}.png`));
        this.left.position.x = -0.5;
        this.left.rotation.y = Math.PI / 2.0;
        this.left.updateMatrixWorld();
        this.left.name = ViewType.Left;
        this.add(this.left);

        this.right = new Mesh(planeGeometry, createPlaneMaterial(`${ViewType.Right}.png`));
        this.right.position.x = 0.5;
        this.right.rotation.y = Math.PI / 2.0;
        this.right.updateMatrixWorld();
        this.right.name = ViewType.Right;
        this.add(this.right);

        this.bottom = new Mesh(planeGeometry, createPlaneMaterial(`${ViewType.Bottom}.png`));
        this.bottom.position.z = -0.5;
        this.bottom.updateMatrixWorld();
        this.bottom.name = ViewType.Bottom;
        this.add(this.bottom);

        this.top = new Mesh(planeGeometry, createPlaneMaterial(`${ViewType.Top}.png`));
        this.top.position.z = 0.5;
        this.top.updateMatrixWorld();
        this.top.name = ViewType.Top;
        this.add(this.top);

        this.width = 80; // in px

        // @ts-ignore
        this.camera = new OrthographicCamera(-1, 1, 1, -1, -1, 1);
        this.camera.position.copy(new Vector3(0, 0, 0));
        this.camera.lookAt(new Vector3(0, 1, 0));
        this.camera.updateMatrixWorld();
        this.camera.rotation.order = "ZXY";

        const onMouseDown = (event: MouseEvent) => {
            if (!this.visible) {
                return;
            }

            this.pickedFace = null;
            const mouse = new Vector2();
            mouse.x = event.clientX - (window.innerWidth - this.width);
            mouse.y = event.clientY;

            if (mouse.x < 0 || mouse.y > this.width) return;

            mouse.x = (mouse.x / this.width) * 2 - 1;
            mouse.y = -(mouse.y / this.width) * 2 + 1;

            const raycaster = new Raycaster();
            raycaster.setFromCamera(mouse, this.camera);
            raycaster.ray.origin.sub(this.camera.getWorldDirection(new Vector3()));

            const intersects = raycaster.intersectObjects(this.children);

            let minDistance = 1000;
            for (let i = 0; i < intersects.length; i++) {
                if (intersects[i].distance < minDistance) {
                    this.pickedFace = intersects[i].object.name;
                    minDistance = intersects[i].distance;
                }
            }

            if (this.pickedFace) {
                this.viewer.setView(this.pickedFace);
            }
        }

        this.viewer.renderer.domElement.addEventListener('mousedown', onMouseDown, false);
    }

    update(rotation: Euler) {
        if (this.visible) {
            this.camera.rotation.copy(rotation);
            this.camera.updateMatrixWorld();
        }
    }
}
