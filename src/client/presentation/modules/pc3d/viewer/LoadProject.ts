import {Annotation} from "../core/Annotation";
import {Measure} from "../utils/Measure";
import {CameraAnimation} from "../modules/CameraAnimation/CameraAnimation";
import {OrientedImageLoader} from "../modules/OrientedImages/OrientedImages";
import {GeoPackageLoader} from "../loader/GeoPackageLoader";
import {Utils} from "../core/Utils";
import {PointSizeType} from "../core/Defines";
import {Viewer} from "./viewer";
import proj4 from "proj4";
import {register} from 'ol/proj/proj4';
import {Profile} from "../utils/Profile";
import {BoxVolume, SphereVolume} from "../utils/Volume";
import {App} from "../core/App";
import {Vector3} from "three";

register(proj4);

function loadPointCloud(viewer: Viewer, data: any) {
    const loadMaterial = (target: any) => {
        if (data.material) {
            if (data.material.activeAttributeName != null) {
                target.activeAttributeName = data.material.activeAttributeName;
            }

            if (data.material.ranges != null) {
                for (let range of data.material.ranges) {

                    if (range.name === "elevationRange") {
                        target.elevationRange = range.value;
                    } else if (range.name === "intensityRange") {
                        target.intensityRange = range.value;
                    } else {
                        target.setRange(range.name, range.value);
                    }

                }
            }

            if (data.material.size != null) {
                target.size = data.material.size;
            }

            if (data.material.minSize != null) {
                target.minSize = data.material.minSize;
            }

            if (data.material.pointSizeType != null) {
                // @ts-ignore
                target.pointSizeType = PointSizeType[data.material.pointSizeType];
            }

            if (data.material.matcap != null) {
                target.matcap = data.material.matcap;
            }

        } else if (data.activeAttributeName != null) {
            target.activeAttributeName = data.activeAttributeName;
        } else {
            // no material data
        }

    };

    return new Promise((resolve: any) => {
        const names = viewer.scene.pointClouds.map(p => p.name);
        const alreadyExists = names.includes(data.name);

        if (alreadyExists) {
            resolve();

            return;
        }

        App.loadPointCloud(
            {
                url: data.url,
                name: data.name
            }
        ).then((evt) => {
            if (evt) {
                const pointCloud = evt.pointcloud;

                // @ts-ignore
                pointCloud.position.set(...data.position);
                // @ts-ignore
                pointCloud.rotation.set(...data.rotation);
                // @ts-ignore
                pointCloud.scale.set(...data.scale);

                loadMaterial(pointCloud.material);

                viewer.scene.addPointCloud(pointCloud);

                resolve(pointCloud);
            }
        })

        // App.loadPointCloud(data.url, data.name, (e: any) => {
        //     const {pointcloud} = e;
        //
        //     pointcloud.position.set(...data.position);
        //     pointcloud.rotation.set(...data.rotation);
        //     pointcloud.scale.set(...data.scale);
        //
        //     loadMaterial(pointcloud.material);
        //
        //     viewer.scene.addPointCloud(pointcloud);
        //
        //     resolve(pointcloud);
        // }).then();
    });
}

function loadMeasurement(viewer: Viewer, data: any) {
    const duplicate = viewer.scene.measurements.find(measure => measure.uuid === data.uuid);

    if (duplicate) {
        return;
    }

    const measure = new Measure();

    measure.uuid = data.uuid;
    measure.name = data.name;
    measure.showDistances = data.showDistances;
    measure.showCoordinates = data.showCoordinates;
    measure.showArea = data.showArea;
    measure.closed = data.closed;
    measure.showAngles = data.showAngles;
    measure.showHeight = data.showHeight;
    measure.showCircle = data.showCircle;
    measure.showAzimuth = data.showAzimuth;
    measure.showEdges = data.showEdges;
    // color

    for (const point of data.points) {
        const pos = new Vector3(...point);
        measure.addMarker(pos);
    }

    viewer.scene.addMeasurement(measure);

}

function loadVolume(viewer: Viewer, data: any) {
    const duplicate = viewer.scene.volumes.find(volume => volume.uuid === data.uuid);

    if (duplicate) {
        return;
    }

    let volume: any = null;

    switch (data.type) {
        case 'BoxVolume':
            volume = new BoxVolume();

            break;
        case 'SphereVolume':
            volume = new SphereVolume();

            break;
    }

    if (volume !== null) {
        volume.uuid = data.uuid;
        volume.name = data.name;
        volume.position.set(...data.position);
        volume.rotation.set(...data.rotation);
        volume.scale.set(...data.scale);
        volume.visible = data.visible;
        volume.clip = data.clip;

        viewer.scene.addVolume(volume);
    }
}

function loadCameraAnimation(viewer: Viewer, data: any) {
    const duplicate = viewer.scene.cameraAnimations.find(a => a.uuid === data.uuid);
    if (duplicate) {
        return;
    }

    const animation = new CameraAnimation(viewer);

    animation.uuid = data.uuid;
    animation.name = data.name;
    animation.duration = data.duration;
    animation.t = data.t;
    animation.curveType = data.curveType;
    animation.visible = data.visible;
    animation.controlPoints = [];

    for (const cpData of data.controlPoints) {
        const cp = animation.createControlPoint();
        // @ts-ignore
        cp.position.set(...cpData.position);
        // @ts-ignore
        cp.target.set(...cpData.target);
    }

    viewer.scene.addCameraAnimation(animation);
}

function loadOrientedImages(viewer: Viewer, images: any) {
    const {cameraParamsPath, imageParamsPath} = images;
    const duplicate = viewer.scene.orientedImages.find(i => i.imageParamsPath === imageParamsPath);

    if (duplicate) {
        return;
    }

    OrientedImageLoader.load(cameraParamsPath, imageParamsPath, viewer).then(images => {
        viewer.scene.addOrientedImages(images);
    });
}

function loadGeoPackage(viewer: Viewer, geoPackage: any) {
    const path = geoPackage.path;

    const duplicate = viewer.scene.geoPackages.find(i => i.path === path);
    if (duplicate) {
        return;
    }

    const projection = viewer.getProjection();

    proj4.defs("WGS84", "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");
    if (projection) {
        proj4.defs("pointcloud", projection);
    }
    const transform = proj4("WGS84", "pointcloud");
    const params = {
        transform: transform,
    };

    GeoPackageLoader.loadUrl(path, params).then(data => {
        viewer.scene.addGeoPackage(data);
    });
}

function loadSettings(viewer: Viewer, data: any) {
    if (!data) {
        return;
    }

    viewer.setPointBudget(data.pointBudget);
    viewer.setFOV(data.fov);
    viewer.setEDLEnabled(data.edlEnabled);
    viewer.setEDLRadius(data.edlRadius);
    viewer.setEDLStrength(data.edlStrength);
    viewer.setBackground(data.background);
    viewer.setMinNodeSize(data.minNodeSize);
    viewer.setShowBoundingBox(data.showBoundingBoxes);
}

function loadView(viewer: Viewer, view: any) {
    // @ts-ignore
    viewer.scene.view.position.set(...view.position);
    // @ts-ignore
    viewer.scene.view.lookAt(...view.target);
}

function loadAnnotationItem(item: any) {
    const annotation = new Annotation({
        position: item.position,
        title: item.title,
        cameraPosition: item.cameraPosition,
        cameraTarget: item.cameraTarget,
    });


    annotation.description = item.description;
    annotation.uuid = item.uuid;

    if (item.offset) {
        // @ts-ignore
        annotation.offset.set(...item.offset);
    }

    return annotation;
}

function loadAnnotations(viewer: Viewer, data: any) {
    if (!data) {
        return;
    }

    const findDuplicate = (item: any): any => {
        let duplicate = null;

        viewer.scene.annotations.traverse((a: any) => {
            if (a.uuid === item.uuid) {
                duplicate = a;
            }
        });

        return duplicate;
    };

    const traverse = (item: any, parent: any) => {

        const duplicate = findDuplicate(item);
        if (duplicate) {
            return;
        }

        const annotation = loadAnnotationItem(item);

        for (const childItem of item.children) {
            traverse(childItem, annotation);
        }

        parent.add(annotation);

    };

    for (const item of data) {
        traverse(item, viewer.scene.annotations);
    }

}

function loadProfile(viewer: Viewer, data: any) {
    const {name, points} = data;

    const duplicate = viewer.scene.profiles.find(profile => profile.uuid === data.uuid);
    if (duplicate) {
        return;
    }

    let profile = new Profile();
    profile.name = name;
    profile.uuid = data.uuid;

    profile.setWidth(data.width);

    for (const point of points) {
        profile.addMarker(new Vector3(...point));
    }

    viewer.scene.addProfile(profile);
}

function loadClassification(viewer: Viewer, data: any) {
    if (!data) {
        return;
    }

    viewer.setClassifications(data);
}

export async function loadProject(viewer: Viewer, data: any) {
    if (data.type !== "II") {
        console.error("Not a valid II project");

        return;
    }

    loadSettings(viewer, data.settings);

    loadView(viewer, data.view);

    const pointcloudPromises = [];
    for (const pointcloud of data.pointclouds) {
        const promise = loadPointCloud(viewer, pointcloud);
        pointcloudPromises.push(promise);
    }

    for (const measure of data.measurements) {
        loadMeasurement(viewer, measure);
    }

    for (const volume of data.volumes) {
        loadVolume(viewer, volume);
    }

    for (const animation of data.cameraAnimations) {
        loadCameraAnimation(viewer, animation);
    }

    for (const profile of data.profiles) {
        loadProfile(viewer, profile);
    }

    if (data.orientedImages) {
        for (const images of data.orientedImages) {
            loadOrientedImages(viewer, images);
        }
    }

    loadAnnotations(viewer, data.annotations);

    loadClassification(viewer, data.classification);

    // need to load at least one point cloud that defines the scene projection,
    // before we can load stuff in other projections such as geoPackages
    //await Promise.any(pointcloudPromises); // (not yet supported)
    Utils.waitAny(pointcloudPromises).then(() => {
        if (data.geoPackages) {
            for (const geoPackage of data.geoPackages) {
                loadGeoPackage(viewer, geoPackage);
            }
        }
    });

    await Promise.all(pointcloudPromises);
}
