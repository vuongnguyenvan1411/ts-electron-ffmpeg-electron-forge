import {Vector3} from "three";
import {Easing, Tween} from "@tweenjs/tween.js";

export class View {
    position: Vector3;
    yaw: number;
    protected _pitch: number;
    radius: number;
    maxPitch: number;
    minPitch: number;

    constructor() {
        this.position = new Vector3(0, 0, 0);

        this.yaw = Math.PI / 4;
        this._pitch = -Math.PI / 4;
        this.radius = 1;

        this.maxPitch = Math.PI / 2;
        this.minPitch = -Math.PI / 2;
    }

    clone() {
        const c = new View();

        c.yaw = this.yaw;
        c._pitch = this.pitch;
        c.radius = this.radius;
        c.maxPitch = this.maxPitch;
        c.minPitch = this.minPitch;

        return c;
    }

    get pitch() {
        return this._pitch;
    }

    set pitch(angle) {
        this._pitch = Math.max(Math.min(angle, this.maxPitch), this.minPitch);
    }

    get direction() {
        const dir = new Vector3(0, 1, 0);

        dir.applyAxisAngle(new Vector3(1, 0, 0), this.pitch);
        dir.applyAxisAngle(new Vector3(0, 0, 1), this.yaw);

        return dir;
    }

    set direction(dir) {
        //if(dir.x === dir.y){
        if (dir.x === 0 && dir.y === 0) {
            this.pitch = Math.PI / 2 * Math.sign(dir.z);
        } else {
            const yaw = Math.atan2(dir.y, dir.x) - Math.PI / 2;
            const pitch = Math.atan2(dir.z, Math.sqrt(dir.x * dir.x + dir.y * dir.y));

            this.yaw = yaw;
            this.pitch = pitch;
        }
    }

    lookAt(t: Vector3) {
        let V: Vector3 = new Vector3();

        if (arguments.length === 1) {
            V = V.subVectors(t, this.position);
        } else if (arguments.length === 3) {
            // @ts-ignore
            V = V.subVectors(new Vector3(...arguments), this.position);
        }

        const radius = V.length();
        const dir = V.normalize();

        this.radius = radius;
        this.direction = dir;
    }

    getPivot() {
        return new Vector3().addVectors(this.position, this.direction.multiplyScalar(this.radius));
    }

    getSide() {
        const side = new Vector3(1, 0, 0);
        side.applyAxisAngle(new Vector3(0, 0, 1), this.yaw);

        return side;
    }

    pan(x: number, y: number) {
        const dir = new Vector3(0, 1, 0);
        dir.applyAxisAngle(new Vector3(1, 0, 0), this.pitch);
        dir.applyAxisAngle(new Vector3(0, 0, 1), this.yaw);

        // let side = new Vector3(1, 0, 0);
        // side.applyAxisAngle(new Vector3(0, 0, 1), this.yaw);

        const side = this.getSide();
        const up = side.clone().cross(dir);
        const pan = side.multiplyScalar(x).add(up.multiplyScalar(y));

        this.position = this.position.add(pan);
        // this.target = this.target.add(pan);
    }

    translate(x: number, y: number, z: number) {
        const dir = new Vector3(0, 1, 0);
        dir.applyAxisAngle(new Vector3(1, 0, 0), this.pitch);
        dir.applyAxisAngle(new Vector3(0, 0, 1), this.yaw);

        const side = new Vector3(1, 0, 0);
        side.applyAxisAngle(new Vector3(0, 0, 1), this.yaw);

        const up = side.clone().cross(dir);
        const t = side.multiplyScalar(x)
            .add(dir.multiplyScalar(y))
            .add(up.multiplyScalar(z));

        this.position = this.position.add(t);
    }

    translateWorld(x: number, y: number, z: number) {
        this.position.x += x;
        this.position.y += y;
        this.position.z += z;
    }

    setView(position: any, target: Vector3, duration: number = 0, callback: any = null) {
        let endPosition: any = null;

        if (position instanceof Array) {
            endPosition = new Vector3(...position);
        } else if (position.x != null) {
            endPosition = position.clone();
        }

        let endTarget: any = null;

        if (target instanceof Array) {
            endTarget = new Vector3(...target);
        } else if (target.x != null) {
            endTarget = target.clone();
        }

        const startPosition = this.position.clone();
        const startTarget = this.getPivot();

        //const endPosition = position.clone();
        //const endTarget = target.clone();

        const easing = Easing.Quartic.Out;

        if (duration === 0) {
            this.position.copy(endPosition);
            this.lookAt(endTarget);
        } else {
            const value = {x: 0};
            const tween = new Tween(value).to({x: 1}, duration);

            tween.easing(easing);

            tween.onUpdate(() => {
                let t = value.x;

                const pos = new Vector3(
                    (1 - t) * startPosition.x + t * endPosition.x,
                    (1 - t) * startPosition.y + t * endPosition.y,
                    (1 - t) * startPosition.z + t * endPosition.z,
                );

                const target = new Vector3(
                    (1 - t) * startTarget.x + t * endTarget.x,
                    (1 - t) * startTarget.y + t * endTarget.y,
                    (1 - t) * startTarget.z + t * endTarget.z,
                );

                this.position.copy(pos);
                this.lookAt(target);

            });

            tween.start();

            tween.onComplete(() => {
                if (callback) {
                    callback();
                }
            });
        }
    }
}
