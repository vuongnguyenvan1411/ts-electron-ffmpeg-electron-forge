import {PointCloudSM} from "../utils/PointCloudSM";
import {EyeDomeLightingMaterial} from "../materials/EyeDomeLightingMaterial";
import {SphereVolume} from "../utils/Volume";
import {Utils} from "../core/Utils";
import {Viewer} from "./viewer";
import {updatePointClouds} from "../core/CUpdateVisibility";
import {App} from "../core/App";
import {DepthTexture, FloatType, NearestFilter, RGBAFormat, UnsignedIntType, Vector2, Vector3, WebGLRenderTarget} from "three";
import {ViewerEventName} from "../core/Event";
import {BackgroundType} from "../core/Defines";

export class EDLRenderer {
    viewer: Viewer;
    edlMaterial: EyeDomeLightingMaterial | null;
    rtRegular: WebGLRenderTarget;
    rtEDL: WebGLRenderTarget;
    gl: WebGLRenderingContext;
    shadowMap: PointCloudSM;
    screenshot: {
        target: WebGLRenderTarget
    } | undefined;

    constructor(viewer: Viewer) {
        this.viewer = viewer;
        this.edlMaterial = null;
        this.gl = viewer.renderer.getContext();
        this.shadowMap = new PointCloudSM(this.viewer.pRenderer);
    }

    initEDL() {
        if (this.edlMaterial != null) {
            return;
        }

        this.edlMaterial = new EyeDomeLightingMaterial();
        this.edlMaterial.depthTest = true;
        this.edlMaterial.depthWrite = true;
        this.edlMaterial.transparent = true;

        this.rtEDL = new WebGLRenderTarget(1024, 1024, {
            minFilter: NearestFilter,
            magFilter: NearestFilter,
            format: RGBAFormat,
            type: FloatType,
            depthTexture: new DepthTexture(0, 0, UnsignedIntType)
        });

        this.rtRegular = new WebGLRenderTarget(1024, 1024, {
            minFilter: NearestFilter,
            magFilter: NearestFilter,
            format: RGBAFormat,
            depthTexture: new DepthTexture(0, 0, UnsignedIntType)
        });
    };

    resize(width: number, height: number) {
        if (this.screenshot) {
            width = this.screenshot.target.width;
            height = this.screenshot.target.height;
        }

        this.rtEDL.setSize(width, height);
        this.rtRegular.setSize(width, height);
    }

    makeScreenshot(size?: Vector2) {
        // if (!camera) {
        //     camera = this.viewer.scene.getActiveCamera();
        // }

        if (!size) {
            size = this.viewer.renderer.getSize(new Vector2());
        }

        let {width, height} = size;

        //let maxTextureSize = viewer.renderer.capabilities.maxTextureSize;
        //if(width * 4 <
        width = 2 * width;
        height = 2 * height;

        const target = new WebGLRenderTarget(width, height, {
            format: RGBAFormat,
        });

        this.screenshot = {
            target: target
        };

        // HACK? removed because of error, was this important?
        // this.viewer.renderer.clearTarget(target, true, true, true);

        this.render();

        const pixelCount = width * height;
        const buffer = new Uint8Array(4 * pixelCount);

        this.viewer.renderer.readRenderTargetPixels(target, 0, 0, width, height, buffer);

        // flip vertically
        const bytesPerLine = width * 4;
        for (let i = 0; i < (parseInt(height.toString()) / 2); i++) {
            const j = height - i - 1;
            const lineI = buffer.slice(i * bytesPerLine, i * bytesPerLine + bytesPerLine);
            const lineJ = buffer.slice(j * bytesPerLine, j * bytesPerLine + bytesPerLine);

            buffer.set(lineJ, i * bytesPerLine);
            buffer.set(lineI, j * bytesPerLine);
        }

        this.screenshot.target.dispose();
        delete this.screenshot;

        return {
            width: width,
            height: height,
            buffer: buffer
        };
    }

    clearTargets() {
        const viewer = this.viewer;
        const {renderer} = viewer;
        const oldTarget = renderer.getRenderTarget();

        renderer.setRenderTarget(this.rtEDL);
        renderer.clear(true, true, true);

        renderer.setRenderTarget(this.rtRegular);
        renderer.clear(true, true, false);

        renderer.setRenderTarget(oldTarget);
    }

    clear() {
        this.initEDL();
        const viewer = this.viewer;

        const {renderer, background} = viewer;

        if (background === BackgroundType.Skybox) {
            renderer.setClearColor(0x000000, 0);
        } else if (background === BackgroundType.Gradient) {
            renderer.setClearColor(0x000000, 0);
        } else if (background === BackgroundType.Black) {
            renderer.setClearColor(0x000000, 1);
        } else if (background === BackgroundType.White) {
            renderer.setClearColor(0xFFFFFF, 1);
        } else {
            renderer.setClearColor(0x000000, 0);
        }

        renderer.clear();

        this.clearTargets();
    }

    renderShadowMap(visiblePointClouds: any, lights: any) {
        const {viewer} = this;
        const doShadows = lights.length > 0 && !(lights[0].disableShadowUpdates);

        if (doShadows) {
            const light = lights[0];

            this.shadowMap.setLight(light);

            const originalAttributes = new Map();
            for (const pointCloud of viewer.scene.pointClouds) {
                // TODO IMPORTANT !!! check
                originalAttributes.set(pointCloud, pointCloud.material.activeAttributeName);
                pointCloud.material.disableEvents();
                pointCloud.material.activeAttributeName = "depth";
                // pointCloud.material.pointColorType = PointColorType.DEPTH;
            }

            this.shadowMap.render(viewer.scene.scenePointCloud);

            for (let pointCloud of visiblePointClouds) {
                // TODO IMPORTANT !!! check
                pointCloud.material.activeAttributeName = originalAttributes.get(pointCloud);
                pointCloud.material.enableEvents();
            }

            viewer.shadowTestCam.updateMatrixWorld();
            viewer.shadowTestCam.matrixWorldInverse.copy(viewer.shadowTestCam.matrixWorld).invert();
            viewer.shadowTestCam.updateProjectionMatrix();
        }
    }

    render(params?: any) {
        this.initEDL();

        const viewer = this.viewer;
        let camera = params.camera ? params.camera : viewer.scene.getActiveCamera();
        const {width, height} = this.viewer.renderer.getSize(new Vector2());


        viewer.dispatchEvent({
            type: ViewerEventName.Render_Pass_Begin,
            viewer: viewer
        });

        this.resize(width, height);

        const visiblePointClouds = viewer.scene.pointClouds.filter(pc => pc.visible);

        if (this.screenshot) {
            const oldBudget = App.pointBudget;
            App.pointBudget = Math.max(10 * 1000 * 1000, 2 * oldBudget);
            updatePointClouds(viewer.scene.pointClouds, camera, viewer.renderer);
            App.pointBudget = oldBudget;
        }

        const lights: any[] = [];

        viewer.scene.scene.traverse(node => {
            if (node.type === "SpotLight") {
                lights.push(node);
            }
        });

        if (viewer.background === BackgroundType.Skybox) {
            viewer.skybox.camera.rotation.copy(viewer.scene.cameraP.rotation);
            viewer.skybox.camera.fov = viewer.scene.cameraP.fov;
            viewer.skybox.camera.aspect = viewer.scene.cameraP.aspect;

            viewer.skybox.parent.rotation.x = 0;
            viewer.skybox.parent.updateMatrixWorld();

            viewer.skybox.camera.updateProjectionMatrix();
            viewer.renderer.render(viewer.skybox.scene, viewer.skybox.camera);
        } else if (viewer.background === BackgroundType.Gradient) {
            viewer.renderer.render(viewer.scene.sceneBG, viewer.scene.cameraBG);
        }

        //TODO adapt to multiple lights
        this.renderShadowMap(visiblePointClouds, lights);

        // COLOR & DEPTH PASS
        for (const pointCloud of visiblePointClouds) {
            const octreeSize = pointCloud.pcoGeometry.boundingBox.getSize(new Vector3()).x;

            const material = pointCloud.material;
            material.weighted = false;
            material.useLogarithmicDepthBuffer = false;
            material.useEDL = true;

            material.screenWidth = width;
            material.screenHeight = height;
            material.uniforms.visibleNodes.value = pointCloud.material.visibleNodesTexture;
            material.uniforms.octreeSize.value = octreeSize;
            material.spacing = pointCloud.pcoGeometry.spacing; // * Math.max(pointcloud.scale.x, pointcloud.scale.y, pointcloud.scale.z);
        }

        // TODO adapt to multiple lights
        viewer.renderer.setRenderTarget(this.rtEDL);

        if (lights.length > 0) {
            viewer.pRenderer.render(viewer.scene.scenePointCloud, camera, this.rtEDL, {
                clipSpheres: viewer.scene.volumes.filter(v => (v instanceof SphereVolume)),
                shadowMaps: [this.shadowMap],
                transparent: false,
            });
        } else {
            // let test = camera.clone();
            // test.matrixAutoUpdate = false;

            // //test.updateMatrixWorld = () => {};

            // let mat = new Matrix4().set(
            // 	1, 0, 0, 0,
            // 	0, 0, 1, 0,
            // 	0, -1, 0, 0,
            // 	0, 0, 0, 1,
            // );
            // mat.invert()

            // test.matrix.multiplyMatrices(mat, test.matrix);
            // test.updateMatrixWorld();

            //test.matrixWorld.multiplyMatrices(mat, test.matrixWorld);
            //test.matrixWorld.multiply(mat);
            //test.matrixWorldInverse.invert(test.matrixWorld);
            //test.matrixWorldInverse.multiplyMatrices(test.matrixWorldInverse, mat);

            viewer.pRenderer.render(viewer.scene.scenePointCloud, camera, this.rtEDL, {
                clipSpheres: viewer.scene.volumes.filter(v => (v instanceof SphereVolume)),
                transparent: false,
            });
        }

        viewer.dispatchEvent({
            type: ViewerEventName.Render_Pass_Scene,
            viewer: viewer,
            renderTarget: this.rtRegular
        });
        viewer.renderer.setRenderTarget(null);
        viewer.renderer.render(viewer.scene.scene, camera);

        // EDL PASS
        if (this.edlMaterial) {
            const uniforms = this.edlMaterial.uniforms;

            uniforms.screenWidth.value = width;
            uniforms.screenHeight.value = height;

            const proj = camera.projectionMatrix;
            const projArray = new Float32Array(16);
            projArray.set(proj.elements);

            uniforms.uNear.value = camera.near;
            uniforms.uFar.value = camera.far;
            uniforms.uEDLColor.value = this.rtEDL.texture;
            uniforms.uEDLDepth.value = this.rtEDL.depthTexture;
            uniforms.uProj.value = projArray;

            uniforms.edlStrength.value = viewer.edlStrength;
            uniforms.radius.value = viewer.edlRadius;
            uniforms.opacity.value = viewer.edlOpacity; // HACK

            Utils.screenPassRender(viewer.renderer, this.edlMaterial);

            if (this.screenshot) {
                Utils.screenPassRender(viewer.renderer, this.edlMaterial, this.screenshot.target);
            }
        }

        viewer.dispatchEvent({
            type: ViewerEventName.Render_Pass_Scene,
            viewer: viewer
        });

        viewer.renderer.clearDepth();

        viewer.transformationTool.update();

        viewer.dispatchEvent({
            type: ViewerEventName.Render_Pass_PerspectiveOverlay,
            viewer: viewer
        });

        viewer.renderer.render(viewer.controls.sceneControls, camera);
        viewer.renderer.render(viewer.clippingTool.sceneVolume, camera);
        viewer.renderer.render(viewer.transformationTool.scene, camera);

        viewer.dispatchEvent({
            type: ViewerEventName.Render_Pass_End,
            viewer: viewer
        });
    }
}

