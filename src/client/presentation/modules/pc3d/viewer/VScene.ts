import {Annotation} from "../core/Annotation";
import {CameraMode} from "../core/Defines";
import {Utils} from "../core/Utils";
import {EventDispatcher} from "../core/EventDispatcher";
import {View} from "./View";
import {PointCloudOctree} from "../core/PointCloudOctree";
import {TextureMeshTree} from "../core/TextureMeshTree";
import {TextureMeshModel} from "../models/MapModel";
import {AmbientLight, Box3, Camera, DirectionalLight, LinearFilter, Mesh, MeshBasicMaterial, NearestFilter, Object3D, OrthographicCamera, PerspectiveCamera, PlaneBufferGeometry, Ray, Scene as ThreeScene, Vector3} from "three";
import {Profile} from "../utils/Profile";
import {AnnotationEvent, ViewerEventName} from "../core/Event";
import {Measure} from "../utils/Measure";
import {CameraAnimation} from "../modules/CameraAnimation/CameraAnimation";

export class VScene extends EventDispatcher {
    annotations: Annotation;
    scene: ThreeScene;
    sceneBG: ThreeScene;
    scenePointCloud: ThreeScene;
    cameraP: PerspectiveCamera;
    cameraO: OrthographicCamera;
    cameraVR: PerspectiveCamera;
    fov: number;
    cameraBG: Camera;
    cameraScreenSpace: OrthographicCamera;
    cameraMode: number;
    overrideCamera: any;
    pointClouds: PointCloudOctree[];
    textureMeshs: TextureMeshTree[];
    layerMeshs: TextureMeshModel[];
    measurements: any[];
    profiles: Profile[];
    volumes: any[];
    polygonClipVolumes: any[];
    cameraAnimations: CameraAnimation[];
    orientedImages: any[];
    images360: any[];
    geoPackages: any[];
    fpControls: any;
    orbitControls: any;
    earthControls: any;
    geoControls: any;
    deviceControls: any;
    inputHandler: any;
    view: View;
    directionalLight: DirectionalLight;
    referenceFrame: Object3D;
    lengthUnit: any;
    lengthUnitDisplay: any;
    children: any[];

    constructor() {
        super();

        this.annotations = new Annotation();

        this.scene = new ThreeScene();
        this.sceneBG = new ThreeScene();
        this.scenePointCloud = new ThreeScene();

        // @ts-ignore
        this.cameraP = new PerspectiveCamera(this.fov, 1, 0.1, 1000 * 1000);
        // @ts-ignore
        this.cameraO = new OrthographicCamera(-1, 1, 1, -1, 0.1, 1000 * 1000);
        // @ts-ignore
        this.cameraVR = new PerspectiveCamera();
        this.cameraBG = new Camera();
        // @ts-ignore
        this.cameraScreenSpace = new OrthographicCamera(-1, 1, 1, -1, 0.1, 10);
        this.cameraMode = CameraMode.PERSPECTIVE;
        this.overrideCamera = null;
        this.pointClouds = [];
        this.textureMeshs = [];
        this.layerMeshs = [];

        this.measurements = [];
        this.profiles = [];
        this.volumes = [];
        this.polygonClipVolumes = [];
        this.cameraAnimations = [];
        this.orientedImages = [];
        this.images360 = [];
        this.geoPackages = [];

        this.fpControls = null;
        this.orbitControls = null;
        this.earthControls = null;
        this.geoControls = null;
        this.deviceControls = null;
        this.inputHandler = null;

        this.view = new View();

        // this.directionalLight = null;

        this.initialize();
    }

    estimateHeightAt(position: any) {
        let height = null;
        let fromSpacing = Infinity;

        for (let pointCloud of this.pointClouds) {
            if (!pointCloud.root) {
                continue;
            } else if (pointCloud.root.geometryNode === undefined) {
                continue;
            }

            let pHeight = null;
            let pFromSpacing = Infinity;

            const lPos = position.clone().sub(pointCloud.position);
            lPos.z = 0;
            const ray = new Ray(lPos, new Vector3(0, 0, 1));

            const stack = [pointCloud.root];

            while (stack.length > 0) {
                const node = stack.pop();

                if (!node) {
                    continue
                }

                const box = node.getBoundingBox();
                // @ts-ignore
                const inside = ray.intersectBox(box);

                if (!inside) {
                    continue;
                }

                const h = node.geometryNode.mean.z + pointCloud.position.z + node.geometryNode.boundingBox.min.z;

                if (node.geometryNode.spacing <= pFromSpacing) {
                    pHeight = h;
                    pFromSpacing = node.geometryNode.spacing;
                }

                if (node.children) {
                    node.children.forEach((_, index) => {
                        if (node.children) {
                            const child = node.children[index];

                            if (child.geometryNode) {
                                stack.push(node.children[index]);
                            }
                        }
                    })
                }
            }

            if (height === null || pFromSpacing < fromSpacing) {
                height = pHeight;
                fromSpacing = pFromSpacing;
            }
        }

        return height;
    }

    getBoundingBox(pointClouds: PointCloudOctree[] = this.pointClouds) {
        const box = new Box3();

        this.scenePointCloud.updateMatrixWorld(true);
        this.referenceFrame.updateMatrixWorld(true);

        for (let pointcloud of pointClouds) {
            pointcloud.updateMatrixWorld(true);

            const pointcloudBox = pointcloud.pcoGeometry.tightBoundingBox ? pointcloud.pcoGeometry.tightBoundingBox : pointcloud.boundingBox;
            const boxWorld = Utils.computeTransformedBoundingBox(pointcloudBox, pointcloud.matrixWorld);

            box.union(boxWorld);
        }

        return box;
    }

    addPointCloud(pointCloud: PointCloudOctree) {
        this.pointClouds.push(pointCloud);
        this.scenePointCloud.add(pointCloud);

        this.dispatchEvent({
            type: ViewerEventName.PointCloudAdded,
            pointcloud: pointCloud
        });
    }

    addTextureMesh(textureMesh: TextureMeshTree) {
        this.textureMeshs.push(textureMesh);

        this.dispatchEvent({
            type: ViewerEventName.TextureMeshAdded,
            texturemesh: textureMesh
        });
    }

    addLayerMesh(textureMesh: TextureMeshModel) {
        this.layerMeshs.push(textureMesh);

        this.dispatchEvent({
            type: ViewerEventName.LayerMeshAdded,
            layermesh: textureMesh
        });
    }

    addVolume(volume: any) {
        this.volumes.push(volume);
        this.dispatchEvent({
            type: ViewerEventName.VolumeAdded,
            scene: this,
            volume: volume
        });
    }

    addOrientedImages(images: any) {
        this.orientedImages.push(images);
        this.scene.add(images.node);

        this.dispatchEvent({
            type: ViewerEventName.OrientedImagesAdded,
            scene: this,
            images: images
        });
    }

    removeOrientedImages(images: any) {
        let index = this.orientedImages.indexOf(images);
        if (index > -1) {
            this.orientedImages.splice(index, 1);

            this.dispatchEvent({
                type: ViewerEventName.OrientedImagesRemoved,
                scene: this,
                images: images
            });
        }
    };

    add360Images(images: any) {
        this.images360.push(images);
        this.scene.add(images.node);

        this.dispatchEvent({
            type: ViewerEventName.Vr360ImagesAdded,
            scene: this,
            images: images
        });
    }

    remove360Images(images: any) {
        const index = this.images360.indexOf(images);

        if (index > -1) {
            this.images360.splice(index, 1);

            this.dispatchEvent({
                type: ViewerEventName.Vr360ImagesRemoved,
                scene: this,
                images: images
            });
        }
    }

    addGeoPackage(geoPackage: any) {
        this.geoPackages.push(geoPackage);
        this.scene.add(geoPackage.node);

        this.dispatchEvent({
            type: ViewerEventName.GeoPackageAdded,
            scene: this,
            geopackage: geoPackage
        });
    }

    removeGeoPackage(geoPackage: any) {
        const index = this.geoPackages.indexOf(geoPackage);

        if (index > -1) {
            this.geoPackages.splice(index, 1);

            this.dispatchEvent({
                type: ViewerEventName.GeoPackageRemoved,
                scene: this,
                geopackage: geoPackage
            });
        }
    }

    removeVolume(volume: any) {
        const index = this.volumes.indexOf(volume);

        if (index > -1) {
            this.volumes.splice(index, 1);

            this.dispatchEvent({
                type: ViewerEventName.VolumeRemoved,
                scene: this,
                volume: volume
            });
        }
    }

    addCameraAnimation(animation: CameraAnimation) {
        this.cameraAnimations.push(animation);

        this.dispatchEvent({
            type: ViewerEventName.CameraAnimationAdded,
            scene: this,
            animation: animation
        });
    }

    removeCameraAnimation(animation: CameraAnimation) {
        const index = this.cameraAnimations.indexOf(animation);

        if (index > -1) {
            animation.onRemove();

            this.cameraAnimations.splice(index, 1);

            this.dispatchEvent({
                type: ViewerEventName.CameraAnimationRemoved,
                scene: this,
                animation: animation
            });
        }
    }

    addPolygonClipVolume(volume: any) {
        this.polygonClipVolumes.push(volume);

        this.dispatchEvent({
            type: ViewerEventName.PolygonClipVolumeAdded,
            scene: this,
            volume: volume
        });
    }

    removePolygonClipVolume(volume: any) {
        const index = this.polygonClipVolumes.indexOf(volume);

        if (index > -1) {
            this.polygonClipVolumes.splice(index, 1);
            this.dispatchEvent({
                type: ViewerEventName.PolygonClipVolumeRemoved,
                scene: this,
                volume: volume
            });
        }
    }

    addMeasurement(measurement: Measure) {
        measurement.lengthUnit = this.lengthUnit;
        measurement.lengthUnitDisplay = this.lengthUnitDisplay;

        this.measurements.push(measurement);
        this.dispatchEvent({
            type: ViewerEventName.MeasurementAdded,
            scene: this,
            measurement: measurement
        });
    }

    removeMeasurement(measurement: Measure) {
        const index = this.measurements.indexOf(measurement);

        if (index > -1) {
            this.measurements.splice(index, 1);
            this.dispatchEvent({
                type: ViewerEventName.MeasurementRemoved,
                scene: this,
                measurement: measurement
            });
        }
    }

    addProfile(profile: Profile) {
        this.profiles.push(profile);
        this.dispatchEvent({
            type: ViewerEventName.ProfileAdded,
            scene: this,
            profile: profile
        });
    }

    removeProfile(profile: Profile) {
        const index = this.profiles.indexOf(profile);

        if (index > -1) {
            this.profiles.splice(index, 1);
            this.dispatchEvent({
                type: ViewerEventName.ProfileRemoved,
                scene: this,
                profile: profile
            });
        }
    }

    removeAllMeasurements() {
        while (this.measurements.length > 0) {
            this.removeMeasurement(this.measurements[0]);
        }

        while (this.profiles.length > 0) {
            this.removeProfile(this.profiles[0]);
        }

        while (this.volumes.length > 0) {
            this.removeVolume(this.volumes[0]);
        }
    }

    removeAllClipVolumes() {
        const clipVolumes = this.volumes.filter(volume => volume.clip === true);

        for (let clipVolume of clipVolumes) {
            this.removeVolume(clipVolume);
        }

        while (this.polygonClipVolumes.length > 0) {
            this.removePolygonClipVolume(this.polygonClipVolumes[0]);
        }
    }

    getActiveCamera() {
        if (this.overrideCamera) {
            return this.overrideCamera;
        }

        if (this.cameraMode === CameraMode.PERSPECTIVE) {
            return this.cameraP;
        } else if (this.cameraMode === CameraMode.ORTHOGRAPHIC) {
            return this.cameraO;
        } else if (this.cameraMode === CameraMode.VR) {
            return this.cameraVR;
        }

        return null;
    }

    initialize() {
        this.referenceFrame = new Object3D();
        this.referenceFrame.matrixAutoUpdate = false;
        this.scenePointCloud.add(this.referenceFrame);

        this.cameraP.up.set(0, 0, 1);
        this.cameraP.position.set(1000, 1000, 1000);
        this.cameraO.up.set(0, 0, 1);
        this.cameraO.position.set(1000, 1000, 1000);
        //this.camera.rotation.y = -Math.PI / 4;
        //this.camera.rotation.x = -Math.PI / 6;

        // @ts-ignore
        this.cameraScreenSpace.lookAt(new Vector3(0, 0, 0), new Vector3(0, 0, -1), new Vector3(0, 1, 0));

        this.directionalLight = new DirectionalLight(0xffffff, 0.5);
        this.directionalLight.position.set(10, 10, 10);
        this.directionalLight.lookAt(new Vector3(0, 0, 0));
        this.scenePointCloud.add(this.directionalLight);

        const light = new AmbientLight(0x555555); // soft white light
        this.scenePointCloud.add(light);

        { // background
            const texture = Utils.createBackgroundTexture(512, 512);
            texture.minFilter = texture.magFilter = NearestFilter;
            texture.minFilter = texture.magFilter = LinearFilter;

            const bg = new Mesh(
                new PlaneBufferGeometry(2, 2, 1),
                new MeshBasicMaterial({
                    map: texture
                })
            );
            bg.material.depthTest = false;
            bg.material.depthWrite = false;

            this.sceneBG.add(bg);
        }

        // { // lights
        // 	{
        // 		let light = new DirectionalLight(0xffffff);
        // 		light.position.set(10, 10, 1);
        // 		light.target.position.set(0, 0, 0);
        // 		this.scene.add(light);
        // 	}

        // 	{
        // 		let light = new DirectionalLight(0xffffff);
        // 		light.position.set(-10, 10, 1);
        // 		light.target.position.set(0, 0, 0);
        // 		this.scene.add(light);
        // 	}

        // 	{
        // 		let light = new DirectionalLight(0xffffff);
        // 		light.position.set(0, -10, 20);
        // 		light.target.position.set(0, 0, 0);
        // 		this.scene.add(light);
        // 	}
        // }
    }

    addAnnotation(position: any, args: any = {}) {
        if (position instanceof Array) {
            args.position = new Vector3().fromArray(position);
        } else if (position.x != null) {
            args.position = position;
        }

        const annotation = new Annotation(args);
        this.annotations.add(annotation);

        return annotation;
    }

    getAnnotations() {
        return this.annotations;
    }

    removeAnnotation(annotation: Annotation) {
        this.annotations.remove(annotation);

        this.annotations.dispatchEvent({
            type: AnnotationEvent.AnnotationRemoved,
            annotation: annotation
        });
    }
}
