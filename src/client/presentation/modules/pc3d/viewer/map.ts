import {Viewer} from "./viewer";
import {Feature, Map, View} from "ol";
import {Circle, Fill, Stroke, Style, Text} from "ol/style";
import VectorSource from "ol/source/Vector";
import VectorLayer from "ol/layer/Vector";
import {defaults as defaultControls, MousePosition} from "ol/control";
import {DragBox, Select} from 'ol/interaction';
import {createStringXY} from "ol/coordinate";
import {OSM} from "ol/source";
import {platformModifierKeyOnly} from "ol/events/condition";
import {LineString, Point, Polygon} from "ol/geom";
import {VScene} from "./VScene";
import proj4 from "proj4";
import {register} from 'ol/proj/proj4';
import {Config} from "../const/Config";
import TileLayer from "ol/layer/Tile";
import LayerGroup from "ol/layer/Group";
import {ViewerEventName} from "../core/Event";
import {Vector2, Vector3} from "three";
// import {Vector2, Vector3} from "three";

// http://epsg.io/
proj4.defs([
    ['UTM10N', '+proj=utm +zone=10 +ellps=GRS80 +datum=NAD83 +units=m +no_defs'],
    ['EPSG:6339', '+proj=utm +zone=10 +ellps=GRS80 +units=m +no_defs'],
    ['EPSG:6340', '+proj=utm +zone=11 +ellps=GRS80 +units=m +no_defs'],
    ['EPSG:6341', '+proj=utm +zone=12 +ellps=GRS80 +units=m +no_defs'],
    ['EPSG:6342', '+proj=utm +zone=13 +ellps=GRS80 +units=m +no_defs'],
    ['EPSG:6343', '+proj=utm +zone=14 +ellps=GRS80 +units=m +no_defs'],
    ['EPSG:6344', '+proj=utm +zone=15 +ellps=GRS80 +units=m +no_defs'],
    ['EPSG:6345', '+proj=utm +zone=16 +ellps=GRS80 +units=m +no_defs'],
    ['EPSG:6346', '+proj=utm +zone=17 +ellps=GRS80 +units=m +no_defs'],
    ['EPSG:6347', '+proj=utm +zone=18 +ellps=GRS80 +units=m +no_defs'],
    ['EPSG:6348', '+proj=utm +zone=19 +ellps=GRS80 +units=m +no_defs'],
    ['EPSG:26910', '+proj=utm +zone=10 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs '],
    ['EPSG:26911', '+proj=utm +zone=11 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs '],
    ['EPSG:26912', '+proj=utm +zone=12 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs '],
    ['EPSG:26913', '+proj=utm +zone=13 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs '],
    ['EPSG:26914', '+proj=utm +zone=14 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs '],
    ['EPSG:26915', '+proj=utm +zone=15 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs '],
    ['EPSG:26916', '+proj=utm +zone=16 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs '],
    ['EPSG:26917', '+proj=utm +zone=17 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs '],
    ['EPSG:26918', '+proj=utm +zone=18 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs '],
    ['EPSG:26919', '+proj=utm +zone=19 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs '],
]);
register(proj4);

export class MapView {
    viewer: Viewer;
    webMapService: string;
    mapProjectionName: string;
    mapProjection: any;
    sceneProjection: any;
    elMap: HTMLDivElement;
    toMap: proj4.Converter;
    toScene: proj4.Converter;
    sourcesLayer: VectorLayer<any> | null;
    extentsLayer: VectorLayer<any> | null;
    cameraLayer: VectorLayer<any> | null;
    toolLayer: VectorLayer<any> | null;
    sourcesLabelLayer: VectorLayer<any> | null;
    images360Layer: VectorLayer<any> | null;
    enabled: boolean;
    elTooltip: HTMLDivElement;
    annotationsLayer: VectorLayer<any>;
    map: Map;
    dragBoxLayer: VectorLayer<any>;
    scene: VScene;
    gExtent: LineString;
    gCamera: LineString;

    constructor(viewer: Viewer) {
        this.viewer = viewer;

        this.webMapService = 'WMTS';
        this.mapProjectionName = Config.EPSG;
        this.mapProjection = proj4.defs(this.mapProjectionName);
        this.sceneProjection = null;

        this.extentsLayer = null;
        this.cameraLayer = null;
        this.toolLayer = null;
        this.sourcesLayer = null;
        this.sourcesLabelLayer = null;
        this.images360Layer = null;
        this.enabled = false;
    }

    createAnnotationStyle = (text: string) => {
        console.log("Map.createAnnotationStyle", text);

        return [
            new Style({
                image: new Circle({
                    radius: 10,
                    stroke: new Stroke({
                        color: [255, 255, 255, 0.5],
                        width: 2
                    }),
                    fill: new Fill({
                        color: [0, 0, 0, 0.5]
                    })
                })
            })
        ];
    }

    createLabelStyle = (text: string) => {
        return new Style({
            image: new Circle({
                radius: 6,
                stroke: new Stroke({
                    color: 'white',
                    width: 2
                }),
                fill: new Fill({
                    color: 'green'
                })
            }),
            text: new Text({
                font: '12px helvetica,sans-serif',
                text: text,
                fill: new Fill({
                    color: '#000'
                }),
                stroke: new Stroke({
                    color: '#fff',
                    width: 2
                })
            })
        });
    }

    showSources(show: boolean) {
        if (this.sourcesLayer) this.sourcesLayer.setVisible(show);
        if (this.sourcesLabelLayer) this.sourcesLabelLayer.setVisible(show);
    }

    init = () => {
        this.elMap = document.querySelector('#ol_map')!;
        // this.elMap.draggable({handle: $('#ol_map_header')});
        // this.elMap.resizable();

        this.elTooltip = document.createElement('div');
        this.elTooltip.style.position = 'relative';
        this.elTooltip.style.zIndex = '100';
        this.elMap.append(this.elTooltip);

        const extentsLayer = this.getExtentsLayer();
        const cameraLayer = this.getCameraLayer();
        this.getToolLayer();
        this.getSourcesLayer();
        this.images360Layer = this.getImages360Layer();
        this.getSourcesLabelLayer();
        this.getAnnotationsLayer();

        const mousePositionControl = new MousePosition({
            coordinateFormat: createStringXY(5),
            projection: Config.EPSG,
            undefinedHTML: '&nbsp;'
        });

        const mapLayers: LayerGroup[] = [
            new LayerGroup({
                // @ts-ignore
                title: 'Base maps',
                className: 'map',
                layers: [
                    new TileLayer({
                        // @ts-ignore
                        title: 'OpenStreetMap',
                        type: 'base',
                        visible: true,
                        source: new OSM({
                            attributions: [
                                `<a href="${Config.Domain}" target="_blank">&copy; ${Config.Brand} contributors</a>`
                            ]
                        })
                    }),
                ],
            }),
            new LayerGroup({
                // @ts-ignore
                title: 'Other',
                className: 'other',
                layers: [
                    this.toolLayer!,
                    this.annotationsLayer,
                    this.sourcesLayer!,
                    this.sourcesLabelLayer!,
                    this.images360Layer,
                    extentsLayer,
                    cameraLayer
                ]
            }),
        ];

        // const attribution = new Attribution({
        //     collapsible: false,
        // });

        this.map = new Map({
            controls: defaultControls({
                attributionOptions: ({
                    collapsible: false
                }),
                attribution: false,
            }).extend([
                mousePositionControl
            ]),
            // controls: defaultControls({
            //     attribution: false,
            // }).extend([
            //     mousePositionControl, attribution
            // ]),
            // interactions: defaultInteractions({
            //     onFocusOnly: true,
            // }),
            layers: mapLayers,
            target: 'ol_map_content',
            view: new View({
                center: Config.defaultView,
                zoom: 9,
                projection: Config.EPSG,
            })
        });

        // DRAG-BOX / SELECTION
        this.dragBoxLayer = new VectorLayer({
            source: new VectorSource({}),
            style: new Style({
                stroke: new Stroke({
                    color: 'rgba(0, 0, 255, 1)',
                    width: 2
                })
            })
        });
        this.map.addLayer(this.dragBoxLayer);

        let select = new Select();
        this.map.addInteraction(select);

        let selectedFeatures = select.getFeatures();

        let dragBox = new DragBox({
            condition: platformModifierKeyOnly
        });

        this.map.addInteraction(dragBox);

        // this.map.on('pointermove', evt => {
        // 	let pixel = evt.pixel;
        // 	let feature = this.map.forEachFeatureAtPixel(pixel, function (feature) {
        // 		return feature;
        // 	});

        // 	// console.log(feature);
        // 	// this.elTooltip.css("display", feature ? '' : 'none');
        // 	this.elTooltip.css('display', 'none');
        // 	if (feature && feature.onHover) {
        // 		feature.onHover(evt);
        // 		// overlay.setPosition(evt.coordinate);
        // 		// tooltip.innerHTML = feature.get('name');
        // 	}
        // });

        this.map.on('singleclick', (evt) => {
            const feature = this.map.forEachFeatureAtPixel(evt.pixel,
                function (feature) {
                    return feature;
                });

            // @ts-ignore
            if (feature && feature.onClick) {
                // @ts-ignore
                feature.onClick(evt);
            }
        });

        dragBox.on('boxend', () => {
            // features that intersect the box are added to the collection of
            // selected features, and their names are displayed in the "info"
            // div
            let extent = dragBox.getGeometry().getExtent();
            this.getSourcesLayer().getSource().forEachFeatureIntersectingExtent(extent, (feature: any) => {
                selectedFeatures.push(feature);
            });
        });

        // clear selection when drawing a new box and when clicking on the map
        dragBox.on('boxdrag', () => {
            selectedFeatures.clear();
        });
        this.map.on('click', () => {
            selectedFeatures.clear();
        });

        this.viewer.addEventListener(ViewerEventName.SceneChanged, (e: any) => {
            this.setScene(e.scene);
        });

        this.setScene(this.viewer.scene);
    }

    onPointcloudAdded = (e: any) => {
        this.load(e.pointcloud).then();
    }

    on360ImagesAdded = (e: any) => {
        this.addImages360(e.images);
    }

    onAnnotationAdded = (e: any) => {
        if (!this.sceneProjection) {
            return;
        }

        let annotation = e.annotation;
        let position = annotation.position;
        let mapPos = this.toMap.forward([position.x, position.y]);
        let feature = new Feature({
            geometry: new Point(mapPos),
            name: annotation.title
        });
        feature.setStyle(this.createAnnotationStyle(annotation.title));

        // @ts-ignore
        feature.onHover = () => {
            let coordinates = (feature.getGeometry() as Point).getCoordinates();
            let p = this.map.getPixelFromCoordinate(coordinates);

            this.elTooltip.innerHTML = annotation.title;
            this.elTooltip.style.display = '';
            this.elTooltip.style.left = `${p[0]}px`;
            this.elTooltip.style.top = `${p[1]}px`;
        };

        // @ts-ignore
        feature.onClick = () => {
            annotation.clickTitle();
        };

        this.getAnnotationsLayer().getSource().addFeature(feature);
    }

    setScene(scene: any) {
        if (this.scene === scene) {
            return;
        }

        if (this.scene) {
            this.scene.removeEventListener('pointcloud_added', this.onPointcloudAdded);
            this.scene.removeEventListener('360_images_added', this.on360ImagesAdded);
            this.scene.annotations.removeEventListener('annotation_added', this.onAnnotationAdded);
        }

        this.scene = scene;

        this.scene.addEventListener('pointcloud_added', this.onPointcloudAdded);
        this.scene.addEventListener('360_images_added', this.on360ImagesAdded);
        this.scene.annotations.addEventListener('annotation_added', this.onAnnotationAdded);

        for (let pointcloud of this.viewer.scene.pointClouds) {
            this.load(pointcloud).then();
        }

        this.viewer.scene.annotations.traverseDescendants((annotation: any) => {
            this.onAnnotationAdded({annotation: annotation});
        });

        for (let images of this.viewer.scene.images360) {
            this.on360ImagesAdded({images: images});
        }
    }

    getExtentsLayer() {
        if (this.extentsLayer) {
            return this.extentsLayer;
        }

        this.gExtent = new LineString([[0, 0], [0, 0]]);

        let feature = new Feature(this.gExtent);
        let featureVector = new VectorSource({
            features: [feature]
        });

        this.extentsLayer = new VectorLayer({
            source: featureVector,
            style: new Style({
                fill: new Fill({
                    color: 'rgba(255, 255, 255, 0.2)'
                }),
                stroke: new Stroke({
                    color: '#0000ff',
                    width: 2
                }),
                image: new Circle({
                    radius: 3,
                    fill: new Fill({
                        color: '#0000ff'
                    })
                })
            })
        });

        return this.extentsLayer;
    }

    getAnnotationsLayer() {
        if (this.annotationsLayer) {
            return this.annotationsLayer;
        }

        this.annotationsLayer = new VectorLayer({
            source: new VectorSource({}),
            style: new Style({
                fill: new Fill({
                    color: 'rgba(255, 0, 0, 1)'
                }),
                stroke: new Stroke({
                    color: 'rgba(255, 0, 0, 1)',
                    width: 2
                })
            })
        });

        return this.annotationsLayer;
    }

    getCameraLayer() {
        if (this.cameraLayer) {
            return this.cameraLayer;
        }

        // CAMERA LAYER
        this.gCamera = new LineString([[0, 0], [0, 0], [0, 0], [0, 0]]);
        let feature = new Feature(this.gCamera);
        let featureVector = new VectorSource({
            features: [feature]
        });

        this.cameraLayer = new VectorLayer({
            source: featureVector,
            style: new Style({
                stroke: new Stroke({
                    color: '#0000ff',
                    width: 2
                })
            })
        });

        return this.cameraLayer;
    }

    getToolLayer() {
        if (this.toolLayer) {
            return this.toolLayer;
        }

        this.toolLayer = new VectorLayer({
            source: new VectorSource({}),
            style: new Style({
                fill: new Fill({
                    color: 'rgba(255, 0, 0, 1)'
                }),
                stroke: new Stroke({
                    color: 'rgba(255, 0, 0, 1)',
                    width: 2
                })
            })
        });

        return this.toolLayer;
    }

    getImages360Layer() {
        if (this.images360Layer) {
            return this.images360Layer;
        }

        let style = new Style({
            image: new Circle({
                radius: 4,
                stroke: new Stroke({
                    color: [255, 0, 0, 1],
                    width: 2
                }),
                fill: new Fill({
                    color: [255, 100, 100, 1]
                })
            })
        });

        this.images360Layer = new VectorLayer({
            source: new VectorSource({}),
            style: style,
        });

        return this.images360Layer;
    }

    getSourcesLayer() {
        if (this.sourcesLayer) {
            return this.sourcesLayer;
        }

        this.sourcesLayer = new VectorLayer({
            source: new VectorSource({}),
            style: new Style({
                fill: new Fill({
                    color: 'rgba(0, 0, 150, 0.1)'
                }),
                stroke: new Stroke({
                    color: 'rgba(0, 0, 150, 1)',
                    width: 1
                })
            })
        });

        return this.sourcesLayer;
    }

    getSourcesLabelLayer() {
        if (this.sourcesLabelLayer) {
            return this.sourcesLabelLayer;
        }

        this.sourcesLabelLayer = new VectorLayer({
            source: new VectorSource({}),
            style: new Style({
                fill: new Fill({
                    color: 'rgba(255, 0, 0, 0.1)'
                }),
                stroke: new Stroke({
                    color: 'rgba(255, 0, 0, 1)',
                    width: 2
                })
            }),
            minResolution: 0.01,
            maxResolution: 20
        });

        return this.sourcesLabelLayer;
    }

    setSceneProjection(sceneProjection: any) {
        this.sceneProjection = sceneProjection;
        this.toMap = proj4(this.sceneProjection, this.mapProjection);
        this.toScene = proj4(this.mapProjection, this.sceneProjection);
    };

    getMapExtent() {
        let bb = this.viewer.getBoundingBox();

        let bottomLeft = this.toMap.forward([bb.min.x, bb.min.y]);
        let bottomRight = this.toMap.forward([bb.max.x, bb.min.y]);
        let topRight = this.toMap.forward([bb.max.x, bb.max.y]);
        let topLeft = this.toMap.forward([bb.min.x, bb.max.y]);

        return {
            bottomLeft: bottomLeft,
            bottomRight: bottomRight,
            topRight: topRight,
            topLeft: topLeft
        };
    };

    getMapCenter() {
        let mapExtent = this.getMapExtent();

        return [
            (mapExtent.bottomLeft[0] + mapExtent.topRight[0]) / 2,
            (mapExtent.bottomLeft[1] + mapExtent.topRight[1]) / 2
        ];
    };

    updateToolDrawings() {
        this.toolLayer!.getSource().clear();

        let profiles = this.viewer.profileTool.profiles;

        for (let i = 0; i < profiles.length; i++) {
            let profile = profiles[i];
            let coordinates = [];

            for (let j = 0; j < profile.points.length; j++) {
                let point = profile.points[j];
                let pointMap = this.toMap.forward([point.x, point.y]);
                coordinates.push(pointMap);
            }

            let line = new LineString(coordinates);
            let feature = new Feature(line);
            this.toolLayer!.getSource().addFeature(feature);
        }

        let measurements = this.viewer.measuringTool.measurements;

        for (let i = 0; i < measurements.length; i++) {
            let measurement = measurements[i];
            let coordinates = [];

            for (let j = 0; j < measurement.points.length; j++) {
                let point = measurement.points[j].position;
                let pointMap = this.toMap.forward([point.x, point.y]);
                coordinates.push(pointMap);
            }

            if (measurement.closed && measurement.points.length > 0) {
                coordinates.push(coordinates[0]);
            }

            let line = new LineString(coordinates);
            let feature = new Feature(line);
            this.toolLayer!.getSource().addFeature(feature);
        }
    }

    addImages360(images: any) {
        let transform = this.toMap.forward;
        let layer = this.getImages360Layer();

        for (let image of images.images) {
            let p = transform([image.position[0], image.position[1]]);
            let feature = new Feature({
                geometry: new Point(p),
            });

            // @ts-ignore
            feature.onClick = () => {
                images.focus(image);
            };

            layer.getSource().addFeature(feature);
        }
    }

    async load(pointcloud: any) {
        if (!pointcloud) {
            return;
        }

        if (!pointcloud.projection) {
            return;
        }

        if (!this.sceneProjection) {
            try {
                this.setSceneProjection(pointcloud.projection);
            } catch (e) {
                console.log('Failed projection:', e);

                if (pointcloud.fallbackProjection) {
                    try {
                        console.log('Trying fallback projection...');
                        this.setSceneProjection(pointcloud.fallbackProjection);
                        console.log('Set projection from fallback');
                    } catch (e) {
                        console.log('Failed fallback projection:', e);
                        return;
                    }
                } else {
                    return;
                }
            }
        }

        const mapExtent = this.getMapExtent();
        const mapCenter = this.getMapCenter();
        const view = this.map.getView();
        view.setCenter(mapCenter);

        this.gExtent.setCoordinates([
            mapExtent.bottomLeft,
            mapExtent.bottomRight,
            mapExtent.topRight,
            mapExtent.topLeft,
            mapExtent.bottomLeft
        ]);

        view.fit(this.gExtent, {
            size: [300, 300],
            // constrainResolution: false
        });

        if (pointcloud.pcoGeometry.type === 'ept') {
            return;
        }

        let url = `${pointcloud.pcoGeometry.url}/../sources.json`;
        //let response = await fetch(url);

        fetch(url).then(async (response) => {
            const data = await response.json();
            const sources = data.sources;

            for (let i = 0; i < sources.length; i++) {
                const source = sources[i];
                const name = source.name;
                const bounds = source.bounds;

                const mapBounds = {
                    min: this.toMap.forward([bounds.min[0], bounds.min[1]]),
                    max: this.toMap.forward([bounds.max[0], bounds.max[1]])
                };
                const mapCenter = [
                    (mapBounds.min[0] + mapBounds.max[0]) / 2,
                    (mapBounds.min[1] + mapBounds.max[1]) / 2
                ];

                const p1 = this.toMap.forward([bounds.min[0], bounds.min[1]]);
                const p2 = this.toMap.forward([bounds.max[0], bounds.min[1]]);
                const p3 = this.toMap.forward([bounds.max[0], bounds.max[1]]);
                const p4 = this.toMap.forward([bounds.min[0], bounds.max[1]]);

                // let feature = new Feature({
                //	'geometry': new LineString([p1, p2, p3, p4, p1])
                // });
                let feature: any = new Feature({
                    geometry: new Polygon([[p1, p2, p3, p4, p1]]),
                });

                // @ts-ignore
                feature.source = source;
                // @ts-ignore
                feature.pointcloud = pointcloud;
                this.getSourcesLayer().getSource().addFeature(feature);

                feature = new Feature({
                    geometry: new Point(mapCenter),
                    name: name
                });
                feature.setStyle(this.createLabelStyle(name));
                this.sourcesLabelLayer!.getSource().addFeature(feature);
            }
        }).catch(() => {
            //
        });
    }

    toggle() {
        // if (this.elMap.is(':visible')) {
        //     this.elMap.css('display', 'none');
        //     this.enabled = false;
        // } else {
        //     this.elMap.css('display', 'block');
        //     this.enabled = true;
        // }
    }

    update() {
        if (!this.sceneProjection || this.enabled) {
            return;
        }

        const pm = document.querySelector('#ol_map');

        if (!pm) {
            return;
        }

        // resize
        const mapSize = this.map.getSize();

        if (!mapSize) {
            return;
        }

        const resized = (pm.clientWidth !== mapSize[0] || pm.clientHeight !== mapSize[1]);
        if (resized) {
            this.map.updateSize();
        }

        const camera = this.viewer.scene.getActiveCamera();

        const scale = this.map.getView().getResolution();

        if (!scale) {
            return;
        }

        const campos = camera.position;
        const camDir = camera.getWorldDirection(new Vector3());
        const sceneLookAt = camDir.clone().multiplyScalar(30 * scale).add(campos);
        const geoPos = camera.position;
        const geoLookAt = sceneLookAt;
        const mapPos = new Vector2().fromArray(this.toMap.forward([geoPos.x, geoPos.y]));
        let mapLookAt = new Vector2().fromArray(this.toMap.forward([geoLookAt.x, geoLookAt.y]));
        const mapDir = new Vector2().subVectors(mapLookAt, mapPos).normalize();
        mapLookAt = mapPos.clone().add(mapDir.clone().multiplyScalar(30 * scale));
        const mapLength = mapPos.distanceTo(mapLookAt);
        const mapSide = new Vector2(-mapDir.y, mapDir.x);

        const p1 = mapPos.toArray();
        const p2 = mapLookAt.clone().sub(mapSide.clone().multiplyScalar(0.3 * mapLength)).toArray();
        const p3 = mapLookAt.clone().add(mapSide.clone().multiplyScalar(0.3 * mapLength)).toArray();

        this.gCamera.setCoordinates([p1, p2, p3, p1]);
    }

    get sourcesVisible() {
        return this.getSourcesLayer().getVisible();
    }

    set sourcesVisible(value) {
        this.getSourcesLayer().setVisible(value);
    }
}
