import {PointSizeType} from "../core/Defines";
import {Viewer} from "./viewer";
import {Profile} from "../utils/Profile";

function createPointcloudData(pointcloud: any) {
    let material: any = pointcloud.material;

    let ranges = [];

    for (let [name, value] of material.ranges) {
        ranges.push({
            name: name,
            value: value,
        });
    }

    if (typeof material.elevationRange[0] === "number") {
        ranges.push({
            name: "elevationRange",
            value: material.elevationRange,
        });
    }
    if (typeof material.intensityRange[0] === "number") {
        ranges.push({
            name: "intensityRange",
            value: material.intensityRange,
        });
    }

    // @ts-ignore
    let pointSizeTypeName = Object.entries(PointSizeType).find(e => e[1] === material.pointSizeType)[0];

    let jsonMaterial = {
        activeAttributeName: material.activeAttributeName,
        ranges: ranges,
        size: material.size,
        minSize: material.minSize,
        pointSizeType: pointSizeTypeName,
        matcap: material.matcap,
    };

    return {
        name: pointcloud.name,
        url: pointcloud.pcoGeometry.url,
        position: pointcloud.position.toArray(),
        rotation: pointcloud.rotation.toArray(),
        scale: pointcloud.scale.toArray(),
        material: jsonMaterial,
    };
}

function createProfileData(profile: Profile) {
    return {
        uuid: profile.uuid,
        name: profile.name,
        points: profile.points.map(p => p.toArray()),
        height: profile.height,
        width: profile.width,
    };
}

function createVolumeData(volume: any) {
    return {
        uuid: volume.uuid,
        type: volume.constructor.name,
        name: volume.name,
        position: volume.position.toArray(),
        rotation: volume.rotation.toArray(),
        scale: volume.scale.toArray(),
        visible: volume.visible,
        clip: volume.clip,
    };
}

function createCameraAnimationData(animation: any) {
    const controlPoints = animation.controlPoints.map((cp: any) => {
        return {
            position: cp.position.toArray(),
            target: cp.target.toArray(),
        };
    });

    return {
        uuid: animation.uuid,
        name: animation.name,
        duration: animation.duration,
        t: animation.t,
        curveType: animation.curveType,
        visible: animation.visible,
        controlPoints: controlPoints,
    };
}

function createMeasurementData(measurement: any) {
    return {
        uuid: measurement.uuid,
        name: measurement.name,
        points: measurement.points.map((p: any) => p.position.toArray()),
        showDistances: measurement.showDistances,
        showCoordinates: measurement.showCoordinates,
        showArea: measurement.showArea,
        closed: measurement.closed,
        showAngles: measurement.showAngles,
        showHeight: measurement.showHeight,
        showCircle: measurement.showCircle,
        showAzimuth: measurement.showAzimuth,
        showEdges: measurement.showEdges,
        color: measurement.color.toArray(),
    };
}

function createOrientedImagesData(images: any) {
    return {
        cameraParamsPath: images.cameraParamsPath,
        imageParamsPath: images.imageParamsPath,
    };
}

function createGeopackageData(geopackage: any) {
    return {
        path: geopackage.path,
    };
}

function createAnnotationData(annotation: any) {
    const data: any = {
        uuid: annotation.uuid,
        title: annotation.title.toString(),
        description: annotation.description,
        position: annotation.position.toArray(),
        offset: annotation.offset.toArray(),
        children: [],
    };

    if (annotation.cameraPosition) {
        data.cameraPosition = annotation.cameraPosition.toArray();
    }

    if (annotation.cameraTarget) {
        data.cameraTarget = annotation.cameraTarget.toArray();
    }

    if (typeof annotation.radius !== "undefined") {
        data.radius = annotation.radius;
    }

    return data;
}

function createAnnotationsData(viewer: Viewer) {
    const map = new Map();

    viewer.scene.annotations.traverseDescendants((a: any) => {
        const aData = createAnnotationData(a);

        map.set(a, aData);
    });

    // @ts-ignore
    for (const [annotation, data] of map) {
        for (const child of annotation.children) {
            const childData = map.get(child);
            data.children.push(childData);
        }
    }

    return viewer.scene.annotations.children.map(a => map.get(a));
}

function createSettingsData(viewer: Viewer) {
    return {
        pointBudget: viewer.getPointBudget(),
        fov: viewer.getFOV(),
        edlEnabled: viewer.getEDLEnabled(),
        edlRadius: viewer.getEDLRadius(),
        edlStrength: viewer.getEDLStrength(),
        background: viewer.getBackground(),
        minNodeSize: viewer.getMinNodeSize(),
        showBoundingBoxes: viewer.getShowBoundingBox(),
    };
}

// function createSceneContentData(viewer: Viewer) {
//     const data = [];
//
//     const potreeObjects: any[] = [];
//
//     viewer.scene.scene.traverse((node: any) => {
//         if (node.potree) {
//             potreeObjects.push(node);
//         }
//     });
//
//     for (const object of potreeObjects) {
//         if (object.potree.file) {
//             const saveObject = {
//                 file: object.potree.file,
//             };
//
//             data.push(saveObject);
//         }
//     }
//
//     return data;
// }

function createViewData(viewer: Viewer) {
    const view = viewer.scene.view;

    return {
        position: view.position.toArray(),
        target: view.getPivot().toArray(),
    };
}

function createClassificationData(viewer: Viewer) {
    return viewer.classifications;
}

export function saveProject(viewer: Viewer) {
    const scene = viewer.scene;

    return {
        type: "II",
        version: 1.0,
        settings: createSettingsData(viewer),
        view: createViewData(viewer),
        classification: createClassificationData(viewer),
        pointclouds: scene.pointClouds.map(createPointcloudData),
        measurements: scene.measurements.map(createMeasurementData),
        volumes: scene.volumes.map(createVolumeData),
        cameraAnimations: scene.cameraAnimations.map(createCameraAnimationData),
        profiles: scene.profiles.map(createProfileData),
        annotations: createAnnotationsData(viewer),
        orientedImages: scene.orientedImages.map(createOrientedImagesData),
        geopackages: scene.geoPackages.map(createGeopackageData),
        // objects: createSceneContentData(viewer),
    };
}
