import {Clock, Matrix4, Object3D, OrthographicCamera, PerspectiveCamera, REVISION, Scene as ThreeScene, Sphere, Vector2, Vector3, Vector4, WebGLRenderer} from 'three';
import {BackgroundType, CameraMode, ClipMethod, ClipTask, ElevationGradientRepeat, LengthUnits, ViewType} from "../core/Defines";
import {CRenderer} from "../core/CRenderer";
import {VRenderer} from "./VRenderer";
import {EDLRenderer} from "./EDLRenderer";
import {HQSplatRenderer} from "./HQSplatRenderer";
import {ClippingTool} from "../utils/ClippingTool";
import {TransformationTool} from "../utils/TransformationTool";
import {Utils} from "../core/Utils";
import {MapView} from "./map";
import {BoxVolume} from "../utils/Volume";
import {Features} from "../core/Features";
import {Message} from "../utils/Message";
import {AnnotationTool} from "../utils/AnnotationTool";
import {MeasuringTool} from "../utils/MeasuringTool";
import {ProfileTool} from "../utils/ProfileTool";
import {VolumeTool} from "../utils/VolumeTool";
import {InputHandler} from "../navigation/InputHandler";
import {NavigationCube} from "./NavigationCube";
import {OrbitControls} from "../navigation/OrbitControls";
import {FirstPersonControls} from "../navigation/FirstPersonControls";
import {EarthControls} from "../navigation/EarthControls";
import {DeviceOrientationControls} from "../navigation/DeviceOrientationControls";
import {VRControls} from "../navigation/VRControls";
import {EventDispatcher} from "../core/EventDispatcher";
import {ClassificationScheme} from "../materials/ClassificationScheme";
// import {VRButton} from "../extensions/VRButton";
import Stats from "three/examples/jsm/libs/stats.module";
import {Easing, Tween, update as TweenUpdate} from "@tweenjs/tween.js";
import proj4 from "proj4";
import {register} from 'ol/proj/proj4';
import {ProfileWindow} from "./profile";
import {VScene} from "./VScene";
import {updatePointClouds} from "../core/CUpdateVisibility";
import {App} from "../core/App";
import {AnnotationEvent, CanvasEventName, DocumentEventName, MouseEventName, TR_PointCloudAdded, TR_SceneChanged, TR_VolumeRemoved, ViewerEventName} from "../core/Event";
import {Annotation} from "../core/Annotation";
import {PointCloudOctree} from "../core/PointCloudOctree";
import {WebGLExtensions} from "../extensions/WebGLExtensions";
import {Cartesian3, Cartographic, PerspectiveFrustum, Viewer as CesiumViewer} from "cesium";
import {isMobile} from "react-device-detect";

type ArgViewer = {
    onPointCloudLoaded?: Function,
    noDragAndDrop?: boolean,
    isStats?: boolean,
    useDefaultRenderLoop?: boolean,
}

register(proj4);

export class Viewer extends EventDispatcher {
    renderArea: HTMLDivElement;
    guiLoaded: boolean;
    guiLoadTasks: any[];
    onVrListeners: any[];
    messages: Message[];
    // pointCloudLoadedCallback: Function;
    server: any;
    fov: number;
    isFlipYZ: boolean;
    useDEMCollisions: boolean;
    generateDEM: boolean;
    minNodeSize: number;
    edlStrength: number;
    edlRadius: number;
    edlOpacity: number;
    useEDL: boolean;
    description: string;
    classifications: any;
    moveSpeed: number;
    lengthUnit: any;
    lengthUnitDisplay: any;
    showBoundingBox: boolean;
    showAnnotations: boolean;
    freeze: boolean;
    clipTask: number;
    clipMethod: number;
    elevationGradientRepeat: number;
    filterReturnNumberRange: number[];
    filterNumberOfReturnsRange: number[];
    filterGPSTimeRange: number[];
    filterPointSourceIDRange: number[];
    vRenderer: VRenderer;
    edlRenderer: EDLRenderer;
    renderer: WebGLRenderer;
    pRenderer: CRenderer;
    scene: VScene;
    sceneVR: ThreeScene;
    overlay: ThreeScene;
    overlayCamera: OrthographicCamera;
    inputHandler: InputHandler;
    controls: any;
    clippingTool: ClippingTool;
    transformationTool: TransformationTool;
    navigationCube: NavigationCube;
    skybox: any;
    clock: Clock;
    background: string;
    stats: Stats;
    shadowTestCam: PerspectiveCamera;
    orbitControls: OrbitControls;
    scaleFactor: number;
    annotationTool: AnnotationTool;
    measuringTool: MeasuringTool;
    profileTool: ProfileTool;
    volumeTool: VolumeTool;
    onAnnotationAdded: (e: any) => void;
    vrControls: VRControls;
    fpControls: FirstPersonControls;
    earthControls: EarthControls;
    deviceControls: DeviceOrientationControls;
    mapView: MapView;
    profileWindow: ProfileWindow;
    // profileWindowController: ProfileWindowController;
    visibleAnnotations: any;
    useHQ: boolean;
    hqRenderer: HQSplatRenderer;
    toggle: number;
    cesiumViewer: CesiumViewer;
    isMap: boolean;
    protected _destroy: boolean = false;
    protected _previousCamera: Object3D;

    // private __idRAF: number;

    constructor(domElement: HTMLDivElement, args?: ArgViewer) {
        super();

        this.renderArea = domElement;
        this.guiLoaded = false;
        this.guiLoadTasks = [];
        this.onVrListeners = [];
        this.messages = [];
        this.isMap = true;

        // this.elMessages = $(`<div id="message_listing" style="position: absolute; z-index: 1000; left: 10px; bottom: 10px"></div>`);
        //
        // $(domElement).append(this.elMessages);

        try {
            // // generate missing dom hierarchy
            // if ($(domElement).find('#ol_map').length === 0) {
            //     const olMap = $(`
            //             <div id="ol_map" class="map-box">
            //                 <div id="ol_map_header" class="map-box-header">Map Box</div>
            //                 <div id="ol_map_content" class="map-box-content map"></div>
            //             </div>
            //         `);
            //
            //     $(domElement).append(olMap);
            // }
            //

            if (!domElement.querySelector('#AnnotationContainer')) {
                const elAnnotationContainer = document.createElement('div');
                elAnnotationContainer.id = 'AnnotationContainer';
                elAnnotationContainer.style.position = 'absolute';
                elAnnotationContainer.style.zIndex = '100000';
                elAnnotationContainer.style.width = '100%';
                elAnnotationContainer.style.height = '100%';
                elAnnotationContainer.style.pointerEvents = 'none';

                domElement.append(elAnnotationContainer);
            }

            // if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            // 	defaultSettings.navigation = "Orbit";
            // }

            this.server = null;

            this.fov = 60;
            this.isFlipYZ = false;
            this.useDEMCollisions = false;
            this.generateDEM = false;
            this.minNodeSize = 30;
            this.edlStrength = 1.0;
            this.edlRadius = 1.4;
            this.edlOpacity = 1.0;
            this.useEDL = false;
            this.description = "";

            this.classifications = ClassificationScheme.DEFAULT;

            this.moveSpeed = 10;

            this.lengthUnit = LengthUnits.METER;
            this.lengthUnitDisplay = LengthUnits.METER;

            this.showBoundingBox = false;
            this.showAnnotations = true;
            this.freeze = false;
            this.clipTask = ClipTask.HIGHLIGHT;
            this.clipMethod = ClipMethod.INSIDE_ANY;

            this.elevationGradientRepeat = ElevationGradientRepeat.CLAMP;

            this.filterReturnNumberRange = [0, 7];
            this.filterNumberOfReturnsRange = [0, 7];
            this.filterGPSTimeRange = [-Infinity, Infinity];
            this.filterPointSourceIDRange = [0, 65535];

            // this.iiRenderer = null;
            // this.edlRenderer = null;
            // this.renderer = null;
            // this.pRenderer = null;

            // this.scene = null;
            // this.sceneVR = null;
            // this.overlay = null;
            // this.overlayCamera = null;

            // this.inputHandler = null;
            // this.controls = null;

            // this.clippingTool = null;
            // this.transformationTool = null;
            // this.navigationCube = null;
            // this.compass = null;

            this.skybox = null;
            this.clock = new Clock();
            // this.background = null;

            this.initThree();

            if (args && args.hasOwnProperty('isStats') && args.isStats && typeof Stats !== "undefined") {
                this.stats = Stats();
                this.stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
                this.stats.dom.style.left = 'auto';
                this.stats.dom.style.right = '1rem';
                this.stats.dom.style.top = 'auto';
                this.stats.dom.style.bottom = '1rem';
                document.body.appendChild(this.stats.dom);
            }

            {
                const canvas = this.renderer.domElement;

                canvas.addEventListener(CanvasEventName.WebglContextLost, () => {
                    const gl = this.renderer.getContext();

                    console.error("WebGL context lost. \u2639", gl.getError());
                }, false);
            }

            this.overlay = new ThreeScene();

            // @ts-ignore
            this.overlayCamera = new OrthographicCamera(0, 1, 1, 0, -1000, 1000);

            this.pRenderer = new CRenderer(this.renderer);

            {
                const near = 2.5;
                const far = 10.0;
                const fov = 90;

                // @ts-ignore
                this.shadowTestCam = new PerspectiveCamera(fov, 1, near, far);
                this.shadowTestCam.position.set(3.50, -2.80, 8.561);
                this.shadowTestCam.lookAt(new Vector3(0, 0, 4.87));
            }

            const scene = new VScene();

            // create VR scene
            this.sceneVR = new ThreeScene();

            // let texture = new TextureLoader().load(`${App.resourcePath}/images/vr_controller_help.jpg`);

            // let plane = new PlaneBufferGeometry(1, 1, 1, 1);
            // let infoMaterial = new MeshBasicMaterial({map: texture});
            // let infoNode = new Mesh(plane, infoMaterial);
            // infoNode.position.set(-0.5, 1, 0);
            // infoNode.scale.set(0.4, 0.3, 1);
            // infoNode.lookAt(0, 1, 0)
            // this.sceneVR.add(infoNode);

            // window.infoNode = infoNode;

            this.setScene(scene);

            {
                this.inputHandler = new InputHandler(this);
                this.inputHandler.setScene(this.scene);

                this.clippingTool = new ClippingTool(this);
                this.transformationTool = new TransformationTool(this);
                this.navigationCube = new NavigationCube(this);
                this.navigationCube.visible = !isMobile;

                this.createControls();

                this.clippingTool.setScene(this.scene);

                const onPointcloudAdded = (e: TR_PointCloudAdded) => {
                    if (this.scene.pointClouds.length === 1) {
                        const speed = e.pointcloud.boundingBox.getSize(new Vector3()).length();

                        this.setMoveSpeed(speed / 5);
                    }
                };

                const onVolumeRemoved = (e: TR_VolumeRemoved) => {
                    this.inputHandler.deselect(e.volume);
                };

                this.addEventListener(ViewerEventName.SceneChanged, (e: TR_SceneChanged) => {
                    this.inputHandler.setScene(e.scene);
                    this.clippingTool.setScene(this.scene);

                    if (!e.scene.hasEventListener(ViewerEventName.PointCloudAdded, onPointcloudAdded)) {
                        e.scene.addEventListener(ViewerEventName.PointCloudAdded, onPointcloudAdded);
                    }

                    if (!e.scene.hasEventListener(ViewerEventName.VolumeRemoved, onPointcloudAdded)) {
                        e.scene.addEventListener(ViewerEventName.VolumeRemoved, onVolumeRemoved);
                    }
                });

                this.scene.addEventListener(ViewerEventName.VolumeRemoved, onVolumeRemoved);
                this.scene.addEventListener(ViewerEventName.PointCloudAdded, onPointcloudAdded);
            }

            // set defaults
            this.setFOV(60);
            this.setEDLEnabled(false);
            this.setEDLRadius(1.4);
            this.setEDLStrength(0.4);
            this.setEDLOpacity(1.0);
            this.setClipTask(ClipTask.HIGHLIGHT);
            this.setClipMethod(ClipMethod.INSIDE_ANY);
            this.setPointBudget(1000 * 1000);
            this.setShowBoundingBox(false);
            this.setFreeze(false);
            this.setControls(this.orbitControls);
            // this.setBackground(BackgroundType.Gradient);

            this.scaleFactor = 1;

            this.loadSettingsFromURL();

            // start rendering!
            // this.__idRAF = requestAnimationFrame(this._loop);
            this.renderer.setAnimationLoop(this._loop);

            document.addEventListener(DocumentEventName.VisibilityChange, this._onVisibilityChange)

            this.annotationTool = new AnnotationTool(this);
            this.measuringTool = new MeasuringTool(this);
            this.profileTool = new ProfileTool(this);
            this.volumeTool = new VolumeTool(this);
        } catch (e) {
            this._onCrash(e);
        }
    }

    protected _onVisibilityChange = () => {
        if (document.hidden) {
            // if (this.__idRAF) cancelAnimationFrame(this.__idRAF);
            this.renderer.setAnimationLoop(null);
        } else {
            // this.__idRAF = requestAnimationFrame(this._loop);
            this.renderer.setAnimationLoop(this._loop);
        }
    }

    protected _onCrash(error: any) {
        console.error('Viewer.onCrash', error);

        // $(this.renderArea).empty();
        //
        // if ($(this.renderArea).find('#ii_failpage').length === 0) {
        //     let elFailPage = $(`
        //         <div id="#ii_failpage" class="ii_failpage">
        //             <h1>I&I 3D Encountered An Error </h1>
        //             <p>
        //                 This may happen if your browser or graphics card is not supported.
        //                 <br>
        //                 We recommend to use
        //                 <a href="https://www.google.com/chrome/browser" target="_blank" style="color:initial">Chrome</a>
        //                 or
        //                 <a href="https://www.mozilla.org/" target="_blank">Firefox</a>.
        //             </p>
        //             <p>
        //                 Please also visit <a href="https://webglreport.com/" target="_blank">WebGL Report</a> and
        //                 check whether your system supports WebGL.
        //             </p>
        //             <p>
        //                 If you are already using one of the recommended browsers and WebGL is enabled,<br>
        //                 including your operating system, graphics card, browser and browser version, as well as the
        //                 error message below.<br>
        //                 Please do not report errors on unsupported browsers.
        //             </p>
        //         </div>`
        //     );
        //
        //     $(this.renderArea).append(elFailPage);
        // }
        //
        // throw error;
    }

    // ------------------------------------------------------------------------------------
    // Viewer API
    // ------------------------------------------------------------------------------------

    setScene(scene: VScene) {
        if (scene === this.scene) {
            return;
        }

        const oldScene = this.scene;
        this.scene = scene;

        this.dispatchEvent({
            type: ViewerEventName.SceneChanged,
            oldScene: oldScene,
            scene: scene
        });

        // Annotations
        this.renderArea.querySelectorAll('.annotation').forEach(item => item.remove()); // detach

        // for(let annotation of this.scene.annotations){
        //	this.renderArea.appendChild(annotation.domElement[0]);
        // }

        this.scene.annotations.traverse((annotation: Annotation) => {
            this.renderArea.appendChild(annotation.domElement);
        });

        if (!this.onAnnotationAdded) {
            this.onAnnotationAdded = (e) => {
                e.annotation.traverse((node: Annotation) => {
                    const elAnnotationContainer = this.renderArea.querySelector('#AnnotationContainer');

                    if (elAnnotationContainer) {
                        elAnnotationContainer.append(node.domElement);
                    }

                    node.scene = this.scene;
                });
            };
        }

        if (oldScene) {
            oldScene.annotations.removeEventListener(AnnotationEvent.AnnotationAdded, this.onAnnotationAdded);
        }

        this.scene.annotations.addEventListener(AnnotationEvent.AnnotationAdded, this.onAnnotationAdded);
    }

    setControls(controls: any) {
        if (controls !== this.controls) {
            if (this.controls) {
                this.controls.enabled = false;
                this.inputHandler.removeInputListener(this.controls);
            }

            this.controls = controls;
            this.controls.enabled = true;
            this.inputHandler.addInputListener(this.controls);
        }
    }

    getControls() {
        if (this.renderer.xr.isPresenting) {
            return this.vrControls;
        } else {
            return this.controls;
        }

    }

    getMinNodeSize() {
        return this.minNodeSize;
    };

    setMinNodeSize(value: number) {
        if (this.minNodeSize !== value) {
            this.minNodeSize = value;
            this.dispatchEvent({
                type: ViewerEventName.MinNodeSizeChanged,
                viewer: this
            });
        }
    };

    getBackground() {
        return this.background;
    }

    setBackground(bg: string) {
        if (this.background === bg) {
            return;
        }

        if (bg === "skybox") {
            this.skybox = Utils.loadSkybox(new URL(App.getAsset('resources/textures/skybox2/')).href);
        }

        this.background = bg;
        this.dispatchEvent({
            type: ViewerEventName.BackgroundChanged,
            viewer: this,
            background: bg,
        });
    }

    setShowBoundingBox(value: boolean) {
        if (this.showBoundingBox !== value) {
            this.showBoundingBox = value;
            this.dispatchEvent({
                type: ViewerEventName.ShowBoundingBoxChanged,
                viewer: this
            });
        }
    };

    getShowBoundingBox() {
        return this.showBoundingBox;
    };

    setMoveSpeed(value: number) {
        if (this.moveSpeed !== value) {
            this.moveSpeed = value;
            this.dispatchEvent({
                type: ViewerEventName.MoveSpeedChanged,
                viewer: this, 'speed': value
            });
        }
    };

    getMoveSpeed() {
        return this.moveSpeed;
    };

    setWeightClassification(w: number) {
        for (let i = 0; i < this.scene.pointClouds.length; i++) {
            this.scene.pointClouds[i].material.weightClassification = w;
            this.dispatchEvent({
                type: ViewerEventName.AttributeWeightsChanged + i,
                viewer: this
            });
        }
    };

    setFreeze(value: boolean) {
        value = Boolean(value);

        if (this.freeze !== value) {
            this.freeze = value;
            this.dispatchEvent({
                type: ViewerEventName.FreezeChanged,
                viewer: this
            });
        }
    };

    getFreeze() {
        return this.freeze;
    };

    getClipTask() {
        return this.clipTask;
    }

    getClipMethod() {
        return this.clipMethod;
    }

    setClipTask(value: number) {
        if (this.clipTask !== value) {

            this.clipTask = value;

            this.dispatchEvent({
                type: ViewerEventName.ClipTaskChanged,
                viewer: this
            });
        }
    }

    setClipMethod(value: number) {
        if (this.clipMethod !== value) {

            this.clipMethod = value;

            this.dispatchEvent({
                type: ViewerEventName.ClipMethodChanged,
                viewer: this
            });
        }
    }

    setElevationGradientRepeat(value: number) {
        if (this.elevationGradientRepeat !== value) {

            this.elevationGradientRepeat = value;

            this.dispatchEvent({
                type: ViewerEventName.ElevationGradientRepeatChanged,
                viewer: this
            });
        }
    }

    setPointBudget(value: number) {
        if (App.pointBudget !== value) {
            App.pointBudget = value;

            this.dispatchEvent({
                type: ViewerEventName.PointBudgetChanged,
                viewer: this
            });
        }
    };

    getPointBudget() {
        return App.pointBudget;
    };

    setShowAnnotations(value: boolean) {
        if (this.showAnnotations !== value) {
            this.showAnnotations = value;
            this.dispatchEvent({
                type: ViewerEventName.ShowAnnotationsChanged,
                viewer: this
            });
        }
    }

    getShowAnnotations() {
        return this.showAnnotations;
    }

    setDEMCollisionsEnabled(value: boolean) {
        if (this.useDEMCollisions !== value) {
            this.useDEMCollisions = value;
            this.dispatchEvent({
                type: ViewerEventName.UseDemCollisionsChanged,
                viewer: this
            });
        }
    };

    getDEMCollisionsEnabled() {
        return this.useDEMCollisions;
    };

    setEDLEnabled(value: boolean) {
        value = Boolean(value) && Features!.SHADER_EDL.isSupported();

        if (this.useEDL !== value) {
            this.useEDL = value;
            this.dispatchEvent({
                type: ViewerEventName.UseEdlChanged,
                viewer: this
            });
        }
    };

    getEDLEnabled() {
        return this.useEDL;
    };

    setEDLRadius(value: number) {
        if (this.edlRadius !== value) {
            this.edlRadius = value;
            this.dispatchEvent({
                type: ViewerEventName.EdlRadiusChanged,
                viewer: this
            });
        }
    };

    getEDLRadius() {
        return this.edlRadius;
    };

    setEDLStrength(value: number) {
        if (this.edlStrength !== value) {
            this.edlStrength = value;
            this.dispatchEvent({
                type: ViewerEventName.EdlStrengthChanged,
                viewer: this
            });
        }
    };

    getEDLStrength() {
        return this.edlStrength;
    };

    setEDLOpacity(value: number) {
        if (this.edlOpacity !== value) {
            this.edlOpacity = value;
            this.dispatchEvent({
                type: ViewerEventName.EdlOpacityChanged,
                viewer: this
            });
        }
    };

    getEDLOpacity() {
        return this.edlOpacity;
    };

    setPointSize(number: number) {
        // todo: temp
        return number;
    }

    setOpacity(number: number) {
        // todo: temp
        return number;
    }

    setMaterial(material: string) {
        // todo: temp
        return material;
    }

    setPointSizing(sizing: string) {
        // todo: temp
        return sizing;
    }

    setQuality(quality: string) {
        // todo: temp
        return quality;
    }

    setFOV(value: number) {
        if (this.fov !== value) {
            this.fov = value;
            this.dispatchEvent({
                type: ViewerEventName.FovChanged,
                viewer: this
            });
        }
    };

    getFOV() {
        return this.fov;
    };

    disableAnnotations() {
        this.scene.annotations.traverse((annotation: Annotation) => {
            annotation.domElement.style.pointerEvents = 'none';
        });
    };

    enableAnnotations() {
        this.scene.annotations.traverse((annotation: Annotation) => {
            annotation.domElement.style.pointerEvents = 'auto';
        });
    }

    setClassifications(classifications: any) {
        this.classifications = classifications;

        this.dispatchEvent({
            type: ViewerEventName.ClassificationsChanged,
            viewer: this
        });
    }

    setClassificationVisibility(key: any, value: any) {
        if (!this.classifications[key]) {
            this.classifications[key] = {visible: value, name: 'no name'};
            this.dispatchEvent({
                type: ViewerEventName.ClassificationVisibilityChanged,
                viewer: this
            });
        } else if (this.classifications[key].visible !== value) {
            this.classifications[key].visible = value;
            this.dispatchEvent({
                type: ViewerEventName.ClassificationVisibilityChanged,
                viewer: this
            });
        }
    }

    // toggleAllClassificationsVisibility() {
    //     let numVisible = 0;
    //     let numItems = 0;
    //
    //     for (const key of Object.keys(this.classifications)) {
    //         if (this.classifications[key].visible) {
    //             numVisible++;
    //         }
    //         numItems++;
    //     }
    //
    //     let visible = true;
    //     if (numVisible === numItems) {
    //         visible = false;
    //     }
    //
    //     let somethingChanged = false;
    //
    //     for (const key of Object.keys(this.classifications)) {
    //         if (this.classifications[key].visible !== visible) {
    //             this.classifications[key].visible = visible;
    //             somethingChanged = true;
    //         }
    //     }
    //
    //     if (somethingChanged) {
    //         this.dispatchEvent({
    //             type: ViewerEventName.ClassificationVisibilityChanged,
    //             viewer: this
    //         });
    //     }
    // }

    setFilterReturnNumberRange(from: any, to: any) {
        this.filterReturnNumberRange = [from, to];
        this.dispatchEvent({
            type: ViewerEventName.FilterReturnNumberRangeChanged,
            viewer: this
        });
    }

    setFilterNumberOfReturnsRange(from: any, to: any) {
        this.filterNumberOfReturnsRange = [from, to];
        this.dispatchEvent({
            type: ViewerEventName.FilterNumberOfReturnsRangeChanged,
            viewer: this
        });
    }

    setFilterGPSTimeRange(from: any, to: any) {
        this.filterGPSTimeRange = [from, to];
        this.dispatchEvent({
            type: ViewerEventName.FilterGpsTimeRangeChanged,
            viewer: this
        });
    }

    setFilterPointSourceIDRange(from: any, to: any) {
        this.filterPointSourceIDRange = [from, to]
        this.dispatchEvent({
            type: ViewerEventName.FilterPointSourceIdRangeChanged,
            viewer: this
        });
    }

    setLengthUnit(value: any) {
        switch (value) {
            case 'm':
                this.lengthUnit = LengthUnits.METER;
                this.lengthUnitDisplay = LengthUnits.METER;
                break;
            case 'ft':
                this.lengthUnit = LengthUnits.FEET;
                this.lengthUnitDisplay = LengthUnits.FEET;
                break;
            case 'in':
                this.lengthUnit = LengthUnits.INCH;
                this.lengthUnitDisplay = LengthUnits.INCH;
                break;
        }

        this.dispatchEvent({
            type: ViewerEventName.LengthUnitChanged,
            viewer: this, value: value
        });
    };

    setLengthUnitAndDisplayUnit(lengthUnitValue: string, lengthUnitDisplayValue: string) {
        switch (lengthUnitValue) {
            case 'm':
                this.lengthUnit = LengthUnits.METER;
                break;
            case 'ft':
                this.lengthUnit = LengthUnits.FEET;
                break;
            case 'in':
                this.lengthUnit = LengthUnits.INCH;
                break;
        }

        switch (lengthUnitDisplayValue) {
            case 'm':
                this.lengthUnitDisplay = LengthUnits.METER;
                break;
            case 'ft':
                this.lengthUnitDisplay = LengthUnits.FEET;
                break;
            case 'in':
                this.lengthUnitDisplay = LengthUnits.INCH;
                break;
        }

        this.dispatchEvent({
            type: ViewerEventName.LengthUnitChanged,
            viewer: this, value: lengthUnitValue
        });
    };

    zoomTo = (node: Object3D | any, factor: number, animationDuration: number = 0) => {
        const view = this.scene.view;

        const camera = this.scene.cameraP.clone();
        camera.rotation.copy(this.scene.cameraP.rotation);
        camera.rotation.order = "ZXY";
        camera.rotation.x = Math.PI / 2 + view.pitch;
        camera.rotation.z = view.yaw;
        camera.updateMatrix();
        camera.updateMatrixWorld();
        // @ts-ignore
        camera.zoomTo(node, factor);

        let bs;
        if (node.boundingSphere) {
            bs = node.boundingSphere;
        } else if (node.geometry && node.geometry.boundingSphere) {
            bs = node.geometry.boundingSphere;
        } else {
            bs = node.boundingBox.getBoundingSphere(new Sphere());
        }
        bs = bs.clone().applyMatrix4(node.matrixWorld);

        const startPosition = view.position.clone();
        const endPosition = camera.position.clone();
        const startTarget = view.getPivot();
        const endTarget = bs.center;
        // todo: temp
        // const startRadius = view.radius;
        // todo: temp
        // const endRadius = endPosition.distanceTo(endTarget);

        const easing = Easing.Quartic.Out;

        { // animate camera position
            const pos = startPosition.clone();
            const tween = new Tween(pos).to(endPosition, animationDuration);
            tween.easing(easing);

            tween.onUpdate(() => {
                view.position.copy(pos);
            });

            tween.start();
        }

        { // animate camera target
            const target = startTarget.clone();
            const tween = new Tween(target).to(endTarget, animationDuration);
            tween.easing(easing);
            tween.onUpdate(() => {
                view.lookAt(target);
            });
            tween.onComplete(() => {
                view.lookAt(target);
                this.dispatchEvent({
                    type: ViewerEventName.FocusingFinished,
                    target: this
                });
            });

            this.dispatchEvent({
                type: ViewerEventName.FocusingStarted,
                target: this
            });

            tween.start();
        }
    };

    moveToGpsTimeVicinity(time: number) {
        const result = Utils.findClosestGpsTime(time, this);

        const box = result.node.pointcloud.deepestNodeAt(result.position).getBoundingBox();
        const diameter = box.min.distanceTo(box.max);

        const camera = this.scene.getActiveCamera();
        const offset = camera.getWorldDirection(new Vector3()).multiplyScalar(diameter);
        const newCamPos = result.position.clone().sub(offset);

        this.scene.view.position.copy(newCamPos);
        this.scene.view.lookAt(result.position);
    }

    showAbout() {
        // $(function () {
        //     $('#about-panel').dialog();
        // });
    };

    getBoundingBox(pointClouds?: PointCloudOctree[]) {
        return this.scene.getBoundingBox(pointClouds);
    };

    getGpsTimeExtent() {
        const range = [Infinity, -Infinity];

        for (const pointcloud of this.scene.pointClouds) {
            const attributes = pointcloud.pcoGeometry.pointAttributes.attributes;
            const aGpsTime = attributes.find((a: any) => a.name === "gps-time");

            if (aGpsTime) {
                range[0] = Math.min(range[0], aGpsTime.range[0]);
                range[1] = Math.max(range[1], aGpsTime.range[1]);
            }
        }

        return range;
    }

    fitToScreen(factor: number = 1, animationDuration: number = 0) {
        const box = this.getBoundingBox(this.scene.pointClouds);
        const node = new Object3D();

        (node as any).boundingBox = box;

        this.zoomTo(node, factor, animationDuration);
        this.controls.stop();
    };

    toggleNavigationCube() {
        this.navigationCube.visible = !this.navigationCube.visible;
    }

    setView(view: string) {
        if (!view) return;

        switch (view) {
            case ViewType.Front:
                this.setFrontView();
                break;
            case ViewType.Back:
                this.setBackView();
                break;
            case ViewType.Left:
                this.setLeftView();
                break;
            case ViewType.Right:
                this.setRightView();
                break;
            case ViewType.Top:
                this.setTopView();
                break;
            case ViewType.Bottom:
                this.setBottomView();
                break;
        }
    }

    setTopView() {
        this.scene.view.yaw = 0;
        this.scene.view.pitch = -Math.PI / 2;

        this.fitToScreen();
    };

    setBottomView() {
        this.scene.view.yaw = -Math.PI;
        this.scene.view.pitch = Math.PI / 2;

        this.fitToScreen();
    };

    setFrontView() {
        this.scene.view.yaw = 0;
        this.scene.view.pitch = 0;

        this.fitToScreen();
    };

    setBackView() {
        this.scene.view.yaw = Math.PI;
        this.scene.view.pitch = 0;

        this.fitToScreen();
    };

    setLeftView() {
        this.scene.view.yaw = -Math.PI / 2;
        this.scene.view.pitch = 0;

        this.fitToScreen();
    };

    setRightView() {
        this.scene.view.yaw = Math.PI / 2;
        this.scene.view.pitch = 0;

        this.fitToScreen();
    };

    flipYZ() {
        this.isFlipYZ = !this.isFlipYZ;

        // TODO flipyz
        console.log('TODO');
    }

    setCameraMode(mode: number) {
        this.scene.cameraMode = mode;

        for (let pointcloud of this.scene.pointClouds) {
            pointcloud.material.useOrthographicCamera = mode === CameraMode.ORTHOGRAPHIC;
        }
    }

    getCameraMode() {
        return this.scene.cameraMode;
    }

    getProjection() {
        const pointcloud = this.scene.pointClouds[0];

        if (pointcloud) {
            return pointcloud.projection;
        } else {
            return null;
        }
    }

    loadSettingsFromURL() {
        if (Utils.getParameterByName("pointSize")) {
            this.setPointSize(parseFloat(Utils.getParameterByName("pointSize")!));
        }

        if (Utils.getParameterByName("FOV")) {
            this.setFOV(parseFloat(Utils.getParameterByName("FOV")!));
        }

        if (Utils.getParameterByName("opacity")) {
            this.setOpacity(parseFloat(Utils.getParameterByName("opacity")!));
        }

        if (Utils.getParameterByName("edlEnabled")) {
            this.setEDLEnabled(Utils.getParameterByName("edlEnabled") === "true");
        }

        if (Utils.getParameterByName('edlRadius')) {
            this.setEDLRadius(parseFloat(Utils.getParameterByName('edlRadius')!));
        }

        if (Utils.getParameterByName('edlStrength')) {
            this.setEDLStrength(parseFloat(Utils.getParameterByName('edlStrength')!));
        }

        if (Utils.getParameterByName('pointBudget')) {
            this.setPointBudget(parseFloat(Utils.getParameterByName('pointBudget')!));
        }

        if (Utils.getParameterByName('showBoundingBox')) {
            if (Utils.getParameterByName('showBoundingBox') === 'true') {
                this.setShowBoundingBox(true);
            } else {
                this.setShowBoundingBox(false);
            }
        }

        if (Utils.getParameterByName('material')) {
            const material = Utils.getParameterByName('material');

            if (material) this.setMaterial(material);
        }

        if (Utils.getParameterByName('pointSizing')) {
            const sizing = Utils.getParameterByName('pointSizing');
            if (sizing) this.setPointSizing(sizing);
        }

        if (Utils.getParameterByName('quality')) {
            const quality = Utils.getParameterByName('quality');
            if (quality) this.setQuality(quality);
        }

        if (Utils.getParameterByName('position')) {
            let value = Utils.getParameterByName('position');

            if (value) {
                value = value.replace('[', '').replace(']', '');

                const tokens = value.split(';');
                const x = parseFloat(tokens[0]);
                const y = parseFloat(tokens[1]);
                const z = parseFloat(tokens[2]);

                this.scene.view.position.set(x, y, z);
            }
        }

        if (Utils.getParameterByName('target')) {
            let value = Utils.getParameterByName('target');

            if (value) {
                value = value.replace('[', '').replace(']', '');

                const tokens = value.split(';');
                const x = parseFloat(tokens[0]);
                const y = parseFloat(tokens[1]);
                const z = parseFloat(tokens[2]);

                this.scene.view.lookAt(new Vector3(x, y, z));
            }
        }

        if (Utils.getParameterByName('background')) {
            const value = Utils.getParameterByName('background');

            if (value) this.setBackground(value);
        }

        // if(Utils.getParameterByName("elevationRange")){
        //	let value = Utils.getParameterByName("elevationRange");
        //	value = value.replace("[", "").replace("]", "");
        //	let tokens = value.split(";");
        //	let x = parseFloat(tokens[0]);
        //	let y = parseFloat(tokens[1]);
        //
        //	this.setElevationRange(x, y);
        //	//this.scene.view.target.set(x, y, z);
        // }
    };

    // ------------------------------------------------------------------------------------
    // Viewer Internals
    // ------------------------------------------------------------------------------------

    createControls() {
        // create FIRST PERSON CONTROLS
        this.fpControls = new FirstPersonControls(this);
        this.fpControls.enabled = false;
        this.fpControls.addEventListener('start', this.disableAnnotations.bind(this));
        this.fpControls.addEventListener('end', this.enableAnnotations.bind(this));

        // { // create GEO CONTROLS
        //	this.geoControls = new GeoControls(this.scene.camera, this.renderer.domElement);
        //	this.geoControls.enabled = false;
        //	this.geoControls.addEventListener("start", this.disableAnnotations.bind(this));
        //	this.geoControls.addEventListener("end", this.enableAnnotations.bind(this));
        //	this.geoControls.addEventListener("move_speed_changed", (event) => {
        //		this.setMoveSpeed(this.geoControls.moveSpeed);
        //	});
        // }

        // create ORBIT CONTROLS
        this.orbitControls = new OrbitControls(this);
        this.orbitControls.enabled = false;
        this.orbitControls.addEventListener('start', this.disableAnnotations.bind(this));
        this.orbitControls.addEventListener('end', this.enableAnnotations.bind(this));

        // create EARTH CONTROLS
        this.earthControls = new EarthControls(this);
        this.earthControls.enabled = false;
        this.earthControls.addEventListener('start', this.disableAnnotations.bind(this));
        this.earthControls.addEventListener('end', this.enableAnnotations.bind(this));

        // create DEVICE ORIENTATION CONTROLS
        this.deviceControls = new DeviceOrientationControls(this);
        this.deviceControls.enabled = false;
        this.deviceControls.addEventListener('start', this.disableAnnotations.bind(this));
        this.deviceControls.addEventListener('end', this.enableAnnotations.bind(this));

        // create VR CONTROLS
        this.vrControls = new VRControls(this);
        this.vrControls.enabled = false;
        this.vrControls.addEventListener('start', this.disableAnnotations.bind(this));
        this.vrControls.addEventListener('end', this.enableAnnotations.bind(this));
    };

    toggleMap = () => {
        if (this.mapView) {
            this.mapView.toggle();
        }
    };

    onGUILoaded(callback: Function) {
        if (this.guiLoaded) {
            callback();
        } else {
            this.guiLoadTasks.push(callback);
        }
    }

    promiseGuiLoaded() {
        return new Promise<void>((resolve) => {
            if (this.guiLoaded) {
                resolve();
            } else {
                this.guiLoadTasks.push(resolve);
            }
        });
    }

    loadGUI(callback: Function) {
        if (callback) {
            this.onGUILoaded(callback);
        }

        // const elSidebar = $(Html.Sidebar);
        // const elHeader = $(Html.Header);
        //
        // // load sidebar
        // $(document.body).prepend(elSidebar);
        //
        // elSidebar.find('.c-sidebar-brand').html(`
        //         <img src="${App.resourcePath + '/images/logo-2.png'}" alt="" class="c-sidebar-brand-full" height="15" />
        //         <img src="${App.resourcePath + '/images/logo-1.png'}" alt="" class="c-sidebar-brand-minimized" height="18" />
        //     `);
        //
        // const btnMapToggle = <HTMLButtonElement>document.createElement('button');
        // btnMapToggle.type = 'button';
        // btnMapToggle.setAttribute('class', 'btn btn-light btn-sm');
        // btnMapToggle.style.display = 'none';
        // btnMapToggle.id = 'ol_map_toggle';
        // btnMapToggle.innerHTML = '<i class="fas fa-map-marked"></i>';
        // $(btnMapToggle).on(MouseEventName.Click, this.toggleMap);
        //
        // const elButtons = $("#quick_buttons");
        //
        // elButtons.append(btnMapToggle);
        //
        // // VRButton.createButton(this.renderer).then((vrButton) => {
        // //     if (vrButton == null) {
        // //         console.log("VR not supported or active.");
        // //
        // //         return;
        // //     }
        // //
        // //     this.renderer.xr.enabled = true;
        // //
        // //     const element = vrButton.element;
        // //
        // //     element.style.position = "";
        // //     element.style.bottom = "";
        // //     element.style.left = "";
        // //     element.style.margin = "4px";
        // //     element.style.fontSize = "100%";
        // //     element.style.width = "2.5em";
        // //     element.style.height = "2.5em";
        // //     element.style.padding = "0";
        // //     element.style.textShadow = "black 2px 2px 2px";
        // //     element.style.display = "block";
        // //
        // //     elButtons.append(element);
        // //
        // //     vrButton.onStart(() => {
        // //         this.dispatchEvent({type: "vr_start"});
        // //     });
        // //
        // //     vrButton.onEnd(() => {
        // //         this.dispatchEvent({type: "vr_end"});
        // //     });
        // // });
        //
        // // this.mapView = new MapView(this);
        // // this.mapView.init();
        //
        // // i18n.init({
        // //     lng: 'vi',
        // //     resGetPath: App.resourcePath + '/lang/__lng__/__ns__.json',
        // //     preload: ['vi', 'en'],
        // //     getAsync: true,
        // //     debug: false
        // // }, () => {
        // //     // Start translation once everything is loaded
        // //     // @ts-ignore
        // //     $('body').i18n();
        // // });
        //
        // // $(() => {
        // //     const sidebar = new Sidebar(this);
        // //     sidebar.init();
        // //
        // //     this.sidebar = sidebar;
        // //
        // //     //if (callback) {
        // //     //	$(callback);
        // //     //}
        // //
        // //     $(document.body).append(Html.Profile);
        // //
        // //     this.profileWindow = new ProfileWindow(this);
        // //     this.profileWindowController = new ProfileWindowController(this);
        // //
        // //     $('#profile_window')
        // //         .draggable({
        // //             handle: $('#profile_titlebar'),
        // //             containment: $(document.body)
        // //         })
        // //         .resizable({
        // //             containment: $(document.body),
        // //             handles: 'n, e, s, w'
        // //         });
        // //
        // //     $(() => {
        // //         this.guiLoaded = true;
        // //         for (let task of this.guiLoadTasks) {
        // //             task();
        // //         }
        // //     });
        // // });
        //
        // // load header
        // // $('#wrapper').prepend(elHeader);
        // //
        // // elHeader.find('.c-header-brand').html(`
        // //     <img src="${App.resourcePath + '/images/logo-full.png'}" alt="" width="160" />
        // // `);
        // //
        // // Utils.loadScript(`${App.scriptPath}/script/coreui.bundle.js`).then();
        //
        // return this.promiseGuiLoaded();
    }

    setServer(server: any) {
        this.server = server;
    }

    initThree() {
        console.log(`Init three.js ${REVISION}`);

        const width = this.renderArea.clientWidth;
        const height = this.renderArea.clientHeight;

        const contextAttributes: WebGLContextAttributes = {
            alpha: true,
            depth: true,
            stencil: false,
            antialias: false,
            preserveDrawingBuffer: true,
            powerPreference: "high-performance",
        };

        // let contextAttributes = {
        // 	alpha: false,
        // 	preserveDrawingBuffer: true,
        // };

        // let contextAttributes = {
        // 	alpha: false,
        // 	preserveDrawingBuffer: true,
        // };

        const canvas = document.createElement("canvas");
        const context = canvas.getContext('webgl', contextAttributes) as WebGLRenderingContext;

        this.renderer = new WebGLRenderer({
            alpha: true,
            premultipliedAlpha: false,
            canvas: canvas,
            context: context,
        });
        this.renderer.sortObjects = false;
        this.renderer.setSize(width, height);
        this.renderer.autoClear = false;
        this.renderArea.appendChild(this.renderer.domElement);
        this.renderer.domElement.tabIndex = 2222;
        this.renderer.domElement.style.position = 'absolute';
        this.renderer.domElement.addEventListener(MouseEventName.Down, () => this.renderer.domElement.focus());

        // NOTE: If extension errors occur, pass the string into this.renderer.extensions.get(x) before enabling frag_depth extension for the interpolation shader, if available
        const gl = this.renderer.getContext() as WebGL2RenderingContext;
        const extensions = new WebGLExtensions(gl);

        // TODO: GL_EXT_frag_depth error in mobile
        if (extensions.has('GL_EXT_frag_depth')) {
            gl.getExtension('GL_EXT_frag_depth');
        }
        gl.getExtension('EXT_frag_depth');
        // TODO: gl_FragDepthEXT error in mobile
        if (extensions.has('gl_FragDepthEXT')) {
            gl.getExtension('gl_FragDepthEXT');
        }
        gl.getExtension('WEBGL_depth_texture');
        gl.getExtension('WEBGL_color_buffer_float'); 	// Enable explicitly for more portability, EXT_color_buffer_float is the proper name in WebGL 2

        if (gl.createVertexArray == null) {
            const extVAO = gl.getExtension('OES_vertex_array_object');

            if (!extVAO) {
                throw new Error("OES_vertex_array_object extension not supported");
            }

            gl.createVertexArray = extVAO.createVertexArrayOES.bind(extVAO);
            gl.bindVertexArray = extVAO.bindVertexArrayOES.bind(extVAO);
        }
    }

    updateAnnotations() {
        if (!this.visibleAnnotations) {
            this.visibleAnnotations = new Set();
        }

        this.scene.annotations.updateBounds();
        this.scene.cameraP.updateMatrixWorld();
        this.scene.cameraO.updateMatrixWorld();

        // todo: temp
        // const distances = [];
        const renderAreaSize = this.renderer.getSize(new Vector2());
        const viewer = this;
        const visibleNow: any[] = [];

        this.scene.annotations.traverse((annotation: any) => {
            if (annotation === this.scene.annotations) {
                return true;
            }

            if (!annotation.visible) {
                return false;
            }

            annotation.scene = this.scene;

            const element = annotation.domElement;
            let position = annotation.position.clone();

            position.add(annotation.offset);

            if (!position) {
                position = annotation.boundingBox.getCenter(new Vector3());
            }

            const distance = viewer.scene.cameraP.position.distanceTo(position);
            const radius = annotation.boundingBox.getBoundingSphere(new Sphere()).radius;

            const screenPos = new Vector3();
            let screenSize: number;

            // SCREEN POS
            screenPos.copy(position).project(this.scene.getActiveCamera());
            screenPos.x = renderAreaSize.x * (screenPos.x + 1) / 2;
            screenPos.y = renderAreaSize.y * (1 - (screenPos.y + 1) / 2);

            // SCREEN SIZE
            if (viewer.scene.cameraMode === CameraMode.PERSPECTIVE) {
                let fov = Math.PI * viewer.scene.cameraP.fov / 180;
                let slope = Math.tan(fov / 2.0);
                let projFactor = 0.5 * renderAreaSize.y / (slope * distance);
                screenSize = radius * projFactor;
            } else {
                screenSize = Utils.projectedRadiusOrtho(radius, viewer.scene.cameraO.projectionMatrix, renderAreaSize.x, renderAreaSize.y);
            }

            element.style.left = `${screenPos.x}px`;
            element.style.top = `${screenPos.y}px`;

            let zIndex = 10000000 - distance * (10000000 / this.scene.cameraP.far);
            if (annotation.descriptionVisible) {
                zIndex += 10000000;
            }

            element.style.zIndex = parseInt(zIndex.toString()).toString();

            if (annotation.children.length > 0) {
                let expand = screenSize > annotation.collapseThreshold || annotation.boundingBox.containsPoint(this.scene.getActiveCamera().position);
                annotation.expand = expand;

                if (!expand) {
                    //annotation.display = (screenPos.z >= -1 && screenPos.z <= 1);
                    let inFrustum = (screenPos.z >= -1 && screenPos.z <= 1);
                    if (inFrustum) {
                        visibleNow.push(annotation);
                    }
                }

                return expand;
            } else {
                //annotation.display = (screenPos.z >= -1 && screenPos.z <= 1);
                let inFrustum = (screenPos.z >= -1 && screenPos.z <= 1);
                if (inFrustum) {
                    visibleNow.push(annotation);
                }
            }

        });

        const notVisibleAnymore: any = new Set(this.visibleAnnotations);

        for (let annotation of visibleNow) {
            annotation.display = true;

            notVisibleAnymore.delete(annotation);
        }

        this.visibleAnnotations = visibleNow;

        for (let annotation of notVisibleAnymore) {
            annotation.display = false;
        }
    }

    updateMaterialDefaults(pointCloud: PointCloudOctree) {
        // PROBLEM STATEMENT:
        // * [min, max] of intensity, source id, etc. are computed as point clouds are loaded
        // * the point cloud material won't know the range it should use until some data is loaded
        // * users can modify the range at runtime, but sensible default ranges should be
        //   applied even if no GUI is present
        // * display ranges shouldn't suddenly change even if the actual range changes over time.
        //   e.g. the root node has intensity range [1, 478]. One of the descendants increases range to
        //   [0, 2047]. We should not automatically change to the new range because that would result
        //   in sudden and drastic changes of brightness. We should adjust the min/max of the sidebar slider.

        const material = pointCloud.material;
        const attIntensity = pointCloud.getAttribute("intensity");

        if (attIntensity != null && material.intensityRange[0] === Infinity) {
            material.intensityRange = [...attIntensity.range];
        }

        // const attIntensity = pointCloud.getAttribute("intensity");
        // if(attIntensity && material.intensityRange[0] === Infinity){
        // 	material.intensityRange = [...attIntensity.range];
        // }

        // let attributes = pointCloud.getAttributes();

        // for(let attribute of attributes.attributes){
        // 	if(attribute.range){
        // 		let range = [...attribute.range];
        // 		material.computedRange.set(attribute.name, range);
        // 		//material.setRange(attribute.name, range);
        // 	}
        // }
    }

    update(delta: number, timestamp: number) {
        if (App.measureTimings) performance.mark("update-start");

        this.dispatchEvent({
            type: ViewerEventName.UpdateStart,
            delta: delta,
            timestamp: timestamp
        });

        const scene = this.scene;
        const camera = scene.getActiveCamera();
        const visiblePointClouds = this.scene.pointClouds.filter(pc => pc.visible)

        App.pointLoadLimit = App.pointBudget * 2;

        const lTarget = camera.position.clone().add(camera.getWorldDirection(new Vector3()).multiplyScalar(1000));
        this.scene.directionalLight.position.copy(camera.position);
        this.scene.directionalLight.lookAt(lTarget);

        for (let pointcloud of visiblePointClouds) {
            pointcloud.showBoundingBox = this.showBoundingBox;
            pointcloud.generateDEM = this.generateDEM;
            pointcloud.minimumNodePixelSize = this.minNodeSize;

            const material = pointcloud.material;

            material.uniforms.uFilterReturnNumberRange.value = this.filterReturnNumberRange;
            material.uniforms.uFilterNumberOfReturnsRange.value = this.filterNumberOfReturnsRange;
            material.uniforms.uFilterGPSTimeClipRange.value = this.filterGPSTimeRange;
            material.uniforms.uFilterPointSourceIDClipRange.value = this.filterPointSourceIDRange;

            material.classification = this.classifications;
            material.recomputeClassification();

            this.updateMaterialDefaults(pointcloud);
        }

        if (this.showBoundingBox) {
            let bbRoot = this.scene.scene.getObjectByName("bounding_box_root");

            if (!bbRoot) {
                const node = new Object3D();
                node.name = "bounding_box_root";
                this.scene.scene.add(node);
                bbRoot = node;
            }

            const visibleBoxes: any[] = [];

            for (let pointcloud of this.scene.pointClouds) {
                for (let node of pointcloud.visibleNodes.filter(vn => vn.boundingBoxNode !== undefined)) {
                    visibleBoxes.push(node.boundingBoxNode);
                }
            }

            bbRoot.children = visibleBoxes;
        }

        if (!this.freeze) {
            const result = updatePointClouds(scene.pointClouds, camera, this.renderer);

            // DEBUG - ONLY DISPLAY NODES THAT INTERSECT MOUSE
            //if(false){

            //	let renderer = viewer.renderer;
            //	let mouse = viewer.inputHandler.mouse;

            //	let nmouse = {
            //		x: (mouse.x / renderer.domElement.clientWidth) * 2 - 1,
            //		y: -(mouse.y / renderer.domElement.clientHeight) * 2 + 1
            //	};

            //	let pickParams = {};

            //	//if(params.pickClipped){
            //	//	pickParams.pickClipped = params.pickClipped;
            //	//}

            //	pickParams.x = mouse.x;
            //	pickParams.y = renderer.domElement.clientHeight - mouse.y;

            //	let raycaster = new Raycaster();
            //	raycaster.setFromCamera(nmouse, camera);
            //	let ray = raycaster.ray;

            //	for(let pointcloud of scene.pointClouds){
            //		let nodes = pointcloud.nodesOnRay(pointcloud.visibleNodes, ray);
            //		pointcloud.visibleNodes = nodes;

            //	}
            //}

            // const tStart = performance.now();
            // const worldPos = new Vector3();
            // const camPos = viewer.scene.getActiveCamera().getWorldPosition(new Vector3());
            // let lowestDistance = Infinity;
            // let numNodes = 0;

            // viewer.scene.scene.traverse(node => {
            // 	node.getWorldPosition(worldPos);

            // 	const distance = worldPos.distanceTo(camPos);

            // 	lowestDistance = Math.min(lowestDistance, distance);

            // 	numNodes++;

            // 	if(Number.isNaN(distance)){
            // 		console.error(":(");
            // 	}
            // });
            // const duration = (performance.now() - tStart).toFixed(2);

            // App.debug.computeNearDuration = duration;
            // App.debug.numNodes = numNodes;

            //console.log(lowestDistance.toString(2), duration);

            // todo: temp
            // const tStart = performance.now();
            const campos = camera.position;
            let closestImage = Infinity;

            for (const images of this.scene.orientedImages) {
                for (const image of images.images) {
                    const distance = image.mesh.position.distanceTo(campos);

                    closestImage = Math.min(closestImage, distance);
                }
            }

            // todo: temp
            // const tEnd = performance.now();

            if (result.lowestSpacing !== Infinity) {
                let near = result.lowestSpacing * 10.0;
                let far = -this.getBoundingBox(this.scene.pointClouds).applyMatrix4(camera.matrixWorldInverse).min.z;

                far = Math.max(far * 1.5, 10000);
                near = Math.min(100.0, Math.max(0.01, near));
                near = Math.min(near, closestImage);
                far = Math.max(far, near + 10000);

                if (near === Infinity) {
                    near = 0.1;
                }

                camera.near = near;
                camera.far = far;
            } else {
                // don't change near and far in this case
            }

            if (this.scene.cameraMode === CameraMode.ORTHOGRAPHIC) {
                camera.near = -camera.far;
            }
        }

        this.scene.cameraP.fov = this.fov;

        let controls = this.getControls();
        if (controls === this.deviceControls) {
            this.controls.setScene(scene);
            this.controls.update(delta);

            this.scene.cameraP.position.copy(scene.view.position);
            this.scene.cameraO.position.copy(scene.view.position);
        } else if (controls !== null) {
            controls.setScene(scene);
            controls.update(delta);

            // @ts-ignore
            if (typeof debugDisabled === "undefined") {
                this.scene.cameraP.position.copy(scene.view.position);
                this.scene.cameraP.rotation.order = "ZXY";
                this.scene.cameraP.rotation.x = Math.PI / 2 + this.scene.view.pitch;
                this.scene.cameraP.rotation.z = this.scene.view.yaw;
            }

            this.scene.cameraO.position.copy(scene.view.position);
            this.scene.cameraO.rotation.order = "ZXY";
            this.scene.cameraO.rotation.x = Math.PI / 2 + this.scene.view.pitch;
            this.scene.cameraO.rotation.z = this.scene.view.yaw;
        }

        camera.updateMatrix();
        camera.updateMatrixWorld();
        camera.matrixWorldInverse.copy(camera.matrixWorld).invert();

        if (this._previousCamera === undefined) {
            this._previousCamera = this.scene.getActiveCamera().clone();
            this._previousCamera.rotation.copy(this.scene.getActiveCamera().rotation);
        }

        if (!this._previousCamera.matrixWorld.equals(camera.matrixWorld)) {
            this.dispatchEvent({
                type: ViewerEventName.CameraChanged,
                previous: this._previousCamera,
                camera: camera
            });
            // @ts-ignore
        } else if (!this._previousCamera.projectionMatrix.equals(camera.projectionMatrix)) {
            this.dispatchEvent({
                type: ViewerEventName.CameraChanged,
                previous: this._previousCamera,
                camera: camera
            });
        }

        this._previousCamera = this.scene.getActiveCamera().clone();
        this._previousCamera.rotation.copy(this.scene.getActiveCamera().rotation);


        { // update clip boxes
            const boxes: any[] = [];

            // volumes with clipping enabled
            //boxes.push(...this.scene.volumes.filter(v => (v.clip)));
            boxes.push(...this.scene.volumes.filter(v => (v.clip && v instanceof BoxVolume)));

            // profile segments
            for (let profile of this.scene.profiles) {
                boxes.push(...profile.boxes);
            }

            // Needed for .getInverse(), pre-empt a determinant of 0, see #815 / #816
            const degenerate = (box: any) => box.matrixWorld.determinant() !== 0;

            const clipBoxes = boxes.filter(degenerate).map(box => {
                box.updateMatrixWorld();

                const boxInverse = box.matrixWorld.clone().invert();
                const boxPosition = box.getWorldPosition(new Vector3());

                return {
                    box: box,
                    inverse: boxInverse,
                    position: boxPosition
                };
            });

            const clipPolygons = this.scene.polygonClipVolumes.filter(vol => vol.initialized);

            // set clip volumes in material
            for (const pointcloud of visiblePointClouds) {
                pointcloud.material.setClipBoxes(clipBoxes);
                pointcloud.material.setClipPolygons(clipPolygons);
                pointcloud.material.clipTask = this.clipTask;
                pointcloud.material.clipMethod = this.clipMethod;
            }
        }

        for (const pointcloud of visiblePointClouds) {
            pointcloud.material.elevationGradientRepeat = this.elevationGradientRepeat;
        }

        // update navigation cube
        this.navigationCube.update(camera.rotation);

        this.updateAnnotations();

        if (this.mapView) {
            this.mapView.update();

            if (this.mapView.sceneProjection) {
                // $("#ol_map_toggle").css("display", "block");
            }
        }

        TweenUpdate(timestamp);

        this.dispatchEvent({
            type: ViewerEventName.Update,
            delta: delta,
            timestamp: timestamp
        });

        if (App.measureTimings) {
            performance.mark("update-end");
            performance.measure("update", "update-start", "update-end");
        }
    }

    getPRenderer(): HQSplatRenderer | EDLRenderer | VRenderer {
        if (this.useHQ) {
            if (!this.hqRenderer) {
                this.hqRenderer = new HQSplatRenderer(this);
            }

            this.hqRenderer.useEDL = this.useEDL;

            return this.hqRenderer;
        } else {
            if (this.useEDL && Features!.SHADER_EDL.isSupported()) {
                if (!this.edlRenderer) {
                    this.edlRenderer = new EDLRenderer(this);
                }

                return this.edlRenderer;
            } else {
                if (!this.vRenderer) {
                    this.vRenderer = new VRenderer(this);
                }

                return this.vRenderer;
            }
        }
    }

    renderVR() {
        const renderer = this.renderer;

        renderer.setClearColor(0x550000, 0);
        renderer.clear();

        const xr = renderer.xr;
        const xrCameras = xr.getCamera(new PerspectiveCamera());

        // @ts-ignore
        if (xrCameras.cameras.length !== 2) {
            return;
        }

        const makeCam = this.vrControls.getCamera.bind(this.vrControls);

        // clear frame buffer
        if (this.background === BackgroundType.Skybox) {
            renderer.setClearColor(0xff0000, 1);
        } else if (this.background === BackgroundType.Gradient) {
            renderer.setClearColor(0x112233, 1);
        } else if (this.background === BackgroundType.Black) {
            renderer.setClearColor(0x000000, 1);
        } else if (this.background === BackgroundType.White) {
            renderer.setClearColor(0xFFFFFF, 1);
        } else {
            renderer.setClearColor(0x000000, 0);
        }

        renderer.clear();

        // render background
        if (this.background === BackgroundType.Skybox) {
            let {skybox} = this;

            const cam = makeCam();
            skybox.camera.rotation.copy(cam.rotation);
            skybox.camera.fov = cam.fov;
            skybox.camera.aspect = cam.aspect;

            // let dbg = new Object3D();
            const dbg = skybox.parent;
            // dbg.up.set(0, 0, 1);
            dbg.rotation.x = Math.PI / 2;

            // skybox.camera.parent = dbg;
            // dbg.children.push(skybox.camera);

            dbg.updateMatrix();
            dbg.updateMatrixWorld();

            skybox.camera.updateMatrix();
            skybox.camera.updateMatrixWorld();
            skybox.camera.updateProjectionMatrix();

            renderer.render(skybox.scene, skybox.camera);
            // renderer.render(skybox.scene, cam);
        } else if (this.background === BackgroundType.Gradient) {
            // renderer.render(this.scene.sceneBG, this.scene.cameraBG);
        }

        this.renderer.xr.getSession()!.updateRenderState({
            depthNear: 0.1,
            depthFar: 10000
        }).then();

        let cam: PerspectiveCamera;
        let view: Matrix4;

        // render world scene
        {
            cam = makeCam();
            cam.position.z -= 0.8 * cam.scale.x;
            cam.parent = null;
            // cam.near = 0.05;
            cam.near = this.scene.getActiveCamera().near;
            cam.far = this.scene.getActiveCamera().far;
            cam.updateMatrix();
            cam.updateMatrixWorld();

            this.scene.scene.updateMatrix();
            this.scene.scene.updateMatrixWorld();
            this.scene.scene.matrixAutoUpdate = false;

            const camWorld = cam.matrixWorld.clone();
            view = camWorld.clone().invert();
            this.scene.scene.matrix.copy(view);
            this.scene.scene.matrixWorld.copy(view);

            cam.matrix.identity();
            cam.matrixWorld.identity();
            cam.matrixWorldInverse.identity();

            renderer.render(this.scene.scene, cam);

            this.scene.scene.matrixWorld.identity();
        }

        for (let pointcloud of this.scene.pointClouds) {
            // @ts-ignore
            const viewport = xrCameras.cameras[0].viewport;

            pointcloud.material.useEDL = false;
            pointcloud.screenHeight = viewport.height;
            pointcloud.screenWidth = viewport.width;

            // automatically switch to paraboloids because they cause far less flickering in VR,
            // when point sizes are larger than around 2 pixels
            // if(Features.SHADER_INTERPOLATION.isSupported()){
            // 	pointcloud.material.shape = PointShape.PARABOLOID;
            // }
        }

        // render point clouds
        // @ts-ignore
        for (let xrCamera of xrCameras.cameras) {
            const v = xrCamera.viewport;
            renderer.setViewport(v.x, v.y, v.width, v.height);

            // xrCamera.fov = 90;

            { // estimate VR fov
                const proj = xrCamera.projectionMatrix;
                const inv = proj.clone().invert();

                const p1 = new Vector4(0, 1, -1, 1).applyMatrix4(inv);
                const rad = p1.y
                // todo: temp
                // let fov = 180 * (rad / Math.PI);

                xrCamera.fov = 180 * (rad / Math.PI);
            }

            for (let pointcloud of this.scene.pointClouds) {
                const {material} = pointcloud;
                material.useEDL = false;
            }

            const vrWorld = view.clone().invert();
            vrWorld.multiply(xrCamera.matrixWorld);
            const vrView = vrWorld.clone().invert();

            this.pRenderer.render(this.scene.scenePointCloud as any, xrCamera, null, {
                viewOverride: vrView,
            });
        }

        { // render VR scene
            const cam = makeCam();
            cam.parent = null;

            renderer.render(this.sceneVR, cam);
        }

        renderer.resetState();
    }

    renderDefault() {
        const pRenderer = this.getPRenderer();

        { // resize
            const width = this.scaleFactor * this.renderArea.clientWidth;
            const height = this.scaleFactor * this.renderArea.clientHeight;

            // resize viewer
            this.renderer.setSize(width, height);

            // resize cesium
            this.cesiumViewer.canvas.width = width;
            this.cesiumViewer.canvas.height = height;

            // todo: temp
            // const pixelRatio = this.renderer.getPixelRatio();
            const aspect = width / height;

            const scene = this.scene;

            scene.cameraP.aspect = aspect;
            scene.cameraP.updateProjectionMatrix();

            const frustumScale = this.scene.view.radius;
            scene.cameraO.left = -frustumScale;
            scene.cameraO.right = frustumScale;
            scene.cameraO.top = frustumScale / aspect;
            scene.cameraO.bottom = -frustumScale / aspect;
            scene.cameraO.updateProjectionMatrix();

            scene.cameraScreenSpace.top = 1 / aspect;
            scene.cameraScreenSpace.bottom = -1 / aspect;
            scene.cameraScreenSpace.updateProjectionMatrix();
        }

        pRenderer.clear();
        pRenderer.render(this.renderer);

        this.renderer.render(this.overlay, this.overlayCamera);
    }

    render() {
        if (App.measureTimings) {
            performance.mark("render-start");
        }

        try {
            const vrActive = this.renderer.xr.isPresenting;

            if (vrActive) {
                this.renderVR();
            } else {
                this.renderDefault();
            }
        } catch (e) {
            this._onCrash(e);
        }

        if (App.measureTimings) {
            performance.mark("render-end");
            performance.measure("render", "render-start", "render-end");
        }
    }

    protected _resolveTimings(timestamp: number) {
        if (App.measureTimings) {
            if (!this.toggle) {
                this.toggle = timestamp;
            }

            const duration = timestamp - this.toggle;

            if (duration > 1000.0) {
                const measures: PerformanceEntryList = performance.getEntriesByType("measure");
                const names: Set<any> = new Set();

                measures.forEach((measure) => {
                    names.add(measure.name);
                })

                const groups: Map<string, Record<string, any>> = new Map();

                names.forEach((name: string) => {
                    groups.set(name, {
                        measures: [],
                        sum: 0,
                        n: 0,
                        min: Infinity,
                        max: -Infinity
                    });
                })

                for (let measure of measures) {
                    const group = groups.get(measure.name);

                    if (!group) {
                        continue;
                    }

                    group.measures.push(measure);
                    group.sum += measure.duration;
                    group.n++;
                    group.min = Math.min(group.min, measure.duration);
                    group.max = Math.max(group.max, measure.duration);
                }

                // @ts-ignore
                const glQueries = resolveQueries(this.renderer.getContext());

                for (let [key, value] of glQueries) {
                    const group = {
                        measures: value.map((v: any) => {
                            return {duration: v}
                        }),
                        sum: value.reduce((a: any, i: any) => a + i, 0),
                        n: value.length,
                        min: Math.min(...value),
                        max: Math.max(...value)
                    };

                    const groupName = `[tq] ${key}`;

                    groups.set(groupName, group);
                    names.add(groupName);
                }

                groups.forEach((group) => {
                    group.mean = group.sum / group.n;
                    group.measures.sort((a: any, b: any) => a.duration - b.duration);

                    if (group.n === 1) {
                        group.median = group.measures[0].duration;
                    } else if (group.n > 1) {

                        group.median = group.measures[parseInt(String(group.n / 2))].duration;
                    }
                })

                // @ts-ignore
                const cn = Array.from(names).reduce((a, i) => Math.max(a, i.length)) + 5;
                const cMin = 10;
                const cMed = 10;
                const cMax = 10;
                const cSam = 6;

                let message = ` ${"NAME".padEnd(cn)} |`
                    + ` ${"MIN".padStart(cMin)} |`
                    + ` ${"MEDIAN".padStart(cMed)} |`
                    + ` ${"MAX".padStart(cMax)} |`
                    + ` ${"SAMPLES".padStart(cSam)} \n`;
                message += ` ${"-".repeat(message.length)}\n`;

                const namesSort = Array.from(names).sort();

                for (let name of namesSort) {
                    const group = groups.get(name as string);

                    if (!group) {
                        continue;
                    }

                    const min = group.min.toFixed(3);
                    const median = group.median.toFixed(3);
                    const max = group.max.toFixed(3);
                    const n = group.n;

                    message += ` ${name.padEnd(cn)} |`
                        + ` ${min.padStart(cMin)} |`
                        + ` ${median.padStart(cMed)} |`
                        + ` ${max.padStart(cMax)} |`
                        + ` ${n.toString().padStart(cSam)}\n`;
                }
                message += `\n`;
                console.log(message);

                performance.clearMarks();
                performance.clearMeasures();

                this.toggle = timestamp;
            }
        }
    }

    private __moment: number = Date.now();
    private __scheduled: boolean;
    private __fps = 24;

    protected _loop = () => {
        if (this._destroy) {
            return;
        }

        {
            this.__scheduled = false;
            const dt = Date.now() - this.__moment;

            if (dt < 1000 / this.__fps) {
                if (!this.__scheduled) {
                    this.__scheduled = true;
                    setTimeout(this._loop, 1000 / this.__fps - dt);
                }

                return;
            }

            this.__moment += dt;

            // this.__idRAF = requestAnimationFrame(this._loop);

            this.renderer.setAnimationLoop(this._loop);
        }

        if (this.stats) {
            this.stats.begin();
        }

        if (App.measureTimings) {
            performance.mark("loop-start");
        }

        const timestamp = window.performance.now();

        this.update(this.clock.getDelta(), timestamp);
        this.render();
        this.cesiumViewerRender();

        // let vrActive = viewer.renderer.xr.isPresenting;
        // if(vrActive){
        // 	this.update(this.clock.getDelta(), timestamp);
        // 	this.render();
        // }else{

        // 	this.update(this.clock.getDelta(), timestamp);
        // 	this.render();
        // }

        if (App.measureTimings) {
            performance.mark("loop-end");
            performance.measure("loop", "loop-start", "loop-end");
        }

        this._resolveTimings(timestamp);

        App.frameNumber++;

        if (this.stats) {
            this.stats.end();
        }
    }

    cesiumViewerRender() {
        if (this._destroy) {
            return;
        }

        if (this.isMap && this.cesiumViewer && this.cesiumViewer.scene && this.getBackground() === BackgroundType.Map) {
            {
                const camera = this.scene.getActiveCamera();

                const pPos = new Vector3(0, 0, 0).applyMatrix4(camera.matrixWorld);
                const pUp = new Vector3(0, 600, 0).applyMatrix4(camera.matrixWorld);
                const pTarget = this.scene.view.getPivot();

                const toCes = (pos: Vector3) => {
                    const xy = [pos.x, pos.y];
                    const height = pos.z;
                    const deg = window.toMap.forward(xy);
                    const [dx, dy] = deg;

                    if (!isNaN(dx) && !isNaN(dy)) {
                        // @ts-ignore
                        return Cartesian3.fromDegrees(...deg, height);
                    }
                }

                const toDeg = (c3p: Cartesian3) => {
                    const pos = Cartographic.fromCartesian(c3p)
                    return [pos.longitude / Math.PI * 180, pos.latitude / Math.PI * 180]
                }

                const cPos = toCes(pPos);
                const cUpTarget = toCes(pUp);
                const cTarget = toCes(pTarget);

                if (cPos && cUpTarget && cTarget) {
                    const coord = toDeg(cPos);

                    if (coord[0] < 90) {
                        this.isMap = false;
                        this.setBackground(BackgroundType.Skybox);
                    }

                    let cDir = Cartesian3.subtract(cTarget, cPos, new Cartesian3());
                    let cUp = Cartesian3.subtract(cUpTarget, cPos, new Cartesian3());

                    cDir = Cartesian3.normalize(cDir, new Cartesian3());
                    cUp = Cartesian3.normalize(cUp, new Cartesian3());

                    this.cesiumViewer.camera.setView({
                        destination: cPos,
                        orientation: {
                            direction: cDir,
                            up: cUp
                        }
                    });
                }
            }

            const aspect = this.scene.getActiveCamera().aspect;

            if (isNaN(aspect)) {
                return;
            }

            if (aspect < 1) {
                (this.cesiumViewer.camera.frustum as PerspectiveFrustum).fov = Math.PI * (this.scene.getActiveCamera().fov / 180);
            } else {
                let fovY = Math.PI * (this.scene.getActiveCamera().fov / 180);
                (this.cesiumViewer.camera.frustum as PerspectiveFrustum).fov = Math.atan(Math.tan(0.5 * fovY) * aspect) * 2;
            }

            this.cesiumViewer.render();
        }
    }

    destroy(callback?: Function) {
        if (this._destroy) {
            return;
        }

        this._destroy = true;

        this.renderer.setAnimationLoop(null);
        this.renderer.clear();
        this.renderer.dispose();

        App.frameNumber = 0;
        App.measureTimings = undefined;

        if (this.stats) {
            this.stats.end();
            document.body.removeChild(this.stats.dom);
        }

        if (callback) {
            setTimeout(callback, 1);
        }
    }
}
