import {Viewer} from "./viewer";
import {SphereVolume} from "../utils/Volume";
import {Mesh, MeshBasicMaterial, Scene, SphereGeometry, WebGLRenderer} from "three";
import {ViewerEventName} from "../core/Event";
import {BackgroundType} from "../core/Defines";

export class VRenderer {
    viewer: Viewer;
    renderer: WebGLRenderer;
    dummyMesh: Mesh;
    dummyScene: Scene;

    constructor(viewer: Viewer) {
        this.viewer = viewer;
        this.renderer = viewer.renderer;

        const dummyScene = new Scene();
        const geometry = new SphereGeometry(0.001, 2, 2);
        const mesh = new Mesh(geometry, new MeshBasicMaterial());
        mesh.position.set(36453, 35163, 764712);
        dummyScene.add(mesh);

        this.dummyMesh = mesh;
        this.dummyScene = dummyScene;
    }

    clearTargets() {
        //
    }

    clear() {
        // render skybox
        if (this.viewer.background === BackgroundType.Skybox) {
            this.renderer.setClearColor(0xff0000, 1);
        } else if (this.viewer.background === BackgroundType.Gradient) {
            this.renderer.setClearColor(0x00ff00, 1);
        } else if (this.viewer.background === BackgroundType.Black) {
            this.renderer.setClearColor(0x000000, 1);
        } else if (this.viewer.background === BackgroundType.White) {
            this.renderer.setClearColor(0xFFFFFF, 1);
        } else {
            this.renderer.setClearColor(0x000000, 0);
        }

        this.renderer.clear();
    }

    render = (params: any) => {
        const camera = params.camera ?? this.viewer.scene.getActiveCamera();

        this.viewer.dispatchEvent({
            type: ViewerEventName.Render_Pass_Begin,
            viewer: this.viewer
        });

        // const renderAreaSize = renderer.getSize(new Vector2());
        // const width = params.viewport ? params.viewport[2] : renderAreaSize.x;
        // const height = params.viewport ? params.viewport[3] : renderAreaSize.y;

        // render skybox
        if (this.viewer.background === BackgroundType.Skybox) {
            this.viewer.skybox.camera.rotation.copy(this.viewer.scene.cameraP.rotation);
            this.viewer.skybox.camera.fov = this.viewer.scene.cameraP.fov;
            this.viewer.skybox.camera.aspect = this.viewer.scene.cameraP.aspect;

            this.viewer.skybox.parent.rotation.x = 0;
            this.viewer.skybox.parent.updateMatrixWorld();

            this.viewer.skybox.camera.updateProjectionMatrix();
            this.renderer.render(this.viewer.skybox.scene, this.viewer.skybox.camera);
        } else if (this.viewer.background === BackgroundType.Gradient) {
            this.renderer.render(this.viewer.scene.sceneBG, this.viewer.scene.cameraBG);
        }

        for (let pointcloud of this.viewer.scene.pointClouds) {
            pointcloud.material.useEDL = false;
        }

        this.viewer.pRenderer.render(this.viewer.scene.scenePointCloud, camera, null, {
            clipSpheres: this.viewer.scene.volumes.filter(v => (v instanceof SphereVolume)),
        });

        // render scene
        this.renderer.render(this.viewer.scene.scene, camera);

        this.viewer.dispatchEvent({
            type: ViewerEventName.Render_Pass_Scene,
            viewer: this.viewer
        });

        this.viewer.clippingTool.update();
        this.renderer.render(this.viewer.clippingTool.sceneMarker, this.viewer.scene.cameraScreenSpace); //viewer.scene.cameraScreenSpace);
        this.renderer.render(this.viewer.clippingTool.sceneVolume, camera);

        this.renderer.render(this.viewer.controls.sceneControls, camera);

        this.renderer.clearDepth();

        this.viewer.transformationTool.update();

        this.viewer.dispatchEvent({
            type: ViewerEventName.Render_Pass_PerspectiveOverlay,
            viewer: this.viewer
        });

        // renderer.render(viewer.controls.sceneControls, camera);
        // renderer.render(viewer.clippingTool.sceneVolume, camera);
        // renderer.render(viewer.transformationTool.scene, camera);

        // renderer.setViewport(width - viewer.navigationCube.width,
        // 							height - viewer.navigationCube.width,
        // 							viewer.navigationCube.width, viewer.navigationCube.width);
        // renderer.render(viewer.navigationCube, viewer.navigationCube.camera);
        // renderer.setViewport(0, 0, width, height);

        this.viewer.dispatchEvent({
            type: ViewerEventName.Render_Pass_End,
            viewer: this.viewer
        });
    }
}
