import React, {useEffect, useRef, useState} from "react";
import {Map3DModel} from "../../../models/service/geodetic/Map3DModel";
import EventEmitter from "eventemitter3";
import {Viewer} from "./viewer/viewer";
import {Viewer as CesiumViewer} from "cesium";
import {App} from "./core/App";
import {Config} from "./const/Config";
import {PointShape, PointSizeType} from "./core/Defines";
import {MenuOutlined} from "@ant-design/icons";
import {Button, Drawer, Space} from "antd";
import {MapLayerFC} from "./components/MapLayerFC";
import {CompassFC} from "./components/CompassFC";
import {MapToolFC} from "./components/MapToolFC";
import {isMobile, isTablet} from "react-device-detect";
import {TParamPartGeodetic} from "../../../const/Types";
import {GeodeticPartMenuFC} from "../../widgets/GeodeticPartMenuFC";

export const Main = React.memo((props: {
    m3dId: number,
    item: Map3DModel,
    parts?: TParamPartGeodetic,
    viewer: Viewer,
    csMap: CesiumViewer,
    event: EventEmitter,
}) => {
    const divMasterLayerHeader = document.getElementById('MasterLayerHeader');
    const headerHeightRef = useRef<number>(divMasterLayerHeader ? divMasterLayerHeader.offsetHeight : 0);

    const [isVisibleDrawer, setIsVisibleDrawer] = useState(!(isMobile || isTablet));

    useEffect(() => {
        if (props.item.pc && props.item.pc.length > 0) {
            props.item.pc.forEach((item) => {
                App.loadPointCloud(item).then((evt) => {
                    if (evt) {
                        const pointCloud = evt.pointcloud;

                        pointCloud.projection = Config.PointCloudProjection;
                        // pointCloud.position.z = 0;

                        props.viewer.scene.addPointCloud(pointCloud);

                        const material = pointCloud.material;
                        material.size = 2;
                        material.pointSizeType = PointSizeType.ADAPTIVE;
                        material.shape = PointShape.SQUARE;

                        props.viewer.fitToScreen();

                        // const a = pointCloud.boundingBox;
                        // const pointCloudProjection = "+proj=utm +zone=48 +ellps=WGS84 +towgs84=-192.873,-39.382,-111.202,-0.00205,-0.0005,0.00335,0.0188 +units=m +no_defs";
                        // const mapProjection = proj4.defs('WGS84');
                        // // @ts-ignore
                        // window.toMap = proj4(pointCloudProjection, mapProjection);
                        // // @ts-ignore
                        // window.toScene = proj4(mapProjection, pointCloudProjection);
                    }
                })
            })
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <>
            <Space
                className={"z-10 left-2 absolute"}
                style={{
                    top: `calc(${headerHeightRef.current}px + 1rem)`
                }}
                direction={"vertical"}
            >
                <Button
                    className={'border-none'}
                    onClick={() => setIsVisibleDrawer(true)}
                    type="primary"
                    icon={<MenuOutlined/>}
                />
                <GeodeticPartMenuFC
                    parts={props.parts}
                    active={{
                        type: 'm3d',
                        id: props.m3dId
                    }}
                />
            </Space>
            <Drawer
                title="AutoTimelapse"
                placement="left"
                closable={true}
                onClose={() => setIsVisibleDrawer(false)}
                visible={isVisibleDrawer}
                bodyStyle={{
                    padding: 0
                }}
                maskClosable={false}
                mask={false}
                width={315}
                forceRender={true}
            >
                <MapLayerFC
                    viewer={props.viewer}
                    event={props.event}
                />
            </Drawer>
            <CompassFC viewer={props.viewer}/>
            <MapToolFC
                viewer={props.viewer}
                item={props.item}
            />
        </>
    );
})
