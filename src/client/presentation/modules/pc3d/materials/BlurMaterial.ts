// see http://john-chapman-graphics.blogspot.co.at/2013/01/ssao-tutorial.html

import {ShaderMaterial} from "three";
import {Shaders} from "../shaders/shaders";

export class BlurMaterial extends ShaderMaterial {
    // private _parameters: any;

    constructor(/*parameters: any = {}*/) {
        super();
        // this._parameters = parameters;

        let uniforms: any = {
            near: {type: 'f', value: 0},
            far: {type: 'f', value: 0},
            screenWidth: {type: 'f', value: 0},
            screenHeight: {type: 'f', value: 0},
            map: {type: 't', value: null}
        };

        this.setValues({
            uniforms: uniforms,
            vertexShader: Shaders['blur.vs'],
            fragmentShader: Shaders['blur.fs']
        });
    }
}

