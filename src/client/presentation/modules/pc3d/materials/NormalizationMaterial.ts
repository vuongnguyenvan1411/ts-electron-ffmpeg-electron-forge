import {RawShaderMaterial} from "three";
import {Shaders} from "../shaders/shaders";

export class NormalizationMaterial extends RawShaderMaterial {
    constructor() {
        super();

        const uniforms: any = {
            uDepthMap: {type: 't', value: null},
            uWeightMap: {type: 't', value: null},
        };

        this.setValues({
            uniforms: uniforms,
            vertexShader: this.getDefines() + Shaders['normalize.vs'],
            fragmentShader: this.getDefines() + Shaders['normalize.fs'],
        });
    }

    getDefines() {
        return '';
    }

    updateShaderSource() {
        const vs = this.getDefines() + Shaders['normalize.vs'];
        const fs = this.getDefines() + Shaders['normalize.fs'];

        this.setValues({
            vertexShader: vs,
            fragmentShader: fs
        });

        this.needsUpdate = true;
    }
}

