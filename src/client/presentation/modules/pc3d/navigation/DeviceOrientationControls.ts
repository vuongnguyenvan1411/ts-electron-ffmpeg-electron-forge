import {Euler, MathUtils, Quaternion, Scene as ThreeScene, Vector3, WebGLRenderer} from "three";
import {EventDispatcher} from "../core/EventDispatcher";
import {Viewer} from "../viewer/viewer";
import {VScene} from "../viewer/VScene";
import {WindowEventName} from "../core/Event";

export class DeviceOrientationControls extends EventDispatcher {
    viewer: Viewer;
    renderer: WebGLRenderer;
    scene: VScene;
    sceneControls: ThreeScene;
    screenOrientation: number;
    deviceOrientation: DeviceOrientationEvent;
    enabled: boolean;

    constructor(viewer: Viewer) {
        super();

        this.viewer = viewer;
        this.renderer = viewer.renderer;

        // this.scene = null;
        this.sceneControls = new ThreeScene();

        this.screenOrientation = this.getWindowOrientation();

        const deviceOrientationChange = (e: DeviceOrientationEvent) => {
            this.deviceOrientation = e;
        };

        const screenOrientationChange = () => {
            this.screenOrientation = this.getWindowOrientation();
        };

        if ('ondeviceorientationabsolute' in window) {
            window.addEventListener(WindowEventName.DeviceOrientationAbsolute, deviceOrientationChange);
        } else if ('ondeviceorientation' in window) {
            window.addEventListener(WindowEventName.DeviceOrientation, deviceOrientationChange);
        } else {
            console.warn("No device orientation found.");
        }

        // window.addEventListener('deviceorientation', deviceOrientationChange);
        window.addEventListener(WindowEventName.OrientationChange, screenOrientationChange);
    }

    setScene(scene: VScene) {
        this.scene = scene;
    }

    update(/*delta: any*/) {
        const computeQuaternion = function (alpha: number, beta: number, gamma: number, orient: number) {
            const quaternion = new Quaternion();

            const zee = new Vector3(0, 0, 1);
            const euler = new Euler();
            const q0 = new Quaternion();

            euler.set(beta, gamma, alpha, 'ZXY');
            quaternion.setFromEuler(euler);
            quaternion.multiply(q0.setFromAxisAngle(zee, -orient));

            return quaternion;
        };

        if (typeof this.deviceOrientation !== 'undefined') {
            const alpha = this.deviceOrientation.alpha ? MathUtils.degToRad(this.deviceOrientation.alpha) : 0;
            const beta = this.deviceOrientation.beta ? MathUtils.degToRad(this.deviceOrientation.beta) : 0;
            const gamma = this.deviceOrientation.gamma ? MathUtils.degToRad(this.deviceOrientation.gamma) : 0;
            const orient = this.screenOrientation ? MathUtils.degToRad(this.screenOrientation) : 0;
            const quaternion = computeQuaternion(alpha, beta, gamma, orient);

            this.viewer.scene.cameraP.quaternion.set(quaternion.x, quaternion.y, quaternion.z, quaternion.w);
        }
    }

    protected getWindowOrientation() {
        if ('orientation' in window.screen) {
            return window.screen.orientation.angle || 0;
        } else {
            return window.orientation as number || 0;
        }
    }
}
