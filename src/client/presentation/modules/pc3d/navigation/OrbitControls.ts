import {Scene as ThreeScene, Vector2, Vector3, WebGLRenderer} from "three";
import {MOUSE} from "../core/Defines";
import {Utils} from "../core/Utils";
import {EventDispatcher} from "../core/EventDispatcher";
import {Easing, Tween} from "@tweenjs/tween.js";
import {Viewer} from "../viewer/viewer";
import {VScene} from "../viewer/VScene";

export class OrbitControls extends EventDispatcher {
    viewer: Viewer;
    renderer: WebGLRenderer;
    scene: VScene;
    sceneControls: ThreeScene;
    rotationSpeed: number;
    fadeFactor: number;
    yawDelta: number;
    pitchDelta: number;
    panDelta: Vector2;
    radiusDelta: number;
    doubleClockZoomEnabled: boolean;
    tweens: Tween<any>[];
    enabled: boolean;

    constructor(viewer: Viewer) {
        super();

        this.viewer = viewer;
        this.renderer = viewer.renderer;

        this.sceneControls = new ThreeScene();

        this.rotationSpeed = 5;

        this.fadeFactor = 20;
        this.yawDelta = 0;
        this.pitchDelta = 0;
        this.panDelta = new Vector2(0, 0);
        this.radiusDelta = 0;

        this.doubleClockZoomEnabled = true;

        this.tweens = [];

        const drag = (e: any) => {
            if (e.drag.object !== null) {
                return;
            }

            if (e.drag.startHandled === undefined) {
                e.drag.startHandled = true;

                this.dispatchEvent({type: 'start'});
            }

            const nDrag = {
                x: e.drag.lastDrag.x / this.renderer.domElement.clientWidth,
                y: e.drag.lastDrag.y / this.renderer.domElement.clientHeight
            };

            if (e.drag.mouse === MOUSE.LEFT) {
                this.yawDelta += nDrag.x * this.rotationSpeed;
                this.pitchDelta += nDrag.y * this.rotationSpeed;

                this.stopTweens();
            } else if (e.drag.mouse === MOUSE.RIGHT) {
                this.panDelta.x += nDrag.x;
                this.panDelta.y += nDrag.y;

                this.stopTweens();
            }
        };

        const drop = () => {
            this.dispatchEvent({
                type: 'end'
            });
        };

        const scroll = (e: any) => {
            const resolvedRadius = this.scene.view.radius + this.radiusDelta;

            this.radiusDelta += -e.delta * resolvedRadius * 0.1;

            this.stopTweens();
        };

        const dblclick = (e: any) => {
            if (this.doubleClockZoomEnabled) {
                this.zoomToLocation(e.mouse);
            }
        };

        let previousTouch: any = null;
        const touchStart = (e: any) => {
            previousTouch = e;
        };

        const touchEnd = (e: any) => {
            previousTouch = e;
        };

        const touchMove = (e: any) => {
            if (e.touches.length === 2 && previousTouch.touches.length === 2) {
                const prev = previousTouch;
                const curr = e;

                const prevDX = prev.touches[0].pageX - prev.touches[1].pageX;
                const prevDY = prev.touches[0].pageY - prev.touches[1].pageY;
                const prevDist = Math.sqrt(prevDX * prevDX + prevDY * prevDY);

                const currDX = curr.touches[0].pageX - curr.touches[1].pageX;
                const currDY = curr.touches[0].pageY - curr.touches[1].pageY;
                const currDist = Math.sqrt(currDX * currDX + currDY * currDY);

                const delta = currDist / prevDist;
                const resolvedRadius = this.scene.view.radius + this.radiusDelta;
                const newRadius = resolvedRadius / delta;
                this.radiusDelta = newRadius - resolvedRadius;

                this.stopTweens();
            } else if (e.touches.length === 3 && previousTouch.touches.length === 3) {
                const prev = previousTouch;
                const curr = e;

                const prevMeanX = (prev.touches[0].pageX + prev.touches[1].pageX + prev.touches[2].pageX) / 3;
                const prevMeanY = (prev.touches[0].pageY + prev.touches[1].pageY + prev.touches[2].pageY) / 3;

                const currMeanX = (curr.touches[0].pageX + curr.touches[1].pageX + curr.touches[2].pageX) / 3;
                const currMeanY = (curr.touches[0].pageY + curr.touches[1].pageY + curr.touches[2].pageY) / 3;

                const delta = {
                    x: (currMeanX - prevMeanX) / this.renderer.domElement.clientWidth,
                    y: (currMeanY - prevMeanY) / this.renderer.domElement.clientHeight
                };

                this.panDelta.x += delta.x;
                this.panDelta.y += delta.y;

                this.stopTweens();
            }

            previousTouch = e;
        };

        this.addEventListener('touchstart', touchStart);
        this.addEventListener('touchend', touchEnd);
        this.addEventListener('touchmove', touchMove);
        this.addEventListener('drag', drag);
        this.addEventListener('drop', drop);
        this.addEventListener('mousewheel', scroll);
        this.addEventListener('dblclick', dblclick);
    }

    setScene(scene: VScene) {
        this.scene = scene;
    }

    stop() {
        this.yawDelta = 0;
        this.pitchDelta = 0;
        this.radiusDelta = 0;
        this.panDelta.set(0, 0);
    }

    zoomToLocation(mouse: Vector2) {
        const camera = this.scene.getActiveCamera();

        const I = Utils.getMousePointCloudIntersection(
            mouse,
            camera,
            this.viewer,
            this.scene.pointClouds,
            {
                pickClipped: true
            }
        );

        if (I === null) {
            return;
        }

        let targetRadius = 0;
        {
            const minimumJumpDistance = 0.2;

            const domElement = this.renderer.domElement;
            const ray = Utils.mouseToRay(mouse, camera, domElement.clientWidth, domElement.clientHeight);

            const nodes = I.pointcloud.nodesOnRay(I.pointcloud.visibleNodes, ray);
            const lastNode = nodes[nodes.length - 1];
            const radius = lastNode.getBoundingSphere().radius;

            targetRadius = Math.min(this.scene.view.radius, radius);
            targetRadius = Math.max(minimumJumpDistance, targetRadius);
        }

        const d = this.scene.view.direction.multiplyScalar(-1);
        const cameraTargetPosition = new Vector3().addVectors(I.location, d.multiplyScalar(targetRadius));
        // TODO Unused: let controlsTargetPosition = I.location;

        const animationDuration = 600;
        const easing = Easing.Quartic.Out;

        { // animate
            const value = {x: 0};
            const tween = new Tween(value).to({x: 1}, animationDuration);
            tween.easing(easing);
            this.tweens.push(tween);

            const startPos = this.scene.view.position.clone();
            const targetPos = cameraTargetPosition.clone();
            const startRadius = this.scene.view.radius;
            const targetRadius = cameraTargetPosition.distanceTo(I.location);

            tween.onUpdate(() => {
                let t = value.x;
                this.scene.view.position.x = (1 - t) * startPos.x + t * targetPos.x;
                this.scene.view.position.y = (1 - t) * startPos.y + t * targetPos.y;
                this.scene.view.position.z = (1 - t) * startPos.z + t * targetPos.z;

                this.scene.view.radius = (1 - t) * startRadius + t * targetRadius;
                this.viewer.setMoveSpeed(this.scene.view.radius);
            });

            tween.onComplete(() => {
                this.tweens = this.tweens.filter(e => e !== tween);
            });

            tween.start();
        }
    }

    stopTweens() {
        this.tweens.forEach(e => e.stop());
        this.tweens = [];
    }

    update(delta: number) {
        const view = this.scene.view;

        { // apply rotation
            const progression = Math.min(1, this.fadeFactor * delta);

            let yaw = view.yaw;
            let pitch = view.pitch;
            const pivot = view.getPivot();

            yaw -= progression * this.yawDelta;
            pitch -= progression * this.pitchDelta;

            view.yaw = yaw;
            view.pitch = pitch;

            const V = this.scene.view.direction.multiplyScalar(-view.radius);
            const position = new Vector3().addVectors(pivot, V);

            view.position.copy(position);
        }

        { // apply pan
            const progression = Math.min(1, this.fadeFactor * delta);
            const panDistance = progression * view.radius * 3;

            const px = -this.panDelta.x * panDistance;
            const py = this.panDelta.y * panDistance;

            view.pan(px, py);
        }

        { // apply zoom
            const progression = Math.min(1, this.fadeFactor * delta);

            // let radius = view.radius + progression * this.radiusDelta * view.radius * 0.1;
            const radius = view.radius + progression * this.radiusDelta;

            const V = view.direction.multiplyScalar(-radius);
            const position = new Vector3().addVectors(view.getPivot(), V);
            view.radius = radius;

            view.position.copy(position);
        }

        {
            const speed = view.radius;
            this.viewer.setMoveSpeed(speed);
        }

        { // decelerate over time
            const progression = Math.min(1, this.fadeFactor * delta);
            const attenuation = Math.max(0, 1 - this.fadeFactor * delta);

            this.yawDelta *= attenuation;
            this.pitchDelta *= attenuation;
            this.panDelta.multiplyScalar(attenuation);
            // this.radiusDelta *= attenuation;
            this.radiusDelta -= progression * this.radiusDelta;
        }
    }

    zoom(level: number = 0.1) {
        const resolvedRadius = this.scene.view.radius + this.radiusDelta;

        this.radiusDelta += resolvedRadius * -level;

        this.stopTweens();
    }
}
