/**
 *
 * This set of controls performs first person navigation without mouse lock.
 * Instead, rotating the camera is done by dragging with the left mouse button.
 *
 * move: a/s/d/w or up/down/left/right
 * rotate: left mouse
 * pan: right mouse
 * change speed: mouse wheel
 *
 */

import {Matrix4, Object3D, Vector2, Vector3} from "three";
import {EventDispatcher} from "../core/EventDispatcher";
import {KeyCodes} from "../core/KeyCodes";

const STATE = {
    NONE: -1,
    ROTATE: 0,
    SPEEDCHANGE: 1,
    PAN: 2
};

export class GeoControls extends EventDispatcher {
    object: any;
    domElement: any;
    enabled: boolean;
    track: any;
    trackPos: number | null;
    rotateSpeed: number;
    moveSpeed: number;
    shiftDown: boolean;
    position0: any;
    moveForwardMouse: boolean;
    moveLeft: any;
    moveRight: any;
    moveForward: any;
    moveBackward: any;
    rotLeft: any;
    rotRight: any;
    raiseCamera: any;
    lowerCamera: any;
    thetaDelta: number;
    phiDelta: number;
    state: any;
    noZoom: boolean;
    rotateStart: Vector2;
    rotateEnd: Vector2;
    rotateDelta: Vector2;
    panStart: Vector2;
    panEnd: Vector2;
    panDelta: Vector2;
    panOffset: Vector3;
    pans: Vector3;
    changeEvent: any;
    startEvent: any;
    endEvent: any;
    lastPosition: Vector3;

    constructor(object: any, domElement: any) {
        super();

        console.log("deprecated?");

        this.object = object;
        this.domElement = (domElement !== undefined) ? domElement : document;

        // Set to false to disable this control
        this.enabled = true;

        // Set this to a SplineCurve3 instance
        this.track = null;
        // position on track in interval [0,1]
        this.trackPos = 0;

        this.rotateSpeed = 1.0;
        this.moveSpeed = 10.0;

        this.rotateStart = new Vector2();
        this.rotateEnd = new Vector2();
        this.rotateDelta = new Vector2();

        this.panStart = new Vector2();
        this.panEnd = new Vector2();
        this.panDelta = new Vector2();
        this.panOffset = new Vector3();

        // TODO Unused: let offset = new Vector3();

        this.pans = new Vector3();

        this.phiDelta = 0;
        this.thetaDelta = 0;

        this.shiftDown = false;

        this.lastPosition = new Vector3();

        this.state = STATE.NONE;

        // for reset
        this.position0 = this.object.position.clone();

        // events

        this.changeEvent = {type: 'change'};
        this.startEvent = {type: 'start'};
        this.endEvent = {type: 'end'};

        this.domElement.addEventListener('contextmenu', (event: any) => {
            event.preventDefault();
        }, false);
        this.domElement.addEventListener('mousedown', this.onMouseDown.bind(this), false);
        this.domElement.addEventListener('mousewheel', this.onMouseWheel.bind(this), false);
        this.domElement.addEventListener('DOMMouseScroll', this.onMouseWheel.bind(this), false); // firefox

        this.domElement.addEventListener('mousemove', this.onMouseMove.bind(this), false);
        this.domElement.addEventListener('mouseup', this.onMouseUp.bind(this), false);

        if (this.domElement.tabIndex === -1) {
            this.domElement.tabIndex = 2222;
        }

        this.domElement.addEventListener('keydown', this.onKeyDown.bind(this), false);
        this.domElement.addEventListener('keyup', this.onKeyUp.bind(this), false);
    }

    setTrack(track: any) {
        if (this.track !== track) {
            this.track = track;
            this.trackPos = null;
        }
    };

    setTrackPos(trackPos: any, _preserveRelativeRotation?: any) {
        // TODO Unused: let preserveRelativeRotation = _preserveRelativeRotation || false;

        let newTrackPos = Math.max(0, Math.min(1, trackPos));
        let oldTrackPos = this.trackPos || newTrackPos;

        let newTangent = this.track.getTangentAt(newTrackPos);
        let oldTangent = this.track.getTangentAt(oldTrackPos);

        if (newTangent.equals(oldTangent)) {
            // no change in direction
        } else {
            let tangentDiffNormal = new Vector3().crossVectors(oldTangent, newTangent).normalize();
            let angle = oldTangent.angleTo(newTangent);
            let rot = new Matrix4().makeRotationAxis(tangentDiffNormal, angle);
            let dir = this.object.getWorldDirection(new Vector3()).clone();
            dir = dir.applyMatrix4(rot);
            let target = new Vector3().addVectors(this.object.position, dir);
            this.object.lookAt(target);
            this.object.updateMatrixWorld();

            let event = {
                type: 'path_relative_rotation',
                angle: angle,
                axis: tangentDiffNormal,
                controls: this
            };
            this.dispatchEvent(event);
        }

        if (this.trackPos === null) {
            let target = new Vector3().addVectors(this.object.position, newTangent);
            this.object.lookAt(target);
        }

        this.trackPos = newTrackPos;

        // let pStart = this.track.getPointAt(oldTrackPos);
        // let pEnd = this.track.getPointAt(newTrackPos);
        // let pDiff = pEnd.sub(pStart);

        if (newTrackPos !== oldTrackPos) {
            let event = {
                type: 'move',
                translation: this.pans.clone()
            };

            this.dispatchEvent(event);
        }
    }

    stop() {
        //
    }

    getTrackPos() {
        return this.trackPos!;
    }

    rotateLeft(angle: any) {
        this.thetaDelta -= angle;
    }

    rotateUp(angle: any) {
        this.phiDelta -= angle;
    }

    // pass in distance in world space to move left
    panLeft(distance: any) {
        let te = this.object.matrix.elements;

        // get X column of matrix
        this.panOffset.set(te[0], te[1], te[2]);
        this.panOffset.multiplyScalar(-distance);

        this.pans.add(this.panOffset);
    }

    // pass in distance in world space to move up
    panUp(distance: any) {
        let te = this.object.matrix.elements;

        // get Y column of matrix
        this.panOffset.set(te[4], te[5], te[6]);
        this.panOffset.multiplyScalar(distance);

        this.pans.add(this.panOffset);
    }

    // pass in distance in world space to move forward
    panForward(distance: any) {
        if (this.track) {
            this.setTrackPos(this.getTrackPos() - distance / this.track.getLength());
        } else {
            let te = this.object.matrix.elements;

            // get Y column of matrix
            this.panOffset.set(te[8], te[9], te[10]);
            // this.panOffset.set( te[ 8 ], 0, te[ 10 ] );
            this.panOffset.multiplyScalar(distance);

            this.pans.add(this.panOffset);
        }
    }

    pan(deltaX: any, deltaY: any) {
        let element = this.domElement === document ? this.domElement.body : this.domElement;

        if (this.object.fov !== undefined) {
            // perspective
            let position = this.object.position;
            let offset = position.clone();
            let targetDistance = offset.length();

            // half of the fov is center to top of screen
            targetDistance *= Math.tan((this.object.fov / 2) * Math.PI / 180.0);

            // we actually don't use screenWidth, since perspective camera is fixed to screen height
            this.panLeft(2 * deltaX * targetDistance / element.clientHeight);
            this.panUp(2 * deltaY * targetDistance / element.clientHeight);
        } else if (this.object.top !== undefined) {
            // orthographic
            this.panLeft(deltaX * (this.object.right - this.object.left) / element.clientWidth);
            this.panUp(deltaY * (this.object.top - this.object.bottom) / element.clientHeight);
        } else {
            // camera neither orthographic or perspective
            console.warn('WARNING: GeoControls.js encountered an unknown camera type - pan disabled.');
        }
    }

    update(delta: number) {
        this.object.rotation.order = 'ZYX';

        let object = this.object;

        this.object = new Object3D();
        this.object.position.copy(object.position);
        this.object.rotation.copy(object.rotation);
        this.object.updateMatrix();
        this.object.updateMatrixWorld();

        let position = this.object.position;

        if (delta !== undefined) {
            let multiplier = this.shiftDown ? 4 : 1;
            if (this.moveRight) {
                this.panLeft(-delta * this.moveSpeed * multiplier);
            }
            if (this.moveLeft) {
                this.panLeft(delta * this.moveSpeed * multiplier);
            }
            if (this.moveForward || this.moveForwardMouse) {
                this.panForward(-delta * this.moveSpeed * multiplier);
            }
            if (this.moveBackward) {
                this.panForward(delta * this.moveSpeed * multiplier);
            }
            if (this.rotLeft) {
                this.rotateLeft(-0.5 * Math.PI * delta / this.rotateSpeed);
            }
            if (this.rotRight) {
                this.rotateLeft(0.5 * Math.PI * delta / this.rotateSpeed);
            }
            if (this.raiseCamera) {
                // this.rotateUp( -0.5 * Math.PI * delta / this.rotateSpeed );
                this.panUp(delta * this.moveSpeed * multiplier);
            }
            if (this.lowerCamera) {
                // this.rotateUp( 0.5 * Math.PI * delta / this.rotateSpeed );
                this.panUp(-delta * this.moveSpeed * multiplier);
            }
        }

        if (!this.pans.equals(new Vector3(0, 0, 0))) {
            let event = {
                type: 'move',
                translation: this.pans.clone()
            };
            this.dispatchEvent(event);
        }

        position.add(this.pans);

        if (!(this.thetaDelta === 0.0 && this.phiDelta === 0.0)) {
            let event = {
                type: 'rotate',
                thetaDelta: this.thetaDelta,
                phiDelta: this.phiDelta
            };

            this.dispatchEvent(event);
        }

        this.object.updateMatrix();
        let rot = new Matrix4().makeRotationY(this.thetaDelta);
        let res = new Matrix4().multiplyMatrices(rot, this.object.matrix);
        this.object.quaternion.setFromRotationMatrix(res);

        this.object.rotation.x += this.phiDelta;
        this.object.updateMatrixWorld();

        // send transformation proposal to listeners
        let proposeTransformEvent: any = {
            type: 'proposeTransform',
            oldPosition: object.position,
            newPosition: this.object.position,
            objections: 0,
            counterProposals: []
        };
        this.dispatchEvent(proposeTransformEvent);

        // check some counter proposals if transformation wasn't accepted
        if (proposeTransformEvent.objections > 0) {
            if (proposeTransformEvent.counterProposals.length > 0) {
                let cp = proposeTransformEvent.counterProposals;
                this.object.position.copy(cp[0]);

                proposeTransformEvent.objections = 0;
                proposeTransformEvent.counterProposals = [];
            }
        }

        // apply transformation, if accepted
        if (proposeTransformEvent.objections > 0) {

        } else {
            object.position.copy(this.object.position);
        }

        object.rotation.copy(this.object.rotation);

        this.object = object;

        this.thetaDelta = 0;
        this.phiDelta = 0;
        this.pans.set(0, 0, 0);

        if (this.lastPosition.distanceTo(this.object.position) > 0) {
            this.dispatchEvent(this.changeEvent);

            this.lastPosition.copy(this.object.position);
        }

        if (this.track) {
            let pos = this.track.getPointAt(this.trackPos);
            object.position.copy(pos);
        }
    }

    reset() {
        this.state = STATE.NONE;

        this.object.position.copy(this.position0);
    }

    onMouseDown(e: any) {
        if (this.enabled === false) return;
        e.preventDefault();

        if (e.button === 0) {
            this.state = STATE.ROTATE;

            this.rotateStart.set(e.clientX, e.clientY);
        } else if (e.button === 1) {
            this.state = STATE.PAN;

            this.panStart.set(e.clientX, e.clientY);
        } else if (e.button === 2) {
            // this.state = STATE.PAN;
            // this.panStart.set( event.clientX, event.clientY );
            this.moveForwardMouse = true;
        }

        // this.domElement.addEventListener( 'mousemove', onMouseMove, false );
        // this.domElement.addEventListener( 'mouseup', onMouseUp, false );
        this.dispatchEvent(this.startEvent);
    }

    onMouseMove(event: any) {
        if (this.enabled === false) return;

        event.preventDefault();

        let element = this.domElement === document ? this.domElement.body : this.domElement;

        if (this.state === STATE.ROTATE) {
            this.rotateEnd.set(event.clientX, event.clientY);
            this.rotateDelta.subVectors(this.rotateEnd, this.rotateStart);

            // rotating across whole screen goes 360 degrees around
            this.rotateLeft(2 * Math.PI * this.rotateDelta.x / element.clientWidth * this.rotateSpeed);

            // rotating up and down along whole screen attempts to go 360, but limited to 180
            this.rotateUp(2 * Math.PI * this.rotateDelta.y / element.clientHeight * this.rotateSpeed);

            this.rotateStart.copy(this.rotateEnd);
        } else if (this.state === STATE.PAN) {
            this.panEnd.set(event.clientX, event.clientY);
            this.panDelta.subVectors(this.panEnd, this.panStart);
            // this.panDelta.multiplyScalar(this.moveSpeed).multiplyScalar(0.0001);
            this.panDelta.multiplyScalar(0.002).multiplyScalar(this.moveSpeed);

            this.pan(this.panDelta.x, this.panDelta.y);

            this.panStart.copy(this.panEnd);
        }
    }

    onMouseUp(event: any) {
        if (this.enabled === false) return;

        // console.log(event.which);

        if (event.button === 2) {
            this.moveForwardMouse = false;
        } else {
            // this.domElement.removeEventListener( 'mousemove', onMouseMove, false );
            // this.domElement.removeEventListener( 'mouseup', onMouseUp, false );
            this.dispatchEvent(this.endEvent);
            this.state = STATE.NONE;
        }
    }

    onMouseWheel(event: any) {
        if (this.enabled === false || this.noZoom === true) return;

        event.preventDefault();

        let direction = (event.detail < 0 || event.wheelDelta > 0) ? 1 : -1;
        let moveSpeed = this.moveSpeed + this.moveSpeed * 0.1 * direction;
        moveSpeed = Math.max(0.1, moveSpeed);

        this.setMoveSpeed(moveSpeed);

        this.dispatchEvent(this.startEvent);
        this.dispatchEvent(this.endEvent);
    }

    setMoveSpeed(value: any) {
        if (this.moveSpeed !== value) {
            this.moveSpeed = value;
            this.dispatchEvent({
                type: 'move_speed_changed',
                controls: this
            });
        }
    }

    onKeyDown(event: any) {
        if (this.enabled === false) return;

        this.shiftDown = event.shiftKey;

        switch (event.keyCode) {
            case KeyCodes.UP:
                this.moveForward = true;
                break;
            case KeyCodes.BOTTOM:
                this.moveBackward = true;
                break;
            case KeyCodes.LEFT:
                this.moveLeft = true;
                break;
            case KeyCodes.RIGHT:
                this.moveRight = true;
                break;
            case KeyCodes.W:
                this.moveForward = true;
                break;
            case KeyCodes.S:
                this.moveBackward = true;
                break;
            case KeyCodes.A:
                this.moveLeft = true;
                break;
            case KeyCodes.D:
                this.moveRight = true;
                break;
            case KeyCodes.Q:
                this.rotLeft = true;
                break;
            case KeyCodes.E:
                this.rotRight = true;
                break;
            case KeyCodes.R:
                this.raiseCamera = true;
                break;
            case KeyCodes.F:
                this.lowerCamera = true;
                break;
        }
    }

    onKeyUp(event: any) {
        this.shiftDown = event.shiftKey;

        switch (event.keyCode) {
            case KeyCodes.W:
                this.moveForward = false;
                break;
            case KeyCodes.S:
                this.moveBackward = false;
                break;
            case KeyCodes.A:
                this.moveLeft = false;
                break;
            case KeyCodes.D:
                this.moveRight = false;
                break;
            case KeyCodes.UP:
                this.moveForward = false;
                break;
            case KeyCodes.BOTTOM:
                this.moveBackward = false;
                break;
            case KeyCodes.LEFT:
                this.moveLeft = false;
                break;
            case KeyCodes.RIGHT:
                this.moveRight = false;
                break;
            case KeyCodes.Q:
                this.rotLeft = false;
                break;
            case KeyCodes.E:
                this.rotRight = false;
                break;
            case KeyCodes.R:
                this.raiseCamera = false;
                break;
            case KeyCodes.F:
                this.lowerCamera = false;
                break;
        }
    }
}

