import {CylinderGeometry, Matrix4, Mesh, MeshBasicMaterial, MeshNormalMaterial, Object3D, PerspectiveCamera, PlaneBufferGeometry, PointLight, Quaternion, SphereGeometry, TextureLoader, Vector2, Vector3} from "three";
import {EventDispatcher} from "../core/EventDispatcher";
import {Utils} from "../core/Utils";
import {TextSprite} from "../core/TextSprite";
import {XRControllerModelFactory} from "three/examples/jsm/webxr/XRControllerModelFactory";
import {LineGeometry} from "three/examples/jsm/lines/LineGeometry";
import {LineMaterial} from "three/examples/jsm/lines/LineMaterial";
import {Line2} from "three/examples/jsm/lines/Line2";
import {Viewer} from "../viewer/viewer";
import {VScene} from "../viewer/VScene";
import {App} from "../core/App";
import {ViewerEventName} from "../core/Event";
import {Group} from "three/src/objects/Group";

// @ts-ignore
const fakeCam = new PerspectiveCamera();

function toScene(vec: any, ref: any) {
    const node = ref.clone();
    node.updateMatrix();
    node.updateMatrixWorld();

    const result = vec.clone().applyMatrix4(node.matrix);
    result.z -= 0.8 * node.scale.x;

    return result;
}

function computeMove(vrControls: any, controller: any) {
    if (!controller || !controller.inputSource || !controller.inputSource.gamepad) {
        return null;
    }

    const pad = controller.inputSource.gamepad;

    const axes = pad.axes;
    // [0,1] are for touchpad, [2,3] for thumb sticks?
    let y = 0;
    if (axes.length === 2) {
        y = axes[1];
    } else if (axes.length === 4) {
        y = axes[3];
    }

    y = Math.sign(y) * (2 * y) ** 2;

    let maxSize = 0;
    // @ts-ignore
    for (let pc of viewer.scene.pointClouds) {
        let size = pc.boundingBox.min.distanceTo(pc.boundingBox.max);
        maxSize = Math.max(maxSize, size);
    }

    const multiplicator = Math.pow(maxSize, 0.5) / 2;
    const scale = vrControls.node.scale.x;
    // @ts-ignore
    const moveSpeed = viewer.getMoveSpeed();
    const amount = multiplicator * y * (moveSpeed ** 0.5) / scale;


    const rotation = new Quaternion().setFromEuler(controller.rotation);
    const dir = new Vector3(0, 0, -1);
    dir.applyQuaternion(rotation);

    let move = dir.clone().multiplyScalar(amount);

    const p1 = vrControls.toScene(controller.position);
    const p2 = vrControls.toScene(controller.position.clone().add(move));

    move = p2.clone().sub(p1);

    return move;
}

class FlyMode {
    moveFactor: number;
    dbgLabel: TextSprite | null;

    constructor(/*vrControls?: any*/) {
        this.moveFactor = 1;
        this.dbgLabel = null;
    }

    start(vrControls: any) {
        if (!this.dbgLabel) {
            this.dbgLabel = new TextSprite("abc");
            this.dbgLabel.name = "debug label";
            vrControls.viewer.sceneVR.add(this.dbgLabel);
            this.dbgLabel.visible = false;
        }
    }

    end() {
        //
    }

    update(vrControls: any, delta: any) {
        const primary = vrControls.cPrimary;
        const secondary = vrControls.cSecondary;

        let move1 = computeMove(vrControls, primary);
        let move2 = computeMove(vrControls, secondary);

        if (!move1) {
            move1 = new Vector3();
        }

        if (!move2) {
            move2 = new Vector3();
        }

        const move = move1.clone().add(move2);

        move.multiplyScalar(-delta * this.moveFactor);
        vrControls.node.position.add(move);


        const scale = vrControls.node.scale.x;

        const camVR = vrControls.viewer.renderer.xr.getCamera(fakeCam);

        const vrPos = camVR.getWorldPosition(new Vector3());
        const vrDir = camVR.getWorldDirection(new Vector3());
        vrPos.clone().add(vrDir.multiplyScalar(scale));
        const scenePos = toScene(vrPos, vrControls.node);
        const sceneDir = toScene(vrPos.clone().add(vrDir), vrControls.node).sub(scenePos);
        sceneDir.normalize().multiplyScalar(scale);
        const sceneTarget = scenePos.clone().add(sceneDir);

        vrControls.viewer.scene.view.setView(scenePos, sceneTarget);

        if (App.debug.message && this.dbgLabel) {
            this.dbgLabel.visible = true;
            this.dbgLabel.setText(App.debug.message);
            this.dbgLabel.scale.set(0.1, 0.1, 0.1);
            this.dbgLabel.position.copy(primary.position);
        }
    }
}

class TranslationMode {
    controller: any;
    startPos: any;
    debugLine: any;

    constructor() {
        this.controller = null;
        this.startPos = null;
        this.debugLine = null;
    }

    start(vrControls: any) {
        this.controller = vrControls.triggered.values().next().value;
        this.startPos = vrControls.node.position.clone();
    }

    end(/*vrControls: any*/) {
        //
    }

    update(vrControls: any/*, delta: any*/) {
        let start = this.controller.start.position;
        let end = this.controller.position;

        start = vrControls.toScene(start);
        end = vrControls.toScene(end);

        const diff = end.clone().sub(start);
        diff.set(-diff.x, -diff.y, -diff.z);

        const pos = new Vector3().addVectors(this.startPos, diff);

        vrControls.node.position.copy(pos);
    }
}

class RotScaleMode {
    line: any;
    startState: any;
    dbgLabel: TextSprite;

    constructor() {
        this.line = null;
        this.startState = null;
    }

    start(vrControls: any) {
        if (!this.line) {
            this.line = Utils.debugLine(
                vrControls.viewer.sceneVR,
                new Vector3(0, 0, 0),
                new Vector3(0, 0, 0),
                0xffff00,
            );

            this.dbgLabel = new TextSprite("abc");
            this.dbgLabel.scale.set(0.1, 0.1, 0.1);
            vrControls.viewer.sceneVR.add(this.dbgLabel);
        }

        this.line.node.visible = true;

        this.startState = vrControls.node.clone();
    }

    end(/*vrControls: any*/) {
        this.line.node.visible = false;
        this.dbgLabel.visible = false;
    }

    update(vrControls: any/*, delta: any*/) {
        const start_c1 = vrControls.cPrimary.start.position.clone();
        const start_c2 = vrControls.cSecondary.start.position.clone();
        const start_center = start_c1.clone().add(start_c2).multiplyScalar(0.5);
        const start_c1_c2 = start_c2.clone().sub(start_c1);
        const end_c1 = vrControls.cPrimary.position.clone();
        const end_c2 = vrControls.cSecondary.position.clone();
        const end_center = end_c1.clone().add(end_c2).multiplyScalar(0.5);
        const end_c1_c2 = end_c2.clone().sub(end_c1);

        const d1 = start_c1_c2.length();
        const d2 = end_c1_c2.length();

        const angleStart = new Vector2(start_c1_c2.x, start_c1_c2.z).angle();
        const angleEnd = new Vector2(end_c1_c2.x, end_c1_c2.z).angle();
        const angleDiff = angleEnd - angleStart;

        const scale = d2 / d1;

        const node = this.startState.clone();
        node.updateMatrix();
        node.matrixAutoUpdate = false;

        // @ts-ignore
        const mToOrigin = new Matrix4().makeTranslation(...toScene(start_center, this.startState).multiplyScalar(-1).toArray());
        // @ts-ignore
        const mToStart = new Matrix4().makeTranslation(...toScene(start_center, this.startState).toArray());
        const mRotate = new Matrix4().makeRotationZ(angleDiff);
        const mScale = new Matrix4().makeScale(1 / scale, 1 / scale, 1 / scale);

        node.applyMatrix4(mToOrigin);
        node.applyMatrix4(mRotate);
        node.applyMatrix4(mScale);
        node.applyMatrix4(mToStart);

        const oldScenePos = toScene(start_center, this.startState);
        const newScenePos = toScene(end_center, node);
        const toNew = oldScenePos.clone().sub(newScenePos);
        // @ts-ignore
        const mToNew = new Matrix4().makeTranslation(...toNew.toArray());
        node.applyMatrix4(mToNew);

        node.matrix.decompose(node.position, node.quaternion, node.scale);

        vrControls.node.position.copy(node.position);
        vrControls.node.quaternion.copy(node.quaternion);
        vrControls.node.scale.copy(node.scale);
        vrControls.node.updateMatrix();

        {
            const scale = vrControls.node.scale.x;
            const camVR = vrControls.viewer.renderer.xr.getCamera(fakeCam);

            const vrPos = camVR.getWorldPosition(new Vector3());
            const vrDir = camVR.getWorldDirection(new Vector3());
            vrPos.clone().add(vrDir.multiplyScalar(scale));
            const scenePos = toScene(vrPos, this.startState);
            const sceneDir = toScene(vrPos.clone().add(vrDir), this.startState).sub(scenePos);
            sceneDir.normalize().multiplyScalar(scale);
            const sceneTarget = scenePos.clone().add(sceneDir);

            vrControls.viewer.scene.view.setView(scenePos, sceneTarget);
            vrControls.viewer.setMoveSpeed(scale);
        }

        { // update "GUI"
            this.line.set(end_c1, end_c2);

            const scale = vrControls.node.scale.x;
            this.dbgLabel.visible = true;
            this.dbgLabel.position.copy(end_center);
            this.dbgLabel.setText(`scale: 1 : ${scale.toFixed(2)}`);
            this.dbgLabel.scale.set(0.05, 0.05, 0.05);
        }
    }
}

export class VRControls extends EventDispatcher {
    viewer: Viewer;
    node: Object3D;
    triggered: Set<any>;
    menu: any;
    cPrimary: any;
    cSecondary: any;
    mode_fly: FlyMode;
    mode_translate: TranslationMode;
    mode_rotScale: RotScaleMode;
    mode: any;
    scene: VScene;
    enabled: boolean;

    constructor(viewer: any) {
        super();

        this.viewer = viewer;

        viewer.addEventListener(ViewerEventName.VrStart, this.onStart.bind(this));
        viewer.addEventListener(ViewerEventName.VrEnd, this.onEnd.bind(this));

        this.node = new Object3D();
        this.node.up.set(0, 0, 1);
        this.triggered = new Set();

        let xr = viewer.renderer.xr;

        { // lights
            const light = new PointLight(0xffffff, 5, 0, 1);
            light.position.set(0, 2, 0);
            this.viewer.sceneVR.add(light)
        }

        this.menu = null;

        const controllerModelFactory = new XRControllerModelFactory();

        const sg = new SphereGeometry(1, 32, 32);
        const sm = new MeshNormalMaterial();

        { // setup primary controller
            const controller = xr.getController(0);

            const grip = xr.getControllerGrip(0);
            grip.name = "grip(0)";

            // ADD CONTROLLER MODEL
            grip.add(controllerModelFactory.createControllerModel(grip));
            this.viewer.sceneVR.add(grip);

            // ADD SPHERE
            const sphere = new Mesh(sg, sm);
            sphere.scale.set(0.005, 0.005, 0.005);

            controller.add(sphere);
            controller.visible = true;
            this.viewer.sceneVR.add(controller);

            { // ADD LINE
                const lineGeometry = new LineGeometry();

                lineGeometry.setPositions([
                    0, 0, -0.15,
                    0, 0, 0.05,
                ]);

                const lineMaterial = new LineMaterial({
                    color: 0xff0000,
                    linewidth: 2,
                    resolution: new Vector2(1000, 1000),
                });

                const line = new Line2(lineGeometry, lineMaterial);

                controller.add(line);
            }


            controller.addEventListener('connected', function (event: any) {
                controller.inputSource = event.data;
            });

            controller.addEventListener('selectstart', () => {
                this.onTriggerStart(controller)
            });
            controller.addEventListener('selectend', () => {
                this.onTriggerEnd(controller)
            });

            this.cPrimary = controller;
        }

        { // setup secondary controller
            const controller = xr.getController(1);
            const grip = xr.getControllerGrip(1);

            // ADD CONTROLLER MODEL
            const model = controllerModelFactory.createControllerModel(grip);
            grip.add(model);
            this.viewer.sceneVR.add(grip);

            // ADD SPHERE
            const sphere = new Mesh(sg, sm);
            sphere.scale.set(0.005, 0.005, 0.005);
            controller.add(sphere);
            controller.visible = true;
            this.viewer.sceneVR.add(controller);

            { // ADD LINE
                const lineGeometry = new LineGeometry();

                lineGeometry.setPositions([
                    0, 0, -0.15,
                    0, 0, 0.05,
                ]);

                const lineMaterial = new LineMaterial({
                    color: 0xff0000,
                    linewidth: 2,
                    resolution: new Vector2(1000, 1000),
                });

                const line = new Line2(lineGeometry, lineMaterial);

                controller.add(line);
            }

            controller.addEventListener('connected', (event: any) => {
                controller.inputSource = event.data;
                this.initMenu(controller);
            });

            controller.addEventListener('selectstart', () => {
                this.onTriggerStart(controller)
            });
            controller.addEventListener('selectend', () => {
                this.onTriggerEnd(controller)
            });

            this.cSecondary = controller;
        }

        this.mode_fly = new FlyMode();
        this.mode_translate = new TranslationMode();
        this.mode_rotScale = new RotScaleMode();
        this.setMode(this.mode_fly);
    }

    createSlider(label: any/*, min: any, max: any*/) {
        const sg = new SphereGeometry(1, 8, 8);
        const cg = new CylinderGeometry(1, 1, 1, 8);
        const matHandle = new MeshBasicMaterial({color: 0xff0000});
        const matScale = new MeshBasicMaterial({color: 0xff4444});
        const matValue = new MeshNormalMaterial();

        const node = new Object3D();
        node.name = 'slider';
        const nLabel = new TextSprite(`${label}: 0`);
        const nMax = new Mesh(sg, matHandle);
        const nMin = new Mesh(sg, matHandle);
        const nValue = new Mesh(sg, matValue);
        const nScale = new Mesh(cg, matScale);

        nLabel.scale.set(0.2, 0.2, 0.2);
        nLabel.position.set(0, 0.35, 0);

        nMax.scale.set(0.02, 0.02, 0.02);
        nMax.position.set(0, 0.25, 0);

        nMin.scale.set(0.02, 0.02, 0.02);
        nMin.position.set(0, -0.25, 0);

        nValue.scale.set(0.02, 0.02, 0.02);
        nValue.position.set(0, 0, 0);

        nScale.scale.set(0.005, 0.5, 0.005);

        node.add(nLabel);
        node.add(nMax);
        node.add(nMin);
        node.add(nValue);
        node.add(nScale);

        return node;
    }

    createInfo() {
        const texture = new TextureLoader().load(`${App.resourcePath}/images/vr_controller_help.jpg`);
        const plane = new PlaneBufferGeometry(1, 1, 1, 1);
        const infoMaterial = new MeshBasicMaterial({map: texture});

        return new Mesh(plane, infoMaterial);
    }

    initMenu(controller: Group) {
        if (this.menu) {
            return;
        }

        console.log('VRControls.initMenu', controller)

        const node = new Object3D();
        node.name = 'vr menu';

        let nSlider = this.createSlider("speed");
        let nInfo = this.createInfo();

        node.add(nSlider);
        node.add(nInfo);

        node.rotation.set(-1.5, 0, 0)
        node.scale.set(0.3, 0.3, 0.3);
        node.position.set(-0.2, -0.002, -0.1)

        // nInfo.position.set(0.5, 0, 0);
        nInfo.scale.set(0.8, 0.6, 0);

        // controller.add(node);

        node.position.set(-0.3, 1.2, 0.2);
        node.scale.set(0.3, 0.2, 0.3);
        node.lookAt(new Vector3(0, 1.5, 0.1));

        this.viewer.sceneVR.add(node);

        this.menu = node;

        window.vrSlider = nSlider;
        window.vrMenu = node;
    }


    toScene(vec: any) {
        const camVR = this.getCamera();
        const mat = camVR.matrixWorld;

        return vec.clone().applyMatrix4(mat);
    }

    toVR(vec: any) {
        const camVR = this.getCamera();
        const mat = camVR.matrixWorld.clone();
        mat.invert();

        return vec.clone().applyMatrix4(mat);
    }

    setMode(mode: any) {
        if (this.mode === mode) {
            return;
        }

        if (this.mode) {
            this.mode.end(this);
        }

        for (let controller of [this.cPrimary, this.cSecondary]) {
            controller.start = {
                position: controller.position.clone(),
                rotation: controller.rotation.clone(),
            };
        }

        this.mode = mode;
        this.mode.start(this);
    }

    onTriggerStart(controller: any) {
        this.triggered.add(controller);

        if (this.triggered.size === 0) {
            this.setMode(this.mode_fly);
        } else if (this.triggered.size === 1) {
            this.setMode(this.mode_translate);
        } else if (this.triggered.size === 2) {
            this.setMode(this.mode_rotScale);
        }
    }

    onTriggerEnd(controller: any) {
        this.triggered.delete(controller);

        if (this.triggered.size === 0) {
            this.setMode(this.mode_fly);
        } else if (this.triggered.size === 1) {
            this.setMode(this.mode_translate);
        } else if (this.triggered.size === 2) {
            this.setMode(this.mode_rotScale);
        }
    }

    onStart() {
        const position = this.viewer.scene.view.position.clone();
        const direction = this.viewer.scene.view.direction;
        direction.multiplyScalar(-1);

        const target = position.clone().add(direction);
        target.z = position.z;

        const scale = this.viewer.getMoveSpeed();

        this.node.position.copy(position);
        this.node.lookAt(target);
        this.node.scale.set(scale, scale, scale);
        this.node.updateMatrix();
        this.node.updateMatrixWorld();
    }

    onEnd() {
        //
    }

    setScene(scene: any) {
        this.scene = scene;
    }

    getCamera() {
        // let reference = this.viewer.scene.getActiveCamera();
        // @ts-ignore
        const camera = new PerspectiveCamera();

        // let scale = this.node.scale.x;
        const scale = this.viewer.getMoveSpeed();
        //camera.near = 0.01 / scale;
        camera.near = 0.1;
        camera.far = 1000;
        // camera.near = reference.near / scale;
        // camera.far = reference.far / scale;
        camera.up.set(0, 0, 1);
        camera.lookAt(new Vector3(0, -1, 0));
        camera.updateMatrix();
        camera.updateMatrixWorld();

        camera.position.copy(this.node.position);
        camera.rotation.copy(this.node.rotation);
        camera.scale.set(scale, scale, scale);
        camera.updateMatrix();
        camera.updateMatrixWorld();
        camera.matrixAutoUpdate = false;
        camera.parent = camera;

        return camera;
    }

    update(delta: number) {
        // if(this.mode === this.mode_fly){
        // 	let ray = new Ray(origin, direction);

        // 	for(let object of this.selectables){

        // 		if(object.intersectsRay(ray)){
        // 			object.onHit(ray);
        // 		}

        // 	}

        // }

        this.mode.update(this, delta);
    }
}
