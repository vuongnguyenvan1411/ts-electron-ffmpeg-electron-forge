import {MOUSE, Raycaster, Scene, Scene as ThreeScene, Vector2, WebGLRenderer} from "three";
import {KeyCodes} from "../core/KeyCodes";
import {Utils} from "../core/Utils";
import {EventDispatcher} from "../core/EventDispatcher";
import {Viewer} from "../viewer/viewer";
import {VScene} from "../viewer/VScene";
import {View} from "../viewer/View";
import {BoxVolume, SphereVolume} from "../utils/Volume";
import {InputHandlerEvent, VolumeEvent} from "../core/Event";

export class InputHandler extends EventDispatcher {
    viewer: Viewer;
    renderer: WebGLRenderer;
    scene: VScene;
    sceneControls: ThreeScene;
    domElement: HTMLCanvasElement;
    enabled: boolean;
    interactiveScenes: Scene[];
    interactiveObjects: Set<any>;
    inputListeners: any[];
    blacklist: Set<any>;
    drag: any;
    mouse: Vector2;
    selection: (BoxVolume | SphereVolume)[];
    hoveredElements: any[];
    pressedKeys: Record<number, boolean>;
    wheelDelta: number;
    speed: number;
    logMessages: boolean;
    viewStart: View;
    hoveredElement: any;

    constructor(viewer: Viewer) {
        super();

        this.viewer = viewer;
        this.renderer = viewer.renderer;
        this.domElement = this.renderer.domElement;
        this.enabled = true;

        this.interactiveScenes = [];
        this.interactiveObjects = new Set();
        this.inputListeners = [];
        this.blacklist = new Set();

        this.drag = null;
        this.mouse = new Vector2(0, 0);

        this.selection = [];

        this.hoveredElements = [];
        this.pressedKeys = {};

        this.wheelDelta = 0;

        this.speed = 1;

        this.logMessages = false;

        if (this.domElement.tabIndex === -1) {
            this.domElement.tabIndex = 2222;
        }

        this.domElement.addEventListener('contextmenu', (event) => event.preventDefault(), false);
        this.domElement.addEventListener('click', this.onMouseClick.bind(this), false);
        this.domElement.addEventListener('mousedown', this.onMouseDown.bind(this), false);
        this.domElement.addEventListener('mouseup', this.onMouseUp.bind(this), false);
        this.domElement.addEventListener('mousemove', this.onMouseMove.bind(this), false);
        this.domElement.addEventListener('mousewheel', this.onMouseWheel.bind(this), false);
        this.domElement.addEventListener('DOMMouseScroll', this.onMouseWheel.bind(this), false); // Firefox
        this.domElement.addEventListener('dblclick', this.onDoubleClick.bind(this));
        this.domElement.addEventListener('keydown', this.onKeyDown.bind(this));
        this.domElement.addEventListener('keyup', this.onKeyUp.bind(this));
        this.domElement.addEventListener('touchstart', this.onTouchStart.bind(this));
        this.domElement.addEventListener('touchend', this.onTouchEnd.bind(this));
        this.domElement.addEventListener('touchmove', this.onTouchMove.bind(this));
    }

    addInputListener(listener: any) {
        this.inputListeners.push(listener);
    }

    removeInputListener(listener: any) {
        this.inputListeners = this.inputListeners.filter(e => e !== listener);
    }

    getSortedListeners() {
        return this.inputListeners.sort((a, b) => {
            const ia = (a.importance !== undefined) ? a.importance : 0;
            const ib = (b.importance !== undefined) ? b.importance : 0;

            return ib - ia;
        });
    }

    onTouchStart(e: any) {
        if (this.logMessages) console.log(this.constructor.name + ': onTouchStart');

        e.preventDefault();

        if (e.touches.length === 1) {
            const rect = this.domElement.getBoundingClientRect();
            const x = e.touches[0].pageX - rect.left;
            const y = e.touches[0].pageY - rect.top;
            this.mouse.set(x, y);

            this.startDragging(null);
        }


        for (let inputListener of this.getSortedListeners()) {
            inputListener.dispatchEvent({
                type: e.type,
                touches: e.touches,
                changedTouches: e.changedTouches
            });
        }
    }

    onTouchEnd(e: any) {
        if (this.logMessages) console.log(this.constructor.name + ': onTouchEnd');

        e.preventDefault();

        for (let inputListener of this.getSortedListeners()) {
            inputListener.dispatchEvent({
                type: 'drop',
                drag: this.drag,
                viewer: this.viewer
            });
        }

        this.drag = null;

        for (let inputListener of this.getSortedListeners()) {
            inputListener.dispatchEvent({
                type: e.type,
                touches: e.touches,
                changedTouches: e.changedTouches
            });
        }
    }

    onTouchMove(e: any) {
        if (this.logMessages) console.log(this.constructor.name + ': onTouchMove');

        e.preventDefault();

        if (e.touches.length === 1) {
            const rect = this.domElement.getBoundingClientRect();
            const x = e.touches[0].pageX - rect.left;
            const y = e.touches[0].pageY - rect.top;
            this.mouse.set(x, y);

            if (this.drag) {
                this.drag.mouse = 1;

                this.drag.lastDrag.x = x - this.drag.end.x;
                this.drag.lastDrag.y = y - this.drag.end.y;

                this.drag.end.set(x, y);

                if (this.logMessages) console.log(this.constructor.name + ': drag: ');
                for (let inputListener of this.getSortedListeners()) {
                    inputListener.dispatchEvent({
                        type: 'drag',
                        drag: this.drag,
                        viewer: this.viewer
                    });
                }
            }
        }

        for (let inputListener of this.getSortedListeners()) {
            inputListener.dispatchEvent({
                type: e.type,
                touches: e.touches,
                changedTouches: e.changedTouches
            });
        }

        // DEBUG CODE
        // let debugTouches = [...e.touches, {
        //	pageX: this.domElement.clientWidth / 2,
        //	pageY: this.domElement.clientHeight / 2}];
        // for(let inputListener of this.getSortedListeners()){
        //	inputListener.dispatchEvent({
        //		type: e.type,
        //		touches: debugTouches,
        //		changedTouches: e.changedTouches
        //	});
        // }
    }

    onKeyDown(e: any) {
        if (this.logMessages) console.log(this.constructor.name + ': onKeyDown');

        // DELETE
        if (e.keyCode === KeyCodes.DELETE && this.selection.length > 0) {
            this.dispatchEvent({
                type: InputHandlerEvent.Delete,
                selection: this.selection
            });

            this.deselectAll();
        }

        this.dispatchEvent({
            type: InputHandlerEvent.Keydown,
            keyCode: e.keyCode,
            event: e
        });

        // for(let l of this.getSortedListeners()){
        //	l.dispatchEvent({
        //		type: "keydown",
        //		keyCode: e.keyCode,
        //		event: e
        //	});
        // }

        this.pressedKeys[e.keyCode] = true;

        // e.preventDefault();
    }

    onKeyUp(e: any) {
        if (this.logMessages) console.log(this.constructor.name + ': onKeyUp');

        delete this.pressedKeys[e.keyCode];

        e.preventDefault();
    }

    onDoubleClick(e: any) {
        if (this.logMessages) console.log(this.constructor.name + ': onDoubleClick');

        let consumed = false;
        for (let hovered of this.hoveredElements) {
            if (hovered._listeners && hovered._listeners['dblclick']) {
                hovered.object.dispatchEvent({
                    type: 'dblclick',
                    mouse: this.mouse,
                    object: hovered.object
                });
                consumed = true;

                break;
            }
        }

        if (!consumed) {
            for (let inputListener of this.getSortedListeners()) {
                inputListener.dispatchEvent({
                    type: 'dblclick',
                    mouse: this.mouse,
                    object: null
                });
            }
        }

        e.preventDefault();
    }

    onMouseClick(e: any) {
        if (this.logMessages) console.log(this.constructor.name + ': onMouseClick');

        e.preventDefault();
    }

    onMouseDown(e: any) {
        if (this.logMessages) console.log(this.constructor.name + ': onMouseDown');

        e.preventDefault();

        let consumed = false;
        const consume = () => {
            return consumed = true;
        };
        if (this.hoveredElements.length === 0) {
            for (let inputListener of this.getSortedListeners()) {
                inputListener.dispatchEvent({
                    type: 'mousedown',
                    viewer: this.viewer,
                    mouse: this.mouse
                });
            }
        } else {
            for (let hovered of this.hoveredElements) {
                let object = hovered.object;
                object.dispatchEvent({
                    type: 'mousedown',
                    viewer: this.viewer,
                    consume: consume
                });

                if (consumed) {
                    break;
                }
            }
        }

        if (!this.drag) {
            let target = this.hoveredElements
                .find(el => (
                    el.object._listeners &&
                    el.object._listeners['drag'] &&
                    el.object._listeners['drag'].length > 0));

            if (target) {
                this.startDragging(target.object, {location: target.point});
            } else {
                this.startDragging(null);
            }
        }

        if (this.scene) {
            this.viewStart = this.scene.view.clone();
        }
    }

    onMouseUp(e: any) {
        if (this.logMessages) console.log(this.constructor.name + ': onMouseUp');

        e.preventDefault();

        let noMovement = this.getNormalizedDrag().length() === 0;


        let consumed = false;
        const consume = () => {
            return consumed = true;
        };
        if (this.hoveredElements.length === 0) {
            for (let inputListener of this.getSortedListeners()) {
                inputListener.dispatchEvent({
                    type: 'mouseup',
                    viewer: this.viewer,
                    mouse: this.mouse,
                    consume: consume
                });

                if (consumed) {
                    break;
                }
            }
        } else {
            const hovered = this.hoveredElements
                .map(e => e.object)
                .find(e => (e._listeners && e._listeners['mouseup']));

            if (hovered) {
                hovered.dispatchEvent({
                    type: 'mouseup',
                    viewer: this.viewer,
                    consume: consume
                });
            }
        }

        if (this.drag) {
            if (this.drag.object) {
                if (this.logMessages) console.log(`${this.constructor.name}: drop ${this.drag.object.name}`);
                this.drag.object.dispatchEvent({
                    type: 'drop',
                    drag: this.drag,
                    viewer: this.viewer

                });
            } else {
                for (let inputListener of this.getSortedListeners()) {
                    inputListener.dispatchEvent({
                        type: 'drop',
                        drag: this.drag,
                        viewer: this.viewer
                    });
                }
            }

            // check for a click
            const clicked = this.hoveredElements.map(h => h.object).find(v => v === this.drag.object) !== undefined;

            if (clicked) {
                if (this.logMessages) console.log(`${this.constructor.name}: click ${this.drag.object.name}`);
                this.drag.object.dispatchEvent({
                    type: 'click',
                    viewer: this.viewer,
                    consume: consume,
                });
            }

            this.drag = null;
        }

        if (!consumed) {
            if (e.button === MOUSE.LEFT) {
                if (noMovement) {
                    let selectable = this.hoveredElements.find(el => el.object._listeners && el.object._listeners['select']);

                    if (selectable) {
                        selectable = selectable.object;

                        if (this.isSelected(selectable)) {
                            this.selection
                                .filter(e => e !== selectable)
                                .forEach(e => this.toggleSelection(e));
                        } else {
                            this.deselectAll();
                            this.toggleSelection(selectable);
                        }
                    } else {
                        this.deselectAll();
                    }
                }
            } else if ((e.button === MOUSE.RIGHT) && noMovement) {
                this.deselectAll();
            }
        }
    }

    onMouseMove(e: any) {
        e.preventDefault();

        const rect = this.domElement.getBoundingClientRect();
        const x = e.clientX - rect.left;
        const y = e.clientY - rect.top;
        this.mouse.set(x, y);

        const hoveredElements = this.getHoveredElements();
        if (hoveredElements.length > 0) {
            let names = hoveredElements.map(h => h.object.name).join(", ");
            if (this.logMessages) console.log(`${this.constructor.name}: onMouseMove; hovered: '${names}'`);
        }

        if (this.drag) {
            this.drag.mouse = e.buttons;

            this.drag.lastDrag.x = x - this.drag.end.x;
            this.drag.lastDrag.y = y - this.drag.end.y;

            this.drag.end.set(x, y);

            if (this.drag.object) {
                if (this.logMessages) console.log(this.constructor.name + ': drag: ' + this.drag.object.name);
                this.drag.object.dispatchEvent({
                    type: 'drag',
                    drag: this.drag,
                    viewer: this.viewer
                });
            } else {
                if (this.logMessages) console.log(this.constructor.name + ': drag: ');

                let dragConsumed = false;
                const consume = () => {
                    dragConsumed = true;
                };

                for (let inputListener of this.getSortedListeners()) {
                    inputListener.dispatchEvent({
                        type: 'drag',
                        drag: this.drag,
                        viewer: this.viewer,
                        consume: consume
                    });

                    if (dragConsumed) {
                        break;
                    }
                }
            }
        } else {
            const curr = hoveredElements.map(a => a.object).find(() => true);
            const prev = this.hoveredElements.map(a => a.object).find(() => true);

            if (curr !== prev) {
                if (curr) {
                    if (this.logMessages) console.log(`${this.constructor.name}: mouseover: ${curr.name}`);

                    curr.dispatchEvent({
                        type: 'mouseover',
                        object: curr,
                    });
                }
                if (prev) {
                    if (this.logMessages) console.log(`${this.constructor.name}: mouseleave: ${prev.name}`);

                    prev.dispatchEvent({
                        type: 'mouseleave',
                        object: prev,
                    });
                }
            }

            if (hoveredElements.length > 0) {
                const object = hoveredElements
                    .map(e => e.object)
                    .find((e: any) => (e._listeners && e._listeners['mousemove']));

                if (object) {
                    object.dispatchEvent({
                        type: 'mousemove',
                        object: object
                    });
                }
            }

        }

        // for (let inputListener of this.getSortedListeners()) {
        // 	inputListener.dispatchEvent({
        // 		type: 'mousemove',
        // 		object: null
        // 	});
        // }

        this.hoveredElements = hoveredElements;
    }

    onMouseWheel(e: any) {
        if (!this.enabled) return;

        if (this.logMessages) console.log(this.constructor.name + ": onMouseWheel");

        e.preventDefault();

        let delta = 0;
        if (e.wheelDelta !== undefined) { // WebKit / Opera / Explorer 9
            delta = e.wheelDelta;
        } else if (e.detail !== undefined) { // Firefox
            delta = -e.detail;
        }

        const nDelta = Math.sign(delta);

        // this.wheelDelta += Math.sign(delta);

        if (this.hoveredElement) {
            this.hoveredElement.object.dispatchEvent({
                type: 'mousewheel',
                delta: nDelta,
                object: this.hoveredElement.object
            });
        } else {
            for (let inputListener of this.getSortedListeners()) {
                inputListener.dispatchEvent({
                    type: 'mousewheel',
                    delta: nDelta,
                    object: null
                });
            }
        }
    }

    startDragging(object: any, args: any = null) {
        const name = object ? object.name : "no name";

        if (this.logMessages) console.log(`${this.constructor.name}: startDragging: '${name}'`);

        this.drag = {
            start: this.mouse.clone(),
            end: this.mouse.clone(),
            lastDrag: new Vector2(0, 0),
            startView: this.scene.view.clone(),
            object: object
        };

        if (args) {
            for (let key of Object.keys(args)) {
                this.drag[key] = args[key];
            }
        }
    }

    getMousePointCloudIntersection(/*mouse: any*/) {
        return Utils.getMousePointCloudIntersection(
            this.mouse,
            this.scene.getActiveCamera(),
            this.viewer,
            this.scene.pointClouds
        );
    }

    toggleSelection = (object: BoxVolume | SphereVolume) => {
        const oldSelection = this.selection;
        const index = this.selection.indexOf(object);

        if (index === -1) {
            this.selection.push(object);
            object.dispatchEvent({
                type: VolumeEvent.Select
            });
        } else {
            this.selection.splice(index, 1);
            object.dispatchEvent({
                type: VolumeEvent.Deselect
            });
        }

        this.dispatchEvent({
            type: InputHandlerEvent.SelectionChanged,
            oldSelection: oldSelection,
            selection: this.selection
        });
    }

    deselect = (object: BoxVolume | SphereVolume) => {
        const oldSelection = this.selection;
        const index = this.selection.indexOf(object);

        if (index >= 0) {
            this.selection.splice(index, 1);
            object.dispatchEvent({
                type: VolumeEvent.Deselect
            });

            this.dispatchEvent({
                type: InputHandlerEvent.SelectionChanged,
                oldSelection: oldSelection,
                selection: this.selection
            });
        }
    }

    deselectAll = () => {
        for (let object of this.selection) {
            object.dispatchEvent({
                type: VolumeEvent.Deselect
            });
        }

        const oldSelection = this.selection;

        if (this.selection.length > 0) {
            this.selection = [];

            this.dispatchEvent({
                type: InputHandlerEvent.SelectionChanged,
                oldSelection: oldSelection,
                selection: this.selection
            });
        }
    }

    isSelected(object: any) {
        const index = this.selection.indexOf(object);

        return index !== -1;
    }

    registerInteractiveObject(object: any) {
        this.interactiveObjects.add(object);
    }

    removeInteractiveObject(object: any) {
        this.interactiveObjects.delete(object);
    }

    registerInteractiveScene(scene: Scene) {
        const index = this.interactiveScenes.indexOf(scene);

        if (index === -1) {
            this.interactiveScenes.push(scene);
        }
    }

    unregisterInteractiveScene(scene: Scene) {
        const index = this.interactiveScenes.indexOf(scene);

        if (index > -1) {
            this.interactiveScenes.splice(index, 1);
        }
    }

    getHoveredElement() {
        const hoveredElements = this.getHoveredElements();

        if (hoveredElements.length > 0) {
            return hoveredElements[0];
        } else {
            return null;
        }
    }

    getHoveredElements() {
        const scenes = this.interactiveScenes.concat(this.scene.scene);
        const interactableListeners = ['mouseup', 'mousemove', 'mouseover', 'mouseleave', 'drag', 'drop', 'click', 'select', 'deselect'];
        const interactables: any[] = [];

        for (let scene of scenes) {
            scene.traverseVisible((node: any) => {
                if (node._listeners && node.visible && !this.blacklist.has(node)) {
                    let hasInteractableListener = interactableListeners.filter((e) => {
                        return node._listeners[e] !== undefined;
                    }).length > 0;

                    if (hasInteractableListener) {
                        interactables.push(node);
                    }
                }
            });
        }

        const camera = this.scene.getActiveCamera();
        const ray = Utils.mouseToRay(this.mouse, camera, this.domElement.clientWidth, this.domElement.clientHeight);

        const raycaster = new Raycaster();
        raycaster.ray.set(ray.origin, ray.direction);

        if (raycaster.params.Line) {
            raycaster.params.Line.threshold = 0.2;
        }

        return raycaster.intersectObjects(interactables.filter(o => o.visible), false);
    }

    setScene(scene: any) {
        this.deselectAll();

        this.scene = scene;
    }

    update(/*delta: any*/) {
        //
    }

    getNormalizedDrag() {
        if (!this.drag) {
            return new Vector2(0, 0);
        }

        const diff = new Vector2().subVectors(this.drag.end, this.drag.start);

        diff.x = diff.x / this.domElement.clientWidth;
        diff.y = diff.y / this.domElement.clientHeight;

        return diff;
    }

    getNormalizedLastDrag() {
        if (!this.drag) {
            return new Vector2(0, 0);
        }

        const lastDrag = this.drag.lastDrag.clone();

        lastDrag.x = lastDrag.x / this.domElement.clientWidth;
        lastDrag.y = lastDrag.y / this.domElement.clientHeight;

        return lastDrag;
    }
}
