import {Object3D} from "three";
import {DataNode, EventDataNode} from "rc-tree/lib/interface";
import {Key} from "react";

export interface IEDataNode extends DataNode {
    object?: Object3D;
    children?: IEDataNode[];
}

export interface IEEventDataNode extends EventDataNode<any> {
    object?: Object3D;
    props?: IEDataNode;
    node: IEEventDataNode;
    checked: boolean;
    children?: IEDataNode[];
    key: Key;
    selected: boolean;
    className: string;
    pos: string;
}

export interface IOTreeCheckInfo {
    event: 'check';
    node: IEEventDataNode;
    checked: boolean;
    nativeEvent: MouseEvent;
    checkedNodes: IEDataNode[];
    checkedNodesPositions?: {
        node: IEDataNode;
        pos: string;
    }[];
    halfCheckedKeys?: Key[];
}
