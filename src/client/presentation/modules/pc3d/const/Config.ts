import {Coordinate} from "ol/coordinate";

export class Config {
    static readonly Brand = 'I&I';
    static readonly Title = 'AutoTimelapse';
    static readonly Phone = '(+84)886885808';
    static readonly Version = '1.0.0';
    static Host = 'autotimelapse.com';
    static Domain = `https://${Config.Host}`;
    static UrlAdmin = `https://admin.${Config.Host}`;
    static UrlCdnSc = `https://cdn-sc.${Config.Host}/files`;
    static UrlCdnScIconMarker = `https://cdn-sc.${Config.Host}/files/icon/marker`;

    static Logo = `${Config.UrlCdnSc}/logo-1000.png`;

    static EPSG = 'EPSG:4326';
    static defaultView: Coordinate = [105.84312591311831, 21.006217688215973];
    static PointCloudProjection: string = "+proj=utm +zone=48 +ellps=WGS84 +datum=WGS84 +units=m";

    static BingMapKey = 'Ar84YwkzQ4fXp8noxOuG-dp9cGwCvdllJlJoESSEYf-nxpALy1DKgNIJfet1iNfA';
    static GoogleMapKey = 'AIzaSyA7lClxgJVRg0xqLXjFlf4MDe-OYiidd1w';
}
