import {Box3, BufferAttribute, BufferGeometry, Sphere, Vector3} from "three";
import {PointAttribute, PointAttributes, PointAttributeTypes} from "../../../loader/PointAttributes";
import {OctreeGeometry, OctreeGeometryNode} from "./OctreeGeometry";
import {App} from "../../../core/App";

export class NodeLoader {
    url: string;
    metadata: any;
    attributes: any;
    scale: any;
    offset: any;

    constructor(url: string) {
        this.url = url;
    }

    async load(nodeItem: any) {
        if (nodeItem.loaded || nodeItem.loading) {
            return;
        }

        nodeItem.loading = true;

        App.numNodesLoading++;

        // console.log(nodeItem.name, nodeItem.numPoints);

        // if(loadedNodes.has(nodeItem.name)){
        // 	// debugger;
        // }
        // loadedNodes.add(nodeItem.name);

        try {
            if (nodeItem.nodeType === 2) {
                await this.loadHierarchy(nodeItem);
            }

            const {byteOffset, byteSize} = nodeItem;
            const urlOctree = `${this.url}/../octree.bin`;
            const first = byteOffset;
            // let last = byteOffset + byteSize - 1n;
            const last = byteOffset + byteSize;
            let buffer;

            // if (byteSize === 0n) {
            if (byteSize === 0) {
                buffer = new ArrayBuffer(0);
                console.warn(`loaded node with 0 bytes: ${nodeItem.name}`);
            } else {
                const response = await fetch(urlOctree, {
                    headers: {
                        'content-type': 'multipart/byteranges',
                        'Range': `bytes=${first}-${last}`,
                    },
                });

                buffer = await response.arrayBuffer();
            }

            let workerPath: string;

            if (this.metadata.encoding === "BROTLI") {
                workerPath = App.getAsset('wks/2.0/DecoderWorker_brotli.js');
            } else {
                workerPath = App.getAsset('wks/2.0/DecoderWorker.js');
            }

            const worker = App.workerPool.getWorker(workerPath);

            if (worker) {
                worker.onmessage = (e: MessageEvent) => {
                    const data = e.data;
                    const buffers = data.attributeBuffers;

                    App.workerPool.returnWorker(workerPath, worker);

                    const geometry = new BufferGeometry();

                    for (let property in buffers) {
                        if (!buffers.hasOwnProperty(property)) {
                            continue;
                        }

                        const buffer = buffers[property].buffer;

                        if (property === "position") {
                            geometry.setAttribute('position', new BufferAttribute(new Float32Array(buffer), 3));
                        } else if (property === "rgba") {
                            geometry.setAttribute('rgba', new BufferAttribute(new Uint8Array(buffer), 4, true));
                        } else if (property === "NORMAL") {
                            //geometry.setAttribute('rgba', new BufferAttribute(new Uint8Array(buffer), 4, true));
                            geometry.setAttribute('normal', new BufferAttribute(new Float32Array(buffer), 3));
                        } else if (property === "INDICES") {
                            let bufferAttribute = new BufferAttribute(new Uint8Array(buffer), 4);
                            bufferAttribute.normalized = true;
                            geometry.setAttribute('indices', bufferAttribute);
                        } else {
                            const bufferAttribute = new BufferAttribute(new Float32Array(buffer), 1);

                            let batchAttribute = buffers[property].attribute;
                            // @ts-ignore
                            bufferAttribute.potree = {
                                offset: buffers[property].offset,
                                scale: buffers[property].scale,
                                preciseBuffer: buffers[property].preciseBuffer,
                                range: batchAttribute.range,
                            };

                            geometry.setAttribute(property, bufferAttribute);
                        }

                    }
                    // indices ??

                    nodeItem.density = data.density;
                    nodeItem.geometry = geometry;
                    nodeItem.loaded = true;
                    nodeItem.loading = false;

                    App.numNodesLoading--;
                };

                const pointAttributes = nodeItem.octreeGeometry.pointAttributes;
                const scale = nodeItem.octreeGeometry.scale;

                const box = nodeItem.boundingBox;
                const min = nodeItem.octreeGeometry.offset.clone().add(box.min);
                const size = box.max.clone().sub(box.min);
                const max = min.clone().add(size);
                const numPoints = nodeItem.numPoints;

                const offset = nodeItem.octreeGeometry.loader.offset;

                const message = {
                    name: nodeItem.name,
                    buffer: buffer,
                    pointAttributes: pointAttributes,
                    scale: scale,
                    min: min,
                    max: max,
                    size: size,
                    offset: offset,
                    numPoints: numPoints
                };

                worker.postMessage(message, [message.buffer]);
            }
        } catch (e) {
            nodeItem.loaded = false;
            nodeItem.loading = false;

            App.numNodesLoading--;

            console.log(`failed to load ${nodeItem.name}`);
            console.log(e);
            console.log(`trying again!`);
        }
    }

    parseHierarchy(node: any, buffer: any) {
        const view = new DataView(buffer);
        const tStart = performance.now();

        const bytesPerNode = 22;
        const numNodes = buffer.byteLength / bytesPerNode;

        const octree = node.octreeGeometry;
        // let nodes = [node];
        const nodes = new Array(numNodes);
        nodes[0] = node;
        let nodePos = 1;

        for (let i = 0; i < numNodes; i++) {
            const current = nodes[i];

            const type = view.getUint8(i * bytesPerNode);
            const childMask = view.getUint8(i * bytesPerNode + 1);
            const numPoints = view.getUint32(i * bytesPerNode + 2, true);
            const byteOffset = view.getBigInt64(i * bytesPerNode + 6, true);
            const byteSize = view.getBigInt64(i * bytesPerNode + 14, true);

            // if(byteSize === 0n){
            // 	// debugger;
            // }

            if (current.nodeType === 2) {
                // replace proxy with real node
                current.byteOffset = byteOffset;
                current.byteSize = byteSize;
                current.numPoints = numPoints;
            } else if (type === 2) {
                // load proxy
                current.hierarchyByteOffset = byteOffset;
                current.hierarchyByteSize = byteSize;
                current.numPoints = numPoints;
            } else {
                // load real node
                current.byteOffset = byteOffset;
                current.byteSize = byteSize;
                current.numPoints = numPoints;
            }

            // @ts-ignore
            if (current.byteSize === 0n) {
                // workaround for issue #1125
                // some inner nodes erroneously report >0 points even though have 0 points
                // however, they still report a byteSize of 0, so based on that we now set node.numPoints to 0
                current.numPoints = 0;
            }

            current.nodeType = type;

            if (current.nodeType === 2) {
                continue;
            }

            for (let childIndex = 0; childIndex < 8; childIndex++) {
                const childExists = ((1 << childIndex) & childMask) !== 0;

                if (!childExists) {
                    continue;
                }

                const childName = current.name + childIndex;

                const childAABB = createChildAABB(current.boundingBox, childIndex);
                const child = new OctreeGeometryNode(childName, octree, childAABB);
                child.name = childName;
                child.spacing = current.spacing / 2;
                child.level = current.level + 1;

                current.children[childIndex] = child;
                child.parent = current;

                // nodes.push(child);
                nodes[nodePos] = child;
                nodePos++;
            }

            // if((i % 500) === 0){
            // 	yield;
            // }
        }

        const duration = (performance.now() - tStart);

        if (duration > 20) {
            console.log(`duration: ${duration}ms, numNodes: ${numNodes}`);
        }
    }

    async loadHierarchy(node: any) {
        const {hierarchyByteOffset, hierarchyByteSize} = node;
        const hierarchyPath = `${this.url}/../hierarchy.bin`;

        const first = hierarchyByteOffset;
        // const last = first + hierarchyByteSize - 1n;
        const last = first + hierarchyByteSize;

        const response = await fetch(hierarchyPath, {
            headers: {
                'content-type': 'multipart/byteranges',
                'Range': `bytes=${first}-${last}`,
            },
        });

        const buffer = await response.arrayBuffer();

        this.parseHierarchy(node, buffer);

        // const promise = new Promise((resolve) => {
        //     const generator = this.parseHierarchy(node, buffer);
        //     const repeatUntilDone = () => {
        //         const result = generator.next();
        //
        //         if (result.done) {
        //             resolve();
        //         } else {
        //             requestAnimationFrame(repeatUntilDone);
        //         }
        //     };
        //
        //     repeatUntilDone();
        // });
        // await promise;
    }
}

function createChildAABB(aabb: any, index: any) {
    const tmpVec3 = new Vector3();

    const min = aabb.min.clone();
    const max = aabb.max.clone();
    const size = tmpVec3.subVectors(max, min);

    if ((index & 0b0001) > 0) {
        min.z += size.z / 2;
    } else {
        max.z -= size.z / 2;
    }

    if ((index & 0b0010) > 0) {
        min.y += size.y / 2;
    } else {
        max.y -= size.y / 2;
    }

    if ((index & 0b0100) > 0) {
        min.x += size.x / 2;
    } else {
        max.x -= size.x / 2;
    }

    return new Box3(min, max);
}

const typenameTypeAttributeMap: any = {
    "double": PointAttributeTypes.DATA_TYPE_DOUBLE,
    "float": PointAttributeTypes.DATA_TYPE_FLOAT,
    "int8": PointAttributeTypes.DATA_TYPE_INT8,
    "uint8": PointAttributeTypes.DATA_TYPE_UINT8,
    "int16": PointAttributeTypes.DATA_TYPE_INT16,
    "uint16": PointAttributeTypes.DATA_TYPE_UINT16,
    "int32": PointAttributeTypes.DATA_TYPE_INT32,
    "uint32": PointAttributeTypes.DATA_TYPE_UINT32,
    "int64": PointAttributeTypes.DATA_TYPE_INT64,
    "uint64": PointAttributeTypes.DATA_TYPE_UINT64,
}

export class OctreeLoader {
    static parseAttributes(jsonAttributes: any) {
        const attributes = new PointAttributes();
        const replacements: Record<string, string> = {
            rgb: "rgba",
        };

        for (const jsonAttribute of jsonAttributes) {
            // let {name, description, size, numElements, elementSize, min, max} = jsonAttribute;
            const {name, numElements, min, max} = jsonAttribute;
            const type = typenameTypeAttributeMap[jsonAttribute.type];
            const potreeAttributeName = replacements[name] ? replacements[name] : name;
            const attribute = new PointAttribute(potreeAttributeName, type, numElements);

            if (numElements === 1) {
                attribute.range = [min[0], max[0]];
            } else {
                attribute.range = [min, max];
            }

            if (name === "gps-time") { // HACK: Guard against bad gpsTime range in metadata, see potree/potree#909
                if (attribute.range[0] === attribute.range[1]) {
                    attribute.range[1] += 1;
                }
            }

            attribute.initialRange = attribute.range;

            attributes.add(attribute);
        }

        {
            // check if it has normals
            const hasNormals =
                attributes.attributes.find(a => a.name === "NormalX") !== undefined
                && attributes.attributes.find(a => a.name === "NormalY") !== undefined
                && attributes.attributes.find(a => a.name === "NormalZ") !== undefined;

            if (hasNormals) {
                const vector = {
                    name: "NORMAL",
                    attributes: ["NormalX", "NormalY", "NormalZ"],
                };
                attributes.addVector(vector);
            }
        }

        return attributes;
    }

    static async load(url: string) {
        const response = await fetch(url);
        const metadata = await response.json();

        const attributes = OctreeLoader.parseAttributes(metadata.attributes);

        const loader = new NodeLoader(url);
        loader.metadata = metadata;
        loader.attributes = attributes;
        loader.scale = metadata.scale;
        loader.offset = metadata.offset;

        const octree = new OctreeGeometry();
        octree.url = url;
        octree.spacing = metadata.spacing;
        octree.scale = metadata.scale;

        // let aPosition = metadata.attributes.find(a => a.name === "position");
        // octree

        const min = new Vector3(...metadata.boundingBox.min);
        const max = new Vector3(...metadata.boundingBox.max);
        const boundingBox = new Box3(min, max);

        const offset = min.clone();
        boundingBox.min.sub(offset);
        boundingBox.max.sub(offset);

        octree.projection = metadata.projection;
        octree.boundingBox = boundingBox;
        octree.tightBoundingBox = boundingBox.clone();
        octree.boundingSphere = boundingBox.getBoundingSphere(new Sphere());
        octree.tightBoundingSphere = boundingBox.getBoundingSphere(new Sphere());
        octree.offset = offset;
        octree.pointAttributes = OctreeLoader.parseAttributes(metadata.attributes);
        octree.loader = loader;

        const root = new OctreeGeometryNode("r", octree, boundingBox);
        root.level = 0;
        root.nodeType = 2;
        // root.hierarchyByteOffset = 0n;
        root.hierarchyByteSize = BigInt(metadata.hierarchy.firstChunkSize);
        root.hasChildren = false;
        root.spacing = octree.spacing;
        root.byteOffset = 0;

        octree.root = root;

        await loader.load(root);

        return {
            geometry: octree,
        };
    }
}
