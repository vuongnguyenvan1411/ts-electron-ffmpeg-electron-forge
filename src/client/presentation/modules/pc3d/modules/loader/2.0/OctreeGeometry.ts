import {Sphere} from "three";
import {App} from "../../../core/App";

export class OctreeGeometry {
    url: string | null;
    spacing: number;
    boundingBox: any;
    root: any;
    pointAttributes: any;
    loader: any;
    scale: any;
    projection: any;
    tightBoundingBox: any;
    boundingSphere: any;
    tightBoundingSphere: any;
    offset: any;
    fallbackProjection: any;

    constructor() {
        this.url = null;
        this.spacing = 0;
        this.boundingBox = null;
        this.root = null;
        this.pointAttributes = null;
        this.loader = null;
    }
}

export class OctreeGeometryNode {
    static IDCount: number = 0;

    id: number;
    name: string;
    index: number;
    octreeGeometry: OctreeGeometry;
    boundingBox: any;
    boundingSphere: any;
    children: any;
    numPoints: number;
    level: any;
    oneTimeDisposeHandlers: any[];
    loaded: boolean;
    geometry: any;
    parent: any;
    spacing: number;
    nodeType: number;
    hierarchyByteSize: any;
    hasChildren: boolean;
    byteOffset: number;

    constructor(name: string, octreeGeometry: OctreeGeometry, boundingBox: any) {
        this.id = OctreeGeometryNode.IDCount++;
        this.name = name;
        this.index = parseInt(name.charAt(name.length - 1));
        this.octreeGeometry = octreeGeometry;
        this.boundingBox = boundingBox;
        this.boundingSphere = boundingBox.getBoundingSphere(new Sphere());
        this.children = {};
        this.numPoints = 0;
        this.level = null;
        this.oneTimeDisposeHandlers = [];
    }

    isGeometryNode() {
        return true;
    }

    getLevel() {
        return this.level;
    }

    isTreeNode() {
        return false;
    }

    isLoaded() {
        return this.loaded;
    }

    getBoundingSphere() {
        return this.boundingSphere;
    }

    getBoundingBox() {
        return this.boundingBox;
    }

    getChildren() {
        const children = [];

        for (let i = 0; i < 8; i++) {
            if (this.children[i]) {
                children.push(this.children[i]);
            }
        }

        return children;
    }

    load() {
        if (App.numNodesLoading >= App.maxNodesLoading) {
            return;
        }

        this.octreeGeometry.loader.load(this);
    }

    getNumPoints() {
        return this.numPoints;
    }

    dispose() {
        if (this.geometry && this.parent != null) {
            this.geometry.dispose();
            this.geometry = null;
            this.loaded = false;

            // this.dispatchEvent( { type: 'dispose' } );
            for (let i = 0; i < this.oneTimeDisposeHandlers.length; i++) {
                let handler = this.oneTimeDisposeHandlers[i];

                handler();
            }
            this.oneTimeDisposeHandlers = [];
        }
    }
}
