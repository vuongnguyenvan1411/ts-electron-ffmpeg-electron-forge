import {CatmullRomCurve3, MathUtils, Object3D, Vector2, Vector3} from "three";
import {EventDispatcher} from "../../core/EventDispatcher";
import {Utils} from "../../core/Utils";
import {Line2} from "three/examples/jsm/lines/Line2";
import {LineGeometry} from "three/examples/jsm/lines/LineGeometry";
import {LineMaterial} from "three/examples/jsm/lines/LineMaterial";
import {Viewer} from "../../viewer/viewer";
import {CameraAnimationEvent, MouseEventName, ViewerEventName} from "../../core/Event";

type TArgsDefaultFromView = {
    name: string,
    properties?: Record<string, any>
}

type TCPHandle = {
    svg: SVGElement;
}

export class ControlPoint {
    position: Vector3;
    target: Vector3;
    positionHandle: TCPHandle;
    targetHandle: TCPHandle;

    constructor() {
        this.position = new Vector3(0, 0, 0);
        this.target = new Vector3(0, 0, 0);
    }
}

export class CameraAnimation extends EventDispatcher {
    viewer: Viewer;
    selectedElement: SVGElement | null;
    controlPoints: ControlPoint[];
    uuid: string;
    node: Object3D;
    frustum: Line2;
    name: string;
    duration: number;
    t: number;
    visible: boolean;
    curveType: string;
    line: Line2;
    targetLine: Line2;
    cameraCurve: CatmullRomCurve3;
    targetCurve: CatmullRomCurve3;
    protected _properties: Record<string, any>;

    constructor(viewer: Viewer) {
        super();

        this.viewer = viewer;

        this.selectedElement = null;

        this.controlPoints = [];

        this.uuid = MathUtils.generateUUID();

        this.node = new Object3D();
        this.node.name = "camera animation";
        this.viewer.scene.scene.add(this.node);

        this.frustum = this.createFrustum();
        this.node.add(this.frustum);

        this.name = "Camera Animation";
        this.duration = 5;
        this.t = 0;
        // "centripetal", "chordal", "catmullrom"
        this.curveType = "centripetal"
        this.visible = true;

        this.viewer.addEventListener(ViewerEventName.Update, this.update);
        this.createPath();
    }

    static defaultFromView(viewer: Viewer, args?: TArgsDefaultFromView) {
        const animation = new CameraAnimation(viewer);

        if (args) {
            animation.name = args.name ?? 'Animation';

            if (args.properties) {
                animation.setProperties(args.properties);
            }
        }

        const camera = viewer.scene.getActiveCamera();
        const target = viewer.scene.view.getPivot();

        const cpCenter = new Vector3(
            0.3 * camera.position.x + 0.7 * target.x,
            0.3 * camera.position.y + 0.7 * target.y,
            0.3 * camera.position.z + 0.7 * target.z,
        );

        const targetCenter = new Vector3(
            0.05 * camera.position.x + 0.95 * target.x,
            0.05 * camera.position.y + 0.95 * target.y,
            0.05 * camera.position.z + 0.95 * target.z,
        );

        const r = camera.position.distanceTo(target) * 0.3;

        //const dir = target.clone().sub(camera.position).normalize();
        const angle = Utils.computeAzimuth(camera.position, target);

        const n = 5;

        for (let i = 0; i < n; i++) {
            let u = 1.5 * Math.PI * (i / n) + angle;

            const dx = r * Math.cos(u);
            const dy = r * Math.sin(u);

            const cpPos = [
                cpCenter.x + dx,
                cpCenter.y + dy,
                cpCenter.z,
            ];

            const targetPos = [
                targetCenter.x + dx * 0.1,
                targetCenter.y + dy * 0.1,
                targetCenter.z,
            ];

            const cp = animation.createControlPoint();
            // @ts-ignore
            cp.position.set(...cpPos);
            // @ts-ignore
            cp.target.set(...targetPos);
        }

        return animation;
    }

    update = () => {
        const camera = this.viewer.scene.getActiveCamera();
        const {width, height} = this.viewer.renderer.getSize(new Vector2());

        this.node.visible = this.visible;

        for (const cp of this.controlPoints) {
            { // position
                const projected = cp.position.clone().project(camera);

                const visible = this.visible && (projected.z < 1 && projected.z > -1);

                if (visible) {
                    const x = width * (projected.x * 0.5 + 0.5);
                    const y = height - height * (projected.y * 0.5 + 0.5);

                    if (cp.positionHandle) {
                        cp.positionHandle.svg.style.left = (x - cp.positionHandle.svg.clientWidth / 2).toString();
                        cp.positionHandle.svg.style.top = (y - cp.positionHandle.svg.clientHeight / 2).toString();
                        cp.positionHandle.svg.style.display = "";
                    }
                } else {
                    if (cp.positionHandle) {
                        cp.positionHandle.svg.style.display = "none";
                    }
                }
            }

            { // target
                const projected = cp.target.clone().project(camera);

                const visible = this.visible && (projected.z < 1 && projected.z > -1);

                if (visible) {
                    const x = width * (projected.x * 0.5 + 0.5);
                    const y = height - height * (projected.y * 0.5 + 0.5);

                    if (cp.targetHandle) {
                        cp.targetHandle.svg.style.left = (x - cp.targetHandle.svg.clientWidth / 2).toString();
                        cp.targetHandle.svg.style.top = (y - cp.targetHandle.svg.clientHeight / 2).toString();
                        cp.targetHandle.svg.style.display = "";
                    }
                } else {
                    if (cp.targetHandle) {
                        cp.targetHandle.svg.style.display = "none";
                    }
                }
            }
        }

        this.line.material.resolution.set(width, height);

        this.updatePath();

        { // frustum
            const frame = this.at(this.t);
            const frustum = this.frustum;

            frustum.position.copy(frame.position);
            // @ts-ignore
            frustum.lookAt(...frame.target.toArray());
            frustum.scale.set(20, 20, 20);

            frustum.material.resolution.set(width, height);
        }
    }

    createControlPoint(index?: number) {
        if (index === undefined) {
            index = this.controlPoints.length;
        }

        const cp = new ControlPoint();

        if (this.controlPoints.length >= 2 && index === 0) {
            const cp1 = this.controlPoints[0];
            const cp2 = this.controlPoints[1];

            const dir = cp1.position.clone().sub(cp2.position).multiplyScalar(0.5);
            cp.position.copy(cp1.position).add(dir);

            const tDir = cp1.target.clone().sub(cp2.target).multiplyScalar(0.5);
            cp.target.copy(cp1.target).add(tDir);
        } else if (this.controlPoints.length >= 2 && index === this.controlPoints.length) {
            const cp1 = this.controlPoints[this.controlPoints.length - 2];
            const cp2 = this.controlPoints[this.controlPoints.length - 1];

            const dir = cp2.position.clone().sub(cp1.position).multiplyScalar(0.5);
            cp.position.copy(cp1.position).add(dir);

            const tDir = cp2.target.clone().sub(cp1.target).multiplyScalar(0.5);
            cp.target.copy(cp2.target).add(tDir);
        } else if (this.controlPoints.length >= 2) {
            const cp1 = this.controlPoints[index - 1];
            const cp2 = this.controlPoints[index];

            cp.position.copy(cp1.position.clone().add(cp2.position).multiplyScalar(0.5));
            cp.target.copy(cp1.target.clone().add(cp2.target).multiplyScalar(0.5));
        }

        // cp.position.copy(viewer.scene.view.position);
        // cp.target.copy(viewer.scene.view.getPivot());

        cp.positionHandle = this.createHandle(cp.position, index + 1);
        cp.targetHandle = this.createHandle(cp.target, index + 1);

        this.controlPoints.splice(index, 0, cp);

        this.dispatchEvent({
            type: CameraAnimationEvent.ControlPointAdded,
            controlPoint: cp,
            index: index
        });

        this.reOrder();

        return cp;
    }

    onRemove = () => {
        this.viewer.removeEventListener(ViewerEventName.Update, this.update);

        this.viewer.scene.scene.remove(this.node);

        this.controlPoints.forEach((item) => this.removeControlPoint(item));
    }

    removeControlPoint(cp: ControlPoint) {
        this.controlPoints = this.controlPoints.filter(_cp => _cp !== cp);

        this.dispatchEvent({
            type: CameraAnimationEvent.ControlPointRemoved,
            controlPoint: cp,
        });

        if (cp.positionHandle) {
            cp.positionHandle.svg.remove();
        }

        if (cp.targetHandle) {
            cp.targetHandle.svg.remove();
        }

        this.reOrder();

        // TODO destroy cp
    }

    createPath() {
        { // position
            const geometry = new LineGeometry();
            const material = new LineMaterial({
                color: 0x00ff00,
                dashSize: 5,
                gapSize: 2,
                linewidth: 2,
                resolution: new Vector2(1000, 1000),
            });
            const line = new Line2(geometry, material);

            this.line = line;
            this.node.add(line);
        }

        { // target
            const geometry = new LineGeometry();

            let material = new LineMaterial({
                color: 0x0000ff,
                dashSize: 5,
                gapSize: 2,
                linewidth: 2,
                resolution: new Vector2(1000, 1000),
            });

            const line = new Line2(geometry, material);

            this.targetLine = line;
            this.node.add(line);
        }
    }

    createFrustum() {
        const f = 0.3;

        const positions = [
            0, 0, 0,
            -f, -f, +1,

            0, 0, 0,
            f, -f, +1,

            0, 0, 0,
            f, f, +1,

            0, 0, 0,
            -f, f, +1,

            -f, -f, +1,
            f, -f, +1,

            f, -f, +1,
            f, f, +1,

            f, f, +1,
            -f, f, +1,

            -f, f, +1,
            -f, -f, +1,
        ];

        const geometry = new LineGeometry();

        geometry.setPositions(positions);
        // @ts-ignore
        geometry.verticesNeedUpdate = true;
        geometry.computeBoundingSphere();

        let material = new LineMaterial({
            color: 0xff0000,
            linewidth: 2,
            resolution: new Vector2(1000, 1000),
        });

        const line = new Line2(geometry, material);
        line.computeLineDistances();

        return line;
    }

    updatePath() {
        { // positions
            const positions = this.controlPoints.map(cp => cp.position);
            const first = positions[0];
            const curve = new CatmullRomCurve3(positions, false, this.curveType);
            const n = 100;

            const curvePositions = [];

            for (let k = 0; k <= n; k++) {
                const t = k / n;

                const position = curve.getPoint(t).sub(first);

                curvePositions.push(position.x, position.y, position.z);
            }

            this.line.geometry.setPositions(curvePositions);
            // @ts-ignore
            this.line.geometry.verticesNeedUpdate = true;
            this.line.geometry.computeBoundingSphere();
            this.line.position.copy(first);
            this.line.computeLineDistances();

            this.cameraCurve = curve;
        }

        { // targets
            const positions = this.controlPoints.map(cp => cp.target);
            const first = positions[0];

            const curve = new CatmullRomCurve3(positions, false, this.curveType);

            const n = 100;

            const curvePositions = [];
            for (let k = 0; k <= n; k++) {
                const t = k / n;

                const position = curve.getPoint(t).sub(first);

                curvePositions.push(position.x, position.y, position.z);
            }

            this.targetLine.geometry.setPositions(curvePositions);
            // @ts-ignore
            this.targetLine.geometry.verticesNeedUpdate = true;
            this.targetLine.geometry.computeBoundingSphere();
            this.targetLine.position.copy(first);
            this.targetLine.computeLineDistances();

            this.targetCurve = curve;
        }
    }

    at(t: number) {
        if (t > 1) {
            t = 1;
        } else if (t < 0) {
            t = 0;
        }

        const camPos = this.cameraCurve.getPointAt(t);
        const target = this.targetCurve.getPointAt(t);

        return {
            position: camPos,
            target: target,
        };
    }

    setT(t: number) {
        this.t = t;
    }

    createHandle(vector: Vector3, order?: number) {
        const svgNs = "http://www.w3.org/2000/svg";
        const svg = document.createElementNS(svgNs, "svg");

        svg.setAttribute("width", "2em");
        svg.setAttribute("height", "2em");
        svg.setAttribute("position", "absolute");

        svg.style.left = "50px";
        svg.style.top = "50px";
        svg.style.position = "absolute";
        svg.style.zIndex = "999";

        const circle = document.createElementNS(svgNs, 'circle');
        circle.setAttributeNS(null, 'cx', "1em");
        circle.setAttributeNS(null, 'cy', "1em");
        circle.setAttributeNS(null, 'r', "0.5em");
        circle.setAttributeNS(null, 'style', 'fill: red; stroke: black; stroke-width: 0.1em;');
        svg.appendChild(circle);
        if (order !== undefined) {
            const text = document.createElementNS(svgNs, 'text');
            text.setAttributeNS(null, 'x', "50%");
            text.setAttributeNS(null, 'y', "50%");
            text.setAttributeNS(null, 'fill', "white");
            text.setAttributeNS(null, 'font-size', "11");
            text.setAttributeNS(null, 'font-weight', "bold");
            text.setAttributeNS(null, 'text-anchor', "middle");
            text.setAttributeNS(null, 'cursor', "default");
            text.innerHTML = order.toString();
            svg.appendChild(text);
        }

        const element = this.viewer.renderer.domElement.parentElement!;
        element.appendChild(svg);

        const startDrag = () => {
            this.selectedElement = svg;

            document.addEventListener(MouseEventName.Move, drag);
        };

        const endDrag = () => {
            this.selectedElement = null;

            document.removeEventListener(MouseEventName.Move, drag);
        };

        const drag = (evt: MouseEvent) => {
            if (this.selectedElement) {
                evt.preventDefault();

                const rect = this.viewer.renderer.domElement.getBoundingClientRect();

                const x = evt.clientX - rect.x;
                const y = evt.clientY - rect.y;

                const {width, height} = this.viewer.renderer.getSize(new Vector2());
                const camera = this.viewer.scene.getActiveCamera();
                //const cp = this.controlPoints.find(cp => cp.handle.svg === svg);
                const projected = vector.clone().project(camera);

                projected.x = ((x / width) - 0.5) / 0.5;
                projected.y = (-(y - height) / height - 0.5) / 0.5;

                const unProjected = projected.clone().unproject(camera);
                vector.set(unProjected.x, unProjected.y, unProjected.z);
            }
        };

        svg.addEventListener(MouseEventName.Down, startDrag);
        svg.addEventListener(MouseEventName.Up, endDrag);

        return {
            svg: svg,
        };
    }

    setVisible(visible: boolean) {
        this.node.visible = visible;

        const display = visible ? "" : "none";

        for (const cp of this.controlPoints) {
            if (cp.positionHandle) {
                cp.positionHandle.svg.style.display = display;
            }

            if (cp.targetHandle) {
                cp.targetHandle.svg.style.display = display;
            }
        }

        this.visible = visible;
    }

    setDuration(duration: number) {
        this.duration = duration;
    }

    getDuration() {
        return this.duration;
    }

    play() {
        const tStart = performance.now();
        const duration = this.duration;

        const originalVisible = this.visible;
        this.setVisible(false);

        const onUpdate = () => {
            const tNow = performance.now();
            const elapsed = (tNow - tStart) / 1000;
            const t = elapsed / duration;

            this.setT(t);

            const frame = this.at(t);

            this.viewer.scene.view.position.copy(frame.position);
            this.viewer.scene.view.lookAt(frame.target);

            this.dispatchEvent({
                type: CameraAnimationEvent.Playing,
                time: t * duration,
                duration: duration
            })

            if (t > 1) {
                this.setVisible(originalVisible);

                this.viewer.removeEventListener(ViewerEventName.Update, onUpdate);

                this.dispatchEvent({
                    type: CameraAnimationEvent.Stop,
                })
            }
        };

        this.viewer.addEventListener(ViewerEventName.Update, onUpdate);

        this.dispatchEvent({
            type: CameraAnimationEvent.Start,
        })
    }

    reOrder() {
        this.controlPoints.forEach((item, index) => {
            if (item.positionHandle) {
                const text = item.positionHandle.svg.querySelector('text');

                if (text) {
                    text.innerHTML = (index + 1).toString();
                }
            }

            if (item.targetHandle) {
                const text = item.targetHandle.svg.querySelector('text');

                if (text) {
                    text.innerHTML = (index + 1).toString();
                }
            }
        })
    }

    get(key: string) {
        return this._properties[key];
    }

    set(key: string, value: any) {
        return this._properties[key] = value;
    }

    getKeys() {
        return Object.keys(this._properties);
    }

    getProperties() {
        return this._properties;
    }

    setProperties(data: Record<string, any>) {
        return this._properties = data;
    }

    hasProperties(key?: string) {
        if (key) {
            return this._properties.hasOwnProperty(key);
        } else {
            return this.getKeys.length > 0;
        }
    }
}
