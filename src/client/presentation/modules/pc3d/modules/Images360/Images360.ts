import {BackSide, MathUtils, Mesh, MeshBasicMaterial, Object3D, Raycaster, RepeatWrapping, SphereGeometry, TextureLoader, Vector3} from "three";
import {EventDispatcher} from "../../core/EventDispatcher";
import {Viewer} from "../../viewer/viewer";
import {Utils} from "../../core/Utils";
import {ViewerEventName} from "../../core/Event";

let sg = new SphereGeometry(1, 8, 8);
let sgHigh = new SphereGeometry(1, 128, 128);

let sm = new MeshBasicMaterial({side: BackSide});
let smHovered = new MeshBasicMaterial({side: BackSide, color: 0xff0000});

let rayCaster = new Raycaster();
let currentlyHovered: any = null;

let previousView: any = {
    controls: null,
    position: null,
    target: null,
};

class Image360 {
    file: any;
    time: any;
    longitude: any;
    latitude: any;
    altitude: any;
    course: any;
    pitch: any;
    roll: any;
    mesh: any;
    position: any;

    constructor(file: any, time: any, longitude: any, latitude: any, altitude: any, course: any, pitch: any, roll: any) {
        this.file = file;
        this.time = time;
        this.longitude = longitude;
        this.latitude = latitude;
        this.altitude = altitude;
        this.course = course;
        this.pitch = pitch;
        this.roll = roll;
        this.mesh = null;
    }
}

export class Images360 extends EventDispatcher {
    viewer: Viewer;
    selectingEnabled: boolean;
    images: any[];
    node: Object3D;
    sphere: Mesh;
    protected _visible: boolean;
    focusedImage: any;
    elUnfocus: HTMLInputElement;
    domRoot: HTMLElement;

    constructor(viewer: Viewer) {
        super();

        this.viewer = viewer;

        this.selectingEnabled = true;

        this.images = [];
        this.node = new Object3D();

        this.sphere = new Mesh(sgHigh, sm);
        this.sphere.visible = false;
        this.sphere.scale.set(1000, 1000, 1000);
        this.node.add(this.sphere);
        this._visible = true;
        // this.node.add(label);

        this.focusedImage = null;

        let elUnfocus = document.createElement("input");
        elUnfocus.type = "button";
        elUnfocus.value = "unfocus";
        elUnfocus.style.position = "absolute";
        elUnfocus.style.right = "10px";
        elUnfocus.style.bottom = "10px";
        elUnfocus.style.zIndex = "10000";
        elUnfocus.style.fontSize = "2em";
        elUnfocus.addEventListener("click", () => this.unfocus());
        this.elUnfocus = elUnfocus;

        this.domRoot = viewer.renderer.domElement.parentElement!;
        this.domRoot.appendChild(elUnfocus);
        this.elUnfocus.style.display = "none";

        viewer.addEventListener(ViewerEventName.Update, () => {
            this.update(/*viewer*/);
        });
        viewer.inputHandler.addInputListener(this);

        this.addEventListener("mousedown", () => {
            if (currentlyHovered) {
                this.focus(currentlyHovered.image360);
            }
        });

    };

    set visible(visible) {
        if (this._visible === visible) {
            return;
        }

        for (const image of this.images) {
            image.mesh.visible = visible && (this.focusedImage == null);
        }

        this.sphere.visible = visible && (this.focusedImage != null);
        this._visible = visible;
        this.dispatchEvent({
            type: "visibility_changed",
            images: this,
        });
    }

    get visible() {
        return this._visible;
    }

    focus(image360: any) {
        if (this.focusedImage !== null) {
            this.unfocus();
        }

        previousView = {
            controls: this.viewer.controls,
            position: this.viewer.scene.view.position.clone(),
            target: this.viewer.scene.view.getPivot(),
        };

        this.viewer.setControls(this.viewer.orbitControls);
        this.viewer.orbitControls.doubleClockZoomEnabled = false;

        for (let image of this.images) {
            image.mesh.visible = false;
        }

        this.selectingEnabled = false;

        this.sphere.visible = false;

        this.load(image360).then(() => {
            this.sphere.visible = true;
            // @ts-ignore
            this.sphere.material.map = image360.texture;
            // @ts-ignore
            this.sphere.material.needsUpdate = true;
        });

        { // orientation
            let {course, pitch, roll} = image360;
            this.sphere.rotation.set(
                MathUtils.degToRad(+roll + 90),
                MathUtils.degToRad(-pitch),
                MathUtils.degToRad(-course + 90),
                "ZYX"
            );
        }

        // @ts-ignore
        this.sphere.position.set(...image360.position);

        let target = new Vector3(...image360.position);
        let dir = target.clone().sub(this.viewer.scene.view.position).normalize();
        let move = dir.multiplyScalar(0.000001);
        let newCamPos = target.clone().sub(move);

        this.viewer.scene.view.setView(
            newCamPos,
            target,
            500
        );

        this.focusedImage = image360;
        this.elUnfocus.style.display = "";
    }

    unfocus() {
        this.selectingEnabled = true;

        for (let image of this.images) {
            image.mesh.visible = true;
        }

        let image = this.focusedImage;

        if (image === null) {
            return;
        }

        // @ts-ignore
        this.sphere.material.map = null;
        // @ts-ignore
        this.sphere.material.needsUpdate = true;
        this.sphere.visible = false;

        // let pos = this.viewer.scene.view.position;
        // let target = this.viewer.scene.view.getPivot();
        // let dir = target.clone().sub(pos).normalize();
        // let move = dir.multiplyScalar(10);
        // let newCamPos = target.clone().sub(move);

        this.viewer.orbitControls.doubleClockZoomEnabled = true;
        this.viewer.setControls(previousView.controls);

        this.viewer.scene.view.setView(
            previousView.position,
            previousView.target,
            500
        );

        this.focusedImage = null;

        this.elUnfocus.style.display = "none";
    }

    load(image360: any) {
        return new Promise(resolve => {
            let texture = new TextureLoader().load(image360.file, resolve);
            texture.wrapS = RepeatWrapping;
            texture.repeat.x = -1;

            image360.texture = texture;
        });
    }

    handleHovering() {
        let mouse = this.viewer.inputHandler.mouse;
        let camera = this.viewer.scene.getActiveCamera();
        let domElement = this.viewer.renderer.domElement;

        let ray = Utils.mouseToRay(mouse, camera, domElement.clientWidth, domElement.clientHeight);

        // let tStart = performance.now();
        rayCaster.ray.copy(ray);
        let intersections = rayCaster.intersectObjects(this.node.children);

        if (intersections.length === 0) {
            // label.visible = false;

            return;
        }

        let intersection = intersections[0];
        currentlyHovered = intersection.object;
        currentlyHovered.material = smHovered;

        //label.visible = true;
        //label.setText(currentlyHovered.image360.file);
        //currentlyHovered.getWorldPosition(label.position);
    }

    update() {
        if (currentlyHovered) {
            currentlyHovered.material = sm;
            currentlyHovered = null;
        }

        if (this.selectingEnabled) {
            this.handleHovering();
        }
    }
}


export class Images360Loader {
    static async load(url: any, viewer: any, params: any = {}) {
        if (!params.transform) {
            params.transform = {
                forward: (a: any) => a,
            };
        }

        let response = await fetch(`${url}/coordinates.txt`);
        let text = await response.text();

        let lines = text.split(/\r?\n/);
        let coordinateLines = lines.slice(1);

        let images360 = new Images360(viewer);

        for (let line of coordinateLines) {
            if (line.trim().length === 0) {
                continue;
            }

            let tokens: any = line.split(/\t/);

            let [filename, time, long, lat, alt, course, pitch, roll] = tokens;
            time = parseFloat(time);
            long = parseFloat(long);
            lat = parseFloat(lat);
            alt = parseFloat(alt);
            course = parseFloat(course);
            pitch = parseFloat(pitch);
            roll = parseFloat(roll);

            filename = filename.replace(/"/g, "");
            let file = `${url}/${filename}`;

            let image360 = new Image360(file, time, long, lat, alt, course, pitch, roll);

            let xy = params.transform.forward([long, lat]);
            image360.position = [...xy, alt];

            images360.images.push(image360);
        }

        Images360Loader.createSceneNodes(images360, params.transform);

        return images360;

    }

    static createSceneNodes(images360: any, transform: any) {
        for (let image360 of images360.images) {
            let {longitude, latitude, altitude} = image360;
            let xy = transform.forward([longitude, latitude]);

            let mesh = new Mesh(sg, sm);
            // @ts-ignore
            mesh.position.set(...xy, altitude);
            mesh.scale.set(1, 1, 1);
            mesh.material.transparent = true;
            mesh.material.opacity = 0.75;
            // @ts-ignore
            mesh.image360 = image360;

            { // orientation
                const {course, pitch, roll} = image360;
                mesh.rotation.set(
                    MathUtils.degToRad(+roll + 90),
                    MathUtils.degToRad(-pitch),
                    MathUtils.degToRad(-course + 90),
                    "ZYX"
                );
            }

            images360.node.add(mesh);

            image360.mesh = mesh;
        }
    }
}


