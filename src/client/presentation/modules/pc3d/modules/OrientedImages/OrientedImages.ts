import {BufferAttribute, BufferGeometry, DoubleSide, Line, LineBasicMaterial, MathUtils, Mesh, Object3D, PerspectiveCamera, PlaneGeometry, Raycaster, ShaderMaterial, Texture, TextureLoader, Vector2, Vector3} from "three";
import {OrientedImageControls} from "./OrientedImageControls";
import {EventDispatcher} from "../../core/EventDispatcher";
import {PolygonClipVolume} from "../../utils/PolygonClipVolume";
import {Viewer} from "../../viewer/viewer";
import {App} from "../../core/App";
import {ViewerEventName} from "../../core/Event";

/**
 * @see https://support.pix4d.com/hc/en-us/articles/205675256-How-are-yaw-pitch-roll-defined
 * @see https://support.pix4d.com/hc/en-us/articles/202558969-How-are-omega-phi-kappa-defined
 */
function createMaterial() {
    let vertexShader = `
	uniform float uNear;
	varying vec2 vUV;
	varying vec4 vDebug;
	
	void main(){
		vDebug = vec4(0.0, 1.0, 0.0, 1.0);
		vec4 modelViewPosition = modelViewMatrix * vec4(position, 1.0);
		// make sure that this mesh is at least in front of the near plane
		modelViewPosition.xyz += normalize(modelViewPosition.xyz) * uNear;
		gl_Position = projectionMatrix * modelViewPosition;
		vUV = uv;
	}
	`;

    let fragmentShader = `
	uniform sampler2D tColor;
	uniform float uOpacity;
	varying vec2 vUV;
	varying vec4 vDebug;
	void main(){
		vec4 color = texture2D(tColor, vUV);
		gl_FragColor = color;
		gl_FragColor.a = uOpacity;
	}
	`;

    const material = new ShaderMaterial({
        uniforms: {
            // time: { value: 1.0 },
            // resolution: { value: new Vector2() }
            tColor: {value: new Texture()},
            uNear: {value: 0.0},
            uOpacity: {value: 1.0},
        },
        vertexShader: vertexShader,
        fragmentShader: fragmentShader,
        side: DoubleSide,
    });

    material.side = DoubleSide;

    return material;
}

const planeGeometry = new PlaneGeometry(1, 1);
const lineGeometry = new BufferGeometry();
const vertices = new Float32Array([
    -0.5, -0.5, 0,
    0.5, -0.5, 0,
    0.5, 0.5, 0,

    -0.5, 0.5, 0,
    -0.5, -0.5, 0,
]);
lineGeometry.setAttribute('position', new BufferAttribute(vertices, 3));

// lineGeometry.vertices.push(
//     new Vector3(-0.5, -0.5, 0),
//     new Vector3(0.5, -0.5, 0),
//     new Vector3(0.5, 0.5, 0),
//     new Vector3(-0.5, 0.5, 0),
//     new Vector3(-0.5, -0.5, 0),
// );

export class OrientedImage {
    id: any;
    fov: number;
    position: Vector3;
    rotation: Vector3;
    width: number;
    height: number;
    mesh: Mesh;
    line: Line;
    texture: any;

    constructor(id: any) {
        this.id = id;
        this.fov = 1.0;
        this.position = new Vector3();
        this.rotation = new Vector3();
        this.width = 0;
        this.height = 0;

        const material = createMaterial();
        const lineMaterial = new LineBasicMaterial({color: 0x00ff00});
        this.mesh = new Mesh(planeGeometry, material);
        this.line = new Line(lineGeometry, lineMaterial);
        this.texture = null;

        // @ts-ignore
        this.mesh.orientedImage = this;
    }

    set(position: any, rotation: any, dimension: any, fov: any) {
        let radians = rotation.map(MathUtils.degToRad);

        // @ts-ignore
        this.position.set(...position);
        // @ts-ignore
        this.mesh.position.set(...position);

        // @ts-ignore
        this.rotation.set(...radians);
        // @ts-ignore
        this.mesh.rotation.set(...radians);

        [this.width, this.height] = dimension;
        this.mesh.scale.set(this.width / this.height, 1, 1);

        this.fov = fov;

        this.updateTransform();
    }

    updateTransform() {
        let {mesh, line, fov} = this;

        mesh.updateMatrixWorld();
        // @ts-ignore
        const dir = mesh.getWorldDirection();
        const alpha = MathUtils.degToRad(fov / 2);
        const d = -0.5 / Math.tan(alpha);
        const move = dir.clone().multiplyScalar(d);
        mesh.position.add(move);

        line.position.copy(mesh.position);
        line.scale.copy(mesh.scale);
        line.rotation.copy(mesh.rotation);
    }
}

export class OrientedImages extends EventDispatcher {
    node: any;
    cameraParams: any;
    imageParams: any;
    images: any;
    protected _visible: boolean;
    cameraParamsPath: any;
    imageParamsPath: any;

    constructor() {
        super();

        this.node = null;
        this.cameraParams = null;
        this.imageParams = null;
        this.images = null;
        this._visible = true;
    }

    set visible(visible) {
        if (this._visible === visible) {
            return;
        }

        for (const image of this.images) {
            image.mesh.visible = visible;
            image.line.visible = visible;
        }

        this._visible = visible;
        this.dispatchEvent({
            type: "visibility_changed",
            images: this,
        });
    }

    get visible() {
        return this._visible;
    }
}

export class OrientedImageLoader {
    static async loadCameraParams(path: any) {
        const res = await fetch(path);
        const text = await res.text();

        const parser = new DOMParser();
        const doc: any = parser.parseFromString(text, "application/xml");

        const width = parseInt(doc.getElementsByTagName("width")[0].textContent);
        const height = parseInt(doc.getElementsByTagName("height")[0].textContent);
        const f = parseFloat(doc.getElementsByTagName("f")[0].textContent);

        let a = (height / 2) / f;
        let fov = 2 * MathUtils.radToDeg(Math.atan(a));

        return {
            path: path,
            width: width,
            height: height,
            f: f,
            fov: fov,
        };
    }

    static async loadImageParams(path: any) {
        const response = await fetch(path);

        if (!response.ok) {
            console.error(`failed to load ${path}`);

            return;
        }

        const content = await response.text();
        const lines = content.split(/\r?\n/);
        const imageParams = [];

        for (let i = 1; i < lines.length; i++) {
            const line = lines[i];

            if (line.startsWith("#")) {
                continue;
            }

            const tokens = line.split(/\s+/);

            if (tokens.length < 6) {
                continue;
            }

            const params = {
                id: tokens[0],
                x: Number.parseFloat(tokens[1]),
                y: Number.parseFloat(tokens[2]),
                z: Number.parseFloat(tokens[3]),
                omega: Number.parseFloat(tokens[4]),
                phi: Number.parseFloat(tokens[5]),
                kappa: Number.parseFloat(tokens[6]),
            };

            // const whitelist = ["47518.jpg"];
            // if(whitelist.includes(params.id)){
            // 	imageParams.push(params);
            // }
            imageParams.push(params);
        }

        // debug
        //return [imageParams[50]];

        return imageParams;
    }

    static async load(cameraParamsPath: any, imageParamsPath: any, viewer: Viewer) {
        const tStart = performance.now();

        const [cameraParams, imageParams] = await Promise.all([
            OrientedImageLoader.loadCameraParams(cameraParamsPath),
            OrientedImageLoader.loadImageParams(imageParamsPath),
        ]);

        const orientedImageControls = new OrientedImageControls(viewer);
        const raycaster = new Raycaster();

        const tEnd = performance.now();
        console.log(tEnd - tStart);

        // const sp = new PlaneGeometry(1, 1);
        // const lg = new Geometry();

        // lg.vertices.push(
        // 	new Vector3(-0.5, -0.5, 0),
        // 	new Vector3( 0.5, -0.5, 0),
        // 	new Vector3( 0.5,  0.5, 0),
        // 	new Vector3(-0.5,  0.5, 0),
        // 	new Vector3(-0.5, -0.5, 0),
        // );

        const {width, height} = cameraParams;
        const orientedImages: any[] = [];
        const sceneNode = new Object3D();
        sceneNode.name = "oriented_images";

        for (const params of imageParams!) {
            // const material = createMaterial();
            // const lm = new LineBasicMaterial( { color: 0x00ff00 } );
            // const mesh = new Mesh(sp, material);

            const {x, y, z, omega, phi, kappa} = params;
            // const [rx, ry, rz] = [omega, phi, kappa]
            // 	.map(MathUtils.degToRad);

            // mesh.position.set(x, y, z);
            // mesh.scale.set(width / height, 1, 1);
            // mesh.rotation.set(rx, ry, rz);
            // {
            // 	mesh.updateMatrixWorld();
            // 	const dir = mesh.getWorldDirection();
            // 	const alpha = MathUtils.degToRad(cameraParams.fov / 2);
            // 	const d = -0.5 / Math.tan(alpha);
            // 	const move = dir.clone().multiplyScalar(d);
            // 	mesh.position.add(move);
            // }
            // sceneNode.add(mesh);

            // const line = new Line(lg, lm);
            // line.position.copy(mesh.position);
            // line.scale.copy(mesh.scale);
            // line.rotation.copy(mesh.rotation);
            // sceneNode.add(line);

            let orientedImage = new OrientedImage(params.id);
            // orientedImage.setPosition(x, y, z);
            // orientedImage.setRotation(omega, phi, kappa);
            // orientedImage.setDimension(width, height);
            let position = [x, y, z];
            let rotation = [omega, phi, kappa];
            let dimension = [width, height];
            orientedImage.set(position, rotation, dimension, cameraParams.fov);

            sceneNode.add(orientedImage.mesh);
            sceneNode.add(orientedImage.line);

            orientedImages.push(orientedImage);
        }

        let hoveredElement: any = null;
        let clipVolume: any = null;

        const onMouseMove = (evt: any) => {
            // const tStart = performance.now();

            if (hoveredElement) {
                hoveredElement.line.material.color.setRGB(0, 1, 0);
            }
            evt.preventDefault();

            //var array = getMousePosition( container, evt.clientX, evt.clientY );
            const rect = viewer.renderer.domElement.getBoundingClientRect();
            const [x, y] = [evt.clientX, evt.clientY];
            const array = [
                (x - rect.left) / rect.width,
                (y - rect.top) / rect.height
            ];
            const onClickPosition = new Vector2(...array);
            //const intersects = getIntersects(onClickPosition, scene.children);
            const camera = viewer.scene.getActiveCamera();
            const mouse = new Vector3(
                +(onClickPosition.x * 2) - 1,
                -(onClickPosition.y * 2) + 1
            );
            const objects = orientedImages.map(i => i.mesh);
            raycaster.setFromCamera(mouse, camera);
            const intersects = raycaster.intersectObjects(objects);
            let selectionChanged = false;

            if (intersects.length > 0) {
                //console.log(intersects);
                const intersection = intersects[0];
                // @ts-ignore
                const orientedImage = intersection.object.orientedImage;
                orientedImage.line.material.color.setRGB(1, 0, 0);
                selectionChanged = hoveredElement !== orientedImage;
                hoveredElement = orientedImage;
            } else {
                hoveredElement = null;
            }

            // let shouldRemoveClipVolume = clipVolume !== null && hoveredElement === null;
            let shouldAddClipVolume = clipVolume === null && hoveredElement !== null;

            if (clipVolume !== null && (hoveredElement === null || selectionChanged)) {
                // remove existing
                viewer.scene.removePolygonClipVolume(clipVolume);
                clipVolume = null;
            }

            if (shouldAddClipVolume || selectionChanged) {
                const img = hoveredElement;
                const fov = cameraParams.fov;
                const aspect = cameraParams.width / cameraParams.height;
                const near = 1.0;
                const far = 1000 * 1000;
                // @ts-ignore
                const camera = new PerspectiveCamera(fov, aspect, near, far);
                camera.rotation.order = viewer.scene.getActiveCamera().rotation.order;
                camera.rotation.copy(img.mesh.rotation);
                {
                    const mesh = img.mesh;
                    const dir = mesh.getWorldDirection();
                    const pos = mesh.position;
                    const alpha = MathUtils.degToRad(fov / 2);
                    const d = 0.5 / Math.tan(alpha);
                    const newCamPos = pos.clone().add(dir.clone().multiplyScalar(d));
                    // const newCamDir = pos.clone().sub(newCamPos);
                    // const newCamTarget = new Vector3().addVectors(
                    //     newCamPos,
                    //     newCamDir.clone().multiplyScalar(viewer.getMoveSpeed())
                    // );
                    camera.position.copy(newCamPos);
                }
                let volume = new PolygonClipVolume(camera);
                let m0 = new Mesh();
                let m1 = new Mesh();
                let m2 = new Mesh();
                let m3 = new Mesh();
                m0.position.set(-1, -1, 0);
                m1.position.set(1, -1, 0);
                m2.position.set(1, 1, 0);
                m3.position.set(-1, 1, 0);
                volume.markers.push(m0, m1, m2, m3);
                volume.initialized = true;

                viewer.scene.addPolygonClipVolume(volume);
                clipVolume = volume;
            }
            // const tEnd = performance.now();
            // console.log(tEnd - tStart);
        };

        const moveToImage = (image: any) => {
            console.log("move to image " + image.id);

            const mesh = image.mesh;
            const newCamPos = image.position.clone();
            const newCamTarget = mesh.position.clone();

            viewer.scene.view.setView(newCamPos, newCamTarget, 500, () => {
                orientedImageControls.capture(image);
            });

            if (image.texture === null) {
                const target = image;

                const tmpImagePath = `${App.resourcePath}/images/loading.jpg`;
                new TextureLoader().load(tmpImagePath,
                    (texture) => {
                        if (target.texture === null) {
                            target.texture = texture;
                            target.mesh.material.uniforms.tColor.value = texture;
                            mesh.material.needsUpdate = true;
                        }
                    }
                );

                const imagePath = `${imageParamsPath}/../${target.id}`;
                new TextureLoader().load(imagePath,
                    (texture) => {
                        target.texture = texture;
                        target.mesh.material.uniforms.tColor.value = texture;
                        mesh.material.needsUpdate = true;
                    }
                );
            }
        };

        const onMouseClick = () => {
            if (orientedImageControls.hasSomethingCaptured()) {
                return;
            }

            if (hoveredElement) {
                moveToImage(hoveredElement);
            }
        };

        viewer.renderer.domElement.addEventListener('mousemove', onMouseMove, false);
        viewer.renderer.domElement.addEventListener('mousedown', onMouseClick, false);

        viewer.addEventListener(ViewerEventName.Update, () => {

            for (const image of orientedImages) {
                // const world = image.mesh.matrixWorld;
                const {width, height} = image;
                const aspect = width / height;

                const camera = viewer.scene.getActiveCamera();

                const imgPos = image.mesh.getWorldPosition(new Vector3());
                const camPos = camera.position;
                const d = camPos.distanceTo(imgPos);

                const minSize = 1; // in degrees of fov
                const a = MathUtils.degToRad(minSize);
                let r = d * Math.tan(a);
                r = Math.max(r, 1);


                image.mesh.scale.set(r * aspect, r, 1);
                image.line.scale.set(r * aspect, r, 1);

                image.mesh.material.uniforms.uNear.value = camera.near;

            }

        });

        const images = new OrientedImages();
        images.node = sceneNode;
        images.cameraParamsPath = cameraParamsPath;
        images.imageParamsPath = imageParamsPath;
        images.cameraParams = cameraParams;
        images.imageParams = imageParams;
        images.images = orientedImages;

        App.debug.moveToImage = moveToImage;

        return images;
    }
}

