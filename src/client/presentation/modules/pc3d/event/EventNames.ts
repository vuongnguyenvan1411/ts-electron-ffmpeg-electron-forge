export class MouseEventName {
    // public static readonly Click = "pointertap";
    public static readonly Click = "click";
    // public static readonly Down = "pointerdown";
    public static readonly Down = "mousedown";
    // public static readonly Move = "pointermove";
    public static readonly Move = "mousemove";
    // public static readonly Out = "pointerout";
    public static readonly Out = "mouseout";
    // public static readonly Over = "pointerover";
    public static readonly Over = "mouseover";
    // public static readonly Up = "pointerup";
    public static readonly Up = "mouseup";
    public static readonly UpOutside = "pointerupoutside";
    public static readonly RightClick = "rightclick";
    public static readonly RightDown = "rightdown";
    public static readonly RightUp = "rightup";
    public static readonly RightOutside = "rightupoutside";
    public static readonly Enter = "mouseenter";
    public static readonly Leave = "mouseleave";
    public static readonly Hover = "hover";
}

export class TouchEventName {
    public static readonly Tap = "tap";
    public static readonly End = "touchend";
    public static readonly EndOutside = "touchendoutside";
    public static readonly Move = "touchmove";
    public static readonly Start = "touchstart";
}

export class KeyEventName {
    public static readonly Keyup = "keyup";
    public static readonly KeyDown = "keydown";
}

export class EmitterEventName {
    public static readonly Change = "change";
    public static readonly Click = "click";
}
