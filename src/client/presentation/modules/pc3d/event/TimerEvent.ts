export class TimerEvent extends Event {
    static readonly Start = "start";
    static readonly End = "end";
    static readonly Repeat = "repeat";
    static readonly Update = "update";
    static readonly Stop = "stop";

    constructor(type: string) {
        super(type);
    }
}