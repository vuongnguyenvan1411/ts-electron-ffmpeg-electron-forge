import {Scene, Vector2, Vector3, WebGLRenderer} from "three";
import {BoxVolume, SphereVolume, Volume} from "./Volume";
import {Utils} from "../core/Utils";
import {EventDispatcher} from "../core/EventDispatcher";
import {Viewer} from "../viewer/viewer";
import {InputHandlerEvent, ViewerEventName} from "../core/Event";

type TArgsStartInsertion = {
    type?: any,
    name?: string,
    clip?: boolean,
    properties?: Record<string, any>
}

export class VolumeTool extends EventDispatcher {
    viewer: Viewer;
    renderer: WebGLRenderer;
    scene: Scene;

    constructor(viewer: Viewer) {
        super();

        this.viewer = viewer;
        this.renderer = viewer.renderer;

        this.addEventListener(ViewerEventName.StartInsertingVolume, () => {
            this.viewer.dispatchEvent({
                type: ViewerEventName.CancelInsertions
            });
        });

        this.scene = new Scene();
        this.scene.name = 'scene_volume';

        this.viewer.inputHandler.registerInteractiveScene(this.scene);

        for (let volume of viewer.scene.volumes) {
            this.onAdd({volume: volume});
        }

        this.viewer.inputHandler.addEventListener(InputHandlerEvent.Delete, (e: any) => {
            const volumes = e.selection.filter((e: any) => (e instanceof Volume));

            volumes.forEach((e: any) => this.viewer.scene.removeVolume(e));
        });

        viewer.addEventListener(ViewerEventName.Update, this.update.bind(this));
        viewer.addEventListener(ViewerEventName.Render_Pass_Scene, (e: any) => this.render(e));
        viewer.addEventListener(ViewerEventName.SceneChanged, this.onSceneChange.bind(this));

        viewer.scene.addEventListener(ViewerEventName.VolumeAdded, this.onAdd);
        viewer.scene.addEventListener(ViewerEventName.VolumeRemoved, this.onRemove);
    }

    onRemove = (e: any) => {
        this.scene.remove(e.volume);
    }

    onAdd = (e: any) => {
        this.scene.add(e.volume);
    }

    onSceneChange(e: any) {
        if (e.oldScene) {
            e.oldScene.removeEventListeners(ViewerEventName.VolumeAdded/*, this.onAdd*/);
            e.oldScene.removeEventListeners(ViewerEventName.VolumeRemoved/*, this.onRemove*/);
        }

        e.scene.addEventListener(ViewerEventName.VolumeAdded, this.onAdd);
        e.scene.addEventListener(ViewerEventName.VolumeRemoved, this.onRemove);
    }

    startInsertion(args: TArgsStartInsertion = {}) {
        let volume: BoxVolume | SphereVolume;

        if (args.type) {
            volume = new args.type();
        } else {
            volume = new BoxVolume();
        }

        volume.clip = args.clip ?? false;
        volume.name = args.name ?? 'Volume';

        if (args.properties) {
            volume.setProperties(args.properties);
        }

        this.dispatchEvent({
            type: ViewerEventName.StartInsertingVolume,
            volume: volume
        });

        this.viewer.scene.addVolume(volume);
        this.scene.add(volume);

        const cancel: any = {
            callback: null
        };

        const drag = (e: any) => {
            let camera = this.viewer.scene.getActiveCamera();

            let I = Utils.getMousePointCloudIntersection(
                e.drag.end,
                this.viewer.scene.getActiveCamera(),
                this.viewer,
                this.viewer.scene.pointClouds,
                {pickClipped: false});

            if (I) {
                volume.position.copy(I.location);

                let wp = volume.getWorldPosition(new Vector3()).applyMatrix4(camera.matrixWorldInverse);
                // let pp = new Vector4(wp.x, wp.y, wp.z).applyMatrix4(camera.projectionMatrix);
                let w = Math.abs((wp.z / 5));
                volume.scale.set(w, w, w);
            }
        };

        const drop = () => {
            volume.removeEventListener('drag', drag);
            volume.removeEventListener('drop', drop);

            cancel.callback();
        };

        cancel.callback = () => {
            volume.removeEventListener('drag', drag);
            volume.removeEventListener('drop', drop);
            this.viewer.removeEventListener(ViewerEventName.CancelInsertions, cancel.callback);
        };

        volume.addEventListener('drag', drag);
        volume.addEventListener('drop', drop);
        this.viewer.addEventListener(ViewerEventName.CancelInsertions, cancel.callback);

        this.viewer.inputHandler.startDragging(volume);

        return volume;
    }

    update() {
        if (!this.viewer.scene) {
            return;
        }

        let camera = this.viewer.scene.getActiveCamera();
        let renderAreaSize = this.viewer.renderer.getSize(new Vector2());
        let clientWidth = renderAreaSize.width;
        let clientHeight = renderAreaSize.height;

        let volumes = this.viewer.scene.volumes;
        for (let volume of volumes) {
            let label = volume.label;

            {

                let distance = label.position.distanceTo(camera.position);
                let pr = Utils.projectedRadius(1, camera, distance, clientWidth, clientHeight);

                let scale = (70 / pr);
                label.scale.set(scale, scale, scale);
            }

            let calculatedVolume = volume.getVolume();
            calculatedVolume = calculatedVolume / Math.pow(this.viewer.lengthUnit.unitspermeter, 3) * Math.pow(this.viewer.lengthUnitDisplay.unitspermeter, 3);  //convert to cubic meters then to the cubic display unit
            let text = Utils.addCommas(calculatedVolume.toFixed(3)) + ' ' + this.viewer.lengthUnitDisplay.code + '\u00B3';
            label.setText(text);
        }
    }

    render(params: any) {
        const renderer = this.viewer.renderer;
        const oldTarget = renderer.getRenderTarget();

        if (params.renderTarget) {
            renderer.setRenderTarget(params.renderTarget);
        }

        renderer.render(this.scene, this.viewer.scene.getActiveCamera());
        renderer.setRenderTarget(oldTarget);
    }
}
