import {Matrix4, Mesh, Object3D, Vector2, Vector3} from "three";

export class PolygonClipVolume extends Object3D {
    markers: Mesh[];
    camera: any;
    viewMatrix: Matrix4;
    projMatrix: Matrix4;
    initialized: boolean;
    protected _properties: Record<string, any>;

    constructor(camera: any) {
        super();

        (this.constructor as any).counter = ((this.constructor as any).counter === undefined) ? 0 : (this.constructor as any).counter + 1;
        this.name = "polygon_clip_volume_" + (this.constructor as any).counter;

        this.camera = camera.clone();
        this.camera.rotation.set(...camera.rotation.toArray()); // [r85] workaround because camera.clone() doesn't work on rotation
        this.camera.rotation.order = camera.rotation.order;
        this.camera.updateMatrixWorld();
        this.camera.updateProjectionMatrix();
        this.camera.matrixWorldInverse.copy(this.camera.matrixWorld).invert();

        this.viewMatrix = this.camera.matrixWorldInverse.clone();
        this.projMatrix = this.camera.projectionMatrix.clone();

        // projected markers
        this.markers = [];
        this.initialized = false;
    }

    setName(name?: string) {
        this.name = (name ?? "polygon_clip_volume_") + (this.constructor as any).counter;
    }

    addMarker() {
        const marker = new Mesh();

        const drag = (e: any) => {
            const size = e.viewer.renderer.getSize(new Vector2());
            const projectedPos = new Vector3(
                2.0 * (e.drag.end.x / size.width) - 1.0,
                -2.0 * (e.drag.end.y / size.height) + 1.0,
                0
            );

            marker.position.copy(projectedPos);
        };

        const drop = () => {
            cancel();
        };

        const cancel = () => {
            marker.removeEventListener("drag", drag);
            marker.removeEventListener("drop", drop);
        };

        marker.addEventListener("drag", drag);
        marker.addEventListener("drop", drop);

        this.markers.push(marker);
    }

    removeLastMarker() {
        if (this.markers.length > 0) {
            this.markers.splice(this.markers.length - 1, 1);
        }
    }

    get(key: string) {
        return this._properties[key];
    }

    set(key: string, value: any) {
        return this._properties[key] = value;
    }

    getKeys() {
        return Object.keys(this._properties);
    }

    getProperties() {
        return this._properties;
    }

    setProperties(data: Record<string, any>) {
        return this._properties = data;
    }

    hasProperties(key?: string) {
        if (key) {
            return this._properties.hasOwnProperty(key);
        } else {
            return this.getKeys.length > 0;
        }
    }
}
