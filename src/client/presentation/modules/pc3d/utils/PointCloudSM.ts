import {DepthTexture, FloatType, LinearFilter, PerspectiveCamera, RGBAFormat, Scene as ThreeScene, SpotLight, UnsignedIntType, Vector3, WebGLRenderer, WebGLRenderTarget} from "three";
import {CRenderer} from "../core/CRenderer";

export class PointCloudSM {
    cRenderer: CRenderer;
    threeRenderer: WebGLRenderer;
    target: WebGLRenderTarget;
    light: SpotLight;
    camera: PerspectiveCamera;

    constructor(cRenderer: CRenderer) {
        this.cRenderer = cRenderer;
        this.threeRenderer = this.cRenderer.threeRenderer;

        const width = 2 * 1024;
        const height = 2 * 1024;

        this.target = new WebGLRenderTarget(width, height, {
            minFilter: LinearFilter,
            magFilter: LinearFilter,
            format: RGBAFormat,
            type: FloatType
        });

        this.target.depthTexture = new DepthTexture(width, height);
        this.target.depthTexture.type = UnsignedIntType;

        this.threeRenderer.setClearColor(0xff0000, 1);

        //HACK? removed while moving to three.js 109
        //this.threeRenderer.clearTarget(this.target, true, true, true);
        {
            const oldTarget = this.threeRenderer.getRenderTarget();

            this.threeRenderer.setRenderTarget(this.target);
            this.threeRenderer.clear(true, true, true);

            this.threeRenderer.setRenderTarget(oldTarget);
        }
    }

    setLight(light: any) {
        this.light = light;

        let fov = (180 * light.angle) / Math.PI;
        let aspect = light.shadow.mapSize.width / light.shadow.mapSize.height;
        let near = 0.1;
        let far = light.distance === 0 ? 10000 : light.distance;
        // @ts-ignore
        this.camera = new PerspectiveCamera(fov, aspect, near, far);
        this.camera.up.set(0, 0, 1);
        this.camera.position.copy(light.position);

        let target = new Vector3().subVectors(light.position, light.getWorldDirection(new Vector3()));
        this.camera.lookAt(target);

        this.camera.updateProjectionMatrix();
        this.camera.updateMatrix();
        this.camera.updateMatrixWorld();
        this.camera.matrixWorldInverse.copy(this.camera.matrixWorld).invert();
    }

    setSize(width: number, height: number) {
        if (this.target.width !== width || this.target.height !== height) {
            this.target.dispose();
        }
        this.target.setSize(width, height);
    }

    render(scene: ThreeScene) {
        this.threeRenderer.setClearColor(0x000000, 1);

        const oldTarget = this.threeRenderer.getRenderTarget();

        this.threeRenderer.setRenderTarget(this.target);
        this.threeRenderer.clear(true, true, true);

        this.cRenderer.render(scene, this.camera, this.target, {});

        this.threeRenderer.setRenderTarget(oldTarget);
    }
}
