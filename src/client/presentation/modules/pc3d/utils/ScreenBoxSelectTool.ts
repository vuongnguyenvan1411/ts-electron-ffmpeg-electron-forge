import {Box2, Line3, Ray, Scene, Vector2, Vector3} from "three";
import {BoxVolume} from "./Volume";
import {EventDispatcher} from "../core/EventDispatcher";
import {Viewer} from "../viewer/viewer";
import {ViewerEventName} from "../core/Event";
import {Utils} from "../core/Utils";
import {PointSizeType} from "../core/Defines";

type TArgsStartInsertion = {
    type?: string;
    name?: string;
    properties?: Record<string, any>;
}

export class ScreenBoxSelectTool extends EventDispatcher {
    viewer: Viewer;
    scene: Scene;
    importance: number;

    constructor(viewer: Viewer) {
        super();

        this.viewer = viewer;
        this.scene = new Scene();

        viewer.addEventListener(ViewerEventName.Update, this.update.bind(this));
        viewer.addEventListener(ViewerEventName.Render_Pass_PerspectiveOverlay, this.render.bind(this));
        viewer.addEventListener(ViewerEventName.SceneChanged, this.onSceneChange.bind(this));
    }

    onSceneChange(scene: any) {
        console.log("scene changed", scene);
    }

    startInsertion(args: TArgsStartInsertion = {}) {
        const domElement = this.viewer.renderer.domElement;

        const volume = new BoxVolume();
        volume.position.set(12345, 12345, 12345);
        volume.showVolumeLabel = false;
        volume.visible = false;
        volume.update();

        volume.name = args.name ?? 'Volume';

        if (args.properties) {
            volume.setProperties(args.properties);
        }

        this.viewer.scene.addVolume(volume);

        this.importance = 10;

        const selectionBox = document.createElement('div');
        selectionBox.style.position = 'absolute';
        selectionBox.style.border = '2px dashed white';
        selectionBox.style.pointerEvents = 'none';
        selectionBox.style.right = "10px";
        selectionBox.style.bottom = "10px";
        domElement.parentElement!.append(selectionBox);

        const drag = (e: any) => {
            volume.visible = true;

            const mStart = e.drag.start;
            const mEnd = e.drag.end;

            const box2D = new Box2();
            box2D.expandByPoint(mStart);
            box2D.expandByPoint(mEnd);

            selectionBox.style.left = `${box2D.min.x}px`;
            selectionBox.style.top = `${box2D.min.y}px`;
            selectionBox.style.width = `${box2D.max.x - box2D.min.x}px`;
            selectionBox.style.height = `${box2D.max.y - box2D.min.y}px`;

            const camera = e.viewer.scene.getActiveCamera();
            const size = e.viewer.renderer.getSize(new Vector2());
            const frustumSize = new Vector2(camera.right - camera.left, camera.top - camera.bottom);

            const screenCentroid = new Vector2().addVectors(e.drag.end, e.drag.start).multiplyScalar(0.5);
            const ray = Utils.mouseToRay(screenCentroid, camera, size.width, size.height);

            const diff = new Vector2().subVectors(e.drag.end, e.drag.start);
            diff.divide(size).multiply(frustumSize);

            volume.position.copy(ray.origin);
            volume.up.copy(camera.up);
            volume.rotation.copy(camera.rotation);
            volume.scale.set(diff.x, diff.y, 1000 * 100);

            e.consume();
        };

        const drop = (e: any) => {
            this.importance = 0;

            selectionBox.remove();

            this.viewer.inputHandler.deselectAll();
            this.viewer.inputHandler.toggleSelection(volume);

            const camera = e.viewer.scene.getActiveCamera();
            const size = e.viewer.renderer.getSize(new Vector2());
            const screenCentroid = new Vector2().addVectors(e.drag.end, e.drag.start).multiplyScalar(0.5);
            const ray = Utils.mouseToRay(screenCentroid, camera, size.width, size.height);
            // let line = new Line3(ray.origin, new Vector3().addVectors(ray.origin, ray.direction));

            this.removeEventListener("drag", drag);
            this.removeEventListener("drop", drop);

            const allPointsNear = [];
            const allPointsFar = [];

            // TODO support more than one point cloud
            for (const pointcloud of this.viewer.scene.pointClouds) {
                if (!pointcloud.visible) {
                    continue;
                }

                const volCam = camera.clone();
                volCam.left = -volume.scale.x / 2;
                volCam.right = +volume.scale.x / 2;
                volCam.top = +volume.scale.y / 2;
                volCam.bottom = -volume.scale.y / 2;
                volCam.near = -volume.scale.z / 2;
                volCam.far = +volume.scale.z / 2;
                volCam.rotation.copy(volume.rotation);
                volCam.position.copy(volume.position);

                volCam.updateMatrix();
                volCam.updateMatrixWorld();
                volCam.updateProjectionMatrix();
                volCam.matrixWorldInverse.copy(volCam.matrixWorld).invert();

                const ray = new Ray(volCam.getWorldPosition(new Vector3()), volCam.getWorldDirection(new Vector3()));
                const rayInverse = new Ray(
                    ray.origin.clone().add(ray.direction.clone().multiplyScalar(volume.scale.z)),
                    ray.direction.clone().multiplyScalar(-1));

                const pickerSettings = {
                    width: 8,
                    height: 8,
                    pickWindowSize: 8,
                    all: true,
                    pickClipped: true,
                    pointSizeType: PointSizeType.FIXED,
                    pointSize: 1
                };
                const pointsNear = pointcloud.pick(this.viewer, volCam, ray, pickerSettings);

                if (pointsNear) {
                    volCam.rotateX(Math.PI);
                    volCam.updateMatrix();
                    volCam.updateMatrixWorld();
                    volCam.updateProjectionMatrix();
                    volCam.matrixWorldInverse.copy(volCam.matrixWorld).invert();
                    const pointsFar = pointcloud.pick(this.viewer, volCam, rayInverse, pickerSettings);

                    allPointsNear.push(...pointsNear);
                    allPointsFar.push(...pointsFar);
                }
            }

            if (allPointsNear.length > 0 && allPointsFar.length > 0) {
                const viewLine = new Line3(ray.origin, new Vector3().addVectors(ray.origin, ray.direction));

                const closestOnLine = allPointsNear.map(p => viewLine.closestPointToPoint(p.position, false, new Vector3()));
                const closest = closestOnLine.sort((a, b) => ray.origin.distanceTo(a) - ray.origin.distanceTo(b))[0];

                const farthestOnLine = allPointsFar.map(p => viewLine.closestPointToPoint(p.position, false, new Vector3()));
                const farthest = farthestOnLine.sort((a, b) => ray.origin.distanceTo(b) - ray.origin.distanceTo(a))[0];

                const distance = closest.distanceTo(farthest);
                const centroid = new Vector3().addVectors(closest, farthest).multiplyScalar(0.5);

                volume.scale.z = distance * 1.1;
                volume.position.copy(centroid);
            }

            volume.clip = true;
        };

        this.addEventListener("drag", drag);
        this.addEventListener("drop", drop);

        this.viewer.inputHandler.addInputListener(this);

        return volume;
    }

    update() {
        //
    }

    render() {
        this.viewer.renderer.render(this.scene, this.viewer.scene.getActiveCamera());
    }
}
