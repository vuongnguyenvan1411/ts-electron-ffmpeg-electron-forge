import {BoxGeometry, BufferAttribute, BufferGeometry, Color, Line, LineBasicMaterial, Mesh, MeshBasicMaterial, MeshLambertMaterial, Object3D, Raycaster, SphereGeometry, Vector3} from "three";
import {Utils} from "../core/Utils";
import {MeasureEvent} from "../core/Event";

export class Profile extends Object3D {
    points: Vector3[];
    spheres: Mesh[];
    edges: Line[];
    boxes: any[];
    width: number;
    height: number;
    private _modifiable: boolean;
    sphereGeometry: SphereGeometry;
    color: Color;
    lineColor: Color;
    protected _properties: Record<string, any>;

    constructor() {
        super();

        (this.constructor as any).counter = ((this.constructor as any).counter === undefined) ? 0 : (this.constructor as any).counter + 1;
        this.name = 'Profile_' + (this.constructor as any).counter;

        this.points = [];
        this.spheres = [];
        this.edges = [];
        this.boxes = [];
        this.width = 1;
        this.height = 20;
        this._modifiable = true;

        this.sphereGeometry = new SphereGeometry(0.4, 10, 10);
        this.color = new Color(0xff0000);
        this.lineColor = new Color(0xff0000);
    }

    createSphereMaterial() {
        return new MeshLambertMaterial({
                //shading: SmoothShading,
                color: 0xff0000,
                depthTest: false,
                depthWrite: false
            }
        );
    };

    getSegments() {
        const segments = [];

        for (let i = 0; i < this.points.length - 1; i++) {
            const start = this.points[i].clone();
            const end = this.points[i + 1].clone();

            segments.push({start: start, end: end});
        }

        return segments;
    }

    getSegmentMatrices() {
        const segments = this.getSegments();
        const matrices = [];

        for (let segment of segments) {
            const {start, end} = segment;

            const box = new Object3D();

            const length = start.clone().setZ(0).distanceTo(end.clone().setZ(0));
            box.scale.set(length, 10000, this.width);
            box.up.set(0, 0, 1);

            const center = new Vector3().addVectors(start, end).multiplyScalar(0.5);
            const diff = new Vector3().subVectors(end, start);
            const target = new Vector3(diff.y, -diff.x, 0);

            box.position.set(0, 0, 0);
            box.lookAt(target);
            box.position.copy(center);

            box.updateMatrixWorld();
            matrices.push(box.matrixWorld);
        }

        return matrices;
    }

    addMarker(point: Vector3) {
        this.points.push(point);

        const sphere = new Mesh(this.sphereGeometry, this.createSphereMaterial());

        this.add(sphere);
        this.spheres.push(sphere);

        // edges & boxes
        if (this.points.length > 1) {
            const lineGeometry = new BufferGeometry();

            const vertices = new Float32Array([
                ...(new Vector3().toArray()),
                ...(new Vector3().toArray()),
            ]);

            lineGeometry.setAttribute('position', new BufferAttribute(vertices, 3));
            lineGeometry.setAttribute('color', new BufferAttribute(new Float32Array([
                ...(this.lineColor.toArray()),
                ...(this.lineColor.toArray()),
                ...(this.lineColor.toArray()),
            ]), 3));

            const lineMaterial = new LineBasicMaterial({
                vertexColors: true,
                linewidth: 2,
                transparent: true,
                opacity: 0.4
            });

            lineMaterial.depthTest = false;
            const edge = new Line(lineGeometry, lineMaterial);
            edge.visible = false;

            this.add(edge);
            this.edges.push(edge);

            const boxGeometry = new BoxGeometry(1, 1, 1);
            const boxMaterial = new MeshBasicMaterial({color: 0xff0000, transparent: true, opacity: 0.2});
            const box = new Mesh(boxGeometry, boxMaterial);
            box.visible = false;

            this.add(box);
            this.boxes.push(box);
        }

        { // event listeners
            const drag = (e: any) => {
                let I = Utils.getMousePointCloudIntersection(
                    e.drag.end,
                    e.viewer.scene.getActiveCamera(),
                    e.viewer,
                    e.viewer.scene.pointClouds
                );

                if (I) {
                    const i = this.spheres.indexOf(e.drag.object);

                    if (i !== -1) {
                        this.setPosition(i, I.location);
                        //this.dispatchEvent({
                        //	'type': MeasureEvent.MarkerMoved',
                        //	'profile': this,
                        //	'index': i
                        //});
                    }
                }
            };

            const drop = (e: any) => {
                let i = this.spheres.indexOf(e.drag.object);

                if (i !== -1) {
                    this.dispatchEvent({
                        type: MeasureEvent.MarkerDropped,
                        profile: this,
                        index: i
                    });
                }
            };

            const mouseover = (e: any) => e.object.material.emissive.setHex(0x888888);
            const mouseleave = (e: any) => e.object.material.emissive.setHex(0x000000);

            sphere.addEventListener('drag', drag);
            sphere.addEventListener('drop', drop);
            sphere.addEventListener('mouseover', mouseover);
            sphere.addEventListener('mouseleave', mouseleave);
        }

        this.dispatchEvent({
            type: MeasureEvent.MarkerAdded,
            profile: this,
            sphere: sphere
        });

        this.setPosition(this.points.length - 1, point);
    }

    removeMarker(index: number) {
        this.points.splice(index, 1);

        this.remove(this.spheres[index]);

        const edgeIndex = (index === 0) ? 0 : (index - 1);
        this.remove(this.edges[edgeIndex]);
        this.edges.splice(edgeIndex, 1);
        this.remove(this.boxes[edgeIndex]);
        this.boxes.splice(edgeIndex, 1);

        this.spheres.splice(index, 1);

        this.update();

        this.dispatchEvent({
            type: MeasureEvent.MarkerRemoved,
            profile: this
        });
    }

    setPosition(index: number, position: Vector3) {
        const point = this.points[index];
        point.copy(position);

        this.dispatchEvent({
            type: MeasureEvent.MarkerMoved,
            profile: this,
            index: index,
            position: point.clone()
        });

        this.update();
    }

    setWidth(width: number) {
        this.width = width;

        this.dispatchEvent({
            type: MeasureEvent.WidthChanged,
            profile: this,
            width: width
        });

        this.update();
    }

    getWidth() {
        return this.width;
    }

    update() {
        if (this.points.length === 0) {
            return;
        } else if (this.points.length === 1) {
            let point = this.points[0];
            this.spheres[0].position.copy(point);

            return;
        }

        // console.log('points', this.points);
        // console.log('edges', this.edges);
        // console.log('spheres', this.spheres);

        const min = this.points[0].clone();
        const max = this.points[0].clone();
        const centroid = new Vector3();
        const lastIndex = this.points.length - 1;

        for (let i = 0; i <= lastIndex; i++) {
            if (typeof this.points[i] === "undefined" || typeof this.spheres[i] === "undefined") {
                continue;
            }

            const point = this.points[i];
            const sphere = this.spheres[i];
            const leftIndex = (i === 0) ? lastIndex : i - 1;
            // let rightIndex = (i === lastIndex) ? 0 : i + 1;
            const leftVertex = this.points[leftIndex];
            // let rightVertex = this.points[rightIndex];
            const leftEdge = this.edges[leftIndex];
            const rightEdge = this.edges[i];
            const leftBox = this.boxes[leftIndex];
            // rightBox = this.boxes[i];

            // let leftEdgeLength = point.distanceTo(leftVertex);
            // let rightEdgeLength = point.distanceTo(rightVertex);
            // let leftEdgeCenter = new Vector3().addVectors(leftVertex, point).multiplyScalar(0.5);
            // let rightEdgeCenter = new Vector3().addVectors(point, rightVertex).multiplyScalar(0.5);

            sphere.position.copy(point);

            sphere.visible = this._modifiable;

            if (typeof leftEdge !== "undefined" && leftEdge) {
                // leftEdge.geometry.vertices[1].copy(point);
                // leftEdge.geometry.verticesNeedUpdate = true;
                leftEdge.geometry.computeBoundingSphere();
            }

            if (typeof rightEdge !== "undefined" && rightEdge) {
                // console.log('rightEdge', rightEdge);
                // console.log('rightEdge.geometry', rightEdge.geometry);
                // console.log('rightEdge.geometry.getAttribute(\'position\')', rightEdge.geometry.getAttribute('position').array[0]);

                // rightEdge.geometry.vertices[0].copy(point);
                // rightEdge.geometry.verticesNeedUpdate = true;
                rightEdge.geometry.computeBoundingSphere();
            }

            if (leftBox) {
                const start = leftVertex;
                const end = point;
                const length = start.clone().setZ(0).distanceTo(end.clone().setZ(0));
                leftBox.scale.set(length, 1000000, this.width);
                leftBox.up.set(0, 0, 1);

                const center = new Vector3().addVectors(start, end).multiplyScalar(0.5);
                const diff = new Vector3().subVectors(end, start);
                const target = new Vector3(diff.y, -diff.x, 0);

                leftBox.position.set(0, 0, 0);
                leftBox.lookAt(target);
                leftBox.position.copy(center);
            }

            centroid.add(point);
            min.min(point);
            max.max(point);
        }

        centroid.multiplyScalar(1 / this.points.length);

        for (let i = 0; i < this.boxes.length; i++) {
            const box = this.boxes[i];

            box.position.z = min.z + (max.z - min.z) / 2;
        }
    }

    raycast(raycaster: Raycaster, intersects: any[]) {
        for (let i = 0; i < this.points.length; i++) {
            const sphere = this.spheres[i];

            sphere.raycast(raycaster, intersects);
        }

        // recalculate distances because they are not necessarely correct
        // for scaled objects.
        // see https://github.com/mrdoob/three.js/issues/5827
        // TODO: remove this once the bug has been fixed
        for (let i = 0; i < intersects.length; i++) {
            const I = intersects[i];
            I.distance = raycaster.ray.origin.distanceTo(I.point);
        }

        intersects.sort(function (a, b) {
            return a.distance - b.distance;
        });
    };

    get modifiable() {
        return this._modifiable;
    }

    set modifiable(value) {
        this._modifiable = value;
        this.update();
    }

    get(key: string) {
        return this._properties[key];
    }

    set(key: string, value: any) {
        return this._properties[key] = value;
    }

    getKeys() {
        return Object.keys(this._properties);
    }

    getProperties() {
        return this._properties;
    }

    setProperties(data: Record<string, any>) {
        return this._properties = data;
    }

    hasProperties(key?: string) {
        if (key) {
            return this._properties.hasOwnProperty(key);
        } else {
            return this.getKeys.length > 0;
        }
    }
}
