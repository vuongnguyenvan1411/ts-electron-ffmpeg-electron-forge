import {Annotation} from "../core/Annotation";
import {Utils} from "../core/Utils";
import {EventDispatcher} from "../core/EventDispatcher";
import {Viewer} from "../viewer/viewer";
import {Mesh, MeshNormalMaterial, MOUSE, SphereGeometry, Vector2, WebGLRenderer} from "three";
import {ViewerEventName} from "../core/Event";
import {View} from "../viewer/View";

type TArgsStartInsertion = {
    name?: string;
    title?: string;
    description?: string;
    properties?: Record<string, any>;
}

type TEventDrag = {
    target: any;
    drag: {
        start: Vector2,
        end: Vector2,
        mouse: number,
        lastDrag: Vector2,
        object: Mesh,
        startView: View
    };
    type: string;
    viewer: Viewer;
}

export class AnnotationTool extends EventDispatcher {
    viewer: Viewer;
    renderer: WebGLRenderer;
    sg: SphereGeometry;
    sm: MeshNormalMaterial;
    s: Mesh;

    constructor(viewer: Viewer) {
        super();

        this.viewer = viewer;
        this.renderer = viewer.renderer;

        this.sg = new SphereGeometry(0.1);
        this.sm = new MeshNormalMaterial();
        this.s = new Mesh(this.sg, this.sm);
    }

    startInsertion(args: TArgsStartInsertion = {}) {
        const domElement = this.viewer.renderer.domElement;

        const annotation = new Annotation({
            position: [589748.270, 231444.540, 753.675],
            title: args.title ?? "Annotation Title",
            description: args.description ?? "Annotation Description"
        });

        annotation.name = args.name ?? 'Annotation';

        if (args.properties) {
            annotation.setProperties(args.properties);
        }

        this.dispatchEvent({
            type: ViewerEventName.StartInsertingAnnotation,
            annotation: annotation
        });

        const annotations = this.viewer.scene.annotations;
        annotations.add(annotation);

        const callbacks = {
            cancel: () => {
                //
            },
            finish: () => {
                //
            },
        };

        const insertionCallback = (e: MouseEvent) => {
            if (e.button === MOUSE.LEFT) {
                callbacks.finish();
            } else if (e.button === MOUSE.RIGHT) {
                callbacks.cancel();
            }
        };

        callbacks.cancel = () => {
            annotations.remove(annotation);

            domElement.removeEventListener('mouseup', insertionCallback, true);
        };

        callbacks.finish = () => {
            domElement.removeEventListener('mouseup', insertionCallback, true);
        };

        domElement.addEventListener('mouseup', insertionCallback, true);

        const drag = (e: TEventDrag) => {
            let I = Utils.getMousePointCloudIntersection(
                e.drag.end,
                e.viewer.scene.getActiveCamera(),
                e.viewer,
                e.viewer.scene.pointClouds,
                {
                    pickClipped: true
                }
            );

            if (I) {
                this.s.position.copy(I.location);

                if (annotation.position) annotation.position.copy(I.location);
            }
        };

        const drop = () => {
            this.viewer.scene.scene.remove(this.s);
            this.s.removeEventListener("drag", drag);
            this.s.removeEventListener("drop", drop);
        };

        this.s.addEventListener('drag', drag);
        this.s.addEventListener('drop', drop);

        this.viewer.scene.scene.add(this.s);
        this.viewer.inputHandler.startDragging(this.s);

        return annotation;
    }

    update() {
        // let camera = this.viewer.scene.getActiveCamera();
        // let domElement = this.renderer.domElement;
        // let measurements = this.viewer.scene.measurements;

        // const renderAreaSize = this.renderer.getSize(new Vector2());
        // let clientWidth = renderAreaSize.width;
        // let clientHeight = renderAreaSize.height;

    }

    render() {
        //this.viewer.renderer.render(this.scene, this.viewer.scene.getActiveCamera());
    }
}
