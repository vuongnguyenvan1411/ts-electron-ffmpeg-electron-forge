import {Box3, BoxGeometry, BufferAttribute, BufferGeometry, LineBasicMaterial, LineSegments, Mesh, MeshBasicMaterial, Object3D, Raycaster, Sphere, SphereGeometry, Vector3} from "three";
import {TextSprite} from "../core/TextSprite";

export abstract class Volume extends Object3D {
    protected _clip: any;
    protected _visible: boolean;
    showVolumeLabel: boolean;
    protected _modifiable: any;
    label: TextSprite;
    protected _properties: Record<string, any>;

    protected constructor(args: any = {}) {
        super();

        // if (this.constructor.name === "Volume") {
        //     console.warn("Can't create object of class Volume directly. Use classes BoxVolume or SphereVolume instead.");
        // }

        this._clip = args.clip || false;
        this._visible = true;
        this.showVolumeLabel = true;
        this._modifiable = args.modifiable || true;

        this.label = new TextSprite('0');
        this.label.setBorderColor({r: 0, g: 255, b: 0, a: 0.0});
        this.label.setBackgroundColor({r: 0, g: 255, b: 0, a: 0.0});
        this.label.material.depthTest = false;
        this.label.material.depthWrite = false;
        this.label.material.transparent = true;
        this.label.position.y -= 0.5;
        this.add(this.label);

        this.label.updateMatrixWorld = () => {
            const volumeWorldPos = new Vector3();

            volumeWorldPos.setFromMatrixPosition(this.matrixWorld);

            this.label.position.copy(volumeWorldPos);
            this.label.updateMatrix();
            this.label.matrixWorld.copy(this.label.matrix);
            this.label.matrixWorldNeedsUpdate = false;

            for (let i = 0, l = this.label.children.length; i < l; i++) {
                this.label.children[i].updateMatrixWorld(true);
            }
        };

        // event listeners
        this.addEventListener('select', () => {
            //
        });

        this.addEventListener('deselect', () => {
            //
        });
    }

    // @ts-ignore
    get visible() {
        return this._visible;
    }

    set visible(value) {
        if (this._visible !== value) {
            this._visible = value;

            this.dispatchEvent({type: "visibility_changed", object: this});
        }
    }

    getVolume() {
        console.warn("override this in subclass");
    }

    update() {

    };

    raycast(raycaster: Raycaster, intersects: any[]) {
        console.log('Volume.raycast', raycaster, intersects)
    }

    get clip() {
        return this._clip;
    }

    set clip(value) {
        if (this._clip !== value) {
            this._clip = value;

            this.update();

            this.dispatchEvent({
                type: "clip_changed",
                object: this
            });
        }

    }

    get modifieable() {
        return this._modifiable;
    }

    set modifieable(value) {
        this._modifiable = value;

        this.update();
    }

    get(key: string) {
        return this._properties[key];
    }

    set(key: string, value: any) {
        return this._properties[key] = value;
    }

    getKeys() {
        return Object.keys(this._properties);
    }

    getProperties() {
        return this._properties;
    }

    setProperties(data: Record<string, any>) {
        return this._properties = data;
    }

    hasProperties(key?: string) {
        if (key) {
            return this._properties.hasOwnProperty(key);
        } else {
            return this.getKeys.length > 0;
        }
    }
}

export class BoxVolume extends Volume {
    material: MeshBasicMaterial;
    box: Mesh;
    boundingBox: Box3;
    frame: LineSegments;
    boundingSphere: Sphere;

    constructor(args: any = {}) {
        super(args);

        (this.constructor as any).counter = ((this.constructor as any).counter === undefined) ? 0 : (this.constructor as any).counter + 1;
        this.name = 'box_' + (this.constructor as any).counter;

        const boxGeometry = new BoxGeometry(1, 1, 1);
        boxGeometry.computeBoundingBox();

        const boxFrameGeometry = new BufferGeometry();
        {
            const vertices = new Float32Array([
                // bottom
                -0.5, -0.5, 0.5,
                0.5, -0.5, 0.5,
                0.5, -0.5, 0.5,
                0.5, -0.5, -0.5,
                0.5, -0.5, -0.5,
                -0.5, -0.5, -0.5,
                -0.5, -0.5, -0.5,
                -0.5, -0.5, 0.5,
                // top
                -0.5, 0.5, 0.5,
                0.5, 0.5, 0.5,
                0.5, 0.5, 0.5,
                0.5, 0.5, -0.5,
                0.5, 0.5, -0.5,
                -0.5, 0.5, -0.5,
                -0.5, 0.5, -0.5,
                -0.5, 0.5, 0.5,
                // sides
                -0.5, -0.5, 0.5,
                -0.5, 0.5, 0.5,
                0.5, -0.5, 0.5,
                0.5, 0.5, 0.5,
                0.5, -0.5, -0.5,
                0.5, 0.5, -0.5,
                -0.5, -0.5, -0.5,
                -0.5, 0.5, -0.5,
            ]);

            boxFrameGeometry.setAttribute('position', new BufferAttribute(vertices, 3));
        }

        this.material = new MeshBasicMaterial({
            color: 0x00ff00,
            transparent: true,
            opacity: 0.3,
            depthTest: true,
            depthWrite: false
        });
        this.box = new Mesh(boxGeometry, this.material);
        this.box.geometry.computeBoundingBox();
        if (this.box.geometry.boundingBox) this.boundingBox = this.box.geometry.boundingBox;
        this.add(this.box);

        this.frame = new LineSegments(boxFrameGeometry, new LineBasicMaterial({color: 0x000000}));
        // this.frame.mode = Lines;
        this.add(this.frame);

        this.update();
    }

    update() {
        if (this.box.geometry.boundingBox) this.boundingBox = this.box.geometry.boundingBox;
        this.boundingSphere = this.boundingBox.getBoundingSphere(new Sphere());

        if (this._clip) {
            this.box.visible = false;
            this.label.visible = false;
        } else {
            this.box.visible = true;
            this.label.visible = this.showVolumeLabel;
        }
    }

    raycast(raycaster: Raycaster, intersects: any[]) {
        const is: any[] = [];
        this.box.raycast(raycaster, is);

        if (is.length > 0) {
            const I = is[0];

            intersects.push({
                distance: I.distance,
                object: this,
                point: I.point.clone()
            });
        }
    }

    getVolume() {
        return Math.abs(this.scale.x * this.scale.y * this.scale.z);
    }
}

export class SphereVolume extends Volume {
    material: MeshBasicMaterial;
    sphere: Mesh;
    boundingBox: Box3;
    frame: LineSegments | Mesh;
    boundingSphere: Sphere;

    constructor(args: any = {}) {
        super(args);

        (this.constructor as any).counter = ((this.constructor as any).counter === undefined) ? 0 : (this.constructor as any).counter + 1;
        this.name = 'sphere_' + (this.constructor as any).counter;

        const sphereGeometry = new SphereGeometry(1, 32, 32);
        sphereGeometry.computeBoundingBox();

        this.material = new MeshBasicMaterial({
            color: 0x00ff00,
            transparent: true,
            opacity: 0.3,
            depthTest: true,
            depthWrite: false
        });
        this.sphere = new Mesh(sphereGeometry, this.material);
        this.sphere.visible = false;
        this.sphere.geometry.computeBoundingBox();
        if (this.sphere.geometry.boundingBox) this.boundingBox = this.sphere.geometry.boundingBox;
        this.add(this.sphere);

        this.label.visible = false;

        const frameGeometry = new BufferGeometry();
        {
            const steps = 64;
            const uSegments = 8;
            const vSegments = 5;
            // let r = 1;

            for (let uSegment = 0; uSegment < uSegments; uSegment++) {
                const alpha = (uSegment / uSegments) * Math.PI * 2;
                const dirX = Math.cos(alpha);
                const dirY = Math.sin(alpha);

                for (let i = 0; i <= steps; i++) {
                    const v = (i / steps) * Math.PI * 2;
                    const vNext = v + 2 * Math.PI / steps;

                    const height = Math.sin(v);
                    const xyAmount = Math.cos(v);

                    const heightNext = Math.sin(vNext);
                    const xyAmountNext = Math.cos(vNext);

                    const vertex = new Vector3(dirX * xyAmount, dirY * xyAmount, height);
                    const vertexNext = new Vector3(dirX * xyAmountNext, dirY * xyAmountNext, heightNext);

                    const vertices = new Float32Array([
                        ...(vertex.toArray()),
                        ...(vertexNext.toArray()),
                    ]);

                    frameGeometry.setAttribute('position', new BufferAttribute(vertices, 3));
                }
            }

            // Create rings at poles, just because it's easier to implement
            for (let vSegment = 0; vSegment <= vSegments + 1; vSegment++) {
                //let height = (vSegment / (vSegments + 1)) * 2 - 1; // -1 to 1
                let uh = (vSegment / (vSegments + 1)); // -1 to 1
                uh = (1 - uh) * (-Math.PI / 2) + uh * (Math.PI / 2);
                const height = Math.sin(uh);

                for (let i = 0; i <= steps; i++) {
                    const u = (i / steps) * Math.PI * 2;
                    const uNext = u + 2 * Math.PI / steps;

                    const dirX = Math.cos(u);
                    const dirY = Math.sin(u);

                    const dirXNext = Math.cos(uNext);
                    const dirYNext = Math.sin(uNext);

                    const xyAmount = Math.sqrt(1 - height * height);

                    const vertex = new Vector3(dirX * xyAmount, dirY * xyAmount, height);
                    const vertexNext = new Vector3(dirXNext * xyAmount, dirYNext * xyAmount, height);

                    const vertices = new Float32Array([
                        ...(vertex.toArray()),
                        ...(vertexNext.toArray()),
                    ]);

                    frameGeometry.setAttribute('position', new BufferAttribute(vertices, 3));
                }
            }
        }

        this.frame = new LineSegments(frameGeometry, new LineBasicMaterial({color: 0x000000}));
        this.add(this.frame);

        const frameMaterial = new MeshBasicMaterial({wireframe: true, color: 0x000000});
        this.frame = new Mesh(sphereGeometry, frameMaterial);
        //this.add(this.frame);

        //this.frame = new LineSegments(boxFrameGeometry, new LineBasicMaterial({color: 0x000000}));
        // this.frame.mode = Lines;
        //this.add(this.frame);

        this.update();
    }

    update() {
        if (this.sphere.geometry.boundingBox) {
            this.boundingBox = this.sphere.geometry.boundingBox;
        }

        this.boundingSphere = this.boundingBox.getBoundingSphere(new Sphere());

        //if (this._clip) {
        //	this.sphere.visible = false;
        //	this.label.visible = false;
        //} else {
        //	this.sphere.visible = true;
        //	this.label.visible = this.showVolumeLabel;
        //}
    }

    raycast(raycaster: Raycaster, intersects: any[]) {
        const is: any[] = [];

        this.sphere.raycast(raycaster, is);

        if (is.length > 0) {
            const I = is[0];

            intersects.push({
                distance: I.distance,
                object: this,
                point: I.point.clone()
            });
        }
    }

    /**
     * @see https://en.wikipedia.org/wiki/Ellipsoid#Volume
     */
    getVolume() {
        return (4 / 3) * Math.PI * this.scale.x * this.scale.y * this.scale.z;
    }
}
