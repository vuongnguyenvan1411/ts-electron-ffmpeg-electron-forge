import {MOUSE, PointLight, Scene, Vector2, Vector3, WebGLRenderer} from "three";
import {Profile} from "./Profile";
import {Utils} from "../core/Utils";
import {EventDispatcher} from "../core/EventDispatcher";
import {Viewer} from "../viewer/viewer";
import {ViewerEventName} from "../core/Event";

type TArgsStartInsertion = {
    name?: string,
    properties?: Record<string, any>
}

export class ProfileTool extends EventDispatcher {
    viewer: Viewer;
    renderer: WebGLRenderer;
    scene: Scene;
    light: PointLight;
    profiles: Profile[];

    constructor(viewer: Viewer) {
        super();

        this.viewer = viewer;
        this.renderer = viewer.renderer;

        this.profiles = viewer.scene.profiles;

        this.addEventListener(ViewerEventName.StartInsertingProfile, () => {
            this.viewer.dispatchEvent({
                type: ViewerEventName.CancelInsertions
            });
        });

        this.scene = new Scene();
        this.scene.name = 'scene_profile';
        this.light = new PointLight(0xffffff, 1.0);
        this.scene.add(this.light);

        this.viewer.inputHandler.registerInteractiveScene(this.scene);

        for (let profile of viewer.scene.profiles) {
            this.onAdd({profile: profile});
        }

        viewer.addEventListener(ViewerEventName.Update, this.update.bind(this));
        viewer.addEventListener(ViewerEventName.Render_Pass_PerspectiveOverlay, this.render.bind(this));
        viewer.addEventListener(ViewerEventName.SceneChanged, this.onSceneChange.bind(this));

        viewer.scene.addEventListener(ViewerEventName.ProfileAdded, this.onAdd);
        viewer.scene.addEventListener(ViewerEventName.ProfileRemoved, this.onRemove);
    }

    onRemove = (e: any) => this.scene.remove(e.profile);
    onAdd = (e: any) => this.scene.add(e.profile);

    onSceneChange(e: any) {
        if (e.oldScene) {
            e.oldScene.removeEventListeners(ViewerEventName.ProfileAdded/*, this.onAdd*/);
            e.oldScene.removeEventListeners(ViewerEventName.ProfileRemoved/*, this.onRemove*/);
        }

        e.scene.addEventListener(ViewerEventName.ProfileAdded, this.onAdd);
        e.scene.addEventListener(ViewerEventName.ProfileRemoved, this.onRemove);
    }

    startInsertion(args: TArgsStartInsertion = {}) {
        const domElement = this.viewer.renderer.domElement;

        const profile = new Profile();

        profile.name = args.name ?? 'Profile';

        if (args.properties) {
            profile.setProperties(args.properties);
        }

        this.dispatchEvent({
            type: ViewerEventName.StartInsertingProfile,
            profile: profile
        });

        this.scene.add(profile);

        const cancel = {
            callback: () => {
                //
            }
        };

        const insertionCallback = (e: any) => {
            if (e.button === MOUSE.LEFT) {
                if (profile.points.length <= 1) {
                    let camera = this.viewer.scene.getActiveCamera();
                    let distance = camera.position.distanceTo(profile.points[0]);
                    let clientSize = this.viewer.renderer.getSize(new Vector2());
                    let pr = Utils.projectedRadius(1, camera, distance, clientSize.width, clientSize.height);
                    let width = (10 / pr);

                    profile.setWidth(width);
                }

                profile.addMarker(profile.points[profile.points.length - 1].clone());

                this.viewer.inputHandler.startDragging(
                    profile.spheres[profile.spheres.length - 1]);
            } else if (e.button === MOUSE.RIGHT) {
                cancel.callback();
            }
        };

        cancel.callback = () => {
            profile.removeMarker(profile.points.length - 1);
            domElement.removeEventListener('mouseup', insertionCallback, true);
            this.viewer.removeEventListener(ViewerEventName.CancelInsertions, cancel.callback);
        };

        this.viewer.addEventListener(ViewerEventName.CancelInsertions, cancel.callback);
        domElement.addEventListener('mouseup', insertionCallback, true);

        profile.addMarker(new Vector3(0, 0, 0));
        this.viewer.inputHandler.startDragging(profile.spheres[profile.spheres.length - 1]);

        this.viewer.scene.addProfile(profile);

        return profile;
    }

    update() {
        let camera = this.viewer.scene.getActiveCamera();
        let profiles = this.viewer.scene.profiles;
        let renderAreaSize = this.viewer.renderer.getSize(new Vector2());
        let clientWidth = renderAreaSize.width;
        let clientHeight = renderAreaSize.height;

        this.light.position.copy(camera.position);

        // make size independant of distance
        for (let profile of profiles) {
            for (let sphere of profile.spheres) {
                let distance = camera.position.distanceTo(sphere.getWorldPosition(new Vector3()));
                let pr = Utils.projectedRadius(1, camera, distance, clientWidth, clientHeight);
                let scale = (15 / pr);

                sphere.scale.set(scale, scale, scale);
            }
        }
    }

    render() {
        this.viewer.renderer.render(this.scene, this.viewer.scene.getActiveCamera());
    }
}
