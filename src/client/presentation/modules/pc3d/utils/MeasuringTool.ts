import {MathUtils, MOUSE, PointLight, Scene, Vector2, Vector3, WebGLRenderer} from "three";
import {Utils} from "../core/Utils";
import {CameraMode} from "../core/Defines";
import {EventDispatcher} from "../core/EventDispatcher";
import {Measure} from "./Measure";
import {Viewer} from "../viewer/viewer";
import {App} from "../core/App";
import {ViewerEventName} from "../core/Event";

function updateAzimuth(viewer: Viewer, measure: Measure) {
    const azimuth = measure.azimuth;

    const isOkay = measure.points.length === 2;

    azimuth.node.visible = isOkay && measure.showAzimuth;

    if (!azimuth.node.visible) {
        return;
    }

    const camera = viewer.scene.getActiveCamera();
    const renderAreaSize = viewer.renderer.getSize(new Vector2());
    const width = renderAreaSize.width;
    const height = renderAreaSize.height;

    const [p0, p1] = measure.points;
    const r = p0.position.distanceTo(p1.position);
    const northVec = Utils.getNorthVec(p0.position, r, viewer.getProjection()!);
    const northPos = p0.position.clone().add(northVec);

    azimuth.center.position.copy(p0.position);
    azimuth.center.scale.set(2, 2, 2);

    azimuth.center.visible = false;
    // azimuth.target.visible = false;

    { // north
        azimuth.north.position.copy(northPos);
        azimuth.north.scale.set(2, 2, 2);

        let distance = azimuth.north.position.distanceTo(camera.position);
        let pr = Utils.projectedRadius(1, camera, distance, width, height);

        let scale = (5 / pr);
        azimuth.north.scale.set(scale, scale, scale);
    }

    { // target
        azimuth.target.position.copy(p1.position);
        azimuth.target.position.z = azimuth.north.position.z;

        let distance = azimuth.target.position.distanceTo(camera.position);
        let pr = Utils.projectedRadius(1, camera, distance, width, height);

        let scale = (5 / pr);
        azimuth.target.scale.set(scale, scale, scale);
    }


    azimuth.circle.position.copy(p0.position);
    azimuth.circle.scale.set(r, r, r);
    azimuth.circle.material.resolution.set(width, height);

    // to target
    azimuth.centerToTarget.geometry.setPositions([
        0, 0, 0,
        ...p1.position.clone().sub(p0.position).toArray(),
    ]);
    azimuth.centerToTarget.position.copy(p0.position);
    azimuth.centerToTarget.geometry.verticesNeedUpdate = true;
    azimuth.centerToTarget.geometry.computeBoundingSphere();
    azimuth.centerToTarget.computeLineDistances();
    azimuth.centerToTarget.material.resolution.set(width, height);

    // to target ground
    azimuth.centerToTargetGround.geometry.setPositions([
        0, 0, 0,
        p1.position.x - p0.position.x,
        p1.position.y - p0.position.y,
        0,
    ]);
    azimuth.centerToTargetGround.position.copy(p0.position);
    azimuth.centerToTargetGround.geometry.verticesNeedUpdate = true;
    azimuth.centerToTargetGround.geometry.computeBoundingSphere();
    azimuth.centerToTargetGround.computeLineDistances();
    azimuth.centerToTargetGround.material.resolution.set(width, height);

    // to north
    azimuth.centerToNorth.geometry.setPositions([
        0, 0, 0,
        northPos.x - p0.position.x,
        northPos.y - p0.position.y,
        0,
    ]);
    azimuth.centerToNorth.position.copy(p0.position);
    azimuth.centerToNorth.geometry.verticesNeedUpdate = true;
    azimuth.centerToNorth.geometry.computeBoundingSphere();
    azimuth.centerToNorth.computeLineDistances();
    azimuth.centerToNorth.material.resolution.set(width, height);

    // label
    const radians = Utils.computeAzimuth(p0.position, p1.position, viewer.getProjection()!);
    let degrees = MathUtils.radToDeg(radians);
    if (degrees < 0) {
        degrees = 360 + degrees;
    }
    const txtDegrees = `${degrees.toFixed(2)}°`;
    const labelDir = northPos.clone().add(p1.position).multiplyScalar(0.5).sub(p0.position);
    if (labelDir.length() > 0) {
        labelDir.z = 0;
        labelDir.normalize();
        const labelVec = labelDir.clone().multiplyScalar(r);
        const labelPos = p0.position.clone().add(labelVec);
        azimuth.label.position.copy(labelPos);
    }
    azimuth.label.setText(txtDegrees);
    let distance = azimuth.label.position.distanceTo(camera.position);
    let pr = Utils.projectedRadius(1, camera, distance, width, height);
    let scale = (70 / pr);
    azimuth.label.scale.set(scale, scale, scale);
}

type TArgsStartInsertion = {
    showDistances?: boolean,
    showAngles?: boolean,
    showArea?: boolean,
    showCoordinates?: boolean,
    showHeight?: boolean,
    showCircle?: boolean,
    showAzimuth?: boolean,
    showEdges?: boolean,
    closed: boolean,
    maxMarkers?: number,
    name: string,
    properties?: Record<string, any>
}

export class MeasuringTool extends EventDispatcher {
    viewer: Viewer;
    renderer: WebGLRenderer;
    showLabels: boolean;
    scene: Scene;
    light: PointLight;
    measurements: any[];

    constructor(viewer: Viewer) {
        super();

        this.viewer = viewer;
        this.renderer = viewer.renderer;

        this.measurements = viewer.scene.measurements;

        this.addEventListener(ViewerEventName.StartInsertingMeasurement, () => {
            this.viewer.dispatchEvent({
                type: ViewerEventName.CancelInsertions
            });
        });

        this.showLabels = true;
        this.scene = new Scene();
        this.scene.name = 'scene_measurement';
        this.light = new PointLight(0xffffff, 1.0);
        this.scene.add(this.light);

        this.viewer.inputHandler.registerInteractiveScene(this.scene);

        for (let measurement of viewer.scene.measurements) {
            this.onAdd({measurement: measurement});
        }

        viewer.addEventListener(ViewerEventName.Update, this.update);
        viewer.addEventListener(ViewerEventName.Render_Pass_PerspectiveOverlay, this.render);
        viewer.addEventListener(ViewerEventName.SceneChanged, this.onSceneChange);

        viewer.scene.addEventListener(ViewerEventName.MeasurementAdded, this.onAdd);
        viewer.scene.addEventListener(ViewerEventName.MeasurementRemoved, this.onRemove);
    }

    onRemove = (e: any) => {
        this.scene.remove(e.measurement);
    }

    onAdd = (e: any) => {
        this.scene.add(e.measurement);
    }

    onSceneChange = (e: any) => {
        if (e.oldScene) {
            e.oldScene.removeEventListener(ViewerEventName.MeasurementAdded, this.onAdd);
            e.oldScene.removeEventListener(ViewerEventName.MeasurementRemoved, this.onRemove);
        }

        e.scene.addEventListener(ViewerEventName.MeasurementAdded, this.onAdd);
        e.scene.addEventListener(ViewerEventName.MeasurementRemoved, this.onRemove);
    }

    startInsertion(args: TArgsStartInsertion) {
        const domElement = this.viewer.renderer.domElement;
        const measure = new Measure();

        measure.showDistances = args.showDistances ?? true;
        measure.showArea = args.showArea ?? false;
        measure.showAngles = args.showAngles ?? false;
        measure.showCoordinates = args.showCoordinates ?? false;
        measure.showHeight = args.showHeight ?? false;
        measure.showCircle = args.showCircle ?? false;
        measure.showAzimuth = args.showAzimuth ?? false;
        measure.showEdges = args.showEdges ?? true;
        measure.closed = args.closed ?? false;
        measure.maxMarkers = args.maxMarkers ?? Infinity;

        measure.name = args.name ?? 'Measurement';

        if (args.properties) {
            measure.setProperties(args.properties);
        }

        this.dispatchEvent({
            type: ViewerEventName.StartInsertingMeasurement,
            measure: measure
        });

        this.scene.add(measure);

        const cancel = {
            removeLastMarker: measure.maxMarkers > 3,
            callback: () => {
                //
            }
        };

        const insertionCallback = (e: any) => {
            if (e.button === MOUSE.LEFT) {
                measure.addMarker(measure.points[measure.points.length - 1].position.clone());

                if (measure.points.length >= measure.maxMarkers) {
                    cancel.callback();
                }

                this.viewer.inputHandler.startDragging(
                    measure.spheres[measure.spheres.length - 1]);
            } else if (e.button === MOUSE.RIGHT) {
                cancel.callback();
            }
        };

        cancel.callback = () => {
            if (cancel.removeLastMarker) {
                measure.removeMarker(measure.points.length - 1);
            }

            domElement.removeEventListener('mouseup', insertionCallback, true);
            this.viewer.removeEventListener(ViewerEventName.CancelInsertions, cancel.callback);
        };

        if (measure.maxMarkers > 1) {
            this.viewer.addEventListener(ViewerEventName.CancelInsertions, cancel.callback);
            domElement.addEventListener('mouseup', insertionCallback, true);
        }

        measure.addMarker(new Vector3(0, 0, 0));
        this.viewer.inputHandler.startDragging(
            measure.spheres[measure.spheres.length - 1]);

        this.viewer.scene.addMeasurement(measure);

        return measure;
    }

    update = () => {
        const camera = this.viewer.scene.getActiveCamera();
        const measurements = this.viewer.scene.measurements;

        const renderAreaSize = this.renderer.getSize(new Vector2());
        const clientWidth = renderAreaSize.width;
        const clientHeight = renderAreaSize.height;

        this.light.position.copy(camera.position);

        // make size independent of distance
        for (const measure of measurements) {
            measure.lengthUnit = this.viewer.lengthUnit;
            measure.lengthUnitDisplay = this.viewer.lengthUnitDisplay;
            measure.update();

            updateAzimuth(this.viewer, measure);

            // spheres
            for (const sphere of measure.spheres) {
                const distance = camera.position.distanceTo(sphere.getWorldPosition(new Vector3()));
                const pr = Utils.projectedRadius(1, camera, distance, clientWidth, clientHeight);
                const scale = (15 / pr);
                sphere.scale.set(scale, scale, scale);
            }

            // labels
            const labels = measure.edgeLabels.concat(measure.angleLabels);
            for (const label of labels) {
                const distance = camera.position.distanceTo(label.getWorldPosition(new Vector3()));
                const pr = Utils.projectedRadius(1, camera, distance, clientWidth, clientHeight);
                let scale = (70 / pr);

                if (App.debug.scale) {
                    scale = (App.debug.scale / pr);
                }

                label.scale.set(scale, scale, scale);
            }

            // coordinate labels
            for (let j = 0; j < measure.coordinateLabels.length; j++) {
                const label = measure.coordinateLabels[j];
                const sphere = measure.spheres[j];
                const distance = camera.position.distanceTo(sphere.getWorldPosition(new Vector3()));
                const screenPos = sphere.getWorldPosition(new Vector3()).clone().project(camera);

                screenPos.x = Math.round((screenPos.x + 1) * clientWidth / 2);
                screenPos.y = Math.round((-screenPos.y + 1) * clientHeight / 2);
                screenPos.z = 0;
                screenPos.y -= 30;

                let labelPos = new Vector3(
                    (screenPos.x / clientWidth) * 2 - 1,
                    -(screenPos.y / clientHeight) * 2 + 1,
                    0.5
                );

                labelPos.unproject(camera);

                if (this.viewer.scene.cameraMode === CameraMode.PERSPECTIVE) {
                    let direction = labelPos.sub(camera.position).normalize();
                    labelPos = new Vector3().addVectors(camera.position, direction.multiplyScalar(distance));
                }

                label.position.copy(labelPos);
                const pr = Utils.projectedRadius(1, camera, distance, clientWidth, clientHeight);
                const scale = (70 / pr);
                label.scale.set(scale, scale, scale);
            }

            // height label
            if (measure.showHeight) {
                const label = measure.heightLabel;

                {
                    const distance = label.position.distanceTo(camera.position);
                    const pr = Utils.projectedRadius(1, camera, distance, clientWidth, clientHeight);
                    const scale = (70 / pr);
                    label.scale.set(scale, scale, scale);
                }

                { // height edge
                    const edge = measure.heightEdge;

                    const sorted = measure.points.slice().sort((a: any, b: any) => a.position.z - b.position.z);
                    const lowPoint = sorted[0].position.clone();
                    const highPoint = sorted[sorted.length - 1].position.clone();
                    const min = lowPoint.z;
                    const max = highPoint.z;

                    const start = new Vector3(highPoint.x, highPoint.y, min);
                    const end = new Vector3(highPoint.x, highPoint.y, max);

                    const lowScreen = lowPoint.clone().project(camera);
                    const startScreen = start.clone().project(camera);
                    const endScreen = end.clone().project(camera);

                    const toPixelCoordinates = (v: any) => {
                        let r = v.clone().addScalar(1).divideScalar(2);
                        r.x = r.x * clientWidth;
                        r.y = r.y * clientHeight;
                        r.z = 0;

                        return r;
                    };

                    const lowEL = toPixelCoordinates(lowScreen);
                    const startEL = toPixelCoordinates(startScreen);
                    const endEL = toPixelCoordinates(endScreen);

                    const lToS = lowEL.distanceTo(startEL);
                    const sToE = startEL.distanceTo(endEL);

                    edge.geometry.lineDistances = [0, lToS, lToS, lToS + sToE];
                    edge.geometry.lineDistancesNeedUpdate = true;

                    edge.material.dashSize = 10;
                    edge.material.gapSize = 10;
                }
            }

            { // area label
                const label = measure.areaLabel;
                const distance = label.position.distanceTo(camera.position);
                const pr = Utils.projectedRadius(1, camera, distance, clientWidth, clientHeight);
                const scale = (70 / pr);

                label.scale.set(scale, scale, scale);
            }

            { // radius label
                const label = measure.circleRadiusLabel;
                const distance = label.position.distanceTo(camera.position);
                const pr = Utils.projectedRadius(1, camera, distance, clientWidth, clientHeight);

                const scale = (70 / pr);
                label.scale.set(scale, scale, scale);
            }

            { // edges
                const materials = [
                    measure.circleRadiusLine.material,
                    ...measure.edges.map((e: any) => e.material),
                    measure.heightEdge.material,
                    measure.circleLine.material,
                ];

                for (const material of materials) {
                    material.resolution.set(clientWidth, clientHeight);
                }
            }

            if (!this.showLabels) {
                const labels = [
                    ...measure.sphereLabels,
                    ...measure.edgeLabels,
                    ...measure.angleLabels,
                    ...measure.coordinateLabels,
                    measure.heightLabel,
                    measure.areaLabel,
                    measure.circleRadiusLabel,
                ];

                for (const label of labels) {
                    label.visible = false;
                }
            }
        }
    }

    render = () => {
        this.viewer.renderer.render(this.scene, this.viewer.scene.getActiveCamera());
    }
}
