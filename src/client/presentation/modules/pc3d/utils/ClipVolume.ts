import {Box3, BoxGeometry, BufferAttribute, BufferGeometry, CylinderGeometry, Line, LineBasicMaterial, LineSegments, Mesh, MeshBasicMaterial, Object3D, Sphere, Vector3, Vector4} from "three";
import {MouseEventName} from "../core/Event";

export class ClipVolume extends Object3D {
    clipOffset: number;
    clipRotOffset: number;
    dimension: Vector3;
    material: MeshBasicMaterial;
    box: Mesh;
    boundingBox: Box3 | null;
    frame: LineSegments;
    planeFrame: LineSegments;
    arrowX: Object3D;
    arrowY: Object3D;
    arrowZ: Object3D;
    localX: Vector3;
    localY: Vector3;
    localZ: Vector3;
    boundingSphere: Sphere;

    constructor(args: any) {
        super();

        (this.constructor as any).counter = ((this.constructor as any).counter === undefined) ? 0 : (this.constructor as any).counter + 1;
        this.name = "clip_volume_" + (this.constructor as any).counter;

        let alpha = args.alpha || 0;
        let beta = args.beta || 0;
        let gamma = args.gamma || 0;

        this.rotation.x = alpha;
        this.rotation.y = beta;
        this.rotation.z = gamma;

        this.clipOffset = 0.001;
        this.clipRotOffset = 1;

        let boxGeometry = new BoxGeometry(1, 1, 1);
        boxGeometry.computeBoundingBox();

        let boxFrameGeometry = new BufferGeometry();
        {
            const vertices = new Float32Array([
                // bottom
                -0.5, -0.5, 0.5,
                0.5, -0.5, 0.5,
                0.5, -0.5, 0.5,
                0.5, -0.5, -0.5,
                0.5, -0.5, -0.5,
                -0.5, -0.5, -0.5,
                -0.5, -0.5, -0.5,
                -0.5, -0.5, 0.5,
                // top
                -0.5, 0.5, 0.5,
                0.5, 0.5, 0.5,
                0.5, 0.5, 0.5,
                0.5, 0.5, -0.5,
                0.5, 0.5, -0.5,
                -0.5, 0.5, -0.5,
                -0.5, 0.5, -0.5,
                -0.5, 0.5, 0.5,
                // sides
                -0.5, -0.5, 0.5,
                -0.5, 0.5, 0.5,
                0.5, -0.5, 0.5,
                0.5, 0.5, 0.5,
                0.5, -0.5, -0.5,
                0.5, 0.5, -0.5,
                -0.5, -0.5, -0.5,
                -0.5, 0.5, -0.5,
            ]);

            boxFrameGeometry.setAttribute('position', new BufferAttribute(vertices, 3));
            boxFrameGeometry.setAttribute('colors', new BufferAttribute(new Float32Array([
                1, 1, 1
            ]), 3));
        }

        let planeFrameGeometry = new BufferGeometry();
        {
            const vertices = new Float32Array([
                -0.5, -0.5, 0.0,
                -0.5, 0.5, 0.0,
                0.5, 0.5, 0.0,
                0.5, -0.5, 0.0,
                -0.5, 0.5, 0.0,
                0.5, 0.5, 0.0,
                -0.5, -0.5, 0.0,
                0.5, -0.5, 0.0,
            ]);

            boxFrameGeometry.setAttribute('position', new BufferAttribute(vertices, 3));
        }

        this.dimension = new Vector3(1, 1, 1);
        this.material = new MeshBasicMaterial({
            color: 0x00ff00,
            transparent: true,
            opacity: 0.3,
            depthTest: true,
            depthWrite: false
        });
        this.box = new Mesh(boxGeometry, this.material);
        this.box.geometry.computeBoundingBox();
        this.boundingBox = this.box.geometry.boundingBox;
        this.add(this.box);

        this.frame = new LineSegments(boxFrameGeometry, new LineBasicMaterial({color: 0x000000}));
        this.add(this.frame);
        this.planeFrame = new LineSegments(planeFrameGeometry, new LineBasicMaterial({color: 0xff0000}));
        this.add(this.planeFrame);

        // set default thickness
        this.setScaleZ(0.1);

        // create local coordinate system
        let createArrow = (name: any, color: any) => {
            let material = new MeshBasicMaterial({
                color: color,
                depthTest: false,
                depthWrite: false
            });

            const shaftGeometry = new BufferGeometry();

            const vertices = new Float32Array([
                0, 0, 0,
                0, 1, 0
            ]);

            shaftGeometry.setAttribute('position', new BufferAttribute(vertices, vertices.length));

            const shaftMaterial = new LineBasicMaterial({
                color: color,
                depthTest: false,
                depthWrite: false,
                transparent: true
            });
            const shaft = new Line(shaftGeometry, shaftMaterial);
            shaft.name = name + "_shaft";

            const headGeometry = new CylinderGeometry(0, 0.04, 0.1, 10, 1, false);
            const head = new Mesh(headGeometry, material);
            head.name = name + "_head";
            head.position.y = 1;

            const arrow = new Object3D();
            arrow.name = name;
            arrow.add(shaft);
            arrow.add(head);

            return arrow;
        };

        this.arrowX = createArrow("arrow_x", 0xFF0000);
        this.arrowY = createArrow("arrow_y", 0x00FF00);
        this.arrowZ = createArrow("arrow_z", 0x0000FF);

        this.arrowX.rotation.z = -Math.PI / 2;
        this.arrowZ.rotation.x = Math.PI / 2;

        this.arrowX.visible = false;
        this.arrowY.visible = false;
        this.arrowZ.visible = false;

        this.add(this.arrowX);
        this.add(this.arrowY);
        this.add(this.arrowZ);

        // event listeners
        this.addEventListener("ui_select", () => {
            this.arrowX.visible = true;
            this.arrowY.visible = true;
            this.arrowZ.visible = true;
        });

        this.addEventListener("ui_deselect", () => {
            this.arrowX.visible = false;
            this.arrowY.visible = false;
            this.arrowZ.visible = false;
        });

        this.addEventListener("select", () => {
            const scene_header = document.querySelector(`#${this.name} .scene_header`);

            if (scene_header
                && scene_header.nextElementSibling
                && (scene_header.nextElementSibling as any).style.visibility === "visible"
            ) {
                scene_header.dispatchEvent(new Event(MouseEventName.Click));
            }
        });

        this.addEventListener("deselect", () => {
            const scene_header = document.querySelector(`#${this.name} .scene_header`);

            if (scene_header
                && scene_header.nextElementSibling
                && (scene_header.nextElementSibling as any).style.visibility === "visible"
            ) {
                scene_header.dispatchEvent(new Event(MouseEventName.Click));
            }
        });

        this.update();
    }

    setClipOffset(offset: any) {
        this.clipOffset = offset;
    }

    setClipRotOffset(offset: any) {
        this.clipRotOffset = offset;
    }

    setScaleX(x: any) {
        this.box.scale.x = x;
        this.frame.scale.x = x;
        this.planeFrame.scale.x = x;
    }

    setScaleY(y: any) {
        this.box.scale.y = y;
        this.frame.scale.y = y;
        this.planeFrame.scale.y = y;
    }

    setScaleZ(z: any) {
        this.box.scale.z = z;
        this.frame.scale.z = z;
        this.planeFrame.scale.z = z;
    }

    offset(args: any) {
        let cs = args.cs || null;
        let axis = args.axis || null;
        let dir = args.dir || null;

        if (!cs || !axis || !dir) return;

        if (axis === "x") {
            if (cs === "local") {
                this.position.add(this.localX.clone().multiplyScalar(dir * this.clipOffset));
            } else if (cs === "global") {
                this.position.x = this.position.x + dir * this.clipOffset;
            }
        } else if (axis === "y") {
            if (cs === "local") {
                this.position.add(this.localY.clone().multiplyScalar(dir * this.clipOffset));
            } else if (cs === "global") {
                this.position.y = this.position.y + dir * this.clipOffset;
            }
        } else if (axis === "z") {
            if (cs === "local") {
                this.position.add(this.localZ.clone().multiplyScalar(dir * this.clipOffset));
            } else if (cs === "global") {
                this.position.z = this.position.z + dir * this.clipOffset;
            }
        }

        // @ts-ignore
        this.dispatchEvent({"type": "clip_volume_changed", "viewer": viewer, "volume": this});
    }

    rotate(args: any) {
        let cs = args.cs || null;
        let axis = args.axis || null;
        let dir = args.dir || null;

        if (!cs || !axis || !dir) return;

        if (cs === "local") {
            if (axis === "x") {
                this.rotateOnAxis(new Vector3(1, 0, 0), dir * this.clipRotOffset * Math.PI / 180);
            } else if (axis === "y") {
                this.rotateOnAxis(new Vector3(0, 1, 0), dir * this.clipRotOffset * Math.PI / 180);
            } else if (axis === "z") {
                this.rotateOnAxis(new Vector3(0, 0, 1), dir * this.clipRotOffset * Math.PI / 180);
            }
        } else if (cs === "global") {
            let rotAxis = new Vector4(1, 0, 0, 0);
            if (axis === "y") {
                rotAxis = new Vector4(0, 1, 0, 0);
            } else if (axis === "z") {
                rotAxis = new Vector4(0, 0, 1, 0);
            }
            this.updateMatrixWorld();
            // @ts-ignore
            let invM = newthis.matrixWorld.clone().invert();
            rotAxis = rotAxis.applyMatrix4(invM).normalize();
            // @ts-ignore
            rotAxis = new Vector3(rotAxis.x, rotAxis.y, rotAxis.z);
            // @ts-ignore
            this.rotateOnAxis(rotAxis, dir * this.clipRotOffset * Math.PI / 180);
        }

        this.updateLocalSystem();

        // @ts-ignore
        this.dispatchEvent({"type": "clip_volume_changed", "viewer": viewer, "volume": this});
    }

    update() {
        this.boundingBox = this.box.geometry.boundingBox;
        if (this.boundingBox) this.boundingSphere = this.boundingBox.getBoundingSphere(new Sphere());

        this.box.visible = false;

        this.updateLocalSystem();
    };

    updateLocalSystem() {
        // extract local coordinate axes
        // @ts-ignore
        let rotQuat = this.getWorldQuaternion();

        this.localX = new Vector3(1, 0, 0).applyQuaternion(rotQuat).normalize();
        this.localY = new Vector3(0, 1, 0).applyQuaternion(rotQuat).normalize();
        this.localZ = new Vector3(0, 0, 1).applyQuaternion(rotQuat).normalize();
    }

    raycast(raycaster: any, intersects: any) {
        let is: any[] = [];
        this.box.raycast(raycaster, is);

        if (is.length > 0) {
            let I = is[0];

            intersects.push({
                distance: I.distance,
                object: this,
                point: I.point.clone()
            });
        }
    };
}
