import {Scene as ThreeScene, Vector2} from "three";
import {ClipVolume} from "./ClipVolume";
import {PolygonClipVolume} from "./PolygonClipVolume";
import {EventDispatcher} from "../core/EventDispatcher";
import {Viewer} from "../viewer/viewer";
import {VScene} from "../viewer/VScene";
import {InputHandlerEvent, ViewerEventName, VSceneEvent} from "../core/Event";
import {MOUSE} from "../core/Defines";

type TArgsStartInsertion = {
    type?: string;
    name?: string;
    properties?: Record<string, any>;
}

export class ClippingTool extends EventDispatcher {
    viewer: Viewer;
    scene: VScene;
    maxPolygonVertices: number;
    sceneMarker: ThreeScene;
    sceneVolume: ThreeScene;

    constructor(viewer: Viewer) {
        super();

        this.viewer = viewer;

        this.maxPolygonVertices = 8;

        this.addEventListener(ViewerEventName.StartInsertingClippingVolume, () => {
            this.viewer.dispatchEvent({
                type: ViewerEventName.CancelInsertions
            });
        });

        this.sceneMarker = new ThreeScene();
        this.sceneVolume = new ThreeScene();
        this.sceneVolume.name = "scene_clip_volume";
        this.viewer.inputHandler.registerInteractiveScene(this.sceneVolume);

        this.viewer.inputHandler.addEventListener(InputHandlerEvent.Delete, (e: any) => {
            let volumes = e.selection.filter((e: any) => (e instanceof ClipVolume));
            // @ts-ignore
            volumes.forEach(e => this.viewer.scene.removeClipVolume(e));
            let polyVolumes = e.selection.filter((e: any) => (e instanceof PolygonClipVolume));
            polyVolumes.forEach((e: any) => this.viewer.scene.removePolygonClipVolume(e));
        });
    }

    setScene(scene: VScene) {
        if (this.scene === scene) {
            return;
        }

        if (this.scene) {
            this.scene.removeEventListeners(VSceneEvent.ClipVolumeAdded/*, this.onAdd*/);
            this.scene.removeEventListeners(VSceneEvent.ClipVolumeRemoved/*, this.onRemove*/);
            this.scene.removeEventListeners(VSceneEvent.PolygonClipVolumeAdded/*, this.onAdd*/);
            this.scene.removeEventListeners(VSceneEvent.PolygonClipVolumeRemoved/*, this.onRemove*/);
        }

        this.scene = scene;

        this.scene.addEventListener(VSceneEvent.ClipVolumeAdded, this.onAdd);
        this.scene.addEventListener(VSceneEvent.ClipVolumeRemoved, this.onRemove);
        this.scene.addEventListener(VSceneEvent.PolygonClipVolumeAdded, this.onAdd);
        this.scene.addEventListener(VSceneEvent.PolygonClipVolumeRemoved, this.onRemove);
    }

    onRemove = (e: any) => {
        this.sceneVolume.remove(e.volume);
    }

    onAdd = (e: any) => {
        this.sceneVolume.add(e.volume);
    }

    startInsertion(args: TArgsStartInsertion = {}) {
        const type = args.type || null;

        if (!type) return null;

        const domElement = this.viewer.renderer.domElement;
        const canvasSize = this.viewer.renderer.getSize(new Vector2());

        const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        svg.setAttributeNS(null, "height", canvasSize.height.toString());
        svg.setAttributeNS(null, "width", canvasSize.width.toString());
        svg.setAttributeNS(null, "style", "position:absolute; pointer-events: none");
        svg.innerHTML = `
            <defs>
                <marker id="diamond" markerWidth="24" markerHeight="24" refX="12" refY="12" markerUnits="userSpaceOnUse">
                    <circle cx="12" cy="12" r="6" fill="white" stroke="black" stroke-width="3"/>
                </marker>
            </defs>
            
            <polyline
                    fill="none"
                    stroke="black"
                    style="stroke:rgb(0, 0, 0);stroke-width:6;"
                    stroke-dasharray="9, 6"
                    stroke-dashoffset="2"
                    points=""
            />
            
            <polyline
                    fill="none"
                    stroke="black"
                    style="stroke:rgb(255, 255, 255);stroke-width:2;"
                    stroke-dasharray="5, 10"
                    marker-start="url(#diamond)"
                    marker-mid="url(#diamond)"
                    marker-end="url(#diamond)"
                    points=""
            />
        `;

        domElement.parentElement!.append(svg);

        const polyClipVol = new PolygonClipVolume(this.viewer.scene.getActiveCamera().clone());

        polyClipVol.name = args.name ?? 'PolygonClip';

        if (args.properties) {
            polyClipVol.setProperties(args.properties);
        }

        this.dispatchEvent({
            type: ViewerEventName.StartInsertingClippingVolume
        });

        this.viewer.scene.addPolygonClipVolume(polyClipVol);
        this.sceneMarker.add(polyClipVol);

        const cancel = {
            callback: () => {
                //
            }
        };

        const insertionCallback = (e: any) => {
            if (e.button === MOUSE.LEFT) {
                polyClipVol.addMarker();

                // SVC Screen Line
                svg.querySelectorAll("polyline").forEach((target) => {
                    const newPoint = svg.createSVGPoint();
                    newPoint.x = e.offsetX;
                    newPoint.y = e.offsetY;
                    target.points.appendItem(newPoint);
                })

                if (polyClipVol.markers.length > this.maxPolygonVertices) {
                    cancel.callback();
                }

                this.viewer.inputHandler.startDragging(polyClipVol.markers[polyClipVol.markers.length - 1]);
            } else if (e.button === MOUSE.RIGHT) {
                cancel.callback();
            }
        };

        cancel.callback = () => {
            //let first = svg.find("polyline")[0].points[0];
            //svg.find("polyline").each((index, target) => {
            //	let newPoint = svg[0].createSVGPoint();
            //	newPoint.x = first.x;
            //	newPoint.y = first.y;
            //	let polyline = target.points.appendItem(newPoint);
            //});
            svg.remove();

            if (polyClipVol.markers.length > 3) {
                polyClipVol.removeLastMarker();
                polyClipVol.initialized = true;
            } else {
                this.viewer.scene.removePolygonClipVolume(polyClipVol);
            }

            this.viewer.renderer.domElement.removeEventListener("mouseup", insertionCallback, true);
            this.viewer.removeEventListener(ViewerEventName.CancelInsertions, cancel.callback);
            this.viewer.inputHandler.enabled = true;
        };

        this.viewer.addEventListener(ViewerEventName.CancelInsertions, cancel.callback);
        this.viewer.renderer.domElement.addEventListener("mouseup", insertionCallback, true);
        this.viewer.inputHandler.enabled = false;

        polyClipVol.addMarker();
        this.viewer.inputHandler.startDragging(polyClipVol.markers[polyClipVol.markers.length - 1]);

        return polyClipVol;
    }

    update() {
        //
    }
}
