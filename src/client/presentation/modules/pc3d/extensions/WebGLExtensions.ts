import {WebGLCapabilities} from "three";

export class WebGLExtensions {
    protected _gl: WebGLRenderingContext;
    protected _extensions: any;

    constructor(gl: WebGLRenderingContext) {
        this._gl = gl;
        this._extensions = {};
    }

    protected _getExtension(name: string) {
        if (this._extensions[name] !== undefined) {

            return this._extensions[name];

        }

        let extension;

        switch (name) {
            case 'WEBGL_depth_texture':
                extension = this._gl.getExtension('WEBGL_depth_texture') || this._gl.getExtension('MOZ_WEBGL_depth_texture') || this._gl.getExtension('WEBKIT_WEBGL_depth_texture');

                break;
            case 'EXT_texture_filter_anisotropic':
                extension = this._gl.getExtension('EXT_texture_filter_anisotropic') || this._gl.getExtension('MOZ_EXT_texture_filter_anisotropic') || this._gl.getExtension('WEBKIT_EXT_texture_filter_anisotropic');

                break;
            case 'WEBGL_compressed_texture_s3tc':
                extension = this._gl.getExtension('WEBGL_compressed_texture_s3tc') || this._gl.getExtension('MOZ_WEBGL_compressed_texture_s3tc') || this._gl.getExtension('WEBKIT_WEBGL_compressed_texture_s3tc');

                break;
            case 'WEBGL_compressed_texture_pvrtc':
                extension = this._gl.getExtension('WEBGL_compressed_texture_pvrtc') || this._gl.getExtension('WEBKIT_WEBGL_compressed_texture_pvrtc');

                break;
            default:
                extension = this._gl.getExtension(name);

        }

        this._extensions[name] = extension;

        return extension;

    }

    has(name: string): boolean {
        return this._getExtension(name) !== null;
    }

    init(capabilities: WebGLCapabilities): void {
        if (capabilities.isWebGL2) {
            this._getExtension('EXT_color_buffer_float');
        } else {
            this._getExtension('WEBGL_depth_texture');
            this._getExtension('OES_texture_float');
            this._getExtension('OES_texture_half_float');
            this._getExtension('OES_texture_half_float_linear');
            this._getExtension('OES_standard_derivatives');
            this._getExtension('OES_element_index_uint');
            this._getExtension('OES_vertex_array_object');
            this._getExtension('ANGLE_instanced_arrays');
        }

        this._getExtension('OES_texture_float_linear');
        this._getExtension('EXT_color_buffer_half_float');
    }

    get(name: string): any {
        const extension = this._getExtension(name);

        if (extension === null) {
            console.warn('THREE.WebGLRenderer: ' + name + ' extension not supported.');
        }

        return extension;
    }
}
