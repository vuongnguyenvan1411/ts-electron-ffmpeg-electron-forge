// @ts-ignore
import {OrthographicCamera} from "three";

// declare module 'THREE' {
//     export interface OrthographicCamera {
//         zoomTo: (node: any, factor: any) => void;
//     }
// }

// @ts-ignore
OrthographicCamera.prototype.zoomTo = function (node: any, factor: any = 1) {
    if (!node.geometry && !node.boundingBox) {
        return;
    }

    // TODO

    //let minWS = new THREE.Vector4(node.boundingBox.min.x, node.boundingBox.min.y, node.boundingBox.min.z, 1);
    //let minVS = minWS.applyMatrix4(this.matrixWorldInverse);

    //let right = node.boundingBox.max.x;
    //let bottom	= node.boundingBox.min.y;
    //let top = node.boundingBox.max.y;

    this.updateProjectionMatrix();
};
