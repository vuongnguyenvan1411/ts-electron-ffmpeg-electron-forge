import {PerspectiveCamera, Sphere, Vector3} from "three";

// declare module 'THREE' {
//     export interface PerspectiveCamera {
//         zoomTo: (node: any, factor: any) => void;
//     }
// }

// @ts-ignore
PerspectiveCamera.prototype.zoomTo = function (node: any, factor: any) {
    if (!node.geometry && !node.boundingSphere && !node.boundingBox) {
        return;
    }

    if (node.geometry && node.geometry.boundingSphere === null) {
        node.geometry.computeBoundingSphere();
    }

    node.updateMatrixWorld();

    let bs;

    if (node.boundingSphere) {
        bs = node.boundingSphere;
    } else if (node.geometry && node.geometry.boundingSphere) {
        bs = node.geometry.boundingSphere;
    } else {
        bs = node.boundingBox.getBoundingSphere(new Sphere());
    }

    let _factor = factor || 1;

    bs = bs.clone().applyMatrix4(node.matrixWorld);
    let radius = bs.radius;
    let fovR = this.fov * Math.PI / 180;

    if (this.aspect < 1) {
        fovR = fovR * this.aspect;
    }

    let distanceFactor = Math.abs(radius / Math.sin(fovR / 2)) * _factor;

    let offset = this.getWorldDirection(new Vector3()).multiplyScalar(-distanceFactor);
    this.position.copy(bs.center.clone().add(offset));
};
