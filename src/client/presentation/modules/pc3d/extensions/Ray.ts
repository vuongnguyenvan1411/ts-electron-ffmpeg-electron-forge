import {Ray} from "three";

// declare module 'THREE' {
//     export interface Ray {
//         distanceToPlaneWithNegative: (plane: any) => number;
//     }
// }

// @ts-ignore
Ray.prototype.distanceToPlaneWithNegative = function (plane: any) {
    let denominator = plane.normal.dot(this.direction);
    if (denominator === 0) {
        // line is coplanar, return origin
        if (plane.distanceToPoint(this.origin) === 0) {
            return 0;
        }

        // Null is preferable to undefined since undefined means.... it is undefined
        return null;
    }

    return -(this.origin.dot(plane.normal) + plane.constant) / denominator;
};
