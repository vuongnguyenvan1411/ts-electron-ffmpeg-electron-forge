import {Color, Mesh, MeshNormalMaterial, Object3D, SphereGeometry, Vector2, Vector3} from "three";
import {Line2} from "three/examples/jsm/lines/Line2";
import {LineGeometry} from "three/examples/jsm/lines/LineGeometry";
import {LineMaterial} from "three/examples/jsm/lines/LineMaterial";
import {Utils} from "../core/Utils";
import {App} from "../core/App";

const defaultColors: Record<string, number[]> = {
    "landuse": [0.5, 0.5, 0.5],
    "natural": [0.0, 1.0, 0.0],
    "places": [1.0, 0.0, 1.0],
    "points": [0.0, 1.0, 1.0],
    "roads": [1.0, 1.0, 0.0],
    "waterways": [0.0, 0.0, 1.0],
    "default": [0.9, 0.6, 0.1],
};

function getColor(feature: any) {
    let color = defaultColors[feature];

    if (!color) {
        color = defaultColors["default"];
    }

    return color;
}

export class Geopackage {
    path: any;
    node: any;

    constructor() {
        this.path = null;
        this.node = null;
    }
}

export class GeoPackageLoader {
    // constructor() {
    // }

    static async loadUrl(url: any, params: any) {
        await Promise.all([
            Utils.loadScript(`${App.scriptPath}/lazylibs/geopackage/geopackage.js`),
            Utils.loadScript(`${App.scriptPath}/lazylibs/sql.js/sql-wasm.js`),
        ]);

        const result = await fetch(url);
        const buffer = await result.arrayBuffer();

        params = params || {};

        params.source = url;

        return GeoPackageLoader.loadBuffer(buffer, params);
    }

    static async loadBuffer(buffer: any, params: any) {
        await Promise.all([
            Utils.loadScript(`${App.scriptPath}/lazylibs/geopackage/geopackage.js`),
            Utils.loadScript(`${App.scriptPath}/lazylibs/sql.js/sql-wasm.js`),
        ]);

        params = params || {};

        const resolver = async (resolve: any) => {
            let transform = params.transform;
            if (!transform) {
                transform = {forward: (arg: any) => arg};
            }

            const wasmPath = `${App.scriptPath}/lazylibs/sql.js/sql-wasm.wasm`;
            // @ts-ignore
            const SQL = await initSqlJs({
                locateFile: () => wasmPath
            });

            const u8 = new Uint8Array(buffer);

            // @ts-ignore
            const data = await geopackage.open(u8);
            // @ts-ignore
            window.data = data;

            const geopackageNode = new Object3D();
            geopackageNode.name = params.source;
            // @ts-ignore
            geopackageNode.potree = {
                source: params.source,
            };

            const geo = new Geopackage();
            geo.path = params.source;
            geo.node = geopackageNode;

            const tables = data.getTables();

            for (const table of tables.features) {
                const dao = data.getFeatureDao(table);

                let boundingBox = dao.getBoundingBox();
                boundingBox = boundingBox.projectBoundingBox(dao.projection, 'EPSG:4326');
                const geoJson = data.queryForGeoJSONFeaturesInTable(table, boundingBox);

                const matLine = new LineMaterial({
                    // @ts-ignore
                    color: new Color().setRGB(...getColor(table)).getHex(),
                    linewidth: 2,
                    resolution: new Vector2(1000, 1000),
                    dashed: false
                });

                const node = new Object3D();
                node.name = table;
                geo.node.add(node);

                for (const [, feature] of Object.entries(geoJson)) {
                    // const featureNode = GeoPackageLoader.featureToSceneNode(feature, matLine, transform);
                    const featureNode: any = GeoPackageLoader.featureToSceneNode(feature, matLine, dao.projection, transform);
                    node.add(featureNode);
                }
            }

            resolve(geo);
        }

        return new Promise(resolver);
    }

    static featureToSceneNode(feature: any, matLine: any, geoPackageProjection: any, transform: any) {
        let geometry = feature.geometry;

        // let color = new Color(1, 1, 1);

        if (feature.geometry.type === "Point") {
            let sg = new SphereGeometry(1, 18, 18);
            let sm = new MeshNormalMaterial();
            let s = new Mesh(sg, sm);

            let [long, lat] = geometry.coordinates;
            let pos = transform.forward(geoPackageProjection.forward([long, lat]));

            // @ts-ignore
            s.position.set(...pos, 20);

            s.scale.set(10, 10, 10);

            return s;
        } else if (geometry.type === "LineString") {
            let coordinates = [];

            let min = new Vector3(Infinity, Infinity, Infinity);
            for (let i = 0; i < geometry.coordinates.length; i++) {
                let [long, lat] = geometry.coordinates[i];
                let pos = transform.forward(geoPackageProjection.forward([long, lat]));

                min.x = Math.min(min.x, pos[0]);
                min.y = Math.min(min.y, pos[1]);
                min.z = Math.min(min.z, 20);

                coordinates.push(...pos, 20);
                if (i > 0 && i < geometry.coordinates.length - 1) {
                    coordinates.push(...pos, 20);
                }
            }

            for (let i = 0; i < coordinates.length; i += 3) {
                coordinates[i] -= min.x;
                coordinates[i + 1] -= min.y;
                coordinates[i + 2] -= min.z;
            }

            const lineGeometry = new LineGeometry();
            lineGeometry.setPositions(coordinates);

            const line = new Line2(lineGeometry, matLine);
            line.computeLineDistances();
            line.scale.set(1, 1, 1);
            line.position.copy(min);

            return line;
        } else if (geometry.type === "Polygon" && geometry.coordinates.length > 0) {
            const pc = geometry.coordinates[0];

            let coordinates = [];

            let min = new Vector3(Infinity, Infinity, Infinity);
            for (let i = 0; i < pc.length; i++) {
                let [long, lat] = pc[i];

                let pos = transform.forward(geoPackageProjection.forward([long, lat]));

                min.x = Math.min(min.x, pos[0]);
                min.y = Math.min(min.y, pos[1]);
                min.z = Math.min(min.z, 20);

                coordinates.push(...pos, 20);
                if (i > 0 && i < pc.length - 1) {
                    coordinates.push(...pos, 20);
                }
            }

            for (let i = 0; i < coordinates.length; i += 3) {
                coordinates[i] -= min.x;
                coordinates[i + 1] -= min.y;
                coordinates[i + 2] -= min.z;
            }

            const lineGeometry = new LineGeometry();
            lineGeometry.setPositions(coordinates);

            const line = new Line2(lineGeometry, matLine);
            line.computeLineDistances();
            line.scale.set(1, 1, 1);
            line.position.copy(min);

            return line;
        } else {
            console.log("unhandled feature: ", feature);
        }
    }
}
