import {Box3, Sphere, Vector3} from "three";
import {PointCloudOctreeGeometry, PointCloudOctreeGeometryNode} from "../core/PointCloudOctreeGeometry";
import {Version} from "../core/Version";
import {XHRFactory} from "../core/XHRFactory";
import {LasLazLoader} from "./LasLazLoader";
import {BinaryLoader} from "./BinaryLoader";
import {Utils} from "../core/Utils";
import {PointAttribute, PointAttributes, PointAttributeTypes} from "./PointAttributes";
import {IPOCCloudJs} from "../models/MapModel";
import {TPointCloud} from "../../../../models/service/geodetic/Map3DModel";
import {Utils as RootUtils} from "../../../../core/Utils";

function parseAttributes(cloudJs: IPOCCloudJs) {
    const version = new Version(cloudJs.version);

    const replacements: Record<string, string> = {
        COLOR_PACKED: "rgba",
        RGBA: "rgba",
        INTENSITY: "intensity",
        CLASSIFICATION: "classification",
        GPS_TIME: "gps-time",
    };

    const replaceOldNames = (old: any) => {
        if (replacements[old]) {
            return replacements[old];
        } else {
            return old;
        }
    };

    const pointAttributes: any[] = [];

    if (version.upTo('1.7')) {
        for (let attributeName of cloudJs.pointAttributes) {
            // @ts-ignore
            const oldAttribute = PointAttribute[attributeName];

            const attribute = {
                name: oldAttribute.name,
                size: oldAttribute.byteSize,
                elements: oldAttribute.numElements,
                elementSize: oldAttribute.byteSize / oldAttribute.numElements,
                type: oldAttribute.type.name,
                description: "",
            };

            pointAttributes.push(attribute);
        }
    } else {
        if (typeof cloudJs.pointAttributes === "object") {
            pointAttributes.push(...cloudJs.pointAttributes);
        }
    }

    const attributes = new PointAttributes();

    const typeConversion: Record<string, Record<string, any>> = {
        int8: PointAttributeTypes.DATA_TYPE_INT8,
        int16: PointAttributeTypes.DATA_TYPE_INT16,
        int32: PointAttributeTypes.DATA_TYPE_INT32,
        int64: PointAttributeTypes.DATA_TYPE_INT64,
        uint8: PointAttributeTypes.DATA_TYPE_UINT8,
        uint16: PointAttributeTypes.DATA_TYPE_UINT16,
        uint32: PointAttributeTypes.DATA_TYPE_UINT32,
        uint64: PointAttributeTypes.DATA_TYPE_UINT64,
        double: PointAttributeTypes.DATA_TYPE_DOUBLE,
        float: PointAttributeTypes.DATA_TYPE_FLOAT,
    };

    for (const jsAttribute of pointAttributes) {
        const name = replaceOldNames(jsAttribute.name);
        const type = typeConversion[jsAttribute.type];
        const numElements = jsAttribute.elements;
        // const description = jsAttribute.description;

        const attribute = new PointAttribute(name, type, numElements);

        attributes.add(attribute);
    }

    // check if it has normals
    if (
        pointAttributes.find(a => a.name === "NormalX") !== undefined
        && pointAttributes.find(a => a.name === "NormalY") !== undefined
        && pointAttributes.find(a => a.name === "NormalZ") !== undefined
    ) {
        attributes.addVector({
            name: "NORMAL",
            attributes: ["NormalX", "NormalY", "NormalZ"],
        });
    }

    return attributes;
}

function lasLazAttributes() {
    const attributes = new PointAttributes();

    attributes.add(PointAttribute.POSITION_CARTESIAN);
    attributes.add(new PointAttribute("rgba", PointAttributeTypes.DATA_TYPE_UINT8, 4));
    attributes.add(new PointAttribute("intensity", PointAttributeTypes.DATA_TYPE_UINT16, 1));
    attributes.add(new PointAttribute("classification", PointAttributeTypes.DATA_TYPE_UINT8, 1));
    attributes.add(new PointAttribute("gps-time", PointAttributeTypes.DATA_TYPE_DOUBLE, 1));
    attributes.add(new PointAttribute("number of returns", PointAttributeTypes.DATA_TYPE_UINT8, 1));
    attributes.add(new PointAttribute("return number", PointAttributeTypes.DATA_TYPE_UINT8, 1));
    attributes.add(new PointAttribute("source id", PointAttributeTypes.DATA_TYPE_UINT16, 1));
    //attributes.add(new PointAttribute("pointSourceID", PointAttributeTypes.DATA_TYPE_INT8, 4));

    return attributes;
}

export class POCLoader {
    static load(model: TPointCloud, callback: Function) {
        const url = RootUtils.cdnGdtAsset({
            path: "pc",
            data: {
                i: model.id,
                p: model.type,
                v: model.v
            }
        });

        try {
            const pco = new PointCloudOctreeGeometry();
            pco.url = url;
            const xhr = XHRFactory.createXMLHttpRequest();
            xhr.open('GET', url, true);

            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
                    const fMno: IPOCCloudJs = JSON.parse(xhr.responseText);
                    const version = new Version(fMno.version);

                    // // assume octreeDir is absolute if it starts with http
                    // if (fMno.octreeDir.indexOf('http') === 0) {
                    //     pco.octreeDir = fMno.octreeDir;
                    // } else {
                    //     pco.octreeDir = url + '/../' + fMno.octreeDir;
                    // }

                    pco.octreeDir = fMno.octreeDir;

                    pco.spacing = fMno.spacing;
                    pco.hierarchyStepSize = fMno.hierarchyStepSize;

                    pco.pointAttributes = fMno.pointAttributes as any;

                    const min = new Vector3(fMno.boundingBox.lx, fMno.boundingBox.ly, fMno.boundingBox.lz);
                    const max = new Vector3(fMno.boundingBox.ux, fMno.boundingBox.uy, fMno.boundingBox.uz);
                    const boundingBox = new Box3(min, max);
                    const tightBoundingBox = boundingBox.clone();

                    if (fMno.tightBoundingBox) {
                        tightBoundingBox.min.copy(new Vector3(fMno.tightBoundingBox.lx, fMno.tightBoundingBox.ly, fMno.tightBoundingBox.lz));
                        tightBoundingBox.max.copy(new Vector3(fMno.tightBoundingBox.ux, fMno.tightBoundingBox.uy, fMno.tightBoundingBox.uz));
                    }

                    const offset = min.clone();

                    boundingBox.min.sub(offset);
                    boundingBox.max.sub(offset);

                    tightBoundingBox.min.sub(offset);
                    tightBoundingBox.max.sub(offset);

                    pco.projection = fMno.projection;
                    pco.boundingBox = boundingBox;
                    pco.tightBoundingBox = tightBoundingBox;
                    pco.boundingSphere = boundingBox.getBoundingSphere(new Sphere());
                    pco.tightBoundingSphere = tightBoundingBox.getBoundingSphere(new Sphere());
                    pco.offset = offset;

                    if (fMno.pointAttributes === 'LAS') {
                        pco.loader = new LasLazLoader(fMno.version, "las");
                        pco.loader.setModel(model);
                        pco.pointAttributes = lasLazAttributes();
                    } else if (fMno.pointAttributes === 'LAZ') {
                        pco.loader = new LasLazLoader(fMno.version, "laz");
                        pco.loader.setModel(model);
                        pco.pointAttributes = lasLazAttributes();
                    } else {
                        pco.loader = new BinaryLoader(fMno.version, boundingBox, fMno.scale);
                        pco.loader.setModel(model);
                        pco.pointAttributes = parseAttributes(fMno);
                    }

                    const nodes: any = {};

                    { // load root
                        const name = 'r';
                        const root = new PointCloudOctreeGeometryNode(name, pco, boundingBox);
                        root.setModel(model);
                        root.level = 0;
                        root.hasChildren = true;
                        root.spacing = pco.spacing;
                        if (version.upTo('1.5')) {
                            root.numPoints = fMno.hierarchy[0][1];
                        } else {
                            root.numPoints = 0;
                        }
                        pco.root = root;
                        pco.root.load();
                        nodes[name] = root;
                    }

                    // load remaining hierarchy
                    if (version.upTo('1.4')) {
                        for (let i = 1; i < fMno.hierarchy.length; i++) {
                            const name = fMno.hierarchy[i][0];
                            const numPoints = fMno.hierarchy[i][1];
                            const index = parseInt(name.charAt(name.length - 1));
                            const parentName = name.substring(0, name.length - 1);
                            const parentNode = nodes[parentName] as PointCloudOctreeGeometryNode;
                            const level = name.length - 1;
                            //let boundingBox = POCLoader.createChildAABB(parentNode.boundingBox, index);
                            const boundingBox = Utils.createChildAABB(parentNode.boundingBox, index);

                            const node = new PointCloudOctreeGeometryNode(name, pco, boundingBox);
                            node.setModel(model);
                            node.level = level;
                            node.numPoints = numPoints;
                            node.spacing = pco.spacing / Math.pow(2, level);
                            parentNode.addChild(node);
                            nodes[name] = node;
                        }
                    }

                    pco.nodes = nodes;

                    callback(pco);
                }
            };

            xhr.send(null);
        } catch (e) {
            console.log("Loading failed: '" + url + "'");
            console.log(e);

            callback();
        }
    }

    loadPointAttributes(mno: any) {
        const fpa = mno.pointAttributes;
        const pa = new PointAttributes();

        for (let i = 0; i < fpa.length; i++) {
            // @ts-ignore
            pa.add(PointAttribute[fpa[i]]);
        }

        return pa;
    }

    createChildAABB(aabb: Box3, index: number) {
        const min = aabb.min.clone();
        const max = aabb.max.clone();
        const size = new Vector3().subVectors(max, min);

        if ((index & 0b0001) > 0) {
            min.z += size.z / 2;
        } else {
            max.z -= size.z / 2;
        }

        if ((index & 0b0010) > 0) {
            min.y += size.y / 2;
        } else {
            max.y -= size.y / 2;
        }

        if ((index & 0b0100) > 0) {
            min.x += size.x / 2;
        } else {
            max.x -= size.x / 2;
        }

        return new Box3(min, max);
    }
}

