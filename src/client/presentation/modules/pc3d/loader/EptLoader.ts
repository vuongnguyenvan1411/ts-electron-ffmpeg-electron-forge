import {PointCloudEptGeometry, PointCloudEptGeometryNode} from "../core/PointCloudEptGeometry";

export class EptLoader {
    static async load(file: string, callback: any) {
        let response = await fetch(file);
        let json = await response.json();

        let url = file.substr(0, file.lastIndexOf('ept.json'));
        let geometry = new PointCloudEptGeometry(url, json);
        geometry.root = new PointCloudEptGeometryNode(geometry);
        geometry.root.load();

        callback(geometry);
    }
}

