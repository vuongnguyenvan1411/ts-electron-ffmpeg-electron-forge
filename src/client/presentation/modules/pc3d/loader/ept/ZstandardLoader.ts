import {EptBinaryLoader} from "./BinaryLoader";
import {App} from "../../core/App";

export class EptZstandardLoader extends EptBinaryLoader {
    extension() {
        return '.zst';
    }

    workerPath() {
        return App.getAsset('wks/EptZstandardDecoderWorker.js');
    }
}

