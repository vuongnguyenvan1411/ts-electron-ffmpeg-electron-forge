import {Box3, BufferAttribute, BufferGeometry, Vector3} from "three";
import {XHRFactory} from "../../core/XHRFactory";
import {App} from "../../core/App";

export class EptBinaryLoader {
    extension() {
        return '.bin';
    }

    workerPath() {
        return App.getAsset('wks/EptBinaryDecoderWorker.js');
    }

    load(node: any) {
        if (node.loaded) return;

        let url = node.url() + this.extension();

        let xhr = XHRFactory.createXMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.responseType = 'arraybuffer';
        xhr.overrideMimeType('text/plain; charset=x-user-defined');
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    let buffer = xhr.response;
                    this.parse(node, buffer);
                } else {
                    console.log('Failed ' + url + ': ' + xhr.status);
                }
            }
        };

        try {
            xhr.send(null);
        } catch (e) {
            console.log('Failed request: ' + e);
        }
    }

    parse(node: any, buffer: any) {
        const workerPath = this.workerPath();
        const worker = App.workerPool.getWorker(workerPath);

        if (worker) {
            worker.onmessage = (e: MessageEvent) => {
                let g = new BufferGeometry();
                let numPoints = e.data.numPoints;

                let position = new Float32Array(e.data.position);
                g.setAttribute('position', new BufferAttribute(position, 3));

                let indices = new Uint8Array(e.data.indices);
                g.setAttribute('indices', new BufferAttribute(indices, 4));

                if (e.data.color) {
                    let color = new Uint8Array(e.data.color);
                    g.setAttribute('color', new BufferAttribute(color, 4, true));
                }
                if (e.data.intensity) {
                    let intensity = new Float32Array(e.data.intensity);
                    g.setAttribute('intensity',
                        new BufferAttribute(intensity, 1));
                }
                if (e.data.classification) {
                    let classification = new Uint8Array(e.data.classification);
                    g.setAttribute('classification', new BufferAttribute(classification, 1));
                }
                if (e.data.returnNumber) {
                    let returnNumber = new Uint8Array(e.data.returnNumber);
                    g.setAttribute('return number', new BufferAttribute(returnNumber, 1));
                }
                if (e.data.numberOfReturns) {
                    let numberOfReturns = new Uint8Array(e.data.numberOfReturns);
                    g.setAttribute('number of returns', new BufferAttribute(numberOfReturns, 1));
                }
                if (e.data.pointSourceId) {
                    let pointSourceId = new Uint16Array(e.data.pointSourceId);
                    g.setAttribute('source id', new BufferAttribute(pointSourceId, 1));
                }

                g.attributes.indices.normalized = true;

                const tightBoundingBox = new Box3(
                    new Vector3().fromArray(e.data.tightBoundingBox.min),
                    new Vector3().fromArray(e.data.tightBoundingBox.max)
                );

                node.doneLoading(
                    g,
                    tightBoundingBox,
                    numPoints,
                    new Vector3(...e.data.mean)
                );

                if (worker) App.workerPool.returnWorker(workerPath, worker);
            };

            const toArray = (v: any) => [v.x, v.y, v.z];
            const message = {
                buffer: buffer,
                schema: node.ept.schema,
                scale: node.ept.eptScale,
                offset: node.ept.eptOffset,
                mins: toArray(node.key.b.min)
            };

            worker.postMessage(message, [message.buffer]);
        }
    }
}

