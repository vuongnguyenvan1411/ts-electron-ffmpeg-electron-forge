import {Box3, BufferAttribute, BufferGeometry, Vector3} from "three";
import {XHRFactory} from "../../core/XHRFactory";
import {LASDecoder, LASFile} from "../plasio/LasLaz";
import {App} from "../../core/App";

/**
 * laslaz code taken and adapted from plas.io js-laslaz
 *      http://plas.io/
 *    https://github.com/verma/plasio
 *
 * Thanks to Uday Verma and Howard Butler
 *
 */

export class EptLaszipLoader {
    load(node: any) {
        if (node.loaded) return;

        let url = node.url() + '.laz';

        let xhr = XHRFactory.createXMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.responseType = 'arraybuffer';
        xhr.overrideMimeType('text/plain; charset=x-user-defined');
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    let buffer = xhr.response;
                    this.parse(node, buffer).then();
                } else {
                    console.log('Failed ' + url + ': ' + xhr.status);
                }
            }
        };

        xhr.send(null);
    }

    async parse(node: any, buffer: any) {
        let lf = new LASFile(buffer);
        let handler = new EptLazBatcher(node);

        try {
            await lf.open();

            lf.isOpen = true;

            const header = await lf.getHeader();

            {
                let i = 0;

                let toArray = (v: any) => [v.x, v.y, v.z];
                let mins = toArray(node.key.b.min);
                let maxs = toArray(node.key.b.max);

                let hasMoreData = true;

                while (hasMoreData) {
                    const data = await lf.readData(1000000, 0, 1);

                    let d = new LASDecoder(
                        data.buffer,
                        header.pointsFormatId,
                        header.pointsStructSize,
                        data.count,
                        header.scale,
                        header.offset,
                        mins,
                        maxs);

                    d.extraBytes = header.extraBytes;
                    d.pointsFormatId = header.pointsFormatId;
                    handler.push(d);

                    i += data.count;

                    hasMoreData = data.hasMoreData;
                }

                header.totalRead = i;
                header.versionAsString = lf.versionAsString;
                header.isCompressed = lf.isCompressed;

                await lf.close();

                lf.isOpen = false;
            }

        } catch (err) {
            console.error('Error reading LAZ:', err);

            if (lf.isOpen) {
                await lf.close();

                lf.isOpen = false;
            }

            throw err;
        }
    }
}

export class EptLazBatcher {
    node: any;

    constructor(node: any) {
        this.node = node;
    }

    push(las: any) {
        const workerPath = App.getAsset('wks/EptLaszipDecoderWorker.js');
        const worker = App.workerPool.getWorker(workerPath);

        if (worker) {
            worker.onmessage = (e: MessageEvent) => {
                const g = new BufferGeometry();
                const numPoints = las.pointsCount;

                const positions = new Float32Array(e.data.position);
                const colors = new Uint8Array(e.data.color);

                const intensities = new Float32Array(e.data.intensity);
                const classifications = new Uint8Array(e.data.classification);
                const returnNumbers = new Uint8Array(e.data.returnNumber);
                const numberOfReturns = new Uint8Array(e.data.numberOfReturns);
                const pointSourceIDs = new Uint16Array(e.data.pointSourceID);
                const indices = new Uint8Array(e.data.indices);
                const gpsTime = new Float32Array(e.data.gpsTime);

                g.setAttribute('position', new BufferAttribute(positions, 3));
                g.setAttribute('rgba', new BufferAttribute(colors, 4, true));
                g.setAttribute('intensity', new BufferAttribute(intensities, 1));
                g.setAttribute('classification', new BufferAttribute(classifications, 1));
                g.setAttribute('return number', new BufferAttribute(returnNumbers, 1));
                g.setAttribute('number of returns', new BufferAttribute(numberOfReturns, 1));
                g.setAttribute('source id', new BufferAttribute(pointSourceIDs, 1));
                g.setAttribute('indices', new BufferAttribute(indices, 4));
                g.setAttribute('gpsTime', new BufferAttribute(gpsTime, 1));

                this.node.gpsTime = e.data.gpsMeta;

                g.attributes.indices.normalized = true;

                const tightBoundingBox = new Box3(
                    new Vector3().fromArray(e.data.tightBoundingBox.min),
                    new Vector3().fromArray(e.data.tightBoundingBox.max)
                );

                this.node.doneLoading(
                    g,
                    tightBoundingBox,
                    numPoints,
                    new Vector3(...e.data.mean)
                );

                App.workerPool.returnWorker(workerPath, worker);
            };

            const message = {
                buffer: las.arrayb,
                numPoints: las.pointsCount,
                pointSize: las.pointSize,
                pointFormatID: las.pointsFormatId,
                scale: las.scale,
                offset: las.offset,
                mins: las.mins,
                maxs: las.maxs
            };

            worker.postMessage(message, [message.buffer]);
        }
    };
}

