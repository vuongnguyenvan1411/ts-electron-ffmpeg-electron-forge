import {Box3, BufferAttribute, BufferGeometry, Vector3} from "three";
import {Version} from "../core/Version";
import {XHRFactory} from "../core/XHRFactory";
import {App} from "../core/App";
import {TPointCloud} from "../../../../models/service/geodetic/Map3DModel";
import {LASDecoder, LASFile} from "./plasio/LasLaz";

export class LasLazLoader {
    version: Version;
    extension: string;
    protected model: TPointCloud;

    constructor(version: any, extension: any) {
        if (typeof (version) === 'string') {
            this.version = new Version(version);
        } else {
            this.version = version;
        }

        this.extension = extension;
    }

    setModel(model: TPointCloud) {
        this.model = model;
    }

    getModel(): TPointCloud {
        return this.model
    }

    static progressCB(id: number) {
        console.log('progressCB', id);
    }

    load(node: any) {
        if (node.loaded) {
            return;
        }

        let url = node.getURL();

        if (this.version.equalOrHigher('1.4')) {
            url += `.${this.extension}`;
        }

        const xhr = XHRFactory.createXMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.responseType = 'arraybuffer';
        xhr.overrideMimeType('text/plain; charset=x-user-defined');
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status === 200 || xhr.status === 0) {
                    let buffer = xhr.response;
                    this.parse(node, buffer).then();
                } else {
                    console.log('Failed to load file! HTTP status: ' + xhr.status + ', file: ' + url);
                }
            }
        };

        xhr.send(null);
    }

    async parse(node: any, buffer: any) {
        const lf = new LASFile(buffer);
        const handler = new LasLazBatcher(node);

        try {
            await lf.open();
            lf.isOpen = true;
        } catch (e) {
            console.log("failed to open file. :(");

            return;
        }

        const header = await lf.getHeader();

        const skip = 1;
        let totalRead = 0;
        const totalToRead = (skip <= 1 ? header.pointsCount : header.pointsCount / skip);

        let hasMoreData = true;

        while (hasMoreData) {
            const data = await lf.readData(1000 * 1000, 0, skip);

            handler.push(new LASDecoder(data.buffer,
                header.pointsFormatId,
                header.pointsStructSize,
                data.count,
                header.scale,
                header.offset,
                header.mins, header.maxs
            ));

            totalRead += data.count;
            LasLazLoader.progressCB(totalRead / totalToRead);

            hasMoreData = data.hasMoreData;
        }

        header.totalRead = totalRead;
        header.versionAsString = lf.versionAsString;
        header.isCompressed = lf.isCompressed;

        LasLazLoader.progressCB(1);

        try {
            await lf.close();

            lf.isOpen = false;
        } catch (e) {
            console.error("failed to close las/laz file!!!");

            throw e;
        }
    }

    handle(node: any, url: any) {
        console.log(node, url);
    }
}

export class LasLazBatcher {
    node: any;

    constructor(node: any) {
        this.node = node;
    }

    push(lasBuffer: any) {
        const workerPath = App.getAsset('wks/LASDecoderWorker.js');
        const worker = App.workerPool.getWorker(workerPath);
        const node = this.node;
        const pointAttributes = node.pcoGeometry.pointAttributes;

        if (worker) {
            worker.onmessage = (e: MessageEvent) => {
                const geometry = new BufferGeometry();
                const numPoints = lasBuffer.pointsCount;

                const positions = new Float32Array(e.data.position);
                const colors = new Uint8Array(e.data.color);
                const intensities = new Float32Array(e.data.intensity);
                const classifications = new Uint8Array(e.data.classification);
                const returnNumbers = new Uint8Array(e.data.returnNumber);
                const numberOfReturns = new Uint8Array(e.data.numberOfReturns);
                const pointSourceIDs = new Uint16Array(e.data.pointSourceID);
                const indices = new Uint8Array(e.data.indices);

                geometry.setAttribute('position', new BufferAttribute(positions, 3));
                geometry.setAttribute('color', new BufferAttribute(colors, 4, true));
                geometry.setAttribute('intensity', new BufferAttribute(intensities, 1));
                geometry.setAttribute('classification', new BufferAttribute(classifications, 1));
                geometry.setAttribute('return number', new BufferAttribute(returnNumbers, 1));
                geometry.setAttribute('number of returns', new BufferAttribute(numberOfReturns, 1));
                geometry.setAttribute('source id', new BufferAttribute(pointSourceIDs, 1));
                geometry.setAttribute('indices', new BufferAttribute(indices, 4));
                geometry.attributes.indices.normalized = true;

                for (const key in e.data.ranges) {
                    if (!e.data.ranges.hasOwnProperty(key)) {
                        continue;
                    }

                    const range = e.data.ranges[key];

                    const attribute = pointAttributes.attributes.find((a: any) => a.name === key);
                    attribute.range[0] = Math.min(attribute.range[0], range[0]);
                    attribute.range[1] = Math.max(attribute.range[1], range[1]);
                }

                const tightBoundingBox = new Box3(
                    new Vector3().fromArray(e.data.tightBoundingBox.min),
                    new Vector3().fromArray(e.data.tightBoundingBox.max)
                );

                geometry.boundingBox = this.node.boundingBox;
                this.node.tightBoundingBox = tightBoundingBox;

                this.node.geometry = geometry;
                this.node.numPoints = numPoints;
                this.node.loaded = true;
                this.node.loading = false;
                App.numNodesLoading--;
                this.node.mean = new Vector3(...e.data.mean);

                App.workerPool.returnWorker(workerPath, worker);
            };

            const message = {
                buffer: lasBuffer.arrayb,
                numPoints: lasBuffer.pointsCount,
                pointSize: lasBuffer.pointSize,
                pointFormatID: 2,
                scale: lasBuffer.scale,
                offset: lasBuffer.offset,
                mins: lasBuffer.mins,
                maxs: lasBuffer.maxs
            };

            worker.postMessage(message, [message.buffer]);
        }
    };
}
