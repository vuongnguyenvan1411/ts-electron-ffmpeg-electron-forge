import {App} from "../../core/App";

const pointFormatReaders: any = {
    0: (dv: DataView) => {
        return {
            position: [dv.getInt32(0, true), dv.getInt32(4, true), dv.getInt32(8, true)],
            intensity: dv.getUint16(12, true),
            classification: dv.getUint8(16)
        }
    },
    1: (dv: DataView) => {
        return {
            position: [dv.getInt32(0, true), dv.getInt32(4, true), dv.getInt32(8, true)],
            intensity: dv.getUint16(12, true),
            classification: dv.getUint8(16)
        }
    },
    2: (dv: DataView) => {
        return {
            position: [dv.getInt32(0, true), dv.getInt32(4, true), dv.getInt32(8, true)],
            intensity: dv.getUint16(12, true),
            classification: dv.getUint8(16),
            color: [dv.getUint16(20, true), dv.getUint16(22, true), dv.getUint16(24, true)]
        }
    },
    3: (dv: DataView) => {
        return {
            position: [dv.getInt32(0, true), dv.getInt32(4, true), dv.getInt32(8, true)],
            intensity: dv.getUint16(12, true),
            classification: dv.getUint8(16),
            color: [dv.getUint16(28, true), dv.getUint16(30, true), dv.getUint16(32, true)]
        }
    }
};

function readAs(buf: any, Type: any, offset: any, count?: any) {
    count = (count === undefined || count === 0 ? 1 : count);
    const sub = buf.slice(offset, offset + Type.BYTES_PER_ELEMENT * count);

    const r = new Type(sub);
    if (count === undefined || count === 1)
        return r[0];

    const ret = [];
    for (let i = 0; i < count; i++) {
        // @ts-ignore
        ret.push(r[i]);
    }

    return ret;
}

function parseLASHeader(arrayBuffer: any) {
    const o: any = {};

    o.pointsOffset = readAs(arrayBuffer, Uint32Array, 32 * 3);
    o.pointsFormatId = readAs(arrayBuffer, Uint8Array, 32 * 3 + 8);
    o.pointsStructSize = readAs(arrayBuffer, Uint16Array, 32 * 3 + 8 + 1);
    o.pointsCount = readAs(arrayBuffer, Uint32Array, 32 * 3 + 11);


    let start = 32 * 3 + 35;
    o.scale = readAs(arrayBuffer, Float64Array, start, 3);
    start += 24; // 8*3
    o.offset = readAs(arrayBuffer, Float64Array, start, 3);
    start += 24;


    const bounds = readAs(arrayBuffer, Float64Array, start, 6);
    // start += 48; // 8*6;
    o.maxs = [bounds[0], bounds[2], bounds[4]];
    o.mins = [bounds[1], bounds[3], bounds[5]];

    return o;
}

export class LASLoader {
    arrayBuffer: any;
    readOffset: number;
    header: any;

    constructor(arrayBuffer: any) {
        this.arrayBuffer = arrayBuffer;
    }

    open() {
        this.readOffset = 0;

        return new Promise((res) => {
            setTimeout(res, 0);
        });
    }

    getHeader() {
        return new Promise((res) => {
            setTimeout(() => {
                this.header = parseLASHeader(this.arrayBuffer);
                res(this.header);
            }, 0);
        });
    }

    readData(count: any, offset: any, skip: any) {
        console.log(offset);

        return new Promise((res, rej) => {
            setTimeout(() => {
                if (!this.header)
                    return rej(new Error("Cannot start reading data till a header request is issued"));

                let start;

                if (skip <= 1) {
                    count = Math.min(count, this.header.pointsCount - this.readOffset);
                    start = this.header.pointsOffset + this.readOffset * this.header.pointsStructSize;
                    const end = start + count * this.header.pointsStructSize;
                    res({
                        buffer: this.arrayBuffer.slice(start, end),
                        count: count,
                        hasMoreData: this.readOffset + count < this.header.pointsCount
                    });
                    this.readOffset += count;
                } else {
                    const pointsToRead = Math.min(count * skip, this.header.pointsCount - this.readOffset);
                    const bufferSize = Math.ceil(pointsToRead / skip);
                    let pointsRead = 0;

                    const buf = new Uint8Array(bufferSize * this.header.pointsStructSize);
                    for (let i = 0; i < pointsToRead; i++) {
                        if (i % skip === 0) {
                            start = this.header.pointsOffset + this.readOffset * this.header.pointsStructSize;
                            const src = new Uint8Array(this.arrayBuffer, start, this.header.pointsStructSize);

                            buf.set(src, pointsRead * this.header.pointsStructSize);
                            pointsRead++;
                        }

                        this.readOffset++;
                    }

                    res({
                        buffer: buf.buffer,
                        count: pointsRead,
                        hasMoreData: this.readOffset < this.header.pointsCount
                    });
                }
            }, 0);
        });
    }

    close() {
        return new Promise((res) => {
            this.arrayBuffer = null;
            setTimeout(res, 0);
        });
    }
}

export class LAZLoader {
    arrayBuffer: any;
    ww: Worker;
    nextCB: any;
    dorR: any;

    constructor(arrayBuffer: any) {
        this.arrayBuffer = arrayBuffer;

        const workerPath = App.getAsset('wks/LASLAZWorker.js');
        this.ww = App.workerPool.getWorker(workerPath)!;

        this.nextCB = null;

        this.ww.onmessage = (e) => {
            if (this.nextCB !== null) {
                this.nextCB(e.data);
                this.nextCB = null;
            }
        };

        this.dorR = (req: any, cb: any) => {
            this.nextCB = cb;
            this.ww.postMessage(req);
        };
    }

    open() {
        return new Promise((res, rej) => {
            this.dorR({type: "open", arraybuffer: this.arrayBuffer}, (r: any) => {
                if (r.status !== 1) {
                    return rej(new Error("Failed to open file"));
                }

                res(true);
            })
        })
    }

    getHeader() {
        return new Promise((res, rej) => {
            this.dorR({type: 'header'}, (r: any) => {
                if (r.status !== 1) {
                    return rej(new Error("Failed to get header"))
                }

                res(r.header);
            })
        });
    }

    readData(count: any, offset: any, skip: any) {
        return new Promise((res, rej) => {
            this.dorR({type: 'read', count: count, offset: offset, skip: skip}, (r: any) => {
                if (r.status !== 1) {
                    return rej(new Error("Failed to read data"))
                }

                res({
                    buffer: r.buffer,
                    count: r.count,
                    hasMoreData: r.hasMoreData
                });
            });
        });
    }

    close() {
        return new Promise((res, rej) => {
            this.dorR({type: 'close'}, (r: any) => {
                const workerPath = App.getAsset('wks/LASLAZWorker.js');
                App.workerPool.returnWorker(workerPath, this.ww);

                if (r.status !== 1) {
                    return rej(new Error("Failed to close file"))
                }

                res(true);
            });
        });
    }
}

export class LASFile {
    arrayBuffer: any;
    loader: any;
    formatId: number;
    version: number;
    isCompressed: boolean;
    versionAsString: string;
    isOpen: boolean;

    constructor(arrayBuffer: any) {
        this.arrayBuffer = arrayBuffer;

        this.determineVersion();
        if (this.version > 12) {
            throw new Error("Only file versions <= 1.2 are supported at this time")
        }

        this.determineFormat();
        if (pointFormatReaders[this.formatId] === undefined)
            throw new Error("The point format ID is not supported");

        this.loader = this.isCompressed
            ? new LAZLoader(this.arrayBuffer)
            : new LASLoader(this.arrayBuffer);
    }

    determineFormat() {
        const formatId = readAs(this.arrayBuffer, Uint8Array, 32 * 3 + 8);
        const bit_7 = (formatId & 0x80) >> 7;
        const bit_6 = (formatId & 0x40) >> 6;

        if (bit_7 === 1 && bit_6 === 1)
            throw new Error("Old style compression not supported");

        this.formatId = formatId & 0x3f;
        this.isCompressed = (bit_7 === 1 || bit_6 === 1);
    }

    determineVersion() {
        const ver = new Int8Array(this.arrayBuffer, 24, 2);
        this.version = ver[0] * 10 + ver[1];
        this.versionAsString = ver[0] + "." + ver[1];
    }

    open = () => this.loader.open;
    getHeader = () => this.loader.getHeader();
    readData = (count: any, start: any, skip: any) => this.loader.readData(count, start, skip);
    close = () => this.loader.close();
}

export class LASDecoder {
    arrayB: any;
    decoder: any;
    pointsCount: any;
    pointSize: any;
    scale: any;
    offset: any;
    mins: any;
    maxs: any;
    extraBytes: any;
    pointsFormatId: any;

    constructor(buffer: any, pointFormatID: any, pointSize: any, pointsCount: any, scale: any, offset: any, mins: any, maxs: any) {
        this.arrayB = buffer;
        this.decoder = pointFormatReaders[pointFormatID];
        this.pointsCount = pointsCount;
        this.pointSize = pointSize;
        this.scale = scale;
        this.offset = offset;
        this.mins = mins;
        this.maxs = maxs;
    }

    getPoint(index: number) {
        if (index < 0 || index >= this.pointsCount)
            throw new Error("Point index out of range");

        const dv = new DataView(this.arrayB, index * this.pointSize, this.pointSize);

        return this.decoder(dv);
    }


}
