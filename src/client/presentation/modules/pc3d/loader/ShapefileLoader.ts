import {Mesh, MeshNormalMaterial, Object3D, SphereGeometry, Vector2, Vector3} from "three";
import {Line2} from "three/examples/jsm/lines/Line2";
import {LineGeometry} from "three/examples/jsm/lines/LineGeometry";
import {LineMaterial} from "three/examples/jsm/lines/LineMaterial";
import shapefile from "shapefile";

export class ShapefileLoader {
    transform: any;

    constructor() {
        this.transform = null;
    }

    async load(path: any) {
        const matLine = new LineMaterial({
            color: 0xff0000,
            linewidth: 3, // in pixels
            resolution: new Vector2(1000, 1000),
            dashed: false
        });

        const features = await this.loadShapefileFeatures(path);
        const node = new Object3D();

        for (const feature of features) {
            const fNode: any = this.featureToSceneNode(feature, matLine);

            node.add(fNode);
        }

        const setResolution = (x: any, y: any) => {
            matLine.resolution.set(x, y);
        };

        return {
            features: features,
            node: node,
            setResolution: setResolution,
        };
    }

    featureToSceneNode(feature: any, matLine: any) {
        const geometry = feature.geometry;
        // let color = new Color(1, 1, 1);

        let transform = this.transform;
        if (transform === null) {
            transform = {forward: (v: any) => v};
        }

        if (feature.geometry.type === "Point") {
            const sg = new SphereGeometry(1, 18, 18);
            const sm = new MeshNormalMaterial();
            const s = new Mesh(sg, sm);

            const [long, lat] = geometry.coordinates;
            const pos = transform.forward([long, lat]);

            // @ts-ignore
            s.position.set(...pos, 20);

            s.scale.set(10, 10, 10);

            return s;
        } else if (geometry.type === "LineString") {
            const coordinates = [];

            const min = new Vector3(Infinity, Infinity, Infinity);

            for (let i = 0; i < geometry.coordinates.length; i++) {
                const [long, lat] = geometry.coordinates[i];
                const pos = transform.forward([long, lat]);

                min.x = Math.min(min.x, pos[0]);
                min.y = Math.min(min.y, pos[1]);
                min.z = Math.min(min.z, 20);

                coordinates.push(...pos, 20);
                if (i > 0 && i < geometry.coordinates.length - 1) {
                    coordinates.push(...pos, 20);
                }
            }

            for (let i = 0; i < coordinates.length; i += 3) {
                coordinates[i] -= min.x;
                coordinates[i + 1] -= min.y;
                coordinates[i + 2] -= min.z;
            }

            const lineGeometry = new LineGeometry();
            lineGeometry.setPositions(coordinates);

            const line = new Line2(lineGeometry, matLine);
            line.computeLineDistances();
            line.scale.set(1, 1, 1);
            line.position.copy(min);

            return line;
        } else if (geometry.type === "Polygon" && geometry.coordinates.length > 0) {
            const pc = geometry.coordinates[0];

            const coordinates = [];

            const min = new Vector3(Infinity, Infinity, Infinity);
            for (let i = 0; i < pc.length; i++) {
                const [long, lat] = pc[i];
                const pos = transform.forward([long, lat]);

                min.x = Math.min(min.x, pos[0]);
                min.y = Math.min(min.y, pos[1]);
                min.z = Math.min(min.z, 20);

                coordinates.push(...pos, 20);
                if (i > 0 && i < pc.length - 1) {
                    coordinates.push(...pos, 20);
                }
            }

            for (let i = 0; i < coordinates.length; i += 3) {
                coordinates[i] -= min.x;
                coordinates[i + 1] -= min.y;
                coordinates[i + 2] -= min.z;
            }

            const lineGeometry = new LineGeometry();
            lineGeometry.setPositions(coordinates);

            const line = new Line2(lineGeometry, matLine);
            line.computeLineDistances();
            line.scale.set(1, 1, 1);
            line.position.copy(min);

            return line;
        } else {
            console.log("unhandled feature: ", feature);
        }
    }

    async loadShapefileFeatures(file: any) {
        const features = [];

        const source = await shapefile.open(file);

        while (true) {
            const result = await source.read();

            if (result.done) {
                break;
            }

            if (result.value && result.value.type === 'Feature' && result.value.geometry !== undefined) {
                features.push(result.value);
            }
        }

        return features;
    }
}

