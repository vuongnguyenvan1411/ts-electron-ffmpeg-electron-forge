import {Box3, BufferAttribute, BufferGeometry, Vector3} from "three";
import {Version} from "../core/Version";
import {XHRFactory} from "../core/XHRFactory";
import {App} from "../core/App";
import {PointCloudOctreeGeometryNode} from "../core/PointCloudOctreeGeometry";
import {PointAttribute} from "./PointAttributes";
import {Utils as RootUtils} from "../../../../core/Utils";
import {TPointCloud} from "../../../../models/service/geodetic/Map3DModel";

export class BinaryLoader {
    version: Version;
    boundingBox: Box3;
    scale: number;
    protected model: TPointCloud;

    constructor(version: Version | string, boundingBox: Box3, scale: number) {
        if (version instanceof Version) {
            this.version = version;
        } else {
            this.version = new Version(version);
        }

        this.boundingBox = boundingBox;
        this.scale = scale;
    }

    setModel(model: TPointCloud) {
        this.model = model;
    }

    getModel(): TPointCloud {
        return this.model
    }

    load = (node: PointCloudOctreeGeometryNode) => {
        if (node.loaded) {
            return;
        }

        let url: string;

        const model = this.getModel()

        if (model) {
            url = RootUtils.cdnGdtAsset({
                path: "pc",
                data: {
                    i: model.id,
                    p: `${node.getURL()}.bin`,
                    v: model.v
                }
            });

            console.log('Load bin:', `${node.getURL()}.bin`);
        } else {
            url = node.getURL();

            if (this.version.equalOrHigher('1.4')) {
                url += '.bin';
            }
        }

        const xhr = XHRFactory.createXMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.responseType = 'arraybuffer';
        xhr.overrideMimeType('text/plain; charset=x-user-defined');
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if ((xhr.status === 200 || xhr.status === 0) && xhr.response !== null) {
                    let buffer = xhr.response;
                    this.parse(node, buffer);
                } else {
                    //console.error(`Failed to load file! HTTP status: ${xhr.status}, file: ${url}`);
                    throw new Error(`Failed to load file! HTTP status: ${xhr.status}, file: ${url}`);
                }
            }
        };

        try {
            xhr.send(null);
        } catch (e) {
            console.log('error loading point cloud: ' + e);
        }
    };

    parse(node: PointCloudOctreeGeometryNode, buffer: ArrayBuffer) {
        const pointAttributes = node.pcoGeometry.pointAttributes;
        const numPoints = buffer.byteLength / node.pcoGeometry.pointAttributes.byteSize;

        if (this.version.upTo('1.5')) {
            node.numPoints = numPoints;
        }

        const workerPath = App.getAsset('wks/BinaryDecoderWorker.min.js');
        const worker = App.workerPool.getWorker(workerPath);

        if (worker) {
            worker.onmessage = (e: MessageEvent) => {
                const data = e.data;

                const buffers = data.attributeBuffers;
                const tightBoundingBox = new Box3(
                    new Vector3().fromArray(data.tightBoundingBox.min),
                    new Vector3().fromArray(data.tightBoundingBox.max)
                );

                App.workerPool.returnWorker(workerPath, worker);

                const geometry = new BufferGeometry();

                for (let property in buffers) {
                    if (!buffers.hasOwnProperty(property)) {
                        continue;
                    }

                    const buffer = buffers[property].buffer;
                    const batchAttribute = buffers[property].attribute;

                    if (property === "POSITION_CARTESIAN") {
                        geometry.setAttribute('position', new BufferAttribute(new Float32Array(buffer), 3));
                    } else if (property === "rgba") {
                        geometry.setAttribute("rgba", new BufferAttribute(new Uint8Array(buffer), 4, true));
                    } else if (property === "NORMAL_SPHEREMAPPED") {
                        geometry.setAttribute('normal', new BufferAttribute(new Float32Array(buffer), 3));
                    } else if (property === "NORMAL_OCT16") {
                        geometry.setAttribute('normal', new BufferAttribute(new Float32Array(buffer), 3));
                    } else if (property === "NORMAL") {
                        geometry.setAttribute('normal', new BufferAttribute(new Float32Array(buffer), 3));
                    } else if (property === "INDICES") {
                        let bufferAttribute = new BufferAttribute(new Uint8Array(buffer), 4);
                        bufferAttribute.normalized = true;
                        geometry.setAttribute('indices', bufferAttribute);
                    } else if (property === "SPACING") {
                        let bufferAttribute = new BufferAttribute(new Float32Array(buffer), 1);
                        geometry.setAttribute('spacing', bufferAttribute);
                    } else {
                        const bufferAttribute = new BufferAttribute(new Float32Array(buffer), 1);

                        // @ts-ignore
                        bufferAttribute.potree = {
                            offset: buffers[property].offset,
                            scale: buffers[property].scale,
                            preciseBuffer: buffers[property].preciseBuffer,
                            range: batchAttribute.range,
                        };

                        geometry.setAttribute(property, bufferAttribute);

                        const attribute = pointAttributes.attributes.find((a: PointAttribute) => a.name === batchAttribute.name);

                        if (attribute) {
                            attribute.range[0] = Math.min(attribute.range[0], batchAttribute.range[0]);
                            attribute.range[1] = Math.max(attribute.range[1], batchAttribute.range[1]);
                        }

                        if (node.getLevel() === 0 && attribute) {
                            attribute.initialRange = batchAttribute.range;
                        }
                    }
                }

                tightBoundingBox.max.sub(tightBoundingBox.min);
                tightBoundingBox.min.set(0, 0, 0);

                node.numPoints = e.data.buffer.byteLength / pointAttributes.byteSize;
                node.geometry = geometry;
                node.mean = new Vector3(...data.mean);
                node.tightBoundingBox = tightBoundingBox;
                node.loaded = true;
                node.loading = false;
                node.estimatedSpacing = data.estimatedSpacing;

                App.numNodesLoading--;
            }

            const message = {
                buffer: buffer,
                pointAttributes: pointAttributes,
                version: this.version.version,
                min: [node.boundingBox.min.x, node.boundingBox.min.y, node.boundingBox.min.z],
                offset: [node.pcoGeometry.offset.x, node.pcoGeometry.offset.y, node.pcoGeometry.offset.z],
                scale: this.scale,
                spacing: node.spacing,
                hasChildren: node.hasChildren,
                name: node.name
            };

            worker.postMessage(message, [message.buffer]);
        }
    }
}

