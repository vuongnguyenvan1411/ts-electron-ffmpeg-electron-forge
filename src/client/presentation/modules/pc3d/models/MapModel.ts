export interface IPOCCloudJs {
    version: string;
    octreeDir: string;
    projection: string;
    points: number;
    boundingBox: {
        lx: number;
        ly: number;
        lz: number;
        ux: number;
        uy: number;
        uz: number;
    };
    tightBoundingBox: {
        lx: number;
        ly: number;
        lz: number;
        ux: number;
        uy: number;
        uz: number;
    };
    pointAttributes: {
        name: string;
        size: number;
        elements: number;
        elementSize: number;
        type: string;
        description: string;
    }[] | string;
    spacing: number;
    scale: number;
    hierarchyStepSize: number;
    hierarchy: any;
}

export interface PointCloudModel {
    url: string;
    name: string;
    v?: string;
}

export interface TextureMeshModel {
    id: number;
    url: string;
    name: string;
    object: string;
    texture: string;
    offset: string;
    v?: string;
}
