import React, {useCallback, useEffect, useRef, useState} from "react";
import {Viewer} from "../viewer/viewer";
import IconCompass from '../../../../assets/image/v3d/compas.svg';
import {ViewerEventName} from "../core/Event";
import {Vector3} from "three";
import {Utils} from "../core/Utils";
import Image from "next/image";

export const CompassFC = React.memo((props: {
    viewer: Viewer,
}) => {
    const [azimuth, setAzimuth] = useState<number>(0);

    const divMasterLayerHeader = document.getElementById('MasterLayerHeader');
    const headerHeightRef = useRef<number>(divMasterLayerHeader ? divMasterLayerHeader.offsetHeight : 0);

    useEffect(() => {
        const onViewerUpdate = () => {
            const direction = props.viewer.scene.view.direction.clone();
            direction.z = 0;
            direction.normalize();

            const camera = props.viewer.scene.getActiveCamera();

            const p1 = camera.getWorldPosition(new Vector3());
            const p2 = p1.clone().add(direction);

            const projection = props.viewer.getProjection();

            if (projection) {
                setAzimuth(Utils.computeAzimuth(p1, p2, projection));
            }
        }

        props.viewer.addEventListener(ViewerEventName.Update, onViewerUpdate);

        return () => {
            props.viewer.removeEventListener(ViewerEventName.Update, onViewerUpdate);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onClick = useCallback(() => props.viewer.setTopView(), [props.viewer])

    return (
        <span
            style={{
                transform: `rotateZ(${-azimuth}rad)`,
                position: "absolute",
                top: `calc(${headerHeightRef.current}px + 0.25rem)`,
                right: "calc(64px + 1rem)",
                zIndex: 1000,
                width: "64px",
                cursor: "pointer"
            }}
            onClick={onClick}
        >
            <Image
                src={IconCompass}
                alt={"compass"}
            />
        </span>
    )
});
