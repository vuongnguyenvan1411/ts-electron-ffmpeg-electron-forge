import React, {useEffect, useRef, useState} from "react";
import {Viewer} from "../viewer/viewer";
import sms from "../styles/Viewer.module.scss";
import scm from "../../../../styles/module/Common.module.scss";
import {Button, Menu, notification, Popover, Space, Tooltip} from "antd";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {falArrowsAlt, falMinus, falPlaneAlt, falPlus, farCameraMovie, farCommentsAlt, farHelicopter, farPaperclip, farPlanetRinged, farRulerTriangle, farShareSquare, farTimes, fasCompressArrowsAlt, fasEye} from "../../../../const/FontAwesome";
import {useTranslation} from "react-i18next";
import {EViewerControl, EViewerTool, ViewType} from "../core/Defines";
import {OrbitControls} from "../navigation/OrbitControls";
import {EarthControls} from "../navigation/EarthControls";
import {FirstPersonControls} from "../navigation/FirstPersonControls";
import IconViewLeft from "../../../../assets/image/v3d/view_left.svg";
import IconViewRight from "../../../../assets/image/v3d/view_right.svg";
import IconViewFront from "../../../../assets/image/v3d/view_front.svg";
import IconViewBack from "../../../../assets/image/v3d/view_back.svg";
import IconViewTop from "../../../../assets/image/v3d/view_top.svg";
import {CameraAnimation} from "../modules/CameraAnimation/CameraAnimation";
import {SphereVolume} from "../utils/Volume";
import IconMeasureAngle from "../../../../assets/image/v3d/measure_angle.png";
import IconPoint from "../../../../assets/image/v3d/point.svg";
import IconMeasureDistance from "../../../../assets/image/v3d/measure_distance.svg";
import IconMeasureHeight from "../../../../assets/image/v3d/measure_height.svg";
import IconMeasureCircle from "../../../../assets/image/v3d/measure_circle.svg";
import IconAzimuth from "../../../../assets/image/v3d/azimuth.svg";
import IconMeasureArea from "../../../../assets/image/v3d/measure_area.svg";
import IconMeasureVolume from "../../../../assets/image/v3d/measure_volume.svg";
import IconSphereVolume from "../../../../assets/image/v3d/sphere_volume.svg";
import IconProfileHeight from "../../../../assets/image/v3d/profile_height.svg";
import IconClipVolume from "../../../../assets/image/v3d/clip_volume.svg";
import IconClipPolygon from "../../../../assets/image/v3d/clip_polygon.svg";
import IconClipScreen from "../../../../assets/image/v3d/clip_screen.svg";
import {ScreenBoxSelectTool} from "../utils/ScreenBoxSelectTool";
import {Map3DShareModalFC} from "./Map3DShareModalFC";
import {Map3DModel} from "../../../../models/service/geodetic/Map3DModel";
import Image from "next/image";
import {FeatureSingleton} from "../../../../models/FeatureSingleton";

export const MapToolFC = React.memo((props: {
    viewer: Viewer,
    item: Map3DModel
}) => {
    const {t} = useTranslation();

    const measuringTool = props.viewer.measuringTool;
    const volumeTool = props.viewer.volumeTool;
    const profileTool = props.viewer.profileTool;
    const annotationTool = props.viewer.annotationTool;
    const clippingTool = props.viewer.clippingTool;

    const divMasterLayerHeader = document.getElementById('MasterLayerHeader');
    const headerHeightRef = useRef<number>(divMasterLayerHeader ? divMasterLayerHeader.offsetHeight : 0);

    const [activeControlBtn, setActiveControlBtn] = useState<EViewerControl>();
    const [ppMeasureVisible, setPpMeasureVisible] = useState<boolean>(false);

    const [ppClipVisible, setPpClipVisible] = useState<boolean>(false);

    const [isShareModalVisible, setIsShareModalVisible] = useState(false);

    useEffect(() => {
        const control = props.viewer.getControls();

        if (control instanceof EarthControls) {
            setActiveControlBtn(EViewerControl.Earth);
        } else if (control instanceof OrbitControls) {
            setActiveControlBtn(EViewerControl.Orbit);
        } else if (control instanceof FirstPersonControls) {
            if (props.viewer.fpControls.lockElevation) {
                setActiveControlBtn(EViewerControl.Heli);
            } else {
                setActiveControlBtn(EViewerControl.Flight);
            }
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onZoomIn = () => {
        const control = props.viewer.getControls();

        if (control && typeof control['zoom'] === 'function') {
            control.zoom(0.1);
        }
    }

    const onZoomOut = () => {
        const control = props.viewer.getControls();

        if (control && typeof control['zoom'] === 'function') {
            control.zoom(-0.1);
        }
    }

    return (
        <div
            className={sms.Tool}
            style={{
                top: `calc(${headerHeightRef.current}px + 1rem + ${headerHeightRef.current}px + 1rem)`
            }}
        >
            <Space
                direction={'vertical'}
                size={10}
                align={'center'}
            >
                <Space
                    className={sms.SpaceGroup}
                    direction={'vertical'}
                    size={0}
                >
                    <Tooltip title={t('text.zoomIn')} placement="left">
                        <Button
                            className={sms.Button}
                            icon={<FontAwesomeIcon icon={falPlus} className={sms.Icon}/>}
                            onClick={onZoomIn}
                        />
                    </Tooltip>
                    <Tooltip title={t('text.zoomOut')} placement="left">
                        <Button
                            className={sms.Button}
                            icon={<FontAwesomeIcon icon={falMinus} className={sms.Icon}/>}
                            onClick={onZoomOut}
                        />
                    </Tooltip>
                </Space>

                <Space
                    className={sms.SpaceGroup}
                    direction={'vertical'}
                    size={0}
                >
                    <Tooltip title={t('v3d.text.earthControl')} placement="left">
                        <Button
                            className={`${sms.Button} ${activeControlBtn === EViewerControl.Earth ? sms.Active : ''}`}
                            icon={<FontAwesomeIcon icon={falArrowsAlt} className={sms.Icon}/>}
                            onClick={() => {
                                setActiveControlBtn(EViewerControl.Earth);

                                props.viewer.setControls(props.viewer.earthControls);
                            }}
                        />
                    </Tooltip>
                    <Tooltip title={t('v3d.text.orbitControl')} placement="left">
                        <Button
                            className={`${sms.Button} ${activeControlBtn === EViewerControl.Orbit ? sms.Active : ''}`}
                            icon={<FontAwesomeIcon icon={farPlanetRinged} className={sms.Icon}/>}
                            onClick={() => {
                                setActiveControlBtn(EViewerControl.Orbit);

                                props.viewer.setControls(props.viewer.orbitControls);
                            }}
                        />
                    </Tooltip>
                    <Tooltip title={t('v3d.text.flightControl')} placement="left">
                        <Button
                            className={`${sms.Button} ${activeControlBtn === EViewerControl.Flight ? sms.Active : ''}`}
                            icon={<FontAwesomeIcon icon={falPlaneAlt} className={sms.Icon}/>}
                            onClick={() => {
                                setActiveControlBtn(EViewerControl.Flight);

                                props.viewer.setControls(props.viewer.fpControls);
                                props.viewer.fpControls.lockElevation = false;
                            }}
                        />
                    </Tooltip>
                    <Tooltip title={t('v3d.text.heliControl')} placement="left">
                        <Button
                            className={`${sms.Button} ${activeControlBtn === EViewerControl.Heli ? sms.Active : ''}`}
                            icon={<FontAwesomeIcon icon={farHelicopter} className={sms.Icon}/>}
                            onClick={() => {
                                setActiveControlBtn(EViewerControl.Heli);

                                props.viewer.setControls(props.viewer.fpControls);
                                props.viewer.fpControls.lockElevation = true;
                            }}
                        />
                    </Tooltip>
                    <Tooltip title={t('v3d.text.focusControl')} placement="left">
                        <Button
                            className={sms.Button}
                            icon={<FontAwesomeIcon icon={fasCompressArrowsAlt} className={sms.Icon}/>}
                            onClick={() => {
                                props.viewer.fitToScreen();
                            }}
                        />
                    </Tooltip>
                    <Popover
                        overlayClassName={scm.PopoverMenu}
                        placement="left"
                        title={t('v3d.text.viewControl')}
                        trigger="click"
                        content={
                            <Menu
                                className={'border-0'}
                                onClick={({key}) => {
                                    switch (key) {
                                        case ViewType.Left:
                                            props.viewer.setLeftView();

                                            break;
                                        case ViewType.Right:
                                            props.viewer.setRightView();

                                            break;
                                        case ViewType.Front:
                                            props.viewer.setFrontView();

                                            break;
                                        case ViewType.Back:
                                            props.viewer.setBackView();

                                            break;
                                        case ViewType.Top:
                                            props.viewer.setTopView();

                                            break;
                                        case ViewType.Bottom:
                                            props.viewer.setBottomView();

                                            break;
                                    }
                                }}
                            >
                                <Menu.Item key={ViewType.Left}>
                                    <Image alt={""} src={IconViewLeft} className={"h-6 mr-1"}/>
                                    {t('v3d.text.leftViewControl')}
                                </Menu.Item>
                                <Menu.Item key={ViewType.Right}>
                                    <Image alt={""} src={IconViewRight} className={"h-6 mr-1"}/>
                                    {t('v3d.text.rightViewControl')}
                                </Menu.Item>
                                <Menu.Item key={ViewType.Front}>
                                    <Image alt={""} src={IconViewFront} className={"h-6 mr-1"}/>
                                    {t('v3d.text.frontViewControl')}
                                </Menu.Item>
                                <Menu.Item key={ViewType.Back}>
                                    <Image alt={""} src={IconViewBack} className={"h-6 mr-1"}/>
                                    {t('v3d.text.backViewControl')}
                                </Menu.Item>
                                <Menu.Item key={ViewType.Top}>
                                    <Image alt={""} src={IconViewTop} className={"h-6 mr-1"}/>
                                    {t('v3d.text.topViewControl')}
                                </Menu.Item>
                                <Menu.Item key={ViewType.Bottom}>
                                    <Image alt={""} src={IconViewBack} className={"h-6 mr-1"}/>
                                    {t('v3d.text.bottomViewControl')}
                                </Menu.Item>
                            </Menu>
                        }
                    >
                        <Button
                            className={sms.Button}
                            icon={<FontAwesomeIcon icon={fasEye} className={sms.Icon}/>}
                        />
                    </Popover>
                    <Tooltip title={t('v3d.text.cameraAnimation')} placement="left">
                        <Button
                            className={sms.Button}
                            icon={<FontAwesomeIcon icon={farCameraMovie} className={sms.Icon}/>}
                            onClick={() => {
                                const animation = CameraAnimation.defaultFromView(props.viewer);

                                animation.name = t('v3d.text.cameraAnimation');
                                animation.setProperties({
                                    type: EViewerTool.Animation
                                })

                                props.viewer.scene.addCameraAnimation(animation);
                            }}
                        />
                    </Tooltip>
                </Space>

                <Space
                    className={sms.SpaceGroup}
                    direction={'vertical'}
                    size={0}
                >
                    <Popover
                        overlayClassName={scm.PopoverMenu}
                        placement="left"
                        title={t('v3d.text.measurements')}
                        trigger="click"
                        content={
                            <Menu
                                className={'border-0'}
                                onClick={({key}) => {
                                    setPpMeasureVisible(false);

                                    switch (key) {
                                        case EViewerTool.MeasureAngle: {
                                            measuringTool.startInsertion({
                                                showDistances: false,
                                                showAngles: true,
                                                showArea: false,
                                                closed: true,
                                                maxMarkers: 3,
                                                name: t('v3d.text.angleMeasurement'),
                                                properties: {
                                                    type: key
                                                }
                                            });

                                            break;
                                        }
                                        case EViewerTool.Point: {
                                            measuringTool.startInsertion({
                                                showDistances: false,
                                                showAngles: false,
                                                showCoordinates: true,
                                                showArea: false,
                                                closed: true,
                                                maxMarkers: 1,
                                                name: t('v3d.text.pointMeasurement'),
                                                properties: {
                                                    type: key
                                                }
                                            });

                                            break;
                                        }
                                        case EViewerTool.MeasureDistance: {
                                            measuringTool.startInsertion({
                                                showDistances: true,
                                                showArea: false,
                                                closed: false,
                                                name: t('v3d.text.distanceMeasurement'),
                                                properties: {
                                                    type: key
                                                }
                                            });

                                            break;
                                        }
                                        case EViewerTool.MeasureHeight: {
                                            measuringTool.startInsertion({
                                                showDistances: false,
                                                showHeight: true,
                                                showArea: false,
                                                closed: false,
                                                maxMarkers: 2,
                                                name: t('v3d.text.heightMeasurement'),
                                                properties: {
                                                    type: key
                                                }
                                            });

                                            break;
                                        }
                                        case EViewerTool.MeasureCircle: {
                                            measuringTool.startInsertion({
                                                showDistances: false,
                                                showHeight: false,
                                                showArea: false,
                                                showCircle: true,
                                                showEdges: false,
                                                closed: false,
                                                maxMarkers: 3,
                                                name: t('v3d.text.circleMeasurement'),
                                                properties: {
                                                    type: key
                                                }
                                            });

                                            break;
                                        }
                                        case EViewerTool.MeasureAzimuth: {
                                            measuringTool.startInsertion({
                                                showDistances: false,
                                                showHeight: false,
                                                showArea: false,
                                                showCircle: false,
                                                showEdges: false,
                                                showAzimuth: true,
                                                closed: false,
                                                maxMarkers: 2,
                                                name: t('v3d.text.azimuth'),
                                                properties: {
                                                    type: key
                                                }
                                            });

                                            break;
                                        }
                                        case EViewerTool.MeasureArea: {
                                            measuringTool.startInsertion({
                                                showDistances: true,
                                                showArea: true,
                                                closed: true,
                                                name: t('v3d.text.areaMeasurement'),
                                                properties: {
                                                    type: key
                                                }
                                            });

                                            break;
                                        }
                                        case EViewerTool.MeasureBoxVolume: {
                                            volumeTool.startInsertion({
                                                name: t('v3d.text.volumeMeasurement'),
                                                properties: {
                                                    type: key
                                                }
                                            });

                                            // props.viewer.inputHandler.deselectAll();
                                            // props.viewer.inputHandler.toggleSelection(measurement);

                                            break;
                                        }
                                        case EViewerTool.MeasureSphereVolume: {
                                            volumeTool.startInsertion({
                                                type: SphereVolume,
                                                name: t('v3d.text.sphereVolume'),
                                                properties: {
                                                    type: key
                                                }
                                            });

                                            // props.viewer.inputHandler.deselectAll();
                                            // props.viewer.inputHandler.toggleSelection(measurement);

                                            break;
                                        }
                                        case EViewerTool.Profile:
                                            profileTool.startInsertion({
                                                name: t('v3d.text.profileHeight'),
                                                properties: {
                                                    type: key
                                                }
                                            });

                                            break;
                                        case EViewerTool.RemoveMeasure:
                                            props.viewer.scene.removeAllMeasurements();

                                            break;
                                    }
                                }}
                            >
                                <Menu.Item key={EViewerTool.MeasureAngle}>
                                    {/*<FontAwesomeIcon icon={farRulerTriangle} className={"mr-1"}/>*/}
                                    <Image alt={""} src={IconMeasureAngle} className={"h-6 mr-1"}/>
                                    {t('v3d.text.angleMeasurement')}
                                </Menu.Item>
                                <Menu.Item key={EViewerTool.Point}>
                                    {/*<FontAwesomeIcon icon={farDotCircle} className={"mr-1"}/>*/}
                                    <Image alt={""} src={IconPoint} className={"h-6 mr-1"}/>
                                    {t('v3d.text.pointMeasurement')}
                                </Menu.Item>
                                <Menu.Item key={EViewerTool.MeasureDistance}>
                                    <Image alt={""} src={IconMeasureDistance} className={"h-6 mr-1"}/>
                                    {t('v3d.text.distanceMeasurement')}
                                </Menu.Item>
                                <Menu.Item key={EViewerTool.MeasureHeight}>
                                    <Image alt={""} src={IconMeasureHeight} className={"h-6 mr-1"}/>
                                    {t('v3d.text.heightMeasurement')}
                                </Menu.Item>
                                <Menu.Item key={EViewerTool.MeasureCircle}>
                                    {/*<FontAwesomeIcon icon={falDrawCircle} className={"mr-1"}/>*/}
                                    <Image alt={""} src={IconMeasureCircle} className={"h-6 mr-1"}/>
                                    {t('v3d.text.circleMeasurement')}
                                </Menu.Item>
                                <Menu.Item key={EViewerTool.MeasureAzimuth}>
                                    <Image alt={""} src={IconAzimuth} className={"h-6 mr-1"}/>
                                    {t('v3d.text.azimuth')}
                                </Menu.Item>
                                <Menu.Item key={EViewerTool.MeasureArea}>
                                    {/*<FontAwesomeIcon icon={falDrawPolygon} className={"mr-1"}/>*/}
                                    <Image alt={""} src={IconMeasureArea} className={"h-6 mr-1"}/>
                                    {t('v3d.text.areaMeasurement')}
                                </Menu.Item>
                                <Menu.Item key={EViewerTool.MeasureBoxVolume}>
                                    <Image alt={""} src={IconMeasureVolume} className={"h-6 mr-1"}/>
                                    {t('v3d.text.volumeMeasurement')}
                                </Menu.Item>
                                <Menu.Item key={EViewerTool.MeasureSphereVolume}>
                                    <Image alt={""} src={IconSphereVolume} className={"h-6 mr-1"}/>
                                    {t('v3d.text.sphereVolume')}
                                </Menu.Item>
                                <Menu.Item key={EViewerTool.Profile}>
                                    <Image alt={""} src={IconProfileHeight} className={"h-6 mr-1"}/>
                                    {t('v3d.text.profileHeight')}
                                </Menu.Item>
                                <Menu.Divider/>
                                <Menu.Item key={EViewerTool.RemoveMeasure}>
                                    <FontAwesomeIcon icon={farTimes} className={"mr-1 text-red-500"}/>
                                    {t('v3d.text.removeAllMeasurement')}
                                </Menu.Item>
                            </Menu>
                        }
                        visible={ppMeasureVisible}
                        onVisibleChange={(visible) => setPpMeasureVisible(visible)}
                    >
                        <Button
                            className={sms.Button}
                            icon={<FontAwesomeIcon icon={farRulerTriangle} className={sms.Icon}/>}
                        />
                    </Popover>
                    <Popover
                        overlayClassName={scm.PopoverMenu}
                        placement="left"
                        title={t('v3d.text.clipping')}
                        trigger="click"
                        content={
                            <Menu
                                className={'border-0'}
                                onClick={({key}) => {
                                    setPpClipVisible(false);

                                    switch (key) {
                                        case EViewerTool.ClipVolume: {
                                            volumeTool.startInsertion({
                                                clip: true,
                                                name: t('v3d.text.clipVolume'),
                                                properties: {
                                                    type: key
                                                }
                                            });

                                            break;
                                        }
                                        case EViewerTool.ClipPolygon: {
                                            clippingTool.startInsertion({
                                                type: "polygon",
                                                name: t('v3d.text.clipPolygon'),
                                                properties: {
                                                    type: key
                                                }
                                            });

                                            break;
                                        }
                                        case EViewerTool.ClipScreenBox:
                                            if (!(props.viewer.scene.getActiveCamera().isOrthographicCamera)) {
                                                notification.warning({
                                                    message: t('text.warning'),
                                                    description: t('v3d.text.switchOrthographicCamera'),
                                                })
                                            } else {
                                                (new ScreenBoxSelectTool(props.viewer)).startInsertion({
                                                    name: t('v3d.text.screenBoxSelect'),
                                                    properties: {
                                                        type: key
                                                    }
                                                });
                                            }

                                            break;
                                        case EViewerTool.RemoveClip:
                                            props.viewer.scene.removeAllClipVolumes();

                                            break;
                                    }
                                }}
                            >
                                <Menu.Item key={EViewerTool.ClipVolume}>
                                    <Image alt={""} src={IconClipVolume} className={"h-6 mr-1"}/>
                                    {t('v3d.text.clipVolume')}
                                </Menu.Item>
                                <Menu.Item key={EViewerTool.ClipPolygon}>
                                    <Image alt={""} src={IconClipPolygon} className={"h-6 mr-1"}/>
                                    {t('v3d.text.clipPolygon')}
                                </Menu.Item>
                                <Menu.Item key={EViewerTool.ClipScreenBox}>
                                    <Image alt={""} src={IconClipScreen} className={"h-6 mr-1"}/>
                                    {t('v3d.text.screenBoxSelect')}
                                </Menu.Item>
                                <Menu.Divider/>
                                <Menu.Item key={EViewerTool.RemoveClip}>
                                    <FontAwesomeIcon icon={farTimes} className={"mr-1 text-red-500"}/>
                                    {t('v3d.text.removeAllClipping')}
                                </Menu.Item>
                            </Menu>
                        }
                        visible={ppClipVisible}
                        onVisibleChange={(visible) => setPpClipVisible(visible)}
                    >
                        <Button
                            className={sms.Button}
                            icon={<FontAwesomeIcon icon={farPaperclip} className={sms.Icon}/>}
                        />
                    </Popover>
                    <Tooltip title={t('v3d.text.annotation')} placement="left">
                        <Button
                            className={sms.Button}
                            icon={<FontAwesomeIcon icon={farCommentsAlt} className={sms.Icon}/>}
                            onClick={() => {
                                const annotation = annotationTool.startInsertion({
                                    name: t('v3d.text.annotation'),
                                    title: t('text.title'),
                                    description: t('text.description'),
                                    properties: {
                                        type: EViewerTool.Annotation
                                    }
                                });

                                console.log('measurement: Annotation', annotation);
                            }}
                        />
                    </Tooltip>
                </Space>

                <Space
                    className={sms.SpaceGroup}
                    direction={'vertical'}
                    size={0}
                >
                    {
                        FeatureSingleton.getInstance().v3DShare
                            ? <>
                                <Tooltip title={t('text.share3D')} placement="left">
                                    <Button
                                        className={sms.Button}
                                        icon={<FontAwesomeIcon icon={farShareSquare} className={sms.Icon}/>}
                                        onClick={() => setIsShareModalVisible(true)}
                                    />
                                </Tooltip>
                                {
                                    isShareModalVisible
                                        ? <Map3DShareModalFC
                                            item={props.item}
                                            onClose={() => setIsShareModalVisible(false)}
                                            modalVisible={isShareModalVisible}
                                        />
                                        : null
                                }
                            </>
                            : null
                    }
                </Space>
            </Space>
        </div>
    )
})
