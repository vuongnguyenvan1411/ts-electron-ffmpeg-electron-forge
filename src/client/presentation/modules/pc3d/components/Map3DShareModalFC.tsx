import {Alert, Button, Card, Checkbox, Col, DatePicker, Divider, Form, Input, Modal, notification, Space, Switch, Table, Tag, Tooltip, Typography} from "antd";
import {useTranslation} from "react-i18next";
import React, {useCallback, useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import {CHashids} from "../../../../core/CHashids";
import {SendingStatus} from "../../../../const/Events";
import {CheckOutlined, CloseOutlined, CopyOutlined, DeleteTwoTone, EditOutlined, EyeOutlined, LinkOutlined, PlusCircleOutlined, QrcodeOutlined, SaveOutlined, SettingOutlined} from "@ant-design/icons";
import {ColumnsType} from "antd/es/table";
import {GetQrCodeModalFC} from "../../../widgets/GetQrCodeModalFC";
import {Utils} from "../../../../core/Utils";
import {Color} from "../../../../const/Color";
import {App} from "../../../../const/App";
import {Map3DShareModel, TMap3DShareV0} from "../../../../models/ShareModel";
import {RouteAction} from "../../../../const/RouteAction";
import {RouteConfig} from "../../../../config/RouteConfig";
import {Map3DModel} from "../../../../models/service/geodetic/Map3DModel";
import moment from "moment/moment";
import {useLocation, useNavigationType} from "react-router";
import {Map3DShareAction} from "../../../../recoil/service/geodetic/map_3d_share/Map3DShareAction";

export const Map3DShareModalFC = React.memo((props: {
    item: Map3DModel
    onClose: () => void,
    modalVisible: boolean,
}) => {
    const {t} = useTranslation()
    const navigateType = useNavigationType()
    const location = useLocation()
    const {hash} = useParams<{ hash: string }>()

    const decode = CHashids.decode(hash!)
    const m3dId = decode[1]

    const {
        vm,
        onLoadItems,
        onDeleteItem,
        onClearState,
    } = Map3DShareAction();

    const [formVisible, setFormVisible] = useState<{
        shareId?: string,
        visible: boolean,
        preview: boolean,
        initValues?: Map3DShareModel,
    }>({
        visible: false,
        preview: false,
    });
    const [visibleDelete, setVisibleDelete] = useState<{
        shareId?: string,
        visible: boolean,
    }>({
        visible: false,
    });
    const [isCopyOk, setIsCopyOk] = useState(false);

    useEffect(() => {
        console.log('%cMount Screen: Map3DShareModalFC', Color.ConsoleInfo);

        if (
            vm.items === null || vm.key !== m3dId!.toString()
            || (vm.timestamp !== undefined && Utils.checkHourState(vm.timestamp))
        ) {
            onLoadItems(m3dId, queryParams);
        }

        return () => {
            console.log('%cUnmount Screen: Map3DShareModalFC', Color.ConsoleInfo);
        };

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (vm.isUpdating === SendingStatus.success) {
            setFormVisible(
                {
                    ...formVisible,
                    visible: false,
                }
            )

            notification.success({
                message: t('text.successSetting'),
            });
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.isUpdating]);

    useEffect(() => {
        return () => {
            if (
                navigateType === RouteAction.PUSH
                || navigateType === RouteAction.REPLACE
                || navigateType === RouteAction.POP
            ) {
                if (location.pathname.indexOf(RouteConfig.GEODETIC_VIEW_PC3D) !== 0) {
                    onClearState();
                }
            }
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [navigateType]);

    useEffect(() => {
        if (vm.isDeleting === SendingStatus.success) {
            setVisibleDelete(
                {
                    ...visibleDelete,
                    visible: false,
                }
            )

            notification.success({
                message: t('success.delete'),
            });
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.isDeleting]);

    const [, forceUpdate] = useState({});

    useEffect(() => {
        forceUpdate({});
    }, []);

    const onHandleCancelDelete = () => {
        setVisibleDelete({
            ...visibleDelete,
            visible: false,
        })
    }

    const onHandleOkDelete = useCallback(() => {
        if (m3dId && visibleDelete.shareId) onDeleteItem(m3dId, visibleDelete.shareId);

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [visibleDelete, visibleDelete.shareId]);

    const onShowDelete = (shareId: string) => {
        setVisibleDelete({
            ...visibleDelete,
            shareId: shareId,
            visible: true,
        })
    };

    const showUserModal = (shareId?: string) => {
        setFormVisible({
            ...formVisible,
            shareId: shareId,
            visible: true,
            preview: false,
            initValues: undefined,
        })
    };

    const hideUserModal = () => {
        setFormVisible({
            ...formVisible,
            visible: false,
            preview: false,
        })
    };

    const onPreviewRowTable = (item: Map3DShareModel) => {
        setFormVisible({
            ...formVisible,
            shareId: item.sid,
            visible: true,
            preview: true,
            initValues: item,
        })
    }

    const onClickCopy = (shareId: string) => {
        vm.items?.forEach(item => {
            if (item.sid === shareId) {
                Utils.copyClipboard(item.link).then(() => {
                    setIsCopyOk(true);

                    notification.success({
                        message: t('text.successCopy'),
                        description: item.link
                    });

                    setTimeout(_ => setIsCopyOk(false), App.TimeoutHideCopy)
                });
            }
        })
    }

    const onEditTable = (item: Map3DShareModel) => {
        setFormVisible({
            ...formVisible,
            shareId: item.sid,
            visible: true,
            preview: false,
            initValues: item,
        });
    }

    const columns: ColumnsType<Map3DShareModel> = [
        {
            key: "name",
            title: t("text.name"),
            dataIndex: "name"
        },
        {
            key: "status",
            title: t("text.status"),
            render: (_, item, __) => (
                <Tag key={`tag_status_${item.sid}`} color={item.status ? 'success' : 'red'}>
                    {item.status ? t("text.active") : t("text.inActive")}
                </Tag>
            )
        },
        {
            key: "alive",
            title: t("text.activeTime"),
            render: (_, item) => {
                return (
                    item.expiryDate && item.expiryDate.start && item.expiryDate.end
                        ? <Space direction={"vertical"} style={{gap: 0}}>
                            <Typography.Text>{t("text.dateStart")}:&nbsp;&nbsp;{item.dateStartFormatted(t('format.dateTimeShort'))}</Typography.Text>
                            <Typography.Text>{t("text.dateEnd")}:&nbsp;&nbsp;{item.dateEndFormatted(t('format.dateTimeShort'))}</Typography.Text>
                        </Space>
                        : item.expiryDate?.start
                            ? <Typography.Text>{t("text.dateStart")}:&nbsp;&nbsp;{item.dateStartFormatted(t('format.dateTimeShort'))}</Typography.Text>
                            : item.expiryDate?.end
                                ? <Typography.Text>{t("text.dateEnd")}:&nbsp;&nbsp;{item.dateEndFormatted(t('format.dateTimeShort'))}</Typography.Text>
                                : <Typography.Text>{t("text.unlimited")}</Typography.Text>
                )
            }
        },
        {
            key: "qrCode",
            title: t('text.linkShare'),
            render: (_, item, __) => {
                return (
                    <Space>
                        <div
                            className={"ant-btn-group"}
                        >
                            <Tooltip
                                title={t('text.linkShare')}
                            >
                                <Button
                                    onClick={() => window.open(item.link, '_blank')}
                                    icon={<LinkOutlined/>}
                                />
                            </Tooltip>
                            <Button
                                type={'primary'}
                                onClick={() => onClickCopy(item.sid)}
                                icon={isCopyOk ? <CheckOutlined/> : <CopyOutlined/>}
                            />
                        </div>
                        <Tooltip
                            title={t('text.codeEmbedQrCode')}
                        >
                            <Button
                                onClick={onClickGetQrCode}
                                icon={<QrcodeOutlined/>}
                            />
                        </Tooltip>
                        {
                            isModalVisible
                                ? <GetQrCodeModalFC
                                    sid={item.sid}
                                    name={item.name}
                                    onClose={onCloseGetQrCode}
                                />
                                : null
                        }
                    </Space>
                )
            }
        },
        {
            key: "action",
            title: t("text.actions"),
            align: "right",
            render: (_, item, __) => {
                return (
                    <Space>
                        <Tooltip
                            title={t("button.view")}
                        >
                            <Button
                                type={"text"}
                                icon={<EyeOutlined/>}
                                onClick={() => onPreviewRowTable(item)}
                            />
                        </Tooltip>
                        <Tooltip
                            title={t("button.edit")}
                        >
                            <Button
                                type={"text"}
                                icon={<EditOutlined/>}
                                onClick={() => onEditTable(item)}
                            />
                        </Tooltip>
                        <Tooltip
                            title={t("button.delete")}
                        >
                            <Button
                                type={"text"}
                                icon={<DeleteTwoTone twoToneColor={Color.danger}/>}
                                onClick={() => onShowDelete(item.sid)}
                            />
                        </Tooltip>
                    </Space>
                )
            }
        }
    ]

    const [isModalVisible, setIsModalVisible] = useState(false);

    const onClickGetQrCode = () => {
        setIsModalVisible(true);
    }

    const onCloseGetQrCode = () => {
        setIsModalVisible(false);
    }

    if (m3dId === null) {
        props.onClose()
    }

    const [queryParams, setQueryParams] = useState<{ page: number, limit: number }>({
        page: vm.query.page,
        limit: vm.query.limit,
    })

    const onChangePage = (page: number) => {
        setQueryParams({
            ...queryParams,
            page: page,
            limit: vm.query.limit,
        })

        if (m3dId && vm.items && vm.items?.slice((page - 1) * vm.query.limit, page * vm.query.limit).length === 0)
            onLoadItems(m3dId, {
                page: page,
                limit: queryParams.limit,
            })
    }

    const isUpdatingError = vm.isUpdating === SendingStatus.error;

    return (
        <>
            <Modal
                key={"list"}
                visible={props.modalVisible}
                onCancel={props.onClose}
                title={t('text.createLink')}
                width={1000}
                onOk={props.onClose}
            >
                <Button
                    className={"mb-2"}
                    type={"primary"}
                    icon={<PlusCircleOutlined/>}
                    onClick={() => showUserModal()}
                >
                    {t("text.addSharingLink")}
                </Button>
                <Card
                    bodyStyle={{
                        padding: 0
                    }}
                >
                    {
                        isUpdatingError && vm.error && vm.error.hasOwnProperty('warning')
                            ? <Alert className={"mb-5"} message={vm.error['warning']} type="error" showIcon/>
                            : null
                    }
                    <Table<Map3DShareModel>
                        loading={vm.isLoading === SendingStatus.loading}
                        columns={columns}
                        dataSource={vm.items}
                        pagination={
                            {
                                total: vm.oMeta?.totalCount ?? 0,
                                defaultPageSize: 5,
                                defaultCurrent: 1,
                                onChange: (page) => onChangePage(page),
                            }
                        }
                        rowKey={_ => _.sid}
                    />
                </Card>
            </Modal>
            <ModalShare3D
                key={"modify"}
                visible={formVisible.visible}
                shareId={formVisible.shareId}
                m3dId={m3dId}
                onCancel={hideUserModal}
                initFormValues={formVisible.initValues}
                preview={formVisible.preview}
                item={props.item}
            />
            <Modal
                key={"delete"}
                title={t("title.confirmBeforeDeleting")}
                visible={visibleDelete.visible}
                onCancel={onHandleCancelDelete}
                confirmLoading={vm.isDeleting === SendingStatus.loading}
                onOk={onHandleOkDelete}
            >
                {t("message.confirmBeforeDeleting")}
            </Modal>
        </>
    )
})

const ModalShare3D = (props: {
    m3dId: number,
    shareId?: string,
    visible: boolean,
    preview: boolean,
    onCancel: () => void,
    initFormValues?: Map3DShareModel,
    item: Map3DModel
}) => {
    const {t} = useTranslation();
    const [formMain] = Form.useForm();

    const [isPreview, setIsPreview] = useState(false);

    const {
        vm,
        onAddItem,
        onEditItem,
    } = Map3DShareAction();

    useEffect(() => {
        setIsPreview(props.preview);

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.preview])

    const onAfterClose = () => {
        formMain.resetFields();
    };

    const onOk = () => {
        formMain.submit();
        setIsPreview(false);
    };

    const onChangeToEdit = () => {
        setIsPreview(false);
    };

    const onFinish = (values: any) => {
        const data: TMap3DShareV0 = {
            name: values.name,
            password: values.password,
            dateStart: values['range-time-picker'] && values['range-time-picker'][0] ? moment(values['range-time-picker'][0]).format('YYYY-MM-DD HH:mm:ss').toString() : '',
            dateEnd: values['range-time-picker'] && values['range-time-picker'][1] ? moment(values['range-time-picker'][1]).format('YYYY-MM-DD HH:mm:ss').toString() : '',
            status: values.isShare,
            sInfo: values.shareInfo,
            config: {
                pcs: values.pcs,
            }
        }

        if (props.shareId) {
            onEditItem(props.m3dId, props.shareId, data)
        } else {
            onAddItem(props.m3dId, data)
        }
    };

    return (
        <Modal
            visible={props.visible}
            onCancel={() => {
                props.onCancel()
                setIsPreview(false)
            }}
            key={"modal-form-main-setting-map3D"}
            title={isPreview ? t("text.detailInfo") : !props.shareId ? t("text.createLink") : t("text.editLink")}
            onOk={onOk}
            afterClose={onAfterClose}
            destroyOnClose={true}
            confirmLoading={vm.isLoading === SendingStatus.loading}
            width={700}
            footer={
                isPreview ?
                    [
                        <Button
                            key={"edit"}
                            type={"primary"}
                            onClick={onChangeToEdit}
                            icon={<EditOutlined/>}
                        >
                            {t("button.edit")}
                        </Button>,
                        <Button
                            key={"close"}
                            onClick={() => {
                                props.onCancel()
                                setIsPreview(false)
                            }}
                            icon={<CloseOutlined/>}
                        >
                            {t("button.close")}
                        </Button>
                    ] : [
                        <Button
                            key={"close"}
                            onClick={() => {
                                props.onCancel()
                                setIsPreview(false)
                            }}
                            icon={<CloseOutlined/>}
                        >
                            {t("button.close")}
                        </Button>,
                        <Button
                            key={"save"}
                            type={"primary"}
                            onClick={() => onOk()}
                            icon={<SaveOutlined/>}
                            loading={vm.isUpdating === SendingStatus.loading}
                        >
                            {props.shareId ? t("button.save") : t("button.saveNew")}
                        </Button>
                    ]

            }
        >
            <Form
                form={formMain}
                labelCol={{
                    xs: {span: 24},
                    sm: {span: 8},
                    xxl: {span: 10},
                    xl: {span: 10},
                }}
                wrapperCol={{
                    xs: {span: 24},
                    sm: {span: 16},
                    xxl: {span: 14},
                    xl: {span: 14},
                }}
                onFinish={onFinish}
            >
                <Form.Item
                    labelAlign={"left"}
                    key={"k_name"}
                    label={t("text.name")}
                    name={"name"}
                    rules={[
                        {
                            required: true,
                            message: t("validation.emptyData"),
                        },
                        {
                            max: 100,
                            message: t('validation.minAndMaxCharacter', {
                                label: t("text.name"),
                                min: '1',
                                max: '100'
                            }),
                        }
                    ]}
                    initialValue={props.initFormValues?.name}
                >
                    <Input disabled={isPreview} allowClear={true}/>
                </Form.Item>
                <Form.Item
                    labelAlign={"left"}
                    key={"k_password"}
                    label={t("label.password")}
                    name={"password"}
                    initialValue={props.initFormValues?.password}
                >
                    <Input disabled={isPreview}/>
                </Form.Item>
                <Form.Item
                    labelAlign={"left"}
                    key={"k_range-time-picker"}
                    name="range-time-picker"
                    label={t("text.activeTime")}
                    initialValue={[
                        props.initFormValues?.expiryDate?.start ? moment(props.initFormValues.expiryDate.start, App.FormatISOFromMoment) : null,
                        props.initFormValues?.expiryDate?.end ? moment(props.initFormValues.expiryDate.end, App.FormatISOFromMoment) : null
                    ]}
                >
                    <DatePicker.RangePicker
                        allowEmpty={[true, true]}
                        disabled={isPreview}
                        showTime
                        format={t("format.dateTime")}
                    />
                </Form.Item>
                <Form.Item
                    key={"isShare"}
                    label={t('text.sharingAllow')}
                    labelAlign={'left'}
                    name={"isShare"}
                    valuePropName={"checked"}
                    initialValue={props.initFormValues?.status ?? false}
                >
                    <Switch
                        disabled={isPreview}
                        defaultChecked={false}
                    />
                </Form.Item>
                <Form.Item
                    key={"sInfo"}
                    label={t('text.displayInformation')}
                    labelAlign={'left'}
                    name={"shareInfo"}
                    valuePropName={"checked"}
                    initialValue={props.initFormValues?.sInfo ?? false}
                >
                    <Switch
                        disabled={isPreview}
                        defaultChecked={false}
                    />
                </Form.Item>
                <Divider/>
                <Form.Item>
                    <Space>
                        <SettingOutlined/>
                        <Typography.Text>
                            {t("title.setUpAdvancedPermissions")}
                        </Typography.Text>
                    </Space>
                </Form.Item>
                <Form.Item
                    key={`pcs`}
                    label={t('v3d.text.pointCloud')}
                    labelAlign={'left'}
                    name={"pcs"}
                    initialValue={props.initFormValues?.config?.pcs ?? []}
                >
                    <Checkbox.Group
                        className={'overflow-auto w-full max-h-32'}
                        disabled={isPreview}
                    >
                        {
                            props.item.pc
                                ? props.item.pc.map((item, index) => {
                                    return (
                                        <Col flex={'500px'} key={`tiles_&${index}`}>
                                            <Checkbox
                                                value={item.id}
                                                style={{lineHeight: '32px'}}
                                            >
                                                {item.name}
                                            </Checkbox>
                                        </Col>
                                    )
                                })
                                : null
                        }
                    </Checkbox.Group>
                </Form.Item>
            </Form>
        </Modal>
    )
}
