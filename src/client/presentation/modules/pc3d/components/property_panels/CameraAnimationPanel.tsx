import {Viewer} from "../../viewer/viewer";
import {useTranslation} from "react-i18next";
import {CameraAnimation, ControlPoint} from "../../modules/CameraAnimation/CameraAnimation";
import {useEffect, useState} from "react";
import {Color} from "../../../../../const/Color";
import {CameraAnimationEvent} from "../../core/Event";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {farCameraMovie, farPlusCircle, farScrubber, fasEquals} from "../../../../../const/FontAwesome";
import {Badge, Button, Divider, InputNumber, List, message, Popconfirm, Slider, Space, Tooltip} from "antd";
import {DFConfig} from "../../core/Defines";
import {DeleteOutlined, PauseCircleOutlined, PlayCircleOutlined} from "@ant-design/icons";

type TEventPlaying = {
    type: string;
    time: number;
    duration: number;
}

export const CameraAnimationPanel = (props: {
    viewer: Viewer,
    animation: CameraAnimation
}) => {
    const {t} = useTranslation();

    const [ipWidth, setIpWidth] = useState<number>(props.animation.getDuration());
    const [isStart, setIsStart] = useState<boolean>(false);
    const [timePlaying, setTimePlaying] = useState<string>();
    const [keyframes, setKeyframes] = useState<ControlPoint[]>();

    useEffect(() => {
        console.log('%cMount Panel: CameraAnimationPanel', Color.ConsoleInfo, props.animation);

        updateKeyFrames();

        props.animation.addEventListener(CameraAnimationEvent.ControlPointAdded, updateKeyFrames);
        props.animation.addEventListener(CameraAnimationEvent.ControlPointRemoved, updateKeyFrames);
        props.animation.addEventListener(CameraAnimationEvent.Playing, onAnimationPlaying);
        props.animation.addEventListener(CameraAnimationEvent.Start, onAnimationStart);
        props.animation.addEventListener(CameraAnimationEvent.Stop, onAnimationStop);

        return () => {
            console.log('%cUnmount Panel: CameraAnimationPanel', Color.ConsoleInfo);

            props.animation.removeEventListener(CameraAnimationEvent.ControlPointAdded, updateKeyFrames);
            props.animation.removeEventListener(CameraAnimationEvent.ControlPointRemoved, updateKeyFrames);
            props.animation.removeEventListener(CameraAnimationEvent.Playing, onAnimationPlaying);
            props.animation.removeEventListener(CameraAnimationEvent.Start, onAnimationStart);
            props.animation.removeEventListener(CameraAnimationEvent.Stop, onAnimationStop);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.animation])

    const updateKeyFrames = () => {
        setKeyframes([...props.animation.controlPoints])
    }

    const onAnimationStart = () => {
        setIsStart(true);
    }

    const onAnimationStop = () => {
        setIsStart(false);
    }

    const onAnimationPlaying = (evt: TEventPlaying) => {
        setTimePlaying(evt.time.toFixed(2))
    }

    return (
        <>
            <Divider
                orientation="left"
                plain
                style={{
                    marginTop: 0,
                    marginBottom: "0.5rem"
                }}
            >
                <FontAwesomeIcon icon={farCameraMovie} className={"mr-1"}/>
                {t('text.info')} - {props.animation.name}
            </Divider>
            <List
                className={"mb-2"}
                header={t('v3d.text.keyframe')}
                size={"small"}
                bordered
                dataSource={keyframes}
                renderItem={(cp, index) => (
                    <List.Item className={"flex justify-between"}>
                        <div>
                            <Badge
                                className={"pt-0.5"}
                                style={{backgroundColor: '#1890ff'}}
                                count={index + 1}
                            />
                        </div>
                        <Space>
                            <Tooltip title={t('v3d.text.assign')} placement="top">
                                <Button
                                    size={"small"}
                                    onClick={() => {
                                        cp.position.copy(props.viewer.scene.view.position);
                                        cp.target.copy(props.viewer.scene.view.getPivot());
                                    }}
                                >
                                    <FontAwesomeIcon icon={fasEquals}/>
                                </Button>
                            </Tooltip>
                            <Tooltip title={t('v3d.text.move')} placement="top">
                                <Button
                                    size={"small"}
                                    onClick={() => {
                                        props.viewer.scene.view.position.copy(cp.position);
                                        props.viewer.scene.view.lookAt(cp.target);
                                    }}
                                >
                                    <FontAwesomeIcon icon={farScrubber}/>
                                </Button>
                            </Tooltip>
                            <Tooltip title={t('v3d.text.insertControlPoint')} placement="top">
                                <Button
                                    type={'primary'}
                                    size={"small"}
                                    onClick={() => props.animation.createControlPoint(index)}
                                >
                                    <FontAwesomeIcon icon={farPlusCircle}/>
                                </Button>
                            </Tooltip>
                            <Popconfirm
                                title={t('text.confirmDeleteMeasure')}
                                onConfirm={() => props.animation.removeControlPoint(cp)}
                                okText={t('text.yes')}
                                cancelText={t('text.no')}
                            >
                                <Tooltip title={t('button.delete')} placement="top">
                                    <Button
                                        type={"primary"}
                                        size={"small"}
                                        danger
                                        icon={<DeleteOutlined/>}
                                    />
                                </Tooltip>
                            </Popconfirm>
                        </Space>
                    </List.Item>
                )}
            />
            <div className={'px-2 pb-3'}>
                <InputNumber<number>
                    className={"w-full mb-1"}
                    min={DFConfig.DurationAnimation.min}
                    max={DFConfig.DurationAnimation.max}
                    step={DFConfig.DurationAnimation.step}
                    addonBefore={t('text.duration')}
                    addonAfter="s"
                    size={"small"}
                    parser={value => {
                        if (value) {
                            return parseFloat(parseFloat(value).toFixed(2));
                        }

                        return 0.00;
                    }}
                    formatter={value => {
                        if (value) {
                            if (typeof value === "string") {
                                value = parseFloat(value);
                            }

                            return (value as number).toFixed(2);
                        }

                        return '0.00';
                    }}
                    value={ipWidth}
                    onChange={(value: number) => {
                        props.animation.setDuration(value);

                        setIpWidth(value);
                    }}
                    onStep={(value, info) => {
                        if (info.type === 'up') {
                            const delta = value * 0.05;
                            const max = Math.max(1, delta / DFConfig.DurationAnimation.step);
                            const nv = (max * DFConfig.DurationAnimation.step) + value;

                            if (nv <= DFConfig.DurationAnimation.max) {
                                setIpWidth(nv);
                            }
                        }
                    }}
                    disabled={isStart}
                />
                <Slider
                    className={"mb-3"}
                    min={DFConfig.PlayAnimation.min}
                    max={DFConfig.PlayAnimation.max}
                    step={DFConfig.PlayAnimation.step}
                    defaultValue={DFConfig.PlayAnimation.value}
                    tipFormatter={(value: number) => value ?? 0}
                    onChange={(value: number) => {
                        props.animation.setT(value);
                    }}
                    disabled={isStart}
                />
                <div className={"flex justify-center"}>
                    <Button
                        type={"primary"}
                        size={"small"}
                        icon={isStart ? <PauseCircleOutlined/> : <PlayCircleOutlined/>}
                        onClick={() => props.animation.play()}
                        disabled={isStart}
                    >
                        {
                            isStart && timePlaying
                                ? timePlaying
                                : t('text.play')
                        }
                    </Button>
                </div>
                <Divider className={"my-2"}/>
                <div className={"flex justify-end"}>
                    <Popconfirm
                        title={t('text.confirmDeleteAnnotation')}
                        onConfirm={() => {
                            props.viewer.scene.removeCameraAnimation(props.animation);
                            message.success(t('success.delete')).then();
                        }}
                        okText={t('text.yes')}
                        cancelText={t('text.no')}
                    >
                        <Tooltip title={t('button.delete')} placement="top">
                            <Button
                                type={"primary"}
                                size={"small"}
                                danger
                                icon={<DeleteOutlined/>}
                            />
                        </Tooltip>
                    </Popconfirm>
                </div>
            </div>
        </>
    )
}
