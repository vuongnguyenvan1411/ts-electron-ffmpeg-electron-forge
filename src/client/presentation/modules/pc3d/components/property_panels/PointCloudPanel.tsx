import {Viewer} from "../../viewer/viewer";
import {PointCloudOctree} from "../../core/PointCloudOctree";
import {useTranslation} from "react-i18next";
import {useEffect, useMemo, useRef, useState} from "react";
import {DFConfig, ElevationGradientRepeat, PointShape, PointSizeType, TSliderRange} from "../../core/Defines";
import {Color as TColor, ColorResult, SketchPicker} from "react-color";
import {ViewerEventName} from "../../core/Event";
import {PointAttribute} from "../../loader/PointAttributes";
import {Divider, Radio, Select, Slider} from "antd";
import {Utils} from "../../core/Utils";
import {Gradients} from "../../materials/Gradients";
import {Color} from "three";
import {App} from "../../core/App";
import styles from "../../styles/Viewer.module.scss";
import Image from "next/image";

export const PointCloudPanel = (props: {
    viewer: Viewer,
    pointCloud: PointCloudOctree
}) => {
    const {t} = useTranslation();

    const material = props.pointCloud.material;

    const [sldPointSize, setSldPointSize] = useState<number>(material.size);
    const [sldMinPointSize, setSldMinPointSize] = useState<number>(material.minSize);
    const [optPointSizing, setOptPointSizing] = useState<number>(material.pointSizeType);
    const [optShape, setOptShape] = useState<number>(material.shape);
    const [sldOpacity, setSldOpacity] = useState<number>(material.opacity);
    const [optMaterial, setOptMaterial] = useState<string>();

    /* materials */
    // rgb_container
    const [sldRGBGamma, setSldRGBGamma] = useState<number>(material.rgbGamma);
    const [sldRGBBrightness, setSldRGBBrightness] = useState<number>(material.rgbBrightness);
    const [sldRGBContrast, setSldRGBContrast] = useState<number>(material.rgbContrast);
    // composite_weight_container
    const [sldWeightRGB, setSldWeightRGB] = useState<number>(material.weightRGB);
    const [sldWeightIntensity, setSldWeightIntensity] = useState<number>(material.weightIntensity);
    const [sldWeightElevation, setSldWeightElevation] = useState<number>(material.weightElevation);
    const [sldWeightClassification, setSldWeightClassification] = useState<number>(material.weightClassification);
    const [sldWeightReturnNumber, setSldWeightReturnNumber] = useState<number>(material.weightReturnNumber);
    const [sldWeightSourceID, setSldWeightSourceID] = useState<number>(material.weightSourceID);
    // elevation_container
    const [sldHeightRange, setSldHeightRange] = useState<[number, number]>(DFConfig.ElevationHeightRange.values);
    const refHeightRange = useRef<TSliderRange>({
        min: DFConfig.ElevationHeightRange.min,
        max: DFConfig.ElevationHeightRange.max,
        step: DFConfig.ElevationHeightRange.step,
        values: DFConfig.ElevationHeightRange.values
    });
    const [rdGro, setRdGro] = useState<number>(props.viewer.elevationGradientRepeat);
    // color_container
    const [pkColor, setPkColor] = useState<TColor>(material.color.getHexString());
    // intensity_container
    const [sldIntensityGamma, setSldIntensityGamma] = useState<number>(material.intensityGamma);
    const [sldIntensityBrightness, setSldIntensityBrightness] = useState<number>(material.intensityBrightness);
    const [sldIntensityContrast, setSldIntensityContrast] = useState<number>(material.intensityContrast);
    const [sldIntensityRange, setSldIntensityRange] = useState<[number, number]>();
    const refIntensityRange = useRef<TSliderRange>({
        min: 0,
        max: 0,
        step: 0,
        values: [0, 0]
    });
    // extra_container
    const [sldExtraRange, setSldExtraRange] = useState<[number, number]>([0, 0]);
    const [sldExtraGamma, setSldExtraGamma] = useState<number>(material.extraGamma);
    const [sldExtraBrightness, setSldExtraBrightness] = useState<number>(material.extraBrightness);
    const [sldExtraContrast, setSldExtraContrast] = useState<number>(material.extraContrast);
    const refExtraRange = useRef<TSliderRange>({
        min: 0,
        max: 0,
        step: 0,
        values: [0, 0]
    });
    /* --------- */

    useEffect(() => {
        material.addEventListener(ViewerEventName.MaterialPropertyChanged, updateExtraRange);
        material.addEventListener(ViewerEventName.MaterialPropertyChanged, updateHeightRange);
        material.addEventListener(ViewerEventName.MaterialPropertyChanged, onIntensityChange);
        material.addEventListener(ViewerEventName.MaterialPropertyChanged, onRGBChange);

        updateExtraRange();
        updateHeightRange();
        onIntensityChange();
        onRGBChange();

        return () => {
            material.removeEventListener(ViewerEventName.MaterialPropertyChanged, updateExtraRange);
            material.removeEventListener(ViewerEventName.MaterialPropertyChanged, updateHeightRange);
            material.removeEventListener(ViewerEventName.MaterialPropertyChanged, onIntensityChange);
            material.removeEventListener(ViewerEventName.MaterialPropertyChanged, onRGBChange);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.pointCloud])

    const getSelectOptionMaterial = useMemo(() => {
        const attributes = props.pointCloud.pcoGeometry.pointAttributes.attributes;

        let options = [];

        options.push(...attributes.map((a: PointAttribute) => a.name));

        const intensityIndex = options.indexOf("intensity");

        if (intensityIndex >= 0) {
            options.splice(intensityIndex + 1, 0, "intensity gradient");
        }

        options.push(
            "elevation",
            "color",
            'matcap',
            // 'indices',
            'level of detail',
            'composite'
        );

        const blacklist = [
            "POSITION_CARTESIAN",
            "position",
        ];

        options = options.filter(o => !blacklist.includes(o));

        setOptMaterial(options[0]);

        return options.map((item, index) => <Select.Option value={item} key={index}>{item}</Select.Option>)

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.pointCloud]);

    const onChangeOptMaterial = (value: string) => {
        console.log('onChangeOptMaterial', value);

        setOptMaterial(value);

        material.activeAttributeName = value;

        let attribute = props.pointCloud.getAttribute(value);

        if (value === "intensity gradient") {
            attribute = props.pointCloud.getAttribute("intensity");
        }

        const isIntensity = attribute ? ["intensity", "intensity gradient"].includes(attribute.name) : false;

        if (isIntensity) {
            if (props.pointCloud.material.intensityRange[0] === Infinity) {
                props.pointCloud.material.intensityRange = attribute.range;
            }

            const [min, max] = attribute.range;

            refIntensityRange.current = {
                ...refIntensityRange.current,
                min: min,
                max: max,
                step: 0.01,
                values: [min, max]
            }
        } else if (attribute) {
            const [min, max] = attribute.range;

            let selectedRange = material.getRange(attribute.name);

            if (!selectedRange) {
                selectedRange = [...attribute.range];
            }

            let minMaxAreNumbers = typeof min === "number" && typeof max === "number";

            if (minMaxAreNumbers) {
                refExtraRange.current = {
                    ...refIntensityRange.current,
                    min: min,
                    max: max,
                    step: 0.01,
                    values: selectedRange
                }
            }
        }
    }

    const onIntensityChange = () => {
        updateIntensityRange();

        setSldIntensityGamma(material.intensityGamma);
        setSldIntensityBrightness(material.intensityBrightness);
        setSldIntensityContrast(material.intensityContrast);
    }

    const onRGBChange = () => {
        setSldRGBGamma(material.rgbGamma);
        setSldRGBBrightness(material.rgbBrightness);
        setSldRGBContrast(material.rgbContrast);
    }

    const updateHeightRange = () => {
        const aPosition = props.pointCloud.getAttribute("position");

        let bMin, bMax;

        if (aPosition) {
            // for new format 2.0 and loader that contain precomputed min/max of attributes
            const min = aPosition.range[0][2];
            const max = aPosition.range[1][2];
            const width = max - min;

            bMin = min - 0.2 * width;
            bMax = max + 0.2 * width;
        } else {
            // for format up until exclusive 2.0
            let box = [
                props.pointCloud.pcoGeometry.tightBoundingBox,
                props.pointCloud.getBoundingBoxWorld()
            ].find(v => v !== undefined);

            props.pointCloud.updateMatrixWorld(true);
            box = Utils.computeTransformedBoundingBox(box, props.pointCloud.matrixWorld);

            const bWidth = box.max.z - box.min.z;

            bMin = box.min.z - 0.2 * bWidth;
            bMax = box.max.z + 0.2 * bWidth;
        }

        refHeightRange.current = {
            ...refHeightRange.current,
            min: bMin,
            max: bMax,
            values: material.elevationRange
        }
    }

    const updateExtraRange = () => {
        const attributeName = material.activeAttributeName;
        const attribute = props.pointCloud.getAttribute(attributeName);

        if (attribute == null) {
            return;
        }

        let range = material.getRange(attributeName);

        if (range == null) {
            range = attribute.range;
        }

        // currently, only supporting scalar ranges.
        // rgba, normals, positions, etc. have vector ranges, however
        const isValidRange = (typeof range[0] === "number") && (typeof range[1] === "number");

        if (!isValidRange) {
            return;
        }

        if (range) {
            refExtraRange.current = {
                ...refExtraRange.current,
                values: range
            }
        }
    }

    const updateIntensityRange = () => {
        const range = material.intensityRange;

        if (range.length > 2) {
            refIntensityRange.current = {
                ...refIntensityRange.current,
                values: range
            }
        }
    }

    const MBlockWeights = useMemo(() => {
        return (
            <>
                <Divider
                    style={{
                        fontSize: "small",
                        marginBottom: '0.5rem'
                    }}
                >{t('v3d.text.materialAttributeWeights')}</Divider>
                <div className={styles.NavItem}>
                    <div className={styles.NavTitle}>
                        {t('v3d.text.weightRGB')}: {sldWeightRGB.toFixed(2)}
                    </div>
                    <Slider
                        min={DFConfig.WeightRGB.min}
                        max={DFConfig.WeightRGB.max}
                        step={DFConfig.WeightRGB.step}
                        defaultValue={sldWeightRGB}
                        tipFormatter={(value: number) => value ? value.toFixed(2) : 0}
                        onChange={(value: number) => {
                            setSldWeightRGB(value);
                            material.weightRGB = value;
                        }}
                    />
                </div>
                <div className={styles.NavItem}>
                    <div className={styles.NavTitle}>
                        {t('v3d.text.weightIntensity')}: {sldWeightIntensity.toFixed(2)}
                    </div>
                    <Slider
                        min={DFConfig.WeightIntensity.min}
                        max={DFConfig.WeightIntensity.max}
                        step={DFConfig.WeightIntensity.step}
                        defaultValue={sldWeightIntensity}
                        tipFormatter={(value: number) => value ? value.toFixed(2) : 0}
                        onChange={(value: number) => {
                            setSldWeightIntensity(value);
                            material.weightIntensity = value;
                        }}
                    />
                </div>
                <div className={styles.NavItem}>
                    <div className={styles.NavTitle}>
                        {t('v3d.text.weightElevation')}: {sldWeightElevation.toFixed(2)}
                    </div>
                    <Slider
                        min={DFConfig.WeightElevation.min}
                        max={DFConfig.WeightElevation.max}
                        step={DFConfig.WeightElevation.step}
                        defaultValue={sldWeightElevation}
                        tipFormatter={(value: number) => value ? value.toFixed(2) : 0}
                        onChange={(value: number) => {
                            setSldWeightElevation(value);
                            material.weightElevation = value;
                        }}
                    />
                </div>
                <div className={styles.NavItem}>
                    <div className={styles.NavTitle}>
                        {t('v3d.text.weightClassification')}: {sldWeightClassification.toFixed(2)}
                    </div>
                    <Slider
                        min={DFConfig.WeightClassification.min}
                        max={DFConfig.WeightClassification.max}
                        step={DFConfig.WeightClassification.step}
                        defaultValue={sldWeightClassification}
                        tipFormatter={(value: number) => value ? value.toFixed(2) : 0}
                        onChange={(value: number) => {
                            setSldWeightClassification(value);
                            material.weightClassification = value;
                        }}
                    />
                </div>
                <div className={styles.NavItem}>
                    <div className={styles.NavTitle}>
                        {t('v3d.text.weightReturnNumber')}: {sldWeightReturnNumber.toFixed(2)}
                    </div>
                    <Slider
                        min={DFConfig.WeightReturnNumber.min}
                        max={DFConfig.WeightReturnNumber.max}
                        step={DFConfig.WeightReturnNumber.step}
                        defaultValue={sldWeightReturnNumber}
                        tipFormatter={(value: number) => value ? value.toFixed(2) : 0}
                        onChange={(value: number) => {
                            setSldWeightReturnNumber(value);
                            material.weightReturnNumber = value;
                        }}
                    />
                </div>
                <div className={styles.NavItem}>
                    <div className={styles.NavTitle}>
                        {t('v3d.text.weightSourceID')}: {sldWeightSourceID.toFixed(2)}
                    </div>
                    <Slider
                        min={DFConfig.WeightSourceID.min}
                        max={DFConfig.WeightSourceID.max}
                        step={DFConfig.WeightSourceID.step}
                        defaultValue={sldWeightSourceID}
                        tipFormatter={(value: number) => value ? value.toFixed(2) : 0}
                        onChange={(value: number) => {
                            setSldWeightSourceID(value);
                            material.weightSourceID = value;
                        }}
                    />
                </div>
            </>
        )

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [
        sldWeightRGB,
        sldWeightIntensity,
        sldWeightElevation,
        sldWeightClassification,
        sldWeightReturnNumber,
        sldWeightSourceID
    ])

    const MBlockElevation = useMemo(() => {
        updateHeightRange();

        const schemes = Object.keys(Gradients).map(name => ({name: name, values: Gradients[name]}));

        const listSvg: JSX.Element[] = [];

        schemes.forEach((item, index) => {
            const svg = Utils.createSvgGradient(item.values);

            listSvg.push(
                <span
                    className={"flex-grow cursor-pointer"}
                    key={index}
                    onClick={() => {
                        material.gradient = Gradients[item.name];
                    }}
                    dangerouslySetInnerHTML={{
                        __html: svg.outerHTML
                    }}
                />
            )
        })

        return (
            <>
                <Divider
                    style={{
                        fontSize: "small",
                        marginBottom: '0.5rem'
                    }}
                >{t('v3d.text.materialElevation')}</Divider>
                <div className={styles.NavItem}>
                    <div className={styles.NavTitle}>
                        {t('v3d.text.elevationRange')}: {refHeightRange.current.values[0].toFixed(2)} - {refHeightRange.current.values[1].toFixed(2)}
                    </div>
                    <Slider
                        range
                        min={refHeightRange.current.min}
                        max={refHeightRange.current.max}
                        step={refHeightRange.current.step}
                        defaultValue={refHeightRange.current.values}
                        onChange={(value) => {
                            setSldHeightRange(value);
                            material.heightMin = value[0];
                            material.heightMax = value[1];
                        }}
                    />
                </div>
                <div className={styles.NavItem}>
                    <div className={styles.NavTitle}>
                        {t('v3d.text.gradient')}
                    </div>
                    <Radio.Group
                        className={"pl-1 flex w-full mt-1 mb-2"}
                        options={[
                            {
                                label: t('v3d.text.clamp'),
                                value: ElevationGradientRepeat.CLAMP,
                            },
                            {
                                label: t('v3d.text.repeat'),
                                value: ElevationGradientRepeat.REPEAT,
                            },
                            {
                                label: t('v3d.text.mirroredRepeat'),
                                value: ElevationGradientRepeat.MIRRORED_REPEAT,
                            },
                        ]}
                        onChange={(e) => {
                            setRdGro(e.target.value);
                            props.viewer.setElevationGradientRepeat(e.target.value);
                        }}
                        value={rdGro}
                        optionType="button"
                        buttonStyle="solid"
                        size={"small"}
                    />
                </div>
                <div className={styles.NavItem}>
                    <div className={styles.NavTitle}>
                        {t('v3d.text.gradient')}
                    </div>
                    <div
                        className={"flex pl-1 mt-1"}
                    >
                        {
                            listSvg.map((item) => item)
                        }
                    </div>
                </div>
            </>
        )

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [
        sldHeightRange,
        refHeightRange,
        rdGro
    ])

    const MBlockRGB = useMemo(() => {
        return (
            <>
                <Divider
                    style={{
                        fontSize: "small",
                        marginBottom: '0.5rem'
                    }}
                >{t('v3d.text.materialRgba')}</Divider>
                <div className={styles.NavItem}>
                    <div className={styles.NavTitle}>
                        {t('v3d.text.gamma')}: {sldRGBGamma.toFixed(2)}
                    </div>
                    <Slider
                        min={DFConfig.RGBGamma.min}
                        max={DFConfig.RGBGamma.max}
                        step={DFConfig.RGBGamma.step}
                        defaultValue={sldRGBGamma}
                        tipFormatter={(value: number) => value ? value.toFixed(2) : 0}
                        onChange={(value: number) => {
                            setSldRGBGamma(value);
                            material.rgbGamma = value;
                        }}
                    />
                </div>
                <div className={styles.NavItem}>
                    <div className={styles.NavTitle}>
                        {t('v3d.text.brightness')}: {sldRGBBrightness.toFixed(2)}
                    </div>
                    <Slider
                        min={DFConfig.RGBBrightness.min}
                        max={DFConfig.RGBBrightness.max}
                        step={DFConfig.RGBBrightness.step}
                        defaultValue={sldRGBBrightness}
                        tipFormatter={(value: number) => value ? value.toFixed(2) : 0}
                        onChange={(value: number) => {
                            setSldRGBBrightness(value);
                            material.rgbBrightness = value;
                        }}
                    />
                </div>
                <div className={styles.NavItem}>
                    <div className={styles.NavTitle}>
                        {t('v3d.text.contrast')}: {sldRGBContrast.toFixed(2)}
                    </div>
                    <Slider
                        min={DFConfig.RGBContrast.min}
                        max={DFConfig.RGBContrast.max}
                        step={DFConfig.RGBContrast.step}
                        defaultValue={sldRGBContrast}
                        tipFormatter={(value: number) => value ? value.toFixed(2) : 0}
                        onChange={(value: number) => {
                            setSldRGBContrast(value);
                            material.rgbContrast = value;
                        }}
                    />
                </div>
            </>
        )

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [
        sldRGBGamma,
        sldRGBBrightness,
        sldRGBContrast
    ])

    const MBlockColor = useMemo(() => {
        return (
            <>
                <Divider
                    style={{
                        fontSize: "small",
                        marginBottom: '0.5rem'
                    }}
                >{t('v3d.text.materialColor')}</Divider>
                <SketchPicker
                    className={'mx-auto leading-none'}
                    color={pkColor}
                    onChange={(color: ColorResult) => {
                        const cRGB = color.rgb;

                        setPkColor(cRGB);

                        material.color = new Color().setRGB(cRGB.r / 255, cRGB.g / 255, cRGB.b / 255);
                    }}
                />
            </>
        )

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [pkColor])

    const MBlockIntensity = useMemo(() => {
        updateIntensityRange();

        return (
            <>
                <Divider
                    style={{
                        fontSize: "small",
                        marginBottom: 0
                    }}
                >{t('v3d.text.materialIntensity')}</Divider>
                <div className={styles.NavItem}>
                    {
                        refIntensityRange.current.values.length > 2
                            ? <div className={styles.NavTitle}>
                                {t('v3d.text.range')}: {refIntensityRange.current.values[0]} - {refIntensityRange.current.values[1]}
                            </div>
                            : null
                    }
                    <Slider
                        range
                        min={refIntensityRange.current.min}
                        max={refIntensityRange.current.max}
                        step={refIntensityRange.current.step}
                        defaultValue={refIntensityRange.current.values}
                        onChange={(value) => {
                            setSldIntensityRange(value);

                            material.intensityRange = [value[0], value[1]];
                        }}
                    />
                </div>
                <div className={styles.NavItem}>
                    <div className={styles.NavTitle}>
                        {t('v3d.text.gamma')}: {sldIntensityGamma.toFixed(2)}
                    </div>
                    <Slider
                        min={DFConfig.IntensityGamma.min}
                        max={DFConfig.IntensityGamma.max}
                        step={DFConfig.IntensityGamma.step}
                        defaultValue={sldIntensityGamma}
                        tipFormatter={(value: number) => value ? value.toFixed(2) : 0}
                        onChange={(value: number) => {
                            setSldIntensityGamma(value);
                            material.intensityGamma = value;
                        }}
                    />
                </div>
                <div className={styles.NavItem}>
                    <div className={styles.NavTitle}>
                        {t('v3d.text.brightness')}: {sldIntensityBrightness.toFixed(2)}
                    </div>
                    <Slider
                        min={DFConfig.IntensityBrightness.min}
                        max={DFConfig.IntensityBrightness.max}
                        step={DFConfig.IntensityBrightness.step}
                        defaultValue={sldIntensityBrightness}
                        tipFormatter={(value: number) => value ? value.toFixed(2) : 0}
                        onChange={(value: number) => {
                            setSldIntensityBrightness(value);
                            material.intensityBrightness = value;
                        }}
                    />
                </div>
                <div className={styles.NavItem}>
                    <div className={styles.NavTitle}>
                        {t('v3d.text.contrast')}: {sldIntensityContrast.toFixed(2)}
                    </div>
                    <Slider
                        min={DFConfig.IntensityContrast.min}
                        max={DFConfig.IntensityContrast.max}
                        step={DFConfig.IntensityContrast.step}
                        defaultValue={sldIntensityContrast}
                        tipFormatter={(value: number) => value ? value.toFixed(2) : 0}
                        onChange={(value: number) => {
                            setSldIntensityContrast(value);
                            material.intensityContrast = value;
                        }}
                    />
                </div>
            </>
        )

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [
        refIntensityRange,
        sldIntensityRange,
        sldIntensityGamma,
        sldIntensityBrightness,
        sldIntensityContrast,
    ])

    // const MBlockIndex = useMemo(() => {
    //     return (
    //         <Divider
    //             style={{
    //                 fontSize: "small",
    //                 marginBottom: 0
    //             }}
    //         >{t('v3d.text.materialIndices')}</Divider>
    //     )
    //
    //     // eslint-disable-next-line react-hooks/exhaustive-deps
    // }, [])

    // const MBlockTransition

    const MBlockMatcap = useMemo(() => {
        const matcaps = [
            {name: "Normals", icon: `${App.resourcePath}/icons/matcap/check_normal+y.jpg`},
            {name: "Basic 1", icon: `${App.resourcePath}/icons/matcap/basic_1.jpg`},
            {name: "Basic 2", icon: `${App.resourcePath}/icons/matcap/basic_2.jpg`},
            {name: "Basic Dark", icon: `${App.resourcePath}/icons/matcap/basic_dark.jpg`},
            {name: "Basic Side", icon: `${App.resourcePath}/icons/matcap/basic_side.jpg`},
            {name: "Ceramic Dark", icon: `${App.resourcePath}/icons/matcap/ceramic_dark.jpg`},
            {name: "Ceramic Lightbulb", icon: `${App.resourcePath}/icons/matcap/ceramic_lightbulb.jpg`},
            {name: "Clay Brown", icon: `${App.resourcePath}/icons/matcap/clay_brown.jpg`},
            {name: "Clay Muddy", icon: `${App.resourcePath}/icons/matcap/clay_muddy.jpg`},
            {name: "Clay Studio", icon: `${App.resourcePath}/icons/matcap/clay_studio.jpg`},
            {name: "Resin", icon: `${App.resourcePath}/icons/matcap/resin.jpg`},
            {name: "Skin", icon: `${App.resourcePath}/icons/matcap/skin.jpg`},
            {name: "Jade", icon: `${App.resourcePath}/icons/matcap/jade.jpg`},
            {name: "Metal_ Anisotropic", icon: `${App.resourcePath}/icons/matcap/metal_anisotropic.jpg`},
            {name: "Metal Carpaint", icon: `${App.resourcePath}/icons/matcap/metal_carpaint.jpg`},
            {name: "Metal Lead", icon: `${App.resourcePath}/icons/matcap/metal_lead.jpg`},
            {name: "Metal Shiny", icon: `${App.resourcePath}/icons/matcap/metal_shiny.jpg`},
            {name: "Pearl", icon: `${App.resourcePath}/icons/matcap/pearl.jpg`},
            {name: "Toon", icon: `${App.resourcePath}/icons/matcap/toon.jpg`},
            {name: "Check Rim Light", icon: `${App.resourcePath}/icons/matcap/check_rim_light.jpg`},
            {name: "Check Rim Dark", icon: `${App.resourcePath}/icons/matcap/check_rim_dark.jpg`},
            {name: "Contours 1", icon: `${App.resourcePath}/icons/matcap/contours_1.jpg`},
            {name: "Contours 2", icon: `${App.resourcePath}/icons/matcap/contours_2.jpg`},
            {name: "Contours 3", icon: `${App.resourcePath}/icons/matcap/contours_3.jpg`},
            {
                name: "Reflection Check Horizontal",
                icon: `${App.resourcePath}/icons/matcap/reflection_check_horizontal.jpg`
            },
            // {
            //     name: "Reflection Check Vertical",
            //     icon: `${App.resourcePath}/icons/matcap/reflection_check_vertical.jpg`
            // },
        ];

        const listImg: JSX.Element[] = [];

        matcaps.forEach((item, index) => {
            listImg.push(
                <Image
                    className={"w-1/5 cursor-pointer"}
                    key={index}
                    src={item.icon}
                    alt={item.name}
                    onClick={() => material.matcap = item.icon.substring(item.icon.lastIndexOf('/'))}
                />
            )
        })

        return (
            <>
                <Divider
                    style={{
                        fontSize: "small",
                        marginBottom: '0.5rem'
                    }}
                >{t('v3d.text.materialMapCat')}</Divider>
                <div
                    className={"flex flex-wrap pl-1 mt-1"}
                >
                    {
                        listImg.map((item) => item)
                    }
                </div>
            </>
        )

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const MBlockExtra = useMemo(() => {
        updateExtraRange();

        return (
            <>
                <Divider
                    style={{
                        fontSize: "small",
                        marginBottom: 0
                    }}
                >{t('v3d.text.materialExtra')}</Divider>
                <div className={styles.NavItem}>
                    <div className={styles.NavTitle}>
                        {t('v3d.text.extraRange')}: {refExtraRange.current.values[0].toFixed(2)} - {refExtraRange.current.values[1].toFixed(2)}
                    </div>
                    <Slider
                        range
                        min={refExtraRange.current.min}
                        max={refExtraRange.current.max}
                        step={refExtraRange.current.step}
                        defaultValue={refExtraRange.current.values}
                        onChange={(value) => {
                            setSldExtraRange(value);

                            if (optMaterial) {
                                const attribute = props.pointCloud.getAttribute(optMaterial);
                                material.setRange(attribute.name, [value[0], value[1]]);
                            }
                        }}
                    />
                </div>
                <div className={styles.NavItem}>
                    <div className={styles.NavTitle}>
                        {t('v3d.text.gamma')}: {sldExtraGamma.toFixed(2)}
                    </div>
                    <Slider
                        min={DFConfig.ExtraGamma.min}
                        max={DFConfig.ExtraGamma.max}
                        step={DFConfig.ExtraGamma.step}
                        defaultValue={sldExtraGamma}
                        tipFormatter={(value: number) => value ? value.toFixed(2) : 0}
                        onChange={(value: number) => {
                            setSldExtraGamma(value);
                            material.extraGamma = value;
                        }}
                    />
                </div>
                <div className={styles.NavItem}>
                    <div className={styles.NavTitle}>
                        {t('v3d.text.brightness')}: {sldExtraBrightness.toFixed(2)}
                    </div>
                    <Slider
                        min={DFConfig.ExtraBrightness.min}
                        max={DFConfig.ExtraBrightness.max}
                        step={DFConfig.ExtraBrightness.step}
                        defaultValue={sldExtraBrightness}
                        tipFormatter={(value: number) => value ? value.toFixed(2) : 0}
                        onChange={(value: number) => {
                            setSldExtraBrightness(value);
                            material.extraBrightness = value;
                        }}
                    />
                </div>
                <div className={styles.NavItem}>
                    <div className={styles.NavTitle}>
                        {t('v3d.text.contrast')}: {sldExtraContrast.toFixed(2)}
                    </div>
                    <Slider
                        min={DFConfig.ExtraContrast.min}
                        max={DFConfig.ExtraContrast.max}
                        step={DFConfig.ExtraContrast.step}
                        defaultValue={sldExtraContrast}
                        tipFormatter={(value: number) => value ? value.toFixed(2) : 0}
                        onChange={(value: number) => {
                            setSldExtraContrast(value);
                            material.extraContrast = value;
                        }}
                    />
                </div>
            </>
        )

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [
        sldExtraRange,
        refExtraRange,
        sldExtraGamma,
        sldExtraBrightness,
        sldExtraContrast,
    ])

    const getContainerMaterial = () => {
        switch (optMaterial) {
            case "elevation": // elevation_container
                return MBlockElevation;
            case "rgba": // rgb_container
                return MBlockRGB;
            case "color": // color_container
                return MBlockColor;
            // case "indices": // index_container
            //     return MBlockIndex;
            case "matcap": // index_container
                return MBlockMatcap;
            case "composite":
                return (
                    <>
                        {MBlockWeights}
                        {MBlockRGB}
                        {MBlockElevation}
                        {MBlockIntensity}
                    </>
                );
            case "RGB and Elevation":
                return (
                    <>
                        {MBlockRGB}
                        {MBlockElevation}
                    </>
                );
            case "intensity":
            case "intensity gradient":
                return MBlockIntensity;
            case "classification":
            case "number of returns":
            case "return number":
            case "source id":
            case "point source id":
                return null;
        }

        return MBlockExtra;
    }

    return (
        <div className={"p-4 pt-1 bg-white"}>
            <Divider
                orientation="left"
                style={{
                    fontSize: "small",
                    marginTop: 0,
                    marginBottom: '0.5rem'
                }}
            >{t('text.properties')}</Divider>
            <div className={styles.NavItem}>
                <div className={styles.NavTitle}>
                    {t('v3d.text.pointSize')}: {sldPointSize.toFixed(2)}
                </div>
                <Slider
                    min={DFConfig.PointSize.min}
                    max={DFConfig.PointSize.max}
                    step={DFConfig.PointSize.step}
                    value={sldPointSize}
                    tipFormatter={(value: number) => value ? value.toFixed(2) : 0}
                    onChange={(value: number) => {
                        setSldPointSize(value);
                        material.size = value;
                    }}
                />
            </div>
            <div className={styles.NavItem}>
                <div className={styles.NavTitle}>
                    {t('v3d.text.minPointSize')}: {sldPointSize.toFixed(2)}
                </div>
                <Slider
                    min={DFConfig.MinPointSize.min}
                    max={DFConfig.MinPointSize.max}
                    step={DFConfig.MinPointSize.step}
                    value={sldMinPointSize}
                    tipFormatter={(value: number) => value ? value.toFixed(2) : 0}
                    onChange={(value: number) => {
                        setSldMinPointSize(value);
                        material.minSize = value;
                    }}
                />
            </div>
            <div className={styles.NavItem}>
                <div className={styles.NavTitle}>
                    {t('v3d.text.pointSizeType')}
                </div>
                <Select
                    className={"w-full mt-2 ml-1"}
                    defaultValue={optPointSizing}
                    onChange={(value) => {
                        setOptPointSizing(value);
                        material.pointSizeType = value;
                    }}
                >
                    <Select.Option value={PointSizeType.FIXED} key={0}>FIXED</Select.Option>
                    <Select.Option value={PointSizeType.ATTENUATED} key={1}>ATTENUATED</Select.Option>
                    <Select.Option value={PointSizeType.ADAPTIVE} key={2}>ADAPTIVE</Select.Option>
                </Select>
            </div>
            <div className={styles.NavItem}>
                <div className={styles.NavTitle}>
                    {t('v3d.text.pointShape')}
                </div>
                <Select
                    className={"w-full my-2 ml-1"}
                    defaultValue={optShape}
                    onChange={(value) => {
                        setOptShape(value);
                        material.shape = value;
                    }}
                >
                    <Select.Option value={PointShape.SQUARE} key={0}>SQUARE</Select.Option>
                    <Select.Option value={PointShape.CIRCLE} key={1}>CIRCLE</Select.Option>
                    <Select.Option value={PointShape.PARABOLOID} key={2}>PARABOLOID</Select.Option>
                </Select>
            </div>
            <div className={styles.NavItem}>
                <div className={styles.NavTitle}>
                    {t('v3d.text.pointOpacity')}: {sldOpacity.toFixed(2)}
                </div>
                <Slider
                    min={DFConfig.PointOpacity.min}
                    max={DFConfig.PointOpacity.max}
                    step={DFConfig.PointOpacity.step}
                    value={sldOpacity}
                    tipFormatter={(value: number) => value ? value.toFixed(2) : 0}
                    onChange={(value: number) => {
                        setSldOpacity(value);
                        material.opacity = value;
                    }}
                />
            </div>
            {/* Materials */}
            <>
                <Divider
                    orientation="left"
                    style={{
                        fontSize: "small",
                        marginBottom: '0.5rem'
                    }}
                >{t('text.properties')}</Divider>
                <div className={styles.NavItem}>
                    <div className={styles.NavTitle}>
                        {t('v3d.text.pointMaterial')}
                    </div>
                    <Select
                        className={"w-full mt-2 ml-1"}
                        defaultValue={optMaterial}
                        onChange={onChangeOptMaterial}
                    >
                        {getSelectOptionMaterial}
                    </Select>
                </div>
                {getContainerMaterial()}
            </>
        </div>
    )
}
