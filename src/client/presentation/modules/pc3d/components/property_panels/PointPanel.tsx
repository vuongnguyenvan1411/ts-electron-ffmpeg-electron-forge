import {Viewer} from "../../viewer/viewer";
import {Measure} from "../../utils/Measure";
import {useTranslation} from "react-i18next";
import {useEffect, useState} from "react";
import {Color} from "../../../../../const/Color";
import {MeasureEvent} from "../../core/Event";
import {ParamTableCoord} from "../../core/Defines";
import {CoordinatesTable} from "./MeasurementPanel";
import {Button, message, Popconfirm, Table, Tooltip} from "antd";
import {DeleteOutlined} from "@ant-design/icons";
import styles from "../../styles/Viewer.module.scss";

type ParamSelectMeasure = {
    coord: ParamTableCoord[],
    rgba: string,
};

export const PointPanel = (props: {
    viewer: Viewer,
    measurement: Measure
}) => {
    const {t} = useTranslation();

    const [selectInfo, setSelectInfo] = useState<ParamSelectMeasure>();

    useEffect(() => {
        console.log('%cMount Panel: PointPanel', Color.ConsoleInfo, props.measurement);

        update();

        props.measurement.addEventListener(MeasureEvent.MarkerAdded, update);
        props.measurement.addEventListener(MeasureEvent.MarkerRemoved, update);
        props.measurement.addEventListener(MeasureEvent.MarkerMoved, update);

        return () => {
            console.log('%cUnmount Panel: PointPanel', Color.ConsoleInfo);

            props.measurement.removeEventListener(MeasureEvent.MarkerAdded, update);
            props.measurement.removeEventListener(MeasureEvent.MarkerRemoved, update);
            props.measurement.removeEventListener(MeasureEvent.MarkerMoved, update);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.measurement])

    const update = () => {
        const selectProfile: ParamSelectMeasure = {
            coord: [],
            rgba: ''
        };

        props.measurement.points.forEach(({position}, index) => {
            selectProfile.coord.push({
                x: position.x,
                y: position.y,
                z: position.z,
                key: index,
            });
        })

        const point = props.measurement.points[0];

        if (point.rgba) {
            selectProfile.rgba = point.rgba.join(', ')
        }

        setSelectInfo({...selectProfile});
    }

    const onClickDeleteMeasure = () => {
        props.viewer.scene.removeMeasurement(props.measurement);
    }

    return (
        selectInfo
            ? <>
                <CoordinatesTable
                    dataSource={selectInfo.coord}
                    name={props.measurement.name}
                />
                <Table
                    className={styles.TableMeasure}
                    size="small"
                    pagination={false}
                    dataSource={[
                        {
                            key: 1,
                            a: 'rgba',
                            b: selectInfo.rgba,
                        }
                    ]}
                    columns={[
                        {
                            dataIndex: 'a',
                            align: 'right' as 'right',
                        },
                        {
                            dataIndex: 'b',
                            align: 'left' as 'left',
                        },
                    ]}
                    bordered
                    showHeader={false}
                />
                <div className={"flex justify-end pr-2 pb-2"}>
                    <Popconfirm
                        title={t('text.confirmDeleteMeasure')}
                        onConfirm={() => {
                            onClickDeleteMeasure();
                            message.success(t('success.delete')).then();
                        }}
                        okText={t('text.yes')}
                        cancelText={t('text.no')}
                    >
                        <Tooltip title={t('button.delete')} placement="top">
                            <Button
                                type={"primary"}
                                size={"small"}
                                danger
                                icon={<DeleteOutlined/>}
                            />
                        </Tooltip>
                    </Popconfirm>
                </div>
            </>
            : null
    )
}
