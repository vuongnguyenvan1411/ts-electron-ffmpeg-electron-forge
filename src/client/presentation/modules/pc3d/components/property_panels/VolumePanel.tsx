import {Viewer} from "../../viewer/viewer";
import {useTranslation} from "react-i18next";
import {useEffect, useState} from "react";
import {CheckOutlined, CloseOutlined, CopyOutlined, DeleteOutlined, ToolOutlined} from "@ant-design/icons";
import {Button, Divider, message, notification, Popconfirm, Space, Switch, Table, Tooltip, Typography} from "antd";
import {MeasureEvent} from "../../core/Event";
import styles from "../../styles/Viewer.module.scss";
import {Color} from "../../../../../const/Color";
import {ParamTableCoord} from "../../core/Defines";
import {CoordinatesTable} from "./MeasurementPanel";
import {BoxVolume, SphereVolume} from "../../utils/Volume";
import {Utils as RootUtils} from "../../../../../core/Utils";
import {Utils} from "../../core/Utils";

type ParamTableAngle = {
    a: number,
    b: number,
    g: number,
    key?: number,
}
type ParamTableDimension = {
    l: number,
    w: number,
    h: number,
    key?: number,
}

type ParamSelectMeasure = {
    coord: ParamTableCoord[],
    angle: ParamTableAngle,
    dimension: ParamTableDimension,
    volume: number,
};

export const VolumePanel = (props: {
    viewer: Viewer,
    volume: BoxVolume | SphereVolume
}) => {
    const {t} = useTranslation();

    const [selectInfo, setSelectInfo] = useState<ParamSelectMeasure>();

    useEffect(() => {
        console.log('%cMount Panel: VolumePanel', Color.ConsoleInfo, props.volume);

        update();

        props.volume.addEventListener(MeasureEvent.PositionChanged, update);
        props.volume.addEventListener(MeasureEvent.OrientationChanged, update);
        props.volume.addEventListener(MeasureEvent.ScaleChanged, update);
        props.volume.addEventListener(MeasureEvent.ClipChanged, update);

        return () => {
            console.log('%cUnmount Panel: VolumePanel', Color.ConsoleInfo);

            props.volume.removeEventListener(MeasureEvent.PositionChanged, update);
            props.volume.removeEventListener(MeasureEvent.OrientationChanged, update);
            props.volume.removeEventListener(MeasureEvent.ScaleChanged, update);
            props.volume.removeEventListener(MeasureEvent.ClipChanged, update);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.volume])

    const update = () => {
        const r2v = props.volume.rotation;
        const angles = r2v.toArray().map(v => 180 * v / Math.PI);

        const dimensions = props.volume.scale.toArray();

        setSelectInfo({
            ...selectInfo,
            coord: [
                {
                    x: props.volume.position.x,
                    y: props.volume.position.y,
                    z: props.volume.position.z,
                    key: 1,
                }
            ],
            angle: {
                a: angles[0],
                b: angles[1],
                g: angles[2]
            },
            dimension: {
                l: dimensions[0],
                w: dimensions[1],
                h: dimensions[2],
            },
            volume: props.volume.getVolume()
        });
    }

    const onClickDeleteMeasure = () => {
        props.viewer.scene.removeVolume(props.volume);
    }

    const copyMsg = (msg: string) => {
        return (
            <Button
                type={'primary'}
                onClick={() => {
                    RootUtils
                        .copyClipboard(msg)
                        .then(() => {
                            notification.success({
                                message: t('text.successCopy'),
                                description: msg
                            });
                        });
                }}
                size={"small"}
            >
                <CopyOutlined/>
            </Button>
        )
    }

    return (
        selectInfo
            ? <>
                <CoordinatesTable
                    dataSource={selectInfo.coord}
                    name={props.volume.name}
                />
                <Table
                    className={styles.TableMeasure}
                    size="small"
                    pagination={false}
                    dataSource={[
                        {
                            key: 1,
                            a: selectInfo.angle.a,
                            b: selectInfo.angle.b,
                            g: selectInfo.angle.g,
                        }
                    ]}
                    columns={[
                        {
                            title: '\u03b1',
                            dataIndex: 'a',
                            key: 'a',
                            render: (value: number) => value.toFixed(1) + '\u00B0'
                        },
                        {
                            title: '\u03b2',
                            dataIndex: 'b',
                            key: 'b',
                            render: (value: number) => value.toFixed(1) + '\u00B0'
                        },
                        {
                            title: '\u03b3',
                            dataIndex: 'g',
                            key: 'g',
                            render: (value: number) => value.toFixed(1) + '\u00B0'
                        },
                        {
                            title: <ToolOutlined/>,
                            dataIndex: 'c',
                            key: 'c',
                            align: 'right' as 'right',
                            render: (_: string, record: ParamTableAngle) => copyMsg(`${record.a.toFixed(1)},${record.b.toFixed(1)},${record.g.toFixed(1)}`)
                        }
                    ]}
                />
                <Table
                    className={styles.TableMeasure}
                    size="small"
                    pagination={false}
                    dataSource={[
                        {
                            key: 1,
                            l: selectInfo.dimension.l,
                            w: selectInfo.dimension.w,
                            h: selectInfo.dimension.h,
                        }
                    ]}
                    columns={[
                        {
                            title: props.volume instanceof SphereVolume ? 'rx' : t('text.length'),
                            dataIndex: 'l',
                            key: 'l',
                            render: (value: number) => value.toFixed(1) + 'm'
                        },
                        {
                            title: props.volume instanceof SphereVolume ? 'ry' : t('text.width'),
                            dataIndex: 'w',
                            key: 'w',
                            render: (value: number) => value.toFixed(1) + 'm'
                        },
                        {
                            title: props.volume instanceof SphereVolume ? 'rz' : t('text.height'),
                            dataIndex: 'h',
                            key: 'h',
                            render: (value: number) => value.toFixed(1) + 'm'
                        },
                        {
                            title: <ToolOutlined/>,
                            dataIndex: 'c',
                            key: 'c',
                            align: 'right' as 'right',
                            render: (_: string, record: ParamTableDimension) => copyMsg(`${record.l.toFixed(1)},${record.w.toFixed(1)},${record.h.toFixed(1)}`)
                        }
                    ]}
                />
                <div className={"px-2 pb-2"}>
                    <div className={"mb-2"}>
                        <Typography.Text strong>{t('text.volume')}:</Typography.Text>&nbsp;{Utils.addCommas(selectInfo.volume.toFixed(2))}&nbsp;{'\u33A5'}
                    </div>
                    {
                        props.volume instanceof BoxVolume
                            ? <>
                                <div className={"mb-2"}>
                                    <Switch
                                        checkedChildren={<CheckOutlined/>}
                                        unCheckedChildren={<CloseOutlined/>}
                                        defaultChecked={props.volume.clip}
                                        onChange={(checked) => {
                                            props.volume.clip = checked;
                                            // props.volume.visible = checked;
                                        }}
                                    />
                                    <span className={"ml-2"}>{t('v3d.text.makeClipVolume')}</span>
                                </div>
                                <div className={"mb-2"}>
                                    <Switch
                                        checkedChildren={<CheckOutlined/>}
                                        unCheckedChildren={<CloseOutlined/>}
                                        defaultChecked={props.volume.visible}
                                        onChange={(checked) => {
                                            props.volume.visible = checked;
                                        }}
                                    />
                                    <span className={"ml-2"}>{t('v3d.text.showVolume')}</span>
                                </div>
                            </>
                            : null
                    }
                    <Space className={"flex justify-center mb-2 mt-3"}>
                        <Button
                            type={"primary"}
                            size={"small"}
                            onClick={() => props.volume.rotation.set(0, 0, 0)}
                        >
                            {t('v3d.text.resetOrientation')}
                        </Button>
                        <Button
                            type={"primary"}
                            size={"small"}
                            onClick={() => {
                                const mean = (props.volume.scale.x + props.volume.scale.y + props.volume.scale.z) / 3;
                                props.volume.scale.set(mean, mean, mean);
                            }}
                        >
                            {t('v3d.text.makeUniform')}
                        </Button>
                    </Space>
                    <Divider className={"my-1"}/>
                    <div className={"flex justify-end"}>
                        <Popconfirm
                            title={t('text.confirmDeleteMeasure')}
                            onConfirm={() => {
                                onClickDeleteMeasure();
                                message.success(t('success.delete')).then();
                            }}
                            okText={t('text.yes')}
                            cancelText={t('text.no')}
                        >
                            <Tooltip title={t('button.delete')} placement="top">
                                <Button
                                    type={"primary"}
                                    size={"small"}
                                    danger
                                    icon={<DeleteOutlined/>}
                                />
                            </Tooltip>
                        </Popconfirm>
                    </div>
                </div>
            </>
            : null
    )
}
