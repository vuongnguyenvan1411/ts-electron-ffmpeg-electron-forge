import {Viewer} from "../../viewer/viewer";
import {Measure} from "../../utils/Measure";
import {useTranslation} from "react-i18next";
import {useEffect, useState} from "react";
import {Color} from "../../../../../const/Color";
import {MeasureEvent} from "../../core/Event";
import {ParamTableCoord} from "../../core/Defines";
import {CoordinatesTable} from "./MeasurementPanel";
import {Button, message, Popconfirm, Tooltip, Typography} from "antd";
import {DeleteOutlined} from "@ant-design/icons";

type ParamSelectMeasure = {
    coord: ParamTableCoord[],
    height: string,
};

export const HeightPanel = (props: {
    viewer: Viewer,
    measurement: Measure
}) => {
    const {t} = useTranslation();

    const [selectInfo, setSelectInfo] = useState<ParamSelectMeasure>();

    useEffect(() => {
        console.log('%cMount Panel: HeightPanel', Color.ConsoleInfo, props.measurement);

        update();

        props.measurement.addEventListener(MeasureEvent.MarkerAdded, update);
        props.measurement.addEventListener(MeasureEvent.MarkerRemoved, update);
        props.measurement.addEventListener(MeasureEvent.MarkerMoved, update);

        return () => {
            console.log('%cUnmount Panel: HeightPanel', Color.ConsoleInfo);

            props.measurement.removeEventListener(MeasureEvent.MarkerAdded, update);
            props.measurement.removeEventListener(MeasureEvent.MarkerRemoved, update);
            props.measurement.removeEventListener(MeasureEvent.MarkerMoved, update);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.measurement])

    const update = () => {
        const selectProfile: ParamSelectMeasure = {
            coord: [],
            height: ''
        };

        props.measurement.points.forEach(({position}, index) => {
            selectProfile.coord.push({
                x: position.x,
                y: position.y,
                z: position.z,
                key: index,
            });
        })

        const points = props.measurement.points;
        const sorted = points.slice().sort((a, b) => a.position.z - b.position.z);
        const lowPoint = sorted[0].position.clone();
        const highPoint = sorted[sorted.length - 1].position.clone();
        const min = lowPoint.z;
        const max = highPoint.z;

        selectProfile.height = (max - min).toFixed(3);

        setSelectInfo({...selectProfile});
    }

    const onClickDeleteMeasure = () => {
        props.viewer.scene.removeMeasurement(props.measurement);
    }

    return (
        selectInfo
            ? <>
                <CoordinatesTable
                    dataSource={selectInfo.coord}
                    name={props.measurement.name}
                />
                <div className={"pl-2"}>
                    <Typography.Text strong>{t('text.height')}:</Typography.Text>&nbsp;{selectInfo.height}&nbsp;m
                </div>
                <div className={"flex justify-end pr-2 pb-2"}>
                    <Popconfirm
                        title={t('text.confirmDeleteMeasure')}
                        onConfirm={() => {
                            onClickDeleteMeasure();
                            message.success(t('success.delete')).then();
                        }}
                        okText={t('text.yes')}
                        cancelText={t('text.no')}
                    >
                        <Tooltip title={t('button.delete')} placement="top">
                            <Button
                                type={"primary"}
                                size={"small"}
                                danger
                                icon={<DeleteOutlined/>}
                            />
                        </Tooltip>
                    </Popconfirm>
                </div>
            </>
            : null
    )
}
