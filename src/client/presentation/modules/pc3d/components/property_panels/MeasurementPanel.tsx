import {Viewer} from "../../viewer/viewer";
import {Measure} from "../../utils/Measure";
import {Profile} from "../../utils/Profile";
import {BoxVolume, SphereVolume, Volume} from "../../utils/Volume";
import {DistancePanel} from "./DistancePanel";
import {AreaPanel} from "./AreaPanel";
import {PointPanel} from "./PointPanel";
import {AnglePanel} from "./AnglePanel";
import {HeightPanel} from "./HeightPanel";
import {CirclePanel} from "./CirclePanel";
import {ProfilePanel} from "./ProfilePanel";
import {VolumePanel} from "./VolumePanel";
import {useEffect} from "react";
import {Color} from "../../../../../const/Color";
import {Button, Divider, notification, Table} from "antd";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {farRulerTriangle} from "../../../../../const/FontAwesome";
import {useTranslation} from "react-i18next";
import {Breakpoint} from "antd/lib/_util/responsiveObserve";
import {Utils} from "../../core/Utils";
import {CopyOutlined, ToolOutlined} from "@ant-design/icons";
import {Utils as RootUtils} from "../../../../../core/Utils";
import {ParamTableCoord} from "../../core/Defines";
import scm from "../../../../../styles/module/Common.module.scss";

export const MeasurementPanel = (props: {
    viewer: Viewer,
    objectData: Measure | Profile | Volume
}) => {
    useEffect(() => {
        console.log('%cMount Panel: MeasurementPanel', Color.ConsoleInfo, props.objectData);

        return () => {
            console.log('%cUnmount Panel: MeasurementPanel', Color.ConsoleInfo);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.objectData])

    if (props.objectData instanceof Measure) {
        if (props.objectData.showDistances && !props.objectData.showArea && !props.objectData.showAngles) {
            return <DistancePanel viewer={props.viewer} measurement={props.objectData}/>;
        } else if (props.objectData.showDistances && props.objectData.showArea && !props.objectData.showAngles) {
            return <AreaPanel viewer={props.viewer} measurement={props.objectData}/>;
        } else if (props.objectData.maxMarkers === 1) {
            return <PointPanel viewer={props.viewer} measurement={props.objectData}/>;
        } else if (!props.objectData.showDistances && !props.objectData.showArea && props.objectData.showAngles) {
            return <AnglePanel viewer={props.viewer} measurement={props.objectData}/>;
        } else if (props.objectData.showHeight) {
            return <HeightPanel viewer={props.viewer} measurement={props.objectData}/>;
        } else if (props.objectData.showCircle) {
            return <CirclePanel viewer={props.viewer} measurement={props.objectData}/>;
        } else {
            return <PointPanel viewer={props.viewer} measurement={props.objectData}/>;
        }
    } else if (props.objectData instanceof Profile) {
        return <ProfilePanel viewer={props.viewer} profile={props.objectData}/>;
    } else if (props.objectData instanceof BoxVolume || props.objectData instanceof SphereVolume) {
        return <VolumePanel viewer={props.viewer} volume={props.objectData}/>;
    }

    return null;
}

export const CoordinatesTable = (props: {
    dataSource: ParamTableCoord[],
    name: string
}) => {
    const {t} = useTranslation();

    const columnInfoMeasure = [
        {
            title: 'x',
            dataIndex: 'x',
            key: 'x',
            responsive: ['xxl', 'xl', 'lg', 'md', 'sm', 'xs'] as Breakpoint[],
            render: (value: number) => Utils.addCommas(value.toFixed(3))
        },
        {
            title: 'y',
            dataIndex: 'y',
            key: 'y',
            responsive: ['xxl', 'xl', 'lg', 'md', 'sm', 'xs'] as Breakpoint[],
            render: (value: number) => Utils.addCommas(value.toFixed(3))
        },
        {
            title: 'z',
            dataIndex: 'z',
            key: 'z',
            responsive: ['xxl', 'xl', 'lg', 'md', 'sm', 'xs'] as Breakpoint[],
            render: (value: number) => Utils.addCommas(value.toFixed(3))
        },
        {
            title: <ToolOutlined/>,
            dataIndex: 'c',
            key: 'c',
            align: 'right' as 'right',
            render: (_: string, record: ParamTableCoord) => {
                return (
                    <Button
                        type={'primary'}
                        onClick={() => {
                            const msg = `${record.x},${record.y},${record.z}`;

                            RootUtils
                                .copyClipboard(msg)
                                .then(() => {
                                    notification.success({
                                        message: t('text.successCopy'),
                                        description: msg
                                    });
                                });
                        }}
                        size={"small"}
                    >
                        <CopyOutlined/>
                    </Button>
                )
            }
        }
    ];

    return (
        <Table
            className={scm.TableCoord}
            dataSource={props.dataSource}
            size="small"
            bordered={false}
            columns={columnInfoMeasure}
            pagination={false}
            title={() => (
                <Divider
                    orientation="left"
                    plain
                    style={{
                        marginTop: 0,
                        marginBottom: "0.5rem"
                    }}
                >
                    <FontAwesomeIcon icon={farRulerTriangle} className={"mr-1"}/>
                    {t('text.info')} - {props.name}
                </Divider>
            )}
        />
    )
}
