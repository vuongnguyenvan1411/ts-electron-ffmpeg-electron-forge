import {Viewer} from "../../viewer/viewer";
import {Measure} from "../../utils/Measure";
import {useTranslation} from "react-i18next";
import {useEffect, useState} from "react";
import {DeleteOutlined} from "@ant-design/icons";
import {Button, message, Popconfirm, Table, Tooltip, Typography} from "antd";
import {MeasureEvent} from "../../core/Event";
import styles from "../../styles/Viewer.module.scss";
import {Color} from "../../../../../const/Color";
import {ParamTableCoord} from "../../core/Defines";
import {CoordinatesTable} from "./MeasurementPanel";
import {Vector3} from "three";
import {Utils} from "../../core/Utils";

type ParamTableInfo = {
    center: Vector3,
    radius: number,
    perimeter: number,
}

type ParamSelectMeasure = {
    coord: ParamTableCoord[],
    info?: ParamTableInfo,
};

export const CirclePanel = (props: {
    viewer: Viewer,
    measurement: Measure
}) => {
    const {t} = useTranslation();

    const [selectInfo, setSelectInfo] = useState<ParamSelectMeasure>();

    useEffect(() => {
        console.log('%cMount Panel: CirclePanel', Color.ConsoleInfo, props.measurement);

        update();

        props.measurement.addEventListener(MeasureEvent.MarkerAdded, update);
        props.measurement.addEventListener(MeasureEvent.MarkerRemoved, update);
        props.measurement.addEventListener(MeasureEvent.MarkerMoved, update);

        return () => {
            console.log('%cUnmount Panel: CirclePanel', Color.ConsoleInfo);

            props.measurement.removeEventListener(MeasureEvent.MarkerAdded, update);
            props.measurement.removeEventListener(MeasureEvent.MarkerRemoved, update);
            props.measurement.removeEventListener(MeasureEvent.MarkerMoved, update);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.measurement])

    const update = () => {
        const selectProfile: ParamSelectMeasure = {
            coord: [],
        };

        props.measurement.points.forEach(({position}, index) => {
            selectProfile.coord.push({
                x: position.x,
                y: position.y,
                z: position.z,
                key: index,
            });
        })

        if (props.measurement.points.length === 3) {
            const A = props.measurement.points[0].position;
            const B = props.measurement.points[1].position;
            const C = props.measurement.points[2].position;

            const center = Utils.computeCircleCenter(A, B, C);
            const radius = center.distanceTo(A);
            const perimeter = 2 * Math.PI * radius;

            selectProfile.info = {
                center: center,
                radius: radius,
                perimeter: perimeter
            }
        }

        setSelectInfo({...selectProfile});
    }

    const onClickDeleteMeasure = () => {
        props.viewer.scene.removeMeasurement(props.measurement);
    }

    return (
        selectInfo
            ? <>
                <CoordinatesTable
                    dataSource={selectInfo.coord}
                    name={props.measurement.name}
                />
                {
                    selectInfo.info
                        ? <Table
                            className={styles.TableMeasure}
                            size="small"
                            pagination={false}
                            dataSource={[
                                {
                                    key: 1,
                                    a: <Typography.Text strong>{t('v3d.text.centerCoord')}:</Typography.Text>,
                                    b: `${selectInfo.info.center.x} ${selectInfo.info.center.y} ${selectInfo.info.center.z}`,
                                },
                                {
                                    key: 2,
                                    a: <Typography.Text strong>{t('v3d.text.radius')}:</Typography.Text>,
                                    b: selectInfo.info.radius.toFixed(3) + ' m',
                                },
                                {
                                    key: 3,
                                    a: <Typography.Text strong>{t('v3d.text.perimeter')}:</Typography.Text>,
                                    b: selectInfo.info.perimeter.toFixed(3) + ' m',
                                },
                            ]}
                            columns={[
                                {
                                    dataIndex: 'a',
                                    align: 'right' as 'right',
                                },
                                {
                                    dataIndex: 'b',
                                    align: 'left' as 'left',
                                },
                            ]}
                        />
                        : null
                }
                <div className={"flex justify-end pr-2 pb-2"}>
                    <Popconfirm
                        title={t('text.confirmDeleteMeasure')}
                        onConfirm={() => {
                            onClickDeleteMeasure();
                            message.success(t('success.delete')).then();
                        }}
                        okText={t('text.yes')}
                        cancelText={t('text.no')}
                    >
                        <Tooltip title={t('button.delete')} placement="top">
                            <Button
                                type={"primary"}
                                size={"small"}
                                danger
                                icon={<DeleteOutlined/>}
                            />
                        </Tooltip>
                    </Popconfirm>
                </div>
            </>
            : null
    )
}
