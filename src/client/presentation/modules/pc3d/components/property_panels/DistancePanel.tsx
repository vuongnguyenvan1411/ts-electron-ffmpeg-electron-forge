import {Viewer} from "../../viewer/viewer";
import {Measure} from "../../utils/Measure";
import {useTranslation} from "react-i18next";
import {useEffect, useState} from "react";
import {Color} from "../../../../../const/Color";
import {MeasureEvent} from "../../core/Event";
import {ParamTableCoord} from "../../core/Defines";
import {CoordinatesTable} from "./MeasurementPanel";
import {Button, message, Popconfirm, Table, Tooltip} from "antd";
import {DeleteOutlined, PlusCircleOutlined} from "@ant-design/icons";
import styles from "../../styles/Viewer.module.scss";
import {Profile} from "../../utils/Profile";

type ParamSelectMeasure = {
    coord: ParamTableCoord[],
    distance: {
        list: string[],
        total: string
    },
};

export const DistancePanel = (props: {
    viewer: Viewer,
    measurement: Measure
}) => {
    const {t} = useTranslation();

    const [selectInfo, setSelectInfo] = useState<ParamSelectMeasure>();

    useEffect(() => {
        console.log('%cMount Panel: DistancePanel', Color.ConsoleInfo, props.measurement);

        update();

        props.measurement.addEventListener(MeasureEvent.MarkerAdded, update);
        props.measurement.addEventListener(MeasureEvent.MarkerRemoved, update);
        props.measurement.addEventListener(MeasureEvent.MarkerMoved, update);

        return () => {
            console.log('%cUnmount Panel: DistancePanel', Color.ConsoleInfo);

            props.measurement.removeEventListener(MeasureEvent.MarkerAdded, update);
            props.measurement.removeEventListener(MeasureEvent.MarkerRemoved, update);
            props.measurement.removeEventListener(MeasureEvent.MarkerMoved, update);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.measurement])

    const update = () => {
        const selectProfile: ParamSelectMeasure = {
            coord: [],
            distance: {
                list: [],
                total: ''
            }
        };

        props.measurement.points.forEach(({position}, index) => {
            selectProfile.coord.push({
                x: position.x,
                y: position.y,
                z: position.z,
                key: index,
            });
        })

        const positions = props.measurement.points.map(p => p.position);

        const distances = [];

        for (let i = 0; i < positions.length - 1; i++) {
            const d = positions[i].distanceTo(positions[i + 1]);
            distances.push(d.toFixed(2));
        }

        selectProfile.distance = {
            list: distances,
            total: props.measurement.getTotalDistance().toFixed(3)
        }

        setSelectInfo({...selectProfile});
    }

    const onClickDeleteMeasure = () => {
        props.viewer.scene.removeMeasurement(props.measurement);
    }

    const onClickMakeProfile = () => {
        const profile = new Profile();

        profile.name = props.measurement.name;
        profile.width = props.measurement.getTotalDistance() / 50;

        for (const point of props.measurement.points) {
            profile.addMarker(point.position.clone());
        }

        props.viewer.scene.addProfile(profile);
    }

    return (
        selectInfo
            ? <>
                <CoordinatesTable
                    dataSource={selectInfo.coord}
                    name={props.measurement.name}
                />
                <Table
                    className={styles.TableMeasure}
                    size="small"
                    pagination={false}
                    dataSource={
                        selectInfo.distance.list.map((value, index) => ({
                            key: index,
                            a: index === 0 ? t('text.distance') : '',
                            b: value,
                        })).concat([
                            {
                                key: selectInfo.distance.list.length,
                                a: t('text.total'),
                                b: selectInfo.distance.total + ' m',
                            }
                        ])
                    }
                    columns={[
                        {
                            dataIndex: 'a',
                            align: 'right' as 'right',
                        },
                        {
                            dataIndex: 'b',
                            align: 'right' as 'right',
                        },
                    ]}
                    bordered
                    showHeader={false}
                />
                <div className={"flex justify-between pr-2 pb-2"}>
                    <Button
                        type={"primary"}
                        size={"small"}
                        icon={<PlusCircleOutlined/>}
                        onClick={onClickMakeProfile}
                    >
                        {t('v3d.text.profileFromMeasure')}
                    </Button>
                    <Popconfirm
                        title={t('text.confirmDeleteMeasure')}
                        onConfirm={() => {
                            onClickDeleteMeasure();
                            message.success(t('success.delete')).then();
                        }}
                        okText={t('text.yes')}
                        cancelText={t('text.no')}
                    >
                        <Tooltip title={t('button.delete')} placement="top">
                            <Button
                                type={"primary"}
                                size={"small"}
                                danger
                                icon={<DeleteOutlined/>}
                            />
                        </Tooltip>
                    </Popconfirm>
                </div>
            </>
            : null
    )
}
