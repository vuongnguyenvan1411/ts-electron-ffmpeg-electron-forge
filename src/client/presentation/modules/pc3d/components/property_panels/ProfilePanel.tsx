import {Viewer} from "../../viewer/viewer";
import {useTranslation} from "react-i18next";
import {useEffect, useState} from "react";
import {Color} from "../../../../../const/Color";
import {MeasureEvent} from "../../core/Event";
import {DFConfig, ParamTableCoord} from "../../core/Defines";
import {CoordinatesTable} from "./MeasurementPanel";
import {Button, Divider, InputNumber, message, notification, Popconfirm, Tooltip} from "antd";
import {DeleteOutlined, EyeOutlined} from "@ant-design/icons";
import {Profile} from "../../utils/Profile";

type ParamSelectMeasure = {
    coord: ParamTableCoord[],
};

export const ProfilePanel = (props: {
    viewer: Viewer,
    profile: Profile
}) => {
    const {t} = useTranslation();

    const [selectInfo, setSelectInfo] = useState<ParamSelectMeasure>();
    const [ipWidth, setIpWidth] = useState<number>(props.profile.getWidth());

    useEffect(() => {
        console.log('%cMount Panel: ProfilePanel', Color.ConsoleInfo, props.profile);

        update();

        props.profile.addEventListener(MeasureEvent.MarkerAdded, update);
        props.profile.addEventListener(MeasureEvent.MarkerRemoved, update);
        props.profile.addEventListener(MeasureEvent.MarkerMoved, update);
        props.profile.addEventListener(MeasureEvent.WidthChanged, onWidthChange);

        return () => {
            console.log('%cUnmount Panel: ProfilePanel', Color.ConsoleInfo);

            props.profile.removeEventListener(MeasureEvent.MarkerAdded, update);
            props.profile.removeEventListener(MeasureEvent.MarkerRemoved, update);
            props.profile.removeEventListener(MeasureEvent.MarkerMoved, update);
            props.profile.removeEventListener(MeasureEvent.WidthChanged, onWidthChange);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.profile])

    const update = () => {
        const selectProfile: ParamSelectMeasure = {
            coord: [],
        };

        props.profile.points.forEach((position, index) => {
            selectProfile.coord.push({
                x: position.x,
                y: position.y,
                z: position.z,
                key: index,
            });
        })

        setSelectInfo({...selectProfile});
    }

    const onWidthChange = (evt: any) => {
        setIpWidth(evt.width)
    }

    const onClickDeleteMeasure = () => {
        props.viewer.scene.removeProfile(props.profile);
    }

    return (
        selectInfo
            ? <>
                <CoordinatesTable
                    dataSource={selectInfo.coord}
                    name={props.profile.name}
                />
                <div className={'px-2 pb-3'}>
                    <InputNumber<number>
                        className={"w-full mb-1"}
                        min={DFConfig.ProfileWidth.min}
                        max={DFConfig.ProfileWidth.max}
                        step={DFConfig.ProfileWidth.step}
                        addonBefore={t('text.width')}
                        addonAfter="m"
                        size={"small"}
                        parser={value => {
                            if (value) {
                                return parseFloat(parseFloat(value).toFixed(2));
                            }

                            return 0.00;
                        }}
                        formatter={value => {
                            if (value) {
                                if (typeof value === "string") {
                                    value = parseFloat(value);
                                }

                                return (value as number).toFixed(2);
                            }

                            return '0.00';
                        }}
                        value={ipWidth}
                        onChange={(value: number) => {
                            props.profile.setWidth(value);
                        }}
                    />
                    <Divider className={"my-2"}/>
                    <div className={"flex justify-between"}>
                        <Button
                            type={"primary"}
                            size={"small"}
                            icon={<EyeOutlined/>}
                            onClick={() => {
                                // todo: dev
                                notification.info({
                                    message: 'Thông báo',
                                    description: 'Chức năng sắp được ra mắt',
                                })
                            }}
                        >
                            {t('v3d.text.show2dProfile')}
                        </Button>
                        <Popconfirm
                            title={t('text.confirmDeleteMeasure')}
                            onConfirm={() => {
                                onClickDeleteMeasure();
                                message.success(t('success.delete')).then();
                            }}
                            okText={t('text.yes')}
                            cancelText={t('text.no')}
                        >
                            <Tooltip title={t('button.delete')} placement="top">
                                <Button
                                    type={"primary"}
                                    size={"small"}
                                    danger
                                    icon={<DeleteOutlined/>}
                                />
                            </Tooltip>
                        </Popconfirm>
                    </div>
                </div>
            </>
            : null
    )
}
