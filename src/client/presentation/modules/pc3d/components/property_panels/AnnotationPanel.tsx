import {Viewer} from "../../viewer/viewer";
import {useTranslation} from "react-i18next";
import {BaseSyntheticEvent, useEffect, useState} from "react";
import {Color} from "../../../../../const/Color";
import {ParamTableCoord} from "../../core/Defines";
import {CoordinatesTable} from "./MeasurementPanel";
import {Button, Divider, Form, Input, message, Popconfirm, Tooltip} from "antd";
import {DeleteOutlined} from "@ant-design/icons";
import {Annotation} from "../../core/Annotation";

type ParamSelectAnnotation = {
    coord: ParamTableCoord[],
    title: string,
    description: string,
};

export const AnnotationPanel = (props: {
    viewer: Viewer,
    annotation: Annotation
}) => {
    const {t} = useTranslation();

    const [selectInfo, setSelectInfo] = useState<ParamSelectAnnotation>();

    useEffect(() => {
        console.log('%cMount Panel: AnnotationPanel', Color.ConsoleInfo, props.annotation);

        update();

        return () => {
            console.log('%cUnmount Panel: AnnotationPanel', Color.ConsoleInfo);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.annotation])

    const update = () => {
        setSelectInfo({
            ...selectInfo,
            coord: [
                {
                    x: props.annotation.position ? props.annotation.position.x : 0,
                    y: props.annotation.position ? props.annotation.position.y : 0,
                    z: props.annotation.position ? props.annotation.position.z : 0,
                    key: 1,
                }
            ],
            title: props.annotation.title,
            description: props.annotation.description,
        });
    }

    const onClickDeleteMeasure = () => {
        props.viewer.scene.removeAnnotation(props.annotation);
    }

    return (
        selectInfo
            ? <>
                <CoordinatesTable
                    dataSource={selectInfo.coord}
                    name={props.annotation.name}
                />
                <div className={"px-2 pb-3"}>
                    <Form
                        layout={"vertical"}
                    >
                        <Form.Item
                            label={t('text.title')}
                        >
                            <Input
                                allowClear
                                maxLength={50}
                                defaultValue={selectInfo.title}
                                onInput={(evt: BaseSyntheticEvent) => {
                                    props.annotation.title = evt.target.value;
                                }}
                            />
                        </Form.Item>
                        <Form.Item
                            label={t('text.description')}
                        >
                            <Input.TextArea
                                allowClear
                                maxLength={500}
                                showCount={true}
                                defaultValue={selectInfo.description}
                                onInput={(evt: BaseSyntheticEvent) => {
                                    props.annotation.description = evt.target.value;
                                }}
                                autoSize={{minRows: 2, maxRows: 6}}
                            />
                        </Form.Item>
                    </Form>
                    <Divider className={"my-2"}/>
                    <div className={"flex justify-end"}>
                        <Popconfirm
                            title={t('text.confirmDeleteAnnotation')}
                            onConfirm={() => {
                                onClickDeleteMeasure();
                                message.success(t('success.delete')).then();
                            }}
                            okText={t('text.yes')}
                            cancelText={t('text.no')}
                        >
                            <Tooltip title={t('button.delete')} placement="top">
                                <Button
                                    type={"primary"}
                                    size={"small"}
                                    danger
                                    icon={<DeleteOutlined/>}
                                />
                            </Tooltip>
                        </Popconfirm>
                    </div>
                </div>
            </>
            : null
    )
}
