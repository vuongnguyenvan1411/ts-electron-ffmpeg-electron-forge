import {Viewer} from "../../viewer/viewer";
import {Measure} from "../../utils/Measure";
import {useTranslation} from "react-i18next";
import {useEffect, useState} from "react";
import {DeleteOutlined} from "@ant-design/icons";
import {Button, message, Popconfirm, Table, Tooltip} from "antd";
import {MeasureEvent} from "../../core/Event";
import styles from "../../styles/Viewer.module.scss";
import {Color} from "../../../../../const/Color";
import {ParamTableCoord} from "../../core/Defines";
import {CoordinatesTable} from "./MeasurementPanel";

type ParamTableAngle = {
    a: number,
    b: number,
    g: number,
}

type ParamSelectMeasure = {
    coord: ParamTableCoord[],
    angle: ParamTableAngle,
};

export const AnglePanel = (props: {
    viewer: Viewer,
    measurement: Measure
}) => {
    const {t} = useTranslation();

    const [selectInfo, setSelectInfo] = useState<ParamSelectMeasure>();

    useEffect(() => {
        console.log('%cMount Panel: AnglePanel', Color.ConsoleInfo, props.measurement);

        update();

        props.measurement.addEventListener(MeasureEvent.MarkerAdded, update);
        props.measurement.addEventListener(MeasureEvent.MarkerRemoved, update);
        props.measurement.addEventListener(MeasureEvent.MarkerMoved, update);

        return () => {
            console.log('%cUnmount Panel: AnglePanel', Color.ConsoleInfo);

            props.measurement.removeEventListener(MeasureEvent.MarkerAdded, update);
            props.measurement.removeEventListener(MeasureEvent.MarkerRemoved, update);
            props.measurement.removeEventListener(MeasureEvent.MarkerMoved, update);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.measurement])

    const update = () => {
        const selectProfile: ParamSelectMeasure = {
            coord: [],
            angle: {
                a: 0,
                b: 0,
                g: 0,
            }
        };

        const angles: number[] = [];

        props.measurement.points.forEach(({position}, index) => {
            selectProfile.coord.push({
                x: position.x,
                y: position.y,
                z: position.z,
                key: index,
            });

            angles.push(props.measurement.getAngle(index) * (180.0 / Math.PI));
        })

        selectProfile.angle = {
            a: angles[0],
            b: angles[1],
            g: angles[2]
        };

        setSelectInfo({...selectProfile});
    }

    const onClickDeleteMeasure = () => {
        props.viewer.scene.removeMeasurement(props.measurement);
    }

    return (
        selectInfo
            ? <>
                <CoordinatesTable
                    dataSource={selectInfo.coord}
                    name={props.measurement.name}
                />
                <Table
                    className={styles.TableMeasure}
                    size="small"
                    pagination={false}
                    dataSource={[
                        {
                            key: 1,
                            a: (selectInfo.angle.a ? selectInfo.angle.a.toFixed(1) : (0).toFixed(1)) + '\u00B0',
                            b: (selectInfo.angle.b ? selectInfo.angle.b.toFixed(1) : (0).toFixed(1)) + '\u00B0',
                            g: (selectInfo.angle.g ? selectInfo.angle.g.toFixed(1) : (0).toFixed(1)) + '\u00B0',
                        }
                    ]}
                    columns={[
                        {
                            title: '\u03b1',
                            dataIndex: 'a',
                            key: 'a',
                        },
                        {
                            title: '\u03b2',
                            dataIndex: 'b',
                            key: 'b',
                        },
                        {
                            title: '\u03b3',
                            dataIndex: 'g',
                            key: 'g',
                        }
                    ]}
                />
                <div className={"flex justify-end pr-2 pb-2"}>
                    <Popconfirm
                        title={t('text.confirmDeleteMeasure')}
                        onConfirm={() => {
                            onClickDeleteMeasure();
                            message.success(t('success.delete')).then();
                        }}
                        okText={t('text.yes')}
                        cancelText={t('text.no')}
                    >
                        <Tooltip title={t('button.delete')} placement="top">
                            <Button
                                type={"primary"}
                                size={"small"}
                                danger
                                icon={<DeleteOutlined/>}
                            />
                        </Tooltip>
                    </Popconfirm>
                </div>
            </>
            : null
    )
}
