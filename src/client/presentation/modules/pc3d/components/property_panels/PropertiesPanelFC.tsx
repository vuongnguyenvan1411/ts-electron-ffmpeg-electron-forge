import {useEffect} from "react";
import {Viewer} from "../../viewer/viewer";
import {PointCloudOctree} from "../../core/PointCloudOctree";
import {Measure} from "../../utils/Measure";
import {Profile} from "../../utils/Profile";
import {BoxVolume, SphereVolume, Volume} from "../../utils/Volume";
import {PointCloudPanel} from "./PointCloudPanel";
import {MeasurementPanel} from "./MeasurementPanel";
import {Color} from "../../../../../const/Color";
import {CameraAnimation} from "../../modules/CameraAnimation/CameraAnimation";
import {CameraAnimationPanel} from "./CameraAnimationPanel";
import {Annotation} from "../../core/Annotation";
import {AnnotationPanel} from "./AnnotationPanel";

// let cleanupTasks: Function[] = [];
//
// const addVolatileListener = (target: EventDispatcher, type: string, callback: EventListener<any, any, any>) => {
//     target.addEventListener(type, callback);
//
//     cleanupTasks.push(() => {
//         target.removeEventListener(type, callback);
//     });
// }

export const PropertiesPanelFC = (props: {
    viewer: Viewer,
    object: any
}) => {
    useEffect(() => {
        console.log('%cMount Panel: PropertiesPanelFC', Color.ConsoleInfo, props.object);

        return () => {
            console.log('%cUnmount Panel: PropertiesPanelFC', Color.ConsoleInfo);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.object])

    // cleanupTasks.forEach((task) => task());
    // cleanupTasks = [];

    props.viewer.inputHandler.deselectAll();

    if (props.object instanceof PointCloudOctree) {
        return (
            <PointCloudPanel
                viewer={props.viewer}
                pointCloud={props.object}
            />
        )
    } else if (props.object instanceof Measure || props.object instanceof Profile || props.object instanceof Volume) {
        if (props.object instanceof BoxVolume || props.object instanceof SphereVolume) {
            props.viewer.inputHandler.toggleSelection(props.object);
        }

        return (
            <MeasurementPanel
                viewer={props.viewer}
                objectData={props.object}
            />
        )
    } else if (props.object instanceof CameraAnimation) {
        return (
            <CameraAnimationPanel
                viewer={props.viewer}
                animation={props.object}
            />
        )
    } else if (props.object instanceof Annotation) {
        return (
            <AnnotationPanel
                viewer={props.viewer}
                annotation={props.object}
            />
        )
    }

    // setCamera(camera: any) {
    //     let panel = new CameraPanel(this.viewer, this);
    //     this.container.append(panel.elContent);
    // }
    //
    // setAnnotation(annotation: any) {
    //     let panel = new AnnotationPanel(this.viewer, this, annotation);
    //     this.container.append(panel.elContent);
    // }
    //
    // setCameraAnimation(animation: any) {
    //     let panel = new CameraAnimationPanel(this.viewer, this, animation)
    //     this.container.append(panel.elContent);
    // }

    return null;
};
