import React, {useCallback, useEffect, useMemo, useState} from "react";
import EventEmitter from "eventemitter3";
import {useTranslation} from "react-i18next";
import {Viewer} from "../viewer/viewer";
import {Collapse, Divider, Radio, RadioChangeEvent, Slider, Space, Switch, Tree} from "antd";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {fabSketch, fadLayerGroup, falGlobe, farCameraMovie, farCctv, farCloud, farCommentsAlt} from "../../../../const/FontAwesome";
import {BackgroundType, CameraMode, ClipMethod, ClipTask, DFConfig, ETreeLayer, EViewerTool, TEventAnnotation, TEventCameraAnimation, TEventMeasurement, TEventPointCloud, TEventProfile, TEventVolume} from "../core/Defines";
import {Key} from "antd/es/table/interface";
import styles from "../styles/Viewer.module.scss";
import {CheckOutlined, CloseOutlined} from "@ant-design/icons";
import {Camera, Object3D, Vector2} from "three";
import {AnnotationEvent, ViewerEventName} from "../core/Event";
import {Utils} from "../core/Utils";
import {findKey, includes} from "lodash";
import {PropertiesPanelFC} from "./property_panels/PropertiesPanelFC";
import {Style} from "../../../../const/Style";
import Image from "next/image";
import IconMeasureAngle from "../../../../assets/image/v3d/measure_angle.png";
import IconPoint from "../../../../assets/image/v3d/point.svg";
import IconMeasureDistance from "../../../../assets/image/v3d/measure_distance.svg";
import IconMeasureHeight from "../../../../assets/image/v3d/measure_height.svg";
import IconMeasureCircle from "../../../../assets/image/v3d/measure_circle.svg";
import IconAzimuth from "../../../../assets/image/v3d/azimuth.svg";
import IconMeasureArea from "../../../../assets/image/v3d/measure_area.svg";
import IconMeasureVolume from "../../../../assets/image/v3d/measure_volume.svg";
import IconSphereVolume from "../../../../assets/image/v3d/sphere_volume.svg";
import IconProfileHeight from "../../../../assets/image/v3d/profile_height.svg";
import {PointCloudOctree} from "../core/PointCloudOctree";
import {IEDataNode, IOTreeCheckInfo} from "../const/Defines";

const speedRange = new Vector2(1, 10 * 1000);

const toLinearSpeed = (value: number) => {
    return Math.pow(value, 4) * speedRange.y + speedRange.x;
};

const toExpSpeed = (value: number) => {
    return Math.pow((value - speedRange.x) / speedRange.y, 1 / 4);
};

export const MapLayerFC = React.memo((props: {
    viewer: Viewer,
    event: EventEmitter
}) => {
    const {t} = useTranslation();

    const [isInit, setIsInit] = useState<boolean>(false);
    const [expandedKeys, setExpandedKeys] = useState<Key[]>([ETreeLayer.PointCloud]);
    const [checkedKeys, setCheckedKeys] = useState<Key[]>([]);
    const [selectedKeys, setSelectedKeys] = useState<Key[]>([]);
    const [treeData, setTreeData] = useState<IEDataNode[]>([]);

    const [selectInfo, setSelectInfo] = useState<{ object: any, uuid: string }>();

    const [rdBg, setRdBg] = useState<string>(props.viewer.getBackground());

    const [sldPointBudget, setSldPointBudget] = useState<number>(props.viewer.getPointBudget());
    const [sldFOV, setSldFOV] = useState<number>(props.viewer.getFOV());

    const [chkEDLEnabled, setChkEDLEnabled] = useState<boolean>(props.viewer.getEDLEnabled());
    const [sldEDLRadius, setSldEDLRadius] = useState<number>(props.viewer.getEDLRadius());
    const [sldEDLStrength, setSldEDLStrength] = useState<number>(props.viewer.getEDLStrength());
    const [sldEDLOpacity, setSldEDLOpacity] = useState<number>(props.viewer.getEDLOpacity());

    const [rdSqo, setRdSqo] = useState<string>(props.viewer.useHQ ? 'hq' : 'sq');
    const [sldMinNodeSize, setSldMinNodeSize] = useState<number>(props.viewer.getMinNodeSize());
    // const [chkShowBoundingBox, setChkShowBoundingBox] = useState<boolean>(props.viewer.getShowBoundingBox());
    // const [chkSetFreeze, setChkSetFreeze] = useState<boolean>(props.viewer.getFreeze());

    // const [chkMos, setChkMos] = useState<boolean>(props.viewer.measuringTool.showLabels);
    const [rdClto, setRdClto] = useState<number>(props.viewer.getClipTask());
    const [rdClmo, setRdClmo] = useState<number>(props.viewer.getClipMethod());
    const [rdCpo, setRdCpo] = useState<number>(props.viewer.getCameraMode());
    const [sldMoveSpeed, setSldMoveSpeed] = useState<number>(props.viewer.getMoveSpeed());

    useEffect(() => {
        const camera = new Camera();

        setTreeData([
            {
                title: t('v3d.text.pointCloud'),
                key: ETreeLayer.PointCloud,
                children: []
            },
            {
                title: t('v3d.text.layerMeasure'),
                key: ETreeLayer.Measure,
                children: []
            },
            {
                title: t('v3d.text.layerAnnotation'),
                key: ETreeLayer.Annotation,
                children: []
            },
            {
                title: t('v3d.text.layerOther'),
                key: ETreeLayer.Other,
                children: [
                    {
                        title: 'Camera',
                        key: `${ETreeLayer.Other}_${camera.uuid}`,
                        icon: <FontAwesomeIcon icon={farCctv}/>,
                        object: camera
                    }
                ]
            },
        ])

        setCheckedKeys([
            ETreeLayer.PointCloud,
            ETreeLayer.Measure,
            ETreeLayer.Annotation,
            ETreeLayer.Other,
        ]);

        setIsInit(true);

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    // event
    useEffect(() => {
        if (isInit) {
            // MoveSpeedChanged
            const onMoveSpeedChanged = () => {
                const value = parseFloat(props.viewer.getMoveSpeed().toString());

                setSldMoveSpeed(toExpSpeed(value));
            }

            props.viewer.addEventListener(ViewerEventName.MoveSpeedChanged, onMoveSpeedChanged);

            // PointCloud
            const onPointCloudAdded = (evt: TEventPointCloud) => {
                console.log('onPointCloudAdded', evt);

                _createTreeNode(evt.pointcloud, ETreeLayer.PointCloud);
            }

            // Measurement
            const onMeasurementAdded = (evt: TEventMeasurement) => {
                console.log('onMeasurementAdded', evt);

                _createTreeNode(evt.measurement, ETreeLayer.Measure);
            }

            const onMeasurementRemoved = (evt: TEventMeasurement) => {
                console.log('onMeasurementRemoved', evt);

                _removeTreeNode(evt.measurement.uuid, ETreeLayer.Measure);
            }

            // Volume
            const onVolumeAdded = (evt: TEventVolume) => {
                console.log('onVolumeAdded', evt);

                _createTreeNode(evt.volume, ETreeLayer.Measure);
            }

            const onVolumeRemoved = (evt: TEventVolume) => {
                console.log('onVolumeRemoved', evt);

                _removeTreeNode(evt.volume.uuid, ETreeLayer.Measure);
            }

            // Profile
            const onProfileAdded = (evt: TEventProfile) => {
                console.log('onProfileAdded', evt);

                _createTreeNode(evt.profile, ETreeLayer.Measure);
            }

            const onProfileRemoved = (evt: TEventProfile) => {
                console.log('onProfileRemoved', evt);

                _removeTreeNode(evt.profile.uuid, ETreeLayer.Measure);
            }

            // CameraAnimation
            const onCameraAnimationAdded = (evt: TEventCameraAnimation) => {
                console.log('onCameraAnimationAdded', evt);

                _createTreeNode(evt.animation, ETreeLayer.Other);
            }

            const onCameraAnimationRemoved = (evt: TEventCameraAnimation) => {
                console.log('onCameraAnimationRemoved', evt);

                _removeTreeNode(evt.animation.uuid, ETreeLayer.Other);
            }

            // Annotation
            const onAnnotationAdded = (evt: TEventAnnotation) => {
                console.log('onAnnotationAdded', evt);

                _createTreeNode(evt.annotation, ETreeLayer.Annotation);
            }

            const onAnnotationRemoved = (evt: TEventAnnotation) => {
                console.log('onAnnotationRemoved', evt);

                _removeTreeNode(evt.annotation.uuid, ETreeLayer.Annotation);
            }

            props.viewer.scene.addEventListener(ViewerEventName.PointCloudAdded, onPointCloudAdded);

            props.viewer.scene.addEventListener(ViewerEventName.MeasurementAdded, onMeasurementAdded);
            props.viewer.scene.addEventListener(ViewerEventName.MeasurementRemoved, onMeasurementRemoved);

            props.viewer.scene.addEventListener(ViewerEventName.VolumeAdded, onVolumeAdded);
            props.viewer.scene.addEventListener(ViewerEventName.VolumeRemoved, onVolumeRemoved);

            props.viewer.scene.addEventListener(ViewerEventName.ProfileAdded, onProfileAdded);
            props.viewer.scene.addEventListener(ViewerEventName.ProfileRemoved, onProfileRemoved);

            props.viewer.scene.addEventListener(ViewerEventName.CameraAnimationAdded, onCameraAnimationAdded);
            props.viewer.scene.addEventListener(ViewerEventName.CameraAnimationRemoved, onCameraAnimationRemoved);

            props.viewer.scene.annotations.addEventListener(AnnotationEvent.AnnotationAdded, onAnnotationAdded);
            props.viewer.scene.annotations.addEventListener(AnnotationEvent.AnnotationRemoved, onAnnotationRemoved);

            return () => {
                props.viewer.removeEventListener(ViewerEventName.MoveSpeedChanged, onMoveSpeedChanged);

                props.viewer.scene.removeEventListener(ViewerEventName.PointCloudAdded, onPointCloudAdded);

                props.viewer.scene.removeEventListener(ViewerEventName.MeasurementAdded, onMeasurementAdded);
                props.viewer.scene.removeEventListener(ViewerEventName.MeasurementRemoved, onMeasurementRemoved);

                props.viewer.scene.removeEventListener(ViewerEventName.VolumeAdded, onVolumeAdded);
                props.viewer.scene.removeEventListener(ViewerEventName.VolumeRemoved, onVolumeRemoved);

                props.viewer.scene.removeEventListener(ViewerEventName.ProfileAdded, onProfileAdded);
                props.viewer.scene.removeEventListener(ViewerEventName.ProfileRemoved, onProfileRemoved);

                props.viewer.scene.removeEventListener(ViewerEventName.CameraAnimationAdded, onCameraAnimationAdded);
                props.viewer.scene.removeEventListener(ViewerEventName.CameraAnimationRemoved, onCameraAnimationRemoved);

                props.viewer.scene.annotations.removeEventListener(AnnotationEvent.AnnotationAdded, onAnnotationAdded);
            }
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isInit]);

    const _createTreeNode = (object: any, layer: ETreeLayer) => {
        const id = object.uuid;

        const newData: IEDataNode[] = treeData;

        const keyTree = findKey(treeData, ['key', layer]);

        if (keyTree && newData[parseInt(keyTree)].children) {
            let icon = null;

            if (object instanceof PointCloudOctree) {
                icon = <FontAwesomeIcon icon={farCloud}/>;
            } else {
                switch (object.get('type')) {
                    // Measure
                    case EViewerTool.MeasureAngle:
                        icon = <Image alt={""} src={IconMeasureAngle} className={"h-3.5 mr-1"}/>;

                        break;
                    case EViewerTool.Point:
                        icon = <Image alt={""} src={IconPoint} className={"h-3.5 mr-1"}/>;

                        break;
                    case EViewerTool.MeasureDistance:
                        icon = <Image alt={""} src={IconMeasureDistance} className={"h-3.5 mr-1"}/>;

                        break;
                    case EViewerTool.MeasureHeight:
                        icon = <Image alt={""} src={IconMeasureHeight} className={"h-3.5 mr-1"}/>;

                        break;
                    case EViewerTool.MeasureCircle:
                        icon = <Image alt={""} src={IconMeasureCircle} className={"h-3.5 mr-1"}/>;

                        break;
                    case EViewerTool.MeasureAzimuth:
                        icon = <Image alt={""} src={IconAzimuth} className={"h-3.5 mr-1"}/>;

                        break;
                    case EViewerTool.MeasureArea:
                        icon = <Image alt={""} src={IconMeasureArea} className={"h-3.5 mr-1"}/>;

                        break;
                    // Volume
                    case EViewerTool.MeasureBoxVolume:
                        icon = <Image alt={""} src={IconMeasureVolume} className={"h-3.5 mr-1"}/>;

                        break;
                    case EViewerTool.MeasureSphereVolume:
                        icon = <Image alt={""} src={IconSphereVolume} className={"h-3.5 mr-1"}/>;

                        break;
                    // Profile
                    case EViewerTool.Profile:
                        icon = <Image alt={""} src={IconProfileHeight} className={"h-3.5 mr-1"}/>;

                        break;
                    // CameraAnimation
                    case EViewerTool.Animation:
                        icon = <FontAwesomeIcon icon={farCameraMovie}/>;

                        break;
                    // Annotation
                    case EViewerTool.Annotation:
                        icon = <FontAwesomeIcon icon={farCommentsAlt}/>;

                        break;
                }
            }

            const key = `${layer}_${id}`;

            newData[parseInt(keyTree)].children!.push({
                title: object.name,
                key: key,
                icon: icon,
                object: object
            });

            // Active Tree
            if (!(object instanceof PointCloudOctree)) {
                setSelectedKeys([key]);
                setSelectInfo({
                    ...selectInfo,
                    object: object,
                    uuid: id
                });

                if (!includes(expandedKeys, layer)) {
                    const newKeys = expandedKeys;
                    newKeys.push(layer);
                    setExpandedKeys([...newKeys]);
                }
            }

            setTreeData([...newData]);
        }
    }

    const _removeTreeNode = (id: string, layer: ETreeLayer) => {
        const keyTree = findKey(treeData, ['key', layer]);

        const newData: IEDataNode[] = treeData;

        if (keyTree) {
            let newC = newData[parseInt(keyTree)].children;

            if (newC && newC.length > 0) {
                newC = newC.filter(value => value.key !== `${layer}_${id}`);

                newData[parseInt(keyTree)].children = [...newC];

                setTreeData([...newData]);
                setSelectInfo(undefined);
            }
        }
    }

    const onChangeBackground = (evt: RadioChangeEvent) => {
        const value = evt.target.value;

        if (!props.viewer.isMap && value === BackgroundType.Map) {
            return;
        }

        setRdBg(value);
        props.viewer.setBackground(value);
    };

    const onTreeExpand = useCallback((keys: Key[]) => {
        setExpandedKeys(keys);

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onTreeCheck = useCallback((keys: Key[], info: IOTreeCheckInfo) => {
        const split = info.node.key.toString().split('_');

        if (split.length === 1) {
            if (info.node.children && info.node.children.length > 0) {
                const checked = !info.node.checked;

                info.node.children.forEach((value: IEDataNode) => {
                    if (value.object) {
                        value.object.visible = checked;
                    }
                })
            }
        } else if (split.length === 2) {
            const object: Object3D = (info.node as any).object;

            if (object) {
                object.visible = !info.node.checked;
            }
        }

        setCheckedKeys([...keys]);

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onTreeSelect = useCallback((keys: Key[], info) => {
        const split = info.node.key.toString().split('_');

        if (split.length === 1) {
            setSelectInfo(undefined);
        } else if (split.length === 2) {
            const selected = !info.node.selected;

            if (!selected) {
                setSelectInfo(undefined);
            } else {
                const object: Object3D = (info.node as any).object;

                if (object) {
                    setSelectInfo({
                        ...selectInfo,
                        object: object,
                        uuid: object.uuid
                    })
                }
            }
        }

        setSelectedKeys([...keys]);

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const getPropertiesPanel = useMemo(() => {
        if (selectInfo) {
            return (
                <PropertiesPanelFC
                    viewer={props.viewer}
                    object={selectInfo.object}
                />
            )
        } else {
            return null;
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selectInfo])

    return (
        isInit
            ? (
                <Collapse
                    className={styles.CollapseMenu}
                    defaultActiveKey={['lm', 'tv']}
                    expandIconPosition={"end"}
                >
                    <Collapse.Panel
                        key="bm"
                        header={
                            <>
                                <FontAwesomeIcon icon={falGlobe} className={"mr-2"}/>
                                {t('v3d.text.background')}
                            </>
                        }
                    >
                        <Radio.Group
                            key={"s1"}
                            value={rdBg}
                            buttonStyle="outline"
                            onChange={onChangeBackground}
                        >
                            <Space direction="vertical">
                                {
                                    Object.keys(BackgroundType).map((key, index) => {
                                        let title = BackgroundType[key];

                                        switch (title) {
                                            case BackgroundType.Map:
                                                title = t('v3d.text.map');

                                                break;
                                            case BackgroundType.Skybox:
                                                title = t('v3d.text.skybox');

                                                break;
                                            case BackgroundType.Gradient:
                                                title = t('v3d.text.gradient');

                                                break;
                                            case BackgroundType.Black:
                                                title = t('v3d.text.black');

                                                break;
                                            case BackgroundType.White:
                                                title = t('v3d.text.white');

                                                break;
                                        }

                                        return <Radio key={index} value={BackgroundType[key]}>{title}</Radio>
                                    })
                                }
                            </Space>
                        </Radio.Group>
                    </Collapse.Panel>
                    <Collapse.Panel
                        className={styles.CollapseMenuPanel0}
                        key="lm"
                        header={
                            <>
                                <FontAwesomeIcon icon={fadLayerGroup} className={"mr-2"}/>
                                {t('v3d.text.layer')}
                            </>
                        }
                    >
                        <Tree
                            key={"s21"}
                            autoExpandParent={false}
                            checkable
                            expandedKeys={expandedKeys}
                            checkedKeys={checkedKeys}
                            selectedKeys={selectedKeys}
                            onExpand={onTreeExpand}
                            onCheck={onTreeCheck as unknown as any}
                            onSelect={onTreeSelect}
                            treeData={treeData}
                            showIcon={true}
                        />
                        {getPropertiesPanel}
                    </Collapse.Panel>
                    <Collapse.Panel
                        key="tv"
                        header={
                            <>
                                <FontAwesomeIcon icon={fabSketch} className={"mr-2"}/>
                                {t('text.theme')}
                            </>
                        }
                    >
                        <div className={styles.NavItem}>
                            <div
                                className={styles.NavTitle}
                                style={{
                                    marginTop: 0
                                }}
                            >
                                {t('v3d.text.numberOfPoints')}: {Utils.addCommas(sldPointBudget)}
                            </div>
                            <Slider
                                min={DFConfig.PointBudget.min}
                                max={DFConfig.PointBudget.max}
                                step={DFConfig.PointBudget.step}
                                value={sldPointBudget}
                                tipFormatter={(value) => value ? `${Utils.addCommas(value)} point` : 0}
                                onChange={(value: number) => {
                                    setSldPointBudget(value);
                                    props.viewer.setPointBudget(value);
                                }}
                            />
                        </div>
                        <div className={styles.NavItem}>
                            <div className={styles.NavTitle}>
                                {t('v3d.text.filedOfView')}: {sldFOV}
                            </div>
                            <Slider
                                min={DFConfig.FOV.min}
                                max={DFConfig.FOV.max}
                                step={DFConfig.FOV.step}
                                value={sldFOV}
                                tipFormatter={(value) => value ?? 0}
                                onChange={(value: number) => {
                                    setSldFOV(value);
                                    props.viewer.setFOV(value);
                                }}
                            />
                        </div>
                        <Divider
                            orientation="left"
                            style={{
                                fontSize: "small",
                                marginBottom: '0.5rem'
                            }}
                        >
                            {t('v3d.text.edl')}
                        </Divider>
                        <div className={"pb-2 pl-1"}>
                            <Switch
                                checkedChildren={<CheckOutlined/>}
                                unCheckedChildren={<CloseOutlined/>}
                                checked={chkEDLEnabled}
                                onChange={(checked) => {
                                    setChkEDLEnabled(checked);
                                    props.viewer.setEDLEnabled(checked);
                                }}
                            />
                            <span className={"ml-2"}>{t('text.enabled')}</span>
                        </div>
                        <div className={styles.NavItem}>
                            <div className={styles.NavTitle}>
                                {t('v3d.text.edlRadius')}: {sldEDLRadius.toFixed(2)}
                            </div>
                            <Slider
                                disabled={!chkEDLEnabled}
                                min={DFConfig.EDLRadius.min}
                                max={DFConfig.EDLRadius.max}
                                step={DFConfig.EDLRadius.step}
                                value={sldEDLRadius}
                                tipFormatter={(value) => value ?? 0}
                                onChange={(value: number) => {
                                    setSldEDLRadius(value);
                                    props.viewer.setEDLRadius(value);
                                }}
                            />
                        </div>
                        <div className={styles.NavItem}>
                            <div className={styles.NavTitle}>
                                {t('v3d.text.strength')}: {sldEDLStrength.toFixed(2)}
                            </div>
                            <Slider
                                disabled={!chkEDLEnabled}
                                min={DFConfig.EDLStrength.min}
                                max={DFConfig.EDLStrength.max}
                                step={DFConfig.EDLStrength.step}
                                value={sldEDLStrength}
                                tipFormatter={(value) => value ?? 0}
                                onChange={(value: number) => {
                                    setSldEDLStrength(value);
                                    props.viewer.setEDLStrength(value);
                                }}
                            />
                        </div>
                        <div className={styles.NavItem}>
                            <div className={styles.NavTitle}>
                                {t('v3d.text.opacity')}: {sldEDLOpacity.toFixed(2)}
                            </div>
                            <Slider
                                disabled={!chkEDLEnabled}
                                min={DFConfig.EDLOpacity.min}
                                max={DFConfig.EDLOpacity.max}
                                step={DFConfig.EDLOpacity.step}
                                value={sldEDLOpacity}
                                tipFormatter={(value) => value ?? 0}
                                onChange={(value: number) => {
                                    setSldEDLOpacity(value);
                                    props.viewer.setEDLOpacity(value);
                                }}
                            />
                        </div>
                        <Divider
                            orientation="left"
                            style={{
                                fontSize: "small",
                                marginBottom: '0.5rem'
                            }}
                        >{t('v3d.text.pointQuantity')}</Divider>
                        <div className={"pl-1 mb-2"}>
                            <Radio.Group
                                className={"flex w-full"}
                                options={[
                                    {
                                        label: t('v3d.text.standardQuality'),
                                        value: 'sq',
                                        style: Style.FlexRadioOption
                                    },
                                    {
                                        label: t('v3d.text.highQuality'),
                                        value: 'hq',
                                        style: Style.FlexRadioOption
                                    },
                                ]}
                                onChange={(e) => {
                                    setRdSqo(e.target.value);
                                    props.viewer.useHQ = e.target.value === 'hq';
                                }}
                                value={rdSqo}
                                optionType="button"
                                buttonStyle="solid"
                                size={"small"}
                            />
                        </div>
                        <div className={styles.NavItem}>
                            <div className={styles.NavTitle}>
                                {t('v3d.text.minNodeSize')}: {sldMinNodeSize.toFixed(1)}
                            </div>
                            <Slider
                                min={DFConfig.MinNodeSize.min}
                                max={DFConfig.MinNodeSize.max}
                                step={DFConfig.MinNodeSize.step}
                                value={sldMinNodeSize}
                                tipFormatter={(value) => value ?? 0}
                                onChange={(value: number) => {
                                    setSldMinNodeSize(value);
                                    props.viewer.setMinNodeSize(value);
                                }}
                            />
                        </div>
                        <div className={"pb-2 pl-1"}>
                            <Switch
                                checkedChildren={<CheckOutlined/>}
                                unCheckedChildren={<CloseOutlined/>}
                                defaultChecked={props.viewer.getShowBoundingBox()}
                                onChange={(checked) => {
                                    // setChkShowBoundingBox(checked);
                                    props.viewer.setShowBoundingBox(checked);
                                }}
                            />
                            <span className={"ml-2"}>{t('v3d.text.showBoundingBox')}</span>
                        </div>
                        <div className={"pb-2 pl-1"}>
                            <Switch
                                checkedChildren={<CheckOutlined/>}
                                unCheckedChildren={<CloseOutlined/>}
                                defaultChecked={props.viewer.getFreeze()}
                                onChange={(checked) => {
                                    // setChkSetFreeze(checked);
                                    props.viewer.setFreeze(checked);
                                }}
                            />
                            <span className={"ml-2"}>{t('v3d.text.lockView')}</span>
                        </div>
                        <Divider
                            orientation="left"
                            style={{
                                fontSize: "small",
                                marginBottom: '0.5rem'
                            }}
                        >{t('v3d.text.measurements')}</Divider>
                        <div className={"pb-2 pl-1"}>
                            <Switch
                                checkedChildren={t('text.show')}
                                unCheckedChildren={t('text.hide')}
                                defaultChecked={props.viewer.measuringTool.showLabels}
                                onChange={(checked) => {
                                    // setChkMos(checked);
                                    props.viewer.measuringTool.showLabels = checked
                                }}
                            />
                            <span className={"ml-2"}>{t('v3d.text.showHideLabel')}</span>
                        </div>
                        <Divider
                            orientation="left"
                            style={{
                                fontSize: "small",
                                marginBottom: '0.5rem'
                            }}
                        >{t('v3d.text.clipping')}</Divider>
                        <div className={styles.NavItem}>
                            <div className={styles.NavTitle}>
                                {t('v3d.text.clipTask')}
                            </div>
                            <Radio.Group
                                className={"pl-1 flex w-full mt-1 mb-2"}
                                options={[
                                    {
                                        label: t('v3d.text.cltoNone'),
                                        value: ClipTask.NONE,
                                        style: Style.FlexRadioOption
                                    },
                                    {
                                        label: t('v3d.text.cltoHighlight'),
                                        value: ClipTask.HIGHLIGHT,
                                        style: Style.FlexRadioOption
                                    },
                                    {
                                        label: t('v3d.text.cltoInside'),
                                        value: ClipTask.SHOW_INSIDE,
                                        style: Style.FlexRadioOption
                                    },
                                    {
                                        label: t('v3d.text.cltoOutside'),
                                        value: ClipTask.SHOW_OUTSIDE,
                                        style: Style.FlexRadioOption
                                    },
                                ]}
                                onChange={(e) => {
                                    setRdClto(e.target.value);
                                    props.viewer.setClipTask(e.target.value);
                                }}
                                value={rdClto}
                                optionType="button"
                                buttonStyle="solid"
                                size={"small"}
                            />
                        </div>
                        <div className={styles.NavItem}>
                            <div className={styles.NavTitle}>
                                {t('v3d.text.clipMethod')}
                            </div>
                            <Radio.Group
                                className={"pl-1 flex w-full mt-1"}
                                options={[
                                    {
                                        label: t('v3d.text.clmoInsideAny'),
                                        value: ClipMethod.INSIDE_ANY,
                                        style: Style.FlexRadioOption
                                    },
                                    {
                                        label: t('v3d.text.clmoInsideAll'),
                                        value: ClipMethod.INSIDE_ALL,
                                        style: Style.FlexRadioOption
                                    },
                                ]}
                                onChange={(e) => {
                                    setRdClmo(e.target.value);
                                    props.viewer.setClipMethod(e.target.value);
                                }}
                                value={rdClmo}
                                optionType="button"
                                buttonStyle="solid"
                                size={"small"}
                            />
                        </div>
                        <Divider
                            orientation="left"
                            style={{
                                fontSize: "small",
                                marginBottom: '0.5rem'
                            }}
                        >{t('v3d.text.navigation')}</Divider>
                        <div className={styles.NavItem}>
                            <div className={styles.NavTitle}>
                                {t('v3d.text.cameraProjection')}
                            </div>
                            <Radio.Group
                                className={"pl-1 flex w-full mt-1 mb-2"}
                                options={[
                                    {
                                        label: t('v3d.text.cpoPerspective'),
                                        value: CameraMode.PERSPECTIVE,
                                        style: Style.FlexRadioOption
                                    },
                                    {
                                        label: t('v3d.text.cpoOrthographic'),
                                        value: CameraMode.ORTHOGRAPHIC,
                                        style: Style.FlexRadioOption
                                    }
                                ]}
                                onChange={(e) => {
                                    setRdCpo(e.target.value);
                                    props.viewer.setCameraMode(e.target.value);
                                }}
                                value={rdCpo}
                                optionType="button"
                                buttonStyle="solid"
                                size={"small"}
                            />
                        </div>
                        <div className={styles.NavItem}>
                            <div className={styles.NavTitle}>
                                {t('v3d.text.moveSpeed')}: {props.viewer.getMoveSpeed().toFixed(1)}
                            </div>
                            <Slider
                                min={DFConfig.MoveSpeed.min}
                                max={DFConfig.MoveSpeed.max}
                                step={DFConfig.MoveSpeed.step}
                                value={sldMoveSpeed}
                                tipFormatter={(value) => value ?? 0}
                                onChange={(value: number) => {
                                    props.viewer.setMoveSpeed(toLinearSpeed(value));
                                }}
                            />
                        </div>
                    </Collapse.Panel>
                </Collapse>
            )
            : null
    )
})
