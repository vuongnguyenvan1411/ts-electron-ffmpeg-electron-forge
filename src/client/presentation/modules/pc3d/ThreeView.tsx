import "./require";
import React, {useEffect, useMemo, useRef, useState} from "react";
import {Map3DModel} from "../../../models/service/geodetic/Map3DModel";
import proj4 from "proj4";
import {Cartesian3, Math as CesiumMath, OpenStreetMapImageryProvider, ShadowMode, Viewer as CesiumViewer} from "cesium";
import {Viewer} from "./viewer/viewer";
import {isMobile} from 'react-device-detect';
import {Main} from "./Main";
import EventEmitter from "eventemitter3";
import styles from "./styles/Viewer.module.scss";
import {App} from "./core/App";
import {Config} from "./const/Config";
import {Color} from "../../../const/Color";
import {TParamPartGeodetic} from "../../../const/Types";

type TArgs = {
    csMap: CesiumViewer;
    viewer: Viewer;
}

const ThreeView = React.memo((props: {
    m3dId: number,
    item: Map3DModel,
    parts?: TParamPartGeodetic,
}) => {
    const divViewerRef = useRef<HTMLDivElement>(null);
    const divCSMapRef = useRef<HTMLDivElement>(null);

    const divMasterLayerHeader = document.getElementById('MasterLayerHeader');
    const headerHeightRef = useRef<number>(divMasterLayerHeader ? divMasterLayerHeader.offsetHeight : 0);

    const [args, setArgs] = useState<TArgs>();

    useEffect(() => {
        console.log('%cMount Screen: ThreeView', Color.ConsoleInfo);

        window.CESIUM_BASE_URL = App.getAsset('csm');

        console.log('window.CESIUM_BASE_URL', window.CESIUM_BASE_URL);

        const mapProjection: any = proj4.defs("WGS84");

        window.toMap = proj4(Config.PointCloudProjection, mapProjection);
        window.toScene = proj4(mapProjection, Config.PointCloudProjection);

        // create cesium map
        const cesiumViewer = new CesiumViewer(divCSMapRef.current!, {
            requestRenderMode: true,
            useDefaultRenderLoop: false,
            animation: false,
            baseLayerPicker: false,
            fullscreenButton: false,
            geocoder: false,
            homeButton: false,
            infoBox: false,
            sceneModePicker: false,
            selectionIndicator: false,
            timeline: false,
            navigationHelpButton: false,
            imageryProvider: new OpenStreetMapImageryProvider({
                url: 'https://a.tile.openstreetmap.org/'
            }),
            terrainShadows: ShadowMode.DISABLED,
            creditContainer: document.createElement('div')
        });

        cesiumViewer.camera.setView({
            destination: new Cartesian3(0, 0, 0),
            orientation: {
                heading: 10,
                pitch: -CesiumMath.PI_OVER_TWO * 0.5,
                roll: 0.0
            }
        });

        // create viewer
        const viewer = new Viewer(divViewerRef.current!, {
            noDragAndDrop: false,
            isStats: false,
        })

        viewer.setEDLEnabled(true);
        viewer.setFOV(60);
        viewer.setPointBudget(isMobile ? 2_000_000 : 4_000_000);
        viewer.setEDLEnabled(false);
        viewer.setBackground('map'); // ["skybox", "gradient", "black", "white"];
        viewer.loadSettingsFromURL();

        if (!isMobile) {
            viewer.useHQ = true;
        }

        viewer.cesiumViewer = cesiumViewer;

        setArgs({
            csMap: cesiumViewer,
            viewer: viewer
        })

        return () => {
            viewer.destroy(() => {
                cesiumViewer.destroy();
            });

            console.log('%cUnmount Screen: ThreeView', Color.ConsoleInfo)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const getMainFC = useMemo(() => {
        if (args) {
            return (
                <Main
                    m3dId={props.m3dId}
                    item={props.item}
                    parts={props.parts}
                    viewer={args.viewer}
                    csMap={args.csMap}
                    event={new EventEmitter()}
                />
            )
        }

        return null;

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [args])

    return (
        <>
            <div
                ref={divViewerRef}
                className={styles.ViewerContainer}
                style={{
                    minHeight: `calc(100vh - ${headerHeightRef.current}px)`
                }}
            >
                <div
                    className={styles.CsMapContainer}
                    ref={divCSMapRef}
                />
            </div>
            {getMainFC}
        </>
    )
});

export default ThreeView;
