import proj4 from "proj4";
import {Object3D} from "three";

declare global {
    interface Window {
        toMap: proj4.Converter;
        toScene: proj4.Converter;
        CESIUM_BASE_URL: string;
        vrSlider: Object3D,
        vrMenu: Object3D,
    }
}

window.CESIUM_BASE_URL = 'http://localhost:3000/';
