import {App} from "../const/App";

export class Utils {
    static convertSecondByFrame(index: number) {
        const totalSeconds = index / App.FPS;

        let hours = Math.floor(totalSeconds / 3600);
        let minutes = Math.floor((totalSeconds - (hours * 3600)) / 60);
        let seconds = totalSeconds - (hours * 3600) - (minutes * 60);

        seconds = Math.round((seconds * 100) / 100);

        return {
            hour: hours < 10 ? "0" + hours.toString() : hours.toString(),
            minute: minutes < 10 ? "0" + minutes.toString() : minutes.toString(),
            second: seconds < 10 ? "0" + seconds.toString() : seconds.toString(),
        }
    }

    static random(min: number, max: number) {
        if (max == null) {
            max = min;
            min = 0;
        }

        return Math.random() * (max - min) + min;
    }
}
