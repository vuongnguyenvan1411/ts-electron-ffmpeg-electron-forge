import WebFont from "webfontloader";
import {Style} from "../const/Style";

export class ResLoader {
    static init = (fnActive: any) => {
        WebFont.load({
            custom: {
                families: [Style.fontRobotoMedium, Style.fontRobotoBold, Style.fontFontAwesome],
            },
            active: fnActive,
        })
    }
}
