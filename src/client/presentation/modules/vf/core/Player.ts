import EventEmitter from "eventemitter3";
import {PaginateMetaModel} from "../../../../models/ApiResModel";

export class Player extends EventEmitter {
    runIndex: number = 0;
    totalCount: number = 0;
    pageCount: number = 0;
    currentPage: number = 0;
    nextPage: number = 0;
    perPage: number = 0;
    isPlay: boolean = false;

    constructor() {
        super();

        console.log('Player');
    }

    playing = () => {
        if (!this.isPlay) {
            return false;
        }

        return this.runIndex > 0 && this.runIndex < this.totalCount;
    }

    reset = () => {
        this.runIndex = 0;
        this.totalCount = 0;
        this.isPlay = false;
    }

    setValue = (meta?: PaginateMetaModel) => {
        this.currentPage = meta?.currentPage!;
        this.nextPage = meta?.nextPage!;
        this.pageCount = meta?.pageCount!;
        this.perPage = meta?.perPage!;
        this.totalCount = meta?.totalCount!;
    }
}
