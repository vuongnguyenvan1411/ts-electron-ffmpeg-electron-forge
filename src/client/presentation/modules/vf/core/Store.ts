import {TimelapseVideoFilterModel} from "../../../../models/service/timelapse/TimelapseVideoFilterModel";
import {Player} from "./Player";

export class Store {
    static item?: TimelapseVideoFilterModel;
    static player?: Player;

    static destroy = () => {
        Store.item = undefined;
        Store.player = undefined;
    }
}
