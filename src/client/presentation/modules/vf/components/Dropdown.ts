import {Container, Graphics, Text} from "pixi.js";
import {Style} from "../const/Style";
import {MouseEventName} from "../const/Event";
import {App} from "../const/App";

type IConfig = {
    current: number;
    items: number[];
    width?: number;
}

export class Dropdown extends Container {
    static readonly eventChange: string = 'changeItem';

    protected currentBg: Graphics;
    protected currentText: Text;
    protected dropdownBg: Container;
    protected currentValue: number;
    protected widthBg: number = 49;

    constructor(config: IConfig) {
        super();

        if (config.width) {
            this.widthBg = config.width;
        }

        this.currentValue = config.current;

        this.currentBg = new Graphics();
        this.currentBg.beginFill(0xffffff);
        this.currentBg.drawRect(0, 0, this.widthBg, 22);
        this.currentBg.endFill();
        this.addChild(this.currentBg);

        this.currentText = new Text(`${this.currentValue}x`, Style.TextSpeed);
        this.currentBg.addChild(this.currentText);
        this.currentText.position.set((this.currentBg.width - this.currentText.width) / 2, 2);
        this.position.set((this.currentBg.width - this.currentText.width) / 2, (this.currentBg.height - this.currentText.height) / 2);

        this.currentBg.interactive = true;
        this.currentBg.buttonMode = true;

        this.dropdownBg = new Container();
        this.addChild(this.dropdownBg);

        this.dropdownBg.interactive = true;

        let textHeight = 0;

        config.items.forEach((item, index) => {
            const menuItem = this.menuItem(item);
            menuItem.position.set(2, menuItem.height * index);

            textHeight += (menuItem.height + 2);

            this.dropdownBg.addChild(menuItem);
        });

        this.dropdownBg.width = 49;
        this.dropdownBg.height = textHeight + 2 + 2;
        this.dropdownBg.position.set(-2, -(textHeight + 2 + 2 + 1));
        this.dropdownBg.visible = false;

        this.currentBg.on(MouseEventName.Click, () => {
            this.dropdownBg.visible = !this.dropdownBg.visible;
        });

        this.currentBg.on(MouseEventName.Over, () => {
            this.currentBg.clear();
            this.currentBg.beginFill(0xcccccc);
            this.currentBg.drawRect(0, 0, this.widthBg, 22);
            this.currentBg.endFill();
        });

        this.currentBg.on(MouseEventName.Out, () => {
            this.currentBg.clear();
            this.currentBg.beginFill(0xffffff);
            this.currentBg.drawRect(0, 0, this.widthBg, 22);
            this.currentBg.endFill();
        });
    }

    protected menuItem(value: number) {
        const bg = new Graphics();

        const text = new Text(`${value}x`, Style.TextSpeed);
        bg.addChild(text);
        text.position.set(((this.widthBg + 2) - text.width) / 2, 4);

        bg.beginFill(0xffffff);
        bg.drawRect(0, 0, this.widthBg + 2, text.height + 8);
        bg.endFill();

        bg.interactive = true;
        bg.buttonMode = true;

        bg.on(MouseEventName.Over, () => {
            bg.clear();
            bg.beginFill(App.ColorMain);
            bg.drawRect(0, 0, this.widthBg + 2, text.height + 8);
            bg.endFill();
        });

        bg.on(MouseEventName.Out, () => {
            bg.clear();
            bg.beginFill(0xffffff);
            bg.drawRect(0, 0, this.widthBg + 2, text.height + 8);
            bg.endFill();
        });

        bg.on(MouseEventName.Click, () => {
            this.currentValue = value;

            bg.clear();
            bg.beginFill(App.ColorMain);
            bg.drawRect(0, 0, this.widthBg + 2, text.height + 8);
            bg.endFill();

            this.currentText.text = `${value}x`;
            this.currentText.position.set((this.widthBg + 2 - this.currentText.width) / 2, 2);
            this.dropdownBg.visible = false;
            this.emit(Dropdown.eventChange, value);
        });

        return bg;
    }
}
