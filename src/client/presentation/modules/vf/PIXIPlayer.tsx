import React from "react";
import {TimelapseVideoFilterModel} from "../../../models/service/timelapse/TimelapseVideoFilterModel";
import useCanvas from "./Canvas";
import {MediaQuery} from "../../../core/MediaQuery";

const PIXIPlayer = React.memo((props: {
    item?: TimelapseVideoFilterModel,
    tab: string,
}) => {
    const mediaQuery = new MediaQuery({
        xs: 354,
        sm: 480,
        md: 680,
        lg: 856,
        xl: 856,
        xxl: 1000,
    });

    const width = mediaQuery.getPointOnUp();

    let height = 0;
    let ratio = 1;

    if (props.item && props.item.last?.width !== undefined && props.item.last?.height !== undefined) {
        ratio = props.item.last.width / props.item.last.height;

        height = Math.round(width / ratio);
    }

    const config = {
        width: width,
        height: height,
        ratio: ratio,
    };

    const canvas = useCanvas({
        item: props.item,
        config: config,
    })

    return (
        canvas.canvas
    )
})

export default PIXIPlayer;
