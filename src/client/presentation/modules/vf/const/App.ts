export class App {
    static readonly Id = 1;
    static readonly Version = '1.0.0';
    static RATIO = 1.5;
    static W = 1000;
    static H = 690;
    static PaddingX = 0;
    static PaddingY = 0;
    static readonly Ratio = App.W / App.H;
    static Scale = 1;
    static WV = 1000;
    static HV = 667;
    static IsMobile = 0;
    static FPS = 30;
    static readonly HCtl = 6;
    static readonly ColorMain = 0x179c91;
    static Domain = 'https://app.autotimelapse.com';
    static UrlRs = `${App.Domain}/resources`;
}
