import {Text, TextStyle} from "pixi.js";
import {isMobile, isTablet} from "react-device-detect";

export class Style {
    static readonly fontRobotoMedium: string = "Roboto Medium";
    static readonly fontRobotoBold: string = "Roboto Bold";
    static readonly fontFontAwesome: string = "FontAwesome";

    static readonly TextVideoLogo: TextStyle = new TextStyle({
        padding: 2,
        align: "center",
        fontFamily: Style.fontRobotoMedium,
        fontSize: 19,
        fill: 0xffffff
    });

    static readonly TextDebugBar: TextStyle = new TextStyle({
        padding: 2,
        fontFamily: Style.fontRobotoMedium,
        fontSize: 15,
        fill: 0xffffff
    });

    static readonly TextCtlTime: TextStyle = new TextStyle({
        padding: 2,
        fontFamily: Style.fontRobotoMedium,
        fontSize: 16,
        fontWeight: '500',
        fill: 0xffffff
    });

    static readonly TextCtlTooltip: TextStyle = new TextStyle({
        padding: 2,
        align: "center",
        fontFamily: Style.fontRobotoMedium,
        fontSize: 13,
        fontWeight: '500',
        fill: 0xffffff
    });

    static readonly IconRePlay: Text = new Text('\uf01e', new TextStyle({
        fill: 0xffffff,
        fontSize: (isMobile || isTablet) ? 30 : 22,
        padding: 5,
        fontFamily: Style.fontFontAwesome,
        fontWeight: '700',
    }));

    static readonly IconPlay: Text = new Text('\uf04b', new TextStyle({
        fill: 0xffffff,
        fontSize: (isMobile || isTablet) ? 30 : 22,
        padding: 5,
        fontFamily: Style.fontFontAwesome,
        fontWeight: '700',
    }));

    static readonly IconPause: Text = new Text('\uf04c', new TextStyle({
        fill: 0xffffff,
        fontSize: (isMobile || isTablet) ? 30 : 22,
        padding: 5,
        fontFamily: Style.fontFontAwesome,
        fontWeight: '700',
    }));

    static readonly IconFullScreen: Text = new Text('\uf0b2', new TextStyle({
        fill: 0xffffff,
        fontSize: (isMobile || isTablet) ? 30 : 22,
        padding: 5,
        fontFamily: Style.fontFontAwesome,
        fontWeight: '700',
    }));

    static readonly IconVolumeOn: Text = new Text('\uf028', new TextStyle({
        fill: 0xffffff,
        fontSize: (isMobile || isTablet) ? 30 : 22,
        padding: 5,
        fontFamily: Style.fontFontAwesome,
        fontWeight: '700',
    }));

    static readonly IconVolumeOff: Text = new Text('\uf026', new TextStyle({
        fill: 0xffffff,
        fontSize: (isMobile || isTablet) ? 30 : 22,
        padding: 5,
        fontFamily: Style.fontFontAwesome,
        fontWeight: '700',
    }));

    static readonly TextSpeed: TextStyle = new TextStyle({
        padding: 2,
        align: "center",
        fontFamily: Style.fontRobotoMedium,
        fontSize: 15,
        fill: 0x000000
    });
}
