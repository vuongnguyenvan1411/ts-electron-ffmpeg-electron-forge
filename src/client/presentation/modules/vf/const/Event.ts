export class MouseEventName {
    public static readonly Click = "pointertap";
    public static readonly Down = "pointerdown";
    public static readonly Move = "pointermove";
    public static readonly Out = "pointerout";
    public static readonly Over = "pointerover";
    public static readonly Up = "pointerup";
    public static readonly UpOutside = "pointerupoutside";
    // public static readonly RightClick = "rightclick";
    public static readonly RightDown = "rightdown";
    public static readonly RightUp = "rightup";
    public static readonly RightOutside = "rightupoutside";
}

export class TouchEventName {
    public static readonly Tap = "tap";
    public static readonly End = "touchend";
    public static readonly EndOutside = "touchendoutside";
    public static readonly Move = "touchmove";
    public static readonly Start = "touchstart";
}

export class KeyEventName {
    public static readonly Keyup = "keyup";
    public static readonly KeyDown = "keydown";
}

export class EmitterEventName {
    public static readonly Change = "change";
    public static readonly Click = "click";
}
