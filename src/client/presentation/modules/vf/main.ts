import * as PIXI from "pixi.js";
import {Application, Container} from "pixi.js";
import {TimelapseVideoFilterModel} from "../../../models/service/timelapse/TimelapseVideoFilterModel";
import {App} from "./const/App";
import {AppView} from "./view/AppView";
import {Store} from "./core/Store";
import {Player} from "./core/Player";
import {ResLoader} from "./core/ResLoader";
import gsap from "gsap";
import PixiPlugin from "gsap/PixiPlugin";
import screenfull from "screenfull";
import {Color} from "../../../const/Color";
import EventEmitter from "eventemitter3";

export type IConfig = {
    width: number,
    height: number,
    ratio: number,
}

export default class Main {
    protected item: TimelapseVideoFilterModel;
    protected renderer: Application;
    protected stage: Container;
    protected canvas: HTMLCanvasElement;
    protected config: IConfig;
    protected event: EventEmitter;

    constructor(item: TimelapseVideoFilterModel, config: IConfig, canvas: HTMLCanvasElement) {
        console.log("%cPIXI Init", Color.ConsoleInfo);

        PIXI.utils.skipHello();

        App.WV = config.width;
        App.HV = config.height;
        App.RATIO = config.ratio;

        App.W = App.WV;
        App.H = App.HV + 28;

        this.item = item;
        this.config = config;
        this.event = new EventEmitter();

        this.renderer = new Application({
            view: canvas,
            width: App.W,
            height: App.H,
            backgroundColor: 0x8a8a8a,
            antialias: true
        });
        this.stage = this.renderer.stage;

        // this.context = new Context();

        ResLoader.init(this.onResLoaded);

        this.render();

        this.canvas = canvas;
    }

    private onResLoaded = () => {
        console.log('Main.onResLoaded');

        Store.item = this.item;
        Store.player = new Player();

        PixiPlugin.registerPIXI(PIXI);
        gsap.registerPlugin(PixiPlugin);

        if (screenfull.isEnabled) {
            screenfull.on('change', this.onScreenfull);
        }

        this.initView();
    }

    private initView = () => {
        console.log('Main.initView');

        this.event.on('ScreenFullSignal', (isFullscreen: boolean) => {
            if (isFullscreen) {
                this.renderer.renderer.resize(window.innerWidth, window.innerHeight);
                this.canvas.style.setProperty('position', 'fixed');
                this.canvas.style.setProperty('top', '0');
                this.canvas.style.setProperty('left', '0');
                this.canvas.style.setProperty('zIndex', '999999');
            } else {
                this.renderer.renderer.resize(App.W, App.H);
                this.canvas.style.removeProperty("position");
                this.canvas.style.removeProperty("top");
                this.canvas.style.removeProperty("left");
                this.canvas.style.removeProperty("zIndex");
            }
        })

        if (this.stage.children.length > 0) {
            for (let i = this.stage.children.length - 1; i >= 0; i--) {
                this.stage.removeChild(this.stage.children[i]);
            }
        }

        this.stage.addChild(new AppView(this.event));
    }

    protected render = () => {
        // this.renderer.renderer.render(this.stage);
        //
        // window.requestAnimationFrame(this.render);
    }

    destroy = () => {
        console.log('%cPIXI Destroy', Color.ConsoleInfo);

        // if (this.stage instanceof Container) {
        //     this.stage.destroy({
        //         children: true,
        //         texture: true,
        //         baseTexture: true
        //     });
        // }

        PIXI.utils.clearTextureCache();

        try {
            this.renderer.destroy(true);
        } catch (e) {
            console.log('Error Pixi destroy', e)
        }

        if (screenfull.isEnabled) {
            screenfull.off('change', this.onScreenfull);
        }

        this.event.emit('appDestroySignal');

        Store.destroy();

        this.event.removeAllListeners();
    }

    protected onScreenfull = () => {
        if (screenfull.isEnabled) {
            if (screenfull.isFullscreen) {
                this.renderer.renderer.resize(window.innerWidth, window.innerHeight);
                this.canvas.style.setProperty('position', 'fixed');
                this.canvas.style.setProperty('top', '0');
                this.canvas.style.setProperty('left', '0');
                this.canvas.style.setProperty('zIndex', '999999');
            } else {
                this.renderer.renderer.resize(App.W, App.H);
                this.canvas.style.removeProperty("position");
                this.canvas.style.removeProperty("top");
                this.canvas.style.removeProperty("left");
                this.canvas.style.removeProperty("zIndex");
            }

            this.event.emit('ScreenFullSignal', screenfull.isFullscreen);
        }
    }
}
