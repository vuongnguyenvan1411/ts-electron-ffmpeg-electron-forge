import {App} from "../const/App";
import {Style} from "../const/Style";
import {Container, Text} from "pixi.js";
import i18next from "i18next";
import EventEmitter from "eventemitter3";

export class DebugBarView extends Container {
    textLoading: Text;
    textFps: Text;
    textDateShot: Text;
    marginRight: number = 50;
    event: EventEmitter;

    constructor(event: EventEmitter) {
        super();

        this.event = event;

        // Text Frame
        this.textLoading = new Text('');
        this.textLoading.position.set(10, 3);
        this.textLoading.text = '...';
        this.textLoading.name = 'loading';
        this.textLoading.style = Style.TextDebugBar;

        this.addChild(this.textLoading);

        // Text FPS
        this.textFps = new Text('');
        this.updatePostTextFps();
        this.textFps.text = '...';
        this.textFps.name = 'fps';
        this.textFps.style = Style.TextDebugBar;

        this.addChild(this.textFps);

        // Text Date Shot
        this.textDateShot = new Text('');
        if (App.IsMobile) {
            this.textDateShot.text = `00/00/0000`;
        } else {
            this.textDateShot.text = `${i18next.t('text.day')} 00/00/0000`;
        }
        this.textDateShot.name = 'date_shot';
        this.textDateShot.style = Style.TextDebugBar;
        this.addChild(this.textDateShot);
        this.textDateShot.position.set(App.W - this.textDateShot.width - 10, 3);

        // Mediator
        this._initialize();
    }

    public updatePostTextFps = () => {
        this.textFps.position.set(this.textLoading.width + this.marginRight * 2, 3);
    };

    public updateTextDateShot = (day: string) => {
        if (App.IsMobile) {
            this.textDateShot.text = day;
        } else {
            this.textDateShot.text = `${i18next.t('text.day')} ${day}`;
        }
    }

    // Mediator
    protected _initialize = () => {
        console.log("DebugBarView initialized!");

        this.interactive = true;

        this.event.on('loadingChangeSignal', this.changeLoading);
        // this.event.on('iframeChangeSignal', this.changeFrame);
        this.event.on('fpsChangeSignal', this.changeFps);
        this.event.on('dateShotChangeSignal', this.changeDateShot);

        if (App.IsMobile) {
            this.textFps.visible = false;
        }
    }

    protected isPosLoading = false;

    public changeLoading = (percent: number) => {
        if (App.IsMobile) {
            this.textLoading.text = `${percent} %`;
        } else {
            this.textLoading.text = `${percent === 100 ? i18next.t('text.complete') : i18next.t('text.loading')} ${percent} %`;
        }

        if (!this.isPosLoading) {
            this.updatePostTextFps();

            this.isPosLoading = true;
        }
    }

    // public changeFrame = (index: number, total: number) => {
    //     console.log('DebugBarMediator.changeFrame', index, total);
    // };

    public changeFps = (value: number) => {
        this.textFps.text = `${i18next.t('text.fps')} ${value}`;
    }

    protected isPosDateShot = false;

    public changeDateShot = (date: string) => {
        this.updateTextDateShot(date);

        if (!this.isPosDateShot) {
            this.isPosDateShot = true;
        }
    }

    public destroy(): void {
        console.log("DebugBarView destroyed!");
    }
}
