import {Container, Graphics, Loader, LoaderResource, Sprite, Text, Texture, Ticker, utils} from "pixi.js";
import {App} from "../const/App";
import LogoLogin from "../../../../assets/image/logo_login.png";
import {Store} from "../core/Store";
import {Style} from "../const/Style";
import gsap from "gsap";
import EventEmitter from "eventemitter3";
import {Howl, Howler} from "howler";
import {AxiosClient} from "../../../../repositories/AxiosClient";
import {ApiService} from "../../../../repositories/ApiService";
import {ApiResModel} from "../../../../models/ApiResModel";
import {TimelapseImageModel} from "../../../../models/service/timelapse/TimelapseImageModel";
import {E_ResUrlType} from "../../../../const/Events";
import {isMobile} from "react-device-detect";

export class VideoView extends Container {
    image: Sprite | Container;
    event: EventEmitter;

    constructor(event: EventEmitter) {
        super();

        this.event = event;

        utils.clearTextureCache();

        this.pivot.set(0, 0);

        const bg = new Graphics();
        bg.beginFill(0x979292, 1);
        bg.drawRect(0, 0, App.WV, App.HV);
        bg.pivot.set(0, 0);
        this.addChild(bg);

        this.image = new Sprite();
        // this.image.width = App.WV;
        // this.image.height = App.HV;

        this.renderLogo();

        // Mediator
        this._initialize();
    }

    viewRenderImage = (texture: Texture) => {
        this.removeChild(this.image);

        this.image = Sprite.from(texture);

        this.image.width = App.WV;
        this.image.height = App.HV;
        this.image.position.set(0, 0);
        this.image.pivot.set(0, 0);

        this.addChild(this.image);
    }

    renderLogo = () => {
        if (Store.item && Store.item.name) {
            Texture
                .fromURL(LogoLogin.src)
                .then(texture => {
                    if (!Store.item) {
                        return
                    }

                    this.removeChild(this.image);

                    this.image = new Container();

                    const ratio = texture.width / texture.height;
                    const width = App.WV / 3.5;
                    const height = Math.round(width / ratio);

                    const brand = new Text(Store.item.name, Style.TextVideoLogo);
                    this.image.addChild(brand);

                    const logo = Sprite.from(texture);
                    logo.width = width;
                    logo.height = height;

                    this.image.addChild(logo);

                    brand.position.set((this.image.width - brand.width) / 2, 0)
                    logo.position.set((this.image.width - logo.width) / 2, brand.height + 20)

                    this.image.position.set((App.W - this.image.width) / 2, ((App.H - this.image.height) / 2) - 40);
                    this.image.scale.set(0, 0);
                    this.image.rotation = 60 * Math.PI / 45;
                    this.addChild(this.image);

                    gsap
                        .to(this.image, {
                            pixi: {
                                scaleX: 1,
                                scaleY: 1,
                                rotation: 60 * Math.PI / 180
                            },
                            duration: 2.5,
                            yoyo: true
                        })
                })
        }
    }

    // Mediator
    private readonly loaderOptions = {
        loadType: LoaderResource.LOAD_TYPE.IMAGE,
        xhrType: LoaderResource.XHR_RESPONSE_TYPE.BLOB
    }
    private loader!: Loader
    private ticker!: Ticker
    private frame!: TimelapseImageModel[]
    private fpsDelta!: number
    private sound!: Howl
    private isSound!: boolean
    private soundDuration!: number

    protected _initialize = () => {
        console.log("VideoView initialized!");

        this.frame = []

        this.ticker = new Ticker();
        this.ticker.autoStart = false;
        this.ticker.add(this.updateTicker);

        try {
            this.sound = new Howl({
                src: ['/sound/mercury.ogg', '/sound/gray-clouds.ogg'],
                preload: true,
                onload: () => this.soundDuration = this.sound.duration(),
            });
            Howler.autoSuspend = true;
            Howler.autoUnlock = true;
        } catch (e) {
            console.warn('Howler', e);
        }

        this.isSound = true;

        this.loader = new Loader();
        this.loader.reset();
        // this.loader.pre(PIXI.compressedTextures.extensionChooser(Main.extensions));
        this.loader.onComplete.add(this.loadOnComplete);
        this.loader.onProgress.add(this.loadOnProgress);

        this.callApi(1);

        this.event.on('videoReplaySignal', this.clickReplay);
        this.event.on('videoPlaySignal', this.clickPlay);
        this.event.on('videoPauseSignal', this.clickPause);
        this.event.on('videoRenderImageSignal', this.renderImage);
        this.event.on('speedChangeSignal', this.calculatorSpeed);
        this.event.on('isSoundSignal', this.onIsSound);
        this.event.on('appDestroySignal', this.appDestroy);

        this.calculatorSpeed(1);
    }

    private calculatorSpeed(speed: number) {
        console.log('VideoMediator.calculatorSpeed', speed);

        this.fpsDelta = 60 / (App.FPS * speed);
    }

    private callApi = (page: number) => {
        if (Store.item === undefined) {
            return;
        }

        AxiosClient
            .get(ApiService.getTimelapseVideoImages(Store.item!.machineId!), {...Store.item!.filter, page: page})
            .then(this.getData);
    }

    isFirstFrame = true

    private getData = (res: ApiResModel) => {
        if (Store.item === undefined) {
            return;
        }

        if (res.success && res.items && res.items.length > 0) {
            Store.player?.setValue(res.meta);

            res.items.forEach((value, i) => {
                const item = new TimelapseImageModel(value);

                const addResource = () => {
                    this.frame[((res.meta?.currentPage! - 1) * res.meta?.perPage!) + i] = item;

                    if (this.isFirstFrame) {
                        this.renderImage(0);

                        this.isFirstFrame = false;
                    }
                }

                if (item.order) {
                    const url = item.getShotImageUrl({
                        type: E_ResUrlType.Thumb,
                        size: isMobile ? 426 : 1280
                    })

                    if (url && url.length > 0) {
                        if (!this.loader.resources.hasOwnProperty(item.order.toString())) {
                            this.loader.add(
                                item.order.toString(),
                                url,
                                this.loaderOptions,
                                (resource: LoaderResource) => {
                                    if (resource.error === null) {
                                        addResource()
                                    } else {
                                        Store.player!.totalCount--;
                                        this.event.emit('timeTotalChangeSignal', Store.player!.totalCount)
                                    }
                                })
                        } else {
                            addResource()
                        }
                    }
                }
            })

            this.loader.load()

            this.event.emit('timeTotalChangeSignal', Store.player!.totalCount)
        }
    }

    isOk = false;

    private loadOnComplete = () => {
        if (Store.item === undefined) {
            return
        }

        // if (this.frame.length) {
        //     this.frame.sort((a, b) => {
        //         return ((a.order! < b.order!) ? -1 : ((a.order! === b.order!) ? 0 : 1));
        //     })
        // }

        if (Store.player!.currentPage < Store.player!.pageCount) {
            this.isOk = true

            this.callApi(Store.player!.nextPage)
        }
    }

    private iLOP = 0;
    private loadOnProgress = () => {
        if (Store.item === undefined) {
            return;
        }

        const per = Math.round((this.iLOP / Store.player!.totalCount) * 100);

        this.event.emit('loadingChangeSignal', per);

        this.iLOP++
    }

    protected elapsedTime = 0;

    private updateTicker = (delta: number) => {
        if (Store.item === undefined) {
            return;
        }

        this.event.emit('fpsChangeSignal', Math.round(this.ticker.FPS));

        this.elapsedTime += delta;

        if (this.elapsedTime >= (this.fpsDelta)) {
            // enough time passed, update app
            this.runTimeVideo();
            // reset
            this.elapsedTime = 0;
        }
    }

    private runTimeVideo = () => {
        if (Store.item === undefined) {
            return;
        }

        if (this.frame.length && typeof (this.frame[Store.player!.runIndex]) !== "undefined" && this.loader.resources.hasOwnProperty(this.frame[Store.player!.runIndex].order!.toString())) {
            this.viewRenderImage(this.loader.resources[this.frame[Store.player!.runIndex].order!.toString()].texture!);
            const iframeNow = Store.player!.runIndex + 1;

            this.event.emit('iframeChangeSignal', iframeNow, Store.player!.totalCount);
            this.event.emit('timePlayChangeSignal', Store.player!.runIndex);
            this.event.emit('dateShotChangeSignal', this.frame[Store.player!.runIndex].dateShotFormatted('DD/MM/YYYY'));

            // if (Store.player!.sensor.length) {
            //     this.chartUpdateSignal.dispatch(iframeNow, this.frame[Store.player!.runIndex].info);
            // }

            Store.player!.runIndex++;

            if (iframeNow === Store.player!.totalCount) {
                this.event.emit('showReplaySignal');
                this.event.emit('showControlSignal');

                if (this.ticker.started) {
                    this.ticker.stop();
                }

                try {
                    if (this.sound.playing()) {
                        this.sound.stop();
                    }
                } catch (e) {
                    console.warn('Howler', e);
                }
            }
        }
    }

    public renderImage = (index: number) => {
        if (Store.item === undefined) {
            return;
        }

        if (this.isOk && this.frame.length && typeof (this.frame[index]) !== "undefined" && this.loader.resources.hasOwnProperty(this.frame[index].order!.toString())) {
            this.viewRenderImage(this.loader.resources[this.frame[index].order!.toString()].texture!);
            this.event.emit('dateShotChangeSignal', this.frame[index].dateShotFormatted('DD/MM/YYYY'));
        }
    }

    public clickReplay = (index: number) => {
        Store.player!.runIndex = index;

        if (!this.ticker.started) {
            this.ticker.start();
            Store.player!.isPlay = true;
        }

        this.playSound();
    }

    protected isTimeOutControl = false

    public clickPlay = () => {
        console.log('VideoMediator.clickPlay');

        if (this.frame.length) {
            if (!this.ticker.started) {
                this.ticker.start();
                Store.player!.isPlay = true;

                if (!this.isTimeOutControl) {
                    this.event.emit('hideControlSignal');

                    this.isTimeOutControl = true;
                }
            }

            this.playSound();
        }
    }

    public clickPause = () => {
        if (this.ticker.started) {
            this.ticker.stop();
            Store.player!.isPlay = false;
        }

        try {
            if (this.sound.playing()) {
                this.sound.stop();
            }
        } catch (e) {
            console.warn('Howler', e);
        }
    }

    public onIsSound = (isSound: boolean) => {
        this.isSound = isSound;

        try {
            if (isSound) {
                this.playSound();
            } else {
                if (this.sound.playing()) {
                    this.sound.stop();
                }
            }
        } catch (e) {
            console.warn('Howler', e);
        }
    }

    protected playSound = () => {
        try {
            if (this.isSound && !this.sound.playing()) {
                let second = Math.round(Store.player!.runIndex / App.FPS);

                if (second > this.soundDuration) {
                    second = 0;
                }

                this.sound.seek(second);
                this.sound.play();
            }
        } catch (e) {
            console.warn('Howler', e);
        }
    }

    public destroy(): void {
        console.log("VideoMediator destroyed!");

        Store.player!.runIndex = 0;
    }

    public appDestroy = () => {
        console.log("VideoMediator appDestroy!");

        if (this.ticker.started) {
            this.ticker.destroy();
            Store.player!.isPlay = false;

            try {
                this.sound.unload();
            } catch (e) {
                console.warn('Howler', e);
            }
        }
    }
}
