import {Container, Graphics, InteractionData, InteractionEvent} from "pixi.js";
import {DebugBarView} from "./DebugBarView";
import {VideoView} from "./VideoView";
import {App} from "../const/App";
import {ControlView} from "./ControlView";
import {ChartView} from "./ChartView";
import EventEmitter from "eventemitter3";
import {MouseEventName, TouchEventName} from "../const/Event";
import {Store} from "../core/Store";

export class AppView extends Container {
    bg: Graphics;
    debugBarView: DebugBarView;
    videoView: VideoView;
    controlView: ControlView;
    chartView: ChartView;
    event: EventEmitter;

    static instanceAV: AppView;

    constructor(event: EventEmitter) {
        super();

        AppView.instanceAV = this;
        this.event = event;

        this.pivot.set(0, 0);

        this.bg = new Graphics();
        this.bg.beginFill(0x716d6d, 1);
        this.bg.drawRect(0, 0, App.W, App.H);
        this.bg.endFill();
        this.bg.pivot.set(0, 0);
        this.addChild(this.bg);

        this.debugBarView = new DebugBarView(this.event);
        this.debugBarView.position.set(3, 3);
        this.addChild(this.debugBarView);

        this.videoView = new VideoView(this.event);
        this.videoView.position.set(0, App.H - App.HV);
        this.addChild(this.videoView);

        this.controlView = new ControlView(this.event);
        this.controlView.position.set((App.W - this.controlView.width) / 2, App.H - this.controlView.height + 1);
        this.addChild(this.controlView);

        this.chartView = new ChartView(this.event);
        this.chartView.position.set((App.W - this.chartView.width) / 2, App.H - this.chartView.height - 120);
        this.addChild(this.chartView);
        this.chartView.visible = false;

        // Mediator
        this._initialize();
    }

    public applyResize = (isFullscreen: boolean) => {
        if (!isFullscreen) {
            this.scale.set(1, 1);
        } else {
            this.scale.set(window.innerWidth / App.W, window.innerHeight / App.H);
        }
    }

    // Mediator
    private dragging!: boolean;
    private dataMouse!: InteractionData | null;

    protected _initialize = () => {
        console.log("AppView initialized!");

        this.chartView.interactive = true;
        this.chartView.buttonMode = true;

        this.chartView.on(MouseEventName.Down, this.onDragStartChart);

        this.chartView.on(MouseEventName.Down, this.onDragStartChart);
        this.chartView.on(TouchEventName.Start, this.onDragStartChart);
        this.chartView.on(MouseEventName.Up, this.onDragEndChart);
        this.chartView.on(MouseEventName.UpOutside, this.onDragEndChart);
        this.chartView.on(TouchEventName.End, this.onDragEndChart);
        this.chartView.on(TouchEventName.EndOutside, this.onDragEndChart);
        this.chartView.on(MouseEventName.Move, this.onDragMoveChart);
        this.chartView.on(TouchEventName.Move, this.onDragMoveChart);

        this.videoView.interactive = true;
        this.videoView.buttonMode = true;
        this.videoView.on(MouseEventName.Click, this.onClickVideoView);

        this.interactive = true;
        this.on(MouseEventName.Over, this.onOverView);
        this.on(MouseEventName.Out, this.onOutView);

        this.event.on('screenFullSignal', this.applyResize);
        this.event.on('chartInitSignal', this.showChart);
        this.event.on('showControlSignal', this.showControl);
        this.event.on('hideControlSignal', this.hideControl);
    }

    public onDragStartChart = (e: InteractionEvent) => {
        this.dragging = true;
        this.dataMouse = e.data;
        this.chartView.cursor = "move";
    }

    public onDragEndChart = () => {
        this.dragging = false;
        this.dataMouse = null;
        this.chartView.cursor = "auto";
    }

    public onDragMoveChart = () => {
        if (this.dragging) {
            let newPosX: number = this.dataMouse!.getLocalPosition(this.parent).x;

            if (newPosX < this.x) {
                newPosX = 0;
            }

            if (newPosX > this.width) {
                newPosX = this.width;
            }

            let newPosY: number = this.dataMouse!.getLocalPosition(this.parent).y;

            if (newPosY < this.y) {
                newPosY = 0;
            }

            if (newPosY > this.height) {
                newPosY = this.height;
            }

            this.chartView.position.set(newPosX, newPosY);
        }
    }

    protected onClickVideoView = () => {
        if (Store.player!.runIndex > 0 && Store.player!.runIndex !== Store.player!.totalCount) {
            if (Store.player!.playing()) {
                this.event.emit('videoPauseSignal');
                this.event.emit('showPlaySignal');
            } else {
                this.event.emit('videoPlaySignal');
                this.event.emit('showPauseSignal');
            }
        }
    }

    protected onOverView = () => {
        this.event.emit('showControlSignal');
    }

    protected onOutView = () => {
        this.event.emit('hideControlSignal');
    }

    protected showChart = () => {
        this.chartView.visible = true;
    }

    protected timeoutHideControl: any;

    protected showControl = () => {
        if (typeof this.timeoutHideControl !== "undefined") {
            clearTimeout(this.timeoutHideControl);
        }

        this.controlView.visible = true;
    }

    protected hideControl = () => {
        if (Store.player!.isPlay && Store.player!.runIndex > 0 && Store.player!.runIndex !== Store.player!.totalCount) {
            this.timeoutHideControl = setTimeout(() => {
                this.controlView.visible = false;
            }, 1000);
        }
    }

    public destroy(): void {
        console.log("AppView destroyed!");
    }
}
