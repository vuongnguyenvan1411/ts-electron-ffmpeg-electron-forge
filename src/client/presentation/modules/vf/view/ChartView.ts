import {Container, Graphics} from "pixi.js";
import {App} from "../const/App";
import EventEmitter from "eventemitter3";

export class ChartView extends Container {
    private readonly progressWidth: number;

    event: EventEmitter;
    sensor: any;

    constructor(event: EventEmitter) {
        super();

        this.event = event;

        this.progressWidth = App.WV - 40;

        let bg = new Graphics();
        bg.lineStyle(1, 0xc8ced3, 0.8);
        bg.beginFill(0xffffff, 0.3);
        bg.drawRect(0, 0, this.progressWidth, App.HV / 3);
        bg.endFill();

        this.addChild(bg);

        this.sensor = {};

        // Mediator
        this._initialize();
    }

    public init = (sensor: string[]) => {
        sensor.forEach((item) => {
            this.sensor[item] = new Graphics();
            this.sensor[item].name = item;
            this.sensor[item].lineStyle(2, 0xff0000, .5);
            this.sensor[item].position.set(0, 0);
            this.sensor[item].moveTo(0, 0);
            this.sensor[item].lineTo(1, 0);
            this.sensor[item].interactive = true;

            this.addChild(this.sensor[item]);
        });

        this.updateChart(1, {});
    };

    public updateChart = (index: number, info: any) => {
        /*for (let key in this.sensor) {
            let item = key;
            this.sensor[item].lineTo(Utils.random(1, 100), Utils.random(1, 100));
            this.sensor[item].lineTo(Utils.random(1, 100), Utils.random(1, 100));
            this.sensor[item].lineTo(Utils.random(1, 100), Utils.random(1, 100));
            this.sensor[item].lineTo(Utils.random(1, 100), Utils.random(1, 100));
            this.sensor[item].lineTo(Utils.random(1, 100), Utils.random(1, 100));
            this.sensor[item].lineTo(Utils.random(1, 100), Utils.random(1, 100));
        }*/
        for (let key in this.sensor) {
            if (this.sensor.hasOwnProperty(key)) {
                let value = 0;

                if (info !== null && info.hasOwnProperty(key)) {
                    value = info[key];
                }

                this.sensor[key].lineTo(index, value);
            }
        }

    }

    /*private drawSmiley(): void {
        let graphics: Graphics = new Graphics();

        // Head
        graphics.lineStyle(8, 0x000000, 1);
        graphics.beginFill(0xffcc00);
        graphics.drawCircle(0, 0, this._radius);

        // Mouth
        graphics.lineStyle(8, 0x000000, 1);
        graphics.beginFill(0xffcc00);
        graphics.arc(0, 0, this._radius * 0.65, 0, Math.PI, false);

        // Right eye
        graphics.lineStyle(8, 0x000000, 1);
        graphics.beginFill(0x000000);
        graphics.drawCircle(-(this._radius / 3), -(this._radius / 4), this._radius / 8);

        // Left eye
        graphics.lineStyle(8, 0x000000, 1);
        graphics.beginFill(0x000000);
        graphics.drawCircle(this._radius / 3, -(this._radius / 4), this._radius / 8);

        this.addChild(graphics);
    }

    private move(): void {
        this.x = Math.random() * 960;
        this.y = Math.random() * 400;

        this.x = Math.max(this.x, this._radius);
        this.x = Math.min(this.x, 960 - this._radius);

        this.y = Math.max(this.y, this._radius);
        this.y = Math.min(this.y, 400 - this._radius);
    }

    private enable(): void {
        // Opt-in to interactivity
        this.interactive = true;

        // Shows hand cursor
        this.buttonMode = true;
    }*/

    // Mediator

    protected _initialize = () => {
        console.log("ChartView initialized!");

        this.event.on('chartInitSignal', this.initSensor);
        this.event.on('chartUpdateSignal', this.updateChart);
    }

    public initSensor = (sensor: string[]) => {
        this.init(sensor);
    }

    public destroy(): void {
        console.log("ChartView destroyed!");
    }
}
