import {Container, Graphics, InteractionData, InteractionEvent, Rectangle, Sprite, Text} from "pixi.js";
import {Style} from "../const/Style";
import {App} from "../const/App";
import gsap from "gsap";
import {Dropdown} from "../components/Dropdown";
import EventEmitter from "eventemitter3";
import {MouseEventName, TouchEventName} from "../const/Event";
import {Store} from "../core/Store";
import {Utils} from "../core/Utils";
import screenfull from "screenfull";

export class ControlView extends Container {
    readonly progressWidth: number;

    progressList: Graphics;
    progressPlay: Graphics;
    progressLoad: Graphics;
    progressHover: Graphics;
    circleScrubber: Graphics;
    tooltipLoad: Graphics;
    tooltipText: Text
    btnReplay: Sprite;
    btnPlay: Sprite;
    btnPause: Sprite;
    btnVolumeOn: Sprite;
    btnVolumeOff: Sprite;
    textTimePlay: Text;
    textTimeTotal: Text;
    btnFullScreen: Sprite;
    btnChangeSpeed: Dropdown;

    event: EventEmitter;

    constructor(event: EventEmitter) {
        super();

        this.event = event;

        this.pivot.set(0, 0);

        this.progressWidth = App.WV - 40;

        // Progress List
        this.progressList = new Graphics();
        this.progressList.name = 'progressList';
        this.progressList.beginFill(0xffffff, .2);
        this.progressList.drawRect(0, 0, this.progressWidth, App.HCtl);
        this.progressList.endFill();
        this.progressList.hitArea = new Rectangle(0, -3, this.progressWidth, App.HCtl + 8);

        this.addChild(this.progressList);

        // Progress Load
        this.progressLoad = new Graphics();
        this.progressLoad.name = 'progressLoad';
        this.progressLoad.beginFill(0xffffff, .5);
        this.progressLoad.drawRect(0, 0, 1, App.HCtl);
        this.progressLoad.endFill();

        this.addChild(this.progressLoad);

        // Progress Hover
        this.progressHover = new Graphics();
        this.progressHover.name = 'progressHover';
        this.progressHover.beginFill(0xffffff, .7);
        this.progressHover.drawRect(0, 0, 1, App.HCtl);
        this.progressHover.endFill();

        this.addChild(this.progressHover);

        // Progress Play
        this.progressPlay = new Graphics();
        this.progressPlay.name = 'progressPlay';
        this.progressPlay.beginFill(App.ColorMain, 1);
        this.progressPlay.drawRect(0, 0, 1, App.HCtl);
        this.progressPlay.endFill();

        this.addChild(this.progressPlay);

        // Circle Scrubber
        this.circleScrubber = new Graphics();
        this.circleScrubber.name = 'circleScrubber';
        this.circleScrubber.beginFill(App.ColorMain, 1);
        this.circleScrubber.drawCircle(0, (10 - App.HCtl) / 2, 10);
        this.circleScrubber.endFill();

        this.addChild(this.circleScrubber);

        // Tooltip Load
        this.tooltipLoad = new Graphics();
        this.tooltipLoad.name = 'tooltipLoad';
        this.tooltipLoad.beginFill(0x000000, .8);
        this.tooltipLoad.position.set(0, 0);
        this.tooltipLoad.pivot.set(0, 0);
        this.tooltipLoad.drawRect(0, -35, 65, 25);
        this.tooltipLoad.endFill();
        this.addChild(this.tooltipLoad);

        this.tooltipText = new Text('00:00', Style.TextCtlTooltip);
        this.tooltipText.name = 'tooltipText';
        this.tooltipText.position.set((this.tooltipLoad.width - this.tooltipText.width) / 2, -35 + (this.tooltipLoad.height - this.tooltipText.height) / 2);
        this.tooltipLoad.addChild(this.tooltipText);

        // Btn Replay
        this.btnReplay = new Sprite();
        this.btnReplay.name = 'btnReplay';
        this.btnReplay.addChild(Style.IconRePlay);
        this.btnReplay.position.set(12, 14 + App.HCtl);

        this.addChild(this.btnReplay);

        // Btn Play
        this.btnPlay = new Sprite();
        this.btnPlay.name = 'btnPlay';
        this.btnPlay.addChild(Style.IconPlay);
        this.btnPlay.position.set(12, 14 + App.HCtl);

        this.addChild(this.btnPlay);

        // Btn Pause
        this.btnPause = new Sprite();
        this.btnPause.name = 'btnPause';
        this.btnPause.addChild(Style.IconPause);
        this.btnPause.position.set(12, 14 + App.HCtl);

        this.addChild(this.btnPause);

        // Btn FullScreen
        this.btnFullScreen = new Sprite();
        this.btnFullScreen.name = 'btnFullScreen';
        this.btnFullScreen.addChild(Style.IconFullScreen);
        this.btnFullScreen.position.set(this.progressWidth - (12 + Style.IconFullScreen.width), 14 + App.HCtl);

        this.addChild(this.btnFullScreen);

        // Change Speed
        this.btnChangeSpeed = new Dropdown({
            current: 1,
            items: [2, 1.5, 1, 0.5, 0.25]
        });
        this.btnChangeSpeed.name = 'btnChangeSpeed';
        this.btnChangeSpeed.position.set(this.progressWidth - (12 * 3 + Style.IconFullScreen.width + this.btnChangeSpeed.width), 17 + App.HCtl)

        this.addChild(this.btnChangeSpeed);

        // Text Time Play
        this.textTimePlay = new Text('00:00', Style.TextCtlTime);
        this.textTimePlay.name = 'textTimePlay';
        this.textTimePlay.position.set(this.btnReplay.x + this.btnReplay.width + 40, (App.IsMobile ? 20 : 18) + App.HCtl);

        this.addChild(this.textTimePlay);

        // Text Time Center
        let textTimeCenter = new Text('/', Style.TextCtlTime);
        textTimeCenter.name = 'textTimeCenter';
        textTimeCenter.position.set(this.textTimePlay.x + this.textTimePlay.width + 8, (App.IsMobile ? 20 : 18) + App.HCtl);

        this.addChild(textTimeCenter);

        // Text Time Total
        this.textTimeTotal = new Text('00:00', Style.TextCtlTime);
        this.textTimeTotal.name = 'textTimeTotal';
        this.textTimeTotal.position.set(textTimeCenter.x + textTimeCenter.width + 8, (App.IsMobile ? 20 : 18) + App.HCtl);

        this.addChild(this.textTimeTotal);

        // Btn Volume On
        this.btnVolumeOn = new Sprite();
        this.btnVolumeOn.name = 'btnVolumeOn';
        this.btnVolumeOn.addChild(Style.IconVolumeOn);
        this.btnVolumeOn.position.set(this.textTimeTotal.x + this.textTimeTotal.width + 12, 14 + App.HCtl);

        this.addChild(this.btnVolumeOn);

        // Btn Volume Off
        this.btnVolumeOff = new Sprite();
        this.btnVolumeOff.name = 'btnVolumeOff';
        this.btnVolumeOff.addChild(Style.IconVolumeOff);
        this.btnVolumeOff.position.set(this.textTimeTotal.x + this.textTimeTotal.width + 12, 14 + App.HCtl);

        this.addChild(this.btnVolumeOff);

        this.showBtnPlay();
        this.showBtnVolumeOn();

        // Mediator
        this._initialize();
    }

    public updatePositionCircleScrubber = (x: number) => {
        if (x < 10) {
            this.circleScrubber.x = x;
        } else {
            gsap.to(this.circleScrubber, {
                pixi: {
                    x: x
                }
            });
        }
    }

    public updateWidthProgressPlay = (width: number) => {
        if (width < 10) {
            this.progressPlay.width = width > this.progressWidth ? this.progressWidth : width;
        } else {
            gsap.to(this.progressPlay, {
                pixi: {
                    width: width > this.progressWidth ? this.progressWidth : width
                }
            });
        }
    }

    public updateWidthProgressHover = (width: number) => {
        gsap.to(this.progressHover, {
            pixi: {
                width: width
            }
        });
    }

    public updateWidthProgressLoad = (width: number) => {
        gsap.to(this.progressLoad, {
            pixi: {
                width: width > this.progressWidth ? this.progressWidth : width
            }
        });
    }

    public updateXTooltipLoad = (x: number, msg: string = '') => {
        x -= this.tooltipLoad.width / 2;

        if (x < 0) {
            x = 0;
        }

        if (x > this.progressList.width - this.tooltipLoad.width) {
            x = this.progressList.width - this.tooltipLoad.width;
        }

        gsap.to(this.tooltipLoad, {
            pixi: {
                x: x
            }
        });

        this.tooltipText.text = msg;
        this.tooltipText.position.set((this.tooltipLoad.width - this.tooltipText.width) / 2, -35 + (this.tooltipLoad.height - this.tooltipText.height) / 2);
    }

    public updateTextTimePlay = (time: string) => {
        this.textTimePlay.text = time;
    }

    public updateTextTimeTotal = (time: string) => {
        this.textTimeTotal.text = time;
    }

    public showBtnReplay = () => {
        this.btnReplay.visible = true;

        this.btnPlay.visible = this.btnPause.visible = false;
    }

    public showBtnPlay = () => {
        this.btnPlay.visible = true;

        this.btnReplay.visible = this.btnPause.visible = false;
    }

    public showBtnPause = () => {
        this.btnPause.visible = true;

        this.btnPlay.visible = this.btnReplay.visible = false;
    }

    public showBtnVolumeOn = () => {
        this.btnVolumeOn.visible = true;
        this.btnVolumeOff.visible = false;
    }

    public showBtnVolumeOff = () => {
        this.btnVolumeOn.visible = false;
        this.btnVolumeOff.visible = true;
    }

    // Mediator
    private dragging!: boolean;
    private dataMouse!: InteractionData | null;

    protected _initialize = () => {
        console.log("ControlView initialized!");

        this.interactive = true;

        // btnReplay
        this.btnReplay.interactive = true;
        this.btnReplay.buttonMode = true;
        this.btnReplay.cursor = "pointer";
        this.btnReplay.on(MouseEventName.Click, this.onClickReplay);
        this.btnReplay.on(MouseEventName.Over, this.onOverReplay);
        this.btnReplay.on(MouseEventName.Out, this.onOutReplay);

        // btnPlay
        this.btnPlay.interactive = true;
        this.btnPlay.buttonMode = true;
        this.btnPlay.cursor = "pointer";
        this.btnPlay.on(MouseEventName.Click, this.onClickPlay);
        this.btnPlay.on(MouseEventName.Over, this.onOverPlay);
        this.btnPlay.on(MouseEventName.Out, this.onOutPlay);

        // btnPause
        this.btnPause.interactive = true;
        this.btnPause.buttonMode = true;
        this.btnPause.cursor = "pointer";
        this.btnPause.on(MouseEventName.Click, this.onClickPause);
        this.btnPause.on(MouseEventName.Over, this.onOverPause);
        this.btnPause.on(MouseEventName.Out, this.onOutPause);

        // btnFullScreen
        this.btnFullScreen.interactive = true;
        this.btnFullScreen.buttonMode = true;
        this.btnFullScreen.cursor = "pointer";
        this.btnFullScreen.on(MouseEventName.Click, this.onClickFullScreen);
        this.btnFullScreen.on(MouseEventName.Over, this.onOverFullScreen);
        this.btnFullScreen.on(MouseEventName.Out, this.onOutFullScreen);

        // btnVolumeOn
        this.btnVolumeOn.interactive = true;
        this.btnVolumeOn.buttonMode = true;
        this.btnVolumeOn.cursor = "pointer";
        this.btnVolumeOn.on(MouseEventName.Click, this.onClickVolumeOn);
        this.btnVolumeOn.on(MouseEventName.Over, this.onOverVolumeOn);
        this.btnVolumeOn.on(MouseEventName.Out, this.onOutVolumeOn);

        // btnVolumeOff
        this.btnVolumeOff.interactive = true;
        this.btnVolumeOff.buttonMode = true;
        this.btnVolumeOff.cursor = "pointer";
        this.btnVolumeOff.on(MouseEventName.Click, this.onClickVolumeOff);
        this.btnVolumeOff.on(MouseEventName.Over, this.onOverVolumeOff);
        this.btnVolumeOff.on(MouseEventName.Out, this.onOutVolumeOff);

        this.dragging = false;
        this.dataMouse = null;

        this.circleScrubber.visible = true;
        this.circleScrubber.interactive = true;
        this.circleScrubber.buttonMode = true;
        this.circleScrubber.cursor = "pointer";
        this.circleScrubber.on(MouseEventName.Down, this.onDragStartCircleScrubber);
        this.circleScrubber.on(TouchEventName.Start, this.onDragStartCircleScrubber);
        this.circleScrubber.on(MouseEventName.Up, this.onDragEndCircleScrubber);
        this.circleScrubber.on(MouseEventName.UpOutside, this.onDragEndCircleScrubber);
        this.circleScrubber.on(TouchEventName.End, this.onDragEndCircleScrubber);
        this.circleScrubber.on(TouchEventName.EndOutside, this.onDragEndCircleScrubber);
        this.circleScrubber.on(MouseEventName.Move, this.onDragMoveCircleScrubber);
        this.circleScrubber.on(TouchEventName.Move, this.onDragMoveCircleScrubber);

        this.progressList.visible = true;
        this.progressList.interactive = true;
        this.progressList.buttonMode = true;
        this.progressList.cursor = "pointer";
        this.progressList.on(MouseEventName.Move, this.onMoveProgressList);
        this.progressList.on(MouseEventName.Out, this.onOutProgressList);
        this.progressList.on(MouseEventName.Click, this.onClickProgressList);

        this.tooltipLoad.visible = false;

        this.btnChangeSpeed.on(Dropdown.eventChange, this.onChangeSpeed);

        this.event.on('loadingChangeSignal', this.changeLoading);
        this.event.on('iframeChangeSignal', this.changeFrame);
        this.event.on('timePlayChangeSignal', this.changeTimePlay);
        this.event.on('timeTotalChangeSignal', this.changeTimeTotal);
        this.event.on('showReplaySignal', this.showReplay);
        this.event.on('showPlaySignal', this.showPlay);
        this.event.on('showPauseSignal', this.showPause);
    }

    public onDragStartCircleScrubber = (e: InteractionEvent) => {
        this.dragging = true;
        this.dataMouse = e.data;
    };

    public onDragEndCircleScrubber = () => {
        this.dragging = false;
        this.dataMouse = null;
    };

    public onDragMoveCircleScrubber = () => {
        if (this.dragging) {
            let newPosX: number = this.dataMouse!.getLocalPosition(this.parent).x;

            if (newPosX < this.progressList.x) {
                newPosX = 0;
            }

            if (newPosX > this.progressWidth) {
                newPosX = this.progressWidth;
            }

            this.updatePositionCircleScrubber(newPosX);
            this.updateWidthProgressPlay(newPosX);

            if (Store.player!.totalCount) {
                const percent = Math.round((newPosX / this.progressWidth) * 100);
                const index = Math.round(percent / 100 * Store.player!.totalCount);

                this.event.emit('videoReplaySignal', index);
                this.event.emit('videoPauseSignal');
                this.showBtnPlay();
                this.event.emit('videoRenderImageSignal', index);
                this.event.emit('timePlayChangeSignal', index);
            }
        }
    };

    public onMoveProgressList = (e: InteractionEvent) => {
        if (e.target instanceof Graphics && e.target.name === "progressList") {
            let newPosX: number = e.data.getLocalPosition(this.parent).x;

            newPosX = newPosX - 18;

            if (newPosX < this.progressList.x) {
                newPosX = 0;
            }

            if (newPosX > this.progressWidth) {
                newPosX = this.progressWidth;
            }

            this.updateWidthProgressHover(newPosX);

            let percent = Math.round((newPosX / this.progressWidth) * 100);
            let index = percent / 100 * Store.player!.totalCount;
            let cv = Utils.convertSecondByFrame(index);

            this.tooltipLoad.visible = true;
            this.updateXTooltipLoad(newPosX, `${cv.minute}:${cv.second}`);
        }
    };

    public onOutProgressList = () => {
        this.updateWidthProgressHover(0);
        this.tooltipLoad.visible = false;
    };

    public onClickProgressList = () => {
        console.log('ControlMediator.onClickProgressList');

        const newPosX = this.progressHover.width;

        this.updateWidthProgressPlay(newPosX);
        this.updatePositionCircleScrubber(newPosX);

        if (Store.player!.totalCount) {
            const percent = Math.round((newPosX / this.progressWidth) * 100);
            const index = Math.round(percent / 100 * Store.player!.totalCount);

            this.event.emit('videoReplaySignal', index);
            this.event.emit('videoPauseSignal');
            this.showBtnPlay();
            this.event.emit('videoRenderImageSignal', index);
            this.event.emit('timePlayChangeSignal', index);
        }
    };

    public onChangeSpeed = (value: number) => {
        this.event.emit('speedChangeSignal', value);
    }

    public onClickReplay = () => {
        console.log('btnReplay.onClickReplay');

        this.showBtnPause();
        this.event.emit('videoReplaySignal', 0);
    }

    public onOverReplay = () => {
        console.log('btnReplay.onOverReplay');

        this.btnReplay.pivot.set(2, 2);
        this.btnReplay.scale.set(1.2, 1.2);
    }

    public onOutReplay = () => {
        console.log('btnReplay.onOutReplay');

        this.btnReplay.scale.set(1, 1);
        this.btnReplay.pivot.set(0, 0);
    }

    public onClickPlay = () => {
        console.log('btnPlay.onClickPlay');

        this.showBtnPause();
        this.event.emit('videoPlaySignal');
    }

    public onOverPlay = () => {
        console.log('btnPlay.onOverPlay');

        this.btnPlay.pivot.set(2, 2);
        this.btnPlay.scale.set(1.2, 1.2);
    }

    public onOutPlay = () => {
        console.log('btnReplay.onOutPlay');

        this.btnPlay.scale.set(1, 1);
        this.btnPlay.pivot.set(0, 0);
    }

    public onClickPause = () => {
        console.log('btnPause.onClickPause');

        this.showBtnPlay();
        this.event.emit('videoPauseSignal');
    }

    public onOverPause = () => {
        console.log('btnPause.onOverPause');

        this.btnPause.pivot.set(2, 2);
        this.btnPause.scale.set(1.2, 1.2);
    }

    public onOutPause = () => {
        console.log('btnPause.onOutPause');

        this.btnPause.scale.set(1, 1);
        this.btnPause.pivot.set(0, 0);
    }

    public onClickVolumeOn = () => {
        console.log('btnVolumeOn.onClickVolumeOn');

        this.showBtnVolumeOff();
        this.event.emit('isSoundSignal');
    }

    public onOverVolumeOn = () => {
        console.log('btnVolumeOn.onOverVolumeOn');

        this.btnVolumeOn.pivot.set(2, 2);
        this.btnVolumeOn.scale.set(1.2, 1.2);
    }

    public onOutVolumeOn = () => {
        console.log('btnVolumeOn.onOutVolumeOn');

        this.btnVolumeOn.scale.set(1, 1);
        this.btnVolumeOn.pivot.set(0, 0);
    }

    public onClickVolumeOff = () => {
        console.log('btnVolumeOff.onClickVolumeOff');

        this.showBtnVolumeOn();
        this.event.emit('isSoundSignal');
    }

    public onOverVolumeOff = () => {
        console.log('btnVolumeOff.onOverVolumeOff');

        this.btnVolumeOff.pivot.set(2, 2);
        this.btnVolumeOff.scale.set(1.2, 1.2);
    }

    public onOutVolumeOff = () => {
        console.log('btnVolumeOff.onOutVolumeOff');

        this.btnVolumeOff.scale.set(1, 1);
        this.btnVolumeOff.pivot.set(0, 0);
    }

    public onOverFullScreen = () => {
        console.log('btnFullScreen.onOverFullScreen');

        this.btnFullScreen.pivot.set(2, 2);
        this.btnFullScreen.scale.set(1.2, 1.2);
    }

    public onOutFullScreen = () => {
        console.log('btnFullScreen.onOutFullScreen');

        this.btnFullScreen.pivot.set(0, 0);
        this.btnFullScreen.scale.set(1, 1);
    }

    public onClickFullScreen = async () => {
        if (screenfull.isEnabled) {
            if (screenfull.isFullscreen) {
                await screenfull.exit();
            } else {
                await screenfull.request();
            }
        }
    }

    public changeLoading = (percent: number) => {
        const width = (percent / 100) * this.progressWidth;

        this.updateWidthProgressLoad(width);
    }

    public changeFrame = (index: number, total: number) => {
        const percent = Math.round((index / total) * 100);
        const width = (percent / 100) * this.progressWidth;

        this.updateWidthProgressPlay(width);
        this.updatePositionCircleScrubber(width);
    }

    public changeTimePlay = (index: number) => {
        const cv = Utils.convertSecondByFrame(index);

        this.updateTextTimePlay(`${cv.minute}:${cv.second}`);
    }

    public changeTimeTotal = (index: number) => {
        const cv = Utils.convertSecondByFrame(index);

        this.updateTextTimeTotal(`${cv.minute}:${cv.second}`);
    }

    public showReplay = () => {
        this.showBtnReplay();
    }

    public showPlay = () => {
        this.showBtnPlay();
    }

    public showPause = () => {
        this.showBtnPause();
    }

    public destroy(): void {
        console.log("ControlView destroyed!");
    }
}
