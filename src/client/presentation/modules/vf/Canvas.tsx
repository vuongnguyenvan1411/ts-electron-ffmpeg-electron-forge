import {useEffect, useRef, useState} from "react";
import {TimelapseVideoFilterModel} from "../../../models/service/timelapse/TimelapseVideoFilterModel";
import Main, {IConfig} from "./main";

type ApplicationProps = {
    autoStart?: boolean;
    width?: number;
    height?: number;
    transparent?: boolean;
    autoDensity?: boolean;
    antialias?: boolean;
    preserveDrawingBuffer?: boolean;
    resolution?: number;
    forceCanvas?: boolean;
    backgroundColor?: number;
    clearBeforeRender?: boolean;
    powerPreference?: string;
    sharedTicker?: boolean;
    sharedLoader?: boolean;
    resizeTo?: Window | HTMLElement;
};

type Props = ApplicationProps & {
    item?: TimelapseVideoFilterModel;
    config: IConfig;
};

type TCanvas = {
    canvas: JSX.Element
    controller: Main | null,
}

const useCanvas = (
    {...props}: Props
): TCanvas => {
    const canvasRef = useRef(null);
    const [main, setMain] = useState<Main | null>(null);

    useEffect(() => {
        let _main: Main | null = null;
        if (props.item) {
            _main = new Main(props.item, props.config, canvasRef.current! as HTMLCanvasElement);
            setMain(_main)
        }

        return () => {
            if (_main) {
                _main.destroy();
            }
        }
        // eslint-disable-next-line
    }, [props.item])

    return {
        canvas: <canvas ref={canvasRef}/>,
        controller: main,
    }
}

export default useCanvas;
