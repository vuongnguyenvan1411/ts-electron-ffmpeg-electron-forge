import React from 'react';
import {IKrpanoConfig} from "../core/types";

export const useKrpano = (config: IKrpanoConfig): void => {
    React.useEffect(() => {
        const defaultConfig: Partial<IKrpanoConfig> = {
            target: 'auto',
            html5: 'only',
            xml: null,
            basepath: 'https://cdn-gdt.autotimelapse.com/raw/test/vtour-19-n/',
            consolelog: process.env.NODE_ENV === 'development',
        };
        const embedpano = () => {
            if (typeof window.embedpano === 'function') {
                return window.embedpano(Object.assign({}, defaultConfig, config))
            }
        };

        if (typeof window.embedpano === 'function') {
            embedpano();
        } else {
            // TODO: install krpano
            throw new Error('Krpano required');
        }
    }, [config]);
};