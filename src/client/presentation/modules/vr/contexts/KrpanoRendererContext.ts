import React from 'react';
import KrpanoActionProxy from '../core/KrpanoActionProxy';

export const KrpanoRendererContext = React.createContext<KrpanoActionProxy | null>(null);