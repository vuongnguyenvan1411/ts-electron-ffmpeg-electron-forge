import React, { useContext } from 'react';
import { KrpanoRendererContext } from '../contexts/KrpanoRendererContext';

/**
 * @see https://krpano.com/docu/xml/#view
 */
export interface SkinSettingProps {
    maps?: string
    maps_type?: string
    maps_bing_api_key?: string
    maps_google_api_key?: string
    maps_zoombuttons?: string
    maps_loadonfirstuse?: string
    gyro?: string
    gyro_keeplookingdirection?: string
    webvr?: string
    webvr_keeplookingdirection?: string
    webvr_prev_next_hotspots?: string
    autotour?: string
    littleplanetintro?: string
    followmousecontrol?: string
    title?: string
    thumbs?: string
    thumbs_width?: string
    thumbs_height?: string
    thumbs_padding?: string
    thumbs_crop?: string
    thumbs_opened?: string
    thumbs_text?: string
    thumbs_dragging?: string
    thumbs_onhoverscrolling?: string
    thumbs_scrollbuttons?: string
    thumbs_scrollindicator?: string
    thumbs_loop?: string
    tooltips_buttons?: string
    tooltips_thumbs?: string
    tooltips_hotspots?: string
    tooltips_mapspots?: string
    deeplinking?: string
    loadscene_flags?: string
    loadscene_blend?: string
    loadscene_blend_prev?: string
    loadscene_blend_next?: string
    loadingtext?: string
    layout_width?: string
    layout_maxwidth?: string
    controlbar_width?: string
    controlbar_height?: string
    controlbar_offset?: string
    controlbar_offset_closed?: string
    controlbar_overlap_no_fractionalscaling?: string
    controlbar_overlap_fractionalscaling?: string
    design_skin_images?: string
    design_bgcolor?: string
    design_bgalpha?: string
    design_bgborder?: string
    design_bgroundedge?: string
    design_bgshadow?: string
    design_thumbborder_bgborder?: string
    design_thumbborder_padding?: string
    design_thumbborder_bgroundedge?: string
    design_text_css?: string
    design_text_shadow?: string
}

const defaultProps = {
    maps: "true",
    maps_type: "google",
    maps_bing_api_key: "",
    maps_google_api_key: "",
    maps_zoombuttons: "false",
    maps_loadonfirstuse: "true",
    gyro: "true",
    gyro_keeplookingdirection: "false",
    webvr: "true",
    webvr_keeplookingdirection: "true",
    webvr_prev_next_hotspots: "true",
    autotour: "false",
    littleplanetintro: "true",
    followmousecontrol: "false",
    title: "true",
    thumbs: "true",
    thumbs_width: "120" ,
    thumbs_height: "80" ,
    thumbs_padding: "10" ,
    thumbs_crop: "0|40|240|160",
    thumbs_opened: "false",
    thumbs_text: "false",
    thumbs_dragging: "true",
    thumbs_onhoverscrolling: "false",
    thumbs_scrollbuttons: "false",
    thumbs_scrollindicator: "false",
    thumbs_loop: "false",
    tooltips_buttons: "false",
    tooltips_thumbs: "false",
    tooltips_hotspots: "false",
    tooltips_mapspots: "false",
    deeplinking: "false",
    loadscene_flags: "MERGE",
    loadscene_blend: "OPENBLEND(0.5, 0.0, 0.75, 0.05, linear)",
    loadscene_blend_prev: "SLIDEBLEND(0.5, 180, 0.75, linear)",
    loadscene_blend_next: "SLIDEBLEND(0.5,   0, 0.75, linear)",
    loadingtext: "loading...",
    layout_width: "100%",
    layout_maxwidth: "814",
    controlbar_width: "-24",
    controlbar_height: "40",
    controlbar_offset: "20",
    controlbar_offset_closed: "-40",
    controlbar_overlap_no_fractionalscaling: "10",
    controlbar_overlap_fractionalscaling: "0",
    design_skin_images: "vtourskin.png",
    design_bgcolor: "0x2D3E50",
    design_bgalpha: "0.8",
    design_bgborder: "0",
    design_bgroundedge: "1",
    design_bgshadow: "0 4 10 0x000000 0.3",
    design_thumbborder_bgborder: "3 0xFFFFFF 1.0",
    design_thumbborder_padding: "2",
    design_thumbborder_bgroundedge: "0",
    design_text_css: "color:#FFFFFF; font-family:Arial;",
    design_text_shadow: "1",
}

export const SkinSetting: React.FC<SkinSettingProps> = ({ children, ...defaultProps }) => {
    const renderer = useContext(KrpanoRendererContext);

    React.useEffect(() => {
        renderer?.setTag('skin_settings', null, { ...defaultProps });
    }, [renderer, defaultProps]);

    return <div className="skin_setting">{children}</div>;
};

export default SkinSetting;