import React from 'react';
import { KrpanoRendererContext } from '../contexts/KrpanoRendererContext';
import { EventCallback } from '../core/types';
import { mapEventPropsToJSCall } from '../core/utils';

export interface EventsConfig {
    /** Event name, if this parameter exists, it is a local event */
    name?: string;
    /** currently not supported */
    keep?: boolean;
    onEnterFullscreen?: EventCallback;
    onExitFullscreen?: EventCallback;
    onXmlComplete?: EventCallback;
    onPreviewComplete?: EventCallback;
    onLoadComplete?: EventCallback;
    onBlendComplete?: EventCallback;
    onNewPano?: EventCallback;
    onRemovePano?: EventCallback;
    onNewScene?: EventCallback;
    onXmlError?: EventCallback;
    onLoadError?: EventCallback;
    onKeydown?: EventCallback;
    onKeyup?: EventCallback;
    onClick?: EventCallback;
    onSingleClick?: EventCallback;
    onDoubleClick?: EventCallback;
    onMousedown?: EventCallback;
    onMouseup?: EventCallback;
    onMousewheel?: EventCallback;
    onContextmenu?: EventCallback;
    onIdle?: EventCallback;
    onViewChange?: EventCallback;
    onViewChanged?: EventCallback;
    onResize?: EventCallback;
    onFrameBufferResize?: EventCallback;
    onAutoRotateStart?: EventCallback;
    onAutoRotateStop?: EventCallback;
    onAutoRotateOneRound?: EventCallback;
    onAutoRotateChange?: EventCallback;
    onIPhoneFullscreen?: EventCallback;
}

export interface EventsProps extends EventsConfig {}

const GlobalEvents = '__GlobalEvents';

export const Events: React.FC<EventsProps> = ({ name, keep, children, ...EventsAttrs }) => {
    const renderer = React.useContext(KrpanoRendererContext);
    const EventSelector = React.useMemo(() => `events[${name || GlobalEvents}]`, [name]);

    // Bind the callback on the renderer
    React.useEffect(() => {
        renderer?.bindEvents(EventSelector, { ...EventsAttrs });

        return () => {
            renderer?.unbindEvents(EventSelector, { ...EventsAttrs });
        };
    }, [renderer, EventsAttrs, EventSelector]);

    // Add a js call to the Krpano tag to trigger an event
    React.useEffect(() => {
        renderer?.setTag(
            'events',
            // Global events set directly
            name || null,
            mapEventPropsToJSCall(
                { ...EventsAttrs },
                eventName => `js(${renderer.name}.fire(${eventName},${EventSelector}))`
            )
        );
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [EventSelector, name, renderer]);

    return <div className="events"></div>;
};

export default Events;