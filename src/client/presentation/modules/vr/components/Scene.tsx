import React, {useContext} from 'react';
import {CurrentSceneContext} from '../contexts/CurrentSceneContext';
import {KrpanoRendererContext} from '../contexts/KrpanoRendererContext';
import {buildXML, Logger, XMLMeta} from '../core/utils';

export interface SceneImage {
    type: string;
    url: string;
}

export interface SceneImageWithMultires {
    type?: string;
    url?: string;
    // multires props
    tiledImageWidth?: string;
    tiledImageHeight?: string;
    tileSize?: string;
    asPreview?: boolean;
}

export interface SceneProps {
    name: string;
    previewUrl?: string;
    /** Directly specify the xml content of the scene. When specified, other settings are ignored */
    content?: string;
    thumburl?: string;
    thumbx?: string;
    thumby?: string;
    lat?: string;
    lng?: string;
    heading?: string;
    /** Additional attributes of the image tag, only used in a few cases */
    imageTagAttributes?: Record<string, string | number | boolean>;
    /** The image contained in the Scene. When the length of the array is greater than 1, it is parsed into multiple levels by multires */
    images?: [SceneImage] | SceneImageWithMultires[];
}

export const Scene: React.FC<SceneProps> = ({
                                                name,
                                                thumburl,
                                                thumbx,
                                                thumby,
                                                previewUrl,
                                                lat,
                                                lng,
                                                heading,
                                                imageTagAttributes = {},
                                                images = [],
                                                content,
                                                children,
                                            }) => {
    const renderer = useContext(KrpanoRendererContext);
    const currentScene = useContext(CurrentSceneContext);

    React.useEffect(() => {
        const contentImageMeta: XMLMeta = {
            tag: 'image',
            attrs: imageTagAttributes,
            children: [],
        };

        // multires
        if (images.length > 1) {
            contentImageMeta.children!.push(
                ...(images as SceneImageWithMultires[]).map(
                    ({tiledImageWidth, tiledImageHeight, tileSize, asPreview = false, type, ...img}) => {
                        const imgXML: XMLMeta = {
                            tag: 'level',
                            // FIXME: Users of tiledImageWidth and other values may not necessarily provide it, and need to check, prompt and fallback
                            attrs: {
                                tiledImageWidth: tiledImageWidth ?? '0',
                                tiledImageHeight: tiledImageHeight ?? '0',
                                asPreview,
                            },
                            children: [
                                {
                                    tag: type ?? '',
                                    attrs: {...img},
                                },
                            ],
                        };

                        if (tileSize) {
                            imgXML.attrs = Object.assign(imgXML.attrs, {tileSize});
                        }

                        return imgXML;
                    }
                )
            );
        } else if (images.length === 1) {
            const {type, ...img} = images[0] as SceneImage;

            contentImageMeta.children!.push({
                tag: type,
                attrs: {...img},
            });
        }

        renderer?.setTag('scene', name, {
            content:
                content ||
                `${previewUrl ? `<preview url="${previewUrl}" />` : ''}${
                    images.length > 0 ? buildXML(contentImageMeta) : ''
                }`,
            thumburl: thumburl,
            thumbx: thumbx,
            thumby: thumby,
            lat: lat,
            lng: lng,
            heading: heading
        });

        return () => {
            renderer?.removeScene(name);
        };
    }, [renderer, name, images, imageTagAttributes, content, lat, lng, heading, thumby, thumbx, previewUrl, thumburl]);

    React.useEffect(() => {
        if (currentScene === name) {
            renderer?.loadScene(name);
            Logger.log(`Scene ${name} loaded due to currentScene change.`);
        }
    }, [name, renderer, currentScene]);

    return <div className="scene">{currentScene === name ? children : null}</div>;
};

export default Scene;