import KrpanoActionProxy from './KrpanoActionProxy';

/**
 * @see https://krpano.com/docu/html/#wmode
 */
export interface IKrpanoConfig {
    /**
     * Panorama xml path. It needs to be manually set to null to not load
     * @see https://krpano.com/docu/html/#xml
     */
    xml?: string | null;
    /**  mount point id */
    target: string;
    swf?: string;
    id?: string;
    bgcolor?: string;
    /**
     * @see https://krpano.com/docu/html/#html5
     */
    html5?: string;
    flash?: string;
    wmode?: string;
    localfallback?: string;
    vars?: Record<string, unknown>;
    initvars?: Record<string, unknown>;
    consolelog?: boolean;
    basepath?: string;
    mwheel?: boolean;
    capturetouch?: boolean;
    focus?: boolean;
    webglsettings?: Record<string, unknown>;
    webxr?: string;
    mobilescale?: number;
    touchdevicemousesupport?: boolean;
    fakedevice?: string;
    onready?: (renderer: NativeKrpanoRendererObject) => void;
}

export interface NativeKrpanoRendererObject {
    get(key: string): any;
    call(action: string): void;
}

export type EventCallback = (renderer: KrpanoActionProxy) => void;

declare global {
    interface Window {
        embedpano?: (config: IKrpanoConfig) => void;
    }
}