import escapeHTML from 'escape-html';

type FuncName = 'set' | 'loadxml' | 'loadscene' | 'addhotspot' | 'removehotspot' | 'nexttick';

/**
 * execute a single function
 * @param func Function name
 * @param params parameter list
 */
export const buildKrpanoAction = (func: FuncName, ...params: Array<string | number | boolean>): string =>
    `${func}(${params.map(p => `${p}`).join(', ')});`;

/**
 * Dynamically add tags for
 * @see https://krpano.com/forum/wbb/index.php?page=Thread&threadID=15873
 */
export const buildKrpanoTagSetterActions = (
    name: string,
    attrs: Record<string, string | boolean | number | undefined>
): string =>
    Object.keys(attrs)
        .map(key => {
            const val = attrs[key];
            key = key.toLowerCase();
            if (val === undefined) {
                return '';
            }
            // If there are double quotes in the attribute value, you need to use single quotes instead
            let quote = '"';
            if (val.toString().includes(quote)) {
                // eslint-disable-next-line quotes
                quote = "'";
            }
            if (key === 'style') {
                return `assignstyle(${name}, ${val});`;
            }
            if (typeof val === 'boolean' || typeof val === 'number') {
                return `set(${name}.${key}, ${val});`;
            }
            // content is XML text and cannot be escaped, since no user input is involved or required
            return `set(${name}.${key}, ${quote}${key === 'content' ? val : escapeHTML(val.toString())}${quote});`;
        })
        .filter(str => !!str)
        .join('');

export const Logger = {
    enabled: false,
    log: (...args: any[]): void => {
        /* istanbul ignore next */
        if (Logger.enabled && process.env.NODE_ENV === 'development') {
            console.log(...args);
        }
    },
};

export interface XMLMeta {
    tag: string;
    attrs: Record<string, string | number | boolean>;
    children?: XMLMeta[];
}

/**
 * Build xml from metadata
 */
export const buildXML = ({ tag, attrs, children }: XMLMeta): string => {
    const attributes = Object.keys(attrs)
        .map(key => `${key.toLowerCase()}="${attrs[key]}"`)
        .join(' ');

    if (children && children.length) {
        return `<${tag} ${attributes}>${children.map(child => buildXML(child)).join('')}</${tag}>`;
    }
    return `<${tag} ${attributes} />`;
};

/**
 * Map operation on Object
 */
export const mapObject = (
    obj: Record<string, unknown>,
    mapper: (key: string, value: unknown) => Record<string, unknown>
): Record<string, unknown> => {
    return Object.assign(
        {},
        ...Object.keys(obj).map(key => {
            const value = obj[key];
            return mapper(key, value);
        })
    );
};

/**
 * Mainly used to bind Krpano events and js calls. Extract the onXXX attribute in an object and replace it with the corresponding call string, discarding other attributes
 */
export const mapEventPropsToJSCall = (
    obj: Record<string, unknown>,
    getString: (key: string, value: unknown) => string
): Record<string, string> =>
    mapObject(obj, (key, value) => {
        if (key.startsWith('on') && typeof value === 'function') {
            return { [key]: getString(key, value) };
        }
        return {};
    }) as Record<string, string>;