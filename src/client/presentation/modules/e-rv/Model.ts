import moment from "moment";
import {App} from "../../../const/App";
import {Normalize} from "../../../core/Normalize";
import {E_ResUrlType} from "../../../const/Events";
import {Utils} from "../../../core/Utils";

export class TimelapseFilterModel {
    machineId?: number
    name?: string
    filter?: any
    first?: {
        name?: string
        order?: number
        width?: number
        height?: number
        dateShot?: string
    }
    last?: {
        name?: string
        order?: number
        width?: number
        height?: number
        dateShot?: string
    }
    total?: number
    timestamp?: number
    isDev?: boolean

    firstDateShotFormatted = (format: string = App.FormatToMoment): string => {
        if (this.first?.dateShot) {
            return moment(this.first?.dateShot, App.FormatISOFromMoment).format(format)
        } else {
            return ''
        }
    }

    lastDateShotFormatted = (format: string = App.FormatToMoment): string => {
        if (this.last?.dateShot) {
            return moment(this.last?.dateShot, App.FormatISOFromMoment).format(format)
        } else {
            return ''
        }
    }

    constructor(data: Record<string, any>) {
        this.machineId = Normalize.initJsonNumber(data, 'm')
        this.name = Normalize.initJsonString(data, 'n')
        this.filter = Normalize.initJsonObject(data, 'f')
        this.first = Normalize.initJsonObject(data, 'pf', (item: any) => ({
            name: Normalize.initJsonString(item, ['n', 'name']),
            order: Normalize.initJsonNumber(item, ['o', 'order']),
            width: Normalize.initJsonNumber(item, ['w', 'width']),
            height: Normalize.initJsonNumber(item, ['h', 'height']),
            dateShot: Normalize.initJsonString(item, ['ds', 'date_shot']),
        }))
        this.last = Normalize.initJsonObject(data, 'pl', (item: any) => ({
            name: Normalize.initJsonString(item, ['n', 'name']),
            order: Normalize.initJsonNumber(item, ['o', 'order']),
            width: Normalize.initJsonNumber(item, ['w', 'width']),
            height: Normalize.initJsonNumber(item, ['h', 'height']),
            dateShot: Normalize.initJsonString(item, ['ds', 'date_shot']),
        }))
        this.total = Normalize.initJsonNumber(data, 't')
        this.timestamp = Normalize.initJsonNumber(data, 'e')
        this.isDev = Normalize.initJsonBool(data, 'is_dev')
    }
}

export class TimelapseVideoImageModel {
    id?: string
    name?: string
    order?: number
    private readonly dateShot?: string

    dateShotFormatted = (format: string = App.FormatToMoment): string | undefined => {
        if (this.dateShot) {
            return moment(this.dateShot, App.FormatISOFromMoment).format(format)
        } else {
            return undefined
        }
    }

    getFilename = (ext: string) => {
        if (!this.name) {
            return ''
        }

        const split = this.name.split('.')
        const [name] = split

        return `${name}.${ext}`
    }

    getShotImageUrl = (options: {
        rs?: boolean,
        id?: string,
        name?: string,
        timeout?: number,
        type: E_ResUrlType,
        size?: 426 | 1280 | 1920,
        attach?: Record<string, any>
    }) => {
        const rs = options.rs ?? false
        const id = options.id ?? this.id
        const name = options.name ?? this.name
        const dateShot = this.dateShotFormatted(App.FormatFromDateTime)

        if (!dateShot || !id) {
            return undefined
        }

        const typeData: any[] = [options.type]

        if (options.size) {
            typeData.push(options.size)
        }

        if (options.attach) {
            typeData.push(options.attach)
        }

        return Utils.assetCdnGs(
            'si',
            {
                rs: rs,
                i: id,
                n: name,
                t: typeData,
                ds: dateShot,
                ...(
                    options.timeout && {
                        e: options.timeout
                    }
                )
            }
        )
    }

    constructor(data: Record<string, any>) {
        this.id = Normalize.initJsonString(data, 'id')
        this.name = Normalize.initJsonString(data, 'name')
        this.order = Normalize.initJsonNumber(data, 'order')
        this.dateShot = Normalize.initJsonString(data, 'date_shot')
    }
}
