import {memo, useCallback, useEffect, useRef, useState} from "react";
import {Color} from "../../../const/Color";
import {RVLayout} from "./RVLayout";
import {Alert, Button, Col, Divider, Input, notification, Progress, Row, Select, Slider, Spin, Table, Typography} from "antd";
import moment from "moment";
import {ArrowRightOutlined, FolderOpenOutlined, LoadingOutlined, VideoCameraAddOutlined} from "@ant-design/icons";
import {E_ResUrlType, SendingStatus} from "../../../const/Events";
import {App} from "../../../const/App";
import {TimelapseFilterModel, TimelapseVideoImageModel} from "./Model";
import {Api} from "./Api";
import {NextTronCmdName, NextTronEventName, T_AtlRenderVideoProgressDownload, T_AtlRenderVideoProgressRender, T_SystemGetOsInfo, T_SystemMonitorOs} from "../../../../electron/events";
import {ColumnsType} from "antd/es/table";
import {Utils} from "../../../core/Utils";
import {kebabCase} from "lodash";
import {useTranslation} from "react-i18next";

type _T_VideoFilterState = {
    isLoading: SendingStatus,
    item: TimelapseVideoImageModel[]
}

interface DataType {
    name: string
    description: any
}

type _T_ProgressDownload = {
    percent: number
    percentSuccess: number
    percentError: number
}

type _T_ProgressRender = {
    percent: number
    remaining?: number
}

const RenderVideoFC = (props: {
    filter: {}
}) => {
    const {t} = useTranslation()

    const filterModel: TimelapseFilterModel = new TimelapseFilterModel(props.filter)
    const initialState: _T_VideoFilterState = {
        isLoading: SendingStatus.idle,
        item: []
    }

    const [osInfo, setOsInfo] = useState<T_SystemGetOsInfo>()
    const [fps, setFps] = useState<number>(App.RV_FPS)
    const [selectDimensionValue, setSelectDimensionValue] = useState<number>(1)
    const keyDimension = useRef<string>('1280')
    const [isLoading, setIsLoading] = useState<SendingStatus>(initialState.isLoading)
    const [item, setItem] = useState<TimelapseVideoImageModel[]>(initialState.item)
    const [videoSize, setVideoSize] = useState<number>(0)
    const [cpuThreads, setCpuThreads] = useState<number>()
    const [videoName, setVideoName] = useState<string>(kebabCase(Utils.strSlug(filterModel.name!)))
    const [visible, setVisible] = useState<boolean>(false)
    const [dirSave, setDirSave] = useState<string>("")
    const [timeRemain,setTimeRemain] = useState<number>()

    const [ipcDownloadState, setIpcDownloadState] = useState<SendingStatus>(SendingStatus.idle)
    const [ipcDownloadProgress, setIpcDownloadProgress] = useState<_T_ProgressDownload>()

    const [ipcRenderState, setIpcRenderState] = useState<SendingStatus>(SendingStatus.idle)
    const [ipcRenderProgress, setIpcRenderProgress] = useState<_T_ProgressRender>()

    const [monitor, setMonitor] = useState<T_SystemMonitorOs>()

    useEffect(() => {
        console.log('%cMount Screen: RenderVideoFC', Color.ConsoleInfo)

        global.ipcRenderer.once(NextTronEventName.SystemGetPathComplete, (_, path) => {
            setDirSave(path)
        })
        global.ipcRenderer.once((NextTronEventName.SystemGetOsInfoComplete), (_, info: T_SystemGetOsInfo) => {
            setOsInfo(info)
            setCpuThreads(Math.round((info.cpu.threads) * 2 / 3))
        })
        global.ipcRenderer.send(NextTronCmdName.SystemGetPath, 'downloads')
        global.ipcRenderer.send(NextTronCmdName.SystemGetOsInfo)
        global.ipcRenderer.send(NextTronCmdName.SystemMonitorOs)

        // Choose folder
        global.ipcRenderer.on(NextTronEventName.FormInputGetPathComplete, (_, path) => {
            setDirSave(path)
        })

        global.ipcRenderer.on(NextTronEventName.SystemMonitorOsComplete, (_, info: T_SystemMonitorOs) => {
            setMonitor(info)
        })

        // Download
        global.ipcRenderer.on(NextTronEventName.AtlRenderVideoStartDownload, onIpcStartDownload)
        global.ipcRenderer.on(NextTronEventName.AtlRenderVideoProgressDownload, onIpcProgressDownload)
        global.ipcRenderer.on(NextTronEventName.AtlRenderVideoCompleteDownload, onIpcCompleteDownload)
        global.ipcRenderer.on(NextTronEventName.AtlRenderVideoErrorDownload, onIpcErrorDownload)
        global.ipcRenderer.on(NextTronEventName.AtlRenderVideoTimeRemainDownload, (_, time_remain ) => {
            setTimeRemain(time_remain)
        })

        // Render
        global.ipcRenderer.on(NextTronEventName.AtlRenderVideoStartRender, onIpcStartRender)
        global.ipcRenderer.on(NextTronEventName.AtlRenderVideoProgressRender, onIpcProgressRender)
        global.ipcRenderer.on(NextTronEventName.AtlRenderVideoCompleteRender, onIpcCompleteRender)
        global.ipcRenderer.on(NextTronEventName.AtlRenderVideoErrorRender, onIpcErrorRender)

        return () => {
            // Choose folder
            global.ipcRenderer.off(NextTronEventName.FormInputGetPathComplete, onChooseDirSave)

            // Choose Threads
            global.ipcRenderer.off(NextTronEventName.FormInputGetPathComplete, onChangeCpuThreads)

            // Download
            global.ipcRenderer.off(NextTronEventName.AtlRenderVideoStartDownload, onIpcStartDownload)
            global.ipcRenderer.off(NextTronEventName.AtlRenderVideoProgressDownload, onIpcProgressDownload)
            global.ipcRenderer.off(NextTronEventName.AtlRenderVideoCompleteDownload, onIpcCompleteDownload)
            global.ipcRenderer.off(NextTronEventName.AtlRenderVideoErrorDownload, onIpcErrorDownload)

            // Render
            global.ipcRenderer.off(NextTronEventName.AtlRenderVideoStartRender, onIpcStartRender)
            global.ipcRenderer.off(NextTronEventName.AtlRenderVideoProgressRender, onIpcProgressRender)
            global.ipcRenderer.off(NextTronEventName.AtlRenderVideoCompleteRender, onIpcCompleteRender)
            global.ipcRenderer.off(NextTronEventName.AtlRenderVideoErrorRender, onIpcErrorRender)

            console.log('%cUnmount Screen: RenderVideoFC', Color.ConsoleInfo)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const onIpcStartDownload = () => {
        setIpcDownloadState(SendingStatus.loading)
    }

    const onIpcProgressDownload = (_, progress: T_AtlRenderVideoProgressDownload) => {
        setIpcDownloadProgress({
            percent: progress.percent,
            percentSuccess: progress.percentSuccess,
            percentError: progress.percentError
        })
    }

    const onIpcCompleteDownload = () => {
        setIpcDownloadState(SendingStatus.complete)
    }

    const onIpcErrorDownload = (_, error: Error) => {
        console.log('onIpcErrorDownload', error)

        setIpcDownloadState(SendingStatus.error)
    }

    const onIpcStartRender = () => {
        setIpcRenderState(SendingStatus.loading)
    }

    const onIpcProgressRender = (_, progress: T_AtlRenderVideoProgressRender) => {
        setIpcRenderProgress({
            percent: progress.percent,
            remaining: progress.remaining
        })
    }

    const onIpcCompleteRender = (_, fileSize) => {
        setIpcRenderState(SendingStatus.complete)
        setIpcRenderProgress({
            percent: 100
        })
        setVideoSize(fileSize)

        notification.success({
            message: t('text.notify'),
            description: "Render video đã hoàn thành"
        })
    }

    const onIpcErrorRender = (_, error: Error) => {
        console.log('onIpcErrorRender', error.message)

        setIpcRenderState(SendingStatus.error)

        notification.error({
            message: t('text.notify'),
            description: "Render video lỗi"
        })
    }

    const onChangeSlider = (value: number) => {
        setFps(value);
    }

    const onOk = () => {
        getImage(1)
    }

    const onChooseDirSave = () => {
        global.ipcRenderer.send(NextTronCmdName.FormInputGetPath)
    }

    const onChangeCpuThreads = (value) => {
        setCpuThreads(value)
        global.ipcRenderer.send(NextTronCmdName.SystemGetOsInfo)
    }

    const getImage = useCallback((page: number) => {
        if (isLoading === SendingStatus.idle) {
            setIsLoading(SendingStatus.loading)
        }

        Api
            .getImages(filterModel.machineId!, {
                ...filterModel.filter,
                limit: 100,
                page: page
            })
            .then((r) => {
                if (r.success) {
                    const merge: TimelapseVideoImageModel[] = []

                    r.items?.map((e) => merge.push(new TimelapseVideoImageModel(e)))

                    setItem(item => [...item, ...merge])

                    if (r.meta?.pageCount === page) {
                        setIsLoading(SendingStatus.success)
                    } else {
                        getImage(r.meta?.nextPage!)
                    }
                }
            })

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])


    useEffect(() => {
        if (
            item && item.length > 0
            && isLoading === SendingStatus.success
            && ipcDownloadState === SendingStatus.idle
            && ipcRenderState === SendingStatus.idle
        ) {
            setItem([])
            setVisible(true)

            const size = parseInt(keyDimension.current)
            const isOrig = (size == lastWidth)

            global.ipcRenderer.send(NextTronCmdName.AtlRenderVideo, {
                fps: fps,
                threads: cpuThreads,
                size: size,
                first: filterModel.first,
                last: filterModel.last,
                output: dirSave,
                name: videoName,
                items: item.map(v => ({
                    id: v.id,
                    name: v.name,
                    order: v.order,
                    link: v.getShotImageUrl({
                        rs: true,
                        type: isOrig ? E_ResUrlType.Orig : E_ResUrlType.Thumb,
                        size: size as any
                    })
                }))
            })
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [item, isLoading])

    const lastWidth = filterModel.last?.width!
    const lastHeight = filterModel.last?.height!

    const ratio = lastWidth / lastHeight
    const listSize = [...App.RV_IMAGE_SIZE, lastWidth];

    const lstSelectDimension: { name: string, value: string }[] = listSize.map(value => {
        let name = `${value}x${Math.round(value / ratio)}`

        if (lastWidth && value == lastWidth) {
            name = `${Math.floor(lastWidth / 1000)}k ${name}`
        }

        return {
            name: name,
            value: value.toString()
        }
    })

    const handleChangeSelectDimension = (value: number) => {
        setSelectDimensionValue(value);

        const select = lstSelectDimension[value].value;

        if (select !== null) {
            keyDimension.current = select;
        }
    }

    const onChangeName = (e) => {
        setVideoName(e.target.value)
    }

    const tableInfoColumns: ColumnsType<DataType> = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name'
        },
        {
            title: 'Description',
            dataIndex: 'description',
            key: 'description'
        }
    ]

    const tableInfoData: DataType[] = [
        {
            name: 'FPS',
            description: ` ${fps} f/s`
        },
        {
            name: 'Số luồng CPU',
            description: `${cpuThreads}/${osInfo?.cpu.threads} luồng`
        },
        {
            name: 'Thư mục lưu trữ',
            description: dirSave
        },
        {
            name: 'Tên video',
            description: `${videoName}`
        },
        {
            name: 'Kích thước video',
            description: `${videoSize} MB`
        },
        {
            name: 'Thời gian',
            description: `${moment(filterModel.filter['filter_time_start'], "HH:mm:ss").format("HH:mm")} -> ${moment(filterModel.filter['filter_time_end'], "HH:mm:ss").format("HH:mm")}`
        },
        {
            name: 'Ngày ',
            description: `${filterModel.firstDateShotFormatted(App.FormatToDate)} --> ${filterModel.lastDateShotFormatted(App.FormatToDate)}`
        },
        {
            name: 'Số lượng ảnh',
            description: `${filterModel.total} ảnh`
        }
    ]

    const getSystemInfo = useCallback(() => {
        if (!osInfo) {
            return null
        }

        const langTime = {
            ending: '',
            year: t("text.year").toLowerCase(),
            day: t("text.day").toLowerCase(),
            hour: t("text.hour").toLowerCase(),
            minute: t("text.minute").toLowerCase(),
            second: t("text.second").toLowerCase()
        }

        return (
            <div className={'w-full'}>
                <div className={"text-center mb-3"}>Thông tin hệ thống</div>
                {
                    monitor && (
                        <div className={'flex justify-center items-center gap-6'}>
                            <div>
                                <Progress
                                    strokeColor={{
                                        '0%': '#fa541c',
                                        '100%': '#ef141f',
                                    }}
                                    success={{percent: 50}}
                                    type="circle"
                                    percent={Math.round(monitor.ram.percent)}
                                />
                                <div className="text-center mt-3">Ram ({Utils.formatByte(osInfo.ram.total)})</div>
                            </div>
                            <div>
                                <Progress
                                    type="circle"
                                    percent={Math.round(monitor.cpu.percent)}
                                    success={{percent: 30}}
                                />
                                <div className="text-center mt-3">CPU ({osInfo?.cpu.threads} luồng)</div>
                            </div>
                        </div>
                    )
                }
                <div>
                    {
                        ipcDownloadProgress
                            ? <div>
                                <Typography.Text>Tải dữ liệu</Typography.Text>
                                <Progress
                                    percent={Math.round(ipcDownloadProgress.percent)}
                                    success={{
                                        percent: Math.round(ipcDownloadProgress.percentSuccess)
                                    }}
                                />
                            </div>
                            : ipcDownloadState == SendingStatus.error
                                ? <Alert
                                    message={"Tải dữ liệu lỗi không thành công"}
                                    type="error"
                                    showIcon
                                />
                                : <Spin tip="Loading..." indicator={<LoadingOutlined spin/>}>
                                    <Alert
                                        message={"Tải dữ liệu"}
                                        description="Vui lòng đợi thông tin phản hồi tự hệ thống"
                                        type="info"
                                    />
                                </Spin>
                    }
                    {
                        timeRemain &&
                        <div>
                            <Typography.Text>Thời gian dự kiến tải xong </Typography.Text>
                            <div>{Math.round(timeRemain / 60)} minute {Math.round(timeRemain % 60)} seconds</div>
                        </div>
                    }
                </div>
                <div className={"mt-2"}>
                    {
                        ipcRenderProgress
                            ? <div>
                                <Typography.Text>
                                    Nén video
                                    {
                                        ipcRenderProgress.remaining !== undefined && (
                                            <span className={"ml-2"}>{Utils.formatTime(ipcRenderProgress.remaining, langTime)}</span>
                                        )
                                    }
                                </Typography.Text>
                                <Progress percent={Math.round(ipcRenderProgress.percent)}/>
                            </div>
                            : ipcRenderState == SendingStatus.error
                                ? <Alert
                                    message={"Nén video lỗi không thành công"}
                                    type="error"
                                    showIcon
                                />
                                : <Spin tip="Loading..." indicator={<LoadingOutlined spin/>}>
                                    <Alert
                                        message={"Nén video"}
                                        description="Vui lòng đợi thông tin phản hồi tự hệ thống"
                                        type="info"
                                    />
                                </Spin>
                    }
                </div>
            </div>
        )

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [osInfo, monitor, ipcDownloadProgress, ipcRenderProgress])

    return (
        <RVLayout>
            <Row justify={"center"}>
                <Col
                    xs={20} sm={18} md={18} lg={12} xl={12} xxl={12}
                    style={{
                        backgroundColor: "#fff"
                    }}
                >
                    <div
                        style={{
                            padding: "1rem 2rem"
                        }}
                    >
                        <Typography.Title
                            level={4}
                            style={{
                                marginBottom: "1rem",
                                textAlign: "center"
                            }}
                        >
                            {filterModel.name}
                        </Typography.Title>
                        {
                            visible
                                ? (
                                    <>
                                        {getSystemInfo()}
                                        <div className={'mt-4'}>
                                            <Table
                                                columns={tableInfoColumns}
                                                dataSource={tableInfoData.map((value, index) => ({...value, key: index}))}
                                                pagination={false}
                                                showHeader={false}
                                            />
                                        </div>
                                    </>
                                )
                                : (
                                    <>
                                        <Typography.Title
                                            level={5}
                                            style={{
                                                margin: "0",
                                                textAlign: "center"
                                            }}
                                        >
                                            {filterModel.firstDateShotFormatted(App.FormatToDate)}
                                            &nbsp;<ArrowRightOutlined/>&nbsp;
                                            {filterModel.lastDateShotFormatted(App.FormatToDate)}
                                        </Typography.Title>
                                        {
                                            filterModel.filter.hasOwnProperty('filter_time_start')
                                            && typeof filterModel.filter['filter_time_start'] === "string"
                                            && filterModel.filter['filter_time_start'].length > 0
                                            && filterModel.filter.hasOwnProperty('filter_time_end')
                                            && typeof filterModel.filter['filter_time_end'] === "string"
                                            && filterModel.filter['filter_time_end'].length > 0
                                                ? <Typography.Title
                                                    level={5}
                                                    style={{
                                                        margin: "0",
                                                        textAlign: "center"
                                                    }}
                                                >
                                                    {moment(filterModel.filter['filter_time_start'], "HH:mm:ss").format("HH:mm")}
                                                    &nbsp;<ArrowRightOutlined/>&nbsp;
                                                    {moment(filterModel.filter['filter_time_end'], "HH:mm:ss").format("HH:mm")}
                                                </Typography.Title>
                                                : null
                                        }
                                        <Typography.Title
                                            level={5}
                                            style={{
                                                margin: "0",
                                                marginBottom: "1rem",
                                                textAlign: "center"
                                            }}
                                        >
                                            {filterModel.total}&nbsp;ảnh
                                        </Typography.Title>
                                        <Typography.Title
                                            level={5}
                                            style={{
                                                margin: "0",
                                                textAlign: "center"
                                            }}
                                        >
                                            {filterModel.firstDateShotFormatted(App.FormatToDate)}
                                            &nbsp;<ArrowRightOutlined/>&nbsp;
                                            {filterModel.lastDateShotFormatted(App.FormatToDate)}
                                        </Typography.Title>
                                        <div>FPS (số khung hình trên giây)</div>
                                        <Slider
                                            defaultValue={30}
                                            min={24}
                                            max={60}
                                            marks={{
                                                24: "24fps",
                                                30: "30fps",
                                                60: "60fps"
                                            }}
                                            onChange={onChangeSlider}
                                        />
                                        <div
                                            style={{
                                                textAlign: "center",
                                                marginTop: "1rem",
                                            }}
                                        >
                                            Số giây của video: <span
                                            className={"font-bold"}>{Math.ceil(filterModel.total! / fps)}s</span>
                                        </div>
                                        <div style={{marginTop: "1rem"}}>Kích thước Video (Ảnh
                                            gốc: {`${filterModel.last?.width}x${filterModel.last?.height}`})
                                        </div>
                                        <Select
                                            style={{
                                                width: "100%",
                                                marginTop: "0.4rem"
                                            }}
                                            value={selectDimensionValue}
                                            onChange={handleChangeSelectDimension}
                                            // disabled={visible}
                                        >
                                            {
                                                lstSelectDimension.map((value, index) =>
                                                    <Select.Option key={`dimension-${index}`} value={index}>
                                                        <Typography.Text strong>{value.name}</Typography.Text>
                                                    </Select.Option>
                                                )
                                            }
                                        </Select>
                                        <Divider>Cấu hình Render</Divider>
                                        <div style={{marginTop: "1rem"}}>Tên của Video</div>
                                        <Input
                                            type={'text'}
                                            onChange={onChangeName}
                                            disabled={visible}
                                            value={videoName}
                                        />
                                        <div style={{marginTop: "1rem"}}>Thư mục lưu trữ</div>
                                        <div className={"mb-2"}>
                                            <Button
                                                onClick={onChooseDirSave}
                                                // disabled={visible}
                                                className={"mr-2"}
                                                size={"small"}
                                                icon={<FolderOpenOutlined/>}
                                            >
                                                Thay đổi
                                            </Button>
                                            {dirSave}
                                        </div>
                                        <div>Số luồng CPU</div>
                                        {
                                            (osInfo && osInfo.cpu) && (
                                                <Slider
                                                    defaultValue={Math.round((osInfo.cpu.threads * 2) / 3)}
                                                    min={1}
                                                    max={osInfo.cpu.threads}
                                                    marks={{
                                                        1: "1",
                                                        3: "3",
                                                        [osInfo.cpu.threads]: osInfo.cpu.threads.toString(),
                                                    }}
                                                    step={1}
                                                    onChange={onChangeCpuThreads}
                                                    // disabled={visible}
                                                />
                                            )
                                        }
                                        <Divider/>
                                        {
                                            (isLoading !== SendingStatus.success) && (
                                                <div style={{textAlign: "center"}}>
                                                    <Button
                                                        type={"primary"}
                                                        size={"large"}
                                                        loading={isLoading === SendingStatus.loading}
                                                        onClick={onOk}
                                                        icon={<VideoCameraAddOutlined/>}
                                                    >
                                                        Render
                                                    </Button>
                                                </div>
                                            )
                                        }
                                    </>
                                )
                        }
                    </div>
                </Col>
            </Row>
        </RVLayout>
    )
}

export default memo(RenderVideoFC)
