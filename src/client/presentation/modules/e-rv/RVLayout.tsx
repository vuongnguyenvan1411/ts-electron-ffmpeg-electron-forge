import React from "react";
import {Button, Layout} from "antd";
import {App} from "../../../const/App";
import {CloseOutlined} from "@ant-design/icons";
import {RouteAction} from "../../../const/RouteAction";
import {useNavigate} from "react-router";

export function RVLayout(props: {
    children: React.ReactNode
}) {
    const navigate = useNavigate()

    return (
        <Layout>
            <Layout.Header
                style={{
                    color: "#fff",
                    textAlign: "center",
                    fontSize: "1.6rem"
                }}
            >
                Online Video Rendering System
                <Button
                    className={"float-right mt-3"}
                    shape={"circle"}
                    icon={<CloseOutlined/>}
                    onClick={() => navigate(RouteAction.GoBack())}
                />
            </Layout.Header>
            <Layout.Content>
                {props.children}
            </Layout.Content>
            <Layout.Footer style={{textAlign: 'center'}}>
                ©2021 Design by <a href={App.Domain} target={"_blank"} rel={"noopener noreferrer"}>{App.Company}</a>
            </Layout.Footer>
        </Layout>
    )
}
