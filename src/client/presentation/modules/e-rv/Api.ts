import {ApiResModel} from "../../../models/ApiResModel";
import {EDFile} from "../../../core/encrypt/EDFile";
import {Color} from "../../../const/Color";
import axios, {AxiosRequestConfig} from "axios";
import {App} from "../../../const/App";
import moment from "moment";
import {EDData} from "../../../core/encrypt/EDData";
import {StoreConfig} from "../../../config/StoreConfig";

export class Api {
    protected static readonly Config = (isUp: boolean = false): AxiosRequestConfig => {
        const config: AxiosRequestConfig = {
            headers: {
                "Content-Type": isUp ? "multipart/form-data" : "text/json",
                "Platform": "web"
            },
            withCredentials: false
        }

        if (!isUp) {
            config.headers!.Accept = "application/json"
        }

        const apiToken = process.env.NEXT_PUBLIC_API_RV_TOKEN

        if (apiToken && apiToken.length > 0) {
            const et = EDData.setData({
                t: apiToken,
                // e: moment().add(30, 'seconds').unix()
                e: moment().add(1, 'days').unix()
            })

            config.headers!.Authorization = `Bearer ${et}`
        }

        const storeConfig = StoreConfig.getInstance()

        if (storeConfig.Agent && storeConfig.Agent.geoLocation && Object.keys(storeConfig.Agent.geoLocation).length > 0) {
            config.headers!['o-agent'] = EDData.setData({
                geoLocation: storeConfig.Agent.geoLocation
            })
        }

        return config
    }

    protected static GET(path: string, query?: any): Promise<ApiResModel> {
        const ep = EDFile.setLinkUrl({
            p: path,
            q: query,
            // e: moment(new Date()).add(1, 'hours').unix()
            e: moment().add(1, 'days').unix()
        })

        console.log('%c<-GET--------------------------------------------', Color.ConsoleInfo);
        console.log(`PATH: ${path}`);
        if (query) console.log(`QUERY:`, query);

        return axios
            .get(`${App.ApiRV}/${ep}`, Api.Config())
            .then(r => {
                const data = EDData.getData(r.data) ?? r.data

                console.log('RES:', data);
                console.log('%c--END------------------------------------------->', Color.ConsoleInfo);

                return new ApiResModel(data);
            })
    }

    public static getImages(mid: number, query: any): Promise<ApiResModel> {
        return Api.GET(`images/${mid}`, query)
    }
}
