import React, {createContext, useCallback, useContext, useEffect, useState} from "react";
import {ConfigModel, initialConfig} from "../../models/ConfigModel";
import {ConfigProvider} from "antd";
import {App} from "../../const/App";
import {getLng, initLng, resources} from "../../locales/i18n";
import {initReactI18next} from "react-i18next";
import {Color} from "../../const/Color";
import i18n from "i18next";
import {Locale} from "antd/lib/locale-provider";
import {findIndex} from "lodash";
import moment from "moment";
import 'moment/locale/vi';
import 'moment/locale/en-gb';
import 'moment/locale/zh-cn';
import antViVN from 'antd/lib/locale/vi_VN';
import antZhCN from 'antd/lib/locale/zh_CN';
import antEnGB from 'antd/lib/locale/en_GB';
import {TNextAppData} from "../../const/Types";
import {AgentData} from "../../models/AgentData";
import {engineName, engineVersion, osName, osVersion} from "react-device-detect";
import {StoreConfig} from "../../config/StoreConfig";
import {useInjection} from "inversify-react";
import axios from "axios";

export const ConfigContext = createContext<[ConfigModel, (config: ConfigModel) => void]>([initialConfig, () => {
    //
}])

export const useConfigContext = () => useContext(ConfigContext)

export const ConfigContextProvider: React.FC<{ data?: TNextAppData }> = (props) => {
    const [configState, setConfigState] = useState(initialConfig)
    const defaultConfigContext: [ConfigModel, typeof setConfigState] = [configState, setConfigState]
    const [localeAnt, setLocaleAnt] = useState<Locale>()
    const [init, setInit] = useState<boolean>()
    const storeConfig = useInjection(StoreConfig)

    useEffect(() => {
        console.log('%cInit: ConfigContextProvider', Color.ConsoleInfo)

        const lng: string = initLng();

        i18n.use(initReactI18next).init({
            lng: lng,
            resources
        }).then(() => console.log(`Init i18n: ${lng}`))

        const lang = App.Lang[findIndex(App.Lang, (o) => o.code === getLng())]

        moment.locale(lang.moment);

        if (lang.code === 'vi') {
            setLocaleAnt({...antViVN})
        } else if (lang.code === 'en') {
            setLocaleAnt({...antEnGB})
        } else if (lang.code === 'zh') {
            setLocaleAnt({...antZhCN})
        }

        // Set UserAgent
        let agentObj: any = {}

        if (props.data?.header && props.data.header.appAgent) {
            agentObj['isApp'] = true

            // if (props.data.header.appAgent.hasOwnProperty('osName') && props.data.header.appAgent.osName) {
            //     agentObj['osName'] = props.data?.header.osName
            // }

            if (props.data.header.appAgent.hasOwnProperty('osVersion') && props.data.header.appAgent.osVersion) {
                agentObj['osVersion'] = props.data?.header.osVersion
            }

            if (props.data.header.appAgent.hasOwnProperty('appVersion') && props.data.header.appAgent.appVersion) {
                agentObj['appVersion'] = props.data?.header.appVersion
            }
        } else {
            agentObj = {
                isApp: false,
                osVersion: osVersion
            }
        }

        agentObj = {
            ...agentObj,
            osName: osName,
            engineName: engineName,
            engineVersion: engineVersion
        }

        const agentData = new AgentData(agentObj)

        axios
            .get('https://api.db-ip.com/v2/free/self')
            .then(r => {
                if (r.data) {
                    agentData.geoLocation = r.data
                }

                setInit(true)
            })
            .catch(() => setInit(true))

        if (props.data?.more) {
            if (props.data.more.now) {
                agentData.svTime = props.data.more.now
            }
        }

        // add to config context
        initialConfig.agent = agentData

        // add to store
        storeConfig.Agent = agentData

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        //Do not use condition (configState.lang) because it always returns false
        if (typeof configState.lang === "number") {
            const lang = App.Lang[configState.lang]

            moment.locale(lang.moment)

            if (lang.code === 'vi') {
                setLocaleAnt({...antViVN})
            } else if (lang.code === 'en') {
                setLocaleAnt({...antEnGB})
            } else if (lang.code === 'zh') {
                setLocaleAnt({...antZhCN})
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [configState.lang])

    const getView = useCallback(() => {
        if (localeAnt) {
            return (
                <ConfigProvider locale={localeAnt}>
                    {props.children}
                </ConfigProvider>
            )
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [localeAnt?.locale, props.children])

    return (
        <ConfigContext.Provider value={defaultConfigContext}>
            {init === true && getView()}
        </ConfigContext.Provider>
    )
}
