import {useTranslation} from "react-i18next";
import {CHashids} from "../../core/CHashids";
import React, {useCallback, useEffect, useState} from "react";
import {Divider, Menu} from "antd";
import {useParams} from "react-router-dom";
import {RouteConfig} from "../../config/RouteConfig";
import {TTimelapseMachineParamState} from "../../const/Types";
import {useLocation, useNavigate} from "react-router";
import {ItemType} from "antd/es/menu/hooks/useItems";
import Image from "next/image";
import {Images} from "../../const/Images";
import styles from "../../styles/module/TimelapseSider.module.scss";

const TimelapseSiderLayout = ({...props}) => {
    const {t} = useTranslation()
    let navigate = useNavigate();
    const {hash} = useParams<{ hash: string }>()
    const location = useLocation()
    const [params, setParams] = useState<TTimelapseMachineParamState | undefined>(undefined);
    const siderState = {
        name: params?.name,
        isSensor: params?.isSensor,
        machines: params?.machines,
    }
    const hashDecode = CHashids.decode(hash!)
    const [projectId] = hashDecode
    const [menuActive, setMenuActive] = useState<string>('');

    useEffect(() => {
        setParams(location.state as TTimelapseMachineParamState | undefined ?? props.state);

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [location])

    useEffect(() => {
        if (location.pathname.indexOf(`${RouteConfig.TIMELAPSE}/view`) === 0 || location.pathname.indexOf(`${RouteConfig.TIMELAPSE}/machine`) === 0) {
            setMenuActive(`machine-${hash}`);
        } else if (location.pathname.indexOf(`${RouteConfig.TIMELAPSE}/setting`) === 0) {
            setMenuActive(`share`);
        } else if (location.pathname.indexOf(`${RouteConfig.TIMELAPSE}/info`) === 0) {
            setMenuActive(`info`);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [location])

    const _items = useCallback(() => {
        return params?.machines && params.machines.length > 0 ?
            params.machines.map<ItemType>((item, index) => {
                const hashPM = CHashids.encode([projectId, item.machineId]);

                return {
                    key: `machine-${hashPM}`,
                    label: item.name,
                    style: {
                        whiteSpace: 'normal',
                        height: 'auto',
                        lineHeight: "1.8rem",
                    },
                    onClick: () => navigate(`${RouteConfig.TIMELAPSE}/view/${hashPM}/image`, {
                        state: siderState,
                    }),
                    icon: <span className={"nil-icon"}>
                                           <span>{index < 9 ? '0' : ''}{index + 1}</span>
                                        </span>
                } as ItemType;
            }) : [];

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [params]);

    const _item2: ItemType[] = [
        {
            key: "info",
            label: t("text.info"),
            onClick: () => navigate(`${RouteConfig.TIMELAPSE}/info/${CHashids.encode(projectId)}`, {
                state: siderState,
            }),
            icon: <Image
                src={Images.iconGear.data}
                alt={Images.iconGear.atl}
                width={32}
                height={32}
                layout={"fixed"}
            />
        },
        {
            key: "share",
            label: t("text.share"),
            onClick: () => navigate(`${RouteConfig.TIMELAPSE}/setting/${CHashids.encode(projectId)}`, {
                state: siderState,
            }),
            icon: <span className={"anticon"}>
                <Image
                    src={Images.iconShareNetwork.data}
                    alt={Images.iconShareNetwork.atl}
                    width={32}
                    height={32}
                    layout={"fixed"}
                />
            </span>
        }
    ];

    return (
        <div className={styles.Sider}>
            <div className={"grow relative"}>
                <div className={"absolute inset-0 overflow-auto pt-10"}>
                    <Menu
                        className={"menu-main menu-border-r-none menu-border-selected-r-none menu-bg-transparent menu-item-no-icon menu-item-h-auto menu-item-p-no-icon menu-item-gap-3"}
                        mode="inline"
                        selectedKeys={[menuActive]}
                        items={_items()}
                    />
                </div>
            </div>
            <div className={"flex-none flex flex-col"}>
                <Divider className={"divider-main divider-border-half-dark50"}/>
                <Menu
                    className={"menu-main menu-bg-transparent menu-item-mb-4 menu-border-selected-r-none menu-border-r-none"}
                    mode="inline"
                    selectedKeys={[menuActive]}
                    items={_item2}
                />
                <div className={"h-10"}/>
            </div>
        </div>
    );
}

export default React.memo(TimelapseSiderLayout);
