import {CSSProperties, Key, memo, ReactNode, useEffect, useState} from "react";
import {CategoriesAction} from "../../recoil/blog/categories/CategoriesAction";
import {Color} from "../../const/Color";
import {Utils} from "../../core/Utils";
import {UrlQuery} from "../../core/UrlQuery";
import {MediaQuery} from "../../core/MediaQuery";
import {Style} from "../../const/Style";
import {SendingStatus} from "../../const/Events";
import {CommonEmptyFC, CommonLoadingSpinFC} from "../widgets/CommonFC";
import {Menu} from "antd";
import type {MenuProps} from 'antd/es/menu';
import {useLocation, useNavigate} from "react-router";
import {TPostsFilterVO} from "../../models/blog/BlogPostModel";
import {PostsAction} from "../../recoil/blog/posts/PostsAction";
import {RouteConfig} from "../../config/RouteConfig";

type MenuItem = Required<MenuProps>['items'][number];

function getItem(
    label: ReactNode,
    key?: Key | null,
    icon?: ReactNode,
    children?: MenuItem[],
    onClick?: () => void,
    style?: CSSProperties,
): MenuItem {
    return {
        key,
        icon,
        children,
        label,
        onClick,
        style,
    } as MenuItem;
}

const BlogCategoriesSiderLayout = () => {
    const navigate = useNavigate();
    let location = useLocation();
    const URL = new UrlQuery(location.search)
    const {
        vm,
        onLoadItems,
        resetStateWithEffect,
    } = CategoriesAction()

    const {
        onLoadItems: onFind,
    } = PostsAction()

    const filter = URL.get('filter', {})
    const sort = URL.get('sort')
    const order = URL.get('order')
    const page = URL.get('page')
    const limit = URL.get('limit')
    const categoryId = URL.get('category_id')

    const [queryParams, setQueryParams] = useState<TPostsFilterVO>({
        filter: filter,
        sort: sort,
        order: order,
        page: page,
        limit: limit,
        category_id: categoryId,
    })

    useEffect(() => {
        const filter = URL.get('filter', {})
        const sort = URL.get('sort')
        const order = URL.get('order')
        const page = URL.get('page')
        const limit = URL.get('limit')
        const categoryId = URL.get('category_id')

        setQueryParams({
            filter: filter,
            sort: sort,
            order: order,
            page: page,
            limit: limit,
            category_id: categoryId,
        })

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [location])

    useEffect(() => {
        console.log('%cMount Screen: BuildCategories', Color.ConsoleInfo)

        if (
            vm.items.length === 0
            || (vm.timestamp !== undefined && Utils.checkHourState(vm.timestamp))
        ) {
            if (vm.items.length > 0) resetStateWithEffect()
            const urlQueryParams = new UrlQuery()
            urlQueryParams.set('limit', (new MediaQuery(Style.GridLimit)).getPoint(vm.query.limit))
            onLoadItems(urlQueryParams.toObject())
        }

        return () => {
            console.log('%cUnmount Screen: BuildCategories', Color.ConsoleInfo)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])


    const onFilter = (data: { categoryId?: number }) => {
        if (!data.categoryId) return;
        const urlQueryParams = new UrlQuery(queryParams)
        urlQueryParams.set('category_id', data.categoryId)
        urlQueryParams.set('page', 1)

        onFind(urlQueryParams.toObject())

        navigate({
            pathname: `${RouteConfig.BLOG}`,
            search: urlQueryParams.toString()
        }, {
            replace: true
        })

        setQueryParams(urlQueryParams.toObject())
    }

    const _items: MenuItem[] = [
        ...vm.items.map((item, _) => getItem(
            item.name,
            item.categoryId,
            null,
            undefined,
            () => onFilter({categoryId: item.categoryId}),
            {
                whiteSpace: 'normal',
                height: 'auto',
                lineHeight: "1.8rem",
            }
        ))
    ]

    return (
        vm.isLoading === SendingStatus.loading
            ? <CommonLoadingSpinFC/>
            : vm.items.length > 0 ?
                <Menu
                    className={"menu-main menu-border-selected-r-none menu-border-r-none menu-blog"}
                    theme="light"
                    mode="inline"
                    items={_items}
                    selectedKeys={[queryParams.category_id?.toString() ?? '']}
                />
                : <CommonEmptyFC/>
    )
}

export default memo(BlogCategoriesSiderLayout);
