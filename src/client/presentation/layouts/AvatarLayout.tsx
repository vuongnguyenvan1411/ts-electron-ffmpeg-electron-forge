import {Avatar, Card, Dropdown, Menu, Modal} from 'antd';
import {useSessionContext} from "../contexts/SessionContext";
import styles from "../../styles/module/TopAccount.module.scss";
import {RouteConfig} from "../../config/RouteConfig";
import {Color} from "../../const/Color";
import Image from "next/image";
import NoAvatar from "../../assets/image/no_avatar.jpg";
import {EditOutlined, ExclamationCircleOutlined, LockOutlined, LogoutOutlined} from "@ant-design/icons";
import {LogoutScreen} from "../screens/account/LogoutScreen";
import {useTranslation} from "react-i18next";
import {useState} from "react";
import {CustomTypography} from "../components/CustomTypography";
import {useNavigate} from "react-router";


export const AvatarLayout = () => {
    const [session] = useSessionContext()
    const {t} = useTranslation()
    const [isModalLogoutVisible, setIsModalLogoutVisible] = useState(false)
    const navigate = useNavigate();

    const onClickLogout = () => {
        Modal.confirm({
            className: "ss",
            title: t('text.confirmLogout'),
            icon: <ExclamationCircleOutlined/>,
            okText: t('button.ok'),
            cancelText: t('button.close'),
            onOk() {
                setIsModalLogoutVisible(true)
            }
        })
    }

    const onCloseModalLogout = () => {
        setIsModalLogoutVisible(false);
    }

    return (
        <>
            <Dropdown
                overlayClassName={"dropdown-menu-main dropdown-atl-language"}
                overlay={
                    <Menu
                        className={`${styles.TopAccount} rounded-lg p-2`}
                        items={[
                            {
                                key: "user:me",
                                onClick: () => navigate(`${RouteConfig.ACCOUNT}/info`),
                                icon: <Card.Meta
                                    avatar={
                                        <Avatar
                                            src={session.user?.image}
                                            style={{
                                                backgroundColor: Color.Main,
                                            }}
                                            size={"large"}
                                            onError={() => true}
                                            icon={<Image src={NoAvatar} alt=""/>}
                                        />
                                    }
                                    title={session.user?.name}
                                    description={t('text.viewProfile')}
                                />
                            },
                            {
                                type: "divider",
                            },
                            {
                                key: "user:info",
                                icon: <EditOutlined/>,
                                label: t("text.editInformation"),
                                onClick: () => navigate(`${RouteConfig.ME_SETTINGS}?tab=account`),
                            },
                            {
                                key: "user:password",
                                icon: <LockOutlined/>,
                                label: t("title.editPassword"),
                                onClick: () => navigate(`${RouteConfig.ME_SETTINGS}?tab=security`),
                            },
                            {
                                type: "divider",
                            },
                            {
                                key: "user:logout",
                                icon: <LogoutOutlined/>,
                                label: t("text.logout"),
                                onClick: onClickLogout,
                            }
                        ]}
                    />
                }
                placement="bottomRight"
                trigger={['click']}
                arrow
            >
                <a>
                    <Avatar
                        style={{
                            backgroundColor: "white",
                            borderRadius: "5px",
                        }}
                        size={32}
                        shape="square"
                        src={session.user?.image}
                    />
                    <CustomTypography
                        className={"hidden md:block"}
                        style={{
                            color: "white"
                        }}
                        textStyle={"text-info"}
                    >
                        {session.user?.username}
                        {/*<br/>Gold Member*/}
                    </CustomTypography>
                </a>
            </Dropdown>
            {
                isModalLogoutVisible
                    ? <LogoutScreen onClose={onCloseModalLogout}/>
                    : null
            }
        </>
    )
}
