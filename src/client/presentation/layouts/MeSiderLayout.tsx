import {useTranslation} from "react-i18next";
import {EditOutlined, LockOutlined} from "@ant-design/icons";
import {Menu} from "antd";
import {RouteConfig} from "../../config/RouteConfig";
import React from "react";
import {UrlQuery} from "../../core/UrlQuery";
import {ItemType} from "antd/es/menu/hooks/useItems";
import {useNavigate} from "react-router";

type TTab = {
    key: string
    name: string
    icon: React.ReactNode
    replace: boolean
}


export const MeSiderLayout = () => {
    const {t} = useTranslation()
    let URL = new UrlQuery(location.search)
    let tab = URL.get('tab')
    let navigate = useNavigate();

    const tabs: TTab[] = [
        {
            key: 'account',
            name: t("text.editInformation"),
            icon: <EditOutlined/>,
            replace: true
        },
        {
            key: 'security',
            name: t("title.editPassword"),
            icon: <LockOutlined/>,
            replace: true
        }
    ]

    return (
        <div className={"py-6"}>
            <Menu
                className={"menu-main menu-border-r-none menu-border-selected-r-none menu-bg-transparent menu-item-no-icon menu-item-h-auto menu-item-p-no-icon menu-item-gap-3"}
                selectedKeys={[`tab-${tab}`]}
                mode="inline"
                items={tabs.map(e => {
                    return {
                        key: `tab-${e.key}`,
                        icon: e.icon,
                        onClick: () => navigate(`${RouteConfig.ME_SETTINGS}?tab=${e.key}`, {
                            replace: e.replace,
                        }),
                        label: e.name,
                    } as ItemType
                })}
            />
        </div>
    )
}
