import {memo, useEffect, useState} from "react";
import {Dropdown, Menu} from 'antd';
import MenuIcon from "../components/svgs/MenuIcon";
import NextImage from "next/image";
import {Images} from "../../const/Images";
import {TLocationLayout} from "../../const/Types";
import {useLocation, useNavigate} from "react-router";
import {RouteConfig} from "../../config/RouteConfig";
import {useTranslation} from "react-i18next";

const MenuLayout = () => {
    const {t} = useTranslation();
    let location = useLocation();
    const navigate = useNavigate();
    const [selectedLocation, setSelectedLocation] = useState<TLocationLayout>("home");

    useEffect(() => {
        if (location.pathname.indexOf(RouteConfig.TIMELAPSE) === 0) {
            setSelectedLocation("timelapse");
        } else if (location.pathname.indexOf(RouteConfig.GEODETIC) === 0) {
            setSelectedLocation("geodetic");
        } else if (location.pathname.indexOf(RouteConfig.VR360) === 0) {
            setSelectedLocation("vr360");
        } else if (location.pathname.indexOf(RouteConfig.BLOG) === 0) {
            setSelectedLocation("news");
        } else if (location.pathname.indexOf(RouteConfig.ABOUT) === 0) {
            setSelectedLocation("about");
        } else if (location.pathname.indexOf(RouteConfig.FEEDBACK) === 0) {
            setSelectedLocation("feedback");
        } else if (location.pathname.indexOf(RouteConfig.CAMERA) === 0) {
            setSelectedLocation("camera");
        } else if (location.pathname.indexOf(RouteConfig.WEIGHING) === 0) {
            setSelectedLocation("weighing");
        } else {
            setSelectedLocation("home");
        }
    }, [location])

    const menu = (
        <Menu
            selectedKeys={[selectedLocation]}
            items={[
                {
                    icon: <NextImage
                        height={19.48}
                        width={32}
                        src={Images.iconTimelapseFill.data}
                    />,
                    label: t("title.timelapse"),
                    key: 'timelapse',
                    onClick: () => navigate(RouteConfig.TIMELAPSE),
                },
                {
                    icon: <NextImage
                        height={32}
                        width={32}
                        src={Images.iconGeodeticFill.data}
                    />,
                    label: t("title.geodetic"),
                    key: 'geodetic',
                    onClick: () => navigate(RouteConfig.GEODETIC),
                },
                {
                    icon: <NextImage
                        height={32}
                        width={32}
                        src={Images.iconVr360Fill.data}
                    />,
                    label: t("title.vr360"),
                    key: 'vr360',
                    onClick: () => navigate(RouteConfig.VR360),
                },
                {
                    icon: <NextImage
                        height={32}
                        width={32}
                        src={Images.iconCameraFill.data}
                    />,
                    label: t("title.camera"),
                    key: 'camera',
                    onClick: () => navigate(RouteConfig.CAMERA),
                },
                {
                    icon: <NextImage
                        height={32}
                        width={32}
                        src={Images.iconWeighingFill.data}
                    />,
                    label: t("title.weighing"),
                    key: 'weighing',
                    onClick: () => navigate(RouteConfig.WEIGHING),
                },
                {
                    type: 'divider',
                },
                {
                    icon: <NextImage
                        height={32}
                        width={32}
                        src={Images.iconNewsFill.data}
                    />,
                    label: t("title.news"),
                    key: 'news',
                    onClick: () => navigate(RouteConfig.BLOG),
                },
                {
                    icon: <NextImage
                        height={32}
                        width={32}
                        src={Images.iconAboutFill.data}
                    />,
                    label: t("title.about"),
                    key: 'about',
                    onClick: () => navigate(RouteConfig.ABOUT),
                },
                {
                    icon: <NextImage
                        height={32}
                        width={32}
                        src={Images.iconFeedbackFill.data}
                    />,
                    label: t("title.feedback"),
                    key: 'feedback',
                    onClick: () => navigate(RouteConfig.FEEDBACK),
                },
            ]}
        />
    );

    return (
        <Dropdown
            arrow
            overlay={menu}
            overlayClassName={"dropdown-menu-main dropdown-atl-menu"}
            trigger={['click']}
        >
            <a onClick={e => e.preventDefault()}>
                <MenuIcon/>
            </a>
        </Dropdown>
    )
}

export default memo(MenuLayout);
