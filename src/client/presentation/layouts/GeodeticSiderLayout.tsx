import {useTranslation} from "react-i18next";
import React, {useEffect, useState} from "react";
import {Menu} from "antd";
import {useParams} from "react-router-dom";
import {RouteConfig} from "../../config/RouteConfig";
import {useLocation, useNavigate} from "react-router";
import {ItemType} from "antd/es/menu/hooks/useItems";
import Image from "next/image";
import {Images} from "../../const/Images";
import styles from "../../styles/module/TimelapseSider.module.scss";

const GeodeticSiderLayout = ({...props}) => {
    const {t} = useTranslation()
    let navigate = useNavigate();
    const {hash} = useParams<{ hash: string }>()
    const location = useLocation()
    const params: any | undefined = location.state ?? props.state;
    const [menuActive, setMenuActive] = useState<string>('');

    useEffect(() => {
        if (location.pathname.indexOf(`${RouteConfig.GEODETIC}/setting`) === 0) {
            setMenuActive(`share`);
        } else if (location.pathname.indexOf(`${RouteConfig.GEODETIC}/info`) === 0) {
            setMenuActive(`info`);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [location])

    const siderState = {
        name: params?.name,
        m2d: params?.m2d,
        m3d: params?.m3d,
        vr360: params?.vr360,
    }

    const _item: ItemType[] = [
        {
            key: "info",
            label: t("text.info"),
            onClick: () => navigate(`${RouteConfig.GEODETIC}/info/${hash}`, {
                state: siderState,
            }),
            icon: <Image
                src={Images.iconGear.data}
                alt={Images.iconGear.atl}
                width={32}
                height={32}
                layout={"fixed"}
            />
        },
        {
            key: "share",
            label: t("text.share"),
            onClick: () => navigate(`${RouteConfig.GEODETIC}/setting/${hash}`, {
                state: siderState,
            }),
            icon: <span className={"anticon"}>
                <Image
                    src={Images.iconShareNetwork.data}
                    alt={Images.iconShareNetwork.atl}
                    width={32}
                    height={32}
                    layout={"fixed"}
                />
            </span>
        }
    ];

    return (
        <div className={styles.Sider}>
            <div className={"py-10"}>
                <Menu
                    className={"menu-main menu-bg-transparent menu-item-mb-4 menu-border-selected-r-none menu-border-r-none"}
                    mode="inline"
                    selectedKeys={[menuActive]}
                    items={_item}
                />
            </div>
        </div>
    );
}

export default React.memo(GeodeticSiderLayout);