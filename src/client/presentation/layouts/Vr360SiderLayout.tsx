import {useTranslation} from "react-i18next";
import {useLocation, useNavigate} from "react-router";
import {useParams} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {RouteConfig} from "../../config/RouteConfig";
import {ItemType} from "antd/es/menu/hooks/useItems";
import Image from "next/image";
import {Images} from "../../const/Images";
import styles from "../../styles/module/TimelapseSider.module.scss";
import {Menu} from "antd";

export const Vr360SiderLayout = ({...props}) => {
    const {t} = useTranslation()
    let navigate = useNavigate();
    const {hash} = useParams<{ hash: string }>()
    const location = useLocation()
    const [params, setParams] = useState<any | undefined>(undefined);
    const [menuActive, setMenuActive] = useState<string>('');

    useEffect(() => {
        setParams(location.state as any | undefined ?? props.state);

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [location])

    useEffect(() => {
        if (location.pathname.indexOf(`${RouteConfig.VR360}/setting`) === 0) {
            setMenuActive(`share`);
        } else if (location.pathname.indexOf(`${RouteConfig.VR360}/view`) === 0) {
            setMenuActive(`info`);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [location])

    const _item: ItemType[] = [
        {
            key: "info",
            label: t("text.info"),
            onClick: () => navigate(`${RouteConfig.VR360}/view/${hash}`, {
                state: params,
            }),
            icon: <Image
                src={Images.iconGear.data}
                alt={Images.iconGear.atl}
                width={32}
                height={32}
                layout={"fixed"}
            />
        },
        {
            key: "share",
            label: t("text.share"),
            onClick: () => navigate(`${RouteConfig.VR360}/setting/${hash}`, {
                state: params,
            }),
            icon: <span className={"anticon"}>
                <Image
                    src={Images.iconShareNetwork.data}
                    alt={Images.iconShareNetwork.atl}
                    width={32}
                    height={32}
                    layout={"fixed"}
                />
            </span>
        }
    ];

    return (
        <div className={styles.Sider}>
            <Menu
                className={"menu-main menu-bg-transparent menu-item-mb-4 menu-border-selected-r-none menu-border-r-none"}
                mode="inline"
                selectedKeys={[menuActive]}
                items={_item}
            />
        </div>
    );
}