import React, {useEffect, useMemo, useState} from "react";
import styles from "../../styles/module/Test.module.scss"
import {Layout} from 'antd';
import CustomHeader from "./CustomHeader";
import {Outlet, useLocation} from "react-router";
import {BaseFooter} from "./BaseFooter";
import {RouteConfig} from "../../config/RouteConfig";
import {THeaderCtx, TOutletCtx} from "../../const/Types";
import {FeatureSingleton} from "../../models/FeatureSingleton";

export const MasterLayoutV2 = (props: {
    isBg?: boolean,
    clnContent?: string
}) => {
    let location = useLocation()
    const [isBg, setIsBg] = useState(true)
    const [header, setHeader] = useState<THeaderCtx | undefined>(undefined)

    useEffect(() => {
        if (
            location.pathname === RouteConfig.HOME
            || location.pathname === RouteConfig.DOWNLOAD
            || location.pathname === RouteConfig.ME_INFO
        ) {
            setIsBg(false);
        } else {
            setIsBg(true);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [location])

    const outletCtx: TOutletCtx = {
        header: {
            header: header,
            setHeader: setHeader
        }
    }

    return useMemo(() => {
        return (
            <Layout className={`${styles.Test} ${isBg ? styles.Bg : styles.NoBg}`}>
                {
                    FeatureSingleton.getInstance().isHeaderEmbedWebView
                        ? <CustomHeader
                            header={header}
                        />
                        : null
                }
                <Layout.Content className={`${styles.Content} ${props.clnContent ? props.clnContent : ''}`}>
                    <Outlet context={{outletCtx}}/>
                </Layout.Content>
                <Layout.Footer className={styles.Footer}>
                    <BaseFooter
                        style={{
                            color: isBg ? "#FFF" : "#969696"
                        }}
                    />
                </Layout.Footer>
            </Layout>
        )

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isBg, header])
}
