import {useTranslation} from "react-i18next";
import React, {useEffect, useState} from "react";
import {Divider, Menu} from "antd";
import {useParams} from "react-router-dom";
import {RouteConfig} from "../../config/RouteConfig";
import {useLocation, useNavigate} from "react-router";
import {ItemType} from "antd/es/menu/hooks/useItems";
import Image from "next/image";
import {Images} from "../../const/Images";
import styles from "../../styles/module/TimelapseSider.module.scss";

const GeodeticSiderLayoutTest = ({...props}) => {
    const {t} = useTranslation()
    let navigate = useNavigate();
    const {hash} = useParams<{ hash: string }>()
    const location = useLocation()
    const params: any | undefined = location.state ?? props.state;
    const [menuActive, setMenuActive] = useState<string>('');

    useEffect(() => {
        if (location.pathname.indexOf(`${RouteConfig.GEODETIC}/setting`) === 0) {
            setMenuActive(`share`);
        } else if (location.pathname.indexOf(`${RouteConfig.GEODETIC}/info`) === 0) {
            setMenuActive(`info`);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [location])

    const siderState = {
        name: params?.name,
        m2d: params?.m2d,
        m3d: params?.m3d,
        vr360: params?.vr360,
    }

    const _item: ItemType[] = [
        {
            key: "current",
            label: "Manager",
            onClick: () => navigate(`${RouteConfig.GEODETIC_MANAGER}`),
            icon: <Image
                src={Images.iconList.data}
                alt={Images.iconList.atl}
                width={32}
                height={32}
                layout={"fixed"}
            />
        },
        {
            key: "info",
            label: "Tải lên",
            onClick: () => navigate(`${RouteConfig.GEODETIC_MANAGER_UPLOAD}`),
            icon: <Image
                src={Images.iconUploadSimple.data}
                alt={Images.iconUploadSimple.atl}
                width={32}
                height={32}
                layout={"fixed"}
            />
        },
        {
            key: "delete",
            label: "Thùng rác",
            onClick: () => navigate(`${RouteConfig.GEODETIC_MANAGER_DELETED}`),
            icon: <span className={"anticon"}>
                <Image
                    src={Images.iconTrash.data}
                    alt={Images.iconTrash.atl}
                    width={32}
                    height={32}
                    layout={"fixed"}
                />
            </span>
        },
        {
            key: 'divider',
            label: "",
            icon: <Divider className={"set-tranformX divider-main divider-horizontal divider-border-half-dark50"}/>
        }
        ,
        {
            key: "memory",
            label: "Bộ nhớ",
            icon: <Image
                src={Images.iconWrenchWhite.data}
                alt={Images.iconWrenchWhite.atl}
                width={32}
                height={32}
                layout={"fixed"}
            />
        },
    ];

    return (
        <div className={styles.Sider}>
            <div className={"py-10"}>
                <Menu
                    className={"menu-main menu-bg-transparent menu-item-mb-4 menu-border-selected-r-none menu-border-r-none"}
                    mode="inline"
                    selectedKeys={[menuActive]}
                    items={_item}
                />
            </div>
        </div>
    );
}

export default React.memo(GeodeticSiderLayoutTest);