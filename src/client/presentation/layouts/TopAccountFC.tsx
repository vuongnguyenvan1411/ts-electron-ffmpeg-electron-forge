import {Avatar, Button, Card, Dropdown, Menu, Modal} from "antd";
import {Color} from "../../const/Color";
import {useState} from "react";
import {useSessionContext} from "../contexts/SessionContext";
import styles from '../../styles/module/TopAccount.module.scss';
import {useTranslation} from "react-i18next";
import {RouteConfig} from "../../config/RouteConfig";
import NoAvatar from "../../assets/image/no_avatar.jpg";
import {EditOutlined, ExclamationCircleOutlined, LockOutlined, LogoutOutlined} from "@ant-design/icons";
import {LogoutScreen} from "../screens/account/LogoutScreen";
import {Images} from "../../const/Images";
import Image from "next/image";
import {useNavigate} from "react-router";

export const TopAccountFC = () => {
    const [session] = useSessionContext()
    const {t} = useTranslation()
    const [isModalLogoutVisible, setIsModalLogoutVisible] = useState(false)
    const navigate = useNavigate();

    const onClickLogout = () => {
        Modal.confirm({
            title: t('text.confirmLogout'),
            icon: <ExclamationCircleOutlined/>,
            okText: t('button.ok'),
            cancelText: t('button.close'),
            onOk() {
                setIsModalLogoutVisible(true)
            }
        })
    }

    const onCloseModalLogout = () => {
        setIsModalLogoutVisible(false);
    }

    return (
        <>
            <Dropdown
                overlayClassName={"dropdown-menu-main dropdown-atl-language"}
                overlay={
                    <Menu
                        className={`${styles.TopAccount} rounded-lg p-2`}
                        items={[
                            {
                                key: "user:me",
                                onClick: () => navigate(`${RouteConfig.ACCOUNT}/info`),
                                icon: <Card.Meta
                                    className={"m-0"}
                                    avatar={
                                        <Avatar
                                            src={session.user?.image}
                                            style={{
                                                backgroundColor: Color.Main,
                                            }}
                                            size={"large"}
                                            onError={() => true}
                                            icon={<Image src={NoAvatar} alt=""/>}
                                        />
                                    }
                                    title={session.user?.name}
                                    description={t('text.viewProfile')}
                                />
                            },
                            {
                                type: "divider",
                            },
                            {
                                key: "user:info",
                                icon: <EditOutlined/>,
                                label: t("text.editInformation"),
                                onClick: () => navigate(`${RouteConfig.ME_SETTINGS}?tab=account`),
                            },
                            {
                                key: "user:password",
                                icon: <LockOutlined/>,
                                label: t("text.editPassword"),
                                onClick: () => navigate(`${RouteConfig.ME_SETTINGS}?tab=security`),
                            },
                            {
                                type: "divider",
                            },
                            {
                                key: "user:logout",
                                icon: <LogoutOutlined/>,
                                label: t("text.logout"),
                                onClick: onClickLogout,
                            }
                        ]}
                    />
                }
                placement="bottomRight"
                trigger={['click']}
                arrow
            >
                <Button
                    className={`${styles.TopBtn}`}
                    type={"text"}
                    shape={"circle"}
                >
                    <Image
                        height={18}
                        src={Images.iconUser}
                        alt={'IconUser'}
                    />
                </Button>
            </Dropdown>
            {
                isModalLogoutVisible
                    ? <LogoutScreen onClose={onCloseModalLogout}/>
                    : null
            }
        </>
    )
}
