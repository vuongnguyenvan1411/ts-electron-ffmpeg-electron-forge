import React, {useEffect, useMemo, useState} from "react";
import styles from "../../styles/module/Test.module.scss"
import {BackTop, Drawer, Layout} from 'antd';
import CustomHeader from "./CustomHeader";
import {BaseFooter} from "./BaseFooter";
import {Outlet, useLocation} from "react-router";
import {SiderLayout} from "./SiderLayout";
import TimelapseSiderLayout from "./TimelapseSiderLayout";
import {RouteConfig} from "../../config/RouteConfig";
import {MediaQuery} from "../../core/MediaQuery";
import GeodeticSiderLayout from "./GeodeticSiderLayout";
import {MeSiderLayout} from "./MeSiderLayout";
import {Vr360SiderLayout} from "./Vr360SiderLayout";
import {THeaderCtx, TOutletCtx} from "../../const/Types";
import NextImage from "next/image";
import {Images} from "../../const/Images";
import GeodeticSiderLayoutTest from "./GeodeticSiderLayoutTest";

type _TSider = "default" | "timelapse" | "vr360" | "geodetic" | "blog" | "me" | "geodetic-manager";

export const MasterLayoutTest = ({...props}) => {
    const location = useLocation();
    const [typeSider, setTypeSider] = useState<_TSider>("default");
    // const [showCollapsed, setShowCollapsed] = useState(window.matchMedia("(min-width: 1200px)").matches);
    const [isMobile, setIsMobile] = useState(window.matchMedia("(min-width: 1200px)").matches);
    const [isBig, setIsBig] = useState(new MediaQuery().isMinBreakpoint("xxxl"));
    // const [collapsed, setCollapsed] = useState(!showCollapsed);
    const [visible, setVisible] = useState(false);
    const [header, setHeader] = useState<THeaderCtx | undefined>(undefined);

    // const onCollapse = (collapsed: boolean) => {
    //     setCollapsed(collapsed);
    // };

    const onDrawerChange = (visible: boolean) => {
        setVisible(visible);
    }

    const onSetShowCollapsed = () => {
        // const _check = window.matchMedia("(min-width: 992px)").matches;
        // setShowCollapsed(_check);
        setIsMobile(window.matchMedia("(min-width: 1200px)").matches);
        setIsBig(new MediaQuery().isMinBreakpoint("xxxl"));
        // setCollapsed(!_check);
    }

    useEffect(() => {
        window.addEventListener("resize", onSetShowCollapsed);
        return () => {
            window.removeEventListener("resize", onSetShowCollapsed);
        }

    }, [])

    useEffect(() => {
        if (location.pathname !== RouteConfig.TIMELAPSE && location.pathname.indexOf(RouteConfig.TIMELAPSE) === 0) {
            setTypeSider("timelapse");
        } else if (location.pathname === RouteConfig.GEODETIC_MANAGER || location.pathname === RouteConfig.GEODETIC_MANAGER_UPLOAD || location.pathname === RouteConfig.GEODETIC_MANAGER_DELETED) {
            setTypeSider("geodetic-manager");
        } else if (location.pathname !== RouteConfig.GEODETIC && location.pathname.indexOf(RouteConfig.GEODETIC) === 0) {
            setTypeSider("geodetic");
        } else if (location.pathname !== RouteConfig.VR360 && location.pathname.indexOf(RouteConfig.VR360) === 0) {
            setTypeSider("vr360");
        } else if (location.pathname === RouteConfig.ME_SETTINGS || location.pathname.indexOf(RouteConfig.ME_SETTINGS) === 0) {
            setTypeSider("me");
        } else {
            setTypeSider("default");
        }
    }, [location])

    const _buildSider: JSX.Element = useMemo(() => {
        switch (typeSider) {
            case "timelapse":
                return <TimelapseSiderLayout {...props}/>;
            case "geodetic":
                return <GeodeticSiderLayout {...props}/>;
            case "vr360":
                return <Vr360SiderLayout {...props}/>;
            case "me":
                return <MeSiderLayout/>;
            case "geodetic-manager":
                return <GeodeticSiderLayoutTest/>
            default:
                return <SiderLayout/>;
        }

    }, [typeSider, props])

    /**
     * Control MasterLayout by using OutLet Context
     * @link https://reactrouter.com/docs/en/v6/hooks/use-outlet-context
     */
    const outletCtx: TOutletCtx = {
        isMobile: isMobile,
        onOpenDrawer: () => onDrawerChange(true),
        header: {
            header: header,
            setHeader: setHeader
        }
    }

    return (
        <>
            <Drawer
                className={"drawer-main"}
                width={280}
                placement={"left"}
                closable={false}
                onClose={() => onDrawerChange(false)}
                visible={visible}
                bodyStyle={{
                    background: "#01686F",
                }}
            >
                {
                    _buildSider
                }
            </Drawer>
            <Layout className={`${styles.Test} ${styles.NoBg}`}>
                <CustomHeader
                    header={header}
                    style={{
                        overflow: 'auto',
                        position: 'fixed',
                        left: 0,
                        right: 0,
                        zIndex: 1,
                    }}
                />
                <Layout hasSider>
                    <Layout.Sider
                        // collapsedWidth={!isMobile ? 0 : 80}
                        // collapsible={
                        //     showCollapsed
                        // }
                        // collapsed={collapsed}
                        // onCollapse={onCollapse}
                        width={!isMobile ? 0 : isBig ? 260 : 240}
                        className={`${styles.Sider} ${location.pathname.indexOf(RouteConfig.GEODETIC) === 0 ? styles.SiderGeodetic : ""}`}
                    >
                        {
                            _buildSider
                        }
                    </Layout.Sider>
                    <Layout
                        className={`site-layout ${styles.ContentLayout}`}
                        style={{
                            // marginLeft: `${collapsed ? !isMobile ? "0px" : "80px" : "300px"}`,
                            marginLeft: `${!isMobile ? "0px" : isBig ? "260px" : "240px"}`,
                            marginTop: 60,
                            background: "#F9FAFB",
                        }}
                    >
                        <Layout.Content
                            style={{
                                background: "#F9FAFB",
                            }}
                        >
                            <Outlet context={{outletCtx}}/>
                        </Layout.Content>
                        <Layout.Footer
                            className={"flex justify-center"}
                            style={{
                                background: "transparent",
                            }}
                        >
                            <BaseFooter/>
                        </Layout.Footer>
                    </Layout>
                </Layout>
            </Layout>
            <BackTop>
                <div className={styles.BackTop}>
                    <NextImage
                        className={"atl-icon atl-icon-white"}
                        width={24}
                        height={24}
                        src={Images.iconCaretUp.data}
                        alt={Images.iconCaretUp.atl}
                    />
                </div>
            </BackTop>
        </>
    )
}
