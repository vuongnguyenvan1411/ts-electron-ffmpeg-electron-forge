import {Layout} from 'antd';
import styles from "../../styles/module/Header.module.scss"
import {CustomButton} from "../components/CustomButton";
import React, {CSSProperties, useEffect, useMemo, useState} from "react";
import {TopLanguageFC} from "./TopLanguageFC";
import ArrowLeftIcon from "../components/svgs/ArrowLeftIcon";
import ArrowRightIcon from "../components/svgs/ArrowRightIcon";
import {useLocation, useNavigate} from "react-router";
import {useTranslation} from "react-i18next";
import {RouteConfig} from "../../config/RouteConfig";
import {AvatarLayout} from "./AvatarLayout";
import Image from "next/image";
import {useSessionContext} from "../contexts/SessionContext";
import {Images} from "../../const/Images";
import Head from "next/head";
import {SyncOutlined} from "@ant-design/icons";
import MenuLayout from "./MenuLayout";
import {THeaderCtx} from "../../const/Types";
import {SendingStatus} from "../../const/Events";

const CustomHeader = (props: {
    style?: CSSProperties | undefined,
    header?: THeaderCtx,
}) => {
    const location = useLocation();
    const [isHome, setIsHome] = useState(false);
    const [session] = useSessionContext()
    const navigate = useNavigate()
    const {t} = useTranslation()

    useEffect(() => {
        if (location.pathname === RouteConfig.HOME) {
            setIsHome(true);
        } else {
            setIsHome(false);
        }
    }, [location])

    const headerTitle = useMemo(() => {
        return (
            <Head>
                <title>{props.header?.title ?? "AutoTimelapse"}</title>
            </Head>
        )
    }, [props.header?.title])

    return (
        <>
            {
                headerTitle
            }
            <Layout.Header
                style={props.style}
                className={`${styles.Header} ${location.pathname.indexOf(RouteConfig.GEODETIC) === 0 ? styles.Header_Geodetic : ""}`}
            >
                <div className={styles.LeftHeader}>
                    <ul className={styles.NavBar}>
                        {
                            session.isAuthenticated && !isHome &&
                            <li className={styles.NavMenu}>
                                <MenuLayout/>
                            </li>
                        }
                        <li className={styles.NavLogo}>
                            <a
                                onClick={() => navigate(RouteConfig.HOME)}
                                className={styles.IconTimelapse}
                            >
                                <Image
                                    width={49.2}
                                    height={30}
                                    src={Images.iconTimelapse.data}
                                    alt={Images.iconTimelapse.atl}
                                />
                            </a>
                            <a
                                onClick={() => navigate(RouteConfig.HOME)}
                                className={styles.LogoTimelapse}
                            >
                                <Image
                                    src={Images.logoTimelapse.data}
                                    alt={Images.logoTimelapse.atl}
                                />
                            </a>
                        </li>
                    </ul>
                    <div className={styles.NavAction}>
                        <ul className={styles.NavBar}>
                            {
                                session.isAuthenticated && !isHome && <>
                                    <li className={styles.NavItem}>
                                        <a onClick={() => navigate(-1)}>
                                            <ArrowLeftIcon/>
                                        </a>
                                    </li>
                                    <li className={styles.NavItem}>
                                        <a onClick={() => navigate(1)}>
                                            <ArrowRightIcon/>
                                        </a>
                                    </li>
                                    <li className={styles.NavItem}>
                                        <a onClick={() => {
                                            props.header?.onReload?.call(this)
                                        }}>
                                            {
                                                props.header?.isLoading === SendingStatus.loading ?
                                                    <SyncOutlined className={"main-svg svg-fill-white svg-24"} spin/>
                                                    :
                                                    <Image
                                                        width={24}
                                                        height={24}
                                                        layout={"fixed"}
                                                        src={Images.iconRefresh.data}
                                                        alt={Images.iconRefresh.atl}
                                                    />
                                            }
                                        </a>
                                    </li>
                                    {/*<li className={styles.NavItem}>*/}
                                    {/*    <a onClick={() => navigate(RouteConfig.HOME)}>*/}
                                    {/*        <Image*/}
                                    {/*            width={27.73}*/}
                                    {/*            height={24}*/}
                                    {/*            src={Images.iconHomeOutlined.data}*/}
                                    {/*            alt={Images.iconHomeOutlined.atl}*/}
                                    {/*        />*/}
                                    {/*    </a>*/}
                                    {/*</li>*/}
                                </>
                            }
                        </ul>
                    </div>
                </div>
                <div className={styles.Actions}>
                    <ul className={styles.NavBar}>
                        <li className={styles.NavLanguage}>
                            <TopLanguageFC/>
                        </li>
                    </ul>
                    {
                        !session.isAuthenticated && (
                            <CustomButton
                                responsive={true}
                                size={"small"}
                                icon={
                                    <Image
                                        src={Images.iconUserCircle.data}
                                        alt={Images.iconUserCircle.atl}
                                    />
                                }
                            >
                                {t("button.login")}
                            </CustomButton>
                        )
                    }
                    <CustomButton
                        responsive={true}
                        className={"fix-text-color"}
                        onClick={() => navigate(RouteConfig.DOWNLOAD)}
                        style={{
                            background: "transparent",
                        }}
                        type={"outline"}
                        size={"small"}
                        icon={
                            <Image
                                className={"atl-icon atl-icon-white"}
                                src={Images.iconUploadSimple.data}
                                alt={Images.iconUploadSimple.atl}
                            />
                        }
                    >
                        Download
                    </CustomButton>
                    {
                        session.isAuthenticated && <ul className={styles.NavBar}>
                            <li className={styles.NavAccount}>
                                <AvatarLayout/>
                            </li>
                        </ul>
                    }
                </div>
            </Layout.Header>
        </>
    )
}

export default React.memo(CustomHeader);
