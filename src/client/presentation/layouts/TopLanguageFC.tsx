import React, {useCallback, useState} from "react";
import {findIndex} from "lodash";
import {getLng, setLng} from "../../locales/i18n";
import {MenuInfo} from "rc-menu/lib/interface";
import {Dropdown, Menu, notification} from "antd";
import {useTranslation} from "react-i18next";
import {App} from "../../const/App";
import {useConfigContext} from "../contexts/ConfigContext";
import Image from "next/image";
import ArrowDownIcon from "../components/svgs/ArrowDownIcon";
import {ItemType} from "antd/lib/menu/hooks/useItems";
import {CustomTypography} from "../components/CustomTypography";

export const TopLanguageFC = () => {
    const {t} = useTranslation()
    const [config, setConfig] = useConfigContext()
    const [indexLng, setIndexLng] = useState<number>(findIndex(App.Lang, (o) => o.code === getLng()))

    const onClickItemLng = useCallback((e: MenuInfo) => {
        const lang = App.Lang[parseInt(e.key)];
        setLng(lang.code);
        setIndexLng(parseInt(e.key));

        setConfig({...config, lang: parseInt(e.key)})

        notification.success({
            // className: "notification-main with-icon",
            message: <CustomTypography textStyle={"text-16-24"} isStrong>
                {t('message.languageSuccess')}
            </CustomTypography>,
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <Dropdown
            overlayClassName={"dropdown-menu-main dropdown-atl-language"}
            overlay={
                <Menu
                    onClick={onClickItemLng}
                    selectedKeys={[indexLng.toString()]}
                    items={[
                        ...App.Lang.map((value, index) => {
                            return {
                                key: index,
                                icon: <Image
                                    src={value.icon}
                                    alt={value.name}
                                    height={15}
                                    width={20}
                                />,
                                label: value.name,
                            } as ItemType;
                        })
                    ]}
                />
            }
            placement={"bottomRight"}
            trigger={['click']}
            arrow={true}
        >
            <a>
                <Image
                    src={App.Lang[indexLng].icon}
                    alt={App.Lang[indexLng].name}
                    height={20}
                    width={30}
                />
                <span>
                    {App.Lang[indexLng].name}
                </span>
                <ArrowDownIcon/>
            </a>
        </Dropdown>
    )
}
