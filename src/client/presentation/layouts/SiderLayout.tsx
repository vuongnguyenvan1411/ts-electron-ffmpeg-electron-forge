import {Divider, Menu} from "antd";
import React, {useEffect, useState} from "react";
import {ItemType} from "antd/es/menu/hooks/useItems";
import Image from "next/image";
import {Images} from "../../const/Images";
import {useLocation, useNavigate} from "react-router";
import {RouteConfig} from "../../config/RouteConfig";
import {useTranslation} from "react-i18next";

export const SiderLayout = () => {
    const {t} = useTranslation();
    const location = useLocation();
    const [selectedKeys, setSelectedKeys] = useState<string[]>([]);
    const navigate = useNavigate();

    useEffect(() => {
        switch (location.pathname) {
            case RouteConfig.TIMELAPSE:
                setSelectedKeys(["timelapse"]);
                break;
            case RouteConfig.GEODETIC:
                setSelectedKeys(["geodetic"]);
                break;
            case RouteConfig.VR360:
                setSelectedKeys(["vr360"]);
                break;
            case RouteConfig.BLOG:
                setSelectedKeys(["news"]);
                break;
            case RouteConfig.ABOUT:
                setSelectedKeys(["about"]);
                break;
            case RouteConfig.FEEDBACK:
                setSelectedKeys(["feedback"]);
                break;
            case RouteConfig.CAMERA:
                setSelectedKeys(["camera"]);
                break;
            case RouteConfig.WEIGHING:
                setSelectedKeys(["weighing"]);
                break;

        }
    }, [location])

    const _items: ItemType[] = [
        {
            key: 'home',
            label: t("title.home"),
            onClick: () => navigate(RouteConfig.HOME),
            icon: <Image
                layout={"fixed"}
                height={32}
                width={32}
                alt={Images.iconHome.atl}
                src={Images.iconHome.data}
            />
        },
        {
            key: 'timelapse',
            label: t("title.timelapse"),
            onClick: () => navigate(RouteConfig.TIMELAPSE),
            icon: <Image
                layout={"fixed"}
                height={19.49}
                width={32}
                alt={Images.iconTimelapse.atl}
                src={Images.iconTimelapse.data}
            />
        },
        {
            key: 'geodetic',
            label: t("title.geodetic"),
            onClick: () => navigate(RouteConfig.GEODETIC),
            icon: <Image
                layout={"fixed"}
                height={32}
                width={32}
                alt={Images.iconGeodetic.atl}
                src={Images.iconGeodetic.data}
            />
        },
        {
            key: 'vr360',
            label: t("title.vr360"),
            onClick: () => navigate(RouteConfig.VR360),
            icon: <Image
                layout={"fixed"}
                height={32}
                width={32}
                alt={Images.iconVr360.atl}
                src={Images.iconVr360.data}
            />
        },
        {
            key: 'camera',
            label: t("title.camera"),
            onClick: () => navigate(RouteConfig.CAMERA),
            icon: <Image
                layout={"fixed"}
                height={32}
                width={32}
                alt={Images.iconCamera.atl}
                src={Images.iconCamera.data}
            />
        },
        {
            key: 'weighing',
            label: t("title.weighing"),
            onClick: () => navigate(RouteConfig.WEIGHING),
            icon: <Image
                layout={"fixed"}
                height={32}
                width={32}
                alt={Images.iconWeighing.atl}
                src={Images.iconWeighing.data}
            />
        },
    ];

    const _itemsMenuSub: ItemType[] = [
        {
            key: 'news',
            label: t("title.news"),
            onClick: () => navigate(RouteConfig.BLOG),
            icon: <Image
                layout={"fixed"}
                height={32}
                width={32}
                alt={Images.iconNews.atl}
                src={Images.iconNews.data}
            />,
        },
        {
            key: 'about',
            label: t("title.about"),
            onClick: () => navigate(RouteConfig.ABOUT),
            icon: <Image
                layout={"fixed"}
                height={32}
                width={32}
                alt={Images.iconAbout.atl}
                src={Images.iconAbout.data}
            />
        },
        {
            key: 'feedback',
            label: t("title.feedback"),
            onClick: () => navigate(RouteConfig.FEEDBACK),
            icon: <Image
                layout={"fixed"}
                height={32}
                width={32}
                alt={Images.iconFeedback.atl}
                src={Images.iconFeedback.data}
            />
        },
    ]

    return <div className={"flex flex-col h-full justify-between pt-7 pb-10"}>
        <div className={"pb-10"}>
            <Menu
                className={"menu-main menu-border-selected-r-none menu-border-r-none"}
                style={{
                    background: "transparent",
                }}
                mode="inline"
                selectedKeys={selectedKeys}
                items={_items}
            />
        </div>
        <div className={"flex flex-col"}>
            <Divider className={"divider-main divider-horizontal-m-0 divider-border-half-dark50"}/>
            <div className={"pt-10"}>
                <Menu
                    className={"menu-main menu-border-selected-r-none menu-border-r-none"}
                    style={{
                        background: "transparent",
                    }}
                    mode="inline"
                    selectedKeys={selectedKeys}
                    items={_itemsMenuSub}
                />
            </div>
        </div>
    </div>
}
