import {Typography} from "antd";
import {App} from "../../const/App";
import {CSSProperties} from "react";

export const BaseFooter = (props: {
    style?: CSSProperties,
}) => {
    return (
        <Typography.Text style={props.style} className={"text-body-text-3"}>
            ©2022 Design by <span className={"text-weight-strong"}>I&I Group</span> - Version {App.Version}
        </Typography.Text>
    )
}
