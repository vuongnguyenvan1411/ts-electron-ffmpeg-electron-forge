import {Button, Layout, Menu, Space, Typography} from "antd";
import React, {useMemo, useState} from "react";
import {App} from "../../const/App";
import {Link} from "react-router-dom";
import {RouteConfig} from "../../config/RouteConfig";
import {ArrowLeftOutlined, HomeOutlined, MenuFoldOutlined, MenuUnfoldOutlined, SettingOutlined, UserOutlined} from "@ant-design/icons";
import {useTranslation} from "react-i18next";
import styles from '../../styles/module/MasterLayout.module.scss';
import {TopLanguageFC} from "./TopLanguageFC";
import {useNavigate} from "react-router";
import {AvatarLayout} from "./AvatarLayout";
import stylesFix from "../../styles/module/Header.module.scss"

interface ml_B {
    text: string,
    href?: string,
}

type TMenuLeft = {
    icon?: React.ReactNode;
    title: React.ReactNode;
    selected?: boolean;
    open?: boolean;
    link?: string;
    children?: TMenuLeft[]
}

export function MasterLayout(props: {
    children: React.ReactNode;
    breadcrumb?: ml_B[],
    contentClassName?: string,
    onBack?: Function,
    tool?: {
        title?: React.ReactNode,
        right?: React.ReactNode,
    }
    header?: null | React.ReactNode,
    sider?: boolean | React.ReactNode,
    siderType?: 'in' | 'out',
    footer?: React.ReactNode,
    isNotShowHomeIcon?: boolean,
}) {
    props = {
        siderType: 'in',
        ...props
    };

    const {t} = useTranslation()
    const navigate = useNavigate()

    const [collapsed, setCollapsed] = useState(false);

    const logoStyle = useMemo(() => collapsed ? styles.Minimized : styles.Full, [collapsed]);

    const menuLeftList = useMemo(() => {
        const list: TMenuLeft[] = [
            {
                icon: <HomeOutlined/>,
                title: t('title.home'),
                link: RouteConfig.HOME,
                selected: true,
            },
            {
                icon: <SettingOutlined/>,
                title: t('title.service'),
                open: true,
                children: [
                    {
                        title: t('title.timelapse'),
                        link: RouteConfig.TIMELAPSE
                    },
                    {
                        title: t('title.vr360'),
                        link: RouteConfig.VR360
                    },
                    {
                        title: t('title.geodetic'),
                        link: RouteConfig.GEODETIC
                    },
                    {
                        title: t('title.camera'),
                        link: RouteConfig.CAMERA
                    },
                    {
                        title: t('title.weighing'),
                        link: RouteConfig.WEIGHING
                    },
                ]
            },
            {
                icon: <UserOutlined/>,
                title: t('title.support'),
                children: [
                    {
                        title: t('title.news'),
                        link: RouteConfig.BLOG
                    },
                    {
                        title: t('title.about'),
                        link: RouteConfig.ABOUT
                    },
                    {
                        title: t('title.feedback'),
                        link: RouteConfig.FEEDBACK
                    },
                ]
            },
        ];

        const dfSelected: string[] = [];
        const dfOpen: string[] = [];

        list.forEach((value, index) => {
            if (value.selected === true) {
                dfSelected.push(index.toString());
            }

            if (value.open === true) {
                dfOpen.push(index.toString());
            }
        })

        return (
            <Menu
                className={styles.SbMenu}
                theme="light"
                defaultSelectedKeys={dfSelected}
                defaultOpenKeys={dfOpen}
                mode="inline"
            >
                {
                    list.map((v1, i1) => {
                        if (v1.children) {
                            return (
                                <Menu.SubMenu
                                    key={i1.toString()}
                                    icon={v1.icon}
                                    title={v1.title}
                                >
                                    {
                                        v1.children.map((v2, i2) => (
                                            <Menu.Item
                                                key={`${i1}-${i2}`}
                                                icon={v2.icon}
                                            >
                                                {
                                                    v2.link
                                                        ? <Link to={v2.link}>
                                                            {v2.title}
                                                        </Link>
                                                        : v2.title
                                                }
                                            </Menu.Item>
                                        ))
                                    }
                                </Menu.SubMenu>
                            )
                        } else {
                            return (
                                <Menu.Item
                                    key={i1.toString()}
                                    icon={v1.icon}
                                >
                                    {
                                        v1.link
                                            ? <Link to={v1.link}>
                                                {v1.title}
                                            </Link>
                                            : v1.title
                                    }
                                </Menu.Item>
                            )
                        }
                    })
                }
            </Menu>
        );

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Layout
            className={styles.MasterLayout}
        >
            {
                props.sider === false
                    ? null
                    : <Layout.Sider
                        className={styles.Sider}
                        breakpoint="sm"
                        collapsedWidth="0"
                        collapsible
                        trigger={null}
                        collapsed={collapsed}
                        onCollapse={(collapsed) => setCollapsed(collapsed)}
                    >
                        <div className={`${styles.Logo} ${logoStyle}`}/>
                        {
                            typeof props.sider === "undefined"
                                ? menuLeftList
                                : props.sider
                        }
                    </Layout.Sider>
            }
            <Layout className={`flex flex-col w-full h-full mx-auto ${styles.Layout}`}>
                {
                    props.header === null ?
                        null : props.header === undefined ?
                            <Layout.Header
                                id={"MasterLayerHeader"}
                                className={`mb-auto px-2 ${stylesFix.Header} ${stylesFix.Header_Geodetic}`}
                            >
                                <Space>
                                    {
                                        props.onBack !== undefined
                                            ? <Button
                                                shape="circle"
                                                icon={<ArrowLeftOutlined/>}
                                                onClick={() => {
                                                    if (props.onBack !== undefined) props.onBack();
                                                }}
                                            />
                                            : null
                                    }
                                    <Button
                                        shape="circle"
                                        icon={<HomeOutlined/>}
                                        onClick={() => navigate(RouteConfig.HOME)}
                                    />
                                </Space>
                                <div className={stylesFix.Actions}>
                                    <ul className={stylesFix.NavBar}>
                                        <li className={stylesFix.NavLanguage}>
                                            <TopLanguageFC/>
                                        </li>
                                    </ul>
                                    <ul className={stylesFix.NavBar}>
                                        <li className={stylesFix.NavAccount}>
                                            <AvatarLayout/>
                                        </li>
                                    </ul>
                                </div>
                            </Layout.Header>
                            : props.header
                }
                <Layout.Content className={styles.Content}>
                    {
                        props.tool !== undefined
                            ? <div className={"w-full flex items-center justify-between bg-white mb-5 pr-4"}>
                                <div className={"flex justify-between items-center"}>
                                    {
                                        React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                                            style: {
                                                padding: '11px 24px 12px',
                                                fontSize: '18px',
                                                lineHeight: '0',
                                                cursor: 'pointer',
                                                transition: 'color 0.3s',
                                            },
                                            onClick: () => setCollapsed(!collapsed),
                                        })
                                    }
                                    {
                                        typeof props.tool.title === "string"
                                            ? <Typography.Text strong>{props.tool.title}</Typography.Text>
                                            : props.tool.title
                                    }
                                </div>
                                {props.tool.right}
                            </div>
                            : null
                    }
                    <div className={`site-layout-content ${props.contentClassName ?? "my-0 mx-4"}`}>
                        {props.children}
                    </div>
                    {/*{
                        typeof props.breadcrumb !== 'undefined' && props.breadcrumb.length > 0
                            ? <Breadcrumb style={{margin: '10px 0'}}>
                                <Breadcrumb.Item>
                                    <Link to={RouteConfig.HOME} replace>
                                        <HomeOutlined/>
                                        <span className="ml-2">{t('title.home')}</span>
                                    </Link>
                                </Breadcrumb.Item>
                                {
                                    props.breadcrumb.map((value, index) => (
                                        <Breadcrumb.Item key={index}>
                                            {
                                                value.href
                                                    ? <Link to={value.href}>
                                                        <span>{value.text}</span>
                                                    </Link>
                                                    : <span>{value.text}</span>
                                            }
                                        </Breadcrumb.Item>
                                    ))
                                }
                            </Breadcrumb>
                            : null
                    }*/}
                </Layout.Content>
                {
                    props.footer === null
                        ? null
                        : (
                            <Layout.Footer className={"mt-auto text-center"}>
                                ©2021 Design by <a href={App.Domain} target={"_blank"} rel={"noopener noreferrer"}>{App.Company}</a>
                                &nbsp;-&nbsp;v{App.Version}
                            </Layout.Footer>
                        )
                }
            </Layout>
        </Layout>
    );
}
