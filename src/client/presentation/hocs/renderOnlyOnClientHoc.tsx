import React from "react";
import useIsClient from "../hooks/useIsClient";


const renderOnlyOnClientHoc = <P extends Object>(
    Component: React.ComponentType<P>
): React.FC<P> => ({
                       ...props
                   }) => {
    const isClient = useIsClient();

    return isClient ? <Component {...props}/> : <></>
}


export default renderOnlyOnClientHoc;