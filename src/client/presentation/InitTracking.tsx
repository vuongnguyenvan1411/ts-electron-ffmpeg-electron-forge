import {ITAction} from "../recoil/it/ITAction";
import React, {useEffect, useMemo, useRef, useState} from "react";
import {SendingStatus} from "../const/Events";
import {InitErrorWidget, InitLoadingWidget} from "./widgets/it/InitWidget";
import {ERequired, InitModel} from "../models/ITModel";
import {Modal} from "antd";
import {MaintenanceWidget} from "./widgets/it/MaintenanceWidget";
import {useRouter} from "next/router";
import {NotificationOutlined, RedoOutlined} from "@ant-design/icons";
import {useTranslation} from "react-i18next";
import {useConfigContext} from "./contexts/ConfigContext";
import {UpdateWidget} from "./widgets/it/UpdateWidget";
import {Utils} from "../core/Utils";
import {App} from "../const/App";
import {useSessionContext} from "./contexts/SessionContext";
import {Color} from "../const/Color";
import {MeAction} from "../recoil/account/me/MeAction";

export const InitTracking = (props: any) => {
    const [config] = useConfigContext()
    const [session, setSession] = useSessionContext()
    const router = useRouter()
    const {t} = useTranslation()
    const {
        onInit,
        onTracking,
        onClearState,
        vm
    } = ITAction()
    const {
        onClearUser
    } = MeAction()

    const [isModalUpdateVisible, setIsModalUpdateVisible] = useState<boolean>(false)
    const [dataChange, setDataChange] = useState<InitModel>()
    const trackingInterval = useRef<NodeJS.Timeout>()

    useEffect(() => {
        console.log('%cMount Screen: InitTracking', Color.ConsoleInfo)

        if (config.agent?.isWebAppInApp()) {
            console.log(`%cDisable Init & Tracking`, Color.ConsoleWarning)
        } else {
            if (vm.isLoading == SendingStatus.idle) {
                onInit()
            }
        }

        return () => {
            if (vm.init && vm.tracking) {
                console.log('%cUnmount Screen: InitTracking', Color.ConsoleInfo)

                onClearState()
            }
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        if (!config.agent?.isWebAppInApp()) {
            if (vm.isLoading == SendingStatus.complete) {
                if (!trackingInterval.current) {
                    trackingInterval.current = setInterval(() => {
                        onTracking()
                    }, App.TimeoutTracking)
                }

                if (vm.init) {
                    setDataChange(vm.init)
                }
            }
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.isLoading])

    useEffect(() => {
        if (vm.tracking) {
            setDataChange(vm.tracking)
        }
    }, [vm.tracking])

    const getView = useMemo(() => {
        if (config.agent?.isWebAppInApp()) {
            return props.children
        }

        if (dataChange) {
            if (session.isAuthenticated) {
                if (!dataChange.user) {
                    // remove store user
                    onClearUser()

                    setSession({
                        ...session,
                        isAuthenticated: false
                    })

                    return props.children
                }
            } else {
                if (dataChange.user) {
                    setSession({
                        ...session,
                        isAuthenticated: true
                    })

                    return props.children
                }
            }

            if (dataChange.maintenance?.status) {
                return <MaintenanceWidget message={dataChange.maintenance?.message}/>
            } else {
                if (dataChange.update?.version) {
                    if (Utils.versionCompare(App.Version, dataChange.update.version) == -1) {
                        if (dataChange.update.required == ERequired.Obligatory) {
                            if (trackingInterval.current) {
                                clearInterval(trackingInterval.current)
                            }

                            return (
                                <UpdateWidget
                                    title={dataChange.update.title}
                                    message={dataChange.update.message}
                                />
                            )
                        } else if (dataChange.update.required == ERequired.Notify) {
                            if (!isModalUpdateVisible) {
                                setIsModalUpdateVisible(true)
                            }
                        }
                    }
                }

                return props.children
            }
        }

        return props.children

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [dataChange])

    if (config.agent?.isWebAppInApp()) {
        return props.children
    } else {
        return (
            <>
                <Modal
                    title={<><NotificationOutlined className={"mr-2"}/>{vm.init?.update?.title}</>}
                    visible={isModalUpdateVisible}
                    keyboard={false}
                    onCancel={() => setIsModalUpdateVisible(false)}
                    onOk={() => router.reload()}
                    cancelText={t("button.close")}
                    okText={<><RedoOutlined className={"mr-2"}/>Tải lại</>}
                >
                    <p>{vm.init?.update?.message}</p>
                </Modal>
                {
                    vm.isLoading == SendingStatus.loading
                        ? <InitLoadingWidget/>
                        : vm.isLoading == SendingStatus.complete
                            ? getView
                            : <InitErrorWidget/>
                }
            </>
        )
    }
}
