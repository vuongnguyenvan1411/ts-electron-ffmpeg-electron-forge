type TOption = {
    text: string;
    href: string;
    icon: string;
}

export class Breadcrumb {
    data: TOption[] = [];

    public add(text: string, href: string = '', icon: string = ''): void {
        this.data.push({
            text: text,
            href: href,
            icon: icon,
        });
    }

    public get(): TOption[] {
        return this.data;
    }

    public clear(): void {
        this.data = [];
    }
}
