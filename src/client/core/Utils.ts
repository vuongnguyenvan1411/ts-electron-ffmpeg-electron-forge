import {App} from "../const/App";
import {forIn, includes, sum} from "lodash";
import moment from "moment";
import {EDFile} from "./encrypt/EDFile";

export class Utils {
    public static isDev = (): boolean => {
        return !process.env.NODE_ENV || process.env.NODE_ENV === 'development';
    }

    /** TODO: error get env */
    public static env = (key: string, df?: string | number) => {
        key = `NEXT_PUBLIC_${key}`;

        if (process.env[key]) {
            return process.env[key];
        } else {
            return df;
        }
    }

    public static copyClipboard = async (str: string) => {
        await navigator.clipboard.writeText(str)
    }

    public static boolToInt = (bool: boolean): number => {
        return bool ? 1 : 0;
    }

    public static intToBool = (value: string | number): boolean => {
        return value.toString() === '1';
    }

    public static debounce = <F extends ((...args: any) => any)>(func: F, waitFor: number) => {
        let timeout: number = 0

        const debounced = (...args: any) => {
            clearTimeout(timeout)
            setTimeout(() => func(...args), waitFor)
        }

        return debounced as (...args: Parameters<F>) => ReturnType<F>
    }

    public static clearEmptyUrl = (data: any) => {
        if (typeof data !== "object") {
            return data;
        }

        const newData: any = {};

        forIn(data, (value: any, key: string) => {
            if (value !== undefined && value.toString().length > 0) {
                newData[key] = value;
            }
        });

        return newData;
    }

    public static strSlug = (str: string): string => {
        let filter = str.toLowerCase();

        filter = filter.replace(/[àáạảãâầấậẩẫăằắặẳẵ]/g, "a");
        filter = filter.replace(/[èéẹẻẽêềếệểễ]/g, "e");
        filter = filter.replace(/[ìíịỉĩ]/g, "i");
        filter = filter.replace(/[òóọỏõôồốộổỗơờớợởỡ]/g, "o");
        filter = filter.replace(/[ùúụủũưừứựửữ]/g, "u");
        filter = filter.replace(/[ỳýỵỷỹ]/g, "y");
        filter = filter.replace(/đ/g, "d");
        filter = filter.replace(/ & /g, "-")
        filter = filter.replace(/-&-/g, "-");
        // filter = filter.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g, "-");
        filter = filter.replace(/!|@|%|\^|\*|\(|\)|\+|=|<|>|\?|\/|,|\.|:|;|'| |"|&|#|\[|]|~|$|_/g, "-");

        filter = filter.replace(/-+-/g, "-");
        // filter = filter.replace(/^\-+|\-+$/g, "");
        filter = filter.replace(/^-+|-+$/g, "");

        return filter;
    }

    public static dataURLtoBlob(dataUrl: any) {
        const arr = dataUrl.split(",");
        const mime = arr[0].match(/:(.*?);/)[1];
        const bStr = atob(arr[1]);
        let n = bStr.length;
        const u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = bStr.charCodeAt(n);
        }

        return new Blob([u8arr], {type: mime});
    }

    public static checkError = (e: any) => {
        console.warn(e.constructor, e.name, e.message, e.stack);
    }

    public static cdnScAsset = (path: string): string => {
        return App.UrlCdnSc + `/${path}`;
    }

    public static checkHourState(timestamp: number, expired: number = App.HoursStoreState) {
        return moment().diff(moment.unix(timestamp), 'hours') > expired;
    }

    public static htmlDecode(msg: string) {
        const e = document.createElement('div');
        e.innerHTML = msg;
        return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue ?? '';
    }

    // public static cdnGdtAsset(path?: string) {
    //     return App.UrlCdnGdt + (path ? `/${path}` : '');
    // }

    public static cdnGdtAssetRaw = (path: string): string => {
        return App.UrlCdnGdt + `/${path}`;
    }

    public static cdnGdtAsset(options: {
        version?: string,
        path: "tile" | "pc" | "tm" | "i3s" | "gs" | "ms",
        data: Record<string, any>,
        timeout?: number
    }) {
        const version = options.version ?? 'v2'

        return `${App.UrlCdnGdt}/${version}/d/` + EDFile.setLinkUrl({
            p: options.path,
            d: options.data,
            ...(
                options.timeout && {
                    e: options.timeout
                }
            )
        })
    }

    public static cdnGdtAssetU(options: {
        version?: string,
        path: string,
        timeout?: number
    }) {
        const version = options.version ?? 'v2'

        return `${App.UrlCdnGdt}/${version}/u/` + EDFile.setLinkUrl({
            p: options.path,
            ...(
                options.timeout && {
                    e: options.timeout
                }
            )
        })
    }

    // public static cdnGdtAssetT2d(options: { i: number, p: Record<string, number> }) {
    //     return `${App.UrlCdnGdt}/t2d/` + EDFile.encrypt(options);
    // }

    // public static cdnGdtAssetT3d(path: string, options: any) {
    //     return `${App.UrlCdnGdt}/t3d/${path}/` + EDFile.encrypt(options);
    // }

    // public static cdnGdtAssetGsw(options: { ws: string, q: Record<string, string> }) {
    //     return `${App.UrlCdnGdt}/gsw/` + EDFile.encrypt(options);
    // }

    // public static cdnGdtAssetMsw(options: { map: string, q: Record<string, string> }) {
    //     return `${App.UrlCdnGdt}/msw/` + EDFile.encrypt(options);
    // }

    static assetCdnGs(
        path: 'si' | 'sv' | 'sp',
        options?: Record<string, any>
    ) {
        let url = `${App.UrlCdnGs}/${path}/`;

        if (options) {
            url += EDFile.setLinkUrl(options)
        }

        return url
    }

    public static getLocalAsset(path?: string) {
        return `${window.location.origin}/${path ?? ''}`;
    }

    public static isObject(obj: any, keysLength?: number) {
        const is = obj.constructor === Object;

        if (keysLength) {
            return is && Object.keys(obj).length > keysLength;
        } else {
            return obj.constructor === Object;
        }
    }

    public static isArray(arr: any, length?: number) {
        const is = arr instanceof Array;

        if (length) {
            return is && arr.length > length;
        } else {
            return arr instanceof Array;
        }
    }

    public static arrayAvg(arr: number[]) {
        if (arr.length === 0) {
            return 0;
        }

        return sum(arr) / arr.length;
    }

    public static formatByte(num: number, precision: number = 1) {
        let i = 0;
        const suffix = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        while ((num / 1024) > 1) {
            num = num / 1024;
            i++;
        }

        return num.toFixed(precision) + suffix[i];
    }

    public static formatTime(temp: number, lang?: Record<string, string>) {
        const numberEnding = number => {
            if (lang?.ending !== undefined) {
                return lang.ending
            }

            return (number > 1) ? 's' : ''
        }

        const years = Math.floor(temp / 31536000)
        if (years) {
            return years + ' ' + (lang?.year ?? 'year') + numberEnding(years)
        }

        const days = Math.floor((temp %= 31536000) / 86400)
        if (days) {
            return days + ' ' + (lang?.day ?? 'day') + numberEnding(days)
        }

        const hours = Math.floor((temp %= 86400) / 3600)
        if (hours) {
            return hours + ' ' + (lang?.hour ?? 'hour') + numberEnding(hours)
        }

        const minutes = Math.floor((temp %= 3600) / 60)
        if (minutes) {
            return minutes + ' ' + (lang?.minute ?? 'minute') + numberEnding(minutes)
        }

        const seconds = temp % 60
        return seconds + ' ' + (lang?.second ?? 'second') + numberEnding(seconds)
    }

    public static versionCompare(v1: string, v2: string, options?: { lexicographical?: boolean, zeroExtend?: boolean }): number {
        let lexicographical = options && options.lexicographical,
            zeroExtend = options && options.zeroExtend,
            v1parts: (number | string)[] = v1.split('.'),
            v2parts: (number | string)[] = v2.split('.');

        const isValidPart = (x: string) => {
            return (lexicographical ? /^\d+[A-Za-z]*$/ : /^\d+$/).test(x);
        }

        if (!v1parts.every(isValidPart) || !v2parts.every(isValidPart)) {
            return NaN;
        }

        if (zeroExtend) {
            while (v1parts.length < v2parts.length) v1parts.push("0");
            while (v2parts.length < v1parts.length) v2parts.push("0");
        }

        if (!lexicographical) {
            v1parts = v1parts.map(Number);
            v2parts = v2parts.map(Number);
        }

        for (let i = 0; i < v1parts.length; ++i) {
            if (v2parts.length == i) {
                return 1;
            }

            if (v1parts[i] == v2parts[i]) {
                // continue;
            } else if (v1parts[i] > v2parts[i]) {
                return 1;
            } else {
                return -1;
            }
        }

        if (v1parts.length != v2parts.length) {
            return -1;
        }

        return 0;
    }

    static rmObjectByValue<T>(obj: Record<string, any>, compare: any = [null, [], '', undefined, {}]): T {
        let data: any = {};

        for (const key in obj) {
            const value = obj[key]

            if (!(value instanceof Array) && value instanceof Object && Object.keys(value).length > 0) {
                data[key] = Utils.rmObjectByValue(value, compare)
            } else {
                if (compare instanceof Array) {
                    if (includes(compare, value)) {
                        continue
                    }
                } else {
                    if (value === compare) {
                        continue
                    }
                }

                data[key] = value
            }
        }

        return data
    }

    static base64Encode(str: string | object) {
        if (typeof str == "object") {
            str = JSON.stringify(str)
        }

        return Buffer.from(str).toString('base64')
    }

    static base64Decode(str: string) {
        return Buffer.from(str, 'base64').toString('ascii')
    }
}
