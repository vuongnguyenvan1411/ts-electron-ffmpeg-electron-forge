import {CryptoJSAesJson, TOptions} from "./CryptoJSAesJson";
import {Utils} from "../Utils";

export class EDData {
    protected static opts: TOptions = {
        secret: process.env.NEXT_PUBLIC_ED_DATA_SECRET,
    }

    public static encrypt(value: any) {
        const driver = new CryptoJSAesJson(this.opts)

        return driver.encrypt(value)
    }

    public static decrypt(value: string | object): any {
        if (typeof value == "object") {
            value = JSON.stringify(value)
        }

        const driver = new CryptoJSAesJson(this.opts)

        return driver.decrypt(value)
    }

    static getData(str: string) {
        try {
            const decode = Utils.base64Decode(str)

            if (decode) {
                return this.decrypt(decode);
            }
        } catch (e) {
            Utils.checkError(e);
        }

        return undefined
    }

    static setData(value: any) {
        try {
            const enc = this.encrypt(value)

            if (enc) {
                return Utils.base64Encode(enc)
            }
        } catch (e) {
            Utils.checkError(e);
        }

        return ''
    }
}
