import {App} from "../const/App";
import Hashids from 'hashids';

type TConnect = {
    salt: string;
    length: number;
}

type NumberLike = number

export class CHashids {
    protected static C: TConnect;

    static _init() {
        if (typeof this.C === "undefined") {
            this.C = (App.Hashids.connections as any)[App.Hashids.default];
        }
    }

    static connection(name: string) {
        const hc: any = App.Hashids.connections;

        if (hc.hasOwnProperty(name)) {
            this.C = hc[name];
        } else {
            this.C = hc.main;
        }

        return this;
    }

    static encode(number: any): string {
        const hashids = new Hashids(this.C.salt, this.C.length);

        return hashids.encode(number);
    }

    static decode(id: string): NumberLike[] {
        const hashids = new Hashids(this.C.salt, this.C.length);

        return hashids.decode(id) as NumberLike[];
    }

    static decodeGetFirst(id: string): number | null {
        const hashids = new Hashids(this.C.salt, this.C.length);
        const result = hashids.decode(id);

        return result.length > 0 ? result[0] as number : null;
    }

    static encodeHex(value: string | bigint): string {
        const hashids = new Hashids(this.C.salt, this.C.length);

        return hashids.encodeHex(value);
    }

    static decodeHex(id: string): string {
        const hashids = new Hashids(this.C.salt, this.C.length);

        return hashids.decodeHex(id);
    }
}

CHashids._init();
