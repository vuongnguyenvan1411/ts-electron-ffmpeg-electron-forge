import {BigUpFile, OFile} from "./BigUpFile";
import EventEmitter from "eventemitter3";
import {BigUpUtils} from "./BigUpUtils";
import {BigUpChunk} from "./BigUpChunk";
import {BigUpEventMap, EventName} from "./BigUpEvents";

export type T_BigUpOptions = {
    chunkSize?: number | Function
    forceChunkSize?: boolean
    simultaneousUploads?: number
    singleFile?: boolean
    fileParameterName?: string
    progressCallbacksInterval?: number
    speedSmoothingFactor?: number
    query?: object | Function
    headers?: object | Function
    withCredentials?: boolean
    preprocess?: (chunk: BigUpChunk) => void;
    method?: string
    testMethod?: string | Function
    uploadMethod?: string | Function
    prioritizeFirstAndLastChunk?: boolean
    allowDuplicateUploads?: boolean
    targetUpload?: string | Function
    targetQueue?: string
    maxChunkRetries?: number
    chunkRetryInterval?: number
    permanentErrors?: number[]
    successStatuses?: number[]
    initFileFn?: (file: BigUpFile, chunk?: BigUpChunk) => any
    readFileFn?: (file: BigUpFile, startByte: number, endByte: number, fileType: string, chunk: BigUpChunk) => void
    generateUniqueIdentifier?: (file: BigUpFile | File) => any
    testChunks?: boolean
    checksum?: boolean
    onDropStopPropagation?: boolean
    changeRawDataBeforeSend?: (chunk: BigUpChunk, data: FormData | Blob | null) => FormData | Blob | null
    changeRawHeaderBeforeSend?: (chunk: BigUpChunk, header: object) => object
    changeRawTargetBeforeSend?: (data: { target: string, path: string, query?: object }) => string
    changeBodyAfterSend?: (data: { target: string, path: string, query?: object }) => string
}

export class BigUp extends EventEmitter {
    opts: T_BigUpOptions

    /**
     * Supported by browser?
     */
    support: boolean = (
        typeof File !== 'undefined'
        && typeof Blob !== 'undefined'
        && typeof FileList !== 'undefined'
        && (
            ('slice' in Blob.prototype)
            || ('webkitSlice' in Blob.prototype)
            || ('mozSlice' in Blob.prototype)
            || false
        )
    )

    /**
     * Check if directory upload is supported
     */
    supportDirectory: boolean = (
        /Chrome/.test(window.navigator.userAgent)
        || /Firefox/.test(window.navigator.userAgent)
        || /Edge/.test(window.navigator.userAgent)
    )

    files: BigUpFile[] = []

    constructor(opts: T_BigUpOptions) {
        super()

        const defaults: T_BigUpOptions = {
            chunkSize: 1024 * 1024,
            forceChunkSize: false,
            simultaneousUploads: 3,
            singleFile: false,
            fileParameterName: 'file',
            progressCallbacksInterval: 500,
            speedSmoothingFactor: 0.1,
            query: {},
            headers: {},
            withCredentials: false,
            method: 'multipart',
            testMethod: 'GET',
            uploadMethod: 'POST',
            prioritizeFirstAndLastChunk: false,
            allowDuplicateUploads: false,
            targetUpload: '/',
            testChunks: true,
            checksum: false,
            maxChunkRetries: 0,
            permanentErrors: [404, 413, 415, 500, 501],
            successStatuses: [200, 201, 202],
            onDropStopPropagation: false,
            readFileFn: this.webAPIFileRead
        }

        this.opts = {
            ...defaults,
            ...opts
        }
    }

    // @ts-ignore
    on<T extends EventName>(event: T, callback: (...args: BigUpEventMap[T]) => void, context?: any): this {
        return super.on(event, callback, context)
    }

    // onRef<T extends EventName>(event: T, fn: (callback: (...args: BigUpEventMap[T]) => void) => this, context?: any): this {
    //     // fn((event, callback, context) => {
    //     //     this.on(event, callback, context)
    //     // })
    //
    //     return fn(() => {
    //         return this.on(event, callback, context)
    //     })
    // }

    // @ts-ignore
    emit<T extends EventName>(event: T, ...args): boolean {
        super.emit(event, ...args)

        return true
    }

    /**
     * Default read function using the webAPI
     */
    webAPIFileRead = (fileObj: BigUpFile, startByte: number, endByte: number, fileType: string, chunk: BigUpChunk) => {
        let function_name = 'slice'

        if ('slice' in fileObj.file) {
            function_name = 'slice'
        } else if ('mozSlice' in fileObj.file) {
            function_name = 'mozSlice'
        } else if ('webkitSlice' in fileObj.file) {
            function_name = 'webkitSlice'
        }

        chunk.readFinished(fileObj.file[function_name](startByte, endByte, fileType))
    }

    /**
     * On drop event
     */
    onDrop = (event: DragEvent) => {
        if (this.opts.onDropStopPropagation) {
            event.stopPropagation()
        }

        event.preventDefault()

        const dataTransfer = event.dataTransfer

        if (dataTransfer) {
            if (
                dataTransfer.items
                && dataTransfer.items[0]
                && dataTransfer.items[0].webkitGetAsEntry()
            ) {
                this.webkitReadDataTransfer(event)
            } else {
                this.addFiles(dataTransfer.files, event)
            }
        }
    }

    preventEvent = (event: MouseEvent) => {
        event.preventDefault()
    }

    webkitReadDataTransfer = (event: DragEvent) => {
        const $ = this

        if (!event.dataTransfer) {
            return
        }

        let queue = event.dataTransfer.items.length;
        const files: any[] = [];

        BigUpUtils.each(event.dataTransfer.items, function (item) {
            const entry = item.webkitGetAsEntry()

            if (!entry) {
                decrement()

                return
            }

            if (entry.isFile) {
                // due to a bug in Chrome's File System API impl - #149735
                fileReadSuccess(item.getAsFile(), entry.fullPath);
            } else {
                readDirectory(entry.createReader());
            }
        })

        function readDirectory(reader) {
            reader.readEntries(function (entries) {
                if (entries.length) {
                    queue += entries.length;
                    BigUpUtils.each(entries, function (entry) {
                        if (entry.isFile) {
                            const fullPath = entry.fullPath

                            entry.file(function (file) {
                                fileReadSuccess(file, fullPath)
                            }, readError)
                        } else if (entry.isDirectory) {
                            readDirectory(entry.createReader())
                        }
                    })
                    readDirectory(reader)
                } else {
                    decrement()
                }
            }, readError)
        }

        function fileReadSuccess(file, fullPath) {
            // relative path should not start with "/"
            file.relativePath = fullPath.substring(1)
            files.push(file)
            decrement()
        }

        function readError(fileError) {
            decrement()
            throw fileError
        }

        function decrement() {
            if (--queue == 0) {
                $.addFiles(files, event)
            }
        }
    }

    /**
     * Generate unique identifier for a file
     */
    generateUniqueIdentifier = (file: BigUpFile | File): string => {
        const custom = this.opts.generateUniqueIdentifier

        if (typeof custom === 'function') {
            return custom(file);
        }

        // Some confusion in different versions of Firefox
        const relativePath =
            file instanceof BigUpFile
                ? file.relativePath ?? file.name
                : ('fileName' in file) ? (file as any).fileName : file.webkitRelativePath

        const name = [file.size]

        if (relativePath.length > 0) {
            name.push(relativePath.replace(/[^0-9a-zA-Z_-]/img, ''))
        }

        return name.join('-')
    }

    /**
     * Upload next chunk from the queue
     */
    uploadNextChunk = (preventEvents?): boolean => {
        let found = false

        if (this.opts.prioritizeFirstAndLastChunk) {
            BigUpUtils.each(this.files, file => {
                if (
                    !file.paused && file.chunks.length
                    && file.chunks[0].status() === 'pending'
                ) {
                    file.chunks[0].send()
                    found = true
                    return false
                }

                if (
                    !file.paused && file.chunks.length > 1
                    && file.chunks[file.chunks.length - 1].status() === 'pending'
                ) {
                    file.chunks[file.chunks.length - 1].send()
                    found = true

                    return false
                }
            })

            if (found) {
                return found
            }
        }

        // Now, simply look for the next, the best thing to upload
        BigUpUtils.each(this.files, file => {
            if (!file.paused) {
                BigUpUtils.each(file.chunks, chunk => {
                    if (chunk.status() === 'pending') {
                        chunk.send()
                        found = true

                        return false
                    }
                })
            }

            if (found) {
                return false
            }
        })

        if (found) {
            return true
        }

        // They are no more outstanding chunks to upload, check is everything is done
        let outstanding = false

        BigUpUtils.each(this.files, file => {
            if (!file.isComplete()) {
                outstanding = true

                return false
            }
        })

        if (!outstanding && !preventEvents) {
            // All chunks have been uploaded, complete
            BigUpUtils.async(() => {
                this.emit("complete")
            }, this)
        }

        return false
    }

    /**
     * Assign a browse action to one or more DOM nodes.
     *
     * @link http://www.w3.org/TR/html-markup/input.file.html#input.file-attributes
     * eg: accept: 'image/*'
     * be selected (Chrome only).
     */
    assignBrowse = (domNodes: Element | Element[], isDirectory?: boolean, singleFile?: boolean, attributes?: object) => {
        if (domNodes instanceof Element) {
            domNodes = [domNodes]
        }

        BigUpUtils.each(domNodes, domNode => {
            let input: HTMLInputElement

            if (domNode.tagName === 'INPUT' && domNode.type === 'file') {
                input = domNode;
            } else {
                input = document.createElement('input');
                input.setAttribute('type', 'file');
                // display:none - not working in Opera 12
                BigUpUtils.extend(input.style, {
                    visibility: 'hidden',
                    position: 'absolute',
                    width: '1px',
                    height: '1px'
                })

                // for Opera 12 browser, input must be assigned to a document
                domNode.appendChild(input)
                // https://developer.mozilla.org/en/using_files_from_web_applications
                // event listener is executed two times
                // first one - original mouse click event
                // second - input.click(), input is inside domNode
                domNode.addEventListener('click', () => {
                    input.click()
                }, false)
            }

            if (!this.opts.singleFile && !singleFile) {
                input.setAttribute('multiple', 'multiple');
            }

            if (isDirectory) {
                input.setAttribute('webkitdirectory', 'webkitdirectory');
            }

            if (attributes) {
                BigUpUtils.each(attributes, (value, key) => {
                    input.setAttribute(key, value);
                })
            }

            // When new files are added, simply append them to the overall list
            input.addEventListener('change', (e) => {
                const target = e.target as HTMLInputElement

                if (target
                    && target.value
                    && target.files
                ) {
                    this.addFiles(target.files, e);
                    (e.target as HTMLInputElement).value = ''
                }
            }, false)
        }, this)
    }

    /**
     * Assign one or more DOM nodes as a drop target.
     */
    assignDrop = (domNodes: Element | Element[]) => {
        if (domNodes instanceof Element) {
            domNodes = [domNodes]
        }

        BigUpUtils.each(domNodes, domNode => {
            domNode.addEventListener('dragover', this.preventEvent, false)
            domNode.addEventListener('dragenter', this.preventEvent, false)
            domNode.addEventListener('drop', this.onDrop, false)
        }, this)
    }

    /**
     * Un-assign drop event from DOM nodes
     */
    unAssignDrop = (domNodes: Element | Element[]) => {
        if (domNodes instanceof Element) {
            domNodes = [domNodes]
        }

        BigUpUtils.each(domNodes, domNode => {
            domNode.removeEventListener('dragover', this.preventEvent)
            domNode.removeEventListener('dragenter', this.preventEvent)
            domNode.removeEventListener('drop', this.onDrop)
        }, this)
    }

    /**
     * Returns a boolean indicating whether the instance is currently
     */
    isUploading = (): boolean => {
        let uploading = false;

        BigUpUtils.each(this.files, file => {
            if (file.isUploading()) {
                uploading = true

                return false
            }
        })

        return uploading
    }

    /**
     * Should upload next chunk
     */
    _shouldUploadNext = (): boolean | number => {
        let num = 0
        let should = true
        const simultaneousUploads = this.opts.simultaneousUploads

        BigUpUtils.each(this.files, file => {
            BigUpUtils.each(file.chunks, chunk => {
                if (chunk.status() === 'uploading') {
                    num++

                    if (simultaneousUploads) {
                        if (num >= simultaneousUploads) {
                            should = false

                            return false
                        }
                    }
                }
            })
        })

        // if should is true then return uploading chunk's length
        return should && num
    }

    /**
     * Start or resume uploading.
     */
    upload = () => {
        // Make sure we don't start too many uploads at once
        const ret = this._shouldUploadNext();

        if (ret === false) {
            return
        }

        // Kick off the queue
        this.emit("uploadStart")

        let started = false

        if (this.opts.simultaneousUploads && typeof ret === "number") {
            for (let num = 1; num <= this.opts.simultaneousUploads - ret; num++) {
                started = this.uploadNextChunk(true) || started
            }
        }

        if (!started) {
            BigUpUtils.async(() => {
                this.emit("complete")
            }, this)
        }
    }

    /**
     * Resume uploading.
     */
    resume = () => {
        BigUpUtils.each(this.files, file => {
            if (!file.isComplete()) {
                file.resume()
            }
        })
    }

    /**
     * Pause uploading.
     */
    pause = () => {
        BigUpUtils.each(this.files, file => {
            file.pause()
        })
    }

    /**
     * Cancel upload of all BigUpFile objects and remove them from the list.
     */
    cancel = () => {
        for (let i = this.files.length - 1; i >= 0; i--) {
            this.files[i].cancel();
        }
    }

    /**
     * Returns a number between 0 and 1 indicating the current upload progress of all files.
     */
    progress = () => {
        let totalDone = 0
        let totalSize = 0

        // Resume all chunks currently being uploaded
        BigUpUtils.each(this.files, file => {
            totalDone += file.progress() * file.size
            totalSize += file.size
        })

        return totalSize > 0 ? totalDone / totalSize : 0
    }

    checksumFile = async (file: BigUpFile): Promise<string | undefined> => {
        return await file.setChecksum()
    }

    /**
     * Add a HTML5 File object to the list of files.
     */
    addFile = (file: OFile, event?: Event) => {
        this.addFiles([file], event)
    }

    /**
     * Add a HTML5 File object to the list of files.
     */
    addFiles = (fileList: FileList | any[], event?: Event) => {
        const files: BigUpFile[] = [];

        BigUpUtils.each(fileList, file => {
            if (
                (!BigUpUtils.ie10plus || BigUpUtils.ie10plus && file.size > 0)
                && !(file.size % 4096 === 0 && (file.name === '.' || file.fileName === '.'))
            ) {
                const uniqueIdentifier = this.generateUniqueIdentifier(file)

                if (this.opts.allowDuplicateUploads || !this.getFromUniqueIdentifier(uniqueIdentifier)) {
                    const f = new BigUpFile(this, file, uniqueIdentifier)

                    if (this.emit("fileAdded", f, event)) {
                        files.push(f)
                    }
                }
            }
        }, this)

        if (this.emit("filesAdded", files, event)) {
            BigUpUtils.each(files, file => {
                if (this.opts.singleFile && this.files.length > 0) {
                    this.removeFile(this.files[0])
                }
                this.files.push(file)
            }, this)

            this.emit("filesSubmitted", files, event)
        }
    }

    /**
     * Cancel upload of a specific BigUpFile object from the list.
     */
    removeFile = (file: BigUpFile) => {
        for (let i = this.files.length - 1; i >= 0; i--) {
            if (this.files[i] === file) {
                this.files.splice(i, 1)
                file.abort()

                this.emit("fileRemoved", file)
            }
        }
    }

    /**
     * Look up a BigUpFile object by its unique identifier.
     *
     * return false if file was not found
     */
    getFromUniqueIdentifier = (uniqueIdentifier: string): boolean | BigUpFile => {
        let ret = false

        BigUpUtils.each(this.files, file => {
            if (file.uniqueIdentifier === uniqueIdentifier) {
                ret = file
            }
        })

        return ret
    }

    /**
     * Returns the total size of all files in bytes.
     */
    getSize = (): number => {
        let totalSize = 0;

        BigUpUtils.each(this.files, file => {
            totalSize += file.size
        })

        return totalSize
    }

    /**
     * Returns the total size uploaded of all files in bytes.
     */
    sizeUploaded = (): number => {
        let size = 0;

        BigUpUtils.each(this.files, file => {
            size += file.sizeUploaded()
        })

        return size
    }

    /**
     * Returns remaining time to upload all files in seconds. Accuracy is based on average speed.
     * If speed is zero, time remaining will be equal to positive infinity `Number.POSITIVE_INFINITY`
     */
    timeRemaining = (): number => {
        let sizeDelta = 0
        let averageSpeed = 0

        BigUpUtils.each(this.files, file => {
            if (!file.paused && !file.error) {
                sizeDelta += file.size - file.sizeUploaded()
                averageSpeed += file.averageSpeed
            }
        })

        if (sizeDelta && !averageSpeed) {
            return Number.POSITIVE_INFINITY
        }

        if (!sizeDelta && !averageSpeed) {
            return 0
        }

        return Math.floor(sizeDelta / averageSpeed)
    }

    addQuery = (key: string, value: string | number): boolean => {
        let is = false

        if (this.opts.query) {
            this.opts.query[key] = value

            is = true
        }

        return is
    }

    hasQuery = (key: string): boolean => {
        if (this.opts.query) {
            return this.opts.query.hasOwnProperty(key)
        } else {
            return false
        }
    }
}
