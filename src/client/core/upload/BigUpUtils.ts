import {isArray} from "lodash";

export class BigUpUtils {
    // ie10+
    static ie10plus = typeof window !== "undefined" && (window.navigator as any).msPointerEnabled ? (window.navigator as any).msPointerEnabled : undefined

    static each(obj: any[] | object, callback: Function, context?: object) {
        if (!obj) {
            return
        }

        let key

        if (isArray(obj)) {
            for (key = 0; key < obj.length; key++) {
                if (callback.call(context, obj[key], key) === false) {
                    return
                }
            }
        } else {
            for (key in obj) {
                if (obj.hasOwnProperty(key) && callback.call(context, obj[key], key) === false) {
                    return
                }
            }
        }
    }

    static async eachAsync(obj: any[] | object, callback: Function, context?: object) {
        if (!obj) {
            return
        }

        let key

        if (isArray(obj)) {
            for (key = 0; key < obj.length; key++) {
                if (await callback.call(context, obj[key], key) === false) {
                    return
                }
            }
        } else {
            for (key in obj) {
                if (obj.hasOwnProperty(key) && await callback.call(context, obj[key], key) === false) {
                    return
                }
            }
        }
    }

    static async(fn, context) {
        setTimeout(fn.bind(context), 0)
    }

    static extend(dst, src) {
        BigUpUtils.each(arguments, (obj) => {
            if (obj !== dst) {
                BigUpUtils.each(obj, (value, key) => {
                    dst[key] = value
                })
            }
        })

        return dst
    }

    /**
     * If option is a function, evaluate it with given params
     */
    static evalOpts(data, ...args) {
        if (typeof data === "function") {
            // `arguments` is an object, not array, in FF, so:
            args = Array.prototype.slice.call(arguments)
            data = data.apply(null, args.slice(1))
        }

        return data
    }
}
