import {BigUp} from "./BigUp";
import {BigUpFile} from "./BigUpFile";
import {BigUpUtils} from "./BigUpUtils";
import {forEach} from "lodash";

export type T_BigUpChunkParams = {
    chunkNumber: number
    chunkSize: number
    currentChunkSize: number
    totalSize: number
    identifier: string
    filename: string
    relativePath: string
    totalChunks: number
    csFile?: string
    csChunk?: string
}

export enum E_ChunkPreprocessState {
    unprocessed,
    processing,
    finished
}

export enum E_ChunkReadState {
    notRead,
    reading,
    finished
}

export class BigUpChunk {
    /** Reference to parent BigUp instance */
    bigUpObj: BigUp

    /** Reference to parent BigUpFile object */
    fileObj: BigUpFile

    /** File offset */
    offset: number

    /** Indicates if chunk existence was checked on the server */
    tested: boolean = false

    /** Number of retries performed */
    retries: number = 0;

    /** Pending retry */
    pendingRetry: boolean = false

    /**
     * Preprocess state
     * 0 = unprocessed, 1 = processing, 2 = finished
     */
    preprocessState: E_ChunkPreprocessState = E_ChunkPreprocessState.unprocessed

    /**
     * Read state
     * 0 = not read, 1 = reading, 2 = finished
     */
    readState: E_ChunkReadState = E_ChunkReadState.notRead

    /** Checksums chunk local and uploaded */
    csChunk?: string

    /** Used to store the bytes read */
    bytes?: Blob

    /** Bytes transferred from total request size */
    loaded: number = 0

    /** Total request size */
    total: number = 0

    /** Size of a chunk */
    chunkSize: number

    /** Chunk start byte in a file */
    startByte: number

    /** A specific filename for this chunk which otherwise default to the main name */
    filename: string | null = null

    /** Chunk end byte in a file */
    endByte: number

    xhr?: XMLHttpRequest
    data: any

    constructor(bigUpObj: BigUp, fileObj: BigUpFile, offset: number) {
        this.bigUpObj = bigUpObj
        this.fileObj = fileObj
        this.offset = offset
        this.chunkSize = this.fileObj.chunkSize
        this.startByte = this.offset * this.chunkSize

        this.endByte = this.computeEndByte()
    }

    /**
     * Compute the end byte in a file
     */
    computeEndByte = () => {
        let endByte = Math.min(this.fileObj.size, (this.offset + 1) * this.chunkSize)

        if (this.fileObj.size - endByte < this.chunkSize && !this.bigUpObj.opts.forceChunkSize) {
            // The last chunk will be bigger than the chunk size,
            // but less than 2 * this.chunkSize
            endByte = this.fileObj.size
        }

        return endByte
    }

    /**
     * Send chunk event
     */
    event(event, ...args) {
        args = Array.prototype.slice.call(arguments)
        args.unshift(this)
        this.fileObj.chunkEvent.apply(this.fileObj, args)
    }

    /**
     * Catch progress event
     */
    progressHandler = (event: ProgressEvent) => {
        if (event.lengthComputable) {
            this.loaded = event.loaded
            this.total = event.total
        }

        this.event('progress', event)
    }

    /**
     * Catch test event
     */
    testHandler = (_: Event) => {
        const status = this.status(true)

        if (status === 'error') {
            this.event(status, this.message())
            this.bigUpObj.uploadNextChunk()
        } else if (status === 'success') {
            this.tested = true
            this.event(status, this.message())
            this.bigUpObj.uploadNextChunk()
        } else if (!this.fileObj.paused) {
            // Error might be caused by file pause method
            // Chunks does not exist on the server side
            this.tested = true
            this.send()
        }
    }

    /**
     * Upload has stopped
     */
    doneHandler = (_: Event) => {
        const status = this.status()

        if (status === 'success' || status === 'error') {
            delete this.data
            this.event(status, this.message())
            this.bigUpObj.uploadNextChunk()
        } else if (!this.fileObj.paused) {
            this.event('retry', this.message())
            this.pendingRetry = true
            this.abort()
            this.retries++
            const retryInterval = this.bigUpObj.opts.chunkRetryInterval

            if (retryInterval !== null) {
                setTimeout(() => {
                    this.send()
                }, retryInterval)
            } else {
                this.send()
            }
        }
    }

    /**
     * Get params for a request
     */
    getParams = (): T_BigUpChunkParams => {
        return {
            chunkNumber: this.offset + 1,
            chunkSize: this.chunkSize,
            currentChunkSize: this.endByte - this.startByte,
            totalSize: this.fileObj.size,
            identifier: this.fileObj.uniqueIdentifier,
            filename: this.fileObj.name,
            relativePath: this.fileObj.relativePath,
            totalChunks: this.fileObj.chunks.length,
            ...(
                this.bigUpObj.opts.checksum && this.fileObj.csFile && {
                    csFile: this.fileObj.csFile
                }
            )
        }
    }

    /**
     * Get target option with query params
     */
    getTarget = (target, params): string => {
        if (params.length == 0) {
            return target;
        }

        if (target.indexOf('?') < 0) {
            target += '?'
        } else {
            target += '&'
        }

        return target + params.join('&');
    }

    /**
     * Makes a GET request without any data to see if the chunk has already
     * been uploaded in a previous session
     */
    test = () => {
        // Set up request and listen for event
        this.xhr = new XMLHttpRequest()
        this.xhr.addEventListener("load", this.testHandler, false)
        this.xhr.addEventListener("error", this.testHandler, false)
        const testMethod = BigUpUtils.evalOpts(this.bigUpObj.opts.testMethod, this.fileObj, this)
        const data = this.prepareXhrRequest(testMethod, true)

        this.xhr.send(data)
    }

    checkDone = () => {
        this.xhr = new XMLHttpRequest()
        this.xhr.addEventListener("load", this.testHandler, false)
        this.xhr.addEventListener("error", this.testHandler, false)
        const testMethod = BigUpUtils.evalOpts(this.bigUpObj.opts.testMethod, this.fileObj, this)
        const data = this.prepareXhrRequest(testMethod, true, this.bigUpObj.opts.targetQueue)

        this.xhr.send(data)
    }

    /**
     * Finish preprocess state
     */
    preprocessFinished = () => {
        // Re-compute the endByte after the preprocess function to allow an
        // implementer of preprocess to set the fileObj size
        this.endByte = this.computeEndByte()

        this.preprocessState = E_ChunkPreprocessState.finished
        this.send()
    }

    /**
     * Finish read state
     */
    readFinished = (bytes) => {
        this.readState = E_ChunkReadState.finished
        this.bytes = bytes
        this.send()
    }

    /**
     * Uploads the actual data in a POST call
     */
    send = () => {
        const preprocess = this.bigUpObj.opts.preprocess;
        const read = this.bigUpObj.opts.readFileFn;

        if (typeof preprocess === 'function') {
            switch (this.preprocessState) {
                case E_ChunkPreprocessState.unprocessed:
                    this.preprocessState = E_ChunkPreprocessState.processing
                    preprocess(this)

                    return
                case E_ChunkPreprocessState.processing:
                    return
            }
        }

        switch (this.readState) {
            case E_ChunkReadState.notRead:
                this.readState = E_ChunkReadState.reading
                if (read) read(this.fileObj, this.startByte, this.endByte, this.fileObj.file.type, this)

                return
            case E_ChunkReadState.reading:
                return;
        }

        if (this.bigUpObj.opts.testChunks && !this.tested) {
            this.test()

            return
        }

        this.loaded = 0
        this.total = 0
        this.pendingRetry = false

        // Set up request and listen for event
        this.xhr = new XMLHttpRequest()
        this.xhr.upload.addEventListener('progress', this.progressHandler, false)
        this.xhr.addEventListener("load", this.doneHandler, false)
        this.xhr.addEventListener("error", this.doneHandler, false)

        const uploadMethod = BigUpUtils.evalOpts(this.bigUpObj.opts.uploadMethod, this.fileObj, this)
        let data = this.prepareXhrRequest(uploadMethod, false, undefined, this.bigUpObj.opts.method, this.bytes)

        const changeRawDataBeforeSend = this.bigUpObj.opts.changeRawDataBeforeSend
        if (typeof changeRawDataBeforeSend === 'function') {
            data = changeRawDataBeforeSend(this, data);
        }

        this.xhr.send(data)
    }

    /**
     * Abort current xhr request
     */
    abort = () => {
        // Abort and reset
        const xhr = this.xhr
        this.xhr = undefined

        if (xhr) {
            xhr.abort()
        }
    }

    /**
     * Retrieve current chunk upload status
     * returns 'pending', 'uploading', 'success', 'error'
     */
    status = (isTest?): string => {
        if (this.readState === E_ChunkReadState.reading) {
            return 'reading'
        } else if (this.pendingRetry || this.preprocessState === 1) {
            // if pending retry then that's effectively the same as actively uploading,
            // there might just be a slight delay before the retry starts
            return 'uploading'
        } else if (!this.xhr) {
            return 'pending'
        } else if (this.xhr.readyState < 4) {
            // Status is really 'OPENED', 'HEADERS_RECEIVED'
            // or 'LOADING' - meaning that stuff is happening
            return 'uploading'
        } else {
            if (this.bigUpObj.opts.successStatuses && this.bigUpObj.opts.successStatuses.indexOf(this.xhr.status) > -1) {
                // HTTP 200, perfect
                // HTTP 202 Accepted - The request has been accepted for processing, but the processing has not been completed.
                return 'success'
            } else if (
                (
                    this.bigUpObj.opts.permanentErrors
                    && this.bigUpObj.opts.permanentErrors.indexOf(this.xhr.status) > -1
                )
                || (
                    !isTest
                    && (
                        this.bigUpObj.opts.maxChunkRetries
                        && this.retries >= this.bigUpObj.opts.maxChunkRetries
                    )
                )
            ) {
                // HTTP 413/415/500/501, permanent error
                return 'error'
            } else {
                // this should never happen, but we'll reset and queue a retry
                // a likely case for this would be 503 service unavailable
                this.abort()

                return 'pending'
            }
        }
    }

    /**
     * Get response from xhr request
     */
    message = (): string => {
        return this.xhr ? this.xhr.responseText : '';
    }

    /**
     * Get upload progress
     */
    progress = (): number => {
        if (this.pendingRetry) {
            return 0
        }

        const s = this.status()

        if (s === 'success' || s === 'error') {
            return 1
        } else if (s === 'pending') {
            return 0
        } else {
            return this.total > 0 ? this.loaded / this.total : 0
        }
    }

    /**
     * Count total size uploaded
     */
    sizeUploaded = (): number => {
        let size = this.endByte - this.startByte

        // can't return only chunk.loaded value, because it is bigger than chunk size
        if (this.status() !== 'success') {
            size = this.progress() * size
        }

        return size
    }

    /**
     * Prepare Xhr request. Set query, headers and data
     */
    prepareXhrRequest = (method: string, isTest: boolean, targetUrl?: string, paramsMethod?: string, blob?: Blob): FormData | Blob | null => {
        // Add data from the query options
        let query = BigUpUtils.evalOpts(this.bigUpObj.opts.query, this.fileObj, this, isTest)
        query = BigUpUtils.extend(query || {}, this.getParams())

        const path = BigUpUtils.evalOpts(targetUrl ?? this.bigUpObj.opts.targetUpload, this.fileObj, this, isTest)

        let target = path
        let data

        let isPost: boolean

        if (method === 'GET' || paramsMethod === 'octet') {
            // Add data from the query options
            const params: string[] = []
            BigUpUtils.each(query, (v, k) => {
                params.push([encodeURIComponent(k), encodeURIComponent(v)].join('='))
            })
            target = this.getTarget(target, params)
            data = blob || null

            isPost = false
        } else {
            // Add data from the query options
            data = new FormData()

            data.append('raw', JSON.stringify(query))

            BigUpUtils.each(query, (v, k) => {
                data.append(k, v)
            })

            if (typeof blob !== "undefined" && this.bigUpObj.opts.fileParameterName) {
                data.append(this.bigUpObj.opts.fileParameterName, blob, this.filename || this.fileObj.file.name)
            }

            isPost = true
        }

        if (this.xhr) {
            const changeRawTargetBeforeSend = this.bigUpObj.opts.changeRawTargetBeforeSend
            if (typeof changeRawTargetBeforeSend === 'function') {
                target = changeRawTargetBeforeSend(
                    isPost
                        ? {
                            target: target,
                            path: path
                        }
                        : {
                            target: target,
                            path: path,
                            query: query
                        }
                )
            }

            this.xhr.open(method, target, true)
            if (this.bigUpObj.opts.withCredentials !== undefined) {
                this.xhr.withCredentials = this.bigUpObj.opts.withCredentials
            }

            let header = {}

            // Add data from header options
            BigUpUtils.each(BigUpUtils.evalOpts(this.bigUpObj.opts.headers, this.fileObj, this, isTest), (v, k) => {
                header[k] = v;
            }, this)

            const changeRawHeaderBeforeSend = this.bigUpObj.opts.changeRawHeaderBeforeSend
            if (typeof changeRawHeaderBeforeSend === 'function') {
                header = changeRawHeaderBeforeSend(this, header);
            }

            forEach(header, (value, key) => {
                if (this.xhr) this.xhr.setRequestHeader(key, value)
            })
        }

        return data
    }
}
