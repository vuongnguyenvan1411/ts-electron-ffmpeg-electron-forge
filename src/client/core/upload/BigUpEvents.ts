import {BigUpChunk} from "./BigUpChunk"
import {BigUpFile} from "./BigUpFile"

// export class BigUpEventName {
//     public static readonly complete = "complete"
//     public static readonly uploadStart = "uploadStart"
//     public static readonly fileAdded = "fileAdded"
//     public static readonly filesAdded = "filesAdded"
//     public static readonly filesSubmitted = "filesSubmitted"
//     public static readonly fileRemoved = "fileRemoved"
//     public static readonly fileProgress = "fileProgress"
//     public static readonly progress = "progress"
//     public static readonly fileError = "fileError"
//     public static readonly error = "error"
//     public static readonly fileSuccess = "fileSuccess"
//     public static readonly fileRetry = "fileRetry"
// }

export interface BigUpEventMap {
    fileSuccess: FileSuccessCallbackArguments
    fileProgress: FileProgressCallbackArguments
    fileChecksum: FileChecksumCallbackArguments
    fileAdded: FileAddedCallbackArguments
    filesAdded: FilesAddedCallbackArguments
    filesSubmitted: FilesSubmittedCallbackArguments
    fileRemoved: FileRemovedCallbackArguments
    fileRetry: FileRetryCallbackArguments
    fileError: FileErrorCallbackArguments
    uploadStart: UploadStartCallbackArguments
    complete: CompleteCallbackArguments
    progress: ProgressCallbackArguments
    error: ErrorCallbackArguments
    catchAll: CatchAllCallbackArguments
}

export type EventName = keyof BigUpEventMap
export type BigUpEvent = BigUpEventMap[keyof BigUpEventMap]

type BigUpEventFromEventName<T extends EventName> = BigUpEventMap[T]
type BigUpEventTypeFromBigUpEvent<T extends BigUpEvent> = T extends BigUpEventFromEventName<infer U> ? U : never

type FileSuccessCallbackArguments = [BigUpFile, string, BigUpChunk]
type FileProgressCallbackArguments = [BigUpFile, BigUpChunk]
type FileChecksumCallbackArguments = [BigUpFile, string]
type FileAddedCallbackArguments = [BigUpFile, Event]
type FilesAddedCallbackArguments = [BigUpFile[], Event]
type FilesSubmittedCallbackArguments = [BigUpFile[], Event]
type FileRemovedCallbackArguments = [BigUpFile]
type FileRetryCallbackArguments = [BigUpFile, BigUpChunk]
type FileErrorCallbackArguments = [BigUpFile, string, BigUpChunk]
type UploadStartCallbackArguments = []
type CompleteCallbackArguments = []
type ProgressCallbackArguments = []
type ErrorCallbackArguments = [string, BigUpFile, BigUpChunk]
type CatchAllCallbackArguments = [Event]
