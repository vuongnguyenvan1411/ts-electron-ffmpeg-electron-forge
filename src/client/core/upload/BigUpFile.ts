import {BigUp} from "./BigUp";
import {BigUpChunk, E_ChunkPreprocessState, E_ChunkReadState} from "./BigUpChunk";
import {BigUpUtils} from "./BigUpUtils";
import {deburr} from "lodash";
import {Utils} from "../Utils";
import {BigUpChecksum} from "./BigUpChecksum";

export class BigUpFile {
    /** Reference to parent BigUp instance */
    bigUpObj: BigUp

    /** Reference to file */
    file: File

    /** Checksums file local and uploaded */
    csFile?: string

    /** File name. Some confusion in different versions of Firefox */
    name: string

    /** File size */
    size: number

    /** Relative file path */
    relativePath: string

    /** File unique identifier */
    uniqueIdentifier: string

    /** Size of Each Chunk */
    chunkSize: number = 0

    /** List of chunks */
    chunks: BigUpChunk[] = []

    /** Indicated if file is paused */
    paused: boolean = false

    /** Indicated if file has encountered an error */
    error: boolean = false

    /** Average upload speed */
    averageSpeed: number = 0

    /** Current upload speed */
    currentSpeed: number = 0

    /** Date then progress was called last time */
    private _lastProgressCallback: number = Date.now()

    /** Previously uploaded file size */
    private _prevUploadedSize: number = 0

    /** Holds previous progress */
    private _prevProgress: number = 0

    constructor(bigUpObj: BigUp, file: File, uniqueIdentifier?: string) {
        this.bigUpObj = bigUpObj
        this.file = file
        this.name = BigUpFile.slugName((file as any).fileName ?? file.name)
        this.size = file.size
        this.relativePath = ('relativePath' in file) ? (file as any).relativePath : (file.webkitRelativePath ?? this.name)
        this.uniqueIdentifier = (uniqueIdentifier === undefined ? bigUpObj.generateUniqueIdentifier(file) : uniqueIdentifier)

        this.bootstrap()
    }

    static slugName = (name: string) => {
        name = deburr(name.toLowerCase())

        const sp = name.split('.')
        const ext = sp[sp.length - 1].toLowerCase()
        const basename = sp.slice(0, sp.length - 1).join('.')
        const filter = Utils.strSlug(basename)

        return `${filter}.${ext}`;
    }

    setChecksum = async (): Promise<string | undefined> => {
        let hash

        if (this.bigUpObj.opts.checksum) {
            const checksums = new BigUpChecksum(this.file)
            hash = await checksums.readFile(this.file)

            this.csFile = hash

            this.bigUpObj.emit("fileChecksum", this, hash)
        }

        return hash
    }

    /**
     * Update speed parameters
     * @link http://stackoverflow.com/questions/2779600/how-to-estimate-download-time-remaining-accurately
     */
    measureSpeed = () => {
        const timeSpan = Date.now() - this._lastProgressCallback

        if (!timeSpan) {
            return
        }

        const smoothingFactor = this.bigUpObj.opts.speedSmoothingFactor
        const uploaded = this.sizeUploaded()

        // Prevent negative upload speed after file upload resume
        this.currentSpeed = Math.max((uploaded - this._prevUploadedSize) / timeSpan * 1000, 0)
        if (smoothingFactor) {
            this.averageSpeed = smoothingFactor * this.currentSpeed + (1 - smoothingFactor) * this.averageSpeed
        }
        this._prevUploadedSize = uploaded
    }

    /**
     * For internal usage only.
     * Callback when something happens within the chunk.
     */
    chunkEvent = (chunk: BigUpChunk, event: 'progress' | 'success' | 'error' | 'retry', message: string) => {
        switch (event) {
            case 'progress':
                if (
                    this.bigUpObj.opts.progressCallbacksInterval
                    && Date.now() - this._lastProgressCallback < this.bigUpObj.opts.progressCallbacksInterval
                ) {
                    break
                }

                this.measureSpeed();
                this.bigUpObj.emit("fileProgress", this, chunk)
                this.bigUpObj.emit("progress")
                this._lastProgressCallback = Date.now()

                break
            case 'error':
                this.error = true
                this.abort(true)
                this.bigUpObj.emit("fileError", this, message, chunk)
                this.bigUpObj.emit("error", message, this, chunk)

                break
            case 'success':
                if (this.error) {
                    return
                }

                this.measureSpeed()
                this.bigUpObj.emit("fileProgress", this, chunk)
                this.bigUpObj.emit("progress")
                this._lastProgressCallback = Date.now()
                if (this.isComplete()) {
                    this.currentSpeed = 0
                    this.averageSpeed = 0
                    this.bigUpObj.emit("fileSuccess", this, message, chunk)
                }

                break
            case 'retry':
                this.bigUpObj.emit("fileRetry", this, chunk)

                break
        }
    }

    /**
     * Pause file upload
     */
    pause = () => {
        this.paused = true
        this.abort()
    }

    /**
     * Resume file upload
     */
    resume = () => {
        this.paused = false
        this.bigUpObj.upload()
    }

    /**
     * Abort current upload
     */
    abort = (reset?) => {
        this.currentSpeed = 0
        this.averageSpeed = 0
        const chunks = this.chunks

        if (reset) {
            this.chunks = []
        }

        BigUpUtils.each(chunks, c => {
            if (c.status() === 'uploading') {
                c.abort();
                this.bigUpObj.uploadNextChunk();
            }
        }, this)
    }

    /**
     * Cancel current upload and remove from a list
     */
    cancel = () => {
        this.bigUpObj.removeFile(this)
    }

    /**
     * Retry aborted file upload
     */
    retry = () => {
        this.bootstrap()
        this.bigUpObj.upload()
    }

    /**
     * Clear current chunks and slice file again
     */
    bootstrap = () => {
        if (typeof this.bigUpObj.opts.initFileFn === "function") {
            const ret = this.bigUpObj.opts.initFileFn(this)

            if (ret && 'then' in ret) {
                ret.then(this._bootstrap)

                return
            }
        }

        this._bootstrap()
    }

    private _bootstrap = () => {
        this.abort(true)
        this.error = false
        // Rebuild stack of chunks from file
        this._prevProgress = 0
        const round = this.bigUpObj.opts.forceChunkSize ? Math.ceil : Math.floor
        this.chunkSize = BigUpUtils.evalOpts(this.bigUpObj.opts.chunkSize, this)
        const chunks = Math.max(round(this.size / this.chunkSize), 1)

        for (let offset = 0; offset < chunks; offset++) {
            this.chunks.push(
                new BigUpChunk(this.bigUpObj, this, offset)
            )
        }
    }

    /**
     * Get current upload progress status
     * return number from 0 to 1
     */
    progress = (): number => {
        if (this.error) {
            return 1
        }

        if (this.chunks.length === 1) {
            this._prevProgress = Math.max(this._prevProgress, this.chunks[0].progress())
            return this._prevProgress
        }

        // Sum up progress across everything
        let bytesLoaded = 0
        BigUpUtils.each(this.chunks, c => {
            // get chunk progress relative to entire file
            bytesLoaded += c.progress() * (c.endByte - c.startByte)
        })

        const percent = bytesLoaded / this.size
        // We don't want to lose percentages when an upload is paused
        this._prevProgress = Math.max(this._prevProgress, percent > 0.9999 ? 1 : percent)

        return this._prevProgress
    }

    /**
     * Indicates if file is being uploaded at the moment
     */
    isUploading = (): boolean => {
        let uploading = false

        BigUpUtils.each(this.chunks, chunk => {
            if (chunk.status() === 'uploading') {
                uploading = true

                return false
            }
        })

        return uploading
    }

    /**
     * Indicates if file is has finished uploading and received a response
     */
    isComplete = (): boolean => {
        let outstanding = false;

        BigUpUtils.each(this.chunks, function (chunk) {
            const status = chunk.status()

            if (
                status === 'pending'
                || status === 'uploading'
                || status === 'reading'
                || chunk.preprocessState === E_ChunkPreprocessState.processing
                || chunk.readState === E_ChunkReadState.reading
            ) {
                outstanding = true

                return false
            }
        })

        return !outstanding
    }

    /**
     * Count total size uploaded
     */
    sizeUploaded = (): number => {
        let size = 0;

        BigUpUtils.each(this.chunks, chunk => {
            size += chunk.sizeUploaded()
        })

        return size
    }

    /**
     * Returns remaining time to finish upload file in seconds. Accuracy is based on average speed.
     * If speed is zero, time remaining will be equal to positive infinity `Number.POSITIVE_INFINITY`
     */
    timeRemaining = (): number => {
        if (this.paused || this.error) {
            return 0
        }

        const delta = this.size - this.sizeUploaded()

        if (delta && !this.averageSpeed) {
            return Number.POSITIVE_INFINITY
        }

        if (!delta && !this.averageSpeed) {
            return 0
        }

        return Math.floor(delta / this.averageSpeed)
    }

    /**
     * Get file type
     */
    getType = (): string => {
        return this.file.type && this.file.type.split('/')[1]
    }

    /**
     * Get file extension
     */
    getExtension = (): string => {
        return this.name.substr((~-this.name.lastIndexOf(".") >>> 0) + 2).toLowerCase();
    }
}

export interface OFile extends File {
    uid: string;
}

export interface OFileList extends FileList {
    item(index: number): OFile | null;

    [index: number]: OFile;
}
