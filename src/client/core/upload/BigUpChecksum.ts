import {createMD5} from "hash-wasm";
import {IHasher} from "hash-wasm/lib/WASMInterface";
import EventEmitter from "eventemitter3";

type _T_Options = {
    chunkSize?: number
}

export class BigUpChecksum extends EventEmitter {
    file: File
    opts: _T_Options
    fileReader: FileReader
    hasher?: IHasher
    chunkSize: number

    constructor(file: File, opts?: _T_Options) {
        super()

        this.file = file
        this.opts = opts ?? {}

        this.fileReader = new FileReader()

        this.chunkSize = opts?.chunkSize ?? 1024 * 1024 * 64
    }

    protected hashChunk(chunk: Blob) {
        return new Promise((resolve) => {
            this.fileReader.onload = async (e) => {
                let is = false

                if (e.target && e.target.result) {
                    const view = new Uint8Array(e.target.result as any);

                    if (this.hasher) {
                        this.hasher.update(view)

                        is = true
                    }
                }

                resolve(is)
            }

            this.fileReader.readAsArrayBuffer(chunk)
        })
    }

    readFile = async (file?: File) => {
        if (!file) {
            file = this.file
        }

        if (this.hasher) {
            this.hasher.init()
        } else {
            this.hasher = await createMD5()
        }

        const chunkNumber = Math.floor(file.size / this.chunkSize)

        for (let i = 0; i <= chunkNumber; i++) {
            const chunk = file.slice(
                this.chunkSize * i,
                Math.min(this.chunkSize * (i + 1), file.size)
            )

            await this.hashChunk(chunk)

            // this.emit('progress', i, chunkNumber)
        }

        return this.hasher.digest()
    }
}
