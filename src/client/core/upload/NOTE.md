## How can I use it?

A new `BigUp` object is created with information of what and where to post:

```javascript
var bigUp = new BigUp({
    target: '/api/upload',
    query: {upload_token: 'token'}
});
// BigUp isn't supported, fall back on a different method
if (!bigUp.support) location.href = '/some-old-api-uploader';
```

To allow files to be either selected and drag-dropped, you'll assign drop target and a DOM item to be clicked for
browsing:

```javascript
bigUp.assignBrowse(document.getElementById('browseButton'));
bigUp.assignDrop(document.getElementById('dropTarget'));
```

After this, interaction with BigUp is done by listening to events:

```javascript
bigUp.on('fileAdded', function (file, event) {
    console.log(file, event);
});
bigUp.on('fileSuccess', function (file, message) {
    console.log(file, message);
});
bigUp.on('fileError', function (file, message) {
    console.log(file, message);
});
```

## How do I set it up with my server?

Most of the magic for BigUp happens in the user's browser, but files still need to be reassembled from chunks on the
server side. This should be a fairly simple task and can be achieved in any web framework or language, which is able to
receive file uploads.

To handle the state of upload chunks, a number of extra parameters are sent along with all requests:

* `chunkNumber`: The index of the chunk in the current upload. First chunk is `1` (no base-0 counting here).
* `totalChunks`: The total number of chunks.
* `chunkSize`: The general chunk size. Using this value and `totalSize` you can calculate the total number of chunks.
  Please note that the size of the data received in the HTTP might be lower than `chunkSize` of this for the last chunk
  for a file.
* `totalSize`: The total file size.
* `identifier`: A unique identifier for the file contained in the request.
* `filename`: The original file name (since a bug in Firefox results in the file name not being transmitted in chunk
  multipart posts).
* `relativePath`: The file's relative path when selecting a directory (defaults to file name in all browsers except
  Chrome).

You should allow for the same chunk to be uploaded more than once; this isn't standard behaviour, but on an unstable
network environment it could happen, and this case is exactly what BigUp is designed for.

For every request, you can confirm reception in HTTP status codes (can be change through the `permanentErrors` option):

* `200`, `201`, `202`: The chunk was accepted and correct. No need to re-upload.
* `404`, `415`. `500`, `501`: The file for which the chunk was uploaded is not supported, cancel the entire upload.
* _Anything else_: Something went wrong, but try reuploading the file.

## Handling GET (or `test()` requests)

Enabling the `testChunks` option will allow uploads to be resumed after browser restarts and even across browsers (in
theory you could even run the same file upload across multiple tabs or different browsers). The `POST` data requests
listed are required to use BigUp to receive data, but you can extend support by implementing a corresponding `GET`
request with the same parameters:

* If this request returns a `200`, `201` or `202` HTTP code, the chunks is assumed to have been completed.
* If request returns a permanent error status, upload is stopped.
* If request returns anything else, the chunk will be uploaded in the standard fashion.

After this is done and `testChunks` enabled, an upload can quickly catch up even after a browser restart by simply
verifying already uploaded chunks that do not need to be uploaded again.

## Full documentation

### BigUp

#### Configuration

The object is loaded with a configuration options:

```javascript
var r = new BigUp({opt1: 'val', ...});
```

Available configuration options are:

* `target` The target URL for the multipart POST request. This can be a string or a function. If a
  function, it will be passed a BigUpFile, a BigUpChunk and isTest boolean (Default: `/`)
* `singleFile` Enable single file upload. Once one file is uploaded, second file will overtake existing one, first one
  will be canceled. (Default: false)
* `chunkSize` The size in bytes of each uploaded chunk of data. This can be a number or a function. If a function, it
  will be passed a BigUpFile. The last uploaded chunk will be at least this size and up to two the size,
  see [Issue #51](https://github.com/23/resumable.js/issues/51) for details and reasons. (Default: `1*1024*1024`, 1MB)
* `forceChunkSize` Force all chunks to be less or equal than chunkSize. Otherwise, the last chunk will be greater than
  or equal to `chunkSize`. (Default: `false`)
* `simultaneousUploads` Number of simultaneous uploads (Default: `3`)
* `fileParameterName` The name of the multipart POST parameter to use for the file chunk  (Default: `file`)
* `query` Extra parameters to include in the multipart POST with data. This can be an object or a
  function. If a function, it will be passed a BigUpFile, a BigUpChunk object and a isTest boolean
  (Default: `{}`)
* `headers` Extra headers to include in the multipart POST with data. If a function, it will be passed a BigUpFile, a
  BigUpChunk object and a isTest boolean (Default: `{}`)
* `withCredentials` Standard CORS requests do not send or set any cookies by default. In order to
  include cookies as part of the request, you need to set the `withCredentials` property to true.
  (Default: `false`)
* `method` Method to use when POSTing chunks to the server (`multipart` or `octet`) (Default: `multipart`)
* `testMethod` HTTP method to use when chunks are being tested. If set to a function, it will be passed a BigUpFile and
  a BigUpChunk arguments. (Default: `GET`)
* `uploadMethod` HTTP method to use when chunks are being uploaded. If set to a function, it will be passed a BigUpFile
  and a BigUpChunk arguments. (Default: `POST`)
* `allowDuplicateUploads ` Once a file is uploaded, allow reupload of the same file. By default, if a file is already
  uploaded, it will be skipped unless the file is removed from the existing BigUp object. (Default: `false`)
* `prioritizeFirstAndLastChunk` Prioritize first and last chunks of all files. This can be handy if you can determine if
  a file is valid for your service from only the first or last chunk. For example, photo or video metadata is usually
  located in the first part of a file, making it easy to test support from only the first chunk. (Default: `false`)
* `testChunks` Make a GET request to the server for each chunks to see if it already exists. If implemented on the
  server-side, this will allow for upload resumes even after a browser crash or even a computer restart. (
  Default: `true`)
* `preprocess` Optional function to process each chunk before testing & sending. To the function it will be passed the
  chunk as parameter, and should call the `preprocessFinished` method on the chunk when finished. (Default: `null`)
* `changeRawDataBeforeSend` Optional function to change Raw Data just before the XHR Request can be sent for each chunk.
  To the function, it will be passed the chunk and the data as a Parameter. Return the data which will be then sent to
  the XHR request without further modification. (Default: `null`). This is helpful when using BigUp
  with [Google Cloud Storage](https://cloud.google.com/storage/docs/json_api/v1/how-tos/multipart-upload).
  <br/><br/>
  Add functionality to change Raw Body Data before sending.<br/>
  This is particularly helpful when using Upload with Google Storage and many other platforms where we have to upload
  multipart related form data. (i.e. Different parts in the same request).<br/>
  Example:<br/>
  Google Cloud Storage expects data body to be in this format.<br/>
  [BOUNDARY] is the Content Boundary which can be set using Custom Header function.<br/>
  [FILE_DATA] is actually the chunk body in this case. (which is a blob).<br/>

```
--[BOUNDARY]
Content-Type: application/json; charset=UTF-8

{
  "name": "myObject",
  "metadata": {
  "a":"b"
  }
}

--[BOUNDARY]
Content-Type: image/jpeg

[FILE_DATA]
--[BOUNDARY]--
```

* `initFileFn` Optional function to initialize the fileObject. To the function it will be passed a BigUpFile and a
  BigUpChunk arguments.
* `readFileFn` Optional function wrapping reading operation from the original file. To the function it will be passed
  the BigUpFile, the startByte and endByte, the fileType and the BigUpChunk.
* `generateUniqueIdentifier` Override the function that generates unique identifiers for each file. (Default: `null`)
* `maxChunkRetries` The maximum number of retries for a chunk before the upload is failed. Valid values are any positive
  integer and `undefined` for no limit. (Default: `0`)
* `chunkRetryInterval` The number of milliseconds to wait before retrying a chunk on a non-permanent error. Valid values
  are any positive integer and `undefined` for immediate retry. (Default: `undefined`)
* `progressCallbacksInterval` The time interval in milliseconds between progress reports. Set it
  to 0 to handle each progress callback. (Default: `500`)
* `speedSmoothingFactor` Used for calculating average upload speed. Number from 1 to 0. Set to 1
  and average upload speed wil be equal to current upload speed. For longer file uploads it is
  better set this number to 0.02, because time remaining estimation will be more accurate. This
  parameter must be adjusted together with `progressCallbacksInterval` parameter. (Default 0.1)
* `successStatuses` Response is success if response status is in this list (Default: `[200,201,
  202]`)
* `permanentErrors` Response fails if response status is in this list (Default: `[404, 415, 500, 501]`)

#### Properties

* `.support` A boolean value indicator whether is supported by the current browser.
* `.supportDirectory` A boolean value, which indicates if browser supports directory uploads.
* `.opts` A hash object of the configuration of the instance.
* `.files` An array of `BigUpFile` file objects added by the user (see full docs for this object type below).

#### Methods

* `.assignBrowse(domNodes, isDirectory, singleFile, attributes)` Assign a browse action to one or more DOM nodes.
    * `domNodes` array of dom nodes or a single node.
    * `isDirectory` Pass in `true` to allow directories to be selected (Chrome only, support can be checked
      with `supportDirectory` property).
    * `singleFile` To prevent multiple file uploads set this to true. Also look at config parameter `singleFile`.
    * `attributes` Pass object of keys and values to set custom attributes on input fields.
      For example, you can set `accept` attribute to `image/*`. This means that user will be able to select only images.
      Full list of attributes: https://www.w3.org/wiki/HTML/Elements/input/file

  Note: avoid using `a` and `button` tags as file upload buttons, use span instead.
* `.assignDrop(domNodes)` Assign one or more DOM nodes as a drop target.
* `.unAssignDrop(domNodes)` Unassign one or more DOM nodes as a drop target.
* `.on(event, callback)` Listen for event from BigUp (see below)
* `.off([event, [callback]])`:
    * `.off()` All events are removed.
    * `.off(event)` Remove all callbacks of specific event.
    * `.off(event, callback)` Remove specific callback of event. `callback` should be a `Function`.
* `.upload()` Start or resume uploading.
* `.pause()` Pause uploading.
* `.resume()` Resume uploading.
* `.cancel()` Cancel upload of all `BigUpFile` objects and remove them from the list.
* `.progress()` Returns a float between 0 and 1 indicating the current upload progress of all files.
* `.isUploading()` Returns a boolean indicating whether the instance is currently uploading anything.
* `.addFile(file)` Add a HTML5 File object to the list of files.
* `.removeFile(file)` Cancel upload of a specific `BigUpFile` object on the list from the list.
* `.getFromUniqueIdentifier(uniqueIdentifier)` Look up a `BigUpFile` object by its unique identifier.
* `.getSize()` Returns the total size of the upload in bytes.
* `.sizeUploaded()` Returns the total size uploaded of all files in bytes.
* `.timeRemaining()` Returns remaining time to upload all files in seconds. Accuracy is based on average speed. If speed
  is zero, time remaining will be equal to positive infinity `Number.POSITIVE_INFINITY`

#### Events

* `.fileSuccess(file, message, chunk)` A specific file was completed. First argument `file` is instance of `BigUpFile`,
  second argument `message` contains server response. Response is always a string.
  Third argument `chunk` is instance of `BigUpChunk`. You can get response status by accessing xhr
  object `chunk.xhr.status`.
* `.fileProgress(file, chunk)` Uploading progressed for a specific file.
* `.fileAdded(file, event)` This event is used for file validation. To reject this file return false.
  This event is also called before file is added to upload queue,
  this means that calling `bigUp.upload()` function will not start current file upload.
  Optionally, you can use the browser `event` object from when the file was
  added.
* `.filesAdded(array, event)` Same as fileAdded, but used for multiple file validation.
* `.filesSubmitted(array, event)` Same as filesAdded, but happens after the file is added to upload queue. Can be used
  to start upload of currently added files.
* `.fileRemoved(file)` The specific file was removed from the upload queue. Combined with filesSubmitted, can be used to
  notify UI to update its state to match the upload queue.
* `.fileRetry(file, chunk)` Something went wrong during upload of a specific file, uploading is being
  retried.
* `.fileError(file, message, chunk)` An error occurred during upload of a specific file.
* `.uploadStart()` Upload has been started on the BigUp object.
* `.complete()` Uploading completed.
* `.progress()` Uploading progress.
* `.error(message, file, chunk)` An error, including fileError, occurred.
* `.catchAll(event, ...)` Listen to all the events listed above with the same callback function.

### BigUpFile

BigUpFile constructor can be accessed in `BigUpFile`.

#### Properties

* `.bigUpObj` A back-reference to the parent `BigUp` object.
* `.file` The correlating HTML5 `File` object.
* `.name` The name of the file.
* `.relativePath` The relative path to the file (defaults to file name if relative path doesn't exist)
* `.size` Size in bytes of the file.
* `.uniqueIdentifier` A unique identifier assigned to this file object. This value is included in uploads to the server
  for reference, but can also be used in CSS classes etc when building your upload UI.
* `.averageSpeed` Average upload speed, bytes per second.
* `.currentSpeed` Current upload speed, bytes per second.
* `.chunks` An array of `BigUpChunk` items. You shouldn't need to dig into these.
* `.paused` Indicated if file is paused.
* `.error` Indicated if file has encountered an error.

#### Methods

* `.progress(relative)` Returns a float between 0 and 1 indicating the current upload progress of the file.
  If `relative` is `true`, the value is returned relative to all files in the BigUp instance.
* `.pause()` Pause uploading the file.
* `.resume()` Resume uploading the file.
* `.cancel()` Abort uploading the file and delete it from the list of files to upload.
* `.retry()` Retry uploading the file.
* `.bootstrap()` Rebuild the state of a `BigUpFile` object, including reassigning chunks and XMLHttpRequest instances.
* `.isUploading()` Returns a boolean indicating whether file chunks is uploading.
* `.isComplete()` Returns a boolean indicating whether the file has completed uploading and received a server response.
* `.sizeUploaded()` Returns size uploaded in bytes.
* `.timeRemaining()` Returns remaining time to finish upload file in seconds. Accuracy is based on average speed. If
  speed is zero, time remaining will be equal to positive infinity `Number.POSITIVE_INFINITY`
* `.getExtension()` Returns file extension in lowercase.
* `.getType()` Returns file type.
