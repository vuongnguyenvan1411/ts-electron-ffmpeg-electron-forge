export  type TAnimation = "easeOut" |
    "linear" |
    "easeInQuad" |
    "easeOutQuad" |
    "easeInOutQuad" |
    "easeInCubic" |
    "easeOutCubic" |
    "easeInOutCubic" |
    "easeInQuart" |
    "easeOutQuart" |
    "easeInOutQuart" |
    "easeInQuint" |
    "easeOutQuint" |
    "easeInOutQuint";