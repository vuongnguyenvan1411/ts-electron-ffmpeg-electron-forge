import {ReactZoomPanPinchContext} from "react-zoom-pan-pinch";
import {getDelta, getMousePosition, handleCalculateWheelZoom} from "./wheel.utils";
import {handleCalculateBounds} from "../bounds/bounds.utils";
import {handleCalculateZoomPositions} from "../zoom/zoom.utils";
import {getContext, handleCallback} from "../../utils";

export const handleWheelZoom = (
    contextInstance: ReactZoomPanPinchContext,
    event: WheelEvent,
): void => {
    const {onWheel, onZoom} = contextInstance.props;

    const {contentComponent, setup, transformState} = contextInstance;
    const {scale} = transformState;
    const {limitToBounds, centerZoomedOut, zoomAnimation, wheel} = setup;
    const {size, disabled} = zoomAnimation;
    const {step} = wheel;

    if (!contentComponent) {
        throw new Error("Component not mounted");
    }

    event.preventDefault();
    event.stopPropagation();

    const delta = getDelta(event, null);
    const newScale = handleCalculateWheelZoom(
        contextInstance,
        delta,
        step,
        !event.ctrlKey,
    );

    // if scale not change
    if (scale === newScale) return;

    const bounds = handleCalculateBounds(contextInstance, newScale);

    const mousePosition = getMousePosition(event, contentComponent, scale);

    const isPaddingDisabled = disabled || size === 0 || centerZoomedOut;
    const isLimitedToBounds = limitToBounds && isPaddingDisabled;

    const {x, y} = handleCalculateZoomPositions(
        contextInstance,
        mousePosition.x,
        mousePosition.y,
        newScale,
        bounds,
        isLimitedToBounds,
    );

    contextInstance.previousWheelEvent = event;

    contextInstance.setTransformState(newScale, x, y);

    handleCallback(getContext(contextInstance), event, onWheel);
    handleCallback(getContext(contextInstance), event, onZoom);
};

export const handleWheelZoom2 = (
    contextInstance: ReactZoomPanPinchContext,
    contextInstance2: ReactZoomPanPinchContext,
    event: WheelEvent,
): void => {
    // const { onWheel, onZoom } = contextInstance.props;

    const {contentComponent, setup, transformState} = contextInstance;
    const {scale} = transformState;
    const {limitToBounds, centerZoomedOut, zoomAnimation, wheel} = setup;
    const {size, disabled} = zoomAnimation;
    const {step} = wheel;

    if (!contentComponent) {
        throw new Error("Component not mounted");
    }

    event.preventDefault();
    event.stopPropagation();

    const delta = getDelta(event, null);
    const newScale = handleCalculateWheelZoom(
        contextInstance,
        delta,
        step,
        !event.ctrlKey,
    );

    // if scale not change
    if (scale === newScale) return;

    const bounds = handleCalculateBounds(contextInstance, newScale);

    const mousePosition = getMousePosition(event, contentComponent, scale);

    const isPaddingDisabled = disabled || size === 0 || centerZoomedOut;
    const isLimitedToBounds = limitToBounds && isPaddingDisabled;

    const {x, y} = handleCalculateZoomPositions(
        contextInstance,
        mousePosition.x,
        mousePosition.y,
        newScale,
        bounds,
        isLimitedToBounds,
    );

    contextInstance2.previousWheelEvent = event;

    contextInstance2.setTransformState(newScale, x, y);

    // handleCallback(getContext(contextInstance), event, onWheel);
    // handleCallback(getContext(contextInstance), event, onZoom);
};

export const handleWheelZoomCustom = (
    contextInstance: ReactZoomPanPinchContext,
    event: WheelEvent,
): any => {
    const {contentComponent, setup, transformState} = contextInstance;
    const {scale} = transformState;
    const {limitToBounds, centerZoomedOut, zoomAnimation, wheel} = setup;
    const {size, disabled} = zoomAnimation;
    const {step} = wheel;

    if (!contentComponent) {
        throw new Error("Component not mounted");
    }

    event.preventDefault();
    event.stopPropagation();

    const delta = getDelta(event, null);
    const newScale = handleCalculateWheelZoom(
        contextInstance,
        delta,
        step,
        !event.ctrlKey,
    );

    // if scale not change
    if (scale === newScale) return;

    const bounds = handleCalculateBounds(contextInstance, newScale);

    const mousePosition = getMousePosition(event, contentComponent, scale);

    const isPaddingDisabled = disabled || size === 0 || centerZoomedOut;
    const isLimitedToBounds = limitToBounds && isPaddingDisabled;

    const {x, y} = handleCalculateZoomPositions(
        contextInstance,
        mousePosition.x,
        mousePosition.y,
        newScale,
        bounds,
        isLimitedToBounds,
    );

    contextInstance.previousWheelEvent = event;

    return {
        scale: newScale,
        x: x,
        y: y,
    };
};
