import {ReactZoomPanPinchRef} from "react-zoom-pan-pinch";


export const handleCallback = <T>(
    context: ReactZoomPanPinchRef,
    event: T,
    callback?: (context: ReactZoomPanPinchRef, event: T) => void,
): void => {
    if (callback && typeof callback === "function") {
        callback(context, event);
    }
};
