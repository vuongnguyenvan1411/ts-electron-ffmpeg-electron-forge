export type TSetup = "centerZoomedOut" |
    "alignmentAnimation" |
    "centerOnInit" |
    "children" |
    "disabled" |
    "doubleClick" |
    "initialPositionX" |
    "initialPositionY" |
    "minScale";
