import {isBrowser, isMobile} from "react-device-detect";
import {Normalize} from "../core/Normalize";
import moment from "moment/moment";
import {unitOfTime} from "moment";
import {E_WebType} from "../const/Types";

enum _E_OsType {
    Android = "Android",
    IOS = "iOS",
    MAC_OS = "Mac OS",
    Windows = "Windows",
    WindowsPhone = "Windows Phone",
    Unknown = "Unknown"
}

// type TOsType = "Android" | "iOS" | "Mac OS" | "Windows" | "Windows Phone"

type T_GeoLocation = {
    ipAddress: string
    continentCode: string
    continentName: string
    countryCode: string
    countryName: string
    city: string
}

export class AgentData {
    protected _isInApp: boolean
    protected _appVersion?: string
    protected _svTime?: number
    protected _lcTime?: number
    protected _diffTime?: number
    protected _geoLocation?: T_GeoLocation

    osName: _E_OsType
    osVersion: string
    engineName: string
    engineVersion: string
    ip: string

    getWebType(): E_WebType {
        if (process.env.NEXT_PUBLIC_WEB_TYPE && process.env.NEXT_PUBLIC_WEB_TYPE in E_WebType) {
            return process.env.NEXT_PUBLIC_WEB_TYPE as E_WebType
        } else {
            return E_WebType.Unknown
        }
    }

    isWebApp() {
        return this.getWebType() == E_WebType.WebApp
    }

    isWebShare() {
        return this.getWebType() == E_WebType.WebShare
    }

    isAppDesktop() {
        return this.getWebType() == E_WebType.AppDesktop
            || global.ipcRenderer !== undefined
            || (global.platform !== undefined && global.platform == E_WebType.AppDesktop)
    }

    isWebAppInApp() {
        return this._isInApp;
    }

    isWebInMobile() {
        return isMobile
    }

    isWebAppInBrowser() {
        return this.isWebApp() && isBrowser
    }

    isWebAppInMobile() {
        return this.isWebApp() && isMobile
    }

    isWebShareInBrowser() {
        return this.isWebShare() && isBrowser
    }

    isWebShareInMobile() {
        return this.isWebShare() && isMobile
    }

    get appVersion() {
        return this._appVersion;
    }

    constructor(data: Record<string, any>) {
        this._isInApp = Normalize.initJsonBool(data, 'isApp') ?? false
        this._appVersion = Normalize.initJsonString(data, 'appVersion')
        this.osName = Normalize.initJsonString(data, 'osName') as _E_OsType
        this.osVersion = Normalize.initJsonString(data, 'osVersion') ?? ''
        this.engineName = Normalize.initJsonString(data, 'engineName') ?? ''
        this.engineVersion = Normalize.initJsonString(data, 'engineVersion') ?? ''
    }

    set svTime(timestamp: number) {
        this._svTime = timestamp
        this._lcTime = moment().unix()

        this._diffTime = moment.unix(this._lcTime).diff(moment.unix(timestamp), 'seconds')

        console.info(
            `Process Time\n`,
            `- Server: ${moment.unix(this._svTime).format('DD-MM-YYYY HH:mm:ss')} (${this._svTime})\n`,
            `- Local: ${moment.unix(this._lcTime).format('DD-MM-YYYY HH:mm:ss')} (${this._lcTime})\n`,
            `- Diff: ${this._diffTime}s\n`,
        )
    }

    set geoLocation(data: any) {
        this._geoLocation = data

        const log: string[] = [`Geo Location\n`]

        Object.entries(data).forEach(([key, value]) => {
            log.push(`- ${key}: ${value}\n`)
        })

        console.info.apply(this, log)
    }

    get geoLocation() {
        return this._geoLocation
    }

    get diffTime(): number {
        return this._diffTime ?? 0
    }

    getDiffTime(timestamp?: number, unitOfTime: unitOfTime.Diff = 'seconds'): number {
        timestamp = timestamp ?? this._svTime

        if (!timestamp || !this._lcTime) {
            return 0
        }

        return moment.unix(this._lcTime).diff(moment.unix(timestamp), unitOfTime)
    }
}
