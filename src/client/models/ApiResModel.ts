import {Normalize} from "../core/Normalize";

export class ApiResModel {
    success: boolean
    data?: Record<string, any>
    error?: any
    items?: any[]
    isEmpty: boolean
    information?: InfoEmptyModel
    meta?: PaginateMetaModel
    links?: PaginateLinksModel
    code: number

    constructor(data: Record<string, any>) {
        this.success = Normalize.initJsonBool(data, 'success') ?? false
        this.isEmpty = Normalize.initJsonBool(data, 'is_empty') ?? false
        this.data = Normalize.initJsonObject(data, 'data')
        this.error = Normalize.initJsonObject(data, 'error')
        this.items = Normalize.initJsonArray(data, 'items')
        this.information = Normalize.initJsonObject(data, 'information', (item: Record<string, any>) => new InfoEmptyModel(item))
        this.meta = Normalize.initJsonObject(data, '_meta', (item: Record<string, any>) => new PaginateMetaModel(item))
        this.links = Normalize.initJsonObject(data, '_links', (item: Record<string, any>) => new PaginateLinksModel(item))
        this.code = Normalize.initJsonNumber(data, 'code') ?? 0
    }
}

export class PaginateLinksModel {
    self: string
    next: string
    last: string

    constructor(data: Record<string, any>) {
        this.self = Normalize.initJsonString(data, 'self') ?? ''
        this.next = Normalize.initJsonString(data, 'next') ?? ''
        this.last = Normalize.initJsonString(data, 'last') ?? ''
    }
}

export class PaginateMetaModel {
    totalCount: number
    pageCount: number
    currentPage: number
    nextPage?: number
    perPage: number

    constructor(data: Record<string, any>) {
        this.totalCount = Normalize.initJsonNumber(data, 'total_count') ?? 0
        this.pageCount = Normalize.initJsonNumber(data, 'page_count') ?? 0
        this.currentPage = Normalize.initJsonNumber(data, 'current_page') ?? 0
        this.nextPage = Normalize.initJsonNumber(data, 'next_page')
        this.perPage = Normalize.initJsonNumber(data, 'per_page') ?? 0
    }

    fromObject = (object: any) => {
        Object.assign(this, object)
    }
}

export class InfoEmptyModel {
    name?: string
    description?: string

    constructor(data: Record<string, any>) {
        this.name = Normalize.initJsonString(data, 'name')
        this.description = Normalize.initJsonString(data, 'description')
    }
}
