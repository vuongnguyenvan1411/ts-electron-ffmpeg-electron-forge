import {AgentData} from "./AgentData";

export type ConfigModel = {
    lang?: number,
    agent?: AgentData
}

export const initialConfig: ConfigModel = {};
