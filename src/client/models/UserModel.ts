import {App} from "../const/App";
import moment from "moment";
import {Normalize} from "../core/Normalize";
import {Model} from "./Model";

export type TLoginVO = {
    username: string
    password: string
}

export type TMeInfoV0 = {
    name?: string
    email?: string
    telephone?: string
    address?: string
}

export type TMePasswordVO = {
    password: string
    current: string
}

export type TMeImageV0 = {
    image: File
    type: string
}

export class UserModel extends Model {
    userId: string
    token: string
    username: string
    name: string
    image: string
    background: string
    email: string
    telephone: string
    address: string
    createdAt: string

    accessToken?: AccessTokenModel

    createdAtFormatted = (format: string = App.FormatToMoment): string => moment(this.createdAt, App.FormatAtomFromMoment).format(format)

    constructor(data: any) {
        super()

        this.userId = Normalize.initJsonString(data, ['user_id', 'id']) ?? ''
        this.token = data.hasOwnProperty('token') ? data['token'] : null
        this.username = data.hasOwnProperty('username') ? data['username'] : null
        this.name = data.hasOwnProperty('name') ? data['name'] : null
        this.image = data.hasOwnProperty('image') ? data['image'] : null
        this.background = data.hasOwnProperty('background') ? data['background'] : null
        this.email = data.hasOwnProperty('email') ? data['email'] : null
        this.telephone = data.hasOwnProperty('telephone') ? data['telephone'] : null
        this.address = data.hasOwnProperty('address') ? data['address'] : null
        this.createdAt = data.hasOwnProperty('created_at') ? data['created_at'] : null

        this.accessToken = Normalize.initJsonObject(data, 'accessToken', v => new AccessTokenModel(v))
    }

    setToken = (value: string) => {
        this.token = value
    }

    setAccessToken = (value: any) => {
        this.accessToken = new AccessTokenModel(value)
    }
}

export class AccessTokenModel extends Model {
    token?: string
    abilities?: string[]
    expiresAt?: string
    updatedAt?: string
    createdAt?: string

    expiresAtFormatted = (format: string = App.FormatToMoment): string => moment(this.expiresAt, App.FormatISOFromMoment).format(format)
    updatedAtFormatted = (format: string = App.FormatToMoment): string => moment(this.updatedAt, App.FormatISOFromMoment).format(format)
    createdAtFormatted = (format: string = App.FormatToMoment): string => moment(this.createdAt, App.FormatISOFromMoment).format(format)

    constructor(data: Record<string, any>) {
        super()

        this.token = Normalize.initJsonString(data, 'token')
        this.abilities = Normalize.initJsonArray(data, 'abilities')
        this.expiresAt = Normalize.initJsonString(data, 'expiresAt')
        this.updatedAt = Normalize.initJsonString(data, 'updatedAt')
        this.createdAt = Normalize.initJsonString(data, 'createdAt')
    }
}
