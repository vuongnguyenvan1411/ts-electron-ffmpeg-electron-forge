import {Normalize} from "../../core/Normalize";
import {App} from "../../const/App";
import moment from "moment";

export type TFeedbackV0 = {
    message: string
}

export class FeedbackModel {
    feedbackId?: number
    message?: string
    createdAt?: string

    createdAtFormatted = (format: string = App.FormatISOFromMoment): string => this.createdAt ? moment(this.createdAt, App.FormatAtomFromMoment).format(format) : ''

    constructor(data: Record<string, any>) {
        this.feedbackId = Normalize.initJsonNumber(data, 'feedback_id')
        this.message = Normalize.initJsonString(data, 'message')
        this.createdAt = Normalize.initJsonString(data, 'created_at')
    }
}
