import {Normalize} from "../../core/Normalize";

type TBrochure = {
    name: string
    file: string
    description?: string,
    image?: string,
}

export class AboutModel {
    description?: string
    brochure?: TBrochure[]

    constructor(data: Record<string, any>) {
        this.description = Normalize.initJsonString(data, 'description')
        this.brochure = Normalize.initJsonArray(data, 'brochure')
    }
}
