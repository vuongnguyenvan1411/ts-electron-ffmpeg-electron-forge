import {Normalize} from "../../core/Normalize";
import {App} from "../../const/App";
import moment from "moment/moment";

export type TPlatformFileEP = {
    name: string,
    type?: 'vr' | '2d' | '3d'
}

export class PlatformFileModel {
    id: string
    name: string
    type: string
    files: {
        id: string
        name: string
        size: number
        extension: string
        state: number
        createdAt: string
    }[]
    size: number
    createdAt: string

    dateCreatedAtFormatted = (format: string = App.FormatToMoment): string | undefined => {
        if (this.createdAt) {
            return moment(this.createdAt, App.FormatISOFromMoment).format(format);
        } else {
            return undefined;
        }
    }

    dateCreatedAtUnix = (): number | undefined => {
        if (this.createdAt) {
            return moment(this.createdAt, App.FormatISOFromMoment).unix()
        } else {
            return undefined;
        }
    }

    constructor(data: Record<string, any>) {
        this.id = Normalize.initJsonString(data, 'id') ?? '';
        this.name = Normalize.initJsonString(data, 'name') ?? '';
        this.type = Normalize.initJsonString(data, 'type') ?? '';
        this.files = Normalize.initJsonArray(data, 'files', (v1) => v1.map(v2 => ({
            id: Normalize.initJsonString(v2, 'id') ?? '',
            name: Normalize.initJsonString(v2, 'name') ?? '',
            size: Normalize.initJsonNumber(v2, 'size') ?? 0,
            extension: Normalize.initJsonString(v2, 'extension') ?? '',
            state: Normalize.initJsonNumber(v2, 'state') ?? 0,
            createdAt: Normalize.initJsonString(v2, 'created_at') ?? ''
        }))) ?? [];
        this.size = Normalize.initJsonNumber(data, 'type') ?? 0;
        this.createdAt = Normalize.initJsonString(data, 'created_at') ?? '';
    }
}
