import {UserModel} from "../../UserModel";
import moment from "moment";
import {App} from "../../../const/App";
import {Normalize} from "../../../core/Normalize";

export type TSpaceFilterVO = {
    page?: number
    limit?: number
    sort?: string
    order?: string
    filter?: {
        q?: string,
    }
}

export type TM2d = {
    m2dId?: number
    name?: string
    image?: string
    link?: string
}

export type TM3d = {
    m3dId?: number
    name?: string
    image?: string
    link?: string,
    data?: {
        pcs?: TM3dPC[]
        tms?: TM3dTM[]
    }
}

export type TM3dPC = {
    id: number,
    name: string,
    v: string
}

export type TM3dTM = {
    id: number,
    name: string,
    v: string
}

export type TVr360 = {
    vr360Id?: number
    name?: string
    image?: string
    link?: string
}

export class SpaceModel {
    spaceId?: number;
    name?: string;
    image?: string;
    user?: UserModel;
    address?: string;
    link?: string;
    createdAt?: string;
    total?: {
        m2d?: number,
        m3d?: number,
        vr360?: number
    }
    m2d?: TM2d[];
    m3d?: TM3d[];
    vr360?: TVr360[];

    createdAtFormatted = (format: string = App.FormatToMoment): string => this.createdAt ? moment(this.createdAt, App.FormatAtomFromMoment).format(format) : '';

    constructor(data: Record<string, any>) {
        this.spaceId = Normalize.initJsonNumber(data, 'space_id');
        this.name = Normalize.initJsonString(data, 'name') ?? '';
        this.image = Normalize.initJsonString(data, 'image');
        this.user = Normalize.initJsonObject(data, 'user', (e: any): UserModel => new UserModel(e));
        this.address = Normalize.initJsonString(data, 'address') ?? '';
        this.link = Normalize.initJsonString(data, 'link') ?? '';
        this.createdAt = Normalize.initJsonString(data, 'created_at') ?? '';
        this.total = Normalize.initJsonObject(data, 'total', (item: Record<string, number>) => ({
            m2d: Normalize.initJsonNumber(item, 'm2d'),
            m3d: Normalize.initJsonNumber(item, 'm3d'),
            vr360: Normalize.initJsonNumber(item, 'vr360')
        }))
        this.m2d = Normalize.initJsonArray(data, 'm2d', (item: any): TM2d[] => item.map((e: any) => ({
            m2dId: Normalize.initJsonNumber(e, 'm2d_id'),
            name: Normalize.initJsonString(e, 'name'),
            image: Normalize.initJsonString(e, 'image'),
            link: Normalize.initJsonString(e, 'link'),
        })));
        this.m3d = Normalize.initJsonArray(data, 'm3d', (item: any): TM3d[] => item.map((e: any) => ({
            m3dId: Normalize.initJsonNumber(e, 'm3d_id'),
            name: Normalize.initJsonString(e, 'name'),
            image: Normalize.initJsonString(e, 'image'),
            link: Normalize.initJsonString(e, 'link'),
            data: Normalize.initJsonObject(e, 'data', (e2) => ({
                pcs: Normalize.initJsonArray(e2, ['pc', 'pcs'], (e3: any[]) => e3.map((e4: any) => ({
                    id: Normalize.initJsonNumber(e4, 'id'),
                    name: Normalize.initJsonString(e4, 'name'),
                    v: Normalize.initJsonString(e4, 'v')
                }))),
                tms: Normalize.initJsonArray(e2, ['tm', 'tms'], (e3: any[]) => e3.map((e4: any) => ({
                    id: Normalize.initJsonNumber(e4, 'id'),
                    name: Normalize.initJsonString(e4, 'name'),
                    v: Normalize.initJsonString(e4, 'v')
                }))),
            }))
        })));
        this.vr360 = Normalize.initJsonArray(data, 'vr360', (item: any): TVr360[] => item.map((e: any) => ({
            vr360Id: Normalize.initJsonNumber(e, 'vr360_id'),
            name: Normalize.initJsonString(e, 'name'),
            image: Normalize.initJsonString(e, 'image'),
            link: Normalize.initJsonString(e, 'link'),
        })));
    }
}

export class SpaceSettingModel {
    isShare?: number;
    settingShare?: {
        share2d?: number,
        share3d?: number,
        shareVr360?: number
    };
    links?: {
        share?: string,
        qrcode?: string,
    };

    constructor(data: Record<string, any>) {
        this.isShare = Normalize.initJsonNumber(data, 'is_share');
        this.settingShare = Normalize.initJsonObject(data, 'setting_share', (item: any) => ({
            share2d: Normalize.initJsonNumber(item, 'share_2d'),
            share3d: Normalize.initJsonNumber(item, 'share_3d'),
            shareVr360: Normalize.initJsonNumber(item, 'share_vr360'),
        }));
        this.links = Normalize.initJsonObject(data, 'links', (item: any) => ({
            share: Normalize.initJsonString(item, 'share'),
            qrcode: Normalize.initJsonString(item, 'qr_code'),
        }));
    }
}
