import {Normalize} from "../../../core/Normalize";

export type TPointCloud = {
    id?: string;
    type?: string;
    name?: string;
    url?: string;
    v?: string;
}

type TTextureMesh = {
    id?: number;
    name?: string;
    v?: string;
}

export class TSceneObject {
    url: string
    altitude: number
    lat: number
    lng: number
    name: string
    scale: number
    orientation: number[]

    constructor(data: Record<string, any>) {
        this.altitude = Normalize.initJsonNumber(data, 'altitude') ?? 0
        this.lat = Normalize.initJsonNumber(data, 'lat') ?? 0
        this.lng = Normalize.initJsonNumber(data, 'lng') ?? 0
        this.scale = Normalize.initJsonNumber(data, 'scale') ?? 0
        this.name = Normalize.initJsonString(data, 'name') ?? ''
        this.url = Normalize.initJsonString(data, 'url') ?? ''
        this.orientation = Normalize.initJsonArray(data, 'orientation') ?? [0, 0, 90]
    }
}

export class TTMesh {
    config?: {
        obj?: TSceneObject[]
    }
    id?: string
    name?: string

    constructor(data: Record<string, any>) {
        this.config = Normalize.initJsonObject(data, 'config', (value) => ({
            obj: Normalize.initJsonArray(value, 'obj', (_: any[]): TSceneObject[] => _.map((e: any) => new TSceneObject(e))) ?? []
        }))
        this.id = Normalize.initJsonString(data, 'id')
        this.name = Normalize.initJsonString(data, 'name')
    }
}

export class Map3DModel {
    path?: string;
    pc?: TPointCloud[];
    tm?: TTextureMesh[];
    info?: {
        tm?: TTMesh
        path?: string
    }

    constructor(data: Record<string, any>) {
        this.path = Normalize.initJsonString(data, 'path');
        this.pc = Normalize.initJsonArray(data, 'pc', (r: any[]): TPointCloud[] => r.map((e: any): TPointCloud => ({
            id: Normalize.initJsonString(e, 'id'),
            type: Normalize.initJsonString(e, 'type'),
            name: Normalize.initJsonString(e, 'name'),
            url: Normalize.initJsonString(e, 'url'),
            v: Normalize.initJsonString(e, 'v'),
        })));
        this.tm = Normalize.initJsonArray(data, 'tm', (r: any[]): TTextureMesh[] => r.map((e: any): TTextureMesh => ({
            id: Normalize.initJsonNumber(e, 'id'),
            name: Normalize.initJsonString(e, 'name'),
            v: Normalize.initJsonString(e, 'v'),
        })));
        // expiryDate: Normalize.initJsonObject(values, 'expiryDate', (value: any): TExpiryDate => ({
        //     start: Normalize.initJsonString(value, 'start'),
        //     end: Normalize.initJsonString(value, 'end'),
        // })) ?? {},
        this.info = Normalize.initJsonObject(data, 'info', (value: Record<string, any>) => ({
            tm: Normalize.initJsonObject(value, 'tm', (_: Record<string, any>): TTMesh => new TTMesh(_)),
        }))
    }
}
