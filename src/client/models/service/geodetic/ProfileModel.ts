import {GeoJSONFeatureCollection} from "ol/format/GeoJSON";
import {App} from "../../../const/App";
import moment from "moment";
import {Normalize} from "../../../core/Normalize";

export type TProfileFilterVO = {
    page: number
    limit: number
    sort?: string
    order?: string
    filter?: Record<string, any>
}
export type TConfigVector = {
    id: string,
    title: string,
    typeDraw: string,
    type: string,
    properties?: any,
}

export class TProfile {
    vector?: TConfigVector;
    features?: GeoJSONFeatureCollection;

    constructor(data: Record<string, any>) {
        this.vector = Normalize.initJsonObject(data, 'vector', (_: any): TConfigVector => ({
            id: Normalize.initJsonString(_, 'id') ?? "",
            title: Normalize.initJsonString(_, 'title') ?? "",
            typeDraw: Normalize.initJsonString(_, 'typeDraw') ?? "",
            properties: Normalize.initJsonObject(_, 'properties'),
            type: Normalize.initJsonString(_, 'type') ?? "",
        }));
        this.features = Normalize.initJsonObject(data, 'features', (_: any): GeoJSONFeatureCollection => ({
            type: 'FeatureCollection',
            features: Normalize.initJsonArray(_, 'features') ?? [],
        }));
    }
}

export class TConfig {
    profile?: TProfile[];
    measures?: GeoJSONFeatureCollection;

    constructor(data: Record<string, any>) {
        this.profile = Normalize.initJsonArray(data, 'profile', (item: any): TProfile[] => item.map((e: Record<string, any>) => new TProfile(e)));
        this.measures = Normalize.initJsonObject(data, 'measures', (_: any): GeoJSONFeatureCollection => ({
            type: 'FeatureCollection',
            features: Normalize.initJsonArray(_, 'features') ?? [],
        }));
    }

}

export type TProfileSaveV0 = {
    name: string
    config: TConfig
    createdAt?: string
    sid?: string
}

export class ProfileModel {
    id: string;
    name?: string;
    config?: TConfig;
    createdAt?: string;

    dateCreatedAtFormatted = (format: string = App.FormatToMoment): string | undefined => {
        if (this.createdAt) {
            return moment(this.createdAt, App.FormatAtomFromMoment).format(format);
        } else {
            return undefined;
        }
    }

    constructor(data: Record<string, any>) {
        this.name = Normalize.initJsonString(data, 'name');
        this.id = Normalize.initJsonString(data, 'id') ?? '';
        this.createdAt = Normalize.initJsonString(data, 'createdAt');
        this.config = Normalize.initJsonObject(data, 'config', (item: any) => new TConfig(item))
        // this.config = Normalize.initJsonArray(data, 'config', (item: any) => item.map((e: any): TProfile => {
        //     return {
        //         vector: Normalize.initJsonObject(e, 'vector', (_: any): TConfigVector => ({
        //             id: Normalize.initJsonString(_, 'id') ?? "",
        //             title: Normalize.initJsonString(_, 'title') ?? "",
        //             typeDraw: Normalize.initJsonString(_, 'typeDraw') ?? "",
        //             properties: Normalize.initJsonObject(_, 'properties'),
        //             type: Normalize.initJsonString(_, 'type') ?? "",
        //         })),
        //         features: Normalize.initJsonObject(e, 'features', (_: any): GeoJSONFeatureCollection => ({
        //             type: 'FeatureCollection',
        //             features: Normalize.initJsonArray(_, 'features') ?? [],
        //         })),
        //     }
        // })) ?? [];
    }
}


export class FeatureWMSModel {
    feature?: {
        fid?: number,
        text?: string,
    }

    constructor(data: Record<string, any>) {
        this.feature = Normalize.initJsonObject(data, 'type', (item: any) => ({
            fid: Normalize.initJsonNumber(item, 'fid'),
            text: Normalize.initJsonString(item, 'text'),
        }));

    }
}
