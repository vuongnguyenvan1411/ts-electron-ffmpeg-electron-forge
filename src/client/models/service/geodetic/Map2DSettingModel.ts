import {App} from "../../../const/App";
import moment from "moment";
import {Normalize} from "../../../core/Normalize";

type TExpDate = {
    start?: string
    end?: string
}

export class Map2DShareModel {
    shareId: string;
    name: string;
    expiryDate?: TExpDate;
    description?: string;
    password?: string;
    status: boolean;
    sInfo: boolean;
    // map2D: IMap2DShare[];
    tiles: string[];
    contours: string[];
    bArea: number[];
    link: string;
    createdAt: string;

    dateStartFormatted = (format: string = App.FormatToMoment): string | undefined => {
        if (this.expiryDate?.start) {
            return moment(this.expiryDate.start, App.FormatISOFromMoment).format(format);
        } else {
            return undefined;
        }
    }

    dateEndFormatted = (format: string = App.FormatToMoment): string | undefined => {
        if (this.expiryDate?.end) {
            return moment(this.expiryDate.end, App.FormatISOFromMoment).format(format);
        } else {
            return undefined;
        }
    }

    constructor(data: Record<string, any>) {
        this.shareId = Normalize.initJsonString(data, 'shareId') ?? '';
        this.name = Normalize.initJsonString(data, 'name') ?? '';
        this.description = Normalize.initJsonString(data, 'description');
        this.password = Normalize.initJsonString(data, 'password');
        this.expiryDate = Normalize.initJsonObject(data, 'expiryDate', (item: any): TExpDate => ({
            start: Normalize.initJsonString(item, 'start'),
            end: Normalize.initJsonString(item, 'end')
        }))
        this.status = Normalize.initJsonBool(data, 'status') ?? false;
        this.sInfo = Normalize.initJsonBool(data, 'sInfo') ?? false;
        this.link = Normalize.initJsonString(data, 'link') ?? '';
        this.createdAt = Normalize.initJsonString(data, 'createdAt') ?? '';
        this.bArea = Normalize.initJsonArray(data, 'bArea') ?? [];
        this.contours = Normalize.initJsonArray(data, 'contours') ?? [];
        this.tiles = Normalize.initJsonArray(data, 'tiles') ?? [];
    }
}
