import {Normalize} from "../../../core/Normalize";
import {v4 as uuid} from "uuid";
import {ProfileModel} from "./ProfileModel";

export interface IMap2dInfo {
    center?: Map2DCoordModel,
    zoom?: number,
    markers?: Map2DMarkerModel[];
    tiles?: Map2DTileModel[];
    contours?: Map2DContourModel[];
    profile?: ProfileModel,
    ok?: string;
}

export class Map2DModel {
    name?: string;
    info?: IMap2dInfo;

    constructor(data: Record<string, any>) {
        this.name = Normalize.initJsonString(data, 'name');
        this.info = Normalize.initJsonObject(data, 'info', (item: Record<string, any>): IMap2dInfo => ({
            center: Normalize.initJsonObject(item, 'center', (r: Record<string, any>): Map2DCoordModel => new Map2DCoordModel(r)),
            zoom: Normalize.initJsonNumber(item, 'zoom'),
            markers: Normalize.initJsonArray(item, 'markers', (r: Record<string, any>[]): Map2DMarkerModel[] => r.map((e: Record<string, any>) => new Map2DMarkerModel(e))),
            tiles: Normalize.initJsonArray(item, 'tiles', (r: Record<string, any>[]): Map2DTileModel[] => r.map((e: Record<string, any>) => new Map2DTileModel(e))),
            contours: Normalize.initJsonArray(item, 'contours', (r: Record<string, any>[]): Map2DContourModel[] => r.map((e: Record<string, any>) => new Map2DContourModel(e))),
            profile: Normalize.initJsonObject(item, 'profile', (r: Record<string, any>): ProfileModel => new ProfileModel(r)),
        }));
    }
}

export interface IMarkerData {
    coord?: Map2DCoordModel,
    link?: {
        icon?: string;
        target?: string;
        href?: string;
        vr360?: {
            id?: number,
            name?: string,
            scene?: number,
            url?: string,
            createdAt?: string,
        };
        scene?: number;
    },
    title?: string,
    description?: string,
}

export class Map2DMarkerModel {
    id: string;
    type?: string;
    data?: { coord: Map2DCoordModel | undefined; link: { vr360: { createdAt: string | undefined; name: string | undefined; id: string | undefined; url: string | undefined; scene: number | undefined } | undefined; icon: string | undefined; href: string | undefined; target: string | undefined; scene: number | undefined } | undefined; description: string | undefined; title: string | undefined } | undefined;
    status?: boolean;

    constructor(data: Record<string, any>) {
        this.id = Normalize.initJsonString(data, 'id') ?? uuid();
        this.type = Normalize.initJsonString(data, 'type');
        this.data = Normalize.initJsonObject(data, 'data', (item: Record<string, any>): { coord: Map2DCoordModel | undefined; link: { vr360: { createdAt: string | undefined; name: string | undefined; id: string | undefined; url: string | undefined; scene: number | undefined } | undefined; icon: string | undefined; href: string | undefined; target: string | undefined; scene: number | undefined } | undefined; description: string | undefined; title: string | undefined } => ({
            coord: Normalize.initJsonObject(item, 'coord', (r: Record<string, any>): Map2DCoordModel => new Map2DCoordModel(r)),
            link: Normalize.initJsonObject(item, 'link', (r: Record<string, any>): { vr360: { createdAt: string | undefined; name: string | undefined; id: string | undefined; url: string | undefined; scene: number | undefined } | undefined; icon: string | undefined; href: string | undefined; target: string | undefined; scene: number | undefined } => ({
                icon: Normalize.initJsonString(r, 'icon'),
                target: Normalize.initJsonString(r, 'target'),
                href: Normalize.initJsonString(r, 'href'),
                vr360: Normalize.initJsonObject(r, 'vr360', (e: Record<string, any>) => ({
                    id: Normalize.initJsonString(e, 'id'),
                    name: Normalize.initJsonString(e, 'name'),
                    scene: Normalize.initJsonNumber(e, 'scene'),
                    url: Normalize.initJsonString(e, 'url'),
                    createdAt: Normalize.initJsonString(e, 'created_at'),
                })),
                scene: Normalize.initJsonNumber(r, 'scene')
            })),
            title: Normalize.initJsonString(item, 'title'),
            description: Normalize.initJsonString(item, 'description'),
        }));
        this.status = Normalize.initJsonBool(data, 'status');
    }
}

export interface ITileSet {
    order?: number,
    unitsPerPixel?: number,
}

export class Map2DTileModel {
    id: string;
    name?: string;
    tile?: {
        id?: number;
        name?: string;
    };
    link?: string;
    boundingBox?: Map2DBoundingBoxModel
    tileFormat?: {
        width?: number;
        height?: number;
        extension?: string;
    };
    tileSets?: {
        profile?: string;
        tileSet?: ITileSet[],
    };

    constructor(data: Record<string, any>) {
        this.id = Normalize.initJsonString(data, 'id') ?? uuid();
        this.name = Normalize.initJsonString(data, 'name');
        this.tile = Normalize.initJsonObject(data, 'tile', (item: Record<string, any>) => ({
            id: Normalize.initJsonNumber(item, 'id'),
            name: Normalize.initJsonString(item, 'name')
        }));
        this.link = Normalize.initJsonString(data, 'link');
        this.boundingBox = Normalize.initJsonObject(data, 'boundingBox', (r: Record<string, any>): Map2DBoundingBoxModel => new Map2DBoundingBoxModel(r));
        this.tileFormat = Normalize.initJsonObject(data, 'tileFormat', (r: Record<string, any>) => ({
            width: Normalize.initJsonNumber(r, 'width'),
            height: Normalize.initJsonNumber(r, 'height'),
            extension: Normalize.initJsonString(r, 'extension'),
        }));
        this.tileSets = Normalize.initJsonObject(data, 'tileSets', (r: Record<string, any>) => ({
            profile: Normalize.initJsonString(r, 'profile'),
            tileSet: Normalize.initJsonArray(r, 'tileSet', (e: Record<string, any>[]): ITileSet[] => e.map((e: Record<string, any>) => ({
                order: Normalize.initJsonNumber(e, 'order'),
                unitsPerPixel: Normalize.initJsonNumber(e, 'unitsPerPixel'),
            })))
        }));
    }
}

export class Map2DContourModel {
    id: string;
    name?: string;
    url?: string;
    status?: boolean;
    boundingBox?: Map2DBoundingBoxModel;
    layer?: string;
    ws?: string;
    style?: string;

    constructor(data: Record<string, any>) {
        this.id = Normalize.initJsonString(data, 'id') ?? uuid();
        this.name = Normalize.initJsonString(data, 'name');
        this.url = Normalize.initJsonString(data, 'url');
        this.status = Normalize.initJsonBool(data, 'status');
        this.boundingBox = Normalize.initJsonObject(data, 'boundingBox', (r: Record<string, any>): Map2DBoundingBoxModel => new Map2DBoundingBoxModel(r));
        this.layer = Normalize.initJsonString(data, 'layer');
        this.ws = Normalize.initJsonString(data, 'ws');
        this.style = Normalize.initJsonString(data, 'style');
    }
}

export class Map2DBoundingBoxModel {
    miny?: number;
    minx?: number;
    maxy?: number;
    maxx?: number;

    constructor(data: Record<string, any>) {
        this.miny = Normalize.initJsonNumber(data, 'miny');
        this.minx = Normalize.initJsonNumber(data, 'minx');
        this.maxy = Normalize.initJsonNumber(data, 'maxy');
        this.maxx = Normalize.initJsonNumber(data, 'maxx');
    }
}

export class Map2DCoordModel {
    lat?: number;
    lon?: number;

    constructor(data: Record<string, any>) {
        this.lat = Normalize.initJsonNumber(data, 'lat');
        this.lon = Normalize.initJsonNumber(data, 'lon');
    }
}

export class MapInfoLocationModel {
    displayName?: string;
    address?: {
        road?: string;
        city?: string;
        county?: string;
        village?: string;
        state?: string;
        country?: string;
    }

    constructor(data: Record<string, any>) {
        this.displayName = Normalize.initJsonString(data, 'display_name');
        this.address = Normalize.initJsonObject(data, 'address', (r: Record<string, any>) => ({
            road: Normalize.initJsonString(r, 'road'),
            city: Normalize.initJsonString(r, 'city'),
            county: Normalize.initJsonString(r, 'county'),
            state: Normalize.initJsonString(r, 'state'),
            country: Normalize.initJsonString(r, 'country'),
        }));
    }
}
