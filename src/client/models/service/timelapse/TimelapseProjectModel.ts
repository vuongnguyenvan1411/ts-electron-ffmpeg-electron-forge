import {UserModel} from "../../UserModel";
import {TimelapseMachineModel} from "./TimelapseMachineModel";
import moment from "moment";
import {App} from "../../../const/App";
import {Normalize} from "../../../core/Normalize";

export interface TTimelapseProjectFilterVO {
    page?: number;
    limit?: number;
    sort?: string;
    order?: string;
    filter?: {
        q?: string,
        date_start?: string,
        date_end?: string,
        active?: string,
    }
}

export class TimelapseProjectModel {
    projectId?: string
    name?: string
    isSensor?: boolean
    machine?: TimelapseMachineModel[]
    active: boolean
    user?: UserModel
    address?: string
    total: number
    protected createdAt?: string

    createdAtFormatted = (format: string = App.FormatToMoment): string | undefined => this.createdAt !== undefined
        ? moment(this.createdAt, App.FormatAtomFromMoment).format(format)
        : undefined;

    constructor(data: Record<string, any>) {
        this.projectId = Normalize.initJsonString(data, 'project_id')
        this.name = Normalize.initJsonString(data, 'name')
        this.isSensor = Normalize.initJsonBool(data, 'is_sensor')
        this.machine = Normalize.initJsonArray(data, 'machine', v1 => v1.map(v2 => new TimelapseMachineModel(v2)))
        this.active = Normalize.initJsonBool(data, 'active') ?? false
        this.user = Normalize.initJsonObject(data, 'user', v => new UserModel(v))
        this.total = Normalize.initJsonNumber(data, 'total') ?? 0
        this.address = Normalize.initJsonString(data, 'address')
        this.createdAt = Normalize.initJsonString(data, 'created_at')
    }
}
