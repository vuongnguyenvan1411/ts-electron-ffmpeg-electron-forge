import {App} from "../../../const/App";
import moment from "moment";
import {Normalize} from "../../../core/Normalize";

export interface TimelapseVideoFilterVO {
    fields?: string,
    filter?: {
        period?: string,
        date_start?: string,
        date_end?: string,
        time_start?: string,
        time_end?: string
    }
}

interface iTVFImg {
    name: string;
    order: number;
    width?: number;
    height?: number;
    dateShot: string;
}

export class TimelapseVideoFilterModel {
    machineId?: number;
    name?: string;
    filter: { filter_date_start: string; filter_date_end: string; filter_time_start: string; filter_time_end: string; isRs: number } | undefined;
    first?: iTVFImg;
    last?: iTVFImg;
    total?: number;
    render?: string;

    firstDateShotFormatted = (format: string = App.FormatToMoment): string => {
        if (this.first?.dateShot) {
            return moment(this.first?.dateShot, App.FormatISOFromMoment).format(format);
        } else {
            return '';
        }
    }

    lastDateShotFormatted = (format: string = App.FormatToMoment): string => {
        if (this.last?.dateShot) {
            return moment(this.last?.dateShot, App.FormatISOFromMoment).format(format);
        } else {
            return '';
        }
    }

    constructor(data: Record<string, any>) {
        this.machineId = Normalize.initJsonNumber(data, 'machine_id');
        this.name = Normalize.initJsonString(data, 'name') ?? '';
        this.filter = Normalize.initJsonObject(data, 'filter');

        this.first = Normalize.initJsonObject(data, 'first', (item: any): iTVFImg => ({
            name: Normalize.initJsonString(item, 'name') ?? '',
            order: Normalize.initJsonNumber(item, 'order') ?? 0,
            height: Normalize.initJsonNumber(item, 'height'),
            width: Normalize.initJsonNumber(item, 'width'),
            dateShot: Normalize.initJsonString(item, 'date_shot') ?? '',
        }));

        this.last = Normalize.initJsonObject(data, 'last', (item: any): iTVFImg => ({
            name: Normalize.initJsonString(item, 'name') ?? '',
            order: Normalize.initJsonNumber(item, 'order') ?? 0,
            height: Normalize.initJsonNumber(item, 'height'),
            width: Normalize.initJsonNumber(item, 'width'),
            dateShot: Normalize.initJsonString(item, 'date_shot') ?? '',
        }));

        this.total = Normalize.initJsonNumber(data, 'total');
        this.render = Normalize.initJsonString(data, 'render');
    }
}
