import {TSensorModel} from "./TimelapseImageModel";
import {Normalize} from "../../../core/Normalize";

interface ILink {
    share?: string,
    qrCode?: string,
}

export interface IMachineShare {
    machineId: number,
    status: boolean,
    image: boolean,
    video: boolean,
    chart: boolean,
    sensor: number[],
}

// export class TimelapseShareModel {
//     shareId: string;
//     name: string;
//     description?: string;
//     password?: string;
//     dateStart?: string;
//     dateEnd?: string;
//     status: boolean;
//     sInfo: boolean;
//     machines: IMachineShare[];
//     link: string;
//     createdAt: string;
//
//     dateStartFormatted = (format: string = App.FormatToMoment): string | undefined => {
//         if (this.dateStart) {
//             return moment(this.dateStart, App.FormatFromMoment).format(format);
//         } else {
//             return undefined;
//         }
//     };
//
//     dateEndFormatted = (format: string = App.FormatToMoment): string | undefined => {
//         if (this.dateEnd) {
//             return moment(this.dateEnd, App.FormatFromMoment).format(format);
//         } else {
//             return undefined;
//         }
//     };
//
//     public constructor(data: any) {
//         this.shareId = Normalize.initJsonString(data, 'shareId') ?? '';
//         this.name = Normalize.initJsonString(data, 'name') ?? '';
//         this.description = Normalize.initJsonString(data, 'description');
//         this.password = Normalize.initJsonString(data, 'password');
//         this.dateStart = Normalize.initJsonString(data, 'dateStart');
//         this.dateEnd = Normalize.initJsonString(data, 'dateEnd');
//         this.status = Normalize.initJsonBool(data, 'status') ?? false;
//         this.sInfo = Normalize.initJsonBool(data, 'sInfo') ?? false;
//         this.machines = Normalize.initJsonArray(data, 'machines', (arr: any) => arr.map((e: any): IMachineShare => {
//             return {
//                 machineId: Normalize.initJsonNumber(e, 'machineId') ?? 0,
//                 status: Normalize.initJsonBool(e, 'status') ?? false,
//                 image: Normalize.initJsonBool(e, 'image') ?? false,
//                 video: Normalize.initJsonBool(e, 'video') ?? false,
//                 chart: Normalize.initJsonBool(e, 'chart') ?? false,
//                 sensor: Normalize.initJsonArray(e, 'sensor') ?? [],
//             }
//         })) ?? [];
//         this.link = Normalize.initJsonString(data, 'link') ?? '';
//         this.createdAt = Normalize.initJsonString(data, 'createdAt') ?? '';
//     }
//
// }

export class TimelapseSettingModel {
    sensor?: TSensorModel[];
    setting?: SettingModel;
    links?: ILink;

    public constructor(data: any) {
        this.sensor = Normalize.initJsonArray(data, 'sensor', (arr: any) => arr.map((e: any) => new TSensorModel(e)));
        this.setting = Normalize.initJsonObject(data, 'setting', (e: any) => new SettingModel(e));
        this.links = Normalize.initJsonObject(data, 'links', (item: any): ILink => ({
            share: Normalize.initJsonString(item, 'share'),
            qrCode: Normalize.initJsonString(item, 'qr_code')
        }));
    }
}

export class SettingModel {
    isShare: number;
    settingShare?: SettingShareModel;

    public constructor(data: any) {
        this.isShare = Normalize.initJsonNumber(data, 'is_share') ?? 0;
        this.settingShare = Normalize.initJsonObject(data, 'setting_share', (e: any) => new SettingShareModel(e));
    }
}


export class SettingShareModel {
    shareInfo: number;
    shareVideo: number;
    shareImage: number;
    shareChart: number;
    shareSensor: string[];

    public constructor(data: any) {
        this.shareInfo = Normalize.initJsonNumber(data, 'share_info') ?? 0;
        this.shareVideo = Normalize.initJsonNumber(data, 'share_video') ?? 0;
        this.shareImage = Normalize.initJsonNumber(data, 'share_image') ?? 0;
        this.shareChart = Normalize.initJsonNumber(data, 'share_chart') ?? 0;
        this.shareSensor = Normalize.initJsonArray(data, 'share_sensor') ?? [];
    }
}

export interface TimelapseSettingVO {
    is_share: number;
    setting_share: {
        share_info: number,
        share_video: number,
        share_image: number,
        share_chart: number,
        share_sensor: string[],
    }
}
