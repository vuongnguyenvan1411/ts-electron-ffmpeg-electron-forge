import {TSensorModel} from "./TimelapseImageModel";
import {Normalize} from "../../../core/Normalize";

export interface TimelapseAnalyticsFilterVO {
    filter?: {
        date_start?: string,
        date_end?: string,
        time_start?: string,
        time_end?: string,
    }
}

type MapDataChart = Record<string, SensorChartModel>;

export class TimelapseSensorModel {
    sensor?: TSensorModel[]
    data?: MapDataChart
    range?: string

    public constructor(data: Record<string, any>) {
        this.sensor = Normalize.initJsonArray(data, "sensor", v1 => v1.map(v2 => new TSensorModel(v2)))
        this.data = Normalize.initJsonObject(data, "data")
        this.range = Normalize.initJsonString(data, "range")
    }
}

interface IChart {
    list?: any[],
    xaxis?: any[],
    data?: number[]
}

export class SensorChartModel {
    avg?: number
    label?: string
    chart?: IChart
    range?: string
    unit?: string
    unit2?: string

    public constructor(data: any) {
        this.avg = Normalize.initJsonNumber(data, 'avg')
        this.label = Normalize.initJsonString(data, 'label')
        this.chart = Normalize.initJsonObject(data, 'chart', (v): IChart => ({
            list: Normalize.initJsonArray(v, 'list'),
            xaxis: Normalize.initJsonArray(v, 'xaxis'),
            data: Normalize.initJsonArray(v, 'number')
        }))
        this.range = Normalize.initJsonString(data, 'range')
        this.unit = Normalize.initJsonString(data, 'unit')
        this.unit2 = Normalize.initJsonString(data, 'unit_2')
    }
}
