import moment, {Moment} from "moment";
import {App} from "../../../const/App";
import {Normalize} from "../../../core/Normalize";

export interface TimelapseVideoFilterVO {
    page?: number;
    limit?: number;
    sort?: string;
    order?: string;
    filter?: {
        q?: string,
    };
}

export class TimelapseVideoModel {
    name?: string
    link?: string
    info?: TimelapseVideoInfoModel
    image?: any[]
    hls?: {
        id: number | undefined
        hid: string | undefined
        file: string | undefined
        name: string | undefined
    } | undefined
    protected createdAt?: string

    createdAtFormatted = (format: string = App.FormatToMoment): string | undefined => this.createdAt !== undefined ? moment(this.createdAt, App.FormatAtomFromMoment).format(format) : undefined;

    public constructor(data: Record<string, any>) {
        this.name = Normalize.initJsonString(data, 'name')
        this.link = Normalize.initJsonString(data, 'link')
        this.info = Normalize.initJsonObject(data, 'info', v => new TimelapseVideoInfoModel(v))
        this.image = Normalize.initJsonArray(data, 'image')
        this.hls = Normalize.initJsonObject(data, 'hls', (e: any) => ({
            id: Normalize.initJsonNumber(e, 'id'),
            hid: Normalize.initJsonString(e, 'hid'),
            name: Normalize.initJsonString(e, 'name'),
            file: Normalize.initJsonString(e, 'file')
        }))
        this.createdAt = Normalize.initJsonString(data, 'created_at')
    }
}

export class TimelapseVideoInfoModel {
    bitRate?: number
    codecName?: string
    dateStart?: string
    dateEnd?: string
    timeStart?: string
    timeEnd?: string
    dateRender?: string
    duration?: number
    fps?: string
    frameRate?: string
    totalFrames?: number
    resolution?: string
    thumb?: string[]
    width?: number
    height?: number

    dateRenderFormatted = (format: string = App.FormatToMoment): string => moment(this.dateRender, App.FormatAtomFromMoment).format(format);
    // dateRenderMoment = (): Moment => moment(this.dateRender, App.FormatFromMoment);

    dateStartFormatted = (format: string = App.FormatToDate): string => moment(this.dateStart, App.FormatFromDate).format(format);
    dateStartMoment = (): Moment => moment(this.dateStart, App.FormatFromDate);

    dateEndFormatted = (format: string = App.FormatToDate): string => moment(this.dateEnd, App.FormatFromDate).format(format);
    dateEndMoment = (): Moment => moment(this.dateEnd, App.FormatFromDate);

    timeStartFormatted = (format: string = App.FormatToTime): string => moment(this.timeStart, App.FormatFromTime).format(format);
    timeEndFormatted = (format: string = App.FormatToTime): string => moment(this.timeEnd, App.FormatFromTime).format(format);

    public constructor(data: any) {
        this.bitRate = Normalize.initJsonNumber(data, 'bit_rate')
        this.codecName = Normalize.initJsonString(data, 'codec_name')
        this.dateStart = Normalize.initJsonString(data, 'date_start')
        this.dateEnd = Normalize.initJsonString(data, 'date_end')
        this.timeStart = Normalize.initJsonString(data, 'time_start')
        this.timeEnd = Normalize.initJsonString(data, 'time_end')
        this.dateRender = Normalize.initJsonString(data, 'date_render')
        this.duration = Normalize.initJsonNumber(data, 'duration')
        this.fps = Normalize.initJsonString(data, 'fps')
        this.frameRate = Normalize.initJsonString(data, 'frame_rate')
        this.totalFrames = Normalize.initJsonNumber(data, 'total_frames')
        this.resolution = Normalize.initJsonString(data, 'resolution')
        if (data.hasOwnProperty('thumb') && typeof data['thumb'] === "object" && data['thumb'].length > 0) this.thumb = data['thumb']
        this.width = Normalize.initJsonNumber(data, 'width')
        this.height = Normalize.initJsonNumber(data, 'height')
    }
}
