import {TimelapseImageModel, TSensorModel} from "./TimelapseImageModel";
import {App} from "../../../const/App";
import moment from "moment";
import {Normalize} from "../../../core/Normalize";

export type TTimelapseMachineFilterVO = {
    page?: number
    limit?: number
    sort?: string
    order?: string
    filter: {
        project_id?: string
        q?: string
    }
}

export class TimelapseMachineModel {
    machineId?: number
    name?: string
    active: boolean
    total: number
    size: number
    image?: TimelapseImageModel
    first?: TimelapseImageModel
    last?: TimelapseImageModel
    sensor?: TSensorModel[]
    createdAt?: string

    createdAtFormatted = (format: string = App.FormatToMoment): string => moment(this.createdAt, App.FormatAtomFromMoment).format(format)

    firstDateShotFormatted = (format: string = App.FormatToMoment): string | undefined => this.first?.dateShotFormatted(format)
    lastDateShotFormatted = (format: string = App.FormatToMoment): string | undefined => this.last?.dateShotFormatted(format)

    public constructor(data: any) {
        this.machineId = Normalize.initJsonNumber(data, 'machine_id')
        this.name = Normalize.initJsonString(data, 'name')
        this.active = Normalize.initJsonBool(data, 'active') ?? false
        this.total = Normalize.initJsonNumber(data, 'total') ?? 0
        this.size = Normalize.initJsonNumber(data, 'size') ?? 0
        this.image = Normalize.initJsonObject(data, 'image', v => new TimelapseImageModel(v))
        this.first = Normalize.initJsonObject(data, 'first', v => new TimelapseImageModel(v))
        this.last = Normalize.initJsonObject(data, 'last', v => new TimelapseImageModel(v))
        this.sensor = Normalize.initJsonArray(data, 'sensor', v1 => v1.map(v2 => new TSensorModel(v2)))
        this.createdAt = Normalize.initJsonString(data, 'created_at')
    }
}
