import moment from "moment";
import {App} from "../../../const/App";
import {Normalize} from "../../../core/Normalize";
import {Utils} from "../../../core/Utils";
import {E_ResUrlType} from "../../../const/Events";
import {Model} from "../../Model";

export interface TimelapseImageFilterVO {
    page?: number
    limit?: number
    sort?: string
    order?: string
    filter?: {
        date_start?: string
        date_end?: string
        time_start?: string
        time_end?: string
    }
}

export interface TimelapseImageAnalyticsFilterVO extends TimelapseImageFilterVO {
    filter?: {
        range?: string
        point?: string
        time_start?: string
        time_end?: string
    }
}

export class TimelapseImageModel {
    id?: string
    name?: string
    order?: string
    isCi: boolean
    sensor?: TSensorModel[]
    dateRaw?: string
    dateShot?: string

    dateGroup?: string

    dateShotFormatted = (format: string = App.FormatToMoment): string | undefined => this.dateShot !== undefined ? moment(this.dateShot, App.FormatAtomFromMoment).format(format) : undefined;

    // dateFormat = (): string | undefined => this._dateShot !== undefined ? moment(this._dateShot, App.FormatAtomFromMoment).format("DD-MM-YYYY") : undefined;

    getShotImageUrl = (options: {
        ci?: boolean,
        id?: string,
        name?: string,
        timeout?: number,
        type: E_ResUrlType,
        size?: 426 | 1280 | 1920,
        attach?: Record<string, any>[] | boolean
    }) => {
        let ci

        if (options.ci) {
            ci = options.ci
        } else if (
            options.type === E_ResUrlType.Thumb
            && this.isCi
            && (options.size && (options.size == 426 || options.size == 1280))
        ) {
            ci = this.isCi
        }

        const id = options.id ?? this.id
        const name = options.name ?? this.name
        const dateShot = this.dateRaw ?? this.dateShotFormatted(App.FormatFromDateTime)

        if (!dateShot || !id) {
            return undefined
        }

        const typeData: any[] = [options.type]

        if (options.size) {
            typeData.push(options.size)
        }

        if (options.attach !== undefined) {
            let attach

            if (typeof options.attach === "boolean") {
                if (options.attach && this.sensor && this.sensor.length > 0) {
                    attach = this.sensor.map(v => v.toObject())
                }
            } else if (options.attach.length > 0) {
                attach = options.attach
            }

            if (attach) {
                typeData.push(attach)
            }
        }

        return Utils.assetCdnGs(
            'si',
            {
                ...(
                    ci && {
                        ci: ci
                    }
                ),
                i: id,
                n: name,
                t: typeData,
                ds: dateShot,
                ...(
                    options.timeout && {
                        e: options.timeout
                    }
                )
            }
        )
    }

    public constructor(data: Record<string, any>) {
        this.id = Normalize.initJsonString(data, 'id');
        this.name = Normalize.initJsonString(data, 'name');
        this.order = Normalize.initJsonString(data, 'order');
        this.isCi = Normalize.initJsonBool(data, 'is_ci') ?? false;
        this.sensor = Normalize.initJsonArray(data, ['info', 'sensor'], v1 => v1.map(v2 => new TSensorModel(v2)));
        this.dateRaw = Normalize.initJsonString(data, 'date_raw');
        this.dateShot = Normalize.initJsonString(data, 'date_shot');
        this.dateGroup = Normalize.initJsonString(data, 'date_shot') ? moment(Normalize.initJsonString(data, 'date_shot'), App.FormatAtomFromMoment).format("DD-MM-YYYY") : undefined;
    }
}

export class TSensorModel extends Model {
    name?: string
    id?: number
    sensor?: string
    value?: string
    unit?: string
    unit2?: string

    public constructor(data: Record<string, any>) {
        super()

        this.name = Normalize.initJsonString(data, 'name')
        this.id = Normalize.initJsonNumber(data, 'sensor_id');
        this.sensor = Normalize.initJsonString(data, ['id', 'sensor'])
        this.value = Normalize.initJsonString(data, 'value')
        this.unit = Normalize.initJsonString(data, 'unit')
        this.unit2 = Normalize.initJsonString(data, ['unit_2', 'unit2'])
    }
}
