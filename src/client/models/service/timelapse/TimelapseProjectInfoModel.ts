import {Normalize} from "../../../core/Normalize";

interface IAttribute {
    name?: string,
    description?: string,
}

export class TimelapseProjectInfoModel {
    projectId?: string;
    name?: string;
    image?: string;
    description?: string;
    attribute?: IAttribute[];
    createdAt?: string;

    public constructor(data: any) {
        this.projectId = Normalize.initJsonString(data, 'project_id');
        this.name = Normalize.initJsonString(data, 'name');
        this.image = Normalize.initJsonString(data, 'image');
        this.description = Normalize.initJsonString(data, 'description');
        this.attribute = Normalize.initJsonArray(data, 'attribute', (arr: any) => arr.map((e: any): IAttribute => ({
            name: Normalize.initJsonString(e, 'name'),
            description: Normalize.initJsonString(e, 'description'),
        })));
        this.createdAt = data["created_at"];
    }

}