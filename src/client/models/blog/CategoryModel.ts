import {App} from "../../const/App";
import moment from "moment";
import {Normalize} from "../../core/Normalize";

export type TCategoriesFilterVO = {
    page?: number
    limit?: number
    sort?: string
    order?: string
    filter?: {
        q?: string
    }
}

export class CategoryModel {
    categoryId?: number
    name?: string
    description?: string
    image?: string
    createdAt?: string

    createdAtFormatted = (format: string = App.FormatToMoment): string => this.createdAt ? moment(this.createdAt, App.FormatAtomFromMoment).format(format) : ''

    constructor(data: Record<string, any>) {
        this.categoryId = Normalize.initJsonNumber(data, 'category_id')
        this.name = Normalize.initJsonString(data, 'name')
        this.description = Normalize.initJsonString(data, 'description')
        this.image = Normalize.initJsonString(data, 'image')
        this.createdAt = Normalize.initJsonString(data, 'created_at')
    }
}
