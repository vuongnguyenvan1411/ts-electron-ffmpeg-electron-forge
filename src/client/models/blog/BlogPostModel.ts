import {App} from "../../const/App";
import moment from "moment";
import {Normalize} from "../../core/Normalize";

export type TPostsFilterVO = {
    page?: number
    limit?: number
    sort?: string
    order?: string
    category_id?: number
    filter?: {
        q?: string
    }
}

export class BlogPostModel {
    postId?: number;
    name?: string;
    description?: string;
    image?: string;
    createdAt?: string;

    createdAtFormatted = (format: string = App.FormatToMoment): string => this.createdAt ? moment(this.createdAt, App.FormatAtomFromMoment).format(format) : '';

    constructor(data: Record<string, any>) {
        this.postId = Normalize.initJsonNumber(data, 'post_id');
        this.name = Normalize.initJsonString(data, 'name');
        this.description = Normalize.initJsonString(data, 'description');
        this.image = Normalize.initJsonString(data, 'image');
        this.createdAt = Normalize.initJsonString(data, 'created_at');
    }
}

export class BlogPostInfoModel extends BlogPostModel {
    share?: string;
    related?: BlogPostRelatedModel[];

    constructor(data: Record<string, any>) {
        super(data)

        this.share = Normalize.initJsonString(data, 'share');
        this.related = Normalize.initJsonArray(data, 'related', (item: Record<string, any>): BlogPostRelatedModel[] => item.map((e: any) => new BlogPostRelatedModel(e)))
    }
}

export class BlogPostRelatedModel extends BlogPostModel {
    constructor(data: any) {
        super(data)
    }
}
