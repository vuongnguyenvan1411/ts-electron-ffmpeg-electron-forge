import {UserModel} from "./UserModel";
import moment from "moment";
import {App} from "../const/App";

export interface Vr360FilterVO {
    page?: number;
    limit?: number;
    sort?: string;
    order?: string;
    filter?: {
        q?: string,
    };
}

export class Vr360Model {
    vr360Id: number;
    name: string;
    image?: string;
    user?: UserModel;
    address?: string;
    link?: string;
    createdAt?: string;

    createdAtFormatted = (format: string = App.FormatToMoment): string => this.createdAt ? moment(this.createdAt, App.FormatAtomFromMoment).format(format) : '';
    createdAtUnix = (): number | null => this.createdAt ? moment(this.createdAt, App.FormatAtomFromMoment).unix() : 0;

    constructor(data: any) {
        this.vr360Id = data['vr360_id'];
        this.name = data['name'];

        if (data.hasOwnProperty('image')) this.image = data['image'];
        if (data.hasOwnProperty('user')) this.user = new UserModel(data['user']);
        if (data.hasOwnProperty('address')) this.address = data['address'];
        if (data.hasOwnProperty('link')) this.link = data['link'];
        if (data.hasOwnProperty('created_at')) this.createdAt = data['created_at'];
    }
}

export interface Vr360SettingVO {
    is_share: number;
}
