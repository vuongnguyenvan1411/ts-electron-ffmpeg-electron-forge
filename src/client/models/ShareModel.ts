import {Normalize} from "../core/Normalize";
import {App} from "../const/App";
import moment from "moment";

export type TMachineShare = {
    id: number
    status: boolean
    image: boolean
    dates: { from: string, to: string, }[]
    video: boolean
    chart: boolean
    sensor: number[]
}

export type TMapPreviewShare = {
    name?: string
    expiryDate?: TExpiryDate
    status?: boolean
    createdAt: string
}

export type TConfigMapShare = {
    id: number
    sid: string
    status: boolean
    share?: TMapPreviewShare
}

type TConfigMachineShare = {
    machines: TMachineShare[]
}

export type TConfigMap2DShare = {
    tiles: number[]
    contours: string[]
    bArea: number[][]
    pId: string
    sPl: boolean
}

export type TConfigMap3DShare = {
    pcs: string[]
}

type TConfigSpaceShare = {
    m2ds: TConfigMapShare[]
    m3ds: TConfigMapShare[]
    vr360s: TConfigMapShare[]
}

export type TExpiryDate = {
    start?: string
    end?: string
}

export type TBaseShareVO = {
    name: string
    sid?: string
    password?: string
    expiryDate?: TExpiryDate
    createdAt?: string
    dateStart?: string
    dateEnd?: string
    status: boolean
    sInfo?: boolean
}

export type TTimelapseShareV0 = TBaseShareVO & {
    config: {
        machines?: TMachineShare[],
    }
}

export type TSpaceShareV0 = TBaseShareVO & {
    config: {
        m2ds: TConfigMapShare[]
        m3ds: TConfigMapShare[]
        vr360s: TConfigMapShare[]
    }
}

export type TMap2DShareV0 = TBaseShareVO & {
    config?: {
        bArea: number[][] | []
        tiles?: number[]
        contours?: string[]
        pId?: string
        sPl?: boolean
    }
}

export type TMap3DShareV0 = TBaseShareVO & {
    config?: {
        pcs?: string[]
    }
}

export type TMap360ShareV0 = TBaseShareVO

export type TShareFilterVO = {
    page: number
    limit: number
    sort?: string
    order?: string
    filter?: Record<string, any>
}

export class BaseShareModel {
    sid: string;
    name: string;
    description?: string;
    expiryDate?: TExpiryDate;
    password?: string;
    status: boolean;
    sInfo: boolean;
    link: string;
    createdAt: string;

    dateStartFormatted = (format: string = App.FormatToMoment): string | undefined => {
        if (this.expiryDate?.start) {
            return moment(this.expiryDate.start, App.FormatISOFromMoment).format(format);
        } else {
            return undefined;
        }
    };

    dateEndFormatted = (format: string = App.FormatToMoment): string | undefined => {
        if (this.expiryDate?.end) {
            return moment(this.expiryDate.end, App.FormatISOFromMoment).format(format);
        } else {
            return undefined;
        }
    };
    dateCreatedAtFormatted = (format: string = App.FormatToMoment): string | undefined => {
        if (this.createdAt) {
            return moment(this.createdAt, App.FormatISOFromMoment).format(format);
        } else {
            return undefined;
        }
    }

    constructor(data: Record<string, any>) {
        this.sid = Normalize.initJsonString(data, 'sid') ?? "";
        this.name = Normalize.initJsonString(data, "name") ?? "";
        this.description = Normalize.initJsonString(data, "description");
        this.expiryDate = Normalize.initJsonObject<TExpiryDate>(data, 'expiryDate', (e?) => {
            return {
                start: Normalize.initJsonString(e, 'start'),
                end: Normalize.initJsonString(e, 'end'),
            }
        });
        this.createdAt = Normalize.initJsonString(data, 'createdAt') ?? '';
        this.password = Normalize.initJsonString(data, "password");
        this.status = Normalize.initJsonBool(data, "status") ?? false;
        this.sInfo = Normalize.initJsonBool(data, "sInfo") ?? false;
        this.link = Normalize.initJsonString(data, 'link') ?? '';
    }
}

export class Vr360ShareModel extends BaseShareModel {
    constructor(data: Record<string, any>) {
        super(data);
    }
}

export class TimelapseShareModel extends BaseShareModel {
    config?: TConfigMachineShare;

    constructor(data: Record<string, any>) {
        super(data);
        this.config = Normalize.initJsonObject(data, 'config', (_: any): TConfigMachineShare => {
            return {
                machines: Normalize.initJsonArray(_, 'machines', (__: any) => __.map((e: any): TMachineShare => {
                    return {
                        id: Normalize.initJsonNumber(e, 'id') ?? 0,
                        status: Normalize.initJsonBool(e, 'status') ?? false,
                        image: Normalize.initJsonBool(e, 'image') ?? false,
                        dates: Normalize.initJsonArray(e, 'dates') ?? [],
                        video: Normalize.initJsonBool(e, 'video') ?? false,
                        chart: Normalize.initJsonBool(e, 'chart') ?? false,
                        sensor: Normalize.initJsonArray(e, 'sensor') ?? [],
                    }
                })) ?? [],
            }
        })
    }
}

export class Map2DShareModel extends BaseShareModel {
    config?: TConfigMap2DShare;

    constructor(data: Record<string, any>) {
        super(data);
        this.config = Normalize.initJsonObject(data, 'config', (_: any): TConfigMap2DShare => {
            return {
                bArea: Normalize.initJsonArray(_, 'bArea') ?? [],
                contours: Normalize.initJsonArray(_, 'contours') ?? [],
                tiles: Normalize.initJsonArray(_, 'tiles') ?? [],
                pId: Normalize.initJsonString(_, 'pId') ?? '',
                sPl: Normalize.initJsonBool(_, 'sPl') ?? false,
            }
        })
    }
}

export class Map3DShareModel extends BaseShareModel {
    config?: TConfigMap3DShare;

    constructor(data: Record<string, any>) {
        super(data);
        this.config = Normalize.initJsonObject(data, 'config', (child: Record<string, any>): TConfigMap3DShare => ({
            pcs: Normalize.initJsonArray(child, 'pcs') ?? [],
        }))
    }
}

export class SpaceShareModel extends BaseShareModel {
    config?: TConfigSpaceShare;

    dateStartFormatted = (format: string = App.FormatToMoment): string | undefined => {
        if (this.expiryDate?.start) {
            return moment(this.expiryDate.start, App.FormatISOFromMoment).format(format);
        } else {
            return undefined;
        }
    };

    dateEndFormatted = (format: string = App.FormatToMoment): string | undefined => {
        if (this.expiryDate?.end) {
            return moment(this.expiryDate.end, App.FormatISOFromMoment).format(format);
        } else {
            return undefined;
        }
    };
    dateCreatedAtFormatted = (format: string = App.FormatToMoment): string | undefined => {
        if (this.createdAt) {
            return moment(this.createdAt, App.FormatISOFromMoment).format(format);
        } else {
            return undefined;
        }
    }

    constructor(data: Record<string, any>) {
        super(data);
        this.config = Normalize.initJsonObject(data, 'config', (_: any): TConfigSpaceShare => ({
            m2ds: Normalize.initJsonArray(_, 'm2ds', (__: any) => __.map((e: any): TConfigMapShare => {
                return {
                    id: Normalize.initJsonNumber(e, 'id') ?? 0,
                    sid: Normalize.initJsonString(e, 'sid') ?? '',
                    status: Normalize.initJsonBool(e, 'status') ?? false,
                    share: Normalize.initJsonObject(e, 'share', (values: any): TMapPreviewShare => ({
                        status: Normalize.initJsonBool(values, 'status') ?? false,
                        name: Normalize.initJsonString(values, 'name') ?? '',
                        createdAt: Normalize.initJsonString(values, 'createdAt') ?? '',
                        expiryDate: Normalize.initJsonObject(values, 'expiryDate', (value: any): TExpiryDate => ({
                            start: Normalize.initJsonString(value, 'start'),
                            end: Normalize.initJsonString(value, 'end'),
                        })) ?? {},
                    }))
                }
            })) ?? [],
            m3ds: Normalize.initJsonArray(_, 'm3ds', (__: any) => __.map((e: any): TConfigMapShare => {
                return {
                    id: Normalize.initJsonNumber(e, 'id') ?? 0,
                    sid: Normalize.initJsonString(e, 'sid') ?? '',
                    status: Normalize.initJsonBool(e, 'status') ?? false,
                    share: Normalize.initJsonObject(e, 'share', (values: any): TMapPreviewShare => ({
                        status: Normalize.initJsonBool(values, 'status') ?? false,
                        name: Normalize.initJsonString(values, 'name') ?? '',
                        createdAt: Normalize.initJsonString(values, 'createdAt') ?? '',
                        expiryDate: Normalize.initJsonObject(values, 'expiryDate', (value: any): TExpiryDate => ({
                            start: Normalize.initJsonString(value, 'start'),
                            end: Normalize.initJsonString(value, 'end'),
                        })) ?? {},
                    }))
                }
            })) ?? [],
            vr360s: Normalize.initJsonArray(_, 'vr360s', (__: any) => __.map((e: any): TConfigMapShare => {
                return {
                    id: Normalize.initJsonNumber(e, 'id') ?? 0,
                    sid: Normalize.initJsonString(e, 'sid') ?? '',
                    status: Normalize.initJsonBool(e, 'status') ?? false,
                    share: Normalize.initJsonObject(e, 'share', (values: any): TMapPreviewShare => ({
                        status: Normalize.initJsonBool(values, 'status') ?? false,
                        name: Normalize.initJsonString(values, 'name') ?? '',
                        createdAt: Normalize.initJsonString(values, 'createdAt') ?? '',
                        expiryDate: Normalize.initJsonObject(values, 'expiryDate', (value: any): TExpiryDate => ({
                            start: Normalize.initJsonString(value, 'start'),
                            end: Normalize.initJsonString(value, 'end'),
                        })) ?? {},
                    }))
                }
            })) ?? [],
        }))
    }
}
