import {StoreConfig} from "../config/StoreConfig";
import {AgentData} from "./AgentData";

export class FeatureSingleton {
    private static instance: FeatureSingleton;

    public agent: AgentData

    public v2DProfile: boolean
    public v2DShare: boolean

    public v3DShare: boolean

    public v360Share: boolean

    public isHeaderEmbedWebView: boolean
    public isContributor: boolean

    private constructor() {
        const storeSingleton = StoreConfig.getInstance();
        this.agent = storeSingleton.Agent

        // 2D
        this.v2DProfile = this.agent.isWebApp();
        this.v2DShare = this.agent.isWebShare();

        // 3D
        this.v3DShare = this.agent.isWebApp();

        // 360
        this.v360Share = this.agent.isWebApp()

        // WebView
        this.isHeaderEmbedWebView = (this.agent.isWebApp() || this.agent.isWebShare() || this.agent.isAppDesktop()) && !this.agent.isWebAppInApp()
        this.isContributor = !this.agent.isWebAppInApp()
    }

    public static getInstance(): FeatureSingleton {
        if (!FeatureSingleton.instance) {
            FeatureSingleton.instance = new FeatureSingleton();
        }

        return FeatureSingleton.instance;
    }
}
