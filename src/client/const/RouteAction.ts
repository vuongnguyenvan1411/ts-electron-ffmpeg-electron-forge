export class RouteAction {
    static readonly POP = "POP";
    static readonly PUSH = "PUSH";
    static readonly REPLACE = "REPLACE";
    static GoBack = (delta: number = -1) => delta;
    static GoForward = (delta: number = 1) => delta;
}
