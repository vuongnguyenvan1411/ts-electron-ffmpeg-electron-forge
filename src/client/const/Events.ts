export enum SendingStatus {
    none,
    loading,
    idle,
    complete,
    success,
    failure,
    error,
    warning,
    disConnect,
    unauthorized,
    serverError,
    maintenance,
}

export enum ResCode {
    HTTP_OK = 200,
    HTTP_BAD_REQUEST = 400,
    HTTP_UNAUTHORIZED = 401,
    HTTP_INTERNAL_SERVER_ERROR = 500,
    HTTP_SERVICE_UNAVAILABLE = 503
}

export enum ResMethod {
    GET = "GET",
    POST = "POST",
    PUT = "PUT",
    DELETE = "DELETE",
}

export enum E_LSKey {
    User = 'user',
    Lang = 'lang'
}

export enum E_CookieKey {
    User = 'user'
}

export enum E_ResUrlType {
    Orig = "o",
    Download = "d",
    Thumb = "t"
}
