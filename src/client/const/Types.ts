import {SendingStatus} from "./Events";

export enum E_WebType {
    WebApp = "WebApp",
    WebShare = "WebShare",
    AppDesktop = "AppDesktop",
    Unknown = "Unknown"
}

export type TNextAppData = {
    header?: Record<string, any>
    more?: {
        now?: number
    }
}

export type TTimelapseMachineParamState = {
    name: string
    isSensor: boolean
    machines: TParamMachine[]
    allTime?: [string | undefined, string | undefined]
}

export type TParamMachine = {
    machineId: number
    active: boolean
    total: number
    name: string
}

export type TParamPartGeodetic = {
    m2d?: { spaceId: number, m2dId: number, name: string }[]
    m3d?: { spaceId: number, m3dId: number, name: string }[]
    vr360?: { spaceId: number, vr360Id: number, name: string }[]
}

export type TOrder = 'asc' | 'desc' | '' | null;
export type TActive = '0' | '1' | '' | null;

export type TFilterData = {
    active: TActive,
    order: TOrder,
    dateStart: string,
    dateEnd: string,
    timeStart: string,
    timeEnd: string,
    period: string,
}

export type TProjectType = 'timelapse' | '360' | 's2d' | 's3d' | 'tm3d' | 's360' | 'space';

export type TLocationLayout =
    "timelapse"
    | "vr360"
    | "geodetic"
    | "home"
    | "feedback"
    | "about"
    | "news"
    | "weighing"
    | "camera";

export type THeaderCtx = {
    title?: string,
    isLoading?: SendingStatus,
    onReload?: VoidFunction,
}

export type TOutletCtx = {
    isMobile?: boolean,
    onOpenDrawer?: VoidFunction,
    header?: {
        header?: THeaderCtx,
        setHeader: (header: THeaderCtx) => void,
    },
}
