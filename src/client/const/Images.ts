import LogoFullWhite from "../assets/image/logo_full_white.svg";
import LogoTimelapse from "../assets/image/logo_timelapse.svg";
import IconTimelapse from "../assets/image/icon_timelapse.svg";
import IconTimelapseFill from "../assets/image/icon_timelapse_fill.svg";
import IconHome from "../assets/image/icon_home.svg";
import IconHomeOutlined from "../assets/image/icon_home_outlined.svg";
import IconVr360 from "../assets/image/icon_vr360.svg";
import IconVr360Dark from "../assets/image/icon_vr360_dark.svg";
import IconVr360Fill from "../assets/image/icon_vr360_fill.svg";
import IconGeodetic from "../assets/image/icon_geodetic.svg";
import IconGeodeticDark from "../assets/image/icon_geodetic_dark.svg";
import IconGeodeticFill from "../assets/image/icon_geodetic_fill.svg";
import IconCamera from "../assets/image/icon_camera.svg";
import IconCameraFill from "../assets/image/icon_camera_fill.svg";
import IconWeighing from "../assets/image/icon_weighing.svg";
import IconWeighingFill from "../assets/image/icon_weighing_fill.svg";
import IconNews from "../assets/image/icon_news.svg";
import IconNewsFill from "../assets/image/icon_news_fill.svg";
import IconAbout from "../assets/image/icon_about.svg";
import IconAboutFill from "../assets/image/icon_about_fill.svg";
import IconFeedback from "../assets/image/icon_feedback.svg";
import IconFeedbackFill from "../assets/image/icon_feedback_fill.svg";
import IconCircleFill from "../assets/image/icon_circleFill.svg";
import IconUser from "../assets/image/icon_user.svg";
import IconCursor from "../assets/image/icon_cursor.svg";
import IconFilter from "../assets/image/icon_filter.svg";
import IconRefresh from "../assets/image/icon_refresh.svg";
import IconGear from "../assets/image/icon_gear.svg";
import IconShareNetwork from "../assets/image/icon_share_network.svg";
import IconClock from "../assets/image/icon_clock.svg";
import IconImage from "../assets/image/icon_image.svg";
import IconFolder from "../assets/image/icon_folder.svg";
import IconCalendar from "../assets/image/icon_calendar.svg";
import IconThermometer from "../assets/image/icon_thermometer.svg";
import IconSunDim from "../assets/image/icon_sun_dim.svg";
import IconUploadSimple from "../assets/image/icon_upload_simple.svg";
import IconAnalytics from "../assets/image/icon_analytics.svg";
import IconCompare from "../assets/image/icon_compare.svg";
import IconVideoCamera from "../assets/image/icon_video_camera.svg";
import IconUserCircle from "../assets/image/icon_user_circle.svg";
import IconRing from "../assets/image/icon_ring.svg";
import IconCalendarRegular from "../assets/image/icon_calendar_regular.svg";
import IconClockSolid from "../assets/image/icon_clock_solid.svg";
import IconImageRegular from "../assets/image/icon_image_regular.svg";
import IconMagnifyingGlass from "../assets/image/icon_magnifying_glass.svg";
import IconSwitchPart from "../assets/image/v2d/ic_switchpart.svg";
import IconLayerGroup from "../assets/image/v2d/ic_layer_base.svg";
import IconLayerMeasure from "../assets/image/v2d/ic_layer_measure.svg";
import IconLayerProfile from "../assets/image/v2d/ic_layer_profile.svg";
import IconCloseCircle from "../assets/image/v2d/ic_close.svg";
import IconCaretLeftCircle from "../assets/image/v2d/ic_caret_left_circle.svg";
import IconCaretUp from "../assets/image/icon_caret_up.svg";
import IconZoomInW from "../assets/image/v2d/ic_zoom_in.svg";
import IconZoomOutW from "../assets/image/v2d/ic_zoom_out.svg";
import IconSave from "../assets/image/v2d/ic_save.svg";
import IconShare from "../assets/image/v2d/ic_share.svg";
import IconProfile from "../assets/image/v2d/ic_profile.svg";
import IconMeasure from "../assets/image/v2d/ic_measure.svg";
import IconTrackLocation from "../assets/image/v2d/ic_track_location.svg";
import IconShowLocation from "../assets/image/v2d/ic_show_location.svg";
import IconCapture from "../assets/image/v2d/ic_capture.svg";
import IconDivider from "../assets/image/v2d/ic_divider.svg";
import IconSwitchBaseMap from "../assets/image/v2d/ic_switch_basemap.svg";
import IconClose from "../assets/image/icon_close.svg";
import IconSaveBg from "../assets/image/v2d/ic_save_bg.svg";
import IconEyeBg from "../assets/image/v2d/ic_eye_bg.svg";
import IconTrashBg from "../assets/image/v2d/ic_trash_bg.svg";
import IconLine from "../assets/image/v2d/ic_linestring.svg";
import IconPolygon from "../assets/image/v2d/ic_polygon.svg";
import IconCircle from "../assets/image/v2d/ic_circle.svg";
import IconCopy from "../assets/image/v2d/ic_copy.svg";
import IconPlus from "../assets/image/v2d/ic_plus.svg";
import IconPlusWhite from "../assets/image/tm3d/ic_plus_white.svg";
import IconNotePencil from "../assets/image/v2d/ic_note_pencil.svg"
import IconFolderNotchOpen from "../assets/image/v2d/ic_folder_notch_open.svg"
import IconTrash from "../assets/image/v2d/ic_trash.svg"
import IconNote from "../assets/image/v2d/ic_note.svg"
import IconShowLocationBl from "../assets/image/v2d/ic_show_location_bl.svg";
import IconStrokeWeight from "../assets/image/v2d/ic_stroke_weight.svg";
import IconStrokeDashed from "../assets/image/v2d/ic_stroke_dashed.svg";
import IconStrokeDefault from "../assets/image/v2d/ic_stroke_default.svg";
import IconAngle from "../assets/image/v2d/ic_angle.svg";
import IconSquare from "../assets/image/v2d/ic_square.svg";
import IconTriangle from "../assets/image/v2d/ic_triangle.svg";
import IconRectangle from "../assets/image/v2d/ic_rectangle.svg";
import IconWrench from "../assets/image/icon_wrench.svg";
import IconEye from "../assets/image/icon_eye.svg";
import IconPencilSimpleLine from "../assets/image/icon_pencil_simple_line.svg";
import IconQrCode from "../assets/image/icon_qr_code.svg";
import IconLinkSimple from "../assets/image/icon_link_simple.svg";
import IconArrowLine from "../assets/image/icon_arrow_line.svg";
import IconMapPinLine from "../assets/image/icon_map_pin_line.svg";
import IconGearOutlined from "../assets/image/icon_gear_outlined.svg";
import IconArrowRight from "../assets/image/icon_arrow_right.svg";
import IconTrackLocationBL from "../assets/image/v2d/ic_track_location_bl.svg";
import IconRefreshBL from "../assets/image/v2d/ic_refresh_bl.svg";
import IconModifyAndAdjust from "../assets/image/v2d/ic_modify_adjust.svg";
import IconPlusCircle from "../assets/image/v2d/ic_plus_circle.svg";
import IconPlusCircleWhite from "../assets/image/v3d/ic_plus_circle_white.svg";
import IconArrowsOutCardinal from "../assets/image/v2d/ic_arrows_outCardinal.svg";
import IconSnap from "../assets/image/v2d/ic_snap.svg";
import IconSnapMiddle from "../assets/image/v2d/ic_snap_middle.svg";
import IconSnapIntersect from "../assets/image/v2d/ic_snap_intersect.svg";
import IconSnapCenter from "../assets/image/v2d/ic_snap_center.svg";
import IconSnapEdge from "../assets/image/v2d/ic_snap_edge.svg";
import IconSnapVertex from "../assets/image/v2d/ic_snap_vertex.svg";
import IconEllipse from "../assets/image/v2d/ic_ellipse.svg";
import IconTrashError from "../assets/image/v2d/ic_trash_red.svg";
import IconPencilSimpleLineBold from "../assets/image/v2d/ic_pencil_simple_line.svg"
import IconWrenchWhite from "../assets/image/v2d/ic_wrench_white.svg"
import IconCLip from "../assets/image/v2d/ic_clip_tool.svg"
import IconTrim from "../assets/image/v2d/ic_trim.svg"
import IconContour from "../assets/image/v2d/ic_contour.svg"
import IconContourBl from "../assets/image/v2d/ic_contour_bl.svg"
import IconQuestion from "../assets/image/v2d/ic_question.svg";
import IconSwitchPartsDisabled from "../assets/image/v2d/ic_switch_part_disabled.svg";
import IconList from "../assets/image/v2d/ic_list.svg";
import IconArrowUpRight from "../assets/image/v2d/ic_arrow_up_right.svg";
import IconContourDisabled from "../assets/image/v2d/ic_contour_disabled.svg";
import IconXWhite from "../assets/image/v2d/ic_close_white.svg";
import IconStop from "../assets/image/v2d/ic_stop.svg";
import IconTriangleEdge from "../assets/image/v2d/ic_triangle_edge.svg";
import IconSaveBL from "../assets/image/v2d/ic_save_bl.svg";
import Icon2DLayer from "../assets/image/v2d/ic_layer_2d.svg";
import IconContourAct from "../assets/image/v2d/ic_contour_active.svg";
import IconArrowUpRight2D from "../assets/image/v2d/ic_arrow_up_right_2d.svg";
import IconCaretDoubleRight from "../assets/image/tm3d/ic_caret_double_right.svg";
import IconCaretDoubleLeft from "../assets/image/tm3d/ic_caret_double_left.svg";
import IconMinus from "../assets/image/tm3d/ic_minus.svg";
import IconUploadSimpleWhite from "../assets/image/v2d/ic_upload_simple_white.svg"
import Icon3dBox from "../assets/image/tm3d/ic_box_3d.svg"
import IconSceneLayer from "../assets/image/tm3d/ic_scene_layer.svg"

interface IImage {
    data: any,
    atl: string,
}

export class Images {
    static readonly iconHome: IImage = {
        data: IconHome,
        atl: "Atl-icon-home",
    };
    static readonly iconHomeOutlined: IImage = {
        data: IconHomeOutlined,
        atl: "Atl-icon-home-outlined",
    };
    static readonly iconMagnifyingGlass: IImage = {
        data: IconMagnifyingGlass,
        atl: "Atl-icon-magnifying-glass",
    };
    static readonly iconTimelapse: IImage = {
        data: IconTimelapse,
        atl: "Atl-icon-timelapse",
    };
    static readonly iconTimelapseFill: IImage = {
        data: IconTimelapseFill,
        atl: "Atl-icon-timelapse-fill",
    };
    static readonly iconVr360: IImage = {
        data: IconVr360,
        atl: "Atl-icon-vr360",
    };
    static readonly iconVr360Dark: IImage = {
        data: IconVr360Dark,
        atl: "Atl-icon-vr360-dark",
    };
    static readonly iconVr360Fill: IImage = {
        data: IconVr360Fill,
        atl: "Atl-icon-vr360",
    };
    static readonly iconGeodetic: IImage = {
        data: IconGeodetic,
        atl: "Atl-icon-geodetic",
    };
    static readonly iconGeodeticDark: IImage = {
        data: IconGeodeticDark,
        atl: "Atl-icon-geodetic-dark",
    };
    static readonly iconGeodeticFill: IImage = {
        data: IconGeodeticFill,
        atl: "Atl-icon-geodetic-fill",
    };
    static readonly iconCamera: IImage = {
        data: IconCamera,
        atl: "Atl-icon-camera",
    };
    static readonly iconCameraFill: IImage = {
        data: IconCameraFill,
        atl: "Atl-icon-camera",
    };
    static readonly iconWeighing: IImage = {
        data: IconWeighing,
        atl: "Atl-icon-weighing",
    };
    static readonly iconWeighingFill: IImage = {
        data: IconWeighingFill,
        atl: "Atl-icon-weighing",
    };
    static readonly iconNews: IImage = {
        data: IconNews,
        atl: "Atl-icon-news",
    };
    static readonly iconNewsFill: IImage = {
        data: IconNewsFill,
        atl: "Atl-icon-news",
    };
    static readonly iconAbout: IImage = {
        data: IconAbout,
        atl: "Atl-icon-about",
    };
    static readonly iconAboutFill: IImage = {
        data: IconAboutFill,
        atl: "Atl-icon-about",
    };
    static readonly iconFeedback: IImage = {
        data: IconFeedback,
        atl: "Atl-icon-feedback",
    };
    static readonly iconFeedbackFill: IImage = {
        data: IconFeedbackFill,
        atl: "Atl-icon-feedback",
    };
    static readonly iconFilter: IImage = {
        data: IconFilter,
        atl: "Atl-icon-filter",
    };
    static readonly iconRefresh: IImage = {
        data: IconRefresh,
        atl: "Atl-icon-refresh",
    };
    static readonly iconGear: IImage = {
        data: IconGear,
        atl: "Atl-icon-gear",
    };
    static readonly iconGearOutlined: IImage = {
        data: IconGearOutlined,
        atl: "Atl-icon-gear-outlined",
    };
    static readonly iconShareNetwork: IImage = {
        data: IconShareNetwork,
        atl: "Atl-icon-share-network",
    };
    static readonly iconClock: IImage = {
        data: IconClock,
        atl: "Atl-icon-clock",
    };
    static readonly iconImage: IImage = {
        data: IconImage,
        atl: "Atl-icon-image",
    };
    static readonly iconFolder: IImage = {
        data: IconFolder,
        atl: "Atl-icon-folder",
    };
    static readonly iconCalendar: IImage = {
        data: IconCalendar,
        atl: "Atl-icon-calendar",
    };
    static readonly iconThermometer: IImage = {
        data: IconThermometer,
        atl: "Atl-icon-thermometer",
    };
    static readonly iconSunDim: IImage = {
        data: IconSunDim,
        atl: "Atl-icon-sun-dim",
    };
    static readonly iconUploadSimple: IImage = {
        data: IconUploadSimple,
        atl: "Atl-icon-upload-simple",
    };
    static readonly iconAnalytics: IImage = {
        data: IconAnalytics,
        atl: "Atl-icon-analytics",
    };
    static readonly iconCompare: IImage = {
        data: IconCompare,
        atl: "Atl-icon-compare",
    };
    static readonly iconVideoCamera: IImage = {
        data: IconVideoCamera,
        atl: "Atl-icon-video-camera",
    };
    static readonly iconUserCircle: IImage = {
        data: IconUserCircle,
        atl: "Atl-icon-user-circle",
    };
    static readonly iconRing: IImage = {
        data: IconRing,
        atl: "Atl-icon-ring",
    };
    static readonly logoTimelapse: IImage = {
        data: LogoTimelapse,
        atl: "Atl-logo-timelapse",
    };
    static readonly iconCalendarRegular: IImage = {
        data: IconCalendarRegular,
        atl: "Atl-icon-calendar-regular",
    };
    static readonly iconClockSolid: IImage = {
        data: IconClockSolid,
        atl: "Atl-icon-clock-solid",
    };
    static readonly iconImageRegular: IImage = {
        data: IconImageRegular,
        atl: "Atl-icon-image-regular",
    };
    static readonly iconSwitchParts: IImage = {
        data: IconSwitchPart,
        atl: "Atl-icon-switch-parts",
    };
    static readonly iconLayerGroup: IImage = {
        data: IconLayerGroup,
        atl: "Atl-icon-layer-group",
    };
    static readonly iconLayerMeasure: IImage = {
        data: IconLayerMeasure,
        atl: "Atl-icon-layer-measure",
    };
    static readonly iconLayerProfile: IImage = {
        data: IconLayerProfile,
        atl: "Atl-icon-layer-profile",
    };
    static readonly iconCloseCircle: IImage = {
        data: IconCloseCircle,
        atl: "Atl-icon-close",
    };
    static readonly iconCaretLeftCircle: IImage = {
        data: IconCaretLeftCircle,
        atl: "Atl-icon-left-circle",
    };
    static readonly iconZoomInW: IImage = {
        data: IconZoomInW,
        atl: "Atl-icon-zoom-in",
    };
    static readonly iconZoomOutW: IImage = {
        data: IconZoomOutW,
        atl: "Atl-icon-zoom-out",
    };
    static readonly iconSave: IImage = {
        data: IconSave,
        atl: "Atl-icon-save",
    };
    static readonly iconShare: IImage = {
        data: IconShare,
        atl: "Atl-icon-share",
    };
    static readonly iconWrench: IImage = {
        data: IconWrench,
        atl: "Atl-icon-wrench",
    };
    static readonly iconTrackLocation: IImage = {
        data: IconTrackLocation,
        atl: "Atl-icon-track-location",
    };
    static readonly iconShowLocation: IImage = {
        data: IconShowLocation,
        atl: "Atl-icon-show-location",
    };
    static readonly iconProfile: IImage = {
        data: IconProfile,
        atl: "Atl-icon-profile",
    };
    static readonly iconMeasure: IImage = {
        data: IconMeasure,
        atl: "Atl-icon-measure",
    };
    static readonly iconCapture: IImage = {
        data: IconCapture,
        atl: "Atl-icon-capture",
    };
    static readonly iconCaretUp: IImage = {
        data: IconCaretUp,
        atl: "Atl-icon-caret-up",
    };
    static readonly iconDivider: IImage = {
        data: IconDivider,
        atl: "Atl-icon-divider",
    };
    static readonly iconSwitchBaseMap: IImage = {
        data: IconSwitchBaseMap,
        atl: "Atl-icon-switch-base-map",
    };
    static readonly iconXCircle: IImage = {
        data: IconClose,
        atl: "Atl-icon-x-close",
    };
    static readonly iconSaveBg: IImage = {
        data: IconSaveBg,
        atl: "Atl-icon-save-back-ground",
    };
    static readonly iconEyeBg: IImage = {
        data: IconEyeBg,
        atl: "Atl-icon-eye-back-ground",
    };
    static readonly iconTrashBg: IImage = {
        data: IconTrashBg,
        atl: "Atl-icon-trash-back-ground",
    };
    static readonly iconLine: IImage = {
        data: IconLine,
        atl: "Atl-icon-draw-line",
    };
    static readonly iconPolygon: IImage = {
        data: IconPolygon,
        atl: "Atl-icon-draw-polygon",
    };
    static readonly iconCircle: IImage = {
        data: IconCircle,
        atl: "Atl-icon-draw-circle",
    };
    static readonly iconCopy: IImage = {
        data: IconCopy,
        atl: "Atl-icon-copy",
    };
    static readonly iconTrash: IImage = {
        data: IconTrash,
        atl: "Atl-icon-trash",
    };
    static readonly iconPlus: IImage = {
        data: IconPlus,
        atl: "Atl-icon-plus",
    };
    static readonly iconNotePencil: IImage = {
        data: IconNotePencil,
        atl: "Atl-icon-note-pencil",
    };
    static readonly iconFolderNotchOpen: IImage = {
        data: IconFolderNotchOpen,
        atl: "Atl-icon-folder-notch-open",
    };
    static readonly iconNote: IImage = {
        data: IconNote,
        atl: "Atl-icon-note",
    }
    static readonly iconShowLocationBl: IImage = {
        data: IconShowLocationBl,
        atl: "Atl-icon-show-location-bl",
    };
    static readonly iconStrokeWeight: IImage = {
        data: IconStrokeWeight,
        atl: "Atl-icon-stroke-weight",
    };
    static readonly iconStrokeDashed: IImage = {
        data: IconStrokeDashed,
        atl: "Atl-icon-stroke-dashed",
    };
    static readonly iconStrokeDefault: IImage = {
        data: IconStrokeDefault,
        atl: "Atl-icon-stroke-default",
    };
    static readonly iconAngle: IImage = {
        data: IconAngle,
        atl: "Atl-icon-angle",
    };
    static readonly iconSquare: IImage = {
        data: IconSquare,
        atl: "Atl-icon-square",
    };
    static readonly iconRectangle: IImage = {
        data: IconRectangle,
        atl: "Atl-icon-rectangle",
    };
    static readonly iconTriangle: IImage = {
        data: IconTriangle,
        atl: "Atl-icon-triangle",
    };
    static readonly iconEye: IImage = {
        data: IconEye,
        atl: "Atl-icon-eye",
    };
    static readonly iconTrackLocationBL: IImage = {
        data: IconTrackLocationBL,
        atl: "Atl-icon-track-location-bl",
    };
    static readonly iconRefreshBL: IImage = {
        data: IconRefreshBL,
        atl: "Atl-icon-refresh-bl",
    };
    static readonly iconModifyAndAdjust: IImage = {
        data: IconModifyAndAdjust,
        atl: "Atl-icon-modify-adjust",
    };
    static readonly iconQrCode: IImage = {
        data: IconQrCode,
        atl: "Atl-icon-qr-code",
    };
    static readonly iconPlusCircle: IImage = {
        data: IconPlusCircle,
        atl: "Atl-icon-plus-circle",
    };
    static readonly iconPlusCircleWhite: IImage = {
        data: IconPlusCircleWhite,
        atl: "Atl-icon-plus-circle-w",
    };
    static readonly iconSnap: IImage = {
        data: IconSnap,
        atl: "Atl-icon-snap-main",
    };
    static readonly iconEllipse: IImage = {
        data: IconEllipse,
        atl: "Atl-icon-ellipse",
    };
    static readonly iconArrowsOutCardinal: IImage = {
        data: IconArrowsOutCardinal,
        atl: "Atl-icon-arrows-out-cardinal",
    };
    static readonly iconSnapMiddle: IImage = {
        data: IconSnapMiddle,
        atl: "Atl-icon-snap-middle",
    };
    static readonly iconSnapIntersect: IImage = {
        data: IconSnapIntersect,
        atl: "Atl-icon-snap-intersect",
    };
    static readonly iconSnapEdge: IImage = {
        data: IconSnapEdge,
        atl: "Atl-icon-snap-edge",
    };
    static readonly iconSnapCenter: IImage = {
        data: IconSnapCenter,
        atl: "Atl-icon-snap-center",
    };
    static readonly iconSnapVertex: IImage = {
        data: IconSnapVertex,
        atl: "Atl-icon-snap-vertex",
    };
    static readonly iconTrashError: IImage = {
        data: IconTrashError,
        atl: "Atl-icon-trash-error",
    };
    static readonly iconPencilSimpleLine: IImage = {
        data: IconPencilSimpleLine,
        atl: "Atl-icon-pencil-simple-line",
    };
    static readonly iconPencilSimpleLineBold: IImage = {
        data: IconPencilSimpleLineBold,
        atl: "Atl-icon-pencil-simple-line-bold",
    };
    static readonly iconWrenchWhite: IImage = {
        data: IconWrenchWhite,
        atl: "Atl-icon-wrench-white",
    };
    static readonly iconLinkSimple: IImage = {
        data: IconLinkSimple,
        atl: "Atl-icon-link-simple",
    };
    static readonly iconArrowLine: IImage = {
        data: IconArrowLine,
        atl: "Atl-icon-arrow-line",
    };
    static readonly iconMapPinLine: IImage = {
        data: IconMapPinLine,
        atl: "Atl-icon-map-pin-line",
    };
    static readonly iconArrowRight: IImage = {
        data: IconArrowRight,
        atl: "Atl-icon-arrow-right",
    };
    static readonly iconCLip: IImage = {
        data: IconCLip,
        atl: "Atl-icon-clip",
    };
    static readonly iconTrim: IImage = {
        data: IconTrim,
        atl: "Atl-icon-trim",
    };
    static readonly iconContour: IImage = {
        data: IconContour,
        atl: "Atl-icon-contour",
    };
    static readonly iconContourBl: IImage = {
        data: IconContourBl,
        atl: "Atl-icon-contour-bl",
    };
    static readonly iconQuestion: IImage = {
        data: IconQuestion,
        atl: "Atl-icon-question",
    };
    static readonly iconSwitchPartsDisabled: IImage = {
        data: IconSwitchPartsDisabled,
        atl: "Atl-icon-switch-parts-disabled",
    };
    static readonly iconList: IImage = {
        data: IconList,
        atl: "Atl-icon-list",
    };
    static readonly iconArrowUpRight: IImage = {
        data: IconArrowUpRight,
        atl: "Atl-icon-arrow-up-right",
    };
    static readonly iconArrowUpRight2D: IImage = {
        data: IconArrowUpRight2D,
        atl: "Atl-icon-arrow-up-right-2d",
    };
    static readonly iconContourDisabled: IImage = {
        data: IconContourDisabled,
        atl: "Atl-icon-contour-disabled",
    };
    static readonly iconXWhite: IImage = {
        data: IconXWhite,
        atl: "Atl-icon-x-close-white",
    };
    static readonly iconStop: IImage = {
        data: IconStop,
        atl: "Atl-icon-stop",
    };
    static readonly iconTriangleEdge: IImage = {
        data: IconTriangleEdge,
        atl: "Atl-icon-triangle-edge",
    };
    static readonly iconSaveBL: IImage = {
        data: IconSaveBL,
        atl: "Atl-icon-save-black",
    };
    static readonly icon2DLayer: IImage = {
        data: Icon2DLayer,
        atl: "Atl-icon-2d-layer",
    };
    static readonly iconContourActive: IImage = {
        data: IconContourAct,
        atl: "Atl-icon-contour-active",
    };
    static readonly iconCaretDoubleRight: IImage = {
        data: IconCaretDoubleRight,
        atl: "Atl-icon-caret-double-right",
    };
    static readonly iconCaretDoubleLeft: IImage = {
        data: IconCaretDoubleLeft,
        atl: "Atl-icon-caret-double-left",
    };
    static readonly iconMinus: IImage = {
        data: IconMinus,
        atl: "Atl-icon-minus",
    };
    static readonly iconPlusW: IImage = {
        data: IconPlusWhite,
        atl: "Atl-icon-plus-w",
    };
    static readonly iconUploadSimpleW: IImage = {
        data: IconUploadSimpleWhite,
        atl: "Atl-icon-upload-simpleW",
    };
    static readonly icon3dBox: IImage = {
        data: Icon3dBox,
        atl: "Atl-icon-3d-box",
    };
    static readonly iconSceneLayer: IImage = {
        data: IconSceneLayer,
        atl: "Atl-icon-scene-layer",
    };

    static readonly iconCircleFill = IconCircleFill;
    static readonly iconUser = IconUser;
    static readonly iconCursor = IconCursor;
    static readonly logoFullWhite = LogoFullWhite;
}
