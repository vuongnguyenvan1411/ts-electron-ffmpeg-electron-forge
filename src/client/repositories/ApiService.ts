import {TProjectType} from "../const/Types";
import {injectable} from "inversify";
import {ApiResModel} from "../models/ApiResModel";
import {AxiosClient} from "./AxiosClient";
import {TLoginVO} from "../models/UserModel";
import {TPlatformFileEP} from "../models/platform/PlatformFileModel";

@injectable()
export class ApiService {
    public static readonly about: string = "information/about";
    public static readonly feedback: string = "information/feedback";
    public static readonly timelapses: string = "timelapse/projects";
    public static readonly machines: string = 'timelapse/machines';
    public static readonly getBlogPosts: string = 'blog/posts';
    public static readonly getBlogCategories: string = 'blog/categories';

    public static readonly editInfoMe: string = 'account/info';
    public static readonly editPasswordMe: string = 'account/password';
    public static readonly editAvatarMe: string = 'account/avatar';
    public static readonly editBackgroundMe: string = 'account/background';

    public static readonly mapGetInfoLocation: string = 'map/getInfoLocation';

    public static uploadImage(type: string): string {
        return type === "avatar" ? this.editAvatarMe : this.editBackgroundMe;
    }

    public static resVr360Setting(vr360Id: number): string {
        return `service/vr360/${vr360Id}/setting`;
    }

    public static resTimelapseSetting(projectId: number): string {
        return `timelapse/project/${projectId}/setting`;
    }

    public static getTimelapseSensors(projectId: number): string {
        return `timelapse/project/${projectId}/sensors`;
    }

    public static resShare(type: TProjectType, pid: number | string, sid?: string | "s"): string {
        return `service/share/${type}/${pid}/${sid === 's' ? 'shares' : 'share'}/${sid && sid !== 's' ? sid : ''}`;
    }

    public static resProfile(type: TProjectType, pid: number | string, sid?: string | 's'): string {
        return `service/profile/${type}/${pid}/${sid === 's' ? 'profiles' : 'profile'}/${sid && sid !== 's' ? sid : ''}`
    }

    public static getTimelapseShare(projectId: number): string {
        return `timelapse/project/${projectId}/shares`;
    }

    public static resTimelapseShare(projectId: number): string {
        return `timelapse/project/${projectId}/share`;
    }

    public static resVr360(vr360Id?: number | "s") {
        return `service/vr360${vr360Id === 's' ? '' : '/'}${vr360Id}`
    }

    public static resCamera(cameraId?: number | "s") {
        return `service/camera${cameraId === 's' ? '' : '/'}${cameraId}`
    }

    public static resWeighing(weighingId?: number | "s") {
        return `service/weighing${weighingId === 's' ? '' : '/'}${weighingId}`
    }

    public static getVr360QrCode(vr360Id: number): string {
        return `qr_code/vr360/${vr360Id}`;
    }

    public static getQrCode(id: string) {
        return `qr_code/share/${id}`;
    }

    public static getTimelapseMachines(projectId: number): string {
        return `timelapse/project/${projectId}/machines`;
    }

    public static getTimelapseImages(machineId: number): string {
        return `timelapse/images/${machineId}`;
    }

    public static getTimelapseProject(projectId: number): string {
        return `timelapse/project/${projectId}`;
    }

    public static resSpace(spaceId?: number | "s") {
        return `service/space${spaceId === 's' ? '' : '/'}${spaceId}`
    }

    public static resSpaceSetting(spaceId: number) {
        return `service/space/${spaceId}/setting`;
    }

    public static getSpaceM2d(spaceId: number, m2dId: number) {
        return `service/space/${spaceId}/m2d/${m2dId}`;
    }

    public static getSpaceM3d(spaceId: number, m3dId: number) {
        return `service/space/${spaceId}/m3d/${m3dId}`;
    }

    public static getTimelapseVideoAuto(machineId: number): string {
        return `timelapse/video/auto/${machineId}`;
    }

    public static getTimelapseAnalytics(machineId: number): string {
        return `timelapse/sensor/chart/${machineId}`;
    }

    public static getTimelapseVideoFilter(machineId: number): string {
        return `timelapse/video/filter/${machineId}`;
    }

    public static getTimelapseVideoImages(machineId: number): string {
        return `timelapse/video/images/${machineId}`;
    }

    public static getTimelapseImageIgroups(machineId: number): string {
        return `timelapse/igroups/${machineId}`;
    }

    public static getBlogPost(postId: number): string {
        return `blog/post/${postId}`;
    }

    /////

    init(): Promise<ApiResModel> {
        return AxiosClient.get("init-web")
    }

    tracking(): Promise<ApiResModel> {
        return AxiosClient.get("tracking-web")
    }

    login(data: TLoginVO): Promise<ApiResModel> {
        return AxiosClient.post("auth/login", data)
    }

    logout(): Promise<ApiResModel> {
        return AxiosClient.post("account/logout");
    }

    getMe(): Promise<ApiResModel> {
        return AxiosClient.get("account/me")
    }

    addPlatformFile(data: TPlatformFileEP): Promise<ApiResModel> {
        return AxiosClient.post("platform/file", data);
    }
}

export function fetchCount(amount = 1) {
    return new Promise<{ data: number }>((resolve) =>
        setTimeout(() => resolve({data: amount}), 500)
    );
}
