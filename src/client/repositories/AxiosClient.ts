import axios, {AxiosRequestConfig, CancelToken} from "axios";
import {getLng} from "../locales/i18n";
import {App} from "../const/App";
import {ApiResModel} from "../models/ApiResModel";
import {StoreConfig} from "../config/StoreConfig";
import moment from "moment";
import {Color} from "../const/Color";
import {EDData} from "../core/encrypt/EDData";
import {EDFile} from "../core/encrypt/EDFile";

export class AxiosClient {
    static readonly Config = (isUp: boolean = false): AxiosRequestConfig => {
        const lng = getLng();

        const config: AxiosRequestConfig = {
            headers: {
                "Content-Type": isUp ? "multipart/form-data" : "text/json",
                "Lang-Code": lng ?? 'vi',
                "Platform": "web",
                // "Content-Type": "application/x-www-form-urlencoded"
            },
            withCredentials: false
        }

        if (!isUp) {
            config.headers!.Accept = "application/json";
        }

        const storeConfig = StoreConfig.getInstance()

        if (storeConfig.accessToken && storeConfig.accessToken.token && storeConfig.accessToken.token.length > 0) {
            const et = EDData.setData({
                t: storeConfig.accessToken.token,
                // e: moment().add(30, 'seconds').unix()
                e: moment().add(1, 'days').unix()
            })

            config.headers!.Authorization = `Bearer ${et}`
        }

        if (storeConfig.Agent && storeConfig.Agent.geoLocation && Object.keys(storeConfig.Agent.geoLocation).length > 0) {
            config.headers!['o-agent'] = EDData.setData({
                geoLocation: storeConfig.Agent.geoLocation
            })
        }

        return config
    }

    public static get(path: string, query?: any, cancelToken?: CancelToken): Promise<ApiResModel> {
        const ep = EDFile.setLinkUrl({
            p: path,
            q: AxiosClient.convertDataGet(query),
            // e: moment().add(30, 'seconds').unix()
            e: moment().add(1, 'days').unix()
        })

        console.log('%c<-GET--------------------------------------------', Color.ConsoleInfo)
        console.log(`[${moment().format(App.FormatTimeFull)}] PATH: ${path}`)
        if (query) console.log(`QUERY:`, query);

        return axios
            .get(`${App.ApiUrl}/${ep}`, cancelToken
                ? {
                    ...AxiosClient.Config(),
                    ...{
                        cancelToken: cancelToken
                    }
                }
                : AxiosClient.Config())
            .then(r => {
                const data = EDData.getData(r.data) ?? r.data

                console.log(`[${moment().format(App.FormatTimeFull)}] RES:`, data)
                console.log('%c--END------------------------------------------->', Color.ConsoleInfo)

                return new ApiResModel(data)
            })
    }

    public static post(
        path: string,
        data?: any,
        isUp: boolean = false,
        config?: AxiosRequestConfig,
    ): Promise<ApiResModel> {
        const ep = EDFile.setLinkUrl({
            p: path,
            // e: moment().add(30, 'seconds').unix()
            e: moment().add(1, 'days').unix()
        })

        console.log('%c<-POST-------------------------------------------', Color.ConsoleInfo)
        console.log(`[${moment().format(App.FormatTimeFull)}] PATH: ${path}`)

        let _data

        if (data) {
            console.log(`DATA:`, data)

            if (isUp) {
                _data = data
            } else {
                _data = EDData.setData(data)
            }
        }

        return axios
            .post(`${App.ApiUrl}/${ep}`, _data, config ?? AxiosClient.Config(isUp))
            .then(r => {
                const data = EDData.getData(r.data) ?? r.data

                console.log(`[${moment().format(App.FormatTimeFull)}] RES:`, data)
                console.log('%c--END------------------------------------------->', Color.ConsoleInfo)

                return new ApiResModel(data)
            })
    }

    public static put(
        path: string,
        data?: any,
        config: AxiosRequestConfig = AxiosClient.Config()
    ): Promise<ApiResModel> {
        const ep = EDFile.setLinkUrl({
            p: path,
            // e: moment().add(30, 'seconds').unix()
            e: moment().add(1, 'days').unix()
        });

        console.log('%c<-PUT--------------------------------------------', Color.ConsoleInfo)
        console.log(`[${moment().format(App.FormatTimeFull)}] PATH: ${path}`)

        let _data

        if (data) {
            console.log(`DATA:`, data)

            _data = EDData.setData(data)
        }

        return axios
            .put(`${App.ApiUrl}/${ep}`, _data, config)
            .then(r => {
                const data = EDData.getData(r.data) ?? r.data

                console.log(`[${moment().format(App.FormatTimeFull)}] RES:`, data)
                console.log('%c--END------------------------------------------->', Color.ConsoleInfo)

                return new ApiResModel(data)
            })
    }

    public static delete(
        path: string,
        config: AxiosRequestConfig = AxiosClient.Config()
    ): Promise<ApiResModel> {
        const ep = EDFile.setLinkUrl({
            p: path,
            // e: moment(new Date()).add(1, 'hours').unix()
            e: moment().add(1, 'days').unix()
        })

        console.log('%c<-DELETE--------------------------------------------', Color.ConsoleInfo)
        console.log(`[${moment().format(App.FormatTimeFull)}] PATH: ${path}`)

        return axios
            .delete(`${App.ApiUrl}/${ep}`, config)
            .then(r => {
                const data = EDData.getData(r.data) ?? r.data

                console.log(`[${moment().format(App.FormatTimeFull)}] RES:`, data)
                console.log('%c--END------------------------------------------->', Color.ConsoleInfo)

                return new ApiResModel(data)
            })
    }

    public static convertDataGet(data: any, prefix: string = '') {
        const cv: any = {};

        if (typeof data === "object") {
            Object.entries(data as any).forEach(([key, value]) => {
                if (typeof value === "object") {
                    Object.assign(cv, AxiosClient.convertDataGet(value, prefix.length > 0 ? `${prefix}_${key}` : key))
                } else {
                    cv[prefix.length > 0 ? `${prefix}_${key}` : key] = value
                }
            })
        }

        return cv
    }
}
