import {injectable} from "inversify";
import {AgentData} from "../models/AgentData";
import {container} from "./InversifyConfig";
import {AccessTokenModel} from "../models/UserModel";
import {includes} from "lodash";

@injectable()
export class StoreConfig {
    public static getInstance(): StoreConfig {
        return container.get(StoreConfig)
    }

    accessToken?: AccessTokenModel

    public Token?: string
    public Agent: AgentData

    public checkSlpkTest(username: string): boolean {
        const env = process.env.NEXT_PUBLIC_SLPK_TEST ?? 'vhv';
        const users = env.split(',')

        return includes(users, username)
    }

    public checkPlatformTest(username: string): boolean {
        const env = process.env.NEXT_PUBLIC_PLATFORM_TEST ?? 'vhv';
        const users = env.split(',')

        return includes(users, username)
    }

    public isEnvDev(): boolean {
        return process.env.NEXT_PUBLIC_APP_ENV == 'dev'
    }

    public isEnvProd(): boolean {
        return process.env.NEXT_PUBLIC_APP_ENV == 'prod'
    }
}
