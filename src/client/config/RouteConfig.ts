import React from "react";
import {Vr360Screen} from "../presentation/screens/service/vr360/Vr360Screen";
import {GeodeticScreen} from "../presentation/screens/service/geodetic/GeodeticScreen";
import {BlogScreen} from "../presentation/screens/blog/BlogScreen";
import {NotFoundScreen} from "../presentation/screens/error/NotFoundScreen";
import {Vr360ViewScreen} from "../presentation/screens/service/vr360/Vr360ViewScreen";
import {Vr360SettingScreen} from "../presentation/screens/service/vr360/Vr360SettingScreen";
import {TimelapseViewTabScreen} from "../presentation/screens/service/timelapse/TimelapseViewTabScreen";
import {GeodeticInfoScreen} from "../presentation/screens/service/geodetic/GeodeticInfoScreen";
import {GeodeticSettingScreen} from "../presentation/screens/service/geodetic/GeodeticSettingScreen";
import {TimelapseSettingScreen} from "../presentation/screens/service/timelapse/TimelapseSettingScreen";
import {MeScreen} from "../presentation/screens/account/MeScreen";
import {TimelapseProjectInfoScreen} from "../presentation/screens/service/timelapse/TimelapseProjectInfoScreen";
import {PostScreen} from "../presentation/screens/blog/PostScreen";
import {FeedbackScreen} from "../presentation/screens/information/FeedbackScreen";
import {CameraScreen} from "../presentation/screens/service/camera/CameraScreen";
import {WeighingScreen} from "../presentation/screens/service/weighing/WeighingScreen";
import {AboutScreen} from "../presentation/screens/information/AboutScreen";
import {MeSettingSiderScreen} from "../presentation/screens/account/MeSettingSiderScreen";
import {GeodeticView2DScreen} from "../presentation/screens/service/geodetic/GeodeticView2DScreen";
import {GeodeticViewPC3DScreen} from "../presentation/screens/service/geodetic/GeodeticViewPC3DScreen";
import {GeodeticViewTM3DScreen} from "../presentation/screens/service/geodetic/GeodeticViewTM3DScreen";
import {GeodeticViewVr360Screen} from "../presentation/screens/service/geodetic/GeodeticViewVr360Screen";
import {LoginScreen} from "../presentation/screens/auth/LoginScreen";
import {Example} from "../presentation/screens/auth/Example";
import {RegisterTest} from "../presentation/screens/auth/RegisterTest";
import {DownloadScreen} from "../presentation/screens/common/DownloadScreen";
import {ForgotPasswordScreen} from "../presentation/screens/auth/ForgotPasswordScreen";
import {HomeScreen} from "../presentation/screens/common/HomeScreen";
import {TimelapseProjectScreen} from "../presentation/screens/service/timelapse/TimelapseProjectScreen";
import {TimelapseMachineScreen} from "../presentation/screens/service/timelapse/TimelapseMachineScreen";
import {TestAgentScreen} from "../presentation/screens/test/TestAgentScreen";
import {GeodeticManagerScreen} from "../presentation/screens/service/geodetic/GeodeticManagerScreen";
import {GeodeticManagerScreenUpload} from "../presentation/screens/service/geodetic/GeodeticManagerScreenUpload";
import {GeodeticManagerScreenDeleted} from "../presentation/screens/service/geodetic/GeodeticManagerScreenDeleted";
import {TestBigUpScreen} from "../presentation/screens/test/TestBigUpScreen";
import {TestEnvScreen} from "../presentation/screens/test/TestEnvScreen";
import {TestWorkerScreen} from "../presentation/screens/test/TestWorkerScreen";
import {TimelapseRenderVideoElectron} from "../presentation/electrons/TimelapseRenderVideoElectron";
import {TestTempScreen} from "../presentation/screens/test/TestTempScreen";
import {TestVrScreen} from "../presentation/screens/test/TestVrScreen";

interface IRcc {
    path?: string
    component: React.ComponentType<any>
    protect?: boolean
    isMaster?: boolean
    children?: IRcc[]
}

export interface IRco extends IRcc {
    routes?: IRcc[]
}

export class RouteConfig {
    static isHashRouter = false

    static readonly HOME: string = "/"
    static readonly LOGIN: string = "/login"
    static readonly REGISTER: string = "/register"
    static readonly EXAMPLE: string = "/example"
    static readonly DOWNLOAD: string = "/download"
    static readonly FORGOT_PASSWORD: string = "/forgot-password"

    static readonly TIMELAPSE: string = "/timelapse"
    static readonly TIMELAPSE_SETTING: string = `${RouteConfig.TIMELAPSE}/setting/:hash`
    static readonly TIMELAPSE_INFO: string = `${RouteConfig.TIMELAPSE}/info/:hash`
    static readonly TIMELAPSE_MACHINE: string = `${RouteConfig.TIMELAPSE}/machine/:hash`
    static readonly TIMELAPSE_VIEW_TAB: string = `${RouteConfig.TIMELAPSE}/view/:hash/:tab`
    static readonly TIMELAPSE_ANALYTICS: string = `${RouteConfig.TIMELAPSE}/image/analytics/:hash`
    static readonly TIMELAPSE_RENDER_VIDEO: string = `${RouteConfig.TIMELAPSE}/e-rv/:hash`

    static readonly VR360: string = "/vr360"
    static readonly VR360_VIEW: string = `${RouteConfig.VR360}/view/:hash`
    static readonly VR360_SETTING: string = `${RouteConfig.VR360}/setting/:hash`

    static readonly GEODETIC: string = "/geodetic"
    static readonly GEODETIC_INFO: string = `${RouteConfig.GEODETIC}/info/:hash`
    static readonly GEODETIC_MANAGER: string = `${RouteConfig.GEODETIC}/manager`
    static readonly GEODETIC_MANAGER_UPLOAD: string = `${RouteConfig.GEODETIC_MANAGER}/upload`
    static readonly GEODETIC_MANAGER_DELETED: string = `${RouteConfig.GEODETIC_MANAGER}/deleted`
    static readonly GEODETIC_SETTING: string = `${RouteConfig.GEODETIC}/setting/:hash`
    static readonly GEODETIC_VIEW_2D: string = `${RouteConfig.GEODETIC}/m2d/:hash`
    static readonly GEODETIC_VIEW_PC3D: string = `${RouteConfig.GEODETIC}/pc3d/:hash`
    static readonly GEODETIC_VIEW_TM3D: string = `${RouteConfig.GEODETIC}/tm3d/:hash`
    static readonly GEODETIC_VIEW_VR360: string = `${RouteConfig.GEODETIC}/vr360/:hash`

    static readonly CAMERA: string = "/camera"
    static readonly WEIGHING: string = "/weighing"
    static readonly BLOG: string = "/blog"
    static readonly BLOG_POST = `${RouteConfig.BLOG}/post/:hash`
    static readonly ABOUT: string = "/about"
    static readonly FEEDBACK: string = "/feedback"

    // Account
    static ACCOUNT: string = "/account"
    static readonly ME_INFO: string = `${RouteConfig.ACCOUNT}/info`
    static readonly ME_SETTINGS: string = `${RouteConfig.ACCOUNT}/settings`

    static readonly NotFound: string = "*"

    static routesNotLayout: IRco[] = [
        {
            path: RouteConfig.LOGIN,
            component: LoginScreen,
            protect: false
        },
        {
            path: RouteConfig.HOME,
            component: HomeScreen,
            protect: true
        },
        {
            path: RouteConfig.DOWNLOAD,
            component: DownloadScreen,
            protect: false
        },
        {
            path: RouteConfig.REGISTER,
            component: RegisterTest,
            protect: false
        },
        {
            path: RouteConfig.FORGOT_PASSWORD,
            component: ForgotPasswordScreen,
            protect: false
        },
        // Account
        {
            path: RouteConfig.ME_INFO,
            component: MeScreen,
            protect: true
        }
    ];

    static routesInLayout: IRco[] = [
        {
            path: RouteConfig.TIMELAPSE,
            component: TimelapseProjectScreen,
            protect: true
        },
        {
            path: RouteConfig.TIMELAPSE_MACHINE,
            component: TimelapseMachineScreen,
            protect: true
        },
        {
            path: RouteConfig.TIMELAPSE_VIEW_TAB,
            component: TimelapseViewTabScreen,
            protect: true
        },
        {
            path: RouteConfig.TIMELAPSE_SETTING,
            component: TimelapseSettingScreen,
            protect: true
        },
        {
            path: RouteConfig.TIMELAPSE_INFO,
            component: TimelapseProjectInfoScreen,
            protect: true
        },
        {
            path: RouteConfig.ABOUT,
            component: AboutScreen,
            protect: true
        },
        {
            path: RouteConfig.FEEDBACK,
            component: FeedbackScreen,
            protect: true
        },
        {
            path: RouteConfig.CAMERA,
            component: CameraScreen,
            protect: true
        },
        {
            path: RouteConfig.WEIGHING,
            component: WeighingScreen,
            protect: true
        },
        {
            path: RouteConfig.VR360,
            component: Vr360Screen,
            protect: true
        },
        {
            path: RouteConfig.VR360_SETTING,
            component: Vr360SettingScreen,
            protect: true
        },
        {
            path: RouteConfig.GEODETIC,
            component: GeodeticScreen,
            protect: true
        },
        {
            path: RouteConfig.GEODETIC_INFO,
            component: GeodeticInfoScreen,
            protect: true
        },
        {
            path: RouteConfig.GEODETIC_SETTING,
            component: GeodeticSettingScreen,
            protect: true
        },
        {
            path: RouteConfig.GEODETIC_MANAGER,
            component: GeodeticManagerScreen,
            protect: true
        },
        {
            path: RouteConfig.GEODETIC_MANAGER_UPLOAD,
            component: GeodeticManagerScreenUpload,
            protect: true
        },
        {
            path: RouteConfig.GEODETIC_MANAGER_DELETED,
            component: GeodeticManagerScreenDeleted,
            protect: true
        },
        {
            path: RouteConfig.BLOG,
            component: BlogScreen,
            protect: true
        },
        {
            path: RouteConfig.BLOG_POST,
            component: PostScreen,
            protect: true
        },
        {
            path: RouteConfig.ME_SETTINGS,
            component: MeSettingSiderScreen,
            protect: true
        }
    ];

    static routes: IRco[] = [
        {
            path: RouteConfig.EXAMPLE,
            component: Example,
            protect: false
        },
        {
            path: RouteConfig.VR360_VIEW,
            component: Vr360ViewScreen,
            protect: true
        },
        {
            path: RouteConfig.GEODETIC_VIEW_2D,
            component: GeodeticView2DScreen,
            protect: true
        },
        {
            path: RouteConfig.GEODETIC_VIEW_PC3D,
            component: GeodeticViewPC3DScreen,
            protect: true
        },
        {
            path: RouteConfig.GEODETIC_VIEW_TM3D,
            component: GeodeticViewTM3DScreen,
            protect: true
        },
        {
            path: RouteConfig.GEODETIC_VIEW_VR360,
            component: GeodeticViewVr360Screen,
            protect: true
        },

        // Electron Render Video
        {
            path: RouteConfig.TIMELAPSE_RENDER_VIDEO,
            component: TimelapseRenderVideoElectron,
            protect: true
        },

        // Error 404 not found
        {
            path: RouteConfig.NotFound,
            component: NotFoundScreen,
            protect: false
        },

        // Test Screen
        {
            path: '/test/agent',
            component: TestAgentScreen,
            protect: false
        },
        {
            path: '/test/big_up',
            component: TestBigUpScreen,
            protect: true
        },
        {
            path: '/test/env',
            component: TestEnvScreen,
            protect: true
        },
        {
            path: '/test/worker',
            component: TestWorkerScreen,
            protect: true
        },
        {
            path: '/test/temp',
            component: TestTempScreen,
            protect: true
        },
        {
            path: '/test/vr',
            component: TestVrScreen,
            protect: true
        }
    ]
}
