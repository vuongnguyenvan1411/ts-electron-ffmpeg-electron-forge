import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {ITState} from "./ITState";
import {SendingStatus} from "../../const/Events";
import {ApiService} from "../../repositories/ApiService";
import {InitModel, TrackingModel} from "../../models/ITModel";
import {useInjection} from "inversify-react";

export const ITAction = () => {
    const [state, setState] = useRecoilState(ITState)
    const resetState = useResetRecoilState(ITState)
    const vm = useRecoilValue(ITState)
    const apiService = useInjection(ApiService)

    const onInit = () => {
        setState({
            ...state,
            isLoading: SendingStatus.loading
        })

        apiService
            .init()
            .then(r => {
                if (r.success) {
                    setState({
                        ...state,
                        init: r.data && new InitModel(r.data),
                        isLoading: SendingStatus.complete
                    })
                } else {
                    setState({
                        ...state,
                        error: r.error,
                        isLoading: SendingStatus.complete
                    })
                }
            })
            .catch(err => {
                setState({
                    ...state,
                    error: {
                        warning: err
                    },
                    isLoading: SendingStatus.complete
                })
            })
    }

    const onTracking = () => {
        apiService
            .tracking()
            .then(r => {
                if (r.success) {
                    setState({
                        ...state,
                        tracking: r.data && new TrackingModel(r.data),
                    })
                }
            })
    }

    const onClearInit = () => {
        setState({
            ...state,
            init: undefined
        })
    }

    return {
        vm,
        onInit,
        onTracking,
        onClearInit,
        onClearState: resetState
    }
}
