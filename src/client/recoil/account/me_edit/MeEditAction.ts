import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {MeEditState} from "./MeEditState";
import {SendingStatus} from "../../../const/Events";
import {AxiosClient} from "../../../repositories/AxiosClient";
import {ApiService} from "../../../repositories/ApiService";
import {TMeInfoV0} from "../../../models/UserModel";
import {setErrorHandled} from "../../CmAction";
import {useSessionContext} from "../../../presentation/contexts/SessionContext";
import {MeAction} from "../me/MeAction";

export const MeEditAction = () => {
    const [state, setState] = useRecoilState(MeEditState)
    const resetState = useResetRecoilState(MeEditState)
    const vm = useRecoilValue(MeEditState)

    const [session, setSession] = useSessionContext()

    const {
        onStoreUser
    } = MeAction()

    const onUpdateInfo = (data: TMeInfoV0) => {
        setState({
            ...state,
            isUpdating: SendingStatus.loading
        })

        AxiosClient
            .put(ApiService.editInfoMe, data)
            .then(r => {
                if (r.success) {
                    setState({
                        ...state,
                        isUpdating: SendingStatus.success,
                    })

                    const user = session.user

                    if (user) {
                        const data = r.data

                        if (data?.hasOwnProperty('name')) {
                            user.name = data.name
                        }

                        if (data?.hasOwnProperty('email')) {
                            user.email = data.email
                        }

                        if (data?.hasOwnProperty('address')) {
                            user.address = data.address
                        }

                        if (data?.hasOwnProperty('telephone')) {
                            user.telephone = data.telephone
                        }

                        onStoreUser(user, true, false)

                        setSession({
                            ...session,
                            user: user
                        })
                    }
                } else {
                    setState({
                        ...state,
                        isUpdating: SendingStatus.error,
                        error: r.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isUpdating', err))
    }

    return {
        vm,
        onUpdateInfo,
        onClearState: resetState
    }
}
