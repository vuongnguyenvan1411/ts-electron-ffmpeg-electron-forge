import {SendingStatus} from "../../../const/Events";
import {atom} from "recoil";
import {KeyMeEdit} from "../../KeyRecoil";

type TMeEditState = {
    isUpdating: SendingStatus
    error?: Record<string, any>
}

export const initialState: TMeEditState = {
    isUpdating: SendingStatus.idle
}

export const MeEditState = atom<TMeEditState>({
    key: KeyMeEdit,
    default: initialState
})
