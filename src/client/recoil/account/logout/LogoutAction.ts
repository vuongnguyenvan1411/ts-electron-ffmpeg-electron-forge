import {initialState, TLogoutState} from "./LogoutState";
import {SendingStatus} from "../../../const/Events";
import {ApiService} from "../../../repositories/ApiService";
import {setErrorHandled} from "../../CmAction";
import {useSessionContext} from "../../../presentation/contexts/SessionContext";
import {MeAction} from "../me/MeAction";
import {useInjection} from "inversify-react";
import {useState} from "react";

export const LogoutAction = () => {
    const [state, setState] = useState<TLogoutState>(initialState)

    const [session, setSession] = useSessionContext()
    const apiService = useInjection(ApiService)

    const {
        onClearUser
    } = MeAction()

    const onLogout = () => {
        setState({
            ...state,
            status: SendingStatus.loading
        })

        apiService
            .logout()
            .then(r => {
                if (r.success) {
                    // remove store user
                    onClearUser()

                    setSession({
                        ...session,
                        isAuthenticated: false,
                        user: undefined,
                        redirectPath: ''
                    })

                    setState({
                        ...state,
                        status: SendingStatus.success
                    })
                } else {
                    setState({
                        ...state,
                        status: SendingStatus.error,
                        error: r.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'status', err))
    }

    const resetState = () => {
        setState(initialState)
    }

    return {
        vm: state,
        onLogout,
        onClearState: resetState
    }
}
