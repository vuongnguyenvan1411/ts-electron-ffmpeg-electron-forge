import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {initialState, MeState} from "./MeState";
import {E_CookieKey, E_LSKey, SendingStatus} from "../../../const/Events";
import {ApiService} from "../../../repositories/ApiService";
import {UserModel} from "../../../models/UserModel";
import {setErrorHandled} from "../../CmAction";
import {useSessionContext} from "../../../presentation/contexts/SessionContext";
import {Utils} from "../../../core/Utils";
import {StoreConfig} from "../../../config/StoreConfig";
import {useInjection} from "inversify-react";
import {EDLocal} from "../../../core/encrypt/EDLocal";

export const MeAction = () => {
    const [state, setState] = useRecoilState(MeState)
    const resetState = useResetRecoilState(MeState)
    const vm = useRecoilValue(MeState)

    const [session, setSession] = useSessionContext()
    const apiService = useInjection(ApiService)
    const storeConfig = useInjection(StoreConfig)

    const onLoadMe = () => {
        setState({
            ...state,
            isLoading: SendingStatus.loading
        })

        apiService
            .getMe()
            .then(r => {
                if (r.success) {
                    setState({
                        ...state,
                        isLoading: SendingStatus.success,
                        user: new UserModel(r.data)
                    })
                } else {
                    setState({
                        ...state,
                        isLoading: SendingStatus.error,
                        error: r.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isLoading', err))
    }

    const onUpdateMeImage = (data: Record<string, any>) => {
        if (!state.user) {
            return
        }

        const user = state.user;

        if (data.hasOwnProperty('image')) {
            user.image = data.image;
        }

        if (data.hasOwnProperty('background')) {
            user.background = data.background;
        }

        onStoreUser(user, true, false)

        setSession({
            ...session,
            user: user
        })

        setState({
            ...state,
            user: user
        })
    }

    const onStoreUser = (user: UserModel, isLS: boolean = true, isCookie: boolean = true) => {
        storeConfig.accessToken = user.accessToken

        // set localStorage
        if (isLS) {
            EDLocal.setLocalStore(E_LSKey.User, Utils.rmObjectByValue(user.toObject()))
        }

        // set cookie
        if (isCookie) {
            if (user.accessToken) {
                EDLocal.setCookie(E_CookieKey.User, Utils.rmObjectByValue(user.accessToken.toObject()))
            }
        }
    }

    const onClearUser = () => {
        console.log("MeAction: onClearUser")

        // clear store singleton
        storeConfig.accessToken = undefined

        // remove localStorage
        EDLocal.removeLocalStore('user')

        // remove cookie
        EDLocal.removeCookie('user')

        // clear recoil
        setState({
            ...state,
            ...initialState,
            user: undefined,
            error: undefined
        })
    }

    return {
        vm,
        onLoadMe,
        onUpdateMeImage,
        onStoreUser,
        onClearUser,
        onClearState: resetState
    }
}
