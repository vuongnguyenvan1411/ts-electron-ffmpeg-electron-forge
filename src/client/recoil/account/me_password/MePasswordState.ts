import {SendingStatus} from "../../../const/Events";
import {atom} from "recoil";
import {KeyMePassword} from "../../KeyRecoil";

type TMePasswordState = {
    isUpdating: SendingStatus
    error?: Record<string, any>
}

export const initialState: TMePasswordState = {
    isUpdating: SendingStatus.idle
}

export const MePasswordState = atom<TMePasswordState>({
    key: KeyMePassword,
    default: initialState
})
