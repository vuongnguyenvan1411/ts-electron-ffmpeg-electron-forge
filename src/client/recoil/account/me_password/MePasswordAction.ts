import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {MePasswordState} from "./MePasswordState";
import {SendingStatus} from "../../../const/Events";
import {AxiosClient} from "../../../repositories/AxiosClient";
import {ApiService} from "../../../repositories/ApiService";
import {TMePasswordVO} from "../../../models/UserModel";
import {setErrorHandled} from "../../CmAction";
import {MeAction} from "../me/MeAction";
import {useSessionContext} from "../../../presentation/contexts/SessionContext";

export const MePasswordAction = () => {
    const [state, setState] = useRecoilState(MePasswordState)
    const resetState = useResetRecoilState(MePasswordState)
    const vm = useRecoilValue(MePasswordState)

    const [session, setSession] = useSessionContext()

    const {
        onStoreUser
    } = MeAction()

    const onInitialStatus = () => {
        setState({...state, isUpdating: SendingStatus.none})
    }

    const onUpdatePassword = (data: TMePasswordVO) => {
        setState({
            ...state,
            isUpdating: SendingStatus.loading
        })

        AxiosClient
            .put(ApiService.editPasswordMe, data)
            .then(r => {
                if (r.success) {
                    const data = r.data

                    if (data?.hasOwnProperty('accessToken')) {
                        const user = session.user

                        if (user) {
                            user.setAccessToken(data.accessToken)

                            onStoreUser(user)

                            setSession({
                                ...session,
                                user: user
                            })
                        }
                    }

                    setState({
                        ...state,
                        isUpdating: SendingStatus.success,
                    })
                } else {
                    setState({
                        ...state,
                        isUpdating: SendingStatus.error,
                        error: r.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isUpdating', err))
    }

    return {
        vm,
        onInitialStatus,
        onUpdatePassword,
        onClearState: resetState
    }
}
