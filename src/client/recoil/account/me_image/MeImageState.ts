import {SendingStatus} from "../../../const/Events";
import {atom} from "recoil";
import {KeyMeImage} from "../../KeyRecoil";

type TMeImageState = {
    isUpdating: SendingStatus
    error?: Record<string, any>
}

export const initialState: TMeImageState = {
    isUpdating: SendingStatus.idle
}

export const MeImageState = atom<TMeImageState>({
    key: KeyMeImage,
    default: initialState
})
