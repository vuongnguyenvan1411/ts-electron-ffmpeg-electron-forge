import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {MeImageState} from "./MeImageState";
import {SendingStatus} from "../../../const/Events";
import {AxiosClient} from "../../../repositories/AxiosClient";
import {ApiService} from "../../../repositories/ApiService";
import {TMeImageV0} from "../../../models/UserModel";
import {setErrorHandled} from "../../CmAction";
import {MeAction} from "../me/MeAction";

export const MeImageAction = () => {
    const [state, setState] = useRecoilState(MeImageState)
    const resetState = useResetRecoilState(MeImageState)
    const vm = useRecoilValue(MeImageState)

    const {
        onUpdateMeImage
    } = MeAction()

    const onUpdateImage = (data: TMeImageV0) => {
        setState({
            ...state,
            isUpdating: SendingStatus.loading
        })

        const formData = new FormData()
        formData.append('file', data.image.slice())

        AxiosClient
            .post(ApiService.uploadImage(data.type), formData, true)
            .then(r => {
                if (r.success && r.data) {
                    setState({
                        ...state,
                        isUpdating: SendingStatus.success,
                    })

                    onUpdateMeImage(r.data)
                } else {
                    setState({
                        ...state,
                        isUpdating: SendingStatus.error,
                        error: r.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isUpdating', err))
    }

    return {
        vm,
        onUpdateImage,
        onClearState: resetState
    }
}
