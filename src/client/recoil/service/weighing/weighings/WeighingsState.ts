import {atom} from "recoil";
import {SendingStatus} from "../../../../const/Events";
import {KeyWeighings} from "../../../KeyRecoil";
import {InfoEmptyModel, PaginateLinksModel, PaginateMetaModel} from "../../../../models/ApiResModel";
import {WeighingModel} from "../../../../models/service/weighing/WeighingModel";

export type TWeighingsState = {
    items: WeighingModel[]
    isLoading: SendingStatus
    infoEmptyModel?: InfoEmptyModel
    oMeta?: PaginateMetaModel
    oLinks?: PaginateLinksModel
    query: {
        page: number
        limit: number
    }
    timestamp?: number
    error?: Record<string, any>
    cacheSearch: Map<any, any>
}

export const initialState: TWeighingsState = {
    items: [],
    isLoading: SendingStatus.idle,
    query: {
        page: 1,
        limit: 12
    },
    cacheSearch: new Map(),
}

export const WeighingsState = atom<TWeighingsState>({
    key: KeyWeighings,
    default: initialState
})
