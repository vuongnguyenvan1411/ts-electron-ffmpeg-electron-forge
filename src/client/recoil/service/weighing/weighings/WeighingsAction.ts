import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {SendingStatus} from "../../../../const/Events";
import {AxiosClient} from "../../../../repositories/AxiosClient";
import {ApiService} from "../../../../repositories/ApiService";
import {setErrorHandled} from "../../../CmAction";
import moment from "moment";
import {PaginateMetaModel} from "../../../../models/ApiResModel";
import {Color} from "../../../../const/Color";
import {StoreRecoilSingleton} from "../../../StoreRecoilSingleton";
import {initialState, WeighingsState} from "./WeighingsState";
import {WeighingModel} from "../../../../models/service/weighing/WeighingModel";

export interface IWeighingFilterVO {
    page?: number;
    limit?: number;
    sort?: string;
    order?: string;
    filter?: {
        q?: string,
    };
}

export const WeighingsAction = () => {
    const _store = StoreRecoilSingleton.getInstance();
    const [state, setState] = useRecoilState(WeighingsState)
    const resetState = useResetRecoilState(WeighingsState)
    const vm = useRecoilValue(WeighingsState)
    _store.resetWeighing["WeighingsAction"] = resetState;

    const onLoadItems = (query?: IWeighingFilterVO, isCache = false) => {
        setState({
            ...state,
            isLoading: SendingStatus.loading
        })

        AxiosClient
            .get(ApiService.resWeighing("s"), query)
            .then(r => {
                if (r.success) {
                    let merge = state

                    const page = r.meta ? r.meta.currentPage : 1;

                    if (query) {
                        if (
                            (query.filter && Object.keys(query.filter).length > 0)
                            || (query.sort && query.sort.length > 0)
                            || (query.order && query.order.length > 0)
                        ) {
                            if (page === 1) {
                                merge = {
                                    ...merge,
                                    items: []
                                }

                                console.log('%cReset Item When Filter: WeighingsAction', Color.ConsoleInfo);
                            }
                        }
                    }

                    if (r.isEmpty && r.information) {
                        merge = {
                            ...merge,
                            infoEmptyModel: r.information,
                        }
                    }

                    if (r.meta instanceof PaginateMetaModel) {
                        merge = {
                            ...merge,
                            oMeta: r.meta
                        }

                        const limit = r.meta.perPage;

                        if (limit) {
                            if (merge.query.limit > limit || merge.query.limit !== limit) {
                                merge = {
                                    ...merge,
                                    query: {
                                        ...merge.query,
                                        limit: limit
                                    }
                                }
                            }
                        }
                    }

                    if (r.items) {
                        merge.items = [...state.items]

                        r.items.map((value, index) => merge.items[((page - 1) * merge.query.limit) + index] = new WeighingModel(value))
                    }

                    if (page === 1) {
                        merge = {
                            ...merge,
                            timestamp: moment().unix()
                        }
                    }

                    if (isCache && query?.filter?.q)
                        merge.cacheSearch.set(query.filter.q, {
                            items: merge.items,
                            oMeta: merge.oMeta,
                            query: merge.query,
                            isLoading: SendingStatus.success,
                        })

                    setState({
                        ...merge,
                        isLoading: SendingStatus.success,
                    })
                } else {
                    setState({
                        ...state,
                        error: r.error,
                        isLoading: SendingStatus.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isLoading', err))
    }

    const onCacheItems = (query?: IWeighingFilterVO) => {
        if (query?.filter?.q && query.filter?.q.length > 0 && state.cacheSearch.has(query.filter.q)) {
            console.log('%cGet value from cache of Weighing by key: ' + query.filter.q, "color: DodgerBlue");

            const _cacheData: {
                items: WeighingModel[],
                oMeta: PaginateMetaModel,
                query: {
                    page: number
                    limit: number
                },
                isLoading: SendingStatus,
            } = state.cacheSearch.get(query.filter.q);

            console.log("cache data: ", _cacheData)

            setState({
                ...state,
                items: _cacheData.items,
                oMeta: _cacheData.oMeta,
                isLoading: _cacheData.isLoading,
            })
        } else {
            onLoadItems(query, true);
        }
    }

    const resetStateWithEffect = () => {
        setState(initialState);
    }

    return {
        vm,
        onLoadItems,
        onCacheItems,
        onClearState: resetState,
        resetStateWithEffect,
    }
}
