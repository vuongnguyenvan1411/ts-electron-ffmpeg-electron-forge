import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {initialState, Vr360sState} from "./Vr360sState";
import {SendingStatus} from "../../../../const/Events";
import {AxiosClient} from "../../../../repositories/AxiosClient";
import {ApiService} from "../../../../repositories/ApiService";
import {setErrorHandled} from "../../../CmAction";
import moment from "moment";
import {PaginateMetaModel} from "../../../../models/ApiResModel";
import {Color} from "../../../../const/Color";
import {Vr360FilterVO, Vr360Model} from "../../../../models/Vr360Model";
import {StoreRecoilSingleton} from "../../../StoreRecoilSingleton";

export const Vr360sAction = () => {
    const _store = StoreRecoilSingleton.getInstance();
    const [state, setState] = useRecoilState(Vr360sState)
    const resetState = useResetRecoilState(Vr360sState)
    const vm = useRecoilValue(Vr360sState)
    _store.resetVr360["Vr360sAction"] = resetState;

    const onLoadItems = (query?: Vr360FilterVO, isCache = false) => {
        setState({
            ...state,
            isLoading: SendingStatus.loading
        })

        AxiosClient
            .get(ApiService.resVr360("s"), query)
            .then(r => {
                if (r.success) {
                    let merge = state

                    const page = r.meta ? r.meta.currentPage : 1;

                    if (query) {
                        if (
                            (query.filter && Object.keys(query.filter).length > 0)
                            || (query.sort && query.sort.length > 0)
                            || (query.order && query.order.length > 0)
                        ) {
                            if (page === 1) {
                                merge = {
                                    ...merge,
                                    items: []
                                }

                                console.log('%cReset Item When Filter: Vr360sAction', Color.ConsoleInfo);
                            }
                        }
                    }

                    if (r.meta instanceof PaginateMetaModel) {
                        merge = {
                            ...merge,
                            oMeta: r.meta
                        }

                        const limit = r.meta.perPage;

                        if (limit) {
                            if (merge.query.limit > limit || merge.query.limit !== limit) {
                                merge = {
                                    ...merge,
                                    query: {
                                        ...merge.query,
                                        limit: limit
                                    }
                                }
                            }
                        }
                    }

                    if (r.items) {
                        merge.items = [...state.items]

                        r.items.map((value, index) => merge.items[((page - 1) * merge.query.limit) + index] = new Vr360Model(value))
                    }

                    if (page === 1) {
                        merge = {
                            ...merge,
                            timestamp: moment().unix()
                        }
                    }

                    if (isCache && query?.filter?.q)
                        merge.memoizeSearch.cache.set(query.filter.q, {
                            items: merge.items,
                            oMeta: merge.oMeta,
                            query: merge.query,
                            isLoading: SendingStatus.success,
                        })

                    setState({
                        ...merge,
                        isLoading: SendingStatus.success,
                    })
                } else {
                    setState({
                        ...state,
                        error: r.error,
                        isLoading: SendingStatus.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isLoading', err))
    }

    const onCacheItems = (query?: Vr360FilterVO) => {
        if (query?.filter?.q && query.filter?.q.length > 0 && state.memoizeSearch.cache.has(query.filter.q)) {
            console.log('%cGet value from cache of Vr360 by key: ' + query.filter.q, "color: DodgerBlue");

            const _cacheData: {
                items: Vr360Model[],
                oMeta: PaginateMetaModel,
                query: {
                    page: number
                    limit: number
                },
                isLoading: SendingStatus,
            } = state.memoizeSearch.cache.get(query.filter.q);

            console.log("cache data: ", _cacheData)

            setState({
                ...state,
                items: _cacheData.items,
                oMeta: _cacheData.oMeta,
                isLoading: _cacheData.isLoading,
            })
        } else {
            onLoadItems(query, true);
        }
    }

    const resetStateWithEffect = () => {
        setState(initialState);
    }

    return {
        vm,
        onLoadItems,
        onCacheItems,
        onClearState: resetState,
        resetStateWithEffect,
    }
}
