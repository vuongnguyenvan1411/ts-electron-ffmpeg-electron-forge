import {atom} from "recoil";
import {SendingStatus} from "../../../../const/Events";
import {KeyVr360s} from "../../../KeyRecoil";
import {PaginateLinksModel, PaginateMetaModel} from "../../../../models/ApiResModel";
import {Vr360Model} from "../../../../models/Vr360Model";
import {memoize, MemoizedFunction, values} from "lodash";

export type TVr360sState = {
    items: Vr360Model[]
    isLoading: SendingStatus
    oMeta?: PaginateMetaModel
    oLinks?: PaginateLinksModel
    query: {
        page: number
        limit: number
    }
    timestamp?: number
    error?: Record<string, any>
    memoizeSearch: MemoizedFunction
}

export const initialState: TVr360sState = {
    items: [],
    isLoading: SendingStatus.idle,
    query: {
        page: 1,
        limit: 12
    },
    memoizeSearch: memoize(values),
}

export const Vr360sState = atom<TVr360sState>({
    key: KeyVr360s,
    default: initialState
})
