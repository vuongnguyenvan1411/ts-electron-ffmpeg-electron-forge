import {atom} from "recoil";
import {SendingStatus} from "../../../../const/Events";
import {KeyVr360Setting} from "../../../KeyRecoil";
import {PaginateLinksModel, PaginateMetaModel} from "../../../../models/ApiResModel";
import {Vr360ShareModel} from "../../../../models/ShareModel";

export type TVr360SettingState = {
    key?: string
    items: Vr360ShareModel[]
    isLoading: SendingStatus
    isUpdating: SendingStatus
    isDeleting: SendingStatus
    oMeta?: PaginateMetaModel
    oLink?: PaginateLinksModel
    query: {
        page: number,
        limit: number
    };
    error?: Record<string, any>
    timestamp?: number
}

export const initialState: TVr360SettingState = {
    items: [],
    isLoading: SendingStatus.idle,
    isUpdating: SendingStatus.idle,
    isDeleting: SendingStatus.idle,
    query: {
        page: 1,
        limit: 12
    }
}

export const Vr360SettingState = atom<TVr360SettingState>({
    key: KeyVr360Setting,
    default: initialState
})
