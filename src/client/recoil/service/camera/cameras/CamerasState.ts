import {atom} from "recoil";
import {SendingStatus} from "../../../../const/Events";
import {KeyCameras} from "../../../KeyRecoil";
import {InfoEmptyModel, PaginateLinksModel, PaginateMetaModel} from "../../../../models/ApiResModel";
import {CameraModel} from "../../../../models/service/camera/CameraModel";

export type TCamerasState = {
    items: CameraModel[]
    isLoading: SendingStatus
    infoEmptyModel?: InfoEmptyModel
    oMeta?: PaginateMetaModel
    oLinks?: PaginateLinksModel
    query: {
        page: number
        limit: number
    }
    timestamp?: number
    error?: Record<string, any>
    cacheSearch: Map<any, any>
}

export const initialState: TCamerasState = {
    items: [],
    isLoading: SendingStatus.idle,
    query: {
        page: 1,
        limit: 12
    },
    cacheSearch: new Map(),
}

export const CamerasState = atom<TCamerasState>({
    key: KeyCameras,
    default: initialState
})
