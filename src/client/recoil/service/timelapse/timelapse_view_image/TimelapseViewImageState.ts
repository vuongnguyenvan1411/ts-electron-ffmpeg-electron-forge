import {atom} from "recoil";
import {SendingStatus} from "../../../../const/Events";
import {KeyTimelapseViewImage} from "../../../KeyRecoil";
import {PaginateLinksModel, PaginateMetaModel} from "../../../../models/ApiResModel";
import {TimelapseImageModel} from "../../../../models/service/timelapse/TimelapseImageModel";

export type TTimelapseViewImageState = {
    key?: string,
    isNextAvailable: boolean
    items: TimelapseImageModel[]
    isLoading: SendingStatus
    isLoadMore: SendingStatus
    oMeta?: PaginateMetaModel
    oLinks?: PaginateLinksModel
    query: {
        page: number
        limit: number
    }
    timestamp?: number
    error?: Record<string, any>
}

export const initialState: TTimelapseViewImageState = {
    items: [],
    isNextAvailable: false,
    isLoading: SendingStatus.idle,
    isLoadMore: SendingStatus.idle,
    query: {
        page: 1,
        limit: 12
    }
}

export const TimelapseViewImageState = atom<TTimelapseViewImageState>({
    key: KeyTimelapseViewImage,
    default: initialState
})
