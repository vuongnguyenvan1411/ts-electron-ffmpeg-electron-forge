import {atom, useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {initialState, TimelapseViewImageState, TTimelapseViewImageState} from "./TimelapseViewImageState";
import {SendingStatus} from "../../../../const/Events";
import {AxiosClient} from "../../../../repositories/AxiosClient";
import {ApiService} from "../../../../repositories/ApiService";
import {setErrorHandled} from "../../../CmAction";
import moment from "moment";
import {PaginateMetaModel} from "../../../../models/ApiResModel";
import {TimelapseImageFilterVO, TimelapseImageModel} from "../../../../models/service/timelapse/TimelapseImageModel";
import {KeyTimelapseViewImageLightGallery} from "../../../KeyRecoil";
import {StoreRecoilSingleton} from "../../../StoreRecoilSingleton";

export const TimelapseViewImageAction = () => {
    const _store = StoreRecoilSingleton.getInstance();
    const [state, setState] = useRecoilState<TTimelapseViewImageState>(TimelapseViewImageState)
    const resetState = useResetRecoilState(TimelapseViewImageState)
    const vm = useRecoilValue(TimelapseViewImageState)
    _store.resetTimelapse["TimelapseViewImageAction"] = resetState;

    const onLoadItems = (machineId: number, query?: TimelapseImageFilterVO, val?: TTimelapseViewImageState) => {
        const _state = val ?? state;

        setState({
            ..._state,
            isLoading: SendingStatus.loading,
        })

        AxiosClient
            .get(ApiService.getTimelapseImages(machineId), query)
            .then(r => {
                // console.log('ssssss ',r.success)
                if (r.success) {
                    let merge = {..._state}

                    if(query?.page?.toString() === '1'){
                        merge.items = []
                    }

                    if (r.meta instanceof PaginateMetaModel) {
                        const page = r.meta.currentPage

                        // Reset items when page = 1
                        if (page === 1) {
                            merge.items = []
                        } else {
                            merge.items = [..._state.items]
                        }

                        if (r.items) {
                            r.items.map(value => merge.items.push(new TimelapseImageModel(value)))
                        }

                        merge.isNextAvailable = r.meta.nextPage !== undefined;

                        const limit = r.meta.perPage;

                        if (limit) {
                            if (merge.query.limit > limit || merge.query.limit !== limit) {
                                merge = {
                                    ...merge,
                                    query: {
                                        ...merge.query,
                                        limit: limit
                                    }
                                }
                            }
                        }

                        merge = {
                            ...merge,
                            oMeta: r.meta
                        }

                        if (page === 1) {
                            merge = {
                                ...merge,
                                key: machineId.toString(),
                                timestamp: moment().unix()
                            }
                        }
                    }else{
                        merge = {
                            ...merge,
                            oLinks:r.links,
                            oMeta: r.meta,
                            key: machineId.toString(),
                            timestamp: moment().unix()
                        }
                    }

                    setState({
                        ...merge,
                        isLoading: SendingStatus.success,
                    })
                } else {
                    setState({
                        ..._state,
                        error: r.error,
                        isLoading: SendingStatus.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isLoading', err))
    }

    const onCacheItems = (data: {
        items: TimelapseImageModel[],
        oMeta: PaginateMetaModel,
        query: {
            page: number
            limit: number
        }
    }) => {
        setState({
            ...state,
            items: data.items,
            oMeta: data.oMeta
        })
    }

    const resetStateWithEffect = (updateState?: TTimelapseViewImageState, callBack?: (val: TTimelapseViewImageState) => void) => {
        setState(updateState ?? initialState)
        if (callBack) {
            callBack(updateState ?? initialState);
        }
    }

    return {
        vm,
        onLoadItems,
        onCacheItems,
        onClearState: resetState,
        resetStateWithEffect,
    }
}

type TLGState = {
    index?: number
}

const currentLightGalleryState = atom<TLGState>({
    key: KeyTimelapseViewImageLightGallery,
    default: {}
})

export const TimelapseViewImageLightGalleryAction = () => {
    const [state, setState] = useRecoilState<TLGState>(currentLightGalleryState)
    const resetState = useResetRecoilState(currentLightGalleryState)
    const vm = useRecoilValue(currentLightGalleryState)

    const onChangeCurrentIndex = (index: number) => {
        setState({
            ...state,
            index: index
        })
    }

    return {
        vm,
        onChangeCurrentIndex,
        onClearState: resetState
    }
}
