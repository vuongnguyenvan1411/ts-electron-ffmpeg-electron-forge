import {initialState, TTimelapseViewVideoAutoState} from "./TimelapseViewVideoAutoState";
import {SendingStatus} from "../../../../const/Events";
import {AxiosClient} from "../../../../repositories/AxiosClient";
import {ApiService} from "../../../../repositories/ApiService";
import {setErrorHandled} from "../../../CmAction";
import moment from "moment";
import {PaginateMetaModel} from "../../../../models/ApiResModel";
import {TimelapseVideoFilterVO, TimelapseVideoModel} from "../../../../models/service/timelapse/TimelapseVideoModel";
import {useState} from "react";

export const TimelapseViewVideoAutoAction = () => {
    const [state, setState] = useState<TTimelapseViewVideoAutoState>(initialState)

    const onLoadItems = (machineId: number, query?: TimelapseVideoFilterVO) => {
        setState({
            ...state,
            isLoading: SendingStatus.loading
        })

        AxiosClient
            .get(ApiService.getTimelapseVideoAuto(machineId), query)
            .then(r => {
                if (r.success) {
                    let merge = {...state}

                    if (r.meta instanceof PaginateMetaModel) {
                        const page = r.meta.currentPage

                        // Reset items when page = 1
                        if (page === 1) {
                            merge.items = []
                        } else {
                            merge.items = [...state.items]
                        }

                        if (r.items) {
                            r.items.map(value => merge.items.push(new TimelapseVideoModel(value)))
                        }

                        merge.isNextAvailable = r.meta.nextPage !== undefined;

                        const limit = r.meta.perPage;

                        if (limit) {
                            if (merge.query.limit > limit || merge.query.limit !== limit) {
                                merge = {
                                    ...merge,
                                    query: {
                                        ...merge.query,
                                        limit: limit
                                    }
                                }
                            }
                        }

                        merge = {
                            ...merge,
                            oMeta: r.meta
                        }

                        if (page === 1) {
                            merge = {
                                ...merge,
                                key: machineId.toString(),
                                timestamp: moment().unix()
                            }
                        }
                    }

                    setState({
                        ...merge,
                        isLoading: SendingStatus.success,
                    })
                } else {
                    setState({
                        ...state,
                        error: r.error,
                        isLoading: SendingStatus.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isLoading', err))
    }

    const resetStateWithEffect = () => {
        setState(initialState);
    }

    const resetState = () => {
        setState(initialState)
    }

    return {
        vm: state,
        onLoadItems,
        onClearState: resetState,
        resetStateWithEffect
    }
}
