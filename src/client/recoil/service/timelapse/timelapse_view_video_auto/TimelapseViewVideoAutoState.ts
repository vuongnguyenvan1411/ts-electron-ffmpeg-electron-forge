import {atom} from "recoil";
import {SendingStatus} from "../../../../const/Events";
import {KeyTimelapseViewVideoAuto} from "../../../KeyRecoil";
import {PaginateLinksModel, PaginateMetaModel} from "../../../../models/ApiResModel";
import {TimelapseVideoModel} from "../../../../models/service/timelapse/TimelapseVideoModel";

export type TTimelapseViewVideoAutoState = {
    key?: string,
    isNextAvailable: boolean
    items: TimelapseVideoModel[]
    isLoading: SendingStatus
    oMeta?: PaginateMetaModel
    oLinks?: PaginateLinksModel
    query: {
        page: number
        limit: number
    }
    timestamp?: number
    error?: Record<string, any>
}

export const initialState: TTimelapseViewVideoAutoState = {
    items: [],
    isNextAvailable: false,
    isLoading: SendingStatus.idle,
    query: {
        page: 1,
        limit: 12
    }
}

export const TimelapseViewVideoAutoState = atom<TTimelapseViewVideoAutoState>({
    key: KeyTimelapseViewVideoAuto,
    default: initialState
})
