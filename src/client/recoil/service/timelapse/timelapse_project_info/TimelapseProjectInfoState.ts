import {atom} from "recoil";
import {SendingStatus} from "../../../../const/Events";
import {KeyTimelapseProjectInfo} from "../../../KeyRecoil";
import {TimelapseProjectInfoModel} from "../../../../models/service/timelapse/TimelapseProjectInfoModel";

export type TTimelapseProjectInfoState = {
    key?: string,
    item?: TimelapseProjectInfoModel
    isLoading: SendingStatus
    timestamp?: number
    error?: Record<string, any>
}

export const initialState: TTimelapseProjectInfoState = {
    isLoading: SendingStatus.idle,
}

export const TimelapseProjectInfoState = atom<TTimelapseProjectInfoState>({
    key: KeyTimelapseProjectInfo,
    default: initialState
})
