import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {initialState, TimelapseProjectInfoState, TTimelapseProjectInfoState} from "./TimelapseProjectInfoState";
import {SendingStatus} from "../../../../const/Events";
import {AxiosClient} from "../../../../repositories/AxiosClient";
import {ApiService} from "../../../../repositories/ApiService";
import {setErrorHandled} from "../../../CmAction";
import moment from "moment";
import {TimelapseProjectInfoModel} from "../../../../models/service/timelapse/TimelapseProjectInfoModel";
import {StoreRecoilSingleton} from "../../../StoreRecoilSingleton";

export const TimelapseProjectInfoAction = () => {
    const _store = StoreRecoilSingleton.getInstance();
    const [state, setState] = useRecoilState<TTimelapseProjectInfoState>(TimelapseProjectInfoState)
    const resetState = useResetRecoilState(TimelapseProjectInfoState)
    const vm = useRecoilValue(TimelapseProjectInfoState)
    _store.resetTimelapse["TimelapseProjectInfoAction"] = resetState;

    const onLoadItem = (projectId: number) => {
        setState({
            ...state,
            isLoading: SendingStatus.loading
        })

        AxiosClient
            .get(ApiService.getTimelapseProject(projectId))
            .then(r => {
                if (r.success) {
                    let merge = {...state}

                    merge.item = new TimelapseProjectInfoModel(r.data)

                    setState({
                        ...merge,
                        isLoading: SendingStatus.success,
                        key: projectId.toString(),
                        timestamp: moment().unix(),
                    })
                } else {
                    setState({
                        ...state,
                        isLoading: SendingStatus.error,
                        error: r.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isLoading', err))
    }

    const resetStateWithEffect = () => {
        setState(initialState);
    }

    return {
        vm,
        onLoadItem,
        onClearState: resetState,
        resetStateWithEffect,
    }
}
