import {atom} from "recoil";
import {SendingStatus} from "../../../../const/Events";
import {KeyTimelapseViewAnalytics} from "../../../KeyRecoil";
import {TimelapseSensorModel} from "../../../../models/service/timelapse/TimelapseSensorModel";

export type TTimelapseViewAnalyticsState = {
    key?: string,
    item?: TimelapseSensorModel
    isLoading: SendingStatus
    timestamp?: number
    error?: Record<string, any>
}

export const initialState: TTimelapseViewAnalyticsState = {
    isLoading: SendingStatus.idle,
}

export const TimelapseViewAnalyticsState = atom<TTimelapseViewAnalyticsState>({
    key: KeyTimelapseViewAnalytics,
    default: initialState
})
