import {initialState, TTimelapseViewAnalyticsState} from "./TimelapseViewAnalyticsState";
import {SendingStatus} from "../../../../const/Events";
import {AxiosClient} from "../../../../repositories/AxiosClient";
import {ApiService} from "../../../../repositories/ApiService";
import {setErrorHandled} from "../../../CmAction";
import moment from "moment";
import {TimelapseVideoFilterVO} from "../../../../models/service/timelapse/TimelapseVideoModel";
import {TimelapseSensorModel} from "../../../../models/service/timelapse/TimelapseSensorModel";
import {useState} from "react";

export const TimelapseViewAnalyticsAction = () => {
    const [state, setState] = useState<TTimelapseViewAnalyticsState>(initialState)

    const onLoadItem = (machineId: number, query?: TimelapseVideoFilterVO) => {
        setState({
            ...state,
            isLoading: SendingStatus.loading
        })

        AxiosClient
            .get(ApiService.getTimelapseAnalytics(machineId), query)
            .then(r => {
                if (r.success) {
                    setState({
                        ...state,
                        item: r.data && new TimelapseSensorModel(r.data),
                        isLoading: SendingStatus.success,
                        key: machineId.toString(),
                        timestamp: moment().unix()
                    })
                } else {
                    setState({
                        ...state,
                        error: r.error,
                        isLoading: SendingStatus.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isLoading', err))
    }

    const resetStateWithEffect = () => {
        setState(initialState);
    }

    const resetState = () => {
        setState(initialState)
    }

    return {
        vm: state,
        onLoadItem,
        onClearState: resetState,
        resetStateWithEffect
    }
}
