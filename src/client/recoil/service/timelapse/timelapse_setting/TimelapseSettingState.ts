import {atom} from "recoil";
import {SendingStatus} from "../../../../const/Events";
import {KeyTimelapseSetting} from "../../../KeyRecoil";
import {PaginateLinksModel, PaginateMetaModel} from "../../../../models/ApiResModel";
import {TimelapseShareModel} from "../../../../models/ShareModel";
import {TSensorModel} from "../../../../models/service/timelapse/TimelapseImageModel";

export type TTimelapseSettingState = {
    key?: string
    items: TimelapseShareModel[]
    sensors?: TSensorModel[]
    isLoading: SendingStatus
    isUpdating: SendingStatus
    isDeleting: SendingStatus
    oMeta?: PaginateMetaModel
    oLink?: PaginateLinksModel
    query: {
        page: number,
        limit: number
    };
    error?: Record<string, any>
    timestamp?: number
}

export const initialState: TTimelapseSettingState = {
    items: [],
    isLoading: SendingStatus.idle,
    isUpdating: SendingStatus.idle,
    isDeleting: SendingStatus.idle,
    query: {
        page: 1,
        limit: 5
    }
}

export const TimelapseSettingState = atom<TTimelapseSettingState>({
    key: KeyTimelapseSetting,
    default: initialState
})
