import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {initialState, TimelapseSettingState} from "./TimelapseSettingState";
import {SendingStatus} from "../../../../const/Events";
import {AxiosClient} from "../../../../repositories/AxiosClient";
import {ApiService} from "../../../../repositories/ApiService";
import {setErrorHandled} from "../../../CmAction";
import moment from "moment";
import {PaginateMetaModel} from "../../../../models/ApiResModel";
import {Color} from "../../../../const/Color";
import {TimelapseShareModel, TShareFilterVO, TTimelapseShareV0} from "../../../../models/ShareModel";
import {TSensorModel} from "../../../../models/service/timelapse/TimelapseImageModel";
import {StoreRecoilSingleton} from "../../../StoreRecoilSingleton";

export const TimelapseSettingAction = () => {
    const _store = StoreRecoilSingleton.getInstance();
    const [state, setState] = useRecoilState(TimelapseSettingState)
    const resetState = useResetRecoilState(TimelapseSettingState)
    const vm = useRecoilValue(TimelapseSettingState)
    _store.resetTimelapse["TimelapseSettingAction"] = resetState;

    const onLoadItems = async (pId: number, query?: TShareFilterVO) => {
        setState({
            ...state,
            isLoading: SendingStatus.loading
        })

        let merge = {...state}

        if (!merge.sensors) {
            const rsSensor = await AxiosClient.get(ApiService.getTimelapseSensors(pId));

            if (rsSensor.success) {
                if (rsSensor.items && rsSensor.items.length > 0) {
                    merge.sensors = [];

                    rsSensor.items.map(value => merge.sensors?.push(new TSensorModel(value)))
                }
            }
        }

        AxiosClient
            .get(ApiService.resShare("timelapse", pId, "s"), query)
            .then(r => {
                if (r.success) {
                    const page = r.meta ? r.meta.currentPage : 1;

                    if (query) {
                        if (
                            (query.filter && Object.keys(query.filter).length > 0)
                            || (query.sort && query.sort.length > 0)
                            || (query.order && query.order.length > 0)
                        ) {
                            if (page === 1) {
                                merge = {
                                    ...merge,
                                    items: []
                                }

                                console.log('%cReset Item When Filter: TimelapseSettingAction', Color.ConsoleInfo);
                            }
                        }
                    }

                    if (r.meta instanceof PaginateMetaModel) {
                        merge = {
                            ...merge,
                            oMeta: r.meta
                        }

                        const limit = r.meta.perPage;

                        if (limit) {
                            if (merge.query.limit > limit || merge.query.limit !== limit) {
                                merge = {
                                    ...merge,
                                    query: {
                                        ...merge.query,
                                        limit: limit
                                    }
                                }
                            }
                        }
                    }

                    if (r.items) {
                        merge.items = [...state.items]

                        r.items.map((value, index) => merge.items[((page - 1) * merge.query.limit) + index] = new TimelapseShareModel(value))
                    }

                    if (page === 1) {
                        merge = {
                            ...merge,
                            timestamp: moment().unix()
                        }
                    }

                    setState({
                        ...merge,
                        isLoading: SendingStatus.success,
                    })
                } else {
                    setState({
                        ...state,
                        error: r.error,
                        isLoading: SendingStatus.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isLoading', err))
    }

    const onAddItem = (pId: number, data: TTimelapseShareV0) => {
        setState({
            ...state,
            isUpdating: SendingStatus.loading
        })

        AxiosClient
            .post(ApiService.resShare("timelapse", pId), data)
            .then(r => {
                if (r.success) {
                    const merge = {...state};

                    if (merge.oMeta) {
                        merge.oMeta = new PaginateMetaModel({});
                        merge.oMeta.fromObject(state.oMeta);
                        merge.oMeta.totalCount += 1;
                    }

                    if (r.data)
                        merge.items = [new TimelapseShareModel(r.data), ...merge.items];

                    setState({
                        ...merge,
                        isUpdating: SendingStatus.success
                    })
                } else {
                    setState({
                        ...state,
                        error: r.error,
                        isUpdating: SendingStatus.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isUpdating', err))
    }

    const onEditItem = (pId: number, shareId: string, data: TTimelapseShareV0) => {
        setState({
            ...state,
            isUpdating: SendingStatus.loading
        })

        AxiosClient
            .put(ApiService.resShare("timelapse", pId, shareId), data)
            .then(r => {
                if (r.success) {
                    const merge = {...state};

                    merge.items = state.items.map(value => {
                        if (value.sid === shareId && r.data) {
                            return new TimelapseShareModel(r.data);
                        } else {
                            return value;
                        }
                    })

                    setState({
                        ...merge,
                        isUpdating: SendingStatus.success
                    })
                } else {
                    setState({
                        ...state,
                        error: r.error,
                        isUpdating: SendingStatus.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isUpdating', err))
    }

    const onDeleteItem = (pId: number, shareId: string) => {
        setState({
            ...state,
            isDeleting: SendingStatus.loading
        })

        AxiosClient
            .delete(ApiService.resShare("timelapse", pId, shareId))
            .then(r => {
                if (r.success) {
                    const merge = {...state};

                    if (merge.oMeta) {
                        merge.oMeta = new PaginateMetaModel({});
                        merge.oMeta.fromObject(state.oMeta);
                        merge.oMeta.totalCount -= 1;
                    }

                    merge.items = state.items?.filter((value) => value.sid !== shareId);

                    setState({
                        ...merge,
                        isDeleting: SendingStatus.success
                    })
                } else {
                    setState({
                        ...state,
                        error: r.error,
                        isDeleting: SendingStatus.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isDeleting', err))
    }

    const resetStateWithEffect = () => {
        setState(initialState);
    }

    return {
        vm,
        onLoadItems,
        onAddItem,
        onEditItem,
        onDeleteItem,
        onClearState: resetState,
        resetStateWithEffect,
    }
}
