import {initialState, TTimelapseViewVideoFilterState} from "./TimelapseViewVideoFilterState";
import {SendingStatus} from "../../../../const/Events";
import {AxiosClient} from "../../../../repositories/AxiosClient";
import {ApiService} from "../../../../repositories/ApiService";
import {setErrorHandled} from "../../../CmAction";
import moment from "moment";
import {TimelapseVideoFilterVO} from "../../../../models/service/timelapse/TimelapseVideoModel";
import {TimelapseVideoFilterModel} from "../../../../models/service/timelapse/TimelapseVideoFilterModel";
import {useState} from "react";

export const TimelapseViewVideoFilterAction = () => {
    const [state, setState] = useState<TTimelapseViewVideoFilterState>(initialState)

    const onLoadItem = (machineId: number, query?: TimelapseVideoFilterVO) => {
        setState({
            ...state,
            isLoading: SendingStatus.loading,
        })

        AxiosClient
            .get(ApiService.getTimelapseVideoFilter(machineId), query)
            .then(r => {
                if (r.success) {
                    setState({
                        ...state,
                        item: r.data && new TimelapseVideoFilterModel(r.data),
                        isLoading: SendingStatus.success,
                        key: machineId.toString(),
                        timestamp: moment().unix()
                    })
                } else {
                    setState({
                        ...state,
                        error: r.error,
                        isLoading: SendingStatus.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isLoading', err))
    }

    const resetStateWithEffect = () => {
        setState(initialState);
    }

    const resetState = () => {
        setState(initialState)
    }

    return {
        vm: state,
        onLoadItem,
        onClearState: resetState,
        resetStateWithEffect,
    }
}
