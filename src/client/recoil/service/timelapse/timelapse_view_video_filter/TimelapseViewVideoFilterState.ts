import {atom} from "recoil";
import {SendingStatus} from "../../../../const/Events";
import {KeyTimelapseViewVideoFilter} from "../../../KeyRecoil";
import {TimelapseVideoFilterModel} from "../../../../models/service/timelapse/TimelapseVideoFilterModel";

export type TTimelapseViewVideoFilterState = {
    key?: string,
    item?: TimelapseVideoFilterModel
    isLoading: SendingStatus
    timestamp?: number
    error?: Record<string, any>
}

export const initialState: TTimelapseViewVideoFilterState = {
    isLoading: SendingStatus.idle,
}

export const TimelapseViewVideoFilterState = atom<TTimelapseViewVideoFilterState>({
    key: KeyTimelapseViewVideoFilter,
    default: initialState
})
