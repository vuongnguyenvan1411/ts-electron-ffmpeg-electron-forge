import {atom} from "recoil";
import {SendingStatus} from "../../../../const/Events";
import {KeyTimelapseImageCompareTool} from "../../../KeyRecoil";
import {TimelapseImageModel} from "../../../../models/service/timelapse/TimelapseImageModel";
import {PaginateLinksModel, PaginateMetaModel} from "../../../../models/ApiResModel";

export type TKeyPushItem = 'left' | 'right';
export type TDates = 'dateStart' | 'dateEnd';

type TCompare = {
    key?: Record<TDates, string | undefined>
    isNextAvailable: boolean
    items: TimelapseImageModel[]
    isLoading: SendingStatus
    isLoadMore: SendingStatus
    oMeta?: PaginateMetaModel
    oLinks?: PaginateLinksModel
    query: {
        page: number
        limit: number
    }
    error?: Record<string, any>
}

export type TTimelapseImageCompareToolState = {
    key?: string,
    left: TCompare,
    right: TCompare,
    timestamp?: number
}

export const initialState: TTimelapseImageCompareToolState = {
    left: {
        items: [],
        isNextAvailable: false,
        isLoading: SendingStatus.idle,
        isLoadMore: SendingStatus.idle,
        query: {
            page: 1,
            limit: 12,
        }
    },
    right: {
        items: [],
        isNextAvailable: false,
        isLoading: SendingStatus.idle,
        isLoadMore: SendingStatus.idle,
        query: {
            page: 1,
            limit: 12,
        }
    }
}

export const TimelapseImageCompareToolState = atom<TTimelapseImageCompareToolState>({
    key: KeyTimelapseImageCompareTool,
    default: initialState
})
