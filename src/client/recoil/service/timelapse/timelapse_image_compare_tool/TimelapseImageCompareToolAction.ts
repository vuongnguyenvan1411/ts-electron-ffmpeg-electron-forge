import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {initialState, TimelapseImageCompareToolState, TKeyPushItem, TTimelapseImageCompareToolState} from "./TimelapseImageCompareToolState";
import {SendingStatus} from "../../../../const/Events";
import {AxiosClient} from "../../../../repositories/AxiosClient";
import {ApiService} from "../../../../repositories/ApiService";
import {setErrorHandled} from "../../../CmAction";
import {TimelapseImageFilterVO, TimelapseImageModel} from "../../../../models/service/timelapse/TimelapseImageModel";
import {PaginateMetaModel} from "../../../../models/ApiResModel";
import {StoreRecoilSingleton} from "../../../StoreRecoilSingleton";

export const TimelapseImageCompareToolAction = () => {
    const _store = StoreRecoilSingleton.getInstance();
    const [state, setState] = useRecoilState<TTimelapseImageCompareToolState>(TimelapseImageCompareToolState)
    const resetState = useResetRecoilState(TimelapseImageCompareToolState)
    const vm = useRecoilValue(TimelapseImageCompareToolState)
    _store.resetTimelapse["TimelapseImageCompareToolAction"] = resetState;

    const onLoadImages = (machineId: number, keyPush: TKeyPushItem, query?: TimelapseImageFilterVO) => {
        setState({
            ...state,
            [keyPush]: {
                ...state[keyPush],
                isLoading: SendingStatus.loading
            }
        })

        AxiosClient
            .get(ApiService.getTimelapseImages(machineId), query)
            .then(r => {
                if (r.success) {
                    let merge = {...state[keyPush]}

                    if (r.meta instanceof PaginateMetaModel) {
                        const page = r.meta.currentPage
                        // Reset items when page = 1
                        if (page === 1) {
                            merge.items = []
                        } else {
                            merge.items = [...state[keyPush].items]
                        }

                        if (r.items) {
                            r.items.map(value => merge.items.push(new TimelapseImageModel(value)))
                        }

                        merge.isNextAvailable = r.meta.nextPage !== undefined;
                        const limit = r.meta.perPage;

                        if (limit) {
                            if (merge.query.limit > limit || merge.query.limit !== limit) {
                                merge = {
                                    ...merge,
                                    query: {
                                        ...merge.query,
                                        limit: limit
                                    }
                                }
                            }
                        }

                        merge = {
                            ...merge,
                            oMeta: r.meta
                        }

                    }

                    setState({
                        ...state,
                        [keyPush]: {
                            ...merge,
                            isLoading: SendingStatus.success
                        }
                    })
                } else {
                    setState({
                        ...state,
                        [keyPush]: {
                            ...state[keyPush],
                            error: r.error,
                            isLoading: SendingStatus.error
                        }
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, [keyPush, 'isLoading'], err))
    }

    const resetStateWithEffect = () => {
        setState(initialState);
    }

    return {
        vm,
        onLoadImages,
        onClearState: resetState,
        resetStateWithEffect,
    }
}
