import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {initialState, TimelapseImageAnalyticsToolState, TTimelapseImageAnalyticsToolState} from "./TimelapseImageAnalyticsToolState";
import {SendingStatus} from "../../../../const/Events";
import {AxiosClient} from "../../../../repositories/AxiosClient";
import {ApiService} from "../../../../repositories/ApiService";
import {setErrorHandled} from "../../../CmAction";
import moment from "moment";
import {PaginateMetaModel} from "../../../../models/ApiResModel";
import {TimelapseImageAnalyticsFilterVO, TimelapseImageModel} from "../../../../models/service/timelapse/TimelapseImageModel";
import {AfterSlideDetail} from "lightgallery/lg-events";
import {StoreRecoilSingleton} from "../../../StoreRecoilSingleton";

export const TimelapseImageAnalyticsToolAction = () => {
    const _store = StoreRecoilSingleton.getInstance();
    const [state, setState] = useRecoilState<TTimelapseImageAnalyticsToolState>(TimelapseImageAnalyticsToolState)
    const resetState = useResetRecoilState(TimelapseImageAnalyticsToolState)
    const vm = useRecoilValue(TimelapseImageAnalyticsToolState)
    _store.resetTimelapse["TimelapseImageAnalyticsToolAction"] = resetState;

    const onLoadItems = (machineId: number, query?: TimelapseImageAnalyticsFilterVO) => {
        setState({
            ...state,
            isLoading: SendingStatus.loading
        })

        AxiosClient
            .get(ApiService.getTimelapseImageIgroups(machineId), query)
            .then(r => {
                if (r.success) {
                    let merge = {...state}

                    if (r.meta instanceof PaginateMetaModel) {
                        const page = r.meta.currentPage

                        // Reset items when page = 1
                        if (page === 1) {
                            merge.items = []
                        } else {
                            merge.items = [...state.items]
                        }

                        if (r.items) {
                            r.items.map(value => merge.items.push(new TimelapseImageModel(value)))
                        }

                        merge.isNextAvailable = r.meta.nextPage !== undefined;

                        const limit = r.meta.perPage;

                        if (limit) {
                            if (merge.query.limit > limit || merge.query.limit !== limit) {
                                merge = {
                                    ...merge,
                                    query: {
                                        ...merge.query,
                                        limit: limit
                                    }
                                }
                            }
                        }

                        merge = {
                            ...merge,
                            oMeta: r.meta
                        }

                        if (page === 1) {
                            merge = {
                                ...merge,
                                key: machineId.toString(),
                                timestamp: moment().unix()
                            }
                        }
                    }

                    setState({
                        ...merge,
                        isLoading: SendingStatus.success,
                    })
                } else {
                    setState({
                        ...state,
                        error: r.error,
                        isLoading: SendingStatus.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isLoading', err))
    }

    const onCacheItems = (data: {
        items: TimelapseImageModel[],
        oMeta: PaginateMetaModel,
        query: {
            page: number
            limit: number
        }
    }) => {
        setState({
            ...state,
            items: data.items,
            oMeta: data.oMeta
        })
    }

    const onChangeCurrentIndex = (detail: AfterSlideDetail) => {
        setState({
            ...state,
            detailLightGalleryChange: detail
        })
    }

    const resetStateWithEffect = () => {
        setState(initialState);
    }

    return {
        vm,
        onLoadItems,
        onCacheItems,
        onChangeCurrentIndex,
        onClearState: resetState,
        resetStateWithEffect,
    }
}
