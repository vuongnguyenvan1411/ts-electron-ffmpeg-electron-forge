import {atom} from "recoil";
import {SendingStatus} from "../../../../const/Events";
import {KeyTimelapseImageAnalyticsTool} from "../../../KeyRecoil";
import {PaginateLinksModel, PaginateMetaModel} from "../../../../models/ApiResModel";
import {TimelapseImageModel} from "../../../../models/service/timelapse/TimelapseImageModel";
import {AfterSlideDetail} from "lightgallery/lg-events";

export type TTimelapseImageAnalyticsToolState = {
    key?: string,
    isNextAvailable: boolean
    items: TimelapseImageModel[]
    detailLightGalleryChange?: AfterSlideDetail
    isLoading: SendingStatus
    isLoadMore: SendingStatus
    oMeta?: PaginateMetaModel
    oLinks?: PaginateLinksModel
    query: {
        page: number
        limit: number
    }
    timestamp?: number
    error?: Record<string, any>
}

export const initialState: TTimelapseImageAnalyticsToolState = {
    items: [],
    isNextAvailable: false,
    isLoading: SendingStatus.idle,
    isLoadMore: SendingStatus.idle,
    query: {
        page: 1,
        limit: 12
    }
}

export const TimelapseImageAnalyticsToolState = atom<TTimelapseImageAnalyticsToolState>({
    key: KeyTimelapseImageAnalyticsTool,
    default: initialState
})
