import {atom} from "recoil";
import {SendingStatus} from "../../../../const/Events";
import {KeyTimelapseMachine} from "../../../KeyRecoil";
import {PaginateLinksModel, PaginateMetaModel} from "../../../../models/ApiResModel";
import {TimelapseMachineModel} from "../../../../models/service/timelapse/TimelapseMachineModel";

export type TTimelapseMachineState = {
    key?: string,
    items: TimelapseMachineModel[]
    isLoading: SendingStatus
    oMeta?: PaginateMetaModel
    oLinks?: PaginateLinksModel
    query: {
        page: number
        limit: number
    }
    timestamp?: number
    error?: Record<string, any>
}

export const initialState: TTimelapseMachineState = {
    items: [],
    isLoading: SendingStatus.idle,
    query: {
        page: 1,
        limit: 12
    }
}

export const TimelapseMachineState = atom<TTimelapseMachineState>({
    key: KeyTimelapseMachine,
    default: initialState
})
