import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {initialState, TimelapseMachineState, TTimelapseMachineState} from "./TimelapseMachineState";
import {SendingStatus} from "../../../../const/Events";
import {AxiosClient} from "../../../../repositories/AxiosClient";
import {ApiService} from "../../../../repositories/ApiService";
import {setErrorHandled} from "../../../CmAction";
import moment from "moment";
import {TimelapseMachineModel, TTimelapseMachineFilterVO} from "../../../../models/service/timelapse/TimelapseMachineModel";
import {StoreRecoilSingleton} from "../../../StoreRecoilSingleton";
import {Color} from "../../../../const/Color";

export const TimelapseMachineAction = () => {
    const _store = StoreRecoilSingleton.getInstance();
    const [state, setState] = useRecoilState<TTimelapseMachineState>(TimelapseMachineState)
    const resetState = useResetRecoilState(TimelapseMachineState)
    const vm = useRecoilValue(TimelapseMachineState)
    _store.resetTimelapse["TimelapseMachineAction"] = resetState;

    const onLoadItems = (projectId: number, query?: TTimelapseMachineFilterVO, val?: TTimelapseMachineState) => {
        const _state = val ?? state;

        setState({
            ..._state,
            isLoading: SendingStatus.loading
        })

        AxiosClient
            .get(ApiService.getTimelapseMachines(projectId), query)
            .then(r => {
                if (r.success) {
                    let merge = {..._state}
                    const page = r.meta?.currentPage ?? 1;

                    if (query) {
                        if (
                            (query.filter && Object.keys(query.filter).length > 0)
                            || (query.sort && query.sort.length > 0)
                            || (query.order && query.order.length > 0)
                        ) {

                            if (page === 1) {
                                merge = {
                                    ...merge,
                                    items: []
                                }

                                console.log('%cReset Item When Filter: TimelapseProjectAction', Color.ConsoleInfo);
                            }
                        }
                    }

                    merge = {
                        ...merge,
                        oMeta: r.meta
                    }

                    const limit = r.meta?.perPage;

                    if (limit) {
                        if (merge.query.limit > limit || merge.query.limit !== limit) {
                            merge = {
                                ...merge,
                                query: {
                                    ...merge.query,
                                    limit: limit
                                }
                            }
                        }
                    }

                    if (r.items) {
                        const _items = [...merge.items];
                        r.items.map((value, index) => _items[((page - 1) * merge.query.limit) + index] = new TimelapseMachineModel(value))
                        merge = {
                            ...merge,
                            items: _items,
                        }
                    }

                    if (page === 1) {
                        merge = {
                            ...merge,
                            timestamp: moment().unix()
                        }
                    }

                    setState({
                        ...merge,
                        isLoading: SendingStatus.success,
                    })
                } else {
                    setState({
                        ..._state,
                        error: r.error,
                        isLoading: SendingStatus.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isLoading', err))
    }

    const resetStateWithEffect = (state?: TTimelapseMachineState, callBack?: (state: TTimelapseMachineState) => void) => {
        // setState(state ?? initialState);
        if (callBack) {
            callBack(state ?? initialState);
        }
    }

    return {
        vm,
        onLoadItems,
        onClearState: resetState,
        resetStateWithEffect,
    }
}
