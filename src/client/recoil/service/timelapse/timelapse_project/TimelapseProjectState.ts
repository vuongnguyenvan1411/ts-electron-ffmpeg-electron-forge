import {atom} from "recoil";
import {SendingStatus} from "../../../../const/Events";
import {KeyTimelapseProject} from "../../../KeyRecoil";
import {PaginateLinksModel, PaginateMetaModel} from "../../../../models/ApiResModel";
import {TimelapseProjectModel} from "../../../../models/service/timelapse/TimelapseProjectModel";
import {memoize, MemoizedFunction, values} from "lodash";

export type TTimelapseProjectState = {
    items: TimelapseProjectModel[]
    isLoading: SendingStatus
    oMeta?: PaginateMetaModel
    oLinks?: PaginateLinksModel
    query: {
        page: number
        limit: number
    }
    timestamp?: number
    error?: Record<string, any>
    memoizeSearch: MemoizedFunction
}

export const initialState: TTimelapseProjectState = {
    items: [],
    isLoading: SendingStatus.idle,
    query: {
        page: 1,
        limit: 6,
    },
    memoizeSearch: memoize(values),
}

export const TimelapseProjectState = atom<TTimelapseProjectState>({
    key: KeyTimelapseProject,
    default: initialState
})
