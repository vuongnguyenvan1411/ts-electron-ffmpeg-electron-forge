import {atom} from "recoil";
import {SendingStatus} from "../../../../const/Events";
import {KeyMap2DProfile} from "../../../KeyRecoil";
import {PaginateLinksModel, PaginateMetaModel} from "../../../../models/ApiResModel";
import {ProfileModel} from "../../../../models/service/geodetic/ProfileModel";

export type TMap2DProfileState = {
    key?: string
    items: ProfileModel[]
    isLoading: SendingStatus
    isUpdating: SendingStatus
    isDeleting: SendingStatus
    oMeta?: PaginateMetaModel
    oLink?: PaginateLinksModel
    query: {
        page: number,
        limit: number
    };
    error?: Record<string, any>
    timestamp?: number
}

export const initialState: TMap2DProfileState = {
    items: [],
    isLoading: SendingStatus.idle,
    isUpdating: SendingStatus.idle,
    isDeleting: SendingStatus.idle,
    query: {
        page: 1,
        limit: 5
    }
}

export const Map2DProfileState = atom<TMap2DProfileState>({
    key: KeyMap2DProfile,
    default: initialState
})
