import {atom} from "recoil";
import {SendingStatus} from "../../../../const/Events";
import {KeyGeodeticInfo} from "../../../KeyRecoil";
import {SpaceModel} from "../../../../models/service/geodetic/SpaceModel";

export type TGeodeticInfoState = {
    key?: string,
    item?: SpaceModel
    isLoading: SendingStatus
    timestamp?: number
    error?: Record<string, any>
}

export const initialState: TGeodeticInfoState = {
    isLoading: SendingStatus.idle,
}

export const GeodeticInfoState = atom<TGeodeticInfoState>({
    key: KeyGeodeticInfo,
    default: initialState
})
