import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {GeodeticInfoState, initialState, TGeodeticInfoState} from "./GeodeticInfoState";
import {SendingStatus} from "../../../../const/Events";
import {AxiosClient} from "../../../../repositories/AxiosClient";
import {ApiService} from "../../../../repositories/ApiService";
import {setErrorHandled} from "../../../CmAction";
import moment from "moment";
import {SpaceModel} from "../../../../models/service/geodetic/SpaceModel";
import {StoreRecoilSingleton} from "../../../StoreRecoilSingleton";

export const GeodeticInfoAction = () => {
    const _store = StoreRecoilSingleton.getInstance();
    const [state, setState] = useRecoilState<TGeodeticInfoState>(GeodeticInfoState)
    const resetState = useResetRecoilState(GeodeticInfoState)
    const vm = useRecoilValue(GeodeticInfoState)
    _store.resetGeodetic["GeodeticInfoAction"] = resetState;

    const onLoadItem = (spaceId: number) => {
        setState({
            ...state,
            isLoading: SendingStatus.loading
        })

        AxiosClient
            .get(ApiService.resSpace(spaceId))
            .then(r => {
                if (r.success) {
                    setState({
                        ...state,
                        item: r.data && new SpaceModel(r.data),
                        isLoading: SendingStatus.success,
                        key: spaceId.toString(),
                        timestamp: moment().unix()
                    })
                } else {
                    setState({
                        ...state,
                        error: r.error,
                        isLoading: SendingStatus.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isLoading', err))
    }

    const resetStateWithEffect = () => {
        setState(initialState);
    }

    return {
        vm,
        onLoadItem,
        onClearState: resetState,
        resetStateWithEffect,
    }
}
