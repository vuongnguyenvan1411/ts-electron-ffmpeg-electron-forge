import {atom} from "recoil";
import {SendingStatus} from "../../../../const/Events";
import {KeyGeodeticView3D} from "../../../KeyRecoil";
import {Map3DModel} from "../../../../models/service/geodetic/Map3DModel";

export type TGeodeticView3DState = {
    key?: string,
    item?: Map3DModel
    isLoading: SendingStatus
    timestamp?: number
    error?: Record<string, any>
}

export const initialState: TGeodeticView3DState = {
    isLoading: SendingStatus.idle,
}

export const GeodeticView3DState = atom<TGeodeticView3DState>({
    key: KeyGeodeticView3D,
    default: initialState
})
