import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {GeodeticView3DState, initialState, TGeodeticView3DState} from "./GeodeticView3DState";
import {SendingStatus} from "../../../../const/Events";
import {AxiosClient} from "../../../../repositories/AxiosClient";
import {ApiService} from "../../../../repositories/ApiService";
import {setErrorHandled} from "../../../CmAction";
import moment from "moment";
import {Map3DModel} from "../../../../models/service/geodetic/Map3DModel";
import {StoreRecoilSingleton} from "../../../StoreRecoilSingleton";

export const GeodeticView3DAction = () => {
    const _store = StoreRecoilSingleton.getInstance();
    const [state, setState] = useRecoilState<TGeodeticView3DState>(GeodeticView3DState)
    const resetState = useResetRecoilState(GeodeticView3DState)
    const vm = useRecoilValue(GeodeticView3DState)
    _store.resetGeodetic["GeodeticView3DAction"] = resetState;

    const onLoadItem = (spaceId: number, m3dId: number) => {
        setState({
            ...state,
            isLoading: SendingStatus.loading
        })

        AxiosClient
            .get(ApiService.getSpaceM3d(spaceId, m3dId))
            .then(r => {
                if (r.success) {
                    setState({
                        ...state,
                        item: r.data && new Map3DModel(r.data),
                        isLoading: SendingStatus.success,
                        key: spaceId.toString(),
                        timestamp: moment().unix()
                    })
                } else {
                    setState({
                        ...state,
                        error: r.error,
                        isLoading: SendingStatus.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isLoading', err))
    }

    const resetStateWithEffect = () => {
        setState(initialState);
    }

    return {
        vm,
        onLoadItem,
        onClearState: resetState,
        resetStateWithEffect,
    }
}
