import {atom} from "recoil";
import {SendingStatus} from "../../../../const/Events";
import {KeyGeodeticSetting} from "../../../KeyRecoil";
import {PaginateLinksModel, PaginateMetaModel} from "../../../../models/ApiResModel";
import {SpaceShareModel} from "../../../../models/ShareModel";

export type TGeodeticSettingState = {
    key?: string
    items: SpaceShareModel[]
    isLoading: SendingStatus
    isUpdating: SendingStatus
    isDeleting: SendingStatus
    oMeta?: PaginateMetaModel
    oLink?: PaginateLinksModel
    query: {
        page: number,
        limit: number
    };
    error?: Record<string, any>
    timestamp?: number
}

export const initialState: TGeodeticSettingState = {
    items: [],
    isLoading: SendingStatus.idle,
    isUpdating: SendingStatus.idle,
    isDeleting: SendingStatus.idle,
    query: {
        page: 1,
        limit: 12
    }
}

export const GeodeticSettingState = atom<TGeodeticSettingState>({
    key: KeyGeodeticSetting,
    default: initialState
})
