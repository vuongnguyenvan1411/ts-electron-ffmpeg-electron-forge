import {atom} from "recoil";
import {SendingStatus} from "../../../../const/Events";
import {KeyMap2DShare} from "../../../KeyRecoil";
import {PaginateLinksModel, PaginateMetaModel} from "../../../../models/ApiResModel";
import {Map2DShareModel} from "../../../../models/ShareModel";

export type TMap2DShareState = {
    key?: string
    items: Map2DShareModel[]
    isLoading: SendingStatus
    isUpdating: SendingStatus
    isDeleting: SendingStatus
    oMeta?: PaginateMetaModel
    oLink?: PaginateLinksModel
    query: {
        page: number,
        limit: number
    };
    error?: Record<string, any>
    timestamp?: number
}

export const initialState: TMap2DShareState = {
    items: [],
    isLoading: SendingStatus.idle,
    isUpdating: SendingStatus.idle,
    isDeleting: SendingStatus.idle,
    query: {
        page: 1,
        limit: 2
    }
}

export const Map2DShareState = atom<TMap2DShareState>({
    key: KeyMap2DShare,
    default: initialState
})
