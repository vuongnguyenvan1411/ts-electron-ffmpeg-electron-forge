import {atom} from "recoil";
import {SendingStatus} from "../../../../const/Events";
import {KeyGeodeticView2D} from "../../../KeyRecoil";
import {Map2DModel} from "../../../../models/service/geodetic/Map2DModel";

export type TGeodeticView2DState = {
    key?: string,
    item?: Map2DModel
    isLoading: SendingStatus
    timestamp?: number
    error?: Record<string, any>
}

export const initialState: TGeodeticView2DState = {
    isLoading: SendingStatus.idle,
}

export const GeodeticView2DState = atom<TGeodeticView2DState>({
    key: KeyGeodeticView2D,
    default: initialState
})
