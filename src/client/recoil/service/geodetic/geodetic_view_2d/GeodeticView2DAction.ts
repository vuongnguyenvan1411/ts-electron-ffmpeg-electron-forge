import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {GeodeticView2DState, initialState, TGeodeticView2DState} from "./GeodeticView2DState";
import {SendingStatus} from "../../../../const/Events";
import {AxiosClient} from "../../../../repositories/AxiosClient";
import {ApiService} from "../../../../repositories/ApiService";
import {setErrorHandled} from "../../../CmAction";
import moment from "moment";
import {Map2DModel} from "../../../../models/service/geodetic/Map2DModel";
import {StoreRecoilSingleton} from "../../../StoreRecoilSingleton";

export const GeodeticView2DAction = () => {
    const _store = StoreRecoilSingleton.getInstance();
    const [state, setState] = useRecoilState<TGeodeticView2DState>(GeodeticView2DState)
    const resetState = useResetRecoilState(GeodeticView2DState)
    const vm = useRecoilValue(GeodeticView2DState)
    _store.resetGeodetic["GeodeticView2DAction"] = resetState;

    const onLoadItem = (spaceId: number, m2dId: number) => {
        setState({
            ...state,
            isLoading: SendingStatus.loading
        })

        AxiosClient
            .get(ApiService.getSpaceM2d(spaceId, m2dId))
            .then(r => {
                if (r.success) {
                    setState({
                        ...state,
                        item: r.data && new Map2DModel(r.data),
                        isLoading: SendingStatus.success,
                        key: spaceId.toString(),
                        timestamp: moment().unix()
                    })
                } else {
                    setState({
                        ...state,
                        error: r.error,
                        isLoading: SendingStatus.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isLoading', err))
    }

    const resetStateWithEffect = () => {
        setState(initialState);
    }

    return {
        vm,
        onLoadItem,
        onClearState: resetState,
        resetStateWithEffect,
    }
}
