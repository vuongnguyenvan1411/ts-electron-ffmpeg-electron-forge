import {atom} from "recoil";
import {SendingStatus} from "../../../../const/Events";
import {KeyMap3DShare} from "../../../KeyRecoil";
import {PaginateLinksModel, PaginateMetaModel} from "../../../../models/ApiResModel";
import {Map3DShareModel} from "../../../../models/ShareModel";

export type TMap3DShareState = {
    key?: string
    items: Map3DShareModel[]
    isLoading: SendingStatus
    isUpdating: SendingStatus
    isDeleting: SendingStatus
    oMeta?: PaginateMetaModel
    oLink?: PaginateLinksModel
    query: {
        page: number,
        limit: number
    };
    error?: Record<string, any>
    timestamp?: number
}

export const initialState: TMap3DShareState = {
    items: [],
    isLoading: SendingStatus.idle,
    isUpdating: SendingStatus.idle,
    isDeleting: SendingStatus.idle,
    query: {
        page: 1,
        limit: 5
    }
}

export const Map3DShareState = atom<TMap3DShareState>({
    key: KeyMap3DShare,
    default: initialState
})
