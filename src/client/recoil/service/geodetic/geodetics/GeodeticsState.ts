import {atom} from "recoil";
import {SendingStatus} from "../../../../const/Events";
import {KeyGeodetics} from "../../../KeyRecoil";
import {PaginateLinksModel, PaginateMetaModel} from "../../../../models/ApiResModel";
import {SpaceModel} from "../../../../models/service/geodetic/SpaceModel";
import {memoize, MemoizedFunction, values} from "lodash";

export type TGeodeticsState = {
    items: SpaceModel[]
    isLoading: SendingStatus
    oMeta?: PaginateMetaModel
    oLinks?: PaginateLinksModel
    query: {
        page: number
        limit: number
    }
    timestamp?: number
    error?: Record<string, any>
    memoizeSearch: MemoizedFunction
}

export const initialState: TGeodeticsState = {
    items: [],
    isLoading: SendingStatus.idle,
    query: {
        page: 1,
        limit: 12
    },
    memoizeSearch: memoize(values),
}

export const GeodeticsState = atom<TGeodeticsState>({
    key: KeyGeodetics,
    default: initialState
})
