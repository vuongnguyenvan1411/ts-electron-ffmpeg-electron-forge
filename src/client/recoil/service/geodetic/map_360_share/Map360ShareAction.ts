import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {Map360ShareState} from "./Map360ShareState";
import {SendingStatus} from "../../../../const/Events";
import {AxiosClient} from "../../../../repositories/AxiosClient";
import {ApiService} from "../../../../repositories/ApiService";
import {setErrorHandled} from "../../../CmAction";
import moment from "moment";
import {PaginateMetaModel} from "../../../../models/ApiResModel";
import {Color} from "../../../../const/Color";
import {TMap360ShareV0, TShareFilterVO, Vr360ShareModel} from "../../../../models/ShareModel";
import {StoreRecoilSingleton} from "../../../StoreRecoilSingleton";

export const Map360ShareAction = () => {
    const _store = StoreRecoilSingleton.getInstance();
    const [state, setState] = useRecoilState(Map360ShareState)
    const resetState = useResetRecoilState(Map360ShareState)
    const vm = useRecoilValue(Map360ShareState)
    _store.resetGeodetic["Map360ShareAction"] = resetState;

    const onLoadItems = (vr360Id: number, query?: TShareFilterVO) => {
        setState({
            ...state,
            isLoading: SendingStatus.loading
        })

        AxiosClient
            .get(ApiService.resShare("s360", vr360Id, "s"), query)
            .then(r => {
                if (r.success) {
                    let merge = {...state}

                    const page = r.meta ? r.meta.currentPage : 1;

                    if (query) {
                        if (
                            (query.filter && Object.keys(query.filter).length > 0)
                            || (query.sort && query.sort.length > 0)
                            || (query.order && query.order.length > 0)
                        ) {
                            if (page === 1) {
                                merge = {
                                    ...merge,
                                    items: []
                                }

                                console.log('%cReset Item When Filter: Map360ShareAction', Color.ConsoleInfo);
                            }
                        }
                    }

                    if (r.meta instanceof PaginateMetaModel) {
                        merge = {
                            ...merge,
                            oMeta: r.meta
                        }

                        const limit = r.meta.perPage;

                        if (limit) {
                            if (merge.query.limit > limit || merge.query.limit !== limit) {
                                merge = {
                                    ...merge,
                                    query: {
                                        ...merge.query,
                                        limit: limit
                                    }
                                }
                            }
                        }
                    }

                    if (r.items) {
                        merge.items = [...state.items]

                        r.items.map((value, index) => merge.items[((page - 1) * merge.query.limit) + index] = new Vr360ShareModel(value))
                    }

                    if (page === 1) {
                        merge = {
                            ...merge,
                            timestamp: moment().unix()
                        }
                    }

                    setState({
                        ...merge,
                        isLoading: SendingStatus.success,
                    })
                } else {
                    setState({
                        ...state,
                        error: r.error,
                        isLoading: SendingStatus.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isLoading', err))
    }

    const onAddItem = (vr360Id: number, data: TMap360ShareV0) => {
        setState({
            ...state,
            isUpdating: SendingStatus.loading
        })

        AxiosClient
            .post(ApiService.resShare("s360", vr360Id), data)
            .then(r => {
                if (r.success) {
                    const merge = {...state};

                    if (merge.oMeta) {
                        merge.oMeta = new PaginateMetaModel({});
                        merge.oMeta.fromObject(state.oMeta);
                        merge.oMeta.totalCount += 1;
                    }

                    if (r.data)
                        merge.items = [new Vr360ShareModel(r.data), ...merge.items];

                    setState({
                        ...merge,
                        isUpdating: SendingStatus.success
                    })
                } else {
                    setState({
                        ...state,
                        error: r.error,
                        isUpdating: SendingStatus.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isUpdating', err))
    }

    const onEditItem = (vr360Id: number, shareId: string, data: TMap360ShareV0) => {
        setState({
            ...state,
            isUpdating: SendingStatus.loading
        })

        AxiosClient
            .put(ApiService.resShare("s360", vr360Id, shareId), data)
            .then(r => {
                if (r.success) {
                    const merge = {...state};

                    merge.items = state.items.map(value => {
                        if (value.sid === shareId && r.data) {
                            return new Vr360ShareModel(r.data);
                        } else {
                            return value;
                        }
                    })

                    setState({
                        ...merge,
                        isUpdating: SendingStatus.success
                    })
                } else {
                    setState({
                        ...state,
                        error: r.error,
                        isUpdating: SendingStatus.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isUpdating', err))
    }

    const onDeleteItem = (vr360Id: number, shareId: string) => {
        setState({
            ...state,
            isDeleting: SendingStatus.loading
        })

        AxiosClient
            .delete(ApiService.resShare("s360", vr360Id, shareId))
            .then(r => {
                if (r.success) {
                    const merge = {...state};

                    if (merge.oMeta) {
                        merge.oMeta = new PaginateMetaModel({});
                        merge.oMeta.fromObject(state.oMeta);
                        merge.oMeta.totalCount -= 1;
                    }

                    merge.items = state.items?.filter((value) => value.sid !== shareId);

                    setState({
                        ...merge,
                        isDeleting: SendingStatus.success
                    })
                } else {
                    setState({
                        ...state,
                        error: r.error,
                        isDeleting: SendingStatus.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isDeleting', err))
    }

    return {
        vm,
        onLoadItems,
        onAddItem,
        onEditItem,
        onDeleteItem,
        onClearState: resetState
    }
}
