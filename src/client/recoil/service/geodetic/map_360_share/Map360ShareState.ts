import {atom} from "recoil";
import {SendingStatus} from "../../../../const/Events";
import {KeyMap360Share} from "../../../KeyRecoil";
import {PaginateLinksModel, PaginateMetaModel} from "../../../../models/ApiResModel";
import {Vr360ShareModel} from "../../../../models/ShareModel";

export type TMap360ShareState = {
    key?: string
    items: Vr360ShareModel[]
    isLoading: SendingStatus
    isUpdating: SendingStatus
    isDeleting: SendingStatus
    oMeta?: PaginateMetaModel
    oLink?: PaginateLinksModel
    query: {
        page: number,
        limit: number
    };
    error?: Record<string, any>
    timestamp?: number
}

export const initialState: TMap360ShareState = {
    items: [],
    isLoading: SendingStatus.idle,
    isUpdating: SendingStatus.idle,
    isDeleting: SendingStatus.idle,
    query: {
        page: 1,
        limit: 5
    }
}

export const Map360ShareState = atom<TMap360ShareState>({
    key: KeyMap360Share,
    default: initialState
})
