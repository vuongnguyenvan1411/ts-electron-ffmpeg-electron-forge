import {initialState, TLoginState} from "./LoginState";
import {SendingStatus} from "../../../const/Events";
import {ApiService} from "../../../repositories/ApiService";
import {TLoginVO, UserModel} from "../../../models/UserModel";
import {setErrorHandled} from "../../CmAction";
import {MeAction} from "../../account/me/MeAction";
import {useSessionContext} from "../../../presentation/contexts/SessionContext";
import {useInjection} from "inversify-react";
import {useState} from "react";

export const LoginAction = () => {
    const [state, setState] = useState<TLoginState>(initialState)

    const [session, setSession] = useSessionContext()
    const apiService = useInjection(ApiService)

    const {
        onStoreUser
    } = MeAction()

    const onLogIn = (data: TLoginVO) => {
        setState({
            ...state,
            status: SendingStatus.loading
        })

        apiService
            .login(data)
            .then(r => {
                if (r.success) {
                    const user = new UserModel(r.data)

                    onStoreUser(user)

                    setSession({
                        ...session,
                        isAuthenticated: true,
                        user: user,
                    })

                    setState({
                        ...state,
                        user: user,
                        status: SendingStatus.success
                    })
                } else {
                    setState({
                        ...state,
                        status: SendingStatus.error,
                        error: r.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'status', err))
    }

    const resetState = () => {
        setState(initialState)
    }

    return {
        vm: state,
        onLogIn,
        onClearState: resetState
    }
}
