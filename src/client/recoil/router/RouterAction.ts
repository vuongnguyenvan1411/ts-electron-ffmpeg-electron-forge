import {useRecoilState} from "recoil";
import {RouterState} from "./RouterState";
import {RouteConfig} from "../../config/RouteConfig";
import {StoreRecoilSingleton} from "../StoreRecoilSingleton";
import {Color} from "../../const/Color";

enum _EPathCheck {
    Timelapse = "timelapse",
    Vr360 = "vr360",
    Geodetic = "geodetic"
}

export const RouterAction = () => {
    const [state, setState] = useRecoilState(RouterState)
    const _store = StoreRecoilSingleton.getInstance();

    const detectRouter = (path: string) => {
        if (_store.resetTimelapse && Object.keys(_store.resetTimelapse).length > 0) {
            console.log('%c<-START: Detect Router Timelapse-----------------', Color.ConsoleInfo);
            console.log(_store.resetTimelapse)
            console.log('%c--END: Detect Router Timelapse------------------>', Color.ConsoleInfo);
        }

        if (_store.resetVr360 && Object.keys(_store.resetVr360).length > 0) {
            console.log('%c<-START: Detect Router Vr360---------------------', Color.ConsoleInfo);
            console.log(_store.resetVr360)
            console.log('%c--END: Detect Router Vr360---------------------->', Color.ConsoleInfo);
        }

        if (_store.resetGeodetic && Object.keys(_store.resetGeodetic).length > 0) {
            console.log('%c<-START: Detect Router Geodetic------------------', Color.ConsoleInfo);
            console.log(_store.resetGeodetic)
            console.log('%c--END: Detect Router Geodetic------------------->', Color.ConsoleInfo);
        }

        let newPath: string;
        let oldPath = state.path;

        if (path.indexOf(RouteConfig.TIMELAPSE) === 0) {
            newPath = _EPathCheck.Timelapse;
        } else if (path.indexOf(RouteConfig.VR360) === 0) {
            newPath = _EPathCheck.Vr360;
        } else if (path.indexOf(RouteConfig.GEODETIC) === 0) {
            newPath = _EPathCheck.Geodetic;
        } else {
            newPath = path;
        }

        if (oldPath !== newPath) {
            if (newPath !== "timelapse" && Object.keys(_store.resetTimelapse).length !== 0) {
                console.log('%c<-RUN: Clear Store Timelapse-----------------', Color.ConsoleInfo);

                Object.entries(_store.resetTimelapse).forEach(value => {
                    console.log("Clear: ", value[0]);
                    value[1]();
                })

                _store.resetTimelapse = {};

                console.log('%c--END: Clear Store Timelapse----------------->', Color.ConsoleInfo);
            }

            if (newPath !== "vr360" && Object.keys(_store.resetVr360).length !== 0) {
                console.log('%c<-RUN: Clear Store Vr360----------------------', Color.ConsoleInfo);

                Object.entries(_store.resetVr360).forEach(value => {
                    console.log("Clear: ", value[0]);
                    value[1]();
                })

                _store.resetVr360 = {};

                console.log('%c--END: Clear Store Vr360---------------------->', Color.ConsoleInfo);
            }

            if (newPath !== "geodetic" && Object.keys(_store.resetGeodetic).length !== 0) {
                console.log('%c<-RUN: Clear Store Geodetic--------------------', Color.ConsoleInfo);

                Object.entries(_store.resetGeodetic).forEach(value => {
                    console.log("Clear: ", value[0]);
                    value[1]();
                })

                _store.resetGeodetic = {};

                console.log('%c--END: Clear Store Geodetic-------------------->', Color.ConsoleInfo);
            }

            setState({
                path: newPath,
            });
        }
    }

    return {
        detectRouter
    }
}
