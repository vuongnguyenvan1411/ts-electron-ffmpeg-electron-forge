import {atom} from "recoil";
import {KeyRouter} from "../KeyRecoil";

type _TRouterState = {
    path: string,
    keyClear?: "timelapse"
}

export const initialState: _TRouterState = {
    path: "/",
}

export const RouterState = atom<_TRouterState>({
    key: KeyRouter,
    default: initialState
})
