import {Resetter} from "recoil";


export class StoreRecoilSingleton {
    private static instance: StoreRecoilSingleton;

    public resetTimelapse: Record<string, Resetter> = {};
    public resetVr360: Record<string, Resetter> = {};
    public resetGeodetic: Record<string, Resetter> = {};
    public resetCamera: Record<string, Resetter> = {};
    public resetWeighing: Record<string, Resetter> = {};

    private constructor() {
        //
    }

    public static getInstance(): StoreRecoilSingleton {
        if (!StoreRecoilSingleton.instance) {
            StoreRecoilSingleton.instance = new StoreRecoilSingleton();
        }

        return StoreRecoilSingleton.instance;
    }
}
