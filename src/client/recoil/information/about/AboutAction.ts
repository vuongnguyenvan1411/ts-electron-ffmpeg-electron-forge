import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {AboutState} from "./AboutState";
import {SendingStatus} from "../../../const/Events";
import {AxiosClient} from "../../../repositories/AxiosClient";
import {ApiService} from "../../../repositories/ApiService";
import {AboutModel} from "../../../models/information/AboutModel";
import {setErrorHandled} from "../../CmAction";

export const AboutAction = () => {
    const [state, setState] = useRecoilState(AboutState)
    const resetState = useResetRecoilState(AboutState)
    const vm = useRecoilValue(AboutState)

    const onLoadItem = () => {
        setState({
            ...state,
            isLoading: SendingStatus.loading
        })

        AxiosClient
            .get(ApiService.about)
            .then(r => {
                if (r.success) {
                    setState({
                        ...state,
                        item: r.data && new AboutModel(r.data),
                        isLoading: SendingStatus.success,
                    })
                } else {
                    setState({
                        ...state,
                        error: r.error,
                        isLoading: SendingStatus.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isLoading', err))
    }

    return {
        vm,
        onLoadItem,
        onClearState: resetState
    }
}

// export const AboutSelector = selector({
//     key: KeyAbout,
//     get: (opts: {
//         get: GetRecoilValue
//         getCallback: GetCallback
//     }) => {
//         const vm = opts.get(AboutState)
//
//         return {
//             vm
//         }
//     },
//     set<T>(opts: {
//         set: SetRecoilState
//         get: GetRecoilValue
//         reset: ResetRecoilState
//     }, newValue: DefaultValue | T) {
//         //
//     },
//     cachePolicy_UNSTABLE: {
//         eviction: 'most-recent'
//     }
// })
