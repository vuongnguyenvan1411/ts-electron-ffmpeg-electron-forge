import {atom} from "recoil";
import {SendingStatus} from "../../../const/Events";
import {KeyAbout} from "../../KeyRecoil";
import {AboutModel} from "../../../models/information/AboutModel";

export type TAboutState = {
    item?: AboutModel
    isLoading: SendingStatus
    error?: Record<string, any>
}

export const initialState: TAboutState = {
    isLoading: SendingStatus.idle,
}

export const AboutState = atom<TAboutState>({
    key: KeyAbout,
    default: initialState
})
