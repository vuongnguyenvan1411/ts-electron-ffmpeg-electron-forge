import {atom} from "recoil";
import {SendingStatus} from "../../../const/Events";
import {KeyFeedback} from "../../KeyRecoil";
import {FeedbackModel} from "../../../models/information/FeedbackModel";

type TFeedbackState = {
    item?: FeedbackModel
    status: SendingStatus,
    error?: Record<string, any>
}

export const initialState: TFeedbackState = {
    status: SendingStatus.idle,
}

export const FeedbackState = atom<TFeedbackState>({
    key: KeyFeedback,
    default: initialState
})
