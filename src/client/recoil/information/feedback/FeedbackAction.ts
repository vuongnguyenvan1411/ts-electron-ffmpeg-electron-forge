import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {FeedbackState} from "./FeedbackState";
import {SendingStatus} from "../../../const/Events";
import {AxiosClient} from "../../../repositories/AxiosClient";
import {ApiService} from "../../../repositories/ApiService";
import {FeedbackModel, TFeedbackV0} from "../../../models/information/FeedbackModel";
import {setErrorHandled} from "../../CmAction";

export const FeedbackAction = () => {
    const [state, setState] = useRecoilState(FeedbackState)
    const resetState = useResetRecoilState(FeedbackState)
    const vm = useRecoilValue(FeedbackState)

    const onPost = (data: TFeedbackV0) => {
        setState({
            ...state,
            status: SendingStatus.loading
        })

        AxiosClient
            .post(ApiService.feedback, data)
            .then(r => {
                if (r.success) {
                    setState({
                        ...state,
                        item: r.data && new FeedbackModel(r.data),
                        status: SendingStatus.success
                    })
                } else {
                    setState({
                        ...state,
                        error: r.error,
                        status: SendingStatus.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'status', err))
    }

    return {
        vm,
        onPost,
        onClearState: resetState
    }
}
