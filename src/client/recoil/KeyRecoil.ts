import {Utils} from "../core/Utils";
import {v4 as uuid} from "uuid";

const createKey = (key: string) => {
    if (Utils.isDev()) {
        return `${key}-${uuid()}`;
    }

    return key;
}

export const KeyIT = createKey("it")

// Auth
export const KeyRouter = createKey("router")
export const KeyLogin = createKey("login")
export const KeyLogout = createKey("logout")

// Account
export const KeyMe = createKey("me")
export const KeyMeEdit = createKey("me_edit")
export const KeyMeImage = createKey("me_image")
export const KeyMePassword = createKey("me_password")

// Information
export const KeyAbout = createKey("about")
export const KeyFeedback = createKey("feedback")

// Blog
export const KeyBlogCategories = createKey("blog_categories")
export const KeyBlogPosts = createKey("blog_posts")
export const KeyBlogPost = createKey("blog_post")

// Timelapse
export const KeyTimelapseProject = createKey("timelapse_project")
export const KeyTimelapseProjectInfo = createKey("timelapse_project_info")
export const KeyTimelapseMachine = createKey("timelapse_machine")
export const KeyTimelapseSetting = createKey("timelapse_setting")
export const KeyTimelapseViewImage = createKey("timelapse_view_image")
export const KeyTimelapseViewImageLightGallery = createKey("timelapse_view_image_light_gallery")
export const KeyTimelapseImageCompareTool = createKey("timelapse_image_compare_tool")
export const KeyTimelapseImageAnalyticsTool = createKey("timelapse_image_analytics_tool")
export const KeyTimelapseViewVideoAuto = createKey("timelapse_view_video_auto")
export const KeyTimelapseViewVideoFilter = createKey("timelapse_view_video_filter")
export const KeyTimelapseViewAnalytics = createKey("timelapse_view_analytics")

// Vr360
export const KeyVr360s = createKey("vr360s")
export const KeyVr360Setting = createKey("vr360_setting")

// Geodetic
export const KeyGeodetics = createKey("geodetics")
export const KeyGeodeticInfo = createKey("geodetic_info")
export const KeyGeodeticSetting = createKey("geodetic_setting")
export const KeyGeodeticView2D = createKey("geodetic_view_2d")
export const KeyGeodeticView3D = createKey("geodetic_view_3d")
export const KeyMap2DShare = createKey("map_2d_share")
export const KeyMap3DShare = createKey("map_3d_share")
export const KeyMap360Share = createKey("map_360_share")
export const KeyMap2DProfile = createKey("map_2d_profile")

// Camera Service
export const KeyCameras = createKey("cameras")


// Weighing Service
export const KeyWeighings = createKey("weighings")
