import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {initialState, PostsState, TPostsState} from "./PostsState";
import {SendingStatus} from "../../../const/Events";
import {AxiosClient} from "../../../repositories/AxiosClient";
import {ApiService} from "../../../repositories/ApiService";
import {setErrorHandled} from "../../CmAction";
import moment from "moment";
import {PaginateMetaModel} from "../../../models/ApiResModel";
import {Color} from "../../../const/Color";
import {BlogPostModel, TPostsFilterVO} from "../../../models/blog/BlogPostModel";

export const PostsAction = () => {
    const [state, setState] = useRecoilState<TPostsState>(PostsState)
    const resetState = useResetRecoilState(PostsState)
    const vm = useRecoilValue(PostsState)

    const onLoadItems = (query?: TPostsFilterVO) => {
        setState({
            ...state,
            isLoading: SendingStatus.loading
        })

        AxiosClient
            .get(ApiService.getBlogPosts, query)
            .then(r => {
                if (r.success) {
                    let merge = {...state}

                    if (r.meta instanceof PaginateMetaModel) {
                        const page = r.meta.currentPage;

                        if (query) {
                            if (
                                (query.filter && Object.keys(query.filter).length > 0)
                                || (query.sort && query.sort.length > 0)
                                || (query.order && query.order.length > 0)
                            ) {
                                if (page === 1) {
                                    merge = {
                                        ...merge,
                                        items: []
                                    }

                                    console.log('%cReset Item When Filter: PostsAction', Color.ConsoleInfo);
                                }
                            }
                        }

                        merge = {
                            ...merge,
                            oMeta: r.meta
                        }

                        const limit = r.meta.perPage;

                        if (limit) {
                            if (merge.query.limit > limit || merge.query.limit !== limit) {
                                merge = {
                                    ...merge,
                                    query: {
                                        ...merge.query,
                                        limit: limit
                                    }
                                }
                            }
                        }

                        merge.items = [...state.items]

                        if (r.items) {
                            r.items.map((value, index) => merge.items[((page - 1) * merge.query.limit) + index] = new BlogPostModel(value))
                        }

                        if (page === 1) {
                            merge = {
                                ...merge,
                                timestamp: moment().unix()
                            }
                        }
                    }

                    setState({
                        ...merge,
                        isLoading: SendingStatus.success,
                    })
                } else {
                    setState({
                        ...state,
                        error: r.error,
                        isLoading: SendingStatus.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isLoading', err))
    }

    const onCacheItems = (data: {
        items: BlogPostModel[],
        oMeta: PaginateMetaModel,
        query: {
            page: number
            limit: number
        }
    }) => {
        setState({
            ...state,
            items: data.items,
            oMeta: data.oMeta
        })
    }

    const resetStateWithEffect = () => {
        setState(initialState);
    }

    return {
        vm,
        onLoadItems,
        onCacheItems,
        onClearState: resetState,
        resetStateWithEffect,
    }
}
