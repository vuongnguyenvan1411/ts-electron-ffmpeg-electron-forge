import {atom} from "recoil";
import {SendingStatus} from "../../../const/Events";
import {KeyBlogPosts} from "../../KeyRecoil";
import {PaginateLinksModel, PaginateMetaModel} from "../../../models/ApiResModel";
import {BlogPostModel} from "../../../models/blog/BlogPostModel";

export type TPostsState = {
    items: BlogPostModel[]
    isLoading: SendingStatus
    oMeta?: PaginateMetaModel
    oLinks?: PaginateLinksModel
    query: {
        page: number
        limit: number
    }
    timestamp?: number
    error?: Record<string, any>
}

export const initialState: TPostsState = {
    items: [],
    isLoading: SendingStatus.idle,
    query: {
        page: 1,
        limit: 6
    }
}

export const PostsState = atom<TPostsState>({
    key: KeyBlogPosts,
    default: initialState
})
