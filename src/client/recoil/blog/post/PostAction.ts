import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {initialState, PostState} from "./PostState";
import {SendingStatus} from "../../../const/Events";
import {AxiosClient} from "../../../repositories/AxiosClient";
import {ApiService} from "../../../repositories/ApiService";
import {BlogPostInfoModel} from "../../../models/blog/BlogPostModel";
import {setErrorHandled} from "../../CmAction";
import moment from "moment";

export const PostAction = () => {
    const [state, setState] = useRecoilState(PostState)
    const resetState = useResetRecoilState(PostState)
    const vm = useRecoilValue(PostState)

    const onLoadPost = (postId: number) => {
        setState({
            ...state,
            isLoading: SendingStatus.loading
        })

        AxiosClient
            .get(ApiService.getBlogPost(postId))
            .then(r => {
                if (r.success) {
                    setState({
                        ...state,
                        item: r.data && new BlogPostInfoModel(r.data),
                        isLoading: SendingStatus.success,
                        key: postId.toString(),
                        timestamp: moment().unix()
                    })
                } else {
                    setState({
                        ...state,
                        error: r.error,
                        isLoading: SendingStatus.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isLoading', err))
    }

    const resetStateWithEffect = () => {
        setState(initialState);
    }

    return {
        vm,
        onLoadPost,
        onClearState: resetState,
        resetStateWithEffect,
    }
}
