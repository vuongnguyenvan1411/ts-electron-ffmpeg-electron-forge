import {atom} from "recoil";
import {SendingStatus} from "../../../const/Events";
import {BlogPostInfoModel} from "../../../models/blog/BlogPostModel";
import {KeyBlogPost} from "../../KeyRecoil";

type TPostState = {
    key?: string,
    item?: BlogPostInfoModel
    isLoading: SendingStatus
    timestamp?: number
    error?: Record<string, any>
}

export const initialState: TPostState = {
    isLoading: SendingStatus.idle,
}

export const PostState = atom<TPostState>({
    key: KeyBlogPost,
    default: initialState
})
