import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {CategoriesState, initialState} from "./CategoriesState";
import {SendingStatus} from "../../../const/Events";
import {AxiosClient} from "../../../repositories/AxiosClient";
import {ApiService} from "../../../repositories/ApiService";
import {CategoryModel, TCategoriesFilterVO} from "../../../models/blog/CategoryModel";
import {setErrorHandled} from "../../CmAction";
import moment from "moment";
import {PaginateMetaModel} from "../../../models/ApiResModel";
import {Color} from "../../../const/Color";

export const CategoriesAction = () => {
    const [state, setState] = useRecoilState(CategoriesState)
    const resetState = useResetRecoilState(CategoriesState)
    const vm = useRecoilValue(CategoriesState)

    const onLoadItems = (query?: TCategoriesFilterVO) => {
        setState({
            ...state,
            isLoading: SendingStatus.loading
        })

        AxiosClient
            .get(ApiService.getBlogCategories, query)
            .then(r => {
                if (r.success) {
                    let merge = {...state}

                    const page = r.meta ? r.meta.currentPage : 1;

                    if (query) {
                        if (
                            (query.filter && Object.keys(query.filter).length > 0)
                            || (query.sort && query.sort.length > 0)
                            || (query.order && query.order.length > 0)
                        ) {
                            if (page === 1) {
                                merge = {
                                    ...merge,
                                    items: []
                                }

                                console.log('%cReset Item When Filter: CategoriesAction', Color.ConsoleInfo);
                            }
                        }
                    }

                    if (r.meta instanceof PaginateMetaModel) {
                        merge = {
                            ...merge,
                            oMeta: r.meta
                        }

                        const limit = r.meta.perPage;

                        if (limit) {
                            if (merge.query.limit > limit || merge.query.limit !== limit) {
                                merge = {
                                    ...merge,
                                    query: {
                                        ...merge.query,
                                        limit: limit
                                    }
                                }
                            }
                        }
                    }

                    if (r.items) {
                        merge.items = [...state.items]

                        r.items.map((value, index) => merge.items[((page - 1) * merge.query.limit) + index] = new CategoryModel(value))
                    }

                    if (page === 1) {
                        merge = {
                            ...merge,
                            timestamp: moment().unix()
                        }
                    }

                    setState({
                        ...merge,
                        isLoading: SendingStatus.success,
                    })
                } else {
                    setState({
                        ...state,
                        error: r.error,
                        isLoading: SendingStatus.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'isLoading', err))
    }

    const resetStateWithEffect = () => {
        setState(initialState);
    }

    return {
        vm,
        onLoadItems,
        onClearState: resetState,
        resetStateWithEffect,
    }
}
