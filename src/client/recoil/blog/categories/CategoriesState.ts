import {atom} from "recoil";
import {SendingStatus} from "../../../const/Events";
import {KeyBlogCategories} from "../../KeyRecoil";
import {CategoryModel} from "../../../models/blog/CategoryModel";
import {PaginateLinksModel, PaginateMetaModel} from "../../../models/ApiResModel";

export type TCategoriesState = {
    items: CategoryModel[]
    isLoading: SendingStatus
    oMeta?: PaginateMetaModel
    oLinks?: PaginateLinksModel
    query: {
        page: number
        limit: number
    }
    timestamp?: number
    error?: Record<string, any>
}

export const initialState: TCategoriesState = {
    items: [],
    isLoading: SendingStatus.idle,
    query: {
        page: 1,
        limit: 12
    }
}

export const CategoriesState = atom<TCategoriesState>({
    key: KeyBlogCategories,
    default: initialState
})
