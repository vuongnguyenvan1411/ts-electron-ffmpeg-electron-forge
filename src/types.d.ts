interface Navigator {
    msSaveBlob?: (blob: any, defaultName?: string) => boolean;
    xr?: any;
}

declare module '*.svg' {
    import React from 'react'

    const Component: React.FunctionComponent<React.SVGProps<SVGSVGElement>>

    export default Component
}