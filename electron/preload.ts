// eslint-disable @typescript-eslint/no-namespace
// eslint-disable @typescript-eslint/no-unused-vars
import {ipcRenderer, IpcRenderer} from 'electron'

declare global {
    namespace NodeJS {
        interface Global {
            ipcRenderer: IpcRenderer
        }
    }
}

// Since we disabled nodeIntegration we can reintroduce
// needed node functionality here
process.once('loaded', () => {
    (global as any).ipcRenderer = ipcRenderer;
    (global as any).platform = 'AppDesktop';
    (global as any).routerType = 'browser';
})
