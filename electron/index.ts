// Native
import nodePath, {join} from "path";
import {format} from "url";

// Packages
import {app, BrowserWindow, crashReporter, dialog, globalShortcut, ipcMain, IpcMainEvent, Menu, shell, Tray} from "electron";
// import prepareNext from "./core/next";
import {APP_DOMAIN, APP_LOG_DATE_BACK_UP, APP_LOG_FILE, APP_LOG_MAX_FILE, APP_LOG_MAX_SIZE, APP_NAME, APP_SERVER, DEV_PORT, DEV_PORT_HOST, eIsDev, ICON_APP_MAC, ICON_APP_WIN, ICON_TRAY_MAC, ICON_TRAY_WIN, PATH_APP_DATA} from "./core/config";
import {AutoUpdateMac} from "./install/autoUpdateMac";
import {AutoUpdateWin} from "./install/autoUpdateWin";
import {Store} from "./core/store";
import i18next from "./locales/i18n";
import {ATL_RenderVideoCmd} from "./cmd/ATL_RenderVideoCmd";
import {NextTronCmdName} from "./core/events";
import {Sys_GetPathCmd} from "./cmd/Sys_GetPathCmd";
import {Form_InputGetPathCmd} from "./cmd/Form_InputGetPathCmd";
import {Sys_GetOsInfoCmd} from "./cmd/Sys_GetOsInfoCmd";
import {Sys_MonitorOsCmd} from "./cmd/Sys_MonitorOsCmd";
import chalk from "chalk";
import log from "electron-log";
import fs from "fs";
import moment from "moment/moment";
import {orderBy} from "lodash";

class Main {
    private mainWindow?: BrowserWindow
    private trayApp?: Tray
    private checkForUpdate?: AutoUpdateMac | AutoUpdateWin

    constructor() {
        this._setCrashReporter()
        this._setAppLogs()
    }

    protected _setCrashReporter() {
        crashReporter.start({
            submitURL: 'https://dev-apis.autotimelapse.com/ad/notify/crash-reporter',
            rateLimit: true
        })
        // process.crash()
    }

    /**
     * Log ra stdout khi dev & lưu lại vào file log ở appData
     * - file log không quá 10MB
     * - tổng số file log không vượt quá 10
     */
    protected _setAppLogs() {
        const logFile = nodePath.join(PATH_APP_DATA, 'logs', APP_LOG_FILE)

        fs.stat(logFile, (err, stats) => {
            const setUpLog = () => {
                log.transports.file.resolvePath = () => logFile
                console.log(chalk.green(`Logs main store in ${logFile}`))

                log.info('App Start')
            }

            if (err) {
                setUpLog()

                return console.log(err)
            }

            if (stats.size >= APP_LOG_MAX_SIZE) {
                const dtLogFile = nodePath.join(PATH_APP_DATA, 'logs', `main_${moment().format(APP_LOG_DATE_BACK_UP)}.log`)

                try {
                    fs.renameSync(logFile, dtLogFile)
                    console.log(chalk.green(`Logs main backup in ${dtLogFile}`))
                } catch (e) {
                    console.log(e)
                }
            }

            setUpLog()

            fs.readdir(nodePath.dirname(logFile), (err, files) => {
                if (err) {
                    return console.log(err)
                }

                const _items = files.filter(value => value.indexOf(APP_LOG_FILE) === -1)

                if (_items.length > APP_LOG_MAX_FILE) {
                    let items = _items
                        .map(value => {
                            const date = value.replace('main_', '').replace('.log', '')

                            return {
                                name: value,
                                date: moment(date, APP_LOG_DATE_BACK_UP).unix()
                            }
                        })

                    items = orderBy(items, 'date', 'asc').splice(0, _items.length - APP_LOG_MAX_FILE)

                    items.forEach((file, index) => {
                        fs.rm(
                            nodePath.join(nodePath.dirname(logFile), file.name),
                            {
                                maxRetries: 2,
                                retryDelay: 1
                            },
                            (err) => {
                                if (err) {
                                    return console.log(err)
                                }

                                console.log(chalk.yellow(`${index}: Remove file ${file.name}`))
                            }
                        )
                    })
                }
            })
        })
    }

    render() {
        // Prepare the renderer once the app is ready
        app.on('ready', this.onAppReady)

        // Quit the app once all windows are closed
        app.on('window-all-closed', this.onAppWindowAllClosed)

        // listen the channel `message` and resend the received message to the renderer process
        ipcMain.on('message', (event: IpcMainEvent, message: any) => {
            if (!this.mainWindow) {
                return
            }

            if (typeof message === "string") {
                dialog
                    .showMessageBox(this.mainWindow, {
                        type: 'info',
                        buttons: [i18next.t('text.close')],
                        defaultId: 0,
                        message: i18next.t('text.info'),
                        detail: message
                    })
                    .then()
            }
        })

        ipcMain.on(NextTronCmdName.AtlRenderVideo, (event, args) => new ATL_RenderVideoCmd(event, args))
        ipcMain.on(NextTronCmdName.SystemGetPath, (event, args) => new Sys_GetPathCmd(event, args))
        ipcMain.on(NextTronCmdName.FormInputGetPath, (event) => new Form_InputGetPathCmd(event))
        ipcMain.on(NextTronCmdName.SystemGetOsInfo, (event) => new Sys_GetOsInfoCmd(event))
        ipcMain.on(NextTronCmdName.SystemMonitorOs, (event) => new Sys_MonitorOsCmd(event))
    }

    protected onAppReady = async () => {
        const isRootHost = process.env.IS_ROOT_HOST

        if (eIsDev() && !isRootHost) {
            console.log(chalk.green(`Electron next server dev starting... http://localhost:${DEV_PORT}`))
            // await prepareNext('./', DEV_PORT)
        }

        // Window
        const mainWindow = new BrowserWindow({
            // width: WINDOW_WIDTH,
            // height: WINDOW_HEIGHT,
            title: APP_NAME,
            webPreferences: {
                nodeIntegration: false,
                contextIsolation: false,
                preload: join(__dirname, 'preload.js')
            },
            icon: join(__dirname, (process.platform === 'darwin') ? ICON_APP_MAC : ICON_APP_WIN)
        })
        mainWindow.maximize()
        mainWindow.focus()
        // mainWindow.setMenu(null)

        if (eIsDev()) {
            mainWindow.webContents.openDevTools()
        }

        const url = isRootHost
            ? isRootHost === 'production'
                ? APP_SERVER
                : `http://localhost:${DEV_PORT_HOST}/`
            : eIsDev()
                ? `http://localhost:${DEV_PORT}/`
                : format({
                    pathname: join(__dirname, '../renderer/index.html'),
                    protocol: 'file:',
                    slashes: false
                })

        console.log(chalk.green(`Electron load url: ${url}`))

        this.mainWindow = mainWindow

        mainWindow
            .loadURL(url)
            .then(() => {
                // Check for update
                const page = mainWindow.webContents

                page.once('did-frame-finish-load', () => {
                    if (!mainWindow) {
                        return
                    }

                    // Auto-updates on macOs, windows and linux
                    if (process.platform === 'darwin') {
                        this.checkForUpdate = new AutoUpdateMac(mainWindow)
                    } else {
                        this.checkForUpdate = new AutoUpdateWin(mainWindow)
                    }
                })

                // Tray
                const iconPath = join(__dirname, (process.platform === 'darwin') ? ICON_TRAY_MAC : ICON_TRAY_WIN)

                this.trayApp = new Tray(iconPath)

                const contextMenu = Menu.buildFromTemplate([
                    {
                        label: i18next.t('text.about'),
                        click: this.onTrayInfo
                    },
                    {
                        label: i18next.t('text.update'),
                        click: this.onTrayUpdate
                    },
                    {
                        label: i18next.t('text.exit'),
                        click: this.onTrayExit
                    }
                ])
                this.trayApp.setToolTip('AutoTimelapse')
                this.trayApp.setContextMenu(contextMenu)
                // double click then show app
                this.trayApp.on('double-click', this.onTrayDoubleClick)

                // to make singleton instance
                app.requestSingleInstanceLock()
                app.on('second-instance', this.onAppSecondInstance)

                // Disable reload via keyboard shortcut electron app
                if (!eIsDev()) {
                    app.on('browser-window-focus', function () {
                        globalShortcut.register("CommandOrControl+R", () => {
                            console.log(chalk.yellow("CommandOrControl+R is pressed: Shortcut Disabled"))
                        })
                        globalShortcut.register("F5", () => {
                            console.log(chalk.yellow("F5 is pressed: Shortcut Disabled"))
                        })
                    })
                    app.on('browser-window-blur', function () {
                        globalShortcut.unregister('CommandOrControl+R')
                        globalShortcut.unregister('F5')
                    })
                }
            })
            .catch(e => {
                log.error('Electron loadURL', e)
            })
    }

    protected onAppWindowAllClosed = () => {
        // On OS X it is common for applications and their menu bar
        // to stay active until the user quits explicitly with Cmd + Q
        if (process.platform !== 'darwin') {
            app.quit()
        }

        // Close windows => close tray
        if (this.trayApp) this.trayApp.destroy()
    }

    protected onAppSecondInstance = (evt: Event) => {
        console.log('onAppSecondInstance', evt)

        if (!this.mainWindow) {
            return
        }

        // Someone tried to run a second instance, we should focus our window.
        if (this.mainWindow.isMinimized()) {
            this.mainWindow.restore()
        } else {
            this.mainWindow.show()
            this.mainWindow.focus()
        }
    }

    protected onTrayInfo = () => {
        if (!this.mainWindow) {
            return
        }

        let message: string = '';

        message += `Phiên bản ${app.getVersion()}\n`;
        message += 'Thiết kế bới I&I Group\n';

        dialog
            .showMessageBox(this.mainWindow, {
                type: 'info',
                buttons: [i18next.t('text.home'), i18next.t('text.ignore')],
                defaultId: 0,
                message: i18next.t('text.infoApp'),
                detail: message
            })
            .then(({response}) => {
                if (response === 0) {
                    shell
                        .openExternal(APP_DOMAIN)
                        .then()
                }
            })
    }

    protected onTrayExit = () => {
        if (!this.mainWindow) {
            return
        }

        dialog.showMessageBox(this.mainWindow, {
            type: 'question',
            buttons: [i18next.t('text.exitApp'), i18next.t('text.ignore')],
            defaultId: 0,
            message: i18next.t('msg.exitApp'),
        })
            .then(({response}) => {
                if (response === 0) {
                    Store.isConfirmExit = false
                    app.quit()
                }
            })
    }

    protected onTrayUpdate = () => {
        if (this.checkForUpdate) {
            this.checkForUpdate.checkUpdate()
        }
    }

    protected onTrayDoubleClick = () => {
        if (!this.mainWindow) {
            return
        }

        this.mainWindow.show()
    }
}

const ejs = new Main()
ejs.render()
