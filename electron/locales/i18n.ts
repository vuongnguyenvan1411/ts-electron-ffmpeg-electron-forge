import i18next from "i18next";
import fsBackend, {FsBackendOptions} from "i18next-fs-backend"
import {join} from "path";
import {lstatSync, readdirSync} from "fs";
import chalk from "chalk";

const i18nextOptions: FsBackendOptions = {
    loadPath: join(__dirname, 'locales/{{lng}}/{{ns}}.json'),

    // path to post missing resources
    addPath: join(__dirname, 'locales/{{lng}}/{{ns}}.missing.json'),
}

i18next.use(fsBackend)

// initialize if not already initialized
if (!i18next.isInitialized) {
    i18next
        .init({
            backend: i18nextOptions,
            lng: 'vi',
            fallbackLng: 'vi',
            ns: ['translation'],
            defaultNS: 'translation',
            preload: readdirSync(join(__dirname, 'locales')).filter((fileName) => {
                const joinedPath = join(join(__dirname, 'locales'), fileName)

                return lstatSync(joinedPath).isDirectory()
            }),
        }, (error, t) => {
            if (error) return console.info(chalk.red('Error: i18next init'), error)

            console.log(chalk.green('i18next is ready...'))
        })
        .then()
}

export default i18next
