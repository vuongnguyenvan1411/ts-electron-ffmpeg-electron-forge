import {dialog, IpcMainEvent} from "electron";
import {NextTronEventName} from "../core/events";
import chalk from "chalk";

export class Form_InputGetPathCmd {
    constructor(event: IpcMainEvent) {
        console.log(
            chalk.green('Form_InputGetPathCmd'),
            "\n"
        )

        dialog.showOpenDialog({
            properties: ['openDirectory']
        }).then(result => {
            if (!result.canceled) {
                event.sender.send(NextTronEventName.FormInputGetPathComplete, result.filePaths[0])
            }
        }).catch(err => {
            console.log(err)
        })
    }
}
