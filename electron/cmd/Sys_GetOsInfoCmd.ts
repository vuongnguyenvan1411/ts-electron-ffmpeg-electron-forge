import {IpcMainEvent} from "electron";
import {NextTronEventName} from "../core/events";
import chalk from "chalk";
import os from "os";

export class Sys_GetOsInfoCmd {
    constructor(event: IpcMainEvent) {
        console.log(
            chalk.green('Sys_GetOsInfoCmd'),
            "\n"
        )

        event.sender.send(NextTronEventName.SystemGetOsInfoComplete, {
            os: {
                platform: os.platform(),
                arch: os.arch(),
                version: os.version(),
                release: os.release()
            },
            cpu: {
                threads: os.cpus().length
            },
            ram: {
                total: os.totalmem()
            }
        })
    }
}
