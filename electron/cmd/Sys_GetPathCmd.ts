import {app, IpcMainEvent} from "electron";
import {NextTronEventName} from "../core/events";
import chalk from "chalk";
import os from "os";

export class Sys_GetPathCmd {
    constructor(event: IpcMainEvent, name: string) {
        console.log(
            chalk.green('Sys_GetPathCmd'),
            `- Name: ${name}`,
            "\n"
        )
        const threads = os.cpus().length
        event.sender.send(NextTronEventName.SystemGetPathComplete, app.getPath(name as any), threads)
    }
}
