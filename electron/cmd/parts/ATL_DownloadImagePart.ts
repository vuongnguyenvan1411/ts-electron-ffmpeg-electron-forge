import {each} from "lodash";

type _T_Options = {
    simultaneousDownloads?: number
    maxRetries?: number
    retryInterval?: number
}

export class ATL_DownloadImagePart {
    opts: _T_Options
    files: DownloadFile[]

    constructor(opts: _T_Options) {
        const defaults: _T_Options = {
            simultaneousDownloads: 3,
            maxRetries: 0,
        }

        this.opts = {
            ...defaults,
            ...opts
        }

        this.files = []
    }

    protected _shouldUploadNext = (): boolean | number => {
        let num = 0
        let should = true
        const simultaneousDownloads = this.opts.simultaneousDownloads

        each(this.files, file => {
            // each(file.chunks, chunk => {
            //     if (chunk.status() === 'uploading') {
            //         num++
            //
            //         if (simultaneousDownloads) {
            //             if (num >= simultaneousDownloads) {
            //                 should = false
            //
            //                 return false
            //             }
            //         }
            //     }
            // })
        })

        // if should is true then return uploading chunk's length
        return should && num
    }

    download = () => {

    }
}

class DownloadFile {

}
