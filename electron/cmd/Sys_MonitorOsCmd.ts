import {IpcMainEvent} from "electron";
import {NextTronEventName} from "../core/events";
import chalk from "chalk";
import os from "os";
import {Store} from "../core/store";

export class Sys_MonitorOsCmd {
    event: IpcMainEvent
    interval: NodeJS.Timer

    constructor(event: IpcMainEvent) {
        console.log(
            chalk.green('Sys_MonitorOsCmd'),
            "\n"
        )

        this.event = event

        Store.isMonitorOs = true

        this.interval = setInterval(this.onSetInterval, 1000 * 2)
    }

    protected onSetInterval = () => {
        if (!Store.isMonitorOs) {
            clearInterval(this.interval)

            return
        }

        // Take the first CPU, considering every CPU have the same specs
        // and every NodeJS process only uses one at a time.
        const cpus = os.cpus()
        const cpu = cpus[0]

        // Accumulate every CPU times values
        const total = Object.values(cpu.times).reduce(
            (acc, tv) => acc + tv, 0
        )

        // Normalize the one returned by process.cpuUsage()
        // (microseconds VS milliseconds)
        const usage = process.cpuUsage()
        const currentCPUUsage = (usage.user + usage.system) * 1000

        const percentCpu = (currentCPUUsage / total) * 100

        // Ram
        const percentRam = 100 - (os.freemem() / os.totalmem() * 100)

        this.event.sender.send(NextTronEventName.SystemMonitorOsComplete, {
            cpu: {
                percent: percentCpu
            },
            ram: {
                percent: percentRam
            }
        })
    }
}
