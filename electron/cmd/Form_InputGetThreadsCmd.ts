import {IpcMainEvent} from "electron";
import {NextTronEventName} from "../core/events";
import chalk from "chalk";

const {dialog} = require('electron')

export class Form_InputGetThreadsCmd {

    constructor(event: IpcMainEvent) {
        console.log(
            chalk.green('Form_InputGetPathC'),
            "\n"
        )

        event.sender.send(NextTronEventName.FormInputGetPathComplete)
    }


}
