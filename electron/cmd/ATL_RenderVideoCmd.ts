import {app, IpcMainEvent, Notification, shell} from "electron";
import {NextTronEventName, T_ATL_RenderVideoArgs, T_FfmpegProgressCallback} from "../core/events";
import nodePath from "path";
import fs from "fs";
import axios from "axios";
import {includes, kebabCase, orderBy, padStart, replace} from "lodash";
import Ffmpeg from "fluent-ffmpeg"
import chalk from "chalk";
import moment from "moment/moment";
import {Utils} from "../core/utils";
import * as os from "os";
import {Store} from "../core/store";
import log from "electron-log";
import i18next from "../locales/i18n";
import {PATH_APP_DATA} from "../core/config";
import Path from "ffmpeg-static"

type _T_DownloadValueEvent = {
    index: number
    order: number
    file: string
}

enum _E_ImageState {
    idea,
    success,
    error
}

type _T_ImageItem = {
    id: string
    name: string
    order: number
    link: string
    state: _E_ImageState
    retry: number
    file?: string
}

export class ATL_RenderVideoCmd {
    protected readonly DIR_FILES = 'files'
    protected readonly FILE_RENDER = 'render.txt'

    event: IpcMainEvent
    size: number
    items: _T_ImageItem[]
    sysTempDir: string
    tempDir: string
    tempTxt: string
    tempFiles: string
    outputDir: string
    videoName: string
    imgExt: string
    fps: number
    threads: number

    constructor(event: IpcMainEvent, args: T_ATL_RenderVideoArgs) {
        console.log(
            chalk.green('ATL_RenderVideoCmd'),
            `- Size: ${args.size}`,
            `- FPS: ${args.fps}`,
            `- Frames: ${args.items.length}`,
            `- Frames: ${args.threads}`,
            "\n"
        )


        let defaultThreads = os.cpus().length - 1
        if (defaultThreads == 0) {
            defaultThreads = 1
        }

        this.event = event
        this.size = args.size
        this.fps = args.fps
        this.items = args.items.map(value => ({
            ...value,
            state: _E_ImageState.idea,
            retry: 0
        }))
        this.items = orderBy(this.items, 'order', 'asc')
        this.threads = args.threads ?? defaultThreads

        if (this.threads > defaultThreads) {
            this.threads = defaultThreads
        }

        const unix = moment().unix().toString()

        // Thự mục tạm của hệ thống để lưu các file tạm thời upload
        // this.sysTempDir = nodePath.join(app.getAppPath(), 'temp')
        // this.sysTempDir = app.getPath("downloads")
        this.sysTempDir = nodePath.join(PATH_APP_DATA, 'temp')
        console.log(`SysTempDir  ${this.sysTempDir}`)

        // this.tempName = `${this.size.toString()}_${moment().unix().toString()}`
        this.tempDir = nodePath.join(this.sysTempDir, `${this.size.toString()}_${unix}`)

        console.log(chalk.green(`Download images in ${this.tempDir}`))

        this.tempTxt = nodePath.join(this.tempDir, this.FILE_RENDER)
        this.tempFiles = nodePath.join(this.tempDir, this.DIR_FILES)

        //Vị trí tệp thực thi của ffmpeg
        console.log("File bin of Ffmpeg on : ", Path )

        // Thư mục lưu video
        this.outputDir = args.output ?? app.getPath('downloads')
        this.videoName = `${this.size}_${(args.name ? kebabCase(Utils.strSlug(args.name)) : 'output')}_${unix}`

        this.imgExt = includes([426, 1280, 1920], this.size) ? 'webp' : 'jpg'

        this
            .downloadImageSync()
            .catch(e => {
                this.event.sender.send(NextTronEventName.AtlRenderVideoErrorDownload, e)
            })

        // this.downloadImage()
        //     .then((r) => {
        //         let files: _T_DownloadValueEvent[] = []
        //
        //         r.forEach(item => {
        //             if (item.status === "fulfilled") {
        //                 files.push(item.value)
        //             } else {
        //                 // this.event.sender.send(NextTronEventName.AtlRenderVideoErrorDownload, new Error(item.status))
        //             }
        //         })
        //
        //         if (files.length > 0) {
        //             files = orderBy(files, 'order', 'asc')
        //             fs.writeFile(
        //                 this.tempTxt,
        //                 files.map(v => `file '${v.file}'`).join("\n"),
        //                 (err) => {
        //                     if (!err) {
        //                         this.renderVideo(files.length)
        //                     } else {
        //                         console.log(chalk.red(err))
        //                     }
        //                 }
        //             )
        //
        //             this.event.sender.send(NextTronEventName.AtlRenderVideoCompleteDownload)
        //         }
        //     })
        //     .catch(() => {
        //         this.event.sender.send(NextTronEventName.AtlRenderVideoErrorDownload, new Error('Error Download'))
        //     })
    }

    protected downloadImageSync(): Promise<string> {
        console.log(chalk.green('Start: DownloadImage'))
        const startTime = Date.now()
        let arr : any = []
        return new Promise((resolve, reject) => {
            let isExist = true

            if (!fs.existsSync(this.tempFiles)) {
                if (!fs.mkdirSync(this.tempFiles, {
                    recursive: true
                })) {
                    isExist = false
                }
            }

            if (isExist) {
                const finalIdx = this.items.length - 1

                let errorTotal = 0
                let successTotal = 0

                const writeFileTempRender = () => {
                    let files: string[] = []

                    this.items.forEach(item => {
                        if (item.file !== undefined) {
                            files.push(item.file)
                        }
                    })

                    if (files.length > 0) {
                        files = orderBy(files, 'order', 'asc')
                        fs.writeFile(
                            this.tempTxt,
                            files.map(v => `file '${v}'`).join("\n"),
                            (err) => {
                                if (err) {
                                    return log.error(err)
                                }

                                this.renderVideo(files.length)
                            }
                        )

                        this.event.sender.send(NextTronEventName.AtlRenderVideoCompleteDownload)
                    } else {
                        this.event.sender.send(NextTronEventName.AtlRenderVideoErrorDownload, new Error('Error download file'))
                    }
                }

                const download = (index: number = 0): boolean => {
                    if (index > finalIdx) {
                        return false
                    }

                    const item = this.items[index]
                    const nextIdx = index + 1

                    if (
                        !item
                        || item.state === _E_ImageState.success
                    ) {
                        return download(nextIdx)
                    }

                    console.log(chalk.green(`Downloading ${index}: ${item.name}`))

                    const url = item.link
                    const order = item.order

                    const setState = (state: _T_ImageItem["state"], merge?: {}) => {
                        this.items[index] = {
                            ...item,
                            state: state,
                            ...(
                                merge && merge
                            )
                        }

                        if (state == _E_ImageState.success) {
                            successTotal++
                        } else if (state == _E_ImageState.error) {
                            errorTotal++
                        }

                        this.event.sender.send(NextTronEventName.AtlRenderVideoProgressDownload, {
                            index: index,
                            order: order,
                            percent: (index / this.items.length) * 100,
                            percentSuccess: (successTotal / this.items.length) * 100,
                            percentError: (errorTotal / this.items.length) * 100
                        })

                        if (index == finalIdx) {
                            writeFileTempRender()
                        }

                        download(nextIdx)
                    }

                    const filename = nodePath.resolve(this.tempFiles, `${padStart(order.toString(), 6, '0')}.${this.imgExt}`)
                    console.log(`FILE NAME :  ${filename}`)

                    if (fs.existsSync(filename)) {
                        setState(_E_ImageState.success, {
                            file: filename
                        })
                    }

                    axios({
                            url: url,
                            method: 'GET',
                            responseType: 'stream',
                        }
                    )
                        .then(res => {
                            if (index === 0) {
                                this.event.sender.send(NextTronEventName.AtlRenderVideoStartDownload)
                            }
                            const writer = fs.createWriteStream(filename, {autoClose: true})

                            res.data.pipe(writer)

                            writer.on('finish', () => {
                                setState(_E_ImageState.success, {
                                    file: filename
                                })
                                const finish = Date.now()
                                const time = (finish - startTime) / 1000
                                let total_time = time * this.items.length
                                arr.push(total_time)
                                let time_remain = 60
                                if(time < +arr[0]) {
                                     time_remain = +arr[0] - time
                                }

                                this.event.sender.send(NextTronEventName.AtlRenderVideoTimeRemainDownload, time_remain)
                            })
                            writer.on('error', (err) => {
                                setState(_E_ImageState.error)

                                log.error('Write file download', err.message)
                            })
                        })
                        .catch(e => {
                            setState(_E_ImageState.error)
                            console.log('Error download !')

                            log.error('Download file download', e.message)
                        })

                    return true
                }

                download()

                resolve("OK")
            } else {
                reject(new Error('Error Download'))
            }
        })
    }

    // protected downloadImage(): Promise<PromiseSettledResult<Awaited<_T_DownloadValueEvent>>[]> {
    //     console.log(chalk.green('Start:DownloadImage'))
    //
    //     if (!fs.existsSync(this.tempFiles)) {
    //         if (!fs.mkdirSync(this.tempFiles, {
    //             recursive: true
    //         })) {
    //             return new Promise((resolve, reject) => {
    //                 reject()
    //             })
    //         }
    //     }
    //
    //     let idx = 0
    //
    //     const download = async (url: string, order: number, index: number): Promise<_T_DownloadValueEvent> => {
    //         const filename = nodePath.resolve(this.tempFiles, `${padStart(order.toString(), 6, '0')}.${this.imgExt}`)
    //         if (fs.existsSync(filename)) {
    //             return new Promise((resolve) => {
    //                 resolve({
    //                     index: index,
    //                     order: order,
    //                     file: filename
    //                 })
    //             })
    //         }
    //
    //         return axios({
    //             url: url,
    //             method: 'GET',
    //             responseType: 'stream'
    //         }).then(res => {
    //             if (index === 0) {
    //                 this.event.sender.send(NextTronEventName.AtlRenderVideoStartDownload)
    //             }
    //
    //             if (index === this.items.length - 1) {
    //                 this.event.sender.send(NextTronEventName.AtlRenderVideoCompleteDownload)
    //             }
    //
    //             const writer = fs.createWriteStream(filename, {autoClose: true})
    //
    //             res.data.pipe(writer)
    //
    //             return new Promise((resolve, reject) => {
    //                     writer.on('finish', () => {
    //                         idx++
    //
    //                         this.event.sender.send(NextTronEventName.AtlRenderVideoProgressDownload, {
    //                             index: idx,
    //                             order: order,
    //                             percent: (idx / this.items.length) * 100
    //                         })
    //
    //                         resolve({
    //                             index: index,
    //                             order: order,
    //                             file: filename
    //                         })
    //                     })
    //                     writer.on('error', (err) => {
    //                         console.log('Error: Write file download', err)
    //
    //                         reject(err)
    //                     })
    //                 }
    //             )
    //         })
    //     }
    //
    //     return Promise.allSettled([
    //         ...this.items.map((item, index) => download(item.link, item.order, index))
    //     ])
    // }

    protected renderVideo(frames: number) {
        console.log(chalk.green('Start: RenderVideo'))

        const duration = frames / this.fps
        console.log(`PATHS FFMPEG: ${this.size} `)
        let lastDateTime = Date.now()
        let frameIdx = 0
        let averageSpeed = 0

        Ffmpeg(this.tempTxt)
            // .setFfmpegPath(`${app.getAppPath()}/bin/ffmpeg`)
            .setFfmpegPath(`${Path}`)
            .inputOptions([
                `-threads ${this.threads}`,
                '-f concat',
                '-safe 0'
            ])
            .outputOptions([
                '-vf mpdecimate,setpts=N/FRAME_RATE/TB,pad=ceil(iw/2)*2:ceil(ih/2)*2'
            ])
            .videoCodec(this.size >= 4000 ? 'libx265' : 'libx264')
            .fps(this.fps)
            .frames(frames)
            .duration(moment().startOf('day').seconds(duration).format('HH:mm:ss.SS'))
            //.size('426x284')
            .save(nodePath.join(this.outputDir, `${this.videoName}.mp4`))
            .on('progress', (progress: T_FfmpegProgressCallback) => {
                let timemark = progress.timemark

                if (timemark.indexOf('-') >= 0) {
                    timemark = replace(timemark, '-', '')
                }

                let percent = (moment.duration(timemark).asSeconds() / duration) * 100

                if (percent < 1) {
                    percent += 1
                }

                averageSpeed = Date.now() - lastDateTime
                lastDateTime = Date.now()
                const remaining = Math.floor((frames - frameIdx) / averageSpeed * 1000)
                console.log(remaining)

                this.event.sender.send(NextTronEventName.AtlRenderVideoProgressRender, {
                    ...progress,
                    timemark: timemark,
                    percent: percent,
                    remaining: remaining
                })

                console.log(chalk.green(`Rendering ${timemark} ${duration}`), averageSpeed, remaining, frameIdx, lastDateTime)

                frameIdx++
            })
            .on('start', (cmd) => {
                console.log(chalk.blue(cmd))

                this.event.sender.send(NextTronEventName.AtlRenderVideoStartRender)
            })
            .on('end', () => {
                // this.removeTemp().then().catch()

                const yourFilesize = fs.statSync(`${this.outputDir}/${this.videoName}.mp4`).size
                const fileSize = Math.round(yourFilesize / (1024 * 1024))

                this.event.sender.send(NextTronEventName.AtlRenderVideoCompleteRender, fileSize)
                shell.openPath(`${this.outputDir}`).then()

                if (Store.isMonitorOs) {
                    Store.isMonitorOs = false
                }

                new Notification({
                    title: i18next.t('text.notify'),
                    body: 'Render video đã hoàn thành'
                }).show()
            })
            .on('error', (error) => {
                // this.removeTemp().then().catch()

                this.event.sender.send(NextTronEventName.AtlRenderVideoErrorRender, error)

                log.error('Render video', error)
            })
            .on('codecData', (data) => {
                console.log(chalk.green('codecData'), data)
            })
    }

    // protected removeTemp(): Promise<string> {
    //     return new Promise((resolve, reject) => {
    //         fs.rm(
    //             this.tempDir,
    //             {
    //                 recursive: true,
    //                 maxRetries: 2,
    //                 retryDelay: 1
    //             },
    //             (err) => {
    //                 if (err) {
    //                     console.log('Error: Remove temp', err)
    //
    //                     reject(err)
    //                 } else {
    //                     resolve('OK')
    //                 }
    //             }
    //         )
    //     })
    // }
}
