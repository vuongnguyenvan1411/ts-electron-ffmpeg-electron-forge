import {autoUpdater, UpdateDownloadedEvent} from "electron-updater"
import {IS_DEV, PLATFORM, SERVER_UPDATE} from "../core/config";
import {app, BrowserWindow, dialog} from "electron"
import {Store} from "../core/store";
import i18next from "../locales/i18n";

const UPDATE_URL = `${SERVER_UPDATE}/${PLATFORM}`

export class AutoUpdateWin {
    private readonly win: BrowserWindow

    constructor(win: BrowserWindow) {
        this.win = win

        autoUpdater.on('update-available', this.onUpdateAvailable)
        autoUpdater.on('checking-for-update', this.onCheckingForUpdate)
        autoUpdater.on('update-not-available', this.onUpdateNotAvailable)
        autoUpdater.on('update-downloaded', this.onUpdateDownloaded)
        autoUpdater.on('error', this.onUpdateError)

        this.checkUpdate()
    }

    public checkUpdate = () => {
        try {
            autoUpdater.setFeedURL(UPDATE_URL)
            autoUpdater
                .checkForUpdates()
                .then()
        } catch (error) {
            if (IS_DEV) {
                console.log('Error:autoUpdater.checkForUpdates\n', error)
            }
        }
    }

    protected onUpdateAvailable = () => {
        if (IS_DEV) {
            console.log('AutoUpdate:update-available\n')
        }

        this.sendStatusToWindow('AutoUpdate:update-available')
    }

    protected onCheckingForUpdate = () => {
        if (IS_DEV) {
            console.log('AutoUpdate:checking-for-update\n')
        }

        this.sendStatusToWindow('AutoUpdate:checking-for-update')
    }

    protected onUpdateNotAvailable = () => {
        if (IS_DEV) {
            console.log('AutoUpdate:update-not-available\n')
        }

        this.sendStatusToWindow('AutoUpdate:update-not-available')
    }

    protected onUpdateDownloaded = (event: UpdateDownloadedEvent) => {
        if (IS_DEV) {
            console.log('AutoUpdate:update-downloaded', event);
        }

        this.sendStatusToWindow('AutoUpdate:update-downloaded');

        let message = i18next.t('msg.updateApp', {
            name: app.getName(),
            release: event.releaseName
        })

        if (event.releaseNotes) {
            const splitNotes = typeof event.releaseNotes === "string" ? event.releaseNotes.split(/[^\r]\n/) : event.releaseNotes
            message += '\n\nGhi chú phiên bản mới:\n'

            splitNotes.forEach(notes => {
                message += notes + '\n\n'
            })
        }

        // Ask user to update the app
        dialog.showMessageBox(this.win, {
            type: 'question',
            buttons: [i18next.t('text.updateAndInstall'), i18next.t('text.later')],
            defaultId: 0,
            message: i18next.t('msg.updateDialog', {name: app.getName()}),
            detail: message
        })
            .then(({response}) => {
                if (response === 0) {
                    Store.isConfirmExit = false

                    autoUpdater.quitAndInstall()

                    setTimeout(() => {
                        app.relaunch({
                            args: process.argv.slice(1).concat(['--relaunch'])
                        })
                        autoUpdater.quitAndInstall()
                    }, 1)
                }
            })
    }

    protected onUpdateError = (err: Error) => {
        if (IS_DEV) {
            console.log('AutoUpdate:error\n', err)
        }

        this.sendStatusToWindow(`AutoUpdate:error ${err.toString()}`)
    }

    protected sendStatusToWindow = (text: string) => {
        console.info(text)

        if (this.win) {
            this.win.webContents.send('message', text)
        }
    }
}
