import {app} from "electron"
import * as path from "path"
import {spawn} from "child_process";

export class SetupEvents {
    protected exeName: string = path.basename(process.execPath);

    handleSquirrelEvent = () => {
        if (process.argv.length === 1) {
            return false
        }

        const squirrelEvent = process.argv[1]

        switch (squirrelEvent) {
            case '--squirrel-install':
            case '--squirrel-updated':
                this.sqUpdated()

                return true
            case '--squirrel-uninstall':
                this.sqUninstall()

                return true
            case '--squirrel-obsolete':
                this.sqObsolete()

                return true
        }
    }

    protected spawnUpdate = (args: string[]) => {
        const appFolder = path.resolve(process.execPath, '..')
        const rootAtomFolder = path.resolve(appFolder, '..')
        const updateDotExe = path.resolve(path.join(rootAtomFolder, 'Update.exe'))

        return this.spawn(updateDotExe, args)
    }

    protected spawn = (command: string, args: string[]) => {
        let spawnedProcess

        try {
            spawnedProcess = spawn(command, args, {detached: true})
        } catch (error) {
            //
        }

        return spawnedProcess
    }

    protected sqInstall = (done?: boolean) => {
        //
    }

    protected sqUpdated(done?: boolean) {
        // Optionally do things such as:
        // - Add your .exe to the PATH
        // - Write to the registry for things like file associations and
        // explorer context menus

        // Remove old shortcuts
        this.spawnUpdate(['--removeShortcut', this.exeName])
        // Install desktop and start menu shortcuts
        this.spawnUpdate(['--createShortcut', this.exeName])
    }

    protected sqUninstall(done?: boolean) {
        // Undo anything you did in the --squirrel-install and
        // --squirrel-updated handlers

        // Remove desktop and start menu shortcuts
        this.spawnUpdate(['--removeShortcut', this.exeName])

        setTimeout(app.quit, 1000)
    }

    protected sqObsolete = (done?: boolean) => {
        // This is called on the outgoing version of your app before
        // we update to the new version - it's the opposite of
        // --squirrel-updated

        app.quit()
    }
}
