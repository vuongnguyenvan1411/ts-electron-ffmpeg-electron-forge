import {app, BrowserWindow, dialog, Menu, shell} from 'electron';
import i18next from "../../locales/i18n";
import {APP_DOMAIN} from "../../core/config";
import {Store} from "../../core/store";
import {AutoUpdateMac} from "../../install/autoUpdateMac";
import {AutoUpdateWin} from "../../install/autoUpdateWin";

class _Tray {
    private mainWindow?: BrowserWindow
    private checkForUpdate?: AutoUpdateMac | AutoUpdateWin

    constructor() {
        const contextMenu = Menu.buildFromTemplate([
            {
                label: i18next.t('text.about'),
                click: this.onTrayInfo
            },
            {
                label: i18next.t('text.update'),
                click: this.onTrayUpdate
            },
            {
                label: i18next.t('text.exit'),
                click: this.onTrayExit
            }
        ])
    }

    protected onTrayInfo = () => {
        if (!this.mainWindow) {
            return
        }

        let message: string = '';

        message += `Phiên bản ${app.getVersion()}\n`;
        message += 'Thiết kế bới I&I Group\n';

        dialog
            .showMessageBox(this.mainWindow, {
                type: 'info',
                buttons: [i18next.t('text.home'), i18next.t('text.ignore')],
                defaultId: 0,
                message: i18next.t('text.infoApp'),
                detail: message
            })
            .then(({response}) => {
                if (response === 0) {
                    shell
                        .openExternal(APP_DOMAIN)
                        .then()
                }
            })
    }

    protected onTrayExit = () => {
        if (!this.mainWindow) {
            return
        }

        dialog.showMessageBox(this.mainWindow, {
            type: 'question',
            buttons: [i18next.t('text.exitApp'), i18next.t('text.ignore')],
            defaultId: 0,
            message: i18next.t('msg.exitApp'),
        })
            .then(({response}) => {
                if (response === 0) {
                    Store.isConfirmExit = false
                    app.quit()
                }
            })
    }

    protected onTrayUpdate = () => {
        if (this.checkForUpdate) {
            this.checkForUpdate.checkUpdate()
        }
    }
}

export function createTemplate() {
    return new _Tray()
}

export function createTrayMenu() {
    // Electron is enforcing certain variables that it doesn't need
    // return Menu.buildFromTemplate(createTemplate());
}
