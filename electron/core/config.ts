import {app} from "electron"
import os from "os";
import path from "path";

export const IS_DEV: boolean = true
export const DEV_PORT: number = 3011
export const DEV_PORT_HOST: number = 3001

export const PATH_APP_DATA: string = path.join(app.getPath('appData'), app.getName())

export const APP_LOG_FILE: string = 'main.log'
export const APP_LOG_MAX_SIZE: number = 10_000_000 // 10MB
export const APP_LOG_MAX_FILE: number = 10
export const APP_LOG_DATE_BACK_UP: string = 'YYYY-MM-DD-HH-mm-ss'

export const APP_NAME: string = 'AutoTimelapse'
export const APP_HOST: string = "autotimelapse.com"
export const APP_DOMAIN: string = `https://${APP_HOST}`
export const APP_SERVER: string = `https://dev-app.${APP_HOST}`
export const SERVER_UPDATE: string = `https://dl.${APP_HOST}/download/com-app/autotimelapse`

export const ICON_APP_WIN: string = 'assets/logo/png/64x64.png'
export const ICON_APP_MAC: string = 'assets/logo/icns/64x64.icns'
export const ICON_TRAY_WIN: string = 'assets/logo/png/32x32.png'
export const ICON_TRAY_MAC: string = 'assets/logo/png/16x16.png'

export const PLATFORM: string = os.platform() + '_' + os.arch()
export const VERSION: string = app.getVersion()

export const WINDOW_WIDTH: number = (process.platform === 'darwin') ? 1290 : 1210
export const WINDOW_HEIGHT: number = 600

export const eIsDev = () => {
    const isEnvSet = 'ELECTRON_IS_DEV' in process.env
    const getFromEnv = Number.parseInt(process.env.ELECTRON_IS_DEV!, 10) === 1

    return isEnvSet ? getFromEnv : !app.isPackaged
}
