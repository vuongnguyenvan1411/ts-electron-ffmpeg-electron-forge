export class Utils {
    public static strSlug = (str: string): string => {
        let filter = str.toLowerCase();

        filter = filter.replace(/[àáạảãâầấậẩẫăằắặẳẵ]/g, "a");
        filter = filter.replace(/[èéẹẻẽêềếệểễ]/g, "e");
        filter = filter.replace(/[ìíịỉĩ]/g, "i");
        filter = filter.replace(/[òóọỏõôồốộổỗơờớợởỡ]/g, "o");
        filter = filter.replace(/[ùúụủũưừứựửữ]/g, "u");
        filter = filter.replace(/[ỳýỵỷỹ]/g, "y");
        filter = filter.replace(/đ/g, "d");
        filter = filter.replace(/ & /g, "-")
        filter = filter.replace(/-&-/g, "-");
        // filter = filter.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g, "-");
        filter = filter.replace(/!|@|%|\^|\*|\(|\)|\+|=|<|>|\?|\/|,|\.|:|;|'| |"|&|#|\[|]|~|$|_/g, "-");

        filter = filter.replace(/-+-/g, "-");
        // filter = filter.replace(/^\-+|\-+$/g, "");
        filter = filter.replace(/^-+|-+$/g, "");

        return filter;
    }

    static base64Encode(str: string | object) {
        if (typeof str == "object") {
            str = JSON.stringify(str)
        }

        return Buffer.from(str).toString('base64')
    }

    static base64Decode(str: string) {
        return Buffer.from(str, 'base64').toString('ascii')
    }

    static joinMsg(msg: string, ...params: any[]): string {
        if (params == null) {
            return msg
        }

        const parts: string[] = msg.split("{")
        let result = parts[0]
        let part
        let sub: string[]
        let index: number

        for (let i: number = 1; i < parts.length; i++) {
            part = parts[i];
            sub = part.split("}");
            index = parseInt(sub[0], 10);
            result +=
                isNaN(index) || params[index] == undefined
                    ? "{" + index + "?}"
                    : params[index];
            result += sub[1];
        }

        return result
    }
}
