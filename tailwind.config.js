module.exports = {
    purge: false,
    content: [
        "./src/client/**/*.{js,ts,jsx,tsx}",
    ],
    corePlugins: {
        aspectRatio: false,
    },
    theme: {
        extend: {
            screens: {
                'sm': '576px',
                'md': '768px',
                'lg': '992px',
                'xl': '1200px',
                'xxl': "1400px",
                '3xl': "1700px",
            },
            colors: {
                "dark50": "rgba(79, 79, 79, 0.5)",
                "dark1": "#172723",
                "dark2": "#4F4F4F",
                "dark3": "#BDBDBD",
                "dark4": "#F4F4F4",
                "primary": "#009B90",
                "bg-button": "rgba(0, 0, 0, 0.66)",
                "2d-primary": "#DE9F59",
                "state-success": "#09B377",
            },
            borderWidth: {
                "0.5": "0.5px",
            },
            boxShadow: {
                'custom': '1px 1px 2px rgba(79, 79, 79, 0.15)',
                'shadow-bg-white': '1px 1px 2px rgba(0, 0, 0, 0.15)',
            },
            height: {
                "4.5": "1.125rem",
                "9.5": "2.375rem",
                "10.5": "2.625rem",
                "13.2": "3.3rem",
                "18": "4.5rem",
                "33.5": "8.375rem",
            },
            width: {
                "4.5": "1.125rem",
                "18px": "1.875rem",
                "13.2": "3.3rem",
                "18": "4.5rem",
                "50": "12.5rem",
                "75": "18.75rem",
            },
            inset: {
                "9.5": "2.375rem",
                "13": "3.25rem",
                "15": "3.75rem",
                "62": "15.5rem"
            },
            padding: {
                "7.5": "1.875rem",
            },
            maxWidth: {
                '20': '5rem',
                '72': '18rem'
            },
            maxHeight: {
                '20': '5rem',
            },
            margin: {
                "0.75": "3px",
                "6.5": "1.625rem"
            },
            borderRadius: {
                '16': "3rem"
            }
        },
    },
    plugins: [
        require('@tailwindcss/aspect-ratio'),
    ],
}
