### Build Docker

```
docker build -t nextjs-docker-timelapse-app .
```

### Run Docker

```
docker run --name nextjs-docker-timelapse-app -d --publish 4001:3000 nextjs-docker-timelapse-app
```

### GitLab: Docker Login

```
docker login registry.gitlab.com
```

### GitLab: Docker Run

```
docker run --name gitlab-nextjs-timelapse-app -d --publish 4101:3000 registry.gitlab.com/autotimelapse/ts-nextjs-timelapse-app
```

### Install dev with SSL
npm install -g local-ssl-proxy
brew install mkcert (https://github.com/FiloSottile/mkcert)
cd _ssl && mkcert localhost 127.0.0.1 ::1

## Add Favicon Images

Tutorial: https://coderrocketfuel.com/article/add-favicon-images-to-a-next-js-website

Web Generator: https://realfavicongenerator.net/

## Helper generate color for change color of svg when using img tag
https://codepen.io/sosuke/pen/Pjoqqp

## WebHint (use webhint to improve your website)
https://webhint.io/docs/user-guide/extensions/extension-browser/#devtools-extension-chrome-edge-firefox

## Various ts compiler errors
https://github.com/visgl/loaders.gl/issues/1828
https://github.com/visgl/loaders.gl/issues/1828#issuecomment-1011574789

## Framework Catalog
https://vis.gl/frameworks/

## Performance Optimization
https://deck.gl/docs/developer-guide/performance

getLoadableWorkerURLFromURL
getLoadableWorkerURLFromSource

nebula.gl (editor)
kepler.gl (web cms)
